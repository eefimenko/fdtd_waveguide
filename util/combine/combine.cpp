#include <stdio.h>
#include <zlib.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char* argv[])
{
    gzFile gz_file = NULL, out = NULL;
    FILE*  in = NULL;
    void*  file_p;
    bool   direct = true;
    char   tmpString[1024];
    int    x_max, y_max;
    double stepx, stepy;
    int start = 1;
    bool rem = false;

    if (argc < 4)
    {
	printf("Usage: [-r - to remove files] <list of files> <output file>\n");
	return 0;
    }
    
    if (Z_NULL == (out = gzopen(argv[argc-1], "w")))
    {
	fprintf(stderr, "Error opening output file\n");
	return -1;
    }

    if(strcmp(argv[1], "-r") == 0)
    {
	start = 2;
	rem = true;
    }

    for (int k = start; k < argc - 1; k++)
    {
	sprintf(tmpString, "%s", argv[k]);
	fprintf(stderr, "Input file: %s\n", tmpString);
	if (NULL == (gz_file = gzopen(tmpString, "r")))
	{
	    sprintf(tmpString, "%s.gz", argv[k]);
	    if (NULL == (gz_file = gzopen(tmpString, "r")))
	    {
		fprintf(stderr, "No such file %s\n", tmpString);
		exit(-1);
	    }
	}
	
	if (1 == gzdirect(gz_file))
	{
	    gzclose(gz_file);
	    
	    if (NULL == (in = fopen(tmpString, "r")))
	    {
		exit(-1);
	    }
	    file_p = (void*)in;
	    direct = true;
	}
	else
	{
	    direct = false;
	    file_p = (void*)gz_file;
	}
	
	if (direct)
	{
	    FILE* in1 = (FILE*)file_p;
	    char c = fgetc(in1);
	    if (c == '#')
	    {
		fgets(tmpString, 1024, in1);
	    }
	}
	else
	{
	    gzFile gz_file = (gzFile)file_p;
	    char c = gzgetc(gz_file);
	    if (c == '#')
	    {
		gzgets(gz_file, tmpString, 1024);
	    }
	}
	sscanf(tmpString, "%d %d %lf %lf\n", 
	       &x_max, &y_max, &stepx, &stepy);
	if (start == k)
	{
	    gzprintf(out, "# %d %d %lf %lf\n", 
		     x_max, y_max, stepx, stepy);
	}

	if (direct)
	{
	    FILE* in1 = (FILE*)file_p;
	    while (fgets(tmpString, 1024, in1)>0)
	    {
		if (0 != strcmp(tmpString, ""))
		{
		    gzprintf(out, "%s", tmpString); 
		}
	    }
	    fclose(in1);
	}
	else
	{
	    gzFile gz_file = (gzFile)file_p;
	    while(gzgets(gz_file, tmpString, 1024)>0)
	    {
		if (0 != strcmp(tmpString, ""))
		{
		    gzprintf(out, "%s", tmpString);
		}	
	    }
	    gzclose(gz_file);
	}
	if (true == rem)
	{
	    remove(argv[k]);
	}
    }
    gzclose(out);
}
