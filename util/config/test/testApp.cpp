#include "../src/configFile.h"
#include <cstdio>
#include <string.h>

int main()
{
    configEntry<int> *intce = new configEntry<int>("Int parameter",10,"Integer parameter");
    configEntry<int> *intce1 = new configEntry<int>("Int parameter",100,"Integer parameter");
    intce->printEntry(std::cout);
    intce1->printEntry(std::cout);
    configEntry<double> *doublece = new configEntry<double>("Double parameter",0.04,"Double parameter");
    doublece->printEntry(std::cout);
    std::cout<<doublece->getValue()<<std::endl;
    double value = 0.02;
    doublece->setValue((void*)&value);
    std::cout<<*(double*)doublece->getValue()<<std::endl;
    configEntry<string> *stringce = new configEntry<string>("String parameter","string","String parameter");
    stringce->printEntry(std::cout);
    
    configTable* ct_p = new configTable("Test");
    ct_p->addEntry(intce);
    ct_p->printTable("console");
    ct_p->addEntry(doublece);
    ct_p->printTable("console");
    ct_p->addEntry(stringce);
    ct_p->printTable("console");
    ct_p->setIntValue("Int parameter", 10000);
    ct_p->printTable("console"); 
    ct_p->setDoubleValue("Double parameter", 10.0);
    ct_p->printTable("console");
    ct_p->setStringValue("String parameter", "New string");
    ct_p->printTable("console");
    ct_p->addEntry(intce1);
    ct_p->printTable("console"); 
    ct_p->setDoubleValue("Double parameter", 0.012);
    ct_p->printTable("out.dat");
    ct_p->fillFromFile("out.dat");
    ct_p->printTable("console");
    std::cout << ct_p->getIntValue("Int parameter") << std::endl;
    std::cout << ct_p->getIntValue("Intoget parameter") << std::endl;
    std::cout << ct_p->getDoubleValue("Double parameter") << std::endl;
    std::cout << ct_p->getDoubleValue("Intoget parameter") << std::endl;
    std::cout << ct_p->getStringValue("String parameter") << std::endl;
    std::cout << ct_p->getStringValue("Drtcgf parameter") << std::endl;
    std::cout << ct_p->getIntValue("Intoget parameter", 1111) << std::endl;
    std::cout << ct_p->getDoubleValue("Intoget parameter", 0.999) << std::endl;
    std::cout << ct_p->getStringValue("Drtcgf parameter", "Default string") << std::endl;
    delete ct_p;
   
    return 0;
}
