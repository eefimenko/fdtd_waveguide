#include "configFile.h"
#include <string>
#include <iostream>
#include <fstream>
#include <cstdlib>

configTable::configTable(const string& name)
{
    this->name = name;
    entry_p = NULL;
}

configTable::~configTable()
{
    if (entry_p != NULL)
    {
	genConfigEntry* temp = this->entry_p;
	genConfigEntry* temp_prev = this->entry_p;
	while(temp != NULL)
	{
	    temp = temp->next();
	    delete temp_prev;
	    temp_prev = temp;
	}
    }
}

void configTable::addEntry(genConfigEntry* ce)
{
    if (NULL == this->entry_p)
    {
	this->entry_p = ce;
	return;
    }
    else
    {
	genConfigEntry* temp = this->entry_p;
	while (temp->next() != NULL)
	{
	    if(temp->getName().compare(ce->getName()) == 0)
	    {
		temp->setValue(ce->getValue());
		if(temp != ce)
		{
		    delete ce;
		}
		return;
	    }
	    temp = temp->next();
	}
	temp->addEntry(ce);
	return;
    }
}

int configTable::printTable(const string& name)
{
    if (this->entry_p == NULL)
    {
	return -1;
    }
    else
    {
	std::ostream* out;
	std::ofstream* to = NULL;
	if (0 != name.compare("console"))
	{
	    to = new std::ofstream(name.c_str(), std::ios_base::out);
	    out = to;
	}
	else
	{
	    out = &std::cout;
	}
	*out << "# " <<this->name <<" #" << std::endl;

	genConfigEntry* temp = this->entry_p;
	while(temp != NULL)
	{
	    temp->printEntry(*out);
	    temp = temp->next();
	}
	
	if(to != NULL && to->is_open())
	{
	    to->close();
	    delete to;
	}
	return 0;
    }
}

int configTable::getIntValue(const string& name)
{
    if (this->entry_p == NULL)
    {
	return -1;
    }
    else
    {
	genConfigEntry* temp = this->entry_p;
	while(temp != NULL)
	{
	    if (temp->getName().compare(name) == 0)
	    {
		return *(int*)temp->getValue();
	    }
	    temp = temp->next();
	}
	return -1;
    }
}

int configTable::getIntValue(const string& name, int def)
{
    if (this->entry_p == NULL)
    {
	return def;
    }
    else
    {
	genConfigEntry* temp = this->entry_p;
	while(temp != NULL)
	{
	    if (temp->getName().compare(name) == 0)
	    {
		return *(int*)temp->getValue();
	    }
	    temp = temp->next();
	}
	return def;
    }
}

void configTable::setIntValue(const string& name, int value)
{
    if (this->entry_p == NULL)
    {
	return;
    }
    else
    {
	genConfigEntry* temp = this->entry_p;
	while(temp != NULL)
	{
	    if (temp->getName().compare(name) == 0)
	    {
		temp->setValue((void*)&value);
	    }
	    temp = temp->next();
	}
	return;
    }
}

double configTable::getDoubleValue(const string& name)
{
    if (this->entry_p == NULL)
    {
	return -1;
    }
    else
    {
	genConfigEntry* temp = this->entry_p;
	while(temp != NULL)
	{
	    if (temp->getName().compare(name) == 0)
	    {
		return *(double*)temp->getValue();
	    }
	    temp = temp->next();
	}
	return -1;
    }
}

double configTable::getDoubleValue(const string& name, double def)
{
    if (this->entry_p == NULL)
    {
	return def;
    }
    else
    {
	genConfigEntry* temp = this->entry_p;
	while(temp != NULL)
	{
	    if (temp->getName().compare(name) == 0)
	    {
		return *(double*)temp->getValue();
	    }
	    temp = temp->next();
	}
	return def;
    }
}

void configTable::setDoubleValue(const string& name, double value)
{
    if (this->entry_p == NULL)
    {
	return;
    }
    else
    {
	genConfigEntry* temp = this->entry_p;
	while(temp != NULL)
	{
	    if (temp->getName().compare(name) == 0)
	    {
		temp->setValue((void*)&value);
	    }
	    temp = temp->next();
	}
	return;
    }
}

string configTable::getStringValue(const string& name)
{
    if (this->entry_p == NULL)
    {
	return "not found";
    }
    else
    {
	genConfigEntry* temp = this->entry_p;
	while(temp != NULL)
	{
	    if (temp->getName().compare(name) == 0)
	    {
		return *(string*)temp->getValue();
	    }
	    temp = temp->next();
	}
	return "not found";
    }
}

string configTable::getStringValue(const string& name, string def)
{
    if (this->entry_p == NULL)
    {
	return def;
    }
    else
    {
	genConfigEntry* temp = this->entry_p;
	while(temp != NULL)
	{
	    if (temp->getName().compare(name) == 0)
	    {
		return *(string*)temp->getValue();
	    }
	    temp = temp->next();
	}
	return def;
    }
}

void configTable::setStringValue(const string& name, string value)
{
    if (this->entry_p == NULL)
    {
	return;
    }
    else
    {
	genConfigEntry* temp = this->entry_p;
	while(temp != NULL)
	{
	    if (temp->getName().compare(name) == 0)
	    {
		temp->setValue((void*)&value);
	    }
	    temp = temp->next();
	}
	return;
    }
}

void configTable::findTextInString(const string& tempString, int begin, int end, int* i_open_p, int* i_close_p)
{
    int open = 0;
    for (int i = begin; i<end; i++)
    {
	if (tempString[i] == '"')
	{
	    if(open == 0)
	    {
		*i_open_p = i;
		open = 1;
	    }
	    else
	    {
		*i_close_p = i;
		break;
	    }
	}
    }
}

int configTable::fillFromFile(const string& name)
{
    std::ifstream from;
    
    from.open(name.c_str(), std::ios::in);
    
    if (!from.is_open())
    {
	return -1;
    }
    
    while(!from.eof())
    {
	string s;
	getline(from, s);
	if(s[0] == '#')
	{
	    continue;
	}
	else if(s[0] == 'i')
	{
	    int i_open = 0, i_close = 0;
	  
	    findTextInString(s, 2, s.length(), &i_open, &i_close);
	    string name = s.substr(i_open+1,i_close - i_open-1);
	   
	    int value;
	    sscanf(&s.data()[i_close+1],"%d",&value);
	  
	    findTextInString(s, i_close+1, s.length(), &i_open, &i_close);
	    string descr = s.substr(i_open+1,i_close - i_open-1);
	 
	    configEntry<int> *ce = new configEntry<int>(name, value, descr);
	    this->addEntry(ce);
	}
	else if(s[0] == 'd')
	{
	   
	    int i_open = 0, i_close = 0;
	   
	    findTextInString(s, 2, s.length(), &i_open, &i_close);
	    string name = s.substr(i_open+1,i_close - i_open-1);
	   
	    double value;
	    sscanf(&s.data()[i_close+1],"%lf",&value);
	 
	    findTextInString(s, i_close+1, s.length(), &i_open, &i_close);
	    string descr = s.substr(i_open+1,i_close - i_open-1);
	  
	    configEntry<double> *ce = new configEntry<double>(name, value, descr);
	    this->addEntry(ce);
	}
	else if(s[0] == 't')
	{
	    int i_open = 0, i_close = 0;
	   
	    findTextInString(s, 2, s.length(), &i_open, &i_close);
	    string name = s.substr(i_open+1,i_close - i_open-1);
	 	    
	    findTextInString(s, i_close+1, s.length(), &i_open, &i_close);
	    string value = s.substr(i_open+1,i_close - i_open-1);

	    findTextInString(s, i_close+1, s.length(), &i_open, &i_close);
	    string descr = s.substr(i_open+1,i_close - i_open-1);
	
	    configEntry<string> *ce = new configEntry<string>(name, value, descr);
	    this->addEntry(ce);	    
	}
    }
    return 0;
}

