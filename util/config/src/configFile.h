#ifndef __CONFIGFILE_H__
#define __CONFIGFILE_H__
#include <string>
#include <iostream>

using std::string;

class genConfigEntry
{
protected:
    string name;
    string description;
    genConfigEntry* nextEntry_p;

public:
    virtual ~genConfigEntry(){}
    void setName(const string& name) {this->name = name;}
    string getName() {return this->name;}
    string getDescription() {return this->description;}
    genConfigEntry* next(){return this->nextEntry_p;}
    void addEntry(genConfigEntry* ce){this->nextEntry_p = ce;}
    virtual void printEntry(std::ostream& out) = 0;
    virtual void setValue(void* value_p) = 0;
    virtual void* getValue() = 0;
};

template<class T> class configEntry : public genConfigEntry
{
private:
    T value;

public:
    configEntry(const string& name, const T& value, const string& descr);
    ~configEntry();
    void printEntry(std::ostream& out);
    void* getValue() {return (void*)&(this->value);}
    void setValue(void* value) {this->value = *((T*)value);}
};

class configTable
{
private:
    string name;
    genConfigEntry* entry_p;
    void findTextInString(const string& tempString, int begin, int end, int* i_open_p, int* i_close_p);

public:
    configTable(const string& name);
    ~configTable();
    genConfigEntry* findEntry(const string& name);
    void addEntry(genConfigEntry* ce);
    int printTable(const string& name);
    int fillFromFile(const string& name);
    int getIntValue(const string& name);
    double getDoubleValue(const string& name);
    string getStringValue(const string& name);
    int getIntValue(const string& name, int def);
    double getDoubleValue(const string& name, double def);
    string getStringValue(const string& name, string def);
    void setIntValue(const string& name, int value);
    void setDoubleValue(const string& name, double value);
    void setStringValue(const string& name, string value);
};

template class configEntry<int>;
template class configEntry<double>;
template class configEntry<string>;

#include "configFile_incl.h"

#endif
