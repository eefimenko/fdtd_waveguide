#include <string>
#include <iostream>
#include <typeinfo>

template<class T> configEntry<T>::configEntry(const string& name, const T& value, const string& descr)
{
    this->name = name;
    this->description = descr;
    this->value = value;
    this->nextEntry_p = NULL;
}

template<class T> configEntry<T>::~configEntry()
{
    this->nextEntry_p = NULL;
}

template<class T> void configEntry<T>::printEntry(std::ostream& out)
{
    if (typeid(T) == typeid(int))
    {
	out<<"i ";
    }
    else if(typeid(T) == typeid(double))
    {
	out<<"d ";
    }
    else if(typeid(T) == typeid(string))
    {
	out<<"t ";
    }

    out <<"\""<< getName() <<"\" ";

    if(typeid(T) == typeid(string))
    {
	out<<"\"";
    }
    out << value;
    if(typeid(T) == typeid(string))
    {
	out<<"\"";
    }
    out << " \"" << this->getDescription()
	<<"\""
	<< std::endl;
}
