#ifdef WIN
#define _CRT_SECURE_NO_WARNINGS
/* FILETIME of Jan 1 1970 00:00:00. */
static const unsigned __int64 epoch = 116444736000000000;

#include <windows.h>
int gettimeofday(struct timeval * tp, struct timezone * tzp)
{
    FILETIME    file_time;
	SYSTEMTIME  system_time;
    ULARGE_INTEGER ularge;
    GetSystemTime(&system_time);
    SystemTimeToFileTime(&system_time, &file_time);
    ularge.LowPart = file_time.dwLowDateTime;
    ularge.HighPart = file_time.dwHighDateTime;
    tp->tv_sec = (long) ((ularge.QuadPart - epoch) / 10000000L);
    tp->tv_usec = (long) (system_time.wMilliseconds * 1000);
    return 0;
}
#else
#include <sys/time.h>
#endif

#include "trace_log.h"
#include <time.h>

int log_init(configTable* ct_p, log_trace* trace_log_p)
{
    string logFile = ct_p->getStringValue("log_file");
    if (0 == logFile.compare("stderr"))
    {
	trace_log_p->file_p = stderr;
	trace_log_p->real_file = 0;
    }
    else if (0 == logFile.compare("stdout"))
    {
	trace_log_p->file_p = stdout;
	trace_log_p->real_file = 0;
    }
    else
    {
	trace_log_p->file_p = fopen(logFile.c_str(), "w");
	if (NULL == trace_log_p->file_p)
	{
	    trace_log_p->file_p = stderr;
	    fprintf(stderr, "Error creating log-file %s," 
		    "all logs are redirected to stderr\n",
		    logFile.c_str());
	}
	else
	{
	    trace_log_p->real_file = 1;
	}
    }
    string tempStr = ct_p->getStringValue("log_level");
    if (0 == tempStr.compare("off"))
    {
	trace_log_p->log_level = LOG_NOTRACE;
    } 
    else if (0 == tempStr.compare("error"))
    {
	trace_log_p->log_level = LOG_ERROR;
    }
    else if (0 == tempStr.compare("warning"))
    {
	trace_log_p->log_level = LOG_WARNING;
    }
    else if (0 == tempStr.compare("info"))
    {
	trace_log_p->log_level = LOG_INFO;
    }
    else
    {
	trace_log_p->log_level = LOG_ERROR;
	fprintf(trace_log_p->file_p, "Wrong log level: %s. Switched to ERROR\n", tempStr.c_str());
    }
    return trace_log_p->log_level;
}

int log_destroy(log_trace* trace_log_p)
{
    if(trace_log_p->real_file > 0)
    {
	fclose(trace_log_p->file_p);
    }
    return 0;
}

int log_print_text(int level, 
		   log_trace* trace_log_p, 
		   const char* text)
{
    if (trace_log_p->log_level >= level)
    {
	log_print_text_def(level, 
			   trace_log_p->file_p, 
			   text);
    }
    return 0;
}

int log_print_text_def(int level, 
		       FILE* out, 
		       const char* text)
{
    if (NULL != out)
    {
 	fprintf(out, "%s: %s", 
		log_debug_text[level], 
		text);
    }
    return 0;
}

int log_print_error(log_trace* trace_log_p,
		    const char* file,
		    int   line,
		    const char* error_text, 
		    int error_code,
		    int error_val1,
		    int error_val2,
		    int error_val3,
		    int error_val4)
{
    if (trace_log_p->log_level >= LOG_ERROR)
    {
	log_print_error_def(trace_log_p->file_p,
			    file,
			    line,
			    error_text, 
			    error_code,
			    error_val1,
			    error_val2,
			    error_val3,
			    error_val4);
    } 
    return 0;
}

int log_print_error_def(FILE* out,
			const char* file,
			int   line,
			const char* error_text, 
			int error_code,
			int error_val1,
			int error_val2,
			int error_val3,
			int error_val4)
{
    char timestamp[20];
    create_timestamp(timestamp);
    if (NULL != out)
    {
	fprintf(out, 
		"%s %s: [%s:%d] %s: %d {p1=%d p2=%d p3=%d p4=%d}\n",
		timestamp,
		log_debug_text[LOG_ERROR],
		file,
		line,
		error_text,
		error_code,
		error_val1,
		error_val2,
		error_val3,
		error_val4);
    }
    return 0;
}

int log_print_warning(log_trace* trace_log_p,
		      const char* file,
		      int line,
		      const char* warning_text, 
		      int warning_val1,
		      int warning_val2,
		      int warning_val3,
		      int warning_val4)
{
    if (trace_log_p->log_level >= LOG_WARNING)
    {
	log_print_warning_def(trace_log_p->file_p,
			      file,
			      line,
			      warning_text, 
			      warning_val1,
			      warning_val2,
			      warning_val3,
			      warning_val4);
    } 
    return 0;
}

int log_print_warning_def(FILE* out,
			  const char* file,
			  int line,
			  const char* warning_text, 
			  int warning_val1,
			  int warning_val2,
			  int warning_val3,
			  int warning_val4)
{
    char timestamp[20];
    create_timestamp(timestamp);
    if (NULL != out)
    {
	fprintf(out, "%s %s: [%s:%d] %s {p1=%d p2=%d p3=%d p4=%d}\n", 
		timestamp,
		log_debug_text[LOG_WARNING],
		file,
		line,
		warning_text,
		warning_val1,
		warning_val2,
		warning_val3,
		warning_val4);
    }
    return 0;
}

int log_print_info(log_trace* trace_log_p,
		   const char* file,
		   int line,
		   const char* info_text, 
		   int info_val1,
		   int info_val2,
		   int info_val3,
		   int info_val4)
{
    if (trace_log_p->log_level >= LOG_INFO)
    {
	log_print_info_def(trace_log_p->file_p,
			   file,
			   line,
			   info_text, 
			   info_val1,
			   info_val2,
			   info_val3,
			   info_val4);
    } 
    return 0;
}

int log_print_info_def(FILE* out,
		       const char* file,
		       int line,
		       const char* info_text, 
		       int info_val1,
		       int info_val2,
		       int info_val3,
		       int info_val4)
{
    char timestamp[128];
    create_timestamp(timestamp);
    if (NULL != out)
    {
	fprintf(out, 
		"%s %s: [%s:%d] %s {p1=%d p2=%d p3=%d p4=%d}\n",
		timestamp,
		log_debug_text[LOG_INFO],
		file,
		line,
		info_text,
		info_val1,
		info_val2,
		info_val3,
		info_val4);
    }
    return 0;
}

int create_timestamp(char* timestamp)
{

    struct timeval detail_time; 
    time_t time1;
    struct tm *Tm;  
    gettimeofday(&detail_time,NULL);  
    time1 = detail_time.tv_sec;
    Tm=localtime(&time1);  

    sprintf(timestamp, "%02d.%02d.%04d %02d:%02d:%02d.%03ld",
	    Tm->tm_mday,  
	    Tm->tm_mon,  
	    Tm->tm_year + 1900, 
	    Tm->tm_hour,  
	    Tm->tm_min,  
	    Tm->tm_sec,
	    detail_time.tv_usec /1000);

    return 0;
}




