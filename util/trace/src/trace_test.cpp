#include "trace_log.h"
#include <sys/time.h>

int main()
{
    configTable* testConfig = new configTable("default");
    log_trace test_trace_log;
    testConfig->fillFromFile("../test/default.cfg");
    /*testConfig->fillFromFile(config);*/
    fprintf(stderr, "Trace level %d\n",
	    log_init(testConfig, &test_trace_log));
    log_print_text(LOG_ERROR, &test_trace_log, "Test error 1");
    log_print_text(LOG_WARNING, &test_trace_log, "Test warning 2");
    log_print_text(LOG_INFO, &test_trace_log, "Test info 3");
    log_error0(&test_trace_log, 
	       "Serious error 0!!!", 
	       1003);
    log_error1(&test_trace_log, 
	       "Serious error 1!!!", 
	       1006, 1);
    log_error2(&test_trace_log, 
	       "Serious error 2!!!", 
	       1003, 1, 2);
    log_error3(&test_trace_log, 
	       "Serious error 3!!!", 
	       1006, 1, 2,3);
    log_error4(&test_trace_log, 
	       "Serious error 4!!!", 
	       1006, 1, 2, 3, 4);
    log_warning0(&test_trace_log, "Simple warning 0");
    log_warning1(&test_trace_log, "Simple warning 1", 1);
    log_warning2(&test_trace_log, "Simple warning 2", 1, 2);
    log_warning3(&test_trace_log, "Simple warning 3", 1, 2, 3);
    log_warning4(&test_trace_log, "Simple warning 4", 1, 2, 3, 4);
    log_info0(&test_trace_log, "Simple info 0");
    log_info1(&test_trace_log, "Simple info 1", 1);
    log_info2(&test_trace_log, "Simple info 2", 1, 2);
    log_info3(&test_trace_log, "Simple info 3", 1, 2, 3);
    log_info4(&test_trace_log, "Simple info 4", 1, 2, 3, 4);
    log_destroy(&test_trace_log);
    log_error_def0("Macro error 0", -1);
    log_error_def1("Macro error 1", -1,1);
    log_error_def2("Macro error 2", -1,1,2);
    log_error_def3("Macro error 3", -1,1,2,3);
    log_error_def4("Macro error 4", -1,1,2,3,4);
    log_warning_def0("Macro warning");
    log_warning_def1("Macro warning",1);
    log_warning_def2("Macro warning",1,2);
    log_warning_def3("Macro warning",1,2,3);
    log_warning_def4("Macro warning",1,2,3,4);
    log_info_def0("Macro info 0");
    log_info_def1("Macro info 1", 1);
    log_info_def2("Macro info 2", 1, 2);
    log_info_def3("Macro info 3", 1, 2, 3);
    log_info_def4("Macro info 4", 1, 2, 3, 4);
    log_text_error_s("Text macro error\n");
    log_text_warning_s("Text macro warning\n");
    log_text_info_s("Text macro info\n");
    delete testConfig;
    return 0;
}
