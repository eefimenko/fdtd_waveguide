#ifndef __TRACE_LOG_H__
#define __TRACE_LOG_H__

#include <string.h>
#include <stdio.h>
#include <configFile.h>

#define log_error0(log,text,code)  \
    log_print_error(log, __FILE__, __LINE__, text, code,0,0,0,0)
#define log_error1(log,text,code,v1) \
    log_print_error(log, __FILE__, __LINE__, text, code,v1,0,0,0)
#define log_error2(log,text,code,v1,v2)	\
    log_print_error(log, __FILE__, __LINE__, text, code,v1,v2,0,0)
#define log_error3(log,text,code,v1,v2,v3) \
    log_print_error(log, __FILE__, __LINE__, text, code,v1,v2,v3,0)
#define log_error4(log,text,code,v1,v2,v3,v4) \
    log_print_error(log, __FILE__, __LINE__, text, code,v1,v2,v3,v4)

#define log_error_def0(text,code)  \
    log_print_error_def(stderr,__FILE__, __LINE__, text, code,0,0,0,0)
#define log_error_def1(text,code,v1) \
    log_print_error_def(stderr,__FILE__, __LINE__, text, code,v1,0,0,0)
#define log_error_def2(text,code,v1,v2)	\
    log_print_error_def(stderr,__FILE__, __LINE__, text, code,v1,v2,0,0)
#define log_error_def3(text,code,v1,v2,v3) \
    log_print_error_def(stderr,__FILE__, __LINE__, text, code,v1,v2,v3,0)
#define log_error_def4(text,code,v1,v2,v3,v4) \
    log_print_error_def(stderr,__FILE__, __LINE__, text, code,v1,v2,v3,v4)

#define log_warning0(log,text) \
    log_print_warning(log,__FILE__, __LINE__,text,0,0,0,0)
#define log_warning1(log,text,v1) \
    log_print_warning(log,__FILE__, __LINE__,text,v1,0,0,0)
#define log_warning2(log,text,v1,v2) \
    log_print_warning(log,__FILE__, __LINE__,text,v1,v2,0,0)
#define log_warning3(log,text,v1,v2,v3)	\
    log_print_warning(log,__FILE__, __LINE__,text,v1,v2,v3,0)
#define log_warning4(log,text,v1,v2,v3,v4) \
    log_print_warning(log,__FILE__, __LINE__,text,v1,v2,v3,v4)

#define log_warning_def0(text) \
    log_print_warning_def(stderr,__FILE__, __LINE__,text,0,0,0,0)
#define log_warning_def1(text,v1) \
    log_print_warning_def(stderr,__FILE__, __LINE__,text,v1,0,0,0)
#define log_warning_def2(text,v1,v2) \
    log_print_warning_def(stderr,__FILE__, __LINE__,text,v1,v2,0,0)
#define log_warning_def3(text,v1,v2,v3) \
    log_print_warning_def(stderr,__FILE__, __LINE__,text,v1,v2,v3,0)
#define log_warning_def4(text,v1,v2,v3,v4) \
    log_print_warning_def(stderr,__FILE__, __LINE__,text,v1,v2,v3,v4)

#define log_info0(log,text) \
    log_print_info(log,__FILE__, __LINE__,text,0,0,0,0)
#define log_info1(log,text,v1) \
    log_print_info(log,__FILE__, __LINE__,text,v1,0,0,0)
#define log_info2(log,text,v1,v2) \
    log_print_info(log,__FILE__, __LINE__,text,v1,v2,0,0)
#define log_info3(log,text,v1,v2,v3) \
    log_print_info(log,__FILE__, __LINE__,text,v1,v2,v3,0)
#define log_info4(log,text,v1,v2,v3,v4) \
    log_print_info(log,__FILE__, __LINE__,text,v1,v2,v3,v4)

#define log_info_def0(text) \
    log_print_info_def(stderr,__FILE__, __LINE__,text,0,0,0,0)
#define log_info_def1(text,v1) \
    log_print_info_def(stderr,__FILE__, __LINE__,text,v1,0,0,0)
#define log_info_def2(text,v1,v2) \
    log_print_info_def(stderr,__FILE__, __LINE__,text,v1,v2,0,0)
#define log_info_def3(text,v1,v2,v3) \
    log_print_info_def(stderr,__FILE__, __LINE__,text,v1,v2,v3,0)
#define log_info_def4(text,v1,v2,v3,v4) \
    log_print_info_def(stderr,__FILE__, __LINE__,text,v1,v2,v3,v4)

#define log_text_error_s(text) \
    log_print_text_def(LOG_ERROR,stderr,text)
#define log_text_warning_s(text) \
    log_print_text_def(LOG_WARNING,stderr,text)
#define log_text_info_s(text) \
    log_print_text_def(LOG_INFO,stderr,text)

#define log_text_error(log,text) \
    log_print_text(LOG_ERROR,log,text)
#define log_text_warning(log,text) \
    log_print_text(LOG_WARNING,log,text)
#define log_text_info(log,text) \
    log_print_text(LOG_INFO,log,text)

typedef enum log_level_type_e {LOG_NOTRACE, 
			       LOG_ERROR, 
			       LOG_WARNING, 
			       LOG_INFO} log_level_type;

const char log_debug_text[4][10] = {"NOTRACE", "ERROR", "WARNING", "INFO"};

typedef struct log_trace_tag
{
    int log_level;
    FILE* file_p;
    int  real_file;
} log_trace;

int log_init(configTable* ct_p, log_trace* trace_log_p);
int log_destroy(log_trace* trace_log_p);
int log_print_text(int level, 
		   log_trace* trace_log_p, 
		   const char* text);
int log_print_text_def(int level, 
		       FILE* out, 
		       const char* text);
int log_print_error(log_trace* trace_log_p,
		    const char* file,
		    int   line, 
		    const char* error_text, 
		    int error_code,
		    int error_val1,
		    int error_val2,
		    int error_val3,
		    int error_val4);
int log_print_warning(log_trace* trace_log_p,
		      const char* file,
		      int line,
		      const char* warning_text, 
		      int warning_val1,
		      int warning_val2,
		      int warning_val3,
		      int warning_val4);
int log_print_info(log_trace* trace_log_p,
		   const char* file,
		   int line, 
		   const char* info_text, 
		   int info_val1,
		   int info_val2,
		   int info_val3,
		   int info_val4);

int create_timestamp(char* timestamp);

int log_print_error_def(FILE* out,
			const char* file,
			int   line,
			const char* error_text, 
			int error_code,
			int error_val1,
			int error_val2,
			int error_val3,
			int error_val4);

int log_print_warning_def(FILE* out,
			  const char* file,
			  int line,
			  const char* warning_text, 
			  int warning_val1,
			  int warning_val2,
			  int warning_val3,
			  int warning_val4);

int log_print_info_def(FILE* out,
		       const char* file,
		       int line,
		       const char* info_text, 
		       int info_val1,
		       int info_val2,
		       int info_val3,
		       int info_val4);
#endif
