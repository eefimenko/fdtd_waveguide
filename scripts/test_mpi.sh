#!/bin/bash
src=../src
bin=../bin
run=../run
gcc_bin=${bin}/wavefdtd3d
mpi_bin=${bin}/wavefdtd3d_mpi
delimiter="-----------------------------------------------------------"
proc_num=$1
config=$2

function check()
{
    if [ $? -eq 0 ]
    then
        echo "$1: PASSED." 
        echo "$1: PASSED." >> test.log
    else
        echo "$1: FAILED." 
        echo "$1: FAILED." >> test.log
    fi
    echo ${delimiter}
}

function create_dir()
{
    if [ -d $1 ]
    then
	echo "directory results exists"
    else
	mkdir $1
    fi
}

# Go to source directory
cd ${src}

#First compile code
echo "Compiling gcc"
make home_gcc
check "Compiling gcc"

echo "Compiling gcc+mpi"
make home_mpi 
check "Compiling gcc+mpi"

# Go to run directory
cd ${run}

#Perform clean-up from previous run if not made
rm out_gcc.log out1_gcc.log gcc.log out_mpi.log out1_mpi.log mpi.log test.log time.log >& /dev/null

#Create results directory to store simulation results
create_dir results

echo "Run serial code: ${gcc_bin} ${config}"
${gcc_bin} ${config} |& tee out_gcc.log
grep "Run time" out_gcc.log >> time.log
check "Run serial code: ${gcc_bin} ${config}"
grep Time out_gcc.log > out1_gcc.log
awk -F"[;:[]" '{print $3}' out1_gcc.log > gcc.log

echo "Run parallel code: mpirun -np ${proc_num} ${mpi_bin} ${config}"
mpirun -np ${proc_num} ${mpi_bin} ${config} |& tee out_mpi.log
grep "Run time" out_mpi.log >> time.log
check "Run parallel code: mpirun -np ${proc_num} ${mpi_bin} ${config}"
grep Time out_mpi.log > out1_mpi.log
awk -F"[;:[]" '{print $3}' out1_mpi.log > mpi.log

#Compare energy files now
diff gcc.log mpi.log
check "Comparing energy files"

# Go to source directory
cd ${src}

#First compile code
echo "Compiling gcc with ndf"
make home_gcc ndf=on
check "Compiling gcc with ndf"

echo "Compiling gcc+mpi with ndf"
make home_mpi ndf=on
check "Compiling gcc+mpi with ndf"

# Go to run directory
cd ${run}
echo "Run serial code with ndf: ${gcc_bin} ${config}"
${gcc_bin} ${config} |& tee out_gcc.log
grep "Run time" out_gcc.log >> time.log
check "Run serial code with ndf: ${gcc_bin} ${config}"
grep Time out_gcc.log > out1_gcc.log
awk -F"[;:[]" '{print $3}' out1_gcc.log > gcc.log

echo "Run parallel code with ndf: mpirun -np ${proc_num} ${mpi_bin} ${config}"
mpirun -np ${proc_num} ${mpi_bin} ${config} |& tee out_mpi.log
grep "Run time" out_mpi.log >> time.log
check "Run parallel code with ndf: mpirun -np ${proc_num} ${mpi_bin} ${config}"
grep Time out_mpi.log > out1_mpi.log
awk -F"[;:[]" '{print $3}' out1_mpi.log > mpi.log

#Compare energy files now
diff gcc.log mpi.log
check "Comparing energy files"

# Go to source directory
cd ${src}

#First compile code
echo "Compiling gcc with fu"
make home_gcc fu=on
check "Compiling gcc with fu"

echo "Compiling gcc+mpi with fu"
make home_mpi fu=on
check "Compiling gcc+mpi with fu"

# Go to run directory
cd ${run}

echo "Run serial code with fu: ${gcc_bin} ${config}"
${gcc_bin} ${config} |& tee out_gcc.log
grep "Run time" out_gcc.log >> time.log
check "Run serial code with fu: ${gcc_bin} ${config}"
grep Time out_gcc.log > out1_gcc.log
awk -F"[;:[]" '{print $3}' out1_gcc.log > gcc.log

echo "Run parallel code with fu: mpirun -np ${proc_num} ${mpi_bin} ${config}"
mpirun -np ${proc_num} ${mpi_bin} ${config} |& tee out_mpi.log
grep "Run time" out_mpi.log >> time.log
check "Run parallel code with fu: mpirun -np ${proc_num} ${mpi_bin} ${config}"
grep Time out_mpi.log > out1_mpi.log
awk -F"[;:[]" '{print $3}' out1_mpi.log > mpi.log

#Compare energy files now
diff gcc.log mpi.log
check "Comparing energy files"

# Go to source directory
cd ${src}

#First compile code
echo "Compiling gcc with fu and ndf"
make home_gcc fu=on ndf=on
check "Compiling gcc with fu and ndf"

echo "Compiling gcc+mpi with fu and ndf"
make home_mpi fu=on ndf=on
check "Compiling gcc+mpi with fu and ndf"

# Go to run directory
cd ${run}

echo "Run serial code with fu and ndf: ${gcc_bin} ${config}"
${gcc_bin} ${config} |& tee out_gcc.log
grep "Run time" out_gcc.log >> time.log
check "Run serial code with fu and ndf: ${gcc_bin} ${config}"
grep Time out_gcc.log > out1_gcc.log
awk -F"[;:[]" '{print $3}' out1_gcc.log > gcc.log

echo "Run parallel code with fu and ndf: mpirun -np ${proc_num} ${mpi_bin} ${config}"
mpirun -np ${proc_num} ${mpi_bin} ${config} |& tee out_mpi.log
grep "Run time" out_mpi.log >> time.log
check "Run parallel code with fu and ndf: mpirun -np ${proc_num} ${mpi_bin} ${config}"
grep Time out_mpi.log > out1_mpi.log
awk -F"[;:[]" '{print $3}' out1_mpi.log > mpi.log

#Compare energy files now
diff gcc.log mpi.log
check "Comparing energy files"

echo "Summary"
cat test.log
cat time.log
