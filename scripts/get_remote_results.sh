#!/bin/bash

#remote host mvs100k
user=evefim
host=mvs100k.jscc.ru
wd_remote=/home2/ipfnn1/evgeny/fdtd/results
wd_remote_bin=/home2/ipfnn1/evgeny/fdtd

#remote host akka
#user=argon
#host=akka.hpc2n.umu.se
#wd_remote=/home/a/argon/pfs/ifdtd/results

ssh_host=${user}@${host}

#working directory
wd=~/work/

#binaries used in script
graph=${wd}/graph/graph
combine=${wd_remote_bin}/combine
calc_ntf=${wd}/fdtd3d_refactor/util/calculate_ntf/calc_ntf_2

#config path
gr_conf=${wd}/graph

#plot function
plotf=get_and_plot

#grpah options
gr_opts=" --ifpath=. --ofpath=./pics --colorbar=Right "

function get_remote()
{
    echo "Combine $2 files on ${host}"
    ssh $ssh_host "cd ${wd_remote}/$1 && $combine -r $2_* $2.dat.gz >& /dev/null"
    echo "Copy ${wd_remote}/$1/$2.dat.gz from ${host}"
    scp $ssh_host:${wd_remote}/$1/$2.dat.gz .

    ssh $ssh_host rm ${wd_remote}/$1/$2.dat.gz
}

function get()
{
     ssh $ssh_host "ls ${wd_remote}/$1/$2_*.dat.gz > /dev/null"
     if [ $? -eq 0 ] ;
     then
	 get_remote $1 $2
     else
	 echo "No files for $2.dat.gz";
     fi
}

function get_and_plot()
{
     ssh $ssh_host "ls ${wd_remote}/$1/$2_*.dat.gz > /dev/null"
     if [ $? -eq 0 ];
     then
	 get_remote $1 $2
         echo "Plotting $2.dat.gz to $2.png"
	 $graph -c $3 --ifname=$2.dat.gz --ofname=$2.png $gr_opts
     else
	 echo "No files for $2.dat.gz";
     fi
}

function plot()
{
    echo "Plotting $2.dat.gz to $2.png"
    $graph -c $3 --ifname=$2.dat.gz --ofname=$2.png $gr_opts
}

function create_dir
{
    if [ ! -d $1 ]; 
    then
	mkdir $1
    else
	echo "$1 already exists"
    fi
}

function copy_file_remote
{
    if [ ! -f $2 ]; 
    then
	scp ${ssh_host}:${wd_remote}/$1/$2 .
    else
	echo "$2 already exists"
    fi
}

function copy_file_local
{
    if [ ! -f $2 ]; 
    then
	cp $1/$2 .
    else
	echo "$2 already exists"
    fi
}

function copy_ntf_files
{
    list=`ssh $ssh_host "ls ${wd_remote}/$1/data/ntf | grep ntf_[0-9] | grep -v __"`
    create_dir ntf 
    for file in $list
    do
	if [ ! -f $file ]; then
	    scp ${ssh_host}:${wd_remote}/$1/data/ntf/$file ./ntf
	else
	    echo "$file already exists"
	fi	
    done
    cd ntf 
    ${calc_ntf}
    cd ..
}

function copy_3d_files
{
    list=`ssh $ssh_host "ls ${wd_remote}/$1/data/3D_020000 | grep absorption"`

    create_dir 3d

    for file in $list
    do
	if [ ! -f $file ]; then
	    scp ${ssh_host}:${wd_remote}/$1/data/3D_020000/$file ./3d
	else
	    echo "$file already exists"
	fi	
    done
}

function copy_pulse_shape
{
    list=`ssh $ssh_host "ls ${wd_remote}/$1/ | grep pulse_"`
 
    echo $list
 
    for file in $list
    do
	if [ ! -f $file ]; then
	    scp ${ssh_host}:${wd_remote}/$1/$file .
	else
	    echo "$file already exists"
	fi	
    done
}

function copy_wave_shape
{
    list=`ssh $ssh_host "ls ${wd_remote}/$1/ | grep wave_"`
  
    for file in $list
    do
	if [ ! -f $file ]; then
	    scp ${ssh_host}:${wd_remote}/$1/$file .
	else
	    echo "$file already exists"
	fi	
    done
}

function copy_point_files
{
    list=`ssh $ssh_host "ls ${wd_remote}/$1/ | grep point_"`

   for file in $list
   do
	if [ ! -f $file ]; then
	    scp ${ssh_host}:${wd_remote}/$1/$file .
	else
	    echo "$file already exists"
	fi	
    done
}

function copy_volume_files
{
    list=`ssh $ssh_host "ls ${wd_remote}/$1/ | grep volume_"`

   for file in $list
   do
	if [ ! -f $file ]; then
	    scp ${ssh_host}:${wd_remote}/$1/$file .
	else
	    echo "$file already exists"
	fi	
    done
}

function combine_pics
{
    if [ $# -eq 5 ];
    then	
	if [ -f pics/$1.png ] && [ -f pics/$2.png ] && [ -f pics/$3.png ]; 
        then
	    convert pics/$1.png  pics/$2.png pics/$3.png $5 pics/$4.png
	else
	    echo "No pics/$1.png, pics/$2.png or pics/$3.png exists"
	fi
    elif [ $# -eq 4 ]; then
	if [ -f pics/$1.png ] && [ -f pics/$2.png ]; 
        then
	    convert pics/$1.png  pics/$2.png $4 pics/$3.png
	else
	    echo "No pics/$1.png or pics/$2.png exists"
	fi
    else
	echo "Wrong number of arguments"
    fi
}

declare -i result
declare -i delta
declare -i dch

dir=$1
i=$2
i_m=$3
x=$4
y=$5
z=$6
delta=$7
channel=0

if [ $# -eq 8 ]
then 
    channel=1
    nmax=$8
fi

result=$i

create_dir pics
copy_file_local $gr_conf default.cnf

copy_file_remote $dir config.log
copy_file_remote $dir absorption.dat
copy_file_remote $dir n.dat
copy_file_remote $dir energy.dat
copy_volume_files $dir
copy_point_files $dir
copy_pulse_shape $dir
copy_wave_shape $dir
copy_ntf_files $dir
#copy_3d_files $dir

$plotf $dir/data/000000 density_x"$x"_000000 ${gr_conf}/density_x.cnf
$plotf $dir/data/000000 density_y"$y"_000000 ${gr_conf}/density_y.cnf
$plotf $dir/data/000000 density_z"$z"_000000 ${gr_conf}/density_z.cnf

$plotf $dir/data/000000 epsilon_x"$x"_000000 ${gr_conf}/density_x.cnf
$plotf $dir/data/000000 epsilon_y"$y"_000000 ${gr_conf}/density_y.cnf
$plotf $dir/data/000000 epsilon_z"$z"_000000 ${gr_conf}/density_z.cnf

while [ $result -le $i_m ]
do
    
    if [ $result -gt 99999 ]; then
	str=$result
    elif [ $result -gt 9999 ]; then
	str=0$result
    elif [ $result -gt 999 ]; then
	str=00$result
    elif [ $result -gt 99 ]; then
	str=000$result
    elif [ $result -gt 9 ]; then
	str=0000$result
    else
	str=00000$result
    fi

    $plotf $dir/data/${str} E_x"$x"_$str ${gr_conf}/field_x.cnf
    $plotf $dir/data/${str} E_y"$y"_$str ${gr_conf}/field_y.cnf
    $plotf $dir/data/${str} E_z"$z"_$str ${gr_conf}/field_z.cnf
    combine_pics E_x"$x"_$str E_y"$y"_$str E_z"$z"_$str pe$str +append

    $plotf $dir/data/${str} N_x"$x"_$str ${gr_conf}/density_x.cnf
    $plotf $dir/data/${str} N_y"$y"_$str ${gr_conf}/density_y.cnf
    $plotf $dir/data/${str} N_z"$z"_$str ${gr_conf}/density_z.cnf

    combine_pics N_x"$x"_$str N_y"$y"_$str N_z"$z"_$str pd$str +append
    	
#    $plotf $dir/data/${str} abs_x"$x"_$str ${gr_conf}/density_x.cnf
#    $plotf $dir/data/${str} abs_y"$y"_$str ${gr_conf}/density_y.cnf
#    $plotf $dir/data/${str} abs_z"$z"_$str ${gr_conf}/density_z.cnf

#    combine_pics abs_x"$x"_$str abs_y"$y"_$str abs_z"$z"_$str pa$str +append

#    combine_pics pe$str pd$str pa$str p$str -append
    combine_pics pe$str pd$str p$str -append
    result=`expr $result + $delta`
done

    $plotf $dir/data/ne_max ne_max_x"$x" ${gr_conf}/density_x.cnf
    $plotf $dir/data/ne_max ne_max_y"$y" ${gr_conf}/density_y.cnf
    $plotf $dir/data/ne_max ne_max_z"$z" ${gr_conf}/density_z.cnf

if [ $channel -eq 1 ]
then
    	$plotf $dir/data/moving ne_x"$x" ${gr_conf}/density_x.cnf
    	$plotf $dir/data/moving ne_y"$y" ${gr_conf}/density_y.cnf

        n=1
        while [ $n -le $nmax ]
        do
            $plotf $dir/data/moving ni${n}_x"$x" ${gr_conf}/density_x.cnf
    	    $plotf $dir/data/moving ni${n}_y"$y" ${gr_conf}/density_y.cnf

	    n=`expr $n + 1`
        done
fi

