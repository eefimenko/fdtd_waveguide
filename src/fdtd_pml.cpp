#include "fdtd.h"

void init_pml(configTable* ct_p,
              pml_data* pml_p,
              geometry_data* geometry_p,
              time_scheme* timescheme_p,
              scheme_constants* sch_p)
{
    FP esigma = 0.;
    FP hsigma = 0.;

    FP delta_z = (FP)geometry_p->delta_z;
    FP delta_y = (FP)geometry_p->delta_y;
    FP delta_x = (FP)geometry_p->delta_x;

    FP powpml = (FP)PML_POWER;
    FP delta_t = (FP)timescheme_p->delta_t;

    FP R = (FP)(-0.5*log(PML_DAMPING)*(powpml+1));

    pml_p->pmlx = ct_p->getIntValue("pmlx",8);
    pml_p->pmlz = ct_p->getIntValue("pmlz",8);
    pml_p->pmly = ct_p->getIntValue("pmly",8);

    FP sigmax = R/(delta_x*pml_p->pmlx);
    FP sigmay = R/(delta_y*pml_p->pmly);
    FP sigmaz = R/(delta_z*pml_p->pmlz);

    pml_p->c_ex = FP_MALLOC(pml_p->pmlx);
    pml_p->c_ez = FP_MALLOC(pml_p->pmlz);
    pml_p->c_hx = FP_MALLOC(pml_p->pmlx);
    pml_p->c_hz = FP_MALLOC(pml_p->pmlz);

    pml_p->d_ex = FP_MALLOC(pml_p->pmlx);
    pml_p->d_ez = FP_MALLOC(pml_p->pmlz);
    pml_p->d_hx = FP_MALLOC(pml_p->pmlx);
    pml_p->d_hz = FP_MALLOC(pml_p->pmlz);

    for (int i = 0; i < pml_p->pmlx; i++)
    {
        esigma = sigmax * pow((FP)fabs((pml_p->pmlx - i)/(FP)pml_p->pmlx), powpml);
        hsigma = sigmax * pow((FP)fabs((pml_p->pmlx - i - 0.5)/(FP)pml_p->pmlx), powpml);

        if (0. >= esigma)
        {
            pml_p->c_ex[i] = 1;
            pml_p->d_ex[i] = delta_t/delta_x;
        }
        else
        {
            pml_p->c_ex[i] = exp(-esigma*delta_t);
            pml_p->d_ex[i] = (exp(-esigma*delta_t)-1)/(esigma*delta_x);
        }
        if (0 >= hsigma)
        {
            pml_p->c_hx[i] = 1;
            pml_p->d_hx[i] = delta_t/delta_x;
        }
        else
        {
            pml_p->c_hx[i] = exp(-hsigma*delta_t);
            pml_p->d_hx[i] = (exp(-hsigma*delta_t)-1)/(hsigma*delta_x);
        }
    }

    for (int i = 0; i < pml_p->pmlz; i++)
    {
        esigma = sigmaz * pow((FP)fabs((pml_p->pmlz - i)/(FP)pml_p->pmlz), powpml);
        hsigma = sigmaz * pow((FP)fabs((pml_p->pmlz - i - 0.5)/(FP)pml_p->pmlz), powpml);

        if (0 >= esigma)
        {
            pml_p->c_ez[i] = 1.;
            pml_p->d_ez[i] = delta_t/delta_z;
        }
        else
        {
            pml_p->c_ez[i] = exp(-esigma*delta_t);
            pml_p->d_ez[i] = (exp(-esigma*delta_t)-1)/(esigma*delta_z);
        }
        if (0 >= hsigma)
        {
            pml_p->c_hz[i] = 1.;
            pml_p->d_hz[i] = delta_t/delta_z;
        }
        else
        {
            pml_p->c_hz[i] = exp(-hsigma*delta_t);
            pml_p->d_hz[i] = (exp(-hsigma*delta_t)-1)/(hsigma*delta_z);
        }
    }

    pml_p->ex_l = pml_p->pmlx;
    pml_p->ez_l = pml_p->pmlz;

    pml_p->hx_l = pml_p->pmlx;
    pml_p->hz_l = pml_p->pmlz;

    pml_p->ex_h = geometry_p->x_full - 1 - pml_p->pmlx;
    pml_p->ez_h = geometry_p->z_full - 1 - pml_p->pmlz;

    pml_p->hx_h = geometry_p->x_full - 1 - pml_p->pmlx;
    pml_p->hz_h = geometry_p->z_full - 1 - pml_p->pmlz;
    
    if (pml_p->ex_h > geometry_p->i_offset)
    {
        pml_p->eoffx_h = pml_p->ex_h + 1;
    }
    else
    {
        pml_p->eoffx_h = geometry_p->i_offset + 1;
    }
    pml_p->eoffx_l = 1;

    if (pml_p->ez_h > geometry_p->k_offset)
    {
        pml_p->eoffz_h = pml_p->ez_h + 1;
    }
    else
    {
        pml_p->eoffz_h = geometry_p->k_offset + 1;
    }
    pml_p->eoffz_l = 1;
    
    if (pml_p->hx_h > geometry_p->i_offset)
    {
        pml_p->hoffx_h = pml_p->hx_h + 1;
    }
    else
    {
        pml_p->hoffx_h = geometry_p->i_offset;
    }
    pml_p->hoffx_l = 0;

    if (pml_p->hz_h > geometry_p->k_offset)
    {
        pml_p->hoffz_h = pml_p->hz_h + 1;
    }
    else
    {
        pml_p->hoffz_h = geometry_p->k_offset;
    }
    pml_p->hoffz_l = 0;

    pml_p->epmlx_h = pml_p->pmlx + pml_p->ex_h;
    pml_p->epmlx_l = 0;
    pml_p->epmlz_h = pml_p->pmlz + pml_p->ez_h;
    pml_p->epmlz_l = 0;
    pml_p->hpmlx_h = pml_p->pmlx + pml_p->hx_h;
    pml_p->hpmlx_l = 0;
    pml_p->hpmlz_h = pml_p->pmlz + pml_p->hz_h;
    pml_p->hpmlz_l = 0;

    if (geometry_p->gtype == THREED_e)
    {
        pml_p->c_ey = FP_MALLOC(pml_p->pmly);
        pml_p->c_hy = FP_MALLOC(pml_p->pmly);
        pml_p->d_ey = FP_MALLOC(pml_p->pmly);
        pml_p->d_hy = FP_MALLOC(pml_p->pmly);

        for (int k = 0; k < pml_p->pmly; k++)
        {
            esigma = sigmay * pow((FP)fabs((pml_p->pmly - k)/(FP)pml_p->pmly), powpml);
            hsigma = sigmay * pow((FP)fabs((pml_p->pmly - k - 0.5)/(FP)pml_p->pmly), powpml);

            if (0 >= esigma)
            {
                pml_p->c_ey[k] = 1.;
                pml_p->d_ey[k] = delta_t/delta_y;
            }
            else
            {
                pml_p->c_ey[k] = exp(-esigma*delta_t);
                pml_p->d_ey[k] = (exp(-esigma*delta_t)-1)/(esigma*delta_y);
            }
            if (0 >= hsigma)
            {
                pml_p->c_hy[k] = 1.;
                pml_p->d_hy[k] = delta_t/delta_y;
            }
            else
            {
                pml_p->c_hy[k] = exp(-hsigma*delta_t);
                pml_p->d_hy[k] = (exp(-hsigma*delta_t)-1)/(hsigma*delta_y);
            }
        }

        pml_p->ey_l = pml_p->pmly;
        pml_p->hy_l = pml_p->pmly;
        pml_p->ey_h = geometry_p->y_full - 1 - pml_p->pmly;
        pml_p->hy_h = geometry_p->y_full - 1 - pml_p->pmly;

        if (pml_p->ey_h > geometry_p->j_offset)
        {
            pml_p->eoffy_h = pml_p->ey_h + 1;
        }
        else
        {
            pml_p->eoffy_h = geometry_p->j_offset + 1;
        }
        pml_p->eoffy_l = 1;

        if (pml_p->hy_h > geometry_p->j_offset)
        {
            pml_p->hoffy_h = pml_p->hy_h + 1;
        }
        else
        {
            pml_p->hoffy_h = geometry_p->j_offset;
        }
        pml_p->hoffy_l = 0;

        pml_p->epmly_h = pml_p->pmly + pml_p->ey_h;
        pml_p->epmly_l = 0;

        pml_p->hpmly_h = pml_p->pmly + pml_p->hy_h;
        pml_p->hpmly_l = 0;
	
        calc_pml_area_3d(pml_p, geometry_p);
        init_pml_fields_3d(pml_p, geometry_p, sch_p);
#ifdef CUDA
        init_cuda_pml_3d(pml_p);
#endif
    }
    else
    {
        calc_pml_area_2d(pml_p, geometry_p);
        init_pml_fields_2d(pml_p, geometry_p, sch_p);
#ifdef CUDA
        init_cuda_pml_2d(pml_p);
#endif
    }
    return;
}

void calc_pml_area_2d(pml_data* pml_p,
                      geometry_data* geometry_p)
{
    pml_p->ne = 0;
    pml_p->nh = 0;
    pml_p->nef = 0;
    pml_p->nhf = 0;

    for (int i=1; i<geometry_p->x_max; i++)
    {
        for (int k=1; k<geometry_p->z_max; k++)
        {
            int i1 = i+geometry_p->i_offset;
            int k1 = k+geometry_p->k_offset;

            if(!(k1 >= pml_p->ez_l && k1 <= pml_p->ez_h &&
                    i1 >= pml_p->ex_l && i1 <= pml_p->ex_h))
            {
                pml_p->ne++;
            }
            else
            {
                pml_p->nef++;
            }
        }
    }

    for (int i=0; i<geometry_p->x_max-1; i++)
    {
        for (int k=0; k<geometry_p->z_max-1; k++)
        {
            int i1 = i+geometry_p->i_offset;
            int k1 = k+geometry_p->k_offset;

            if(!(k1 >= pml_p->hz_l && k1 <= pml_p->hz_h &&
                    i1 >= pml_p->hx_l && i1 <= pml_p->hx_h))
            {
                pml_p->nh++;
            }
            else
            {
                pml_p->nhf++;
            }
        }
    }
}

void calc_pml_area_3d(pml_data* pml_p,
                      geometry_data* g_p)
{
    pml_p->nex = 0;
    pml_p->nhx = 0;
    pml_p->nexf = 0;
    pml_p->nhxf = 0;
    
    for (int k=g_p->kmin; k<g_p->kmax + ADD; k++)
    {
        int k1 = k + g_p->k_offset;
        for (int i=g_p->imin-1; i<g_p->imax+1; i++)
        {
            for (int j=g_p->jmin; j<g_p->jmax + ADD; j++)
            {
		int j1 = j+g_p->j_offset;

                if(k1 >= pml_p->ez_l && k1 <= pml_p->ez_h &&
		   j1 >= pml_p->ey_l && j1 <= pml_p->ey_h)
                {
                    pml_p->nexf++;
                }
                else
                {
                    pml_p->nex++;
                }
            }
        }
    }
    
    for (int k=g_p->kmin - ADD; k<g_p->kmax; k++)
    {
        int k1 = k + g_p->k_offset;
        for (int i=g_p->imin-1; i<g_p->imax+1; i++)
        {
            for (int j=g_p->jmin - ADD; j<g_p->jmax; j++)
            {
		int j1 = j+g_p->j_offset;

                if(k1 >= pml_p->hz_l && k1 <= pml_p->hz_h &&
		   j1 >= pml_p->hy_l && j1 <= pml_p->hy_h)
                {
                    pml_p->nhxf++;
                }
                else
                {
                    pml_p->nhx++;
                }
            }
        }
    }
    
    pml_p->ney = 0;
    pml_p->nhy = 0;
    pml_p->neyf = 0;
    pml_p->nhyf = 0;
    for (int k=g_p->kmin; k<g_p->kmax + ADD; k++)
    {
        int k1 = k+g_p->k_offset;
        for (int i=g_p->imin; i<g_p->imax + ADD; i++)
        {
            int i1 = i+g_p->i_offset;
            for (int j=g_p->jmin-1; j<g_p->jmax+1; j++)
            {
                if(k1 >= pml_p->ez_l && k1 <= pml_p->ez_h &&
		   i1 >= pml_p->ex_l && i1 <= pml_p->ex_h)
                {
                    pml_p->neyf++;
                }
                else
                {
                    pml_p->ney++;
                }
            }
        }
    }
    
    for (int k=g_p->kmin - ADD; k<g_p->kmax; k++)
    {
        int k1 = k + g_p->k_offset;
        for (int i=g_p->imin - ADD; i<g_p->imax; i++)
        {
            int i1 = i + g_p->i_offset;
	    for (int j=g_p->jmin-1; j<g_p->jmax+1; j++)
            {   
                if(k1 >= pml_p->hz_l && k1 <= pml_p->hz_h &&
		   i1 >= pml_p->hx_l && i1 <= pml_p->hx_h)
		{
                    pml_p->nhyf++;
                }
                else
                {
                    pml_p->nhy++;
                }
            }
        }
    }
    
    pml_p->nez = 0;
    pml_p->nhz = 0;
    pml_p->nezf = 0;
    pml_p->nhzf = 0;

    for (int k=g_p->kmin-1; k<g_p->kmax+1; k++)
    {
        for (int i=g_p->imin; i<g_p->imax + ADD; i++)
        {
            int i1 = i + g_p->i_offset;
            for (int j=g_p->jmin; j<g_p->jmax + ADD; j++)
            {
		int j1 = j + g_p->j_offset;

                if(i1 >= pml_p->ex_l && i1 <= pml_p->ex_h &&
		   j1 >= pml_p->ey_l && j1 <= pml_p->ey_h)
                {
                    pml_p->nezf++;
                }
                else
                {
                    pml_p->nez++;
                }
            }
        }
    }

    for (int k = g_p->kmin-1; k < g_p->kmax+1; k++)
    {
        for (int i = g_p->imin - ADD; i < g_p->imax; i++)
        {
            int i1 = i + g_p->i_offset;
            for (int j = g_p->jmin - ADD; j<g_p->jmax; j++)
            {
		int j1 = j+g_p->j_offset;

                if(i1 >= pml_p->hx_l && i1 <= pml_p->hx_h &&
		   j1 >= pml_p->hy_l && j1 <= pml_p->hy_h)
                {
                    pml_p->nhzf++;
                }
                else
                {
                    pml_p->nhz++;
                }
            }
        }
    }
}

void clear_pml_z(pml_data* pml_p)
{
#ifdef FULL_UPDATE
    zero_array(pml_p->Exy, pml_p->starte_x, pml_p->nex);
    zero_array(pml_p->Exz, pml_p->starte_x, pml_p->nex);
    zero_array(pml_p->Hxy, pml_p->starth_x, pml_p->nhx);
    zero_array(pml_p->Hxz, pml_p->starth_x, pml_p->nhx);
    zero_array(pml_p->Eyx, pml_p->starte_y, pml_p->ney);
    zero_array(pml_p->Eyz, pml_p->starte_y, pml_p->ney);
    zero_array(pml_p->Hyx, pml_p->starth_y, pml_p->nhy);
    zero_array(pml_p->Hyz, pml_p->starth_y, pml_p->nhy);
#else
    pml_fields* f = &pml_p->pml_f;
   
    zero_field3d(f->Exy_z_h);
    zero_field3d(f->Exz_z_h);
    zero_field3d(f->Eyx_z_h);
    zero_field3d(f->Eyz_z_h);
    zero_field3d(f->Hxy_z_h);
    zero_field3d(f->Hxz_z_h);
    zero_field3d(f->Hyx_z_h);
    zero_field3d(f->Hyz_z_h);
#endif
}
	    
void zero_pml_z(pml_data* pml_p,
                geometry_data* g_p,
                time_scheme* timescheme_p,
		scheme_constants* sch_p)
{
    pml_p->ez_l = 0;
    pml_p->hz_l = 0;
#ifdef FULL_UPDATE
    calc_pml_area_3d(pml_p, g_p);
    reinit_pml_fields_3d(pml_p, g_p, sch_p);
#endif
}

void zero_pml_x(pml_data* pml_p,
                geometry_data* g_p,
                scheme_constants* sch_p)
{
    FP delta_t = 0.005;
    FP delta_x = g_p->delta_x;
    pml_p->pmlx = 0;
    pml_p->ex_l = 0;
    pml_p->hx_l = 0;
    pml_p->ex_h = g_p->x_full - 1;
    pml_p->hx_h = g_p->x_full - 1;

    if (pml_p->ex_h > g_p->i_offset)
    {
        pml_p->eoffx_h = pml_p->ex_h + 1;
    }
    else
    {
        pml_p->eoffx_h = g_p->i_offset + 1;
    }
    pml_p->eoffx_l = 1;

    if (pml_p->hx_h > g_p->i_offset)
    {
        pml_p->hoffx_h = pml_p->hx_h + 1;
    }
    else
    {
        pml_p->hoffx_h = g_p->i_offset;
    }
    pml_p->hoffx_l = 0;

    pml_p->epmlx_h = pml_p->ex_h;
    pml_p->epmlx_l = 0;

    pml_p->hpmlx_h = pml_p->hx_h;
    pml_p->hpmlx_l = 0;

    for (int i = 0; i < pml_p->pmlx; i++)
    {
        pml_p->c_ex[i] = 1.;
        pml_p->d_ex[i] = delta_t/delta_x;
        pml_p->c_hx[i] = 1.;
        pml_p->d_hx[i] = delta_t/delta_x;
    }

    pml_p->ne = pml_p->ne_m;
    pml_p->nh = pml_p->nh_m;

    pml_p->nef = (g_p->x_max-1)*(g_p->z_max-1) - pml_p->ne_m;
    pml_p->nhf = (g_p->x_max-1)*(g_p->z_max-1) - pml_p->nh_m;

    for (int i=0; i < pml_p->ne; i++)
    {
        pml_p->aex[i] = 1;
        pml_p->bex[i] = -sch_p->dt_dx;
    }

    for (int i=0; i < pml_p->nh; i++)
    {
        pml_p->ahx[i] = 1;
        pml_p->bhx[i] = -sch_p->dt_dx;
    }

    free_int(pml_p->idxef);
    free_int(pml_p->idxhf);

    pml_p->idxef = INT_MALLOC(pml_p->nef);
    pml_p->idxhf = INT_MALLOC(pml_p->nhf);

    int idxef =0;
    int idxhf =0;

    for (int i=1; i<g_p->x_max; i++)
    {
        for (int k=1; k<g_p->z_max; k++)
        {
            int k1 = k+g_p->k_offset;

            if (k1 >= pml_p->ez_l && k1 <= pml_p->ez_h)
            {
                pml_p->idxef[idxef++] = IDX2(i,k, g_p->x_max, g_p->z_max);
            }
        }
    }

    for (int i=0; i<g_p->x_max-1; i++)
    {
        for (int k=0; k<g_p->z_max-1; k++)
        {
            int k1 = k+g_p->k_offset;

            if (k1 >= pml_p->hz_l && k1 <= pml_p->hz_h)
            {
                pml_p->idxhf[idxhf++] = IDX2(i,k, g_p->x_max, g_p->z_max);
            }
        }
    }
}

void swap_pml_2d(pml_data* pml_p, geometry_data* g_p, int move)
{
    swap_field(pml_p->pml_f.Eyz_z_l, move);
    swap_field(pml_p->pml_f.Eyx_z_l, move);
    swap_field(pml_p->pml_f.Eyz_z_h, move);
    swap_field(pml_p->pml_f.Eyx_z_h, move);

    int nx = g_p->x_max-1;
    int nz = pml_p->sl_e;

    //fprintf(stderr, "ne=%d %d %d %d\n", pml_p->ne, nz, nx, pml_p->ne%nz);

    memmove(pml_p->Eyx, pml_p->Eyx+move*nz,
            (nx-move)*nz*sizeof(FP));
    memset(pml_p->Eyx+(nx-move)*nz, 0,
           move*nz*sizeof(FP));
    memmove(pml_p->Eyz, pml_p->Eyz+move*nz,
            (nx-move)*nz*sizeof(FP));
    memset(pml_p->Eyz+(nx-move)*nz, 0,
           move*nz*sizeof(FP));
}

int swap_pml_array_3d(FP* a_p, int move, int s_x, int max)
{
    if (a_p != NULL)
    {
	if (max < move)
	    fprintf(stderr, "swap_pml_array_3d: max < move, s_x %d %d %d\n", max, move, s_x);
	memmove(a_p, a_p + move * s_x, (max - move) * s_x * sizeof(FP));
	memset(a_p + (max - move) * s_x, 0, move * s_x * sizeof(FP));
    }
    return 0;
}

void swap_pml_3d(pml_data* pml_p, int move)
{
#ifdef FULL_UPDATE
    swap_pml_array_3d(pml_p->Exy, move, pml_p->se_x, pml_p->xmax);
    swap_pml_array_3d(pml_p->Exz, move, pml_p->se_x, pml_p->xmax);
    swap_pml_array_3d(pml_p->Hxy, move, pml_p->sh_x, pml_p->xmax);
    swap_pml_array_3d(pml_p->Hxz, move, pml_p->sh_x, pml_p->xmax);
    swap_pml_array_3d(pml_p->Eyx, move, pml_p->se_y, pml_p->ymax);
    swap_pml_array_3d(pml_p->Eyz, move, pml_p->se_y, pml_p->ymax);
    swap_pml_array_3d(pml_p->Hyx, move, pml_p->sh_y, pml_p->ymax);
    swap_pml_array_3d(pml_p->Hyz, move, pml_p->sh_y, pml_p->ymax);
    swap_pml_array_3d(pml_p->Ezy, move, pml_p->se_z, pml_p->zmax);
    swap_pml_array_3d(pml_p->Ezx, move, pml_p->se_z, pml_p->zmax);
    swap_pml_array_3d(pml_p->Hzy, move, pml_p->sh_z, pml_p->zmax);
    swap_pml_array_3d(pml_p->Hzx, move, pml_p->sh_z, pml_p->zmax);
#else
    swap_field3d(pml_p->pml_f.Eyx_x_l, move);
    swap_field3d(pml_p->pml_f.Eyz_x_l, move);
    swap_field3d(pml_p->pml_f.Ezx_x_l, move);
    swap_field3d(pml_p->pml_f.Ezy_x_l, move);

    swap_field3d(pml_p->pml_f.Hyx_x_l, move);
    swap_field3d(pml_p->pml_f.Hyz_x_l, move);
    swap_field3d(pml_p->pml_f.Hzx_x_l, move);
    swap_field3d(pml_p->pml_f.Hzy_x_l, move);

    swap_field3d(pml_p->pml_f.Eyx_x_h, move);
    swap_field3d(pml_p->pml_f.Eyz_x_h, move);
    swap_field3d(pml_p->pml_f.Ezx_x_h, move);
    swap_field3d(pml_p->pml_f.Ezy_x_h, move);

    swap_field3d(pml_p->pml_f.Hyx_x_h, move);
    swap_field3d(pml_p->pml_f.Hyz_x_h, move);
    swap_field3d(pml_p->pml_f.Hzx_x_h, move);
    swap_field3d(pml_p->pml_f.Hzy_x_h, move);

    swap_field3d(pml_p->pml_f.Exz_y_l, move);
    swap_field3d(pml_p->pml_f.Exy_y_l, move);
    swap_field3d(pml_p->pml_f.Ezy_y_l, move);
    swap_field3d(pml_p->pml_f.Ezx_y_l, move);

    swap_field3d(pml_p->pml_f.Hxy_y_l, move);
    swap_field3d(pml_p->pml_f.Hxz_y_l, move);
    swap_field3d(pml_p->pml_f.Hzy_y_l, move);
    swap_field3d(pml_p->pml_f.Hzx_y_l, move);

    swap_field3d(pml_p->pml_f.Exz_y_h, move);
    swap_field3d(pml_p->pml_f.Exy_y_h, move);
    swap_field3d(pml_p->pml_f.Ezy_y_h, move);
    swap_field3d(pml_p->pml_f.Ezx_y_h, move);

    swap_field3d(pml_p->pml_f.Hxy_y_h, move);
    swap_field3d(pml_p->pml_f.Hxz_y_h, move);
    swap_field3d(pml_p->pml_f.Hzy_y_h, move);
    swap_field3d(pml_p->pml_f.Hzx_y_h, move);
#endif
}

void init_pml_fields_2d(pml_data* pml_p,
                        geometry_data* g_p,
                        scheme_constants* sch_p)
{
    pml_p->idxe = INT_MALLOC(pml_p->ne);
    pml_p->idxh = INT_MALLOC(pml_p->nh);

    pml_p->idxef = INT_MALLOC(pml_p->nef);
    pml_p->idxhf = INT_MALLOC(pml_p->nhf);

    pml_p->aex = FP_MALLOC(pml_p->ne);
    pml_p->bex = FP_MALLOC(pml_p->ne);

    pml_p->aez = FP_MALLOC(pml_p->ne);
    pml_p->bez = FP_MALLOC(pml_p->ne);

    pml_p->ahx = FP_MALLOC(pml_p->nh);
    pml_p->bhx = FP_MALLOC(pml_p->nh);

    pml_p->ahz = FP_MALLOC(pml_p->nh);
    pml_p->bhz = FP_MALLOC(pml_p->nh);

    pml_p->Eyx = FP_MALLOC(pml_p->ne);
    pml_p->Eyz = FP_MALLOC(pml_p->ne);

    int idxe = 0;
    int idxef =0;
    int idxh = 0;
    int idxhf = 0;
    int pmli,pmlk;

    pml_p->ne_m = 0;
    pml_p->nh_m = 0;

    for (int i=1; i<g_p->x_max; i++)
    {
        for (int k=1; k<g_p->z_max; k++)
        {
            int i1 = i+g_p->i_offset;
            int k1 = k+g_p->k_offset;

            if (k1 < pml_p->ez_l || k1 > pml_p->ez_h)
            {
                if (k1 < pml_p->ez_l)
                {
                    pmlk = k1 - pml_p->epmlz_l;

                    pml_p->aez[idxe] = pml_p->c_ez[pmlk];
                    pml_p->bez[idxe] = -pml_p->d_ez[pmlk];
                }
                else
                {
                    pmlk = pml_p->epmlz_h - k1;

                    pml_p->aez[idxe] = pml_p->c_hz[pmlk];
                    pml_p->bez[idxe] = -pml_p->d_hz[pmlk];
                }

                if (i1 >= pml_p->ex_l && i1 <= pml_p->ex_h)
                {
                    pml_p->aex[idxe] = 1;
                    pml_p->bex[idxe] = -sch_p->dt_dx;
                }
                else if (i1 > pml_p->ex_h)
                {
                    pmli = pml_p->epmlx_h - i1;

                    pml_p->aex[idxe] = pml_p->c_hx[pmli];
                    pml_p->bex[idxe] = pml_p->d_hx[pmli];
                }
                else
                {
                    pmli = i1 - pml_p->epmlx_l;

                    pml_p->aex[idxe] = pml_p->c_ex[pmli];
                    pml_p->bex[idxe] = pml_p->d_ex[pmli];
                }

                pml_p->idxe[idxe++] = IDX2(i,k, g_p->x_max, g_p->z_max);
                pml_p->ne_m++;
            }
        }
    }

    for (int i=1; i<g_p->x_max; i++)
    {
        for (int k=1; k<g_p->z_max; k++)
        {
            int i1 = i+g_p->i_offset;
            int k1 = k+g_p->k_offset;

            if (k1 >= pml_p->ez_l && k1 <= pml_p->ez_h)
            {
                if (i1 >= pml_p->ex_l && i1 <= pml_p->ex_h)
                {
                    pml_p->idxef[idxef++] = IDX2(i,k, g_p->x_max, g_p->z_max);
                }
                else
                {
                    pml_p->aez[idxe] = 1;
                    pml_p->bez[idxe] = sch_p->dt_dz;

                    if (i1 > pml_p->ex_h)
                    {
                        pmli = pml_p->epmlx_h - i1;

                        pml_p->aex[idxe] = pml_p->c_hx[pmli];
                        pml_p->bex[idxe] = pml_p->d_hx[pmli];
                    }
                    else
                    {
                        pmli = i1 - pml_p->epmlx_l;

                        pml_p->aex[idxe] = pml_p->c_ex[pmli];
                        pml_p->bex[idxe] = pml_p->d_ex[pmli];
                    }

                    pml_p->idxe[idxe++] = IDX2(i,k, g_p->x_max, g_p->z_max);
                }
            }
        }
    }

    for (int i=0; i<g_p->x_max-1; i++)
    {
        for (int k=0; k<g_p->z_max-1; k++)
        {
            int i1 = i+g_p->i_offset;
            int k1 = k+g_p->k_offset;

            if (k1 < pml_p->hz_l || k1 > pml_p->hz_h)
            {
                if (k1 < pml_p->hz_l)
                {
                    pmlk = k1 - pml_p->hpmlz_l;

                    pml_p->ahz[idxh] = pml_p->c_hz[pmlk];
                    pml_p->bhz[idxh] = -pml_p->d_hz[pmlk];
                }
                else
                {
                    pmlk = pml_p->hpmlz_h - k1;

                    pml_p->ahz[idxh] = pml_p->c_ez[pmlk];
                    pml_p->bhz[idxh] = -pml_p->d_ez[pmlk];
                }

                if (i1 >= pml_p->hx_l && i1 <= pml_p->hx_h)
                {
                    pml_p->ahx[idxh] = 1;
                    pml_p->bhx[idxh] = -sch_p->dt_dx;
                }
                else if (i1 > pml_p->hx_h)
                {
                    pmli = pml_p->hpmlx_h - i1;

                    pml_p->ahx[idxh] = pml_p->c_ex[pmli];
                    pml_p->bhx[idxh] = pml_p->d_ex[pmli];
                }
                else
                {
                    pmli = i1 - pml_p->hpmlx_l;

                    pml_p->ahx[idxh] = pml_p->c_hx[pmli];
                    pml_p->bhx[idxh] = pml_p->d_hx[pmli];
                }

                pml_p->idxh[idxh++] = IDX2(i,k, g_p->x_max, g_p->z_max);
                pml_p->nh_m++;
            }
        }
    }

    for (int i=0; i<g_p->x_max-1; i++)
    {
        for (int k=0; k<g_p->z_max-1; k++)
        {
            int i1 = i+g_p->i_offset;
            int k1 = k+g_p->k_offset;

            if (k1 >= pml_p->hz_l && k1 <= pml_p->hz_h)
            {
                if (i1 >= pml_p->hx_l && i1 <= pml_p->hx_h)
                {
                    pml_p->idxhf[idxhf++] = IDX2(i,k, g_p->x_max, g_p->z_max);
                }
                else
                {
                    pml_p->ahz[idxh] = 1;
                    pml_p->bhz[idxh] = sch_p->dt_dz;

                    if (i1 > pml_p->hx_h)
                    {
                        pmli = pml_p->hpmlx_h - i1;

                        pml_p->ahx[idxh] = pml_p->c_ex[pmli];
                        pml_p->bhx[idxh] = pml_p->d_ex[pmli];
                    }
                    else
                    {
                        pmli = i1 - pml_p->hpmlx_l;

                        pml_p->ahx[idxh] = pml_p->c_hx[pmli];
                        pml_p->bhx[idxh] = pml_p->d_hx[pmli];
                    }
                    pml_p->idxh[idxh++] = IDX2(i,k, g_p->x_max, g_p->z_max);
                }
            }
        }
    }
    pml_p->sl_e = pml_p->ne_m/(g_p->x_max-1);
    pml_p->sl_h = pml_p->nh_m/(g_p->x_max-1);

    fprintf(stderr, "ne=%d ne_m=%d sl_e=%d nh=%d nh_m=%d sl_h=%d\n",
            pml_p->ne,
            pml_p->ne_m,
            pml_p->sl_e,
            pml_p->nh,
            pml_p->nh_m,
            pml_p->sl_h);

    FIELD_MALLOC(pml_p->pml_f.Eyx_x_l);
    FIELD_MALLOC(pml_p->pml_f.Eyz_x_l);
    FIELD_MALLOC(pml_p->pml_f.Eyx_x_h);
    FIELD_MALLOC(pml_p->pml_f.Eyz_x_h);
    FIELD_MALLOC(pml_p->pml_f.Eyz_z_l);
    FIELD_MALLOC(pml_p->pml_f.Eyx_z_l);
    FIELD_MALLOC(pml_p->pml_f.Eyz_z_h);
    FIELD_MALLOC(pml_p->pml_f.Eyx_z_h);

#ifdef USEMPI
    if (mpi.num_proc_x == 1)
    {
        init_field(pml_p->pml_f.Eyx_x_l, pml_p->pmlx, g_p->z_max);
        init_field(pml_p->pml_f.Eyz_x_l, pml_p->pmlx, g_p->z_max);

        init_field(pml_p->pml_f.Eyx_x_h, pml_p->pmlx, g_p->z_max);
        init_field(pml_p->pml_f.Eyz_x_h, pml_p->pmlx, g_p->z_max);
    }
    else
    {
        int xmin = g_p->i_offset;
        int xmax = g_p->i_offset+g_p->imax;
        int pml_min = pml_p->pmlx;
        int pml_max = g_p->x_full-1-pml_p->pmlx;

        if (xmin < pml_min && xmax > pml_min)
        {
            init_field(pml_p->pml_f.Eyx_x_l, pml_min-xmin-1, g_p->z_max);
            init_field(pml_p->pml_f.Eyz_x_l, pml_min-xmin-1, g_p->z_max);

            init_field(pml_p->pml_f.Eyx_x_h, 0, 0);
            init_field(pml_p->pml_f.Eyz_x_h, 0, 0);
        }
        else if (xmax > pml_max && xmin < pml_max)
        {
            init_field(pml_p->pml_f.Eyx_x_h, xmax-pml_max, g_p->z_max);
            init_field(pml_p->pml_f.Eyz_x_h, xmax-pml_max, g_p->z_max);

            init_field(pml_p->pml_f.Eyx_x_l, 0, 0);
            init_field(pml_p->pml_f.Eyz_x_l, 0, 0);
        }
        else if (xmax < pml_min)
        {
            init_field(pml_p->pml_f.Eyx_x_l, g_p->x_max-2, g_p->z_max);
            init_field(pml_p->pml_f.Eyz_x_l, g_p->x_max-2, g_p->z_max);

            init_field(pml_p->pml_f.Eyx_x_h, 0, 0);
            init_field(pml_p->pml_f.Eyz_x_h, 0, 0);
        }
        else if (xmin > pml_max)
        {
            init_field(pml_p->pml_f.Eyx_x_h, g_p->x_max-2, g_p->z_max);
            init_field(pml_p->pml_f.Eyz_x_h, g_p->x_max-2, g_p->z_max);

            init_field(pml_p->pml_f.Eyx_x_l, 0, 0);
            init_field(pml_p->pml_f.Eyz_x_l, 0, 0);
        }
        else
        {
            /* simply to avoid warnings during compilation */
            init_field(pml_p->pml_f.Eyx_x_h, 0, 0);
            init_field(pml_p->pml_f.Eyz_x_h, 0, 0);

            init_field(pml_p->pml_f.Eyx_x_l, 0, 0);
            init_field(pml_p->pml_f.Eyz_x_l, 0, 0);
        }
    }

    if (mpi.num_proc_z == 1)
    {
        init_field(pml_p->pml_f.Eyz_z_l, g_p->x_max, pml_p->pmlz);
        init_field(pml_p->pml_f.Eyx_z_l, g_p->x_max, pml_p->pmlz);
        init_field(pml_p->pml_f.Eyz_z_h, g_p->x_max, pml_p->pmlz);
        init_field(pml_p->pml_f.Eyx_z_h, g_p->x_max, pml_p->pmlz);
    }
    else
    {
        int zmin = g_p->k_offset;
        int zmax = g_p->k_offset+g_p->kmax;
        int pml_min = pml_p->pmlz;
        int pml_max = g_p->z_full-1-pml_p->pmlz;

        if (zmin < pml_min && zmax > pml_min)
        {
            init_field(pml_p->pml_f.Eyz_z_l, g_p->x_max, pml_min-zmin-1);
            init_field(pml_p->pml_f.Eyx_z_l, g_p->x_max, pml_min-zmin-1);
            init_field(pml_p->pml_f.Eyz_z_h, 0, 0);
            init_field(pml_p->pml_f.Eyx_z_h, 0, 0);
        }
        else if (zmax > pml_max && zmin < pml_max)
        {
            init_field(pml_p->pml_f.Eyz_z_h, g_p->x_max, zmax-pml_max);
            init_field(pml_p->pml_f.Eyx_z_h, g_p->x_max, zmax-pml_max);
            init_field(pml_p->pml_f.Eyz_z_l, 0, 0);
            init_field(pml_p->pml_f.Eyx_z_l, 0, 0);
        }
        else if (zmax < pml_min)
        {
            init_field(pml_p->pml_f.Eyz_z_l, g_p->x_max, g_p->z_max-2);
            init_field(pml_p->pml_f.Eyx_z_l, g_p->x_max, g_p->z_max-2);
            init_field(pml_p->pml_f.Eyz_z_h, 0, 0);
            init_field(pml_p->pml_f.Eyx_z_h, 0, 0);
        }
        else if (zmin > pml_max)
        {
            init_field(pml_p->pml_f.Eyz_z_h, g_p->x_max, g_p->z_max-2);
            init_field(pml_p->pml_f.Eyx_z_h, g_p->x_max, g_p->z_max-2);
            init_field(pml_p->pml_f.Eyz_z_l, 0, 0);
            init_field(pml_p->pml_f.Eyx_z_l, 0, 0);
        }
        else
        {
            /* simply to avoid warnings during compilation */
            init_field(pml_p->pml_f.Eyz_z_l, 0, 0);
            init_field(pml_p->pml_f.Eyx_z_l, 0, 0);
            init_field(pml_p->pml_f.Eyz_z_h, 0, 0);
            init_field(pml_p->pml_f.Eyx_z_h, 0, 0);
        }
    }
#else /* USEMPI */
    init_field(pml_p->pml_f.Eyx_x_l, pml_p->pmlx, g_p->z_max);
    init_field(pml_p->pml_f.Eyz_x_l, pml_p->pmlx, g_p->z_max);
    init_field(pml_p->pml_f.Eyx_x_h, pml_p->pmlx, g_p->z_max);
    init_field(pml_p->pml_f.Eyz_x_h, pml_p->pmlx, g_p->z_max);
    init_field(pml_p->pml_f.Eyz_z_l, g_p->x_max, pml_p->pmlz);
    init_field(pml_p->pml_f.Eyx_z_l, g_p->x_max, pml_p->pmlz);
    init_field(pml_p->pml_f.Eyz_z_h, g_p->x_max, pml_p->pmlz);
    init_field(pml_p->pml_f.Eyx_z_h, g_p->x_max, pml_p->pmlz);
#endif /* USEMPI */
}

void reinit_pml_fields_3d(pml_data* pml_p,
			  geometry_data* g_p,
			  scheme_constants* sch_p)
{
#ifdef FULL_UPDATE
    free_int(pml_p->idex);
    free_int(pml_p->idhx);
    free_int(pml_p->idexf);
    free_int(pml_p->idhxf);
    free_int(pml_p->idey);
    free_int(pml_p->idhy);
    free_int(pml_p->ideyf);
    free_int(pml_p->idhyf);
    free_int(pml_p->idez);
    free_int(pml_p->idhz);
    free_int(pml_p->idezf);
    free_int(pml_p->idhzf);

    free_fp(pml_p->aexy);
    free_fp(pml_p->bexy);
    free_fp(pml_p->aexz);
    free_fp(pml_p->bexz);

    free_fp(pml_p->aeyx);
    free_fp(pml_p->beyx);
    free_fp(pml_p->aeyz);
    free_fp(pml_p->beyz);

    free_fp(pml_p->aezx);
    free_fp(pml_p->bezx);
    free_fp(pml_p->aezy);
    free_fp(pml_p->bezy);

    free_fp(pml_p->ahxy);
    free_fp(pml_p->bhxy);
    free_fp(pml_p->ahxz);
    free_fp(pml_p->bhxz);

    free_fp(pml_p->ahyx);
    free_fp(pml_p->bhyx);
    free_fp(pml_p->ahyz);
    free_fp(pml_p->bhyz);

    free_fp(pml_p->ahzx);
    free_fp(pml_p->bhzx);
    free_fp(pml_p->ahzy);
    free_fp(pml_p->bhzy);
    
    pml_p->idex = INT_MALLOC(pml_p->nex);
    pml_p->idhx = INT_MALLOC(pml_p->nhx);

    pml_p->idexf = INT_MALLOC(pml_p->nexf);
    pml_p->idhxf = INT_MALLOC(pml_p->nhxf);

    pml_p->idey = INT_MALLOC(pml_p->ney);
    pml_p->idhy = INT_MALLOC(pml_p->nhy);

    pml_p->ideyf = INT_MALLOC(pml_p->neyf);
    pml_p->idhyf = INT_MALLOC(pml_p->nhyf);

    pml_p->idez = INT_MALLOC(pml_p->nez);
    pml_p->idhz = INT_MALLOC(pml_p->nhz);

    pml_p->idezf = INT_MALLOC(pml_p->nezf);
    pml_p->idhzf = INT_MALLOC(pml_p->nhzf);

    pml_p->aexy = FP_MALLOC(pml_p->nex);
    pml_p->bexy = FP_MALLOC(pml_p->nex);
    pml_p->aexz = FP_MALLOC(pml_p->nex);
    pml_p->bexz = FP_MALLOC(pml_p->nex);

    pml_p->aeyx = FP_MALLOC(pml_p->ney);
    pml_p->beyx = FP_MALLOC(pml_p->ney);
    pml_p->aeyz = FP_MALLOC(pml_p->ney);
    pml_p->beyz = FP_MALLOC(pml_p->ney);

    pml_p->aezx = FP_MALLOC(pml_p->nez);
    pml_p->bezx = FP_MALLOC(pml_p->nez);
    pml_p->aezy = FP_MALLOC(pml_p->nez);
    pml_p->bezy = FP_MALLOC(pml_p->nez);

    pml_p->ahxy = FP_MALLOC(pml_p->nhx);
    pml_p->bhxy = FP_MALLOC(pml_p->nhx);
    pml_p->ahxz = FP_MALLOC(pml_p->nhx);
    pml_p->bhxz = FP_MALLOC(pml_p->nhx);

    pml_p->ahyx = FP_MALLOC(pml_p->nhy);
    pml_p->bhyx = FP_MALLOC(pml_p->nhy);
    pml_p->ahyz = FP_MALLOC(pml_p->nhy);
    pml_p->bhyz = FP_MALLOC(pml_p->nhy);

    pml_p->ahzx = FP_MALLOC(pml_p->nhz);
    pml_p->bhzx = FP_MALLOC(pml_p->nhz);
    pml_p->ahzy = FP_MALLOC(pml_p->nhz);
    pml_p->bhzy = FP_MALLOC(pml_p->nhz);
    
    FP* Exy_old = pml_p->Exy;
    FP* Exz_old = pml_p->Exz;
    FP* Eyx_old = pml_p->Eyx;
    FP* Eyz_old = pml_p->Eyz;
    FP* Hxy_old = pml_p->Hxy;
    FP* Hxz_old = pml_p->Hxz;
    FP* Hyx_old = pml_p->Hyx;
    FP* Hyz_old = pml_p->Hyz;

    pml_p->Exy = FP_MALLOC(pml_p->nex);
    pml_p->Exz = FP_MALLOC(pml_p->nex);

    pml_p->Eyx = FP_MALLOC(pml_p->ney);
    pml_p->Eyz = FP_MALLOC(pml_p->ney);

    pml_p->Hxy = FP_MALLOC(pml_p->nhx);
    pml_p->Hxz = FP_MALLOC(pml_p->nhx);

    pml_p->Hyx = FP_MALLOC(pml_p->nhy);
    pml_p->Hyz = FP_MALLOC(pml_p->nhy);

    if (pml_p->starte_x > 0)
    {
	memcpy(pml_p->Exy, Exy_old, pml_p->starte_x*sizeof(FP));
	memcpy(pml_p->Exz, Exz_old, pml_p->starte_x*sizeof(FP));
    }
    memcpy(pml_p->Exy+pml_p->starte_x, Exy_old+pml_p->ende_x, (pml_p->nex-pml_p->starte_x)*sizeof(FP));
    memcpy(pml_p->Exz+pml_p->starte_x, Exz_old+pml_p->ende_x, (pml_p->nex-pml_p->starte_x)*sizeof(FP));
    
    if (pml_p->starte_y > 0)
    {
	memcpy(pml_p->Eyx, Eyx_old, pml_p->starte_y*sizeof(FP));
	memcpy(pml_p->Eyz, Eyz_old, pml_p->starte_y*sizeof(FP));
    }
    memcpy(pml_p->Eyx+pml_p->starte_y, Eyx_old+pml_p->ende_y, (pml_p->ney-pml_p->starte_y)*sizeof(FP));
    memcpy(pml_p->Eyz+pml_p->starte_y, Eyz_old+pml_p->ende_y, (pml_p->ney-pml_p->starte_y)*sizeof(FP));

    if (pml_p->starth_x > 0)
    {
	memcpy(pml_p->Hxy, Hxy_old, pml_p->starth_x*sizeof(FP));
	memcpy(pml_p->Hxz, Hxz_old, pml_p->starth_x*sizeof(FP));
    }
    memcpy(pml_p->Hxy+pml_p->starth_x, Hxy_old+pml_p->endh_x, (pml_p->nhx-pml_p->starth_x)*sizeof(FP));
    memcpy(pml_p->Hxz+pml_p->starth_x, Hxz_old+pml_p->endh_x, (pml_p->nhx-pml_p->starth_x)*sizeof(FP));
    
    if (pml_p->starth_y > 0)
    {
	memcpy(pml_p->Hyx, Hyx_old, pml_p->starth_y*sizeof(FP));
	memcpy(pml_p->Hyz, Hyz_old, pml_p->starth_y*sizeof(FP));
    }
    memcpy(pml_p->Hyx+pml_p->starth_y, Hyx_old+pml_p->endh_y, (pml_p->nhy-pml_p->starth_y)*sizeof(FP));
    memcpy(pml_p->Hyz+pml_p->starth_y, Hyz_old+pml_p->endh_y, (pml_p->nhy-pml_p->starth_y)*sizeof(FP));
       
    free_fp(Exy_old);
    free_fp(Exz_old);
    free_fp(Eyx_old);
    free_fp(Eyz_old);
    free_fp(Hxy_old);
    free_fp(Hxz_old);
    free_fp(Hyx_old);
    free_fp(Hyz_old);
    
    int pmli, pmlj, pmlk;

    int idex = 0;
    int idhx = 0;
    int idexf = 0;
    int idhxf = 0;
    
    pml_p->se_x = 0;
    pml_p->sh_x = 0;
    pml_p->starte_x = 0;
    pml_p->starth_x = 0;
    pml_p->ne1_m = 0;
    pml_p->ne2_m = 0;
    pml_p->ne_m = 0;
    
    for (int k=g_p->kmin; k<g_p->kmax + ADD; k++)
    {
        int k1 = k + g_p->k_offset;
        for (int i=g_p->imin-1; i<g_p->imax+1; i++)
        {
            for (int j=g_p->jmin; j<g_p->jmax + ADD; j++)
            {
		int j1 = j+g_p->j_offset;

                if(k1 >= pml_p->ez_l && k1 <= pml_p->ez_h &&
		   j1 >= pml_p->ey_l && j1 <= pml_p->ey_h)
                {
		    pml_p->idexf[idexf++] = IDX3(i,j,k, g_p->x_max, g_p->y_max, g_p->z_max);
                }
                else
                {
		    if (!(j1 >= pml_p->ey_l && j1 <= pml_p->ey_h))
                    {
			if (j1 > pml_p->ey_h)
			{
			    pmlj = pml_p->epmly_h - j1;
			
			    pml_p->aexy[idex] = pml_p->c_hy[pmlj];
			    pml_p->bexy[idex] = -pml_p->d_hy[pmlj];
			}
			else
			{
			    pmlj = j1 - pml_p->epmly_l;

			    pml_p->aexy[idex] = pml_p->c_ey[pmlj];
			    pml_p->bexy[idex] = -pml_p->d_ey[pmlj];
			}

			if (k1 >= pml_p->ez_l && k1 <= pml_p->ez_h)
			{
			    pml_p->aexz[idex] = 1;
			    pml_p->bexz[idex] = -sch_p->dt_dz;
			}
			else if (k1 > pml_p->ez_h)
			{
			    pmlk = pml_p->epmlz_h - k1;

			    pml_p->aexz[idex] = pml_p->c_hz[pmlk];
			    pml_p->bexz[idex] = pml_p->d_hz[pmlk];
			}
			else
			{
			    pmlk = k1 - pml_p->epmlz_l;

			    pml_p->aexz[idex] = pml_p->c_ez[pmlk];
			    pml_p->bexz[idex] = pml_p->d_ez[pmlk];
			}
		    
			pml_p->idex[idex++] = IDX3(i,j,k, g_p->x_max, g_p->y_max, g_p->z_max);
			pml_p->ne_m++;
			pml_p->starte_x++;
			
			if (k == g_p->kmin)
			{
			    pml_p->se_x++;
			}
		    }
                }
            }
        }
    }
    
    pml_p->ende_x = pml_p->starte_x;
    
    for (int k=g_p->kmin; k<g_p->kmax + ADD; k++)
    {
        int k1 = k + g_p->k_offset;
        for (int i=g_p->imin-1; i<g_p->imax+1; i++)
        {
            for (int j=g_p->jmin; j<g_p->jmax + ADD; j++)
            {
		int j1 = j+g_p->j_offset;

                if(!(k1 >= pml_p->ez_l && k1 <= pml_p->ez_h &&
		     j1 >= pml_p->ey_l && j1 <= pml_p->ey_h))
                {
		    if (j1 >= pml_p->ey_l && j1 <= pml_p->ey_h)
                    {
			pml_p->aexy[idex] = 1;
                        pml_p->bexy[idex] = sch_p->dt_dy;
			
			if (k1 > pml_p->ez_h)
			{
			    pmlk = pml_p->epmlz_h - k1;
			    
			    pml_p->aexz[idex] = pml_p->c_hz[pmlk];
			    pml_p->bexz[idex] = pml_p->d_hz[pmlk];
			    pml_p->ne1_m++;
			}
			else
			{
			    pmlk = k1 - pml_p->epmlz_l;
			    
			    pml_p->aexz[idex] = pml_p->c_ez[pmlk];
			    pml_p->bexz[idex] = pml_p->d_ez[pmlk];
			    pml_p->ende_x++;
			    pml_p->ne2_m++;
			}
		    
			pml_p->idex[idex++] = IDX3(i,j,k, g_p->x_max, g_p->y_max, g_p->z_max);
			//pml_p->ne_m++;
		    }
                }
            }
        }
    }

    for (int k=g_p->kmin - ADD; k<g_p->kmax; k++)
    {
        int k1 = k + g_p->k_offset;
        for (int i=g_p->imin-1; i<g_p->imax+1; i++)
        {
            for (int j=g_p->jmin - ADD; j<g_p->jmax; j++)
            {
		int j1 = j+g_p->j_offset;

		if(k1 >= pml_p->hz_l && k1 <= pml_p->hz_h &&
		   j1 >= pml_p->hy_l && j1 <= pml_p->hy_h)
                {
		    pml_p->idhxf[idhxf++] = IDX3(i,j,k, g_p->x_max, g_p->y_max, g_p->z_max);
                }
                else
                {
		    if (!(j1 >= pml_p->hy_l && j1 <= pml_p->hy_h))
                    {
			if (j1 > pml_p->hy_h)
			{
			    pmlj = pml_p->hpmly_h - j1;

			    pml_p->ahxy[idhx] = pml_p->c_ey[pmlj];
			    pml_p->bhxy[idhx] = pml_p->d_ey[pmlj];
			}
			else
			{
			    pmlj = j1 - pml_p->hpmly_l;

			    pml_p->ahxy[idhx] = pml_p->c_hy[pmlj];
			    pml_p->bhxy[idhx] = pml_p->d_hy[pmlj];
			}

			if (k1 >= pml_p->hz_l && k1 <= pml_p->hz_h)
			{
			    pml_p->ahxz[idhx] = 1;
			    pml_p->bhxz[idhx] = sch_p->dt_dz;
			}
			else if (k1 > pml_p->hz_h)
			{
			    pmlk = pml_p->hpmlz_h - k1;

			    pml_p->ahxz[idhx] = pml_p->c_ez[pmlk];
			    pml_p->bhxz[idhx] = -pml_p->d_ez[pmlk];
			}
			else
			{
			    pmlk = k1 - pml_p->hpmlz_l;

			    pml_p->ahxz[idhx] = pml_p->c_hz[pmlk];
			    pml_p->bhxz[idhx] = -pml_p->d_hz[pmlk];
			}
		    
			pml_p->idhx[idhx++] = IDX3(i,j,k, g_p->x_max, g_p->y_max, g_p->z_max);
			pml_p->nh_m++;
			pml_p->starth_x++;
			
			if (k == g_p->kmin)
			{
			    pml_p->sh_x++;
			}
		    }
                }
            }
        }
    }
    
    pml_p->endh_x = pml_p->starth_x;
    
    for (int k=g_p->kmin - ADD; k<g_p->kmax; k++)
    {
        int k1 = k + g_p->k_offset;
        for (int i=g_p->imin-1; i<g_p->imax+1; i++)
        {
            for (int j=g_p->jmin - ADD; j<g_p->jmax; j++)
            {
		int j1 = j+g_p->j_offset;

		if(!(k1 >= pml_p->hz_l && k1 <= pml_p->hz_h &&
		     j1 >= pml_p->hy_l && j1 <= pml_p->hy_h))
		{
		    if (j1 >= pml_p->hy_l && j1 <= pml_p->hy_h)
                    {
                        pml_p->ahxy[idhx] = 1;
                        pml_p->bhxy[idhx] = -sch_p->dt_dy;
		    

			if (k1 > pml_p->hz_h)
			{
			    pmlk = pml_p->hpmlz_h - k1;

			    pml_p->ahxz[idhx] = pml_p->c_ez[pmlk];
			    pml_p->bhxz[idhx] = -pml_p->d_ez[pmlk];
			}
			else
			{
			    pmlk = k1 - pml_p->hpmlz_l;

			    pml_p->ahxz[idhx] = pml_p->c_hz[pmlk];
			    pml_p->bhxz[idhx] = -pml_p->d_hz[pmlk];
			    pml_p->endh_x++;
			}
		    
			pml_p->idhx[idhx++] = IDX3(i,j,k, g_p->x_max, g_p->y_max, g_p->z_max);
			pml_p->nh_m++;
		    }
                }
            }
        }
    }
    
    int idey = 0;
    int idhy = 0;
    int ideyf = 0;
    int idhyf = 0;
    
    pml_p->se_y = 0;
    pml_p->sh_y = 0;
    pml_p->starte_y = 0;
    pml_p->starth_y = 0;
    
    for (int k=g_p->kmin; k<g_p->kmax + ADD; k++)
    {
        int k1 = k+g_p->k_offset;
        for (int i=g_p->imin; i<g_p->imax + ADD; i++)
        {
            int i1 = i+g_p->i_offset;
            for (int j=g_p->jmin-1; j<g_p->jmax+1; j++)
            {
                if(k1 >= pml_p->ez_l && k1 <= pml_p->ez_h &&
		   i1 >= pml_p->ex_l && i1 <= pml_p->ex_h)
                {
		    pml_p->ideyf[ideyf++] = IDX3(i,j,k, g_p->x_max, g_p->y_max, g_p->z_max);
		}
                else
                {
		    if (!(i1 >= pml_p->ex_l && i1 <= pml_p->ex_h))
                    {
			if (i1 > pml_p->ex_h)
			{
			    pmli = pml_p->epmlx_h - i1;
			    
			    pml_p->aeyx[idey] = pml_p->c_hx[pmli];
			    pml_p->beyx[idey] = pml_p->d_hx[pmli];
			}
			else
			{
			    pmli = i1 - pml_p->epmlx_l;
			    
			    pml_p->aeyx[idey] = pml_p->c_ex[pmli];
			    pml_p->beyx[idey] = pml_p->d_ex[pmli];
			}

			if (k1 >= pml_p->ez_l && k1 <= pml_p->ez_h)
			{
			    pml_p->aeyz[idey] = 1;
			    pml_p->beyz[idey] = sch_p->dt_dz;
			}
			else if (k1 > pml_p->ez_h)
			{
			    pmlk = pml_p->epmlz_h - k1;

			    pml_p->aeyz[idey] = pml_p->c_hz[pmlk];
			    pml_p->beyz[idey] = -pml_p->d_hz[pmlk];
			}
			else
			{
			    pmlk = k1 - pml_p->epmlz_l;

			    pml_p->aeyz[idey] = pml_p->c_ez[pmlk];
			    pml_p->beyz[idey] = -pml_p->d_ez[pmlk];
			}
		    
			pml_p->idey[idey++] = IDX3(i,j,k, g_p->x_max, g_p->y_max, g_p->z_max);
			//pml_p->ne_m++;
			pml_p->starte_y++;
			if (k == g_p->kmin)
			{
			    pml_p->se_y++;
			}
		    }
                }
            }
        }
    }
    pml_p->ende_y =  pml_p->starte_y;
    
    for (int k=g_p->kmin; k<g_p->kmax + ADD; k++)
    {
        int k1 = k+g_p->k_offset;
        for (int i=g_p->imin; i<g_p->imax + ADD; i++)
        {
            int i1 = i+g_p->i_offset;
            for (int j=g_p->jmin-1; j<g_p->jmax+1; j++)
            {
                if(!(k1 >= pml_p->ez_l && k1 <= pml_p->ez_h &&
		     i1 >= pml_p->ex_l && i1 <= pml_p->ex_h))
                {
		    if (i1 >= pml_p->ex_l && i1 <= pml_p->ex_h)
                    {
                        pml_p->aeyx[idey] = 1;
                        pml_p->beyx[idey] = -sch_p->dt_dx;
			
			if (k1 > pml_p->ez_h)
			{
			    pmlk = pml_p->epmlz_h - k1;

			    pml_p->aeyz[idey] = pml_p->c_hz[pmlk];
			    pml_p->beyz[idey] = -pml_p->d_hz[pmlk];
			}
			else
			{
			    pmlk = k1 - pml_p->epmlz_l;

			    pml_p->aeyz[idey] = pml_p->c_ez[pmlk];
			    pml_p->beyz[idey] = -pml_p->d_ez[pmlk];
			    pml_p->ende_y++;
			}
		    
			pml_p->idey[idey++] = IDX3(i,j,k, g_p->x_max, g_p->y_max, g_p->z_max);
			//pml_p->ne_m++;
		    }
                }
            }
        }
    }
    
    for (int k=g_p->kmin - ADD; k<g_p->kmax; k++)
    {
        int k1 = k + g_p->k_offset;
        for (int i=g_p->imin - ADD; i<g_p->imax; i++)
        {
            int i1 = i + g_p->i_offset;
	    for (int j=g_p->jmin-1; j<g_p->jmax+1; j++)
            {
                if(k1 >= pml_p->hz_l && k1 <= pml_p->hz_h &&
		   i1 >= pml_p->hx_l && i1 <= pml_p->hx_h)
		{
		    pml_p->idhyf[idhyf++] = IDX3(i,j,k, g_p->x_max, g_p->y_max, g_p->z_max);
		}
                else
                {
		    if (!(i1 >= pml_p->hx_l && i1 <= pml_p->hx_h))
		    {
			if (i1 > pml_p->hx_h)
			{
			    pmli = pml_p->hpmlx_h - i1;

			    pml_p->ahyx[idhy] = pml_p->c_ex[pmli];
			    pml_p->bhyx[idhy] = -pml_p->d_ex[pmli];
			}
			else
			{
			    pmli = i1 - pml_p->hpmlx_l;

			    pml_p->ahyx[idhy] = pml_p->c_hx[pmli];
			    pml_p->bhyx[idhy] = -pml_p->d_hx[pmli];
			}
			if (k1 >= pml_p->hz_l && k1 <= pml_p->hz_h)
			{
			    pml_p->ahyz[idhy] = 1;
			    pml_p->bhyz[idhy] = -sch_p->dt_dz;
			}
			else if (k1 > pml_p->hz_h)
			{
			    pmlk = pml_p->hpmlz_h - k1;

			    pml_p->ahyz[idhy] = pml_p->c_ez[pmlk];
			    pml_p->bhyz[idhy] = pml_p->d_ez[pmlk];
			}
			else
			{
			    pmlk = k1 - pml_p->hpmlz_l;

			    pml_p->ahyz[idhy] = pml_p->c_hz[pmlk];
			    pml_p->bhyz[idhy] = pml_p->d_hz[pmlk];
			}

			pml_p->idhy[idhy++] = IDX3(i,j,k, g_p->x_max, g_p->y_max, g_p->z_max);
			pml_p->nh_m++;
			pml_p->starth_y++;
			if (k == g_p->kmin)
			{
			    pml_p->sh_y++;
			}
		    }
                }
            }
        }
    }
    pml_p->endh_y = pml_p->starth_y;
    for (int k=g_p->kmin - ADD; k<g_p->kmax; k++)
    {
        int k1 = k + g_p->k_offset;
        for (int i=g_p->imin - ADD; i<g_p->imax; i++)
        {
            int i1 = i + g_p->i_offset;
	    for (int j=g_p->jmin-1; j<g_p->jmax+1; j++)
            {
                if(!(k1 >= pml_p->hz_l && k1 <= pml_p->hz_h &&
		     i1 >= pml_p->hx_l && i1 <= pml_p->hx_h))
		{
		    if (i1 >= pml_p->hx_l && i1 <= pml_p->hx_h)
		    {
                        pml_p->ahyx[idhy] = 1;
                        pml_p->bhyx[idhy] = sch_p->dt_dx;
                   
			if (k1 > pml_p->hz_h)
			{
			    pmlk = pml_p->hpmlz_h - k1;

			    pml_p->ahyz[idhy] = pml_p->c_ez[pmlk];
			    pml_p->bhyz[idhy] = pml_p->d_ez[pmlk];
			}
			else
			{
			    pmlk = k1 - pml_p->hpmlz_l;

			    pml_p->ahyz[idhy] = pml_p->c_hz[pmlk];
			    pml_p->bhyz[idhy] = pml_p->d_hz[pmlk];
			    pml_p->endh_y++;
			}

			pml_p->idhy[idhy++] = IDX3(i,j,k, g_p->x_max, g_p->y_max, g_p->z_max);
			pml_p->nh_m++;
		    }
                }
            }
        }
    }
    
    int idez = 0;
    int idhz = 0;
    int idezf = 0;
    int idhzf = 0;

    pml_p->se_z = 0;
    pml_p->sh_z = 0;
    
    for (int k=g_p->kmin-1; k<g_p->kmax+1; k++)
    {
        for (int i=g_p->imin; i<g_p->imax + ADD; i++)
        {
            int i1 = i + g_p->i_offset;
            for (int j=g_p->jmin; j<g_p->jmax + ADD; j++)
            {
		int j1 = j + g_p->j_offset;
		
                if(i1 >= pml_p->ex_l && i1 <= pml_p->ex_h &&
		   j1 >= pml_p->ey_l && j1 <= pml_p->ey_h)
                {
		    pml_p->idezf[idezf++] = IDX3(i,j,k, g_p->x_max, g_p->y_max, g_p->z_max);
		}
                else
                {
		    if (i1 >= pml_p->ex_l && i1 <= pml_p->ex_h)
                    {
			pml_p->aezx[idez] = 1;
                        pml_p->bezx[idez] = sch_p->dt_dx;
                    }
                    else if (i1 > pml_p->ex_h)
                    {
                        pmli = pml_p->epmlx_h - i1;

                        pml_p->aezx[idez] = pml_p->c_hx[pmli];
                        pml_p->bezx[idez] = -pml_p->d_hx[pmli];
                    }
                    else
                    {
                        pmli = i1 - pml_p->epmlx_l;

                        pml_p->aezx[idez] = pml_p->c_ex[pmli];
                        pml_p->bezx[idez] = -pml_p->d_ex[pmli];
                    }
		    
		    if (j1 >= pml_p->ey_l && j1 <= pml_p->ey_h)
                    {
			pml_p->aezy[idez] = 1;
                        pml_p->bezy[idez] = -sch_p->dt_dy;
                    }
                    else if (j1 > pml_p->ey_h)
                    {
                        pmlj = pml_p->epmly_h - j1;

                        pml_p->aezy[idez] = pml_p->c_hy[pmlj];
                        pml_p->bezy[idez] = pml_p->d_hy[pmlj];
                    }
                    else
                    {
                        pmlj = j1 - pml_p->epmly_l;

                        pml_p->aezy[idez] = pml_p->c_ey[pmlj];
                        pml_p->bezy[idez] = pml_p->d_ey[pmlj];
                    }
		    pml_p->idez[idez++] = IDX3(i,j,k, g_p->x_max, g_p->y_max, g_p->z_max);
                    //pml_p->ne_m++;

		    if (k == g_p->kmin-1)
		    {
			pml_p->se_z++;
		    }
		}
            }
        }
    }

    for (int k = g_p->kmin-1; k < g_p->kmax+1; k++)
    {
        for (int i = g_p->imin - ADD; i < g_p->imax; i++)
        {
            int i1 = i + g_p->i_offset;
            for (int j = g_p->jmin - ADD; j<g_p->jmax; j++)
            {
		int j1 = j+g_p->j_offset;

                if(i1 >= pml_p->hx_l && i1 <= pml_p->hx_h &&
		   j1 >= pml_p->hy_l && j1 <= pml_p->hy_h)
                {
		    pml_p->idhzf[idhzf++] = IDX3(i,j,k, g_p->x_max, g_p->y_max, g_p->z_max);
                }
                else
                {
		    if (i1 >= pml_p->hx_l && i1 <= pml_p->hx_h)
                    {
			pml_p->ahzx[idhz] = 1;
                        pml_p->bhzx[idhz] = -sch_p->dt_dx;
                    }
                    else if (i1 > pml_p->hx_h)
                    {
                        pmli = pml_p->hpmlx_h - i1;

			pml_p->ahzx[idhz] = pml_p->c_ex[pmli];
                        pml_p->bhzx[idhz] = pml_p->d_ex[pmli];
                    }
                    else
                    {
                        pmli = i1 - pml_p->hpmlx_l;

			pml_p->ahzx[idhz] = pml_p->c_hx[pmli];
                        pml_p->bhzx[idhz] = pml_p->d_hx[pmli];
                    }

		    if (j1 >= pml_p->hy_l && j1 <= pml_p->hy_h)
                    {
			pml_p->ahzy[idhz] = 1;
                        pml_p->bhzy[idhz] = sch_p->dt_dy;
                    }
                    else if (j1 > pml_p->hy_h)
                    {
                        pmlj = pml_p->hpmly_h - j1;

			pml_p->ahzy[idhz] = pml_p->c_ey[pmlj];
                        pml_p->bhzy[idhz] = -pml_p->d_ey[pmlj];
                    }
                    else
                    {
                        pmlj = j1 - pml_p->hpmly_l;

			pml_p->ahzy[idhz] = pml_p->c_hy[pmlj];
                        pml_p->bhzy[idhz] = -pml_p->d_hy[pmlj];
                    }

		    pml_p->idhz[idhz++] = IDX3(i,j,k, g_p->x_max, g_p->y_max, g_p->z_max);
                    pml_p->nh_m++;
		    if (k == g_p->kmin-1)
		    {
			pml_p->sh_z++;
		    }
		}
            }
        }
    }
#endif
}

void init_pml_fields_3d(pml_data* pml_p,
			geometry_data* g_p,
			scheme_constants* sch_p)
{
#ifdef FULL_UPDATE
    pml_p->xmax = g_p->kmax + ADD - g_p->kmin;
    pml_p->ymax = g_p->kmax + ADD - g_p->kmin;
    pml_p->zmax = g_p->kmax + 1 - g_p->kmin + 1;
        
    pml_p->idex = INT_MALLOC(pml_p->nex);
    pml_p->idhx = INT_MALLOC(pml_p->nhx);

    pml_p->idexf = INT_MALLOC(pml_p->nexf);
    pml_p->idhxf = INT_MALLOC(pml_p->nhxf);

    pml_p->idey = INT_MALLOC(pml_p->ney);
    pml_p->idhy = INT_MALLOC(pml_p->nhy);

    pml_p->ideyf = INT_MALLOC(pml_p->neyf);
    pml_p->idhyf = INT_MALLOC(pml_p->nhyf);

    pml_p->idez = INT_MALLOC(pml_p->nez);
    pml_p->idhz = INT_MALLOC(pml_p->nhz);

    pml_p->idezf = INT_MALLOC(pml_p->nezf);
    pml_p->idhzf = INT_MALLOC(pml_p->nhzf);

    pml_p->aexy = FP_MALLOC(pml_p->nex);
    pml_p->bexy = FP_MALLOC(pml_p->nex);
    pml_p->aexz = FP_MALLOC(pml_p->nex);
    pml_p->bexz = FP_MALLOC(pml_p->nex);

    pml_p->aeyx = FP_MALLOC(pml_p->ney);
    pml_p->beyx = FP_MALLOC(pml_p->ney);
    pml_p->aeyz = FP_MALLOC(pml_p->ney);
    pml_p->beyz = FP_MALLOC(pml_p->ney);

    pml_p->aezx = FP_MALLOC(pml_p->nez);
    pml_p->bezx = FP_MALLOC(pml_p->nez);
    pml_p->aezy = FP_MALLOC(pml_p->nez);
    pml_p->bezy = FP_MALLOC(pml_p->nez);

    pml_p->ahxy = FP_MALLOC(pml_p->nhx);
    pml_p->bhxy = FP_MALLOC(pml_p->nhx);
    pml_p->ahxz = FP_MALLOC(pml_p->nhx);
    pml_p->bhxz = FP_MALLOC(pml_p->nhx);

    pml_p->ahyx = FP_MALLOC(pml_p->nhy);
    pml_p->bhyx = FP_MALLOC(pml_p->nhy);
    pml_p->ahyz = FP_MALLOC(pml_p->nhy);
    pml_p->bhyz = FP_MALLOC(pml_p->nhy);

    pml_p->ahzx = FP_MALLOC(pml_p->nhz);
    pml_p->bhzx = FP_MALLOC(pml_p->nhz);
    pml_p->ahzy = FP_MALLOC(pml_p->nhz);
    pml_p->bhzy = FP_MALLOC(pml_p->nhz);

    pml_p->Exy = FP_MALLOC(pml_p->nex);
    pml_p->Exz = FP_MALLOC(pml_p->nex);

    pml_p->Eyx = FP_MALLOC(pml_p->ney);
    pml_p->Eyz = FP_MALLOC(pml_p->ney);

    pml_p->Ezx = FP_MALLOC(pml_p->nez);
    pml_p->Ezy = FP_MALLOC(pml_p->nez);

    pml_p->Hxy = FP_MALLOC(pml_p->nhx);
    pml_p->Hxz = FP_MALLOC(pml_p->nhx);

    pml_p->Hyx = FP_MALLOC(pml_p->nhy);
    pml_p->Hyz = FP_MALLOC(pml_p->nhy);

    pml_p->Hzx = FP_MALLOC(pml_p->nhz);
    pml_p->Hzy = FP_MALLOC(pml_p->nhz);

  
    int pmli, pmlj, pmlk;

    int idex = 0;
    int idhx = 0;
    int idexf = 0;
    int idhxf = 0;
    
    pml_p->se_x = 0;
    pml_p->sh_x = 0;
    pml_p->starte_x = 0;
    pml_p->starth_x = 0;
    pml_p->ne1_m = 0;
    pml_p->ne2_m = 0;
    pml_p->ne_m = 0;
    
    for (int k=g_p->kmin; k<g_p->kmax + ADD; k++)
    {
        int k1 = k + g_p->k_offset;
        for (int i=g_p->imin-1; i<g_p->imax+1; i++)
        {
            for (int j=g_p->jmin; j<g_p->jmax + ADD; j++)
            {
		int j1 = j+g_p->j_offset;

                if(k1 >= pml_p->ez_l && k1 <= pml_p->ez_h &&
		   j1 >= pml_p->ey_l && j1 <= pml_p->ey_h)
                {
		    pml_p->idexf[idexf++] = IDX3(i,j,k, g_p->x_max, g_p->y_max, g_p->z_max);
                }
                else
                {
		    if (!(j1 >= pml_p->ey_l && j1 <= pml_p->ey_h))
                    {
			if (j1 > pml_p->ey_h)
			{
			    pmlj = pml_p->epmly_h - j1;
			
			    pml_p->aexy[idex] = pml_p->c_hy[pmlj];
			    pml_p->bexy[idex] = -pml_p->d_hy[pmlj];
			}
			else
			{
			    pmlj = j1 - pml_p->epmly_l;

			    pml_p->aexy[idex] = pml_p->c_ey[pmlj];
			    pml_p->bexy[idex] = -pml_p->d_ey[pmlj];
			}

			if (k1 >= pml_p->ez_l && k1 <= pml_p->ez_h)
			{
			    pml_p->aexz[idex] = 1;
			    pml_p->bexz[idex] = -sch_p->dt_dz;
			}
			else if (k1 > pml_p->ez_h)
			{
			    pmlk = pml_p->epmlz_h - k1;

			    pml_p->aexz[idex] = pml_p->c_hz[pmlk];
			    pml_p->bexz[idex] = pml_p->d_hz[pmlk];
			}
			else
			{
			    pmlk = k1 - pml_p->epmlz_l;

			    pml_p->aexz[idex] = pml_p->c_ez[pmlk];
			    pml_p->bexz[idex] = pml_p->d_ez[pmlk];
			}
		    
			pml_p->idex[idex++] = IDX3(i,j,k, g_p->x_max, g_p->y_max, g_p->z_max);
			pml_p->ne_m++;
			pml_p->starte_x++;
			
			if (k == g_p->kmin)
			{
			    pml_p->se_x++;
			}
		    }
                }
            }
        }
    }
    
    pml_p->ende_x = pml_p->starte_x;
    
    for (int k=g_p->kmin; k<g_p->kmax + ADD; k++)
    {
        int k1 = k + g_p->k_offset;
        for (int i=g_p->imin-1; i<g_p->imax+1; i++)
        {
            for (int j=g_p->jmin; j<g_p->jmax + ADD; j++)
            {
		int j1 = j+g_p->j_offset;

                if(!(k1 >= pml_p->ez_l && k1 <= pml_p->ez_h &&
		     j1 >= pml_p->ey_l && j1 <= pml_p->ey_h))
                {
		    if (j1 >= pml_p->ey_l && j1 <= pml_p->ey_h)
                    {
			pml_p->aexy[idex] = 1;
                        pml_p->bexy[idex] = sch_p->dt_dy;
			
			if (k1 > pml_p->ez_h)
			{
			    pmlk = pml_p->epmlz_h - k1;
			    
			    pml_p->aexz[idex] = pml_p->c_hz[pmlk];
			    pml_p->bexz[idex] = pml_p->d_hz[pmlk];
			    pml_p->ne1_m++;
			}
			else
			{
			    pmlk = k1 - pml_p->epmlz_l;
			    
			    pml_p->aexz[idex] = pml_p->c_ez[pmlk];
			    pml_p->bexz[idex] = pml_p->d_ez[pmlk];
			    pml_p->ende_x++;
			    pml_p->ne2_m++;
			}
		    
			pml_p->idex[idex++] = IDX3(i,j,k, g_p->x_max, g_p->y_max, g_p->z_max);
			//pml_p->ne_m++;
		    }
                }
            }
        }
    }
    
    for (int k=g_p->kmin - ADD; k<g_p->kmax; k++)
    {
        int k1 = k + g_p->k_offset;
        for (int i=g_p->imin-1; i<g_p->imax+1; i++)
        {
            for (int j=g_p->jmin - ADD; j<g_p->jmax; j++)
            {
		int j1 = j+g_p->j_offset;

		if(k1 >= pml_p->hz_l && k1 <= pml_p->hz_h &&
		   j1 >= pml_p->hy_l && j1 <= pml_p->hy_h)
                {
		    pml_p->idhxf[idhxf++] = IDX3(i,j,k, g_p->x_max, g_p->y_max, g_p->z_max);
                }
                else
                {
		    if (!(j1 >= pml_p->hy_l && j1 <= pml_p->hy_h))
                    {
			if (j1 > pml_p->hy_h)
			{
			    pmlj = pml_p->hpmly_h - j1;

			    pml_p->ahxy[idhx] = pml_p->c_ey[pmlj];
			    pml_p->bhxy[idhx] = pml_p->d_ey[pmlj];
			}
			else
			{
			    pmlj = j1 - pml_p->hpmly_l;

			    pml_p->ahxy[idhx] = pml_p->c_hy[pmlj];
			    pml_p->bhxy[idhx] = pml_p->d_hy[pmlj];
			}

			if (k1 >= pml_p->hz_l && k1 <= pml_p->hz_h)
			{
			    pml_p->ahxz[idhx] = 1;
			    pml_p->bhxz[idhx] = sch_p->dt_dz;
			}
			else if (k1 > pml_p->hz_h)
			{
			    pmlk = pml_p->hpmlz_h - k1;

			    pml_p->ahxz[idhx] = pml_p->c_ez[pmlk];
			    pml_p->bhxz[idhx] = -pml_p->d_ez[pmlk];
			}
			else
			{
			    pmlk = k1 - pml_p->hpmlz_l;

			    pml_p->ahxz[idhx] = pml_p->c_hz[pmlk];
			    pml_p->bhxz[idhx] = -pml_p->d_hz[pmlk];
			}
		    
			pml_p->idhx[idhx++] = IDX3(i,j,k, g_p->x_max, g_p->y_max, g_p->z_max);
			pml_p->nh_m++;
			pml_p->starth_x++;
			
			if (k == g_p->kmin)
			{
			    pml_p->sh_x++;
			}
		    }
                }
            }
        }
    }
    
    pml_p->endh_x = pml_p->starth_x;
    
    for (int k=g_p->kmin - ADD; k<g_p->kmax; k++)
    {
        int k1 = k + g_p->k_offset;
        for (int i=g_p->imin-1; i<g_p->imax+1; i++)
        {
            for (int j=g_p->jmin - ADD; j<g_p->jmax; j++)
            {
		int j1 = j+g_p->j_offset;

		if(!(k1 >= pml_p->hz_l && k1 <= pml_p->hz_h &&
		     j1 >= pml_p->hy_l && j1 <= pml_p->hy_h))
		{
		    if (j1 >= pml_p->hy_l && j1 <= pml_p->hy_h)
                    {
                        pml_p->ahxy[idhx] = 1;
                        pml_p->bhxy[idhx] = -sch_p->dt_dy;
		    

			if (k1 > pml_p->hz_h)
			{
			    pmlk = pml_p->hpmlz_h - k1;

			    pml_p->ahxz[idhx] = pml_p->c_ez[pmlk];
			    pml_p->bhxz[idhx] = -pml_p->d_ez[pmlk];
			}
			else
			{
			    pmlk = k1 - pml_p->hpmlz_l;

			    pml_p->ahxz[idhx] = pml_p->c_hz[pmlk];
			    pml_p->bhxz[idhx] = -pml_p->d_hz[pmlk];
			    pml_p->endh_x++;
			}
		    
			pml_p->idhx[idhx++] = IDX3(i,j,k, g_p->x_max, g_p->y_max, g_p->z_max);
			pml_p->nh_m++;
		    }
                }
            }
        }
    }
   
    int idey = 0;
    int idhy = 0;
    int ideyf = 0;
    int idhyf = 0;
    
    pml_p->se_y = 0;
    pml_p->sh_y = 0;
    pml_p->starte_y = 0;
    pml_p->starth_y = 0;
    
    for (int k=g_p->kmin; k<g_p->kmax + ADD; k++)
    {
        int k1 = k+g_p->k_offset;
        for (int i=g_p->imin; i<g_p->imax + ADD; i++)
        {
            int i1 = i+g_p->i_offset;
            for (int j=g_p->jmin-1; j<g_p->jmax+1; j++)
            {
                if(k1 >= pml_p->ez_l && k1 <= pml_p->ez_h &&
		   i1 >= pml_p->ex_l && i1 <= pml_p->ex_h)
                {
		    pml_p->ideyf[ideyf++] = IDX3(i,j,k, g_p->x_max, g_p->y_max, g_p->z_max);
		}
                else
                {
		    if (!(i1 >= pml_p->ex_l && i1 <= pml_p->ex_h))
                    {
			if (i1 > pml_p->ex_h)
			{
			    pmli = pml_p->epmlx_h - i1;
			    
			    pml_p->aeyx[idey] = pml_p->c_hx[pmli];
			    pml_p->beyx[idey] = pml_p->d_hx[pmli];
			}
			else
			{
			    pmli = i1 - pml_p->epmlx_l;
			    
			    pml_p->aeyx[idey] = pml_p->c_ex[pmli];
			    pml_p->beyx[idey] = pml_p->d_ex[pmli];
			}

			if (k1 >= pml_p->ez_l && k1 <= pml_p->ez_h)
			{
			    pml_p->aeyz[idey] = 1;
			    pml_p->beyz[idey] = sch_p->dt_dz;
			}
			else if (k1 > pml_p->ez_h)
			{
			    pmlk = pml_p->epmlz_h - k1;

			    pml_p->aeyz[idey] = pml_p->c_hz[pmlk];
			    pml_p->beyz[idey] = -pml_p->d_hz[pmlk];
			}
			else
			{
			    pmlk = k1 - pml_p->epmlz_l;

			    pml_p->aeyz[idey] = pml_p->c_ez[pmlk];
			    pml_p->beyz[idey] = -pml_p->d_ez[pmlk];
			}
		    
			pml_p->idey[idey++] = IDX3(i,j,k, g_p->x_max, g_p->y_max, g_p->z_max);
			//pml_p->ne_m++;
			pml_p->starte_y++;
			if (k == g_p->kmin)
			{
			    pml_p->se_y++;
			}
		    }
                }
            }
        }
    }
    pml_p->ende_y =  pml_p->starte_y;
    
    for (int k=g_p->kmin; k<g_p->kmax + ADD; k++)
    {
        int k1 = k+g_p->k_offset;
        for (int i=g_p->imin; i<g_p->imax + ADD; i++)
        {
            int i1 = i+g_p->i_offset;
            for (int j=g_p->jmin-1; j<g_p->jmax+1; j++)
            {
                if(!(k1 >= pml_p->ez_l && k1 <= pml_p->ez_h &&
		     i1 >= pml_p->ex_l && i1 <= pml_p->ex_h))
                {
		    if (i1 >= pml_p->ex_l && i1 <= pml_p->ex_h)
                    {
                        pml_p->aeyx[idey] = 1;
                        pml_p->beyx[idey] = -sch_p->dt_dx;
			
			if (k1 > pml_p->ez_h)
			{
			    pmlk = pml_p->epmlz_h - k1;

			    pml_p->aeyz[idey] = pml_p->c_hz[pmlk];
			    pml_p->beyz[idey] = -pml_p->d_hz[pmlk];
			}
			else
			{
			    pmlk = k1 - pml_p->epmlz_l;

			    pml_p->aeyz[idey] = pml_p->c_ez[pmlk];
			    pml_p->beyz[idey] = -pml_p->d_ez[pmlk];
			    pml_p->ende_y++;
			}
		    
			pml_p->idey[idey++] = IDX3(i,j,k, g_p->x_max, g_p->y_max, g_p->z_max);
			//pml_p->ne_m++;
		    }
                }
            }
        }
    }
    
    for (int k=g_p->kmin - ADD; k<g_p->kmax; k++)
    {
        int k1 = k + g_p->k_offset;
        for (int i=g_p->imin - ADD; i<g_p->imax; i++)
        {
            int i1 = i + g_p->i_offset;
	    for (int j=g_p->jmin-1; j<g_p->jmax+1; j++)
            {
                if(k1 >= pml_p->hz_l && k1 <= pml_p->hz_h &&
		   i1 >= pml_p->hx_l && i1 <= pml_p->hx_h)
		{
		    pml_p->idhyf[idhyf++] = IDX3(i,j,k, g_p->x_max, g_p->y_max, g_p->z_max);
		}
                else
                {
		    if (!(i1 >= pml_p->hx_l && i1 <= pml_p->hx_h))
		    {
			if (i1 > pml_p->hx_h)
			{
			    pmli = pml_p->hpmlx_h - i1;

			    pml_p->ahyx[idhy] = pml_p->c_ex[pmli];
			    pml_p->bhyx[idhy] = -pml_p->d_ex[pmli];
			}
			else
			{
			    pmli = i1 - pml_p->hpmlx_l;

			    pml_p->ahyx[idhy] = pml_p->c_hx[pmli];
			    pml_p->bhyx[idhy] = -pml_p->d_hx[pmli];
			}
			if (k1 >= pml_p->hz_l && k1 <= pml_p->hz_h)
			{
			    pml_p->ahyz[idhy] = 1;
			    pml_p->bhyz[idhy] = -sch_p->dt_dz;
			}
			else if (k1 > pml_p->hz_h)
			{
			    pmlk = pml_p->hpmlz_h - k1;

			    pml_p->ahyz[idhy] = pml_p->c_ez[pmlk];
			    pml_p->bhyz[idhy] = pml_p->d_ez[pmlk];
			}
			else
			{
			    pmlk = k1 - pml_p->hpmlz_l;

			    pml_p->ahyz[idhy] = pml_p->c_hz[pmlk];
			    pml_p->bhyz[idhy] = pml_p->d_hz[pmlk];
			}

			pml_p->idhy[idhy++] = IDX3(i,j,k, g_p->x_max, g_p->y_max, g_p->z_max);
			pml_p->nh_m++;
			pml_p->starth_y++;
			if (k == g_p->kmin)
			{
			    pml_p->sh_y++;
			}
		    }
                }
            }
        }
    }
    pml_p->endh_y = pml_p->starth_y;
    for (int k=g_p->kmin - ADD; k<g_p->kmax; k++)
    {
        int k1 = k + g_p->k_offset;
        for (int i=g_p->imin - ADD; i<g_p->imax; i++)
        {
            int i1 = i + g_p->i_offset;
	    for (int j=g_p->jmin-1; j<g_p->jmax+1; j++)
            {
                if(!(k1 >= pml_p->hz_l && k1 <= pml_p->hz_h &&
		     i1 >= pml_p->hx_l && i1 <= pml_p->hx_h))
		{
		    if (i1 >= pml_p->hx_l && i1 <= pml_p->hx_h)
		    {
                        pml_p->ahyx[idhy] = 1;
                        pml_p->bhyx[idhy] = sch_p->dt_dx;
                   
			if (k1 > pml_p->hz_h)
			{
			    pmlk = pml_p->hpmlz_h - k1;

			    pml_p->ahyz[idhy] = pml_p->c_ez[pmlk];
			    pml_p->bhyz[idhy] = pml_p->d_ez[pmlk];
			}
			else
			{
			    pmlk = k1 - pml_p->hpmlz_l;

			    pml_p->ahyz[idhy] = pml_p->c_hz[pmlk];
			    pml_p->bhyz[idhy] = pml_p->d_hz[pmlk];
			    pml_p->endh_y++;
			}

			pml_p->idhy[idhy++] = IDX3(i,j,k, g_p->x_max, g_p->y_max, g_p->z_max);
			pml_p->nh_m++;
		    }
                }
            }
        }
    }
    
    int idez = 0;
    int idhz = 0;
    int idezf = 0;
    int idhzf = 0;

    pml_p->se_z = 0;
    pml_p->sh_z = 0;
    
    for (int k=g_p->kmin-1; k<g_p->kmax+1; k++)
    {
        for (int i=g_p->imin; i<g_p->imax + ADD; i++)
        {
            int i1 = i + g_p->i_offset;
            for (int j=g_p->jmin; j<g_p->jmax + ADD; j++)
            {
		int j1 = j + g_p->j_offset;
		
                if(i1 >= pml_p->ex_l && i1 <= pml_p->ex_h &&
		   j1 >= pml_p->ey_l && j1 <= pml_p->ey_h)
                {
		    pml_p->idezf[idezf++] = IDX3(i,j,k, g_p->x_max, g_p->y_max, g_p->z_max);
		}
                else
                {
		    if (i1 >= pml_p->ex_l && i1 <= pml_p->ex_h)
                    {
			pml_p->aezx[idez] = 1;
                        pml_p->bezx[idez] = sch_p->dt_dx;
                    }
                    else if (i1 > pml_p->ex_h)
                    {
                        pmli = pml_p->epmlx_h - i1;

                        pml_p->aezx[idez] = pml_p->c_hx[pmli];
                        pml_p->bezx[idez] = -pml_p->d_hx[pmli];
                    }
                    else
                    {
                        pmli = i1 - pml_p->epmlx_l;

                        pml_p->aezx[idez] = pml_p->c_ex[pmli];
                        pml_p->bezx[idez] = -pml_p->d_ex[pmli];
                    }
		    
		    if (j1 >= pml_p->ey_l && j1 <= pml_p->ey_h)
                    {
			pml_p->aezy[idez] = 1;
                        pml_p->bezy[idez] = -sch_p->dt_dy;
                    }
                    else if (j1 > pml_p->ey_h)
                    {
                        pmlj = pml_p->epmly_h - j1;

                        pml_p->aezy[idez] = pml_p->c_hy[pmlj];
                        pml_p->bezy[idez] = pml_p->d_hy[pmlj];
                    }
                    else
                    {
                        pmlj = j1 - pml_p->epmly_l;

                        pml_p->aezy[idez] = pml_p->c_ey[pmlj];
                        pml_p->bezy[idez] = pml_p->d_ey[pmlj];
                    }
		    pml_p->idez[idez++] = IDX3(i,j,k, g_p->x_max, g_p->y_max, g_p->z_max);
                    //pml_p->ne_m++;

		    if (k == g_p->kmin-1)
		    {
			pml_p->se_z++;
		    }
		}
            }
        }
    }

    for (int k = g_p->kmin-1; k < g_p->kmax+1; k++)
    {
        for (int i = g_p->imin - ADD; i < g_p->imax; i++)
        {
            int i1 = i + g_p->i_offset;
            for (int j = g_p->jmin - ADD; j<g_p->jmax; j++)
            {
		int j1 = j+g_p->j_offset;

                if(i1 >= pml_p->hx_l && i1 <= pml_p->hx_h &&
		   j1 >= pml_p->hy_l && j1 <= pml_p->hy_h)
                {
		    pml_p->idhzf[idhzf++] = IDX3(i,j,k, g_p->x_max, g_p->y_max, g_p->z_max);
                }
                else
                {
		    if (i1 >= pml_p->hx_l && i1 <= pml_p->hx_h)
                    {
			pml_p->ahzx[idhz] = 1;
                        pml_p->bhzx[idhz] = -sch_p->dt_dx;
                    }
                    else if (i1 > pml_p->hx_h)
                    {
                        pmli = pml_p->hpmlx_h - i1;

			pml_p->ahzx[idhz] = pml_p->c_ex[pmli];
                        pml_p->bhzx[idhz] = pml_p->d_ex[pmli];
                    }
                    else
                    {
                        pmli = i1 - pml_p->hpmlx_l;

			pml_p->ahzx[idhz] = pml_p->c_hx[pmli];
                        pml_p->bhzx[idhz] = pml_p->d_hx[pmli];
                    }

		    if (j1 >= pml_p->hy_l && j1 <= pml_p->hy_h)
                    {
			pml_p->ahzy[idhz] = 1;
                        pml_p->bhzy[idhz] = sch_p->dt_dy;
                    }
                    else if (j1 > pml_p->hy_h)
                    {
                        pmlj = pml_p->hpmly_h - j1;

			pml_p->ahzy[idhz] = pml_p->c_ey[pmlj];
                        pml_p->bhzy[idhz] = -pml_p->d_ey[pmlj];
                    }
                    else
                    {
                        pmlj = j1 - pml_p->hpmly_l;

			pml_p->ahzy[idhz] = pml_p->c_hy[pmlj];
                        pml_p->bhzy[idhz] = -pml_p->d_hy[pmlj];
                    }

		    pml_p->idhz[idhz++] = IDX3(i,j,k, g_p->x_max, g_p->y_max, g_p->z_max);
                    pml_p->nh_m++;
		    if (k == g_p->kmin-1)
		    {
			pml_p->sh_z++;
		    }
		}
            }
        }
    }
#else /* FULL_UPDATE */
    FIELD_MALLOC(pml_p->pml_f.Exy_y_l);
    FIELD_MALLOC(pml_p->pml_f.Exy_y_h);
    FIELD_MALLOC(pml_p->pml_f.Exy_z_l);
    FIELD_MALLOC(pml_p->pml_f.Exy_z_h);
    FIELD_MALLOC(pml_p->pml_f.Exz_y_l);
    FIELD_MALLOC(pml_p->pml_f.Exz_y_h);
    FIELD_MALLOC(pml_p->pml_f.Exz_z_l);
    FIELD_MALLOC(pml_p->pml_f.Exz_z_h);
    FIELD_MALLOC(pml_p->pml_f.Eyx_x_l);
    FIELD_MALLOC(pml_p->pml_f.Eyx_x_h);
    FIELD_MALLOC(pml_p->pml_f.Eyx_z_l);
    FIELD_MALLOC(pml_p->pml_f.Eyx_z_h);
    FIELD_MALLOC(pml_p->pml_f.Eyz_x_l);
    FIELD_MALLOC(pml_p->pml_f.Eyz_x_h);
    FIELD_MALLOC(pml_p->pml_f.Eyz_z_l);
    FIELD_MALLOC(pml_p->pml_f.Eyz_z_h);
    FIELD_MALLOC(pml_p->pml_f.Ezy_y_l);
    FIELD_MALLOC(pml_p->pml_f.Ezy_y_h);
    FIELD_MALLOC(pml_p->pml_f.Ezy_x_l);
    FIELD_MALLOC(pml_p->pml_f.Ezy_x_h);
    FIELD_MALLOC(pml_p->pml_f.Ezx_y_l);
    FIELD_MALLOC(pml_p->pml_f.Ezx_y_h);
    FIELD_MALLOC(pml_p->pml_f.Ezx_x_l);
    FIELD_MALLOC(pml_p->pml_f.Ezx_x_h);

    FIELD_MALLOC(pml_p->pml_f.Hxy_y_l);
    FIELD_MALLOC(pml_p->pml_f.Hxy_y_h);
    FIELD_MALLOC(pml_p->pml_f.Hxy_z_l);
    FIELD_MALLOC(pml_p->pml_f.Hxy_z_h);
    FIELD_MALLOC(pml_p->pml_f.Hxz_y_l);
    FIELD_MALLOC(pml_p->pml_f.Hxz_y_h);
    FIELD_MALLOC(pml_p->pml_f.Hxz_z_l);
    FIELD_MALLOC(pml_p->pml_f.Hxz_z_h);
    FIELD_MALLOC(pml_p->pml_f.Hyx_x_l);
    FIELD_MALLOC(pml_p->pml_f.Hyx_x_h);
    FIELD_MALLOC(pml_p->pml_f.Hyx_z_l);
    FIELD_MALLOC(pml_p->pml_f.Hyx_z_h);
    FIELD_MALLOC(pml_p->pml_f.Hyz_x_l);
    FIELD_MALLOC(pml_p->pml_f.Hyz_x_h);
    FIELD_MALLOC(pml_p->pml_f.Hyz_z_l);
    FIELD_MALLOC(pml_p->pml_f.Hyz_z_h);
    FIELD_MALLOC(pml_p->pml_f.Hzy_y_l);
    FIELD_MALLOC(pml_p->pml_f.Hzy_y_h);
    FIELD_MALLOC(pml_p->pml_f.Hzy_x_l);
    FIELD_MALLOC(pml_p->pml_f.Hzy_x_h);
    FIELD_MALLOC(pml_p->pml_f.Hzx_y_l);
    FIELD_MALLOC(pml_p->pml_f.Hzx_y_h);
    FIELD_MALLOC(pml_p->pml_f.Hzx_x_l);
    FIELD_MALLOC(pml_p->pml_f.Hzx_x_h);

#ifdef USEMPI
    if (mpi.num_proc_x == 1)
    {
        init_field3d(pml_p->pml_f.Eyx_x_l, pml_p->pmlx, g_p->y_max, g_p->z_max);
        init_field3d(pml_p->pml_f.Eyz_x_l, pml_p->pmlx, g_p->y_max, g_p->z_max);
        init_field3d(pml_p->pml_f.Ezx_x_l, pml_p->pmlx, g_p->y_max, g_p->z_max);
        init_field3d(pml_p->pml_f.Ezy_x_l, pml_p->pmlx, g_p->y_max, g_p->z_max);

        init_field3d(pml_p->pml_f.Hyx_x_l, pml_p->pmlx, g_p->y_max, g_p->z_max);
        init_field3d(pml_p->pml_f.Hyz_x_l, pml_p->pmlx, g_p->y_max, g_p->z_max);
        init_field3d(pml_p->pml_f.Hzx_x_l, pml_p->pmlx, g_p->y_max, g_p->z_max);
        init_field3d(pml_p->pml_f.Hzy_x_l, pml_p->pmlx, g_p->y_max, g_p->z_max);

        init_field3d(pml_p->pml_f.Eyx_x_h, pml_p->pmlx, g_p->y_max, g_p->z_max);
        init_field3d(pml_p->pml_f.Eyz_x_h, pml_p->pmlx, g_p->y_max, g_p->z_max);
        init_field3d(pml_p->pml_f.Ezx_x_h, pml_p->pmlx, g_p->y_max, g_p->z_max);
        init_field3d(pml_p->pml_f.Ezy_x_h, pml_p->pmlx, g_p->y_max, g_p->z_max);

        init_field3d(pml_p->pml_f.Hyx_x_h, pml_p->pmlx, g_p->y_max, g_p->z_max);
        init_field3d(pml_p->pml_f.Hyz_x_h, pml_p->pmlx, g_p->y_max, g_p->z_max);
        init_field3d(pml_p->pml_f.Hzx_x_h, pml_p->pmlx, g_p->y_max, g_p->z_max);
        init_field3d(pml_p->pml_f.Hzy_x_h, pml_p->pmlx, g_p->y_max, g_p->z_max);
    }
    else
    {
        int xmin = g_p->i_offset;
        int xmax = g_p->i_offset+g_p->imax;
        int pml_min = pml_p->pmlx;
        int pml_max = g_p->x_full-1-pml_p->pmlx;

        if (xmin < pml_min && xmax > pml_min)
        {
            init_field3d(pml_p->pml_f.Eyx_x_l, pml_min-xmin, g_p->y_max, g_p->z_max);
            init_field3d(pml_p->pml_f.Eyz_x_l, pml_min-xmin, g_p->y_max, g_p->z_max);
            init_field3d(pml_p->pml_f.Ezx_x_l, pml_min-xmin, g_p->y_max, g_p->z_max);
            init_field3d(pml_p->pml_f.Ezy_x_l, pml_min-xmin, g_p->y_max, g_p->z_max);

            init_field3d(pml_p->pml_f.Hyx_x_l, pml_min-xmin, g_p->y_max, g_p->z_max);
            init_field3d(pml_p->pml_f.Hyz_x_l, pml_min-xmin, g_p->y_max, g_p->z_max);
            init_field3d(pml_p->pml_f.Hzx_x_l, pml_min-xmin, g_p->y_max, g_p->z_max);
            init_field3d(pml_p->pml_f.Hzy_x_l, pml_min-xmin, g_p->y_max, g_p->z_max);

            init_field3d(pml_p->pml_f.Eyx_x_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Eyz_x_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Ezx_x_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Ezy_x_h, 0, 0, 0);

            init_field3d(pml_p->pml_f.Hyx_x_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hyz_x_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hzx_x_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hzy_x_h, 0, 0, 0);
        }
        else if (xmax > pml_max && xmin < pml_max)
        {
            init_field3d(pml_p->pml_f.Eyx_x_h, xmax-pml_max, g_p->y_max, g_p->z_max);
            init_field3d(pml_p->pml_f.Eyz_x_h, xmax-pml_max, g_p->y_max, g_p->z_max);
            init_field3d(pml_p->pml_f.Ezx_x_h, xmax-pml_max, g_p->y_max, g_p->z_max);
            init_field3d(pml_p->pml_f.Ezy_x_h, xmax-pml_max, g_p->y_max, g_p->z_max);

            init_field3d(pml_p->pml_f.Hyx_x_h, xmax-pml_max, g_p->y_max, g_p->z_max);
            init_field3d(pml_p->pml_f.Hyz_x_h, xmax-pml_max, g_p->y_max, g_p->z_max);
            init_field3d(pml_p->pml_f.Hzx_x_h, xmax-pml_max, g_p->y_max, g_p->z_max);
            init_field3d(pml_p->pml_f.Hzy_x_h, xmax-pml_max, g_p->y_max, g_p->z_max);

            init_field3d(pml_p->pml_f.Eyx_x_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Eyz_x_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Ezx_x_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Ezy_x_l, 0, 0, 0);

            init_field3d(pml_p->pml_f.Hyx_x_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hyz_x_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hzx_x_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hzy_x_l, 0, 0, 0);
        }
        else if (xmax < pml_min)
        {
            init_field3d(pml_p->pml_f.Eyx_x_l, g_p->x_max-1, g_p->y_max, g_p->z_max);
            init_field3d(pml_p->pml_f.Eyz_x_l, g_p->x_max-1, g_p->y_max, g_p->z_max);
            init_field3d(pml_p->pml_f.Ezx_x_l, g_p->x_max-1, g_p->y_max, g_p->z_max);
            init_field3d(pml_p->pml_f.Ezy_x_l, g_p->x_max-1, g_p->y_max, g_p->z_max);

            init_field3d(pml_p->pml_f.Hyx_x_l, g_p->x_max-1, g_p->y_max, g_p->z_max);
            init_field3d(pml_p->pml_f.Hyz_x_l, g_p->x_max-1, g_p->y_max, g_p->z_max);
            init_field3d(pml_p->pml_f.Hzx_x_l, g_p->x_max-1, g_p->y_max, g_p->z_max);
            init_field3d(pml_p->pml_f.Hzy_x_l, g_p->x_max-1, g_p->y_max, g_p->z_max);

            init_field3d(pml_p->pml_f.Eyx_x_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Eyz_x_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Ezx_x_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Ezy_x_h, 0, 0, 0);

            init_field3d(pml_p->pml_f.Hyx_x_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hyz_x_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hzx_x_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hzy_x_h, 0, 0, 0);
        }
        else if (xmin > pml_max)
        {
            init_field3d(pml_p->pml_f.Eyx_x_h, g_p->x_max-1, g_p->y_max, g_p->z_max);
            init_field3d(pml_p->pml_f.Eyz_x_h, g_p->x_max-1, g_p->y_max, g_p->z_max);
            init_field3d(pml_p->pml_f.Ezx_x_h, g_p->x_max-1, g_p->y_max, g_p->z_max);
            init_field3d(pml_p->pml_f.Ezy_x_h, g_p->x_max-1, g_p->y_max, g_p->z_max);

            init_field3d(pml_p->pml_f.Hyx_x_h, g_p->x_max-1, g_p->y_max, g_p->z_max);
            init_field3d(pml_p->pml_f.Hyz_x_h, g_p->x_max-1, g_p->y_max, g_p->z_max);
            init_field3d(pml_p->pml_f.Hzx_x_h, g_p->x_max-1, g_p->y_max, g_p->z_max);
            init_field3d(pml_p->pml_f.Hzy_x_h, g_p->x_max-1, g_p->y_max, g_p->z_max);

            init_field3d(pml_p->pml_f.Eyx_x_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Eyz_x_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Ezx_x_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Ezy_x_l, 0, 0, 0);

            init_field3d(pml_p->pml_f.Hyx_x_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hyz_x_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hzx_x_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hzy_x_l, 0, 0, 0);
        }
        else
        {
            /* simply to avoid warnings during compilation */
            init_field3d(pml_p->pml_f.Eyx_x_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Eyz_x_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Ezx_x_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Ezy_x_h, 0, 0, 0);

            init_field3d(pml_p->pml_f.Hyx_x_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hyz_x_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hzx_x_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hzy_x_h, 0, 0, 0);

            init_field3d(pml_p->pml_f.Eyx_x_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Eyz_x_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Ezx_x_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Ezy_x_l, 0, 0, 0);

            init_field3d(pml_p->pml_f.Hyx_x_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hyz_x_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hzx_x_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hzy_x_l, 0, 0, 0);
        }
    }

    if (mpi.num_proc_y == 1)
    {
        init_field3d(pml_p->pml_f.Exz_y_l, g_p->x_max, pml_p->pmly, g_p->z_max);
        init_field3d(pml_p->pml_f.Exy_y_l, g_p->x_max, pml_p->pmly, g_p->z_max);
        init_field3d(pml_p->pml_f.Ezy_y_l, g_p->x_max, pml_p->pmly, g_p->z_max);
        init_field3d(pml_p->pml_f.Ezx_y_l, g_p->x_max, pml_p->pmly, g_p->z_max);

        init_field3d(pml_p->pml_f.Hxy_y_l, g_p->x_max, pml_p->pmly, g_p->z_max);
        init_field3d(pml_p->pml_f.Hxz_y_l, g_p->x_max, pml_p->pmly, g_p->z_max);
        init_field3d(pml_p->pml_f.Hzy_y_l, g_p->x_max, pml_p->pmly, g_p->z_max);
        init_field3d(pml_p->pml_f.Hzx_y_l, g_p->x_max, pml_p->pmly, g_p->z_max);

        init_field3d(pml_p->pml_f.Exz_y_h, g_p->x_max, pml_p->pmly, g_p->z_max);
        init_field3d(pml_p->pml_f.Exy_y_h, g_p->x_max, pml_p->pmly, g_p->z_max);
        init_field3d(pml_p->pml_f.Ezy_y_h, g_p->x_max, pml_p->pmly, g_p->z_max);
        init_field3d(pml_p->pml_f.Ezx_y_h, g_p->x_max, pml_p->pmly, g_p->z_max);

        init_field3d(pml_p->pml_f.Hxy_y_h, g_p->x_max, pml_p->pmly, g_p->z_max);
        init_field3d(pml_p->pml_f.Hxz_y_h, g_p->x_max, pml_p->pmly, g_p->z_max);
        init_field3d(pml_p->pml_f.Hzy_y_h, g_p->x_max, pml_p->pmly, g_p->z_max);
        init_field3d(pml_p->pml_f.Hzx_y_h, g_p->x_max, pml_p->pmly, g_p->z_max);
    }
    else
    {
        int ymin = g_p->j_offset;
        int ymax = g_p->j_offset+g_p->jmax;
        int pml_min = pml_p->pmly;
        int pml_max = g_p->y_full-1-pml_p->pmly;

        if (ymin < pml_min && ymax > pml_min)
        {
            init_field3d(pml_p->pml_f.Exz_y_l, g_p->x_max, pml_min-ymin, g_p->z_max);
            init_field3d(pml_p->pml_f.Exy_y_l, g_p->x_max, pml_min-ymin, g_p->z_max);
            init_field3d(pml_p->pml_f.Ezy_y_l, g_p->x_max, pml_min-ymin, g_p->z_max);
            init_field3d(pml_p->pml_f.Ezx_y_l, g_p->x_max, pml_min-ymin, g_p->z_max);

            init_field3d(pml_p->pml_f.Hxy_y_l, g_p->x_max, pml_min-ymin, g_p->z_max);
            init_field3d(pml_p->pml_f.Hxz_y_l, g_p->x_max, pml_min-ymin, g_p->z_max);
            init_field3d(pml_p->pml_f.Hzy_y_l, g_p->x_max, pml_min-ymin, g_p->z_max);
            init_field3d(pml_p->pml_f.Hzx_y_l, g_p->x_max, pml_min-ymin, g_p->z_max);

            init_field3d(pml_p->pml_f.Exz_y_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Exy_y_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Ezy_y_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Ezx_y_h, 0, 0, 0);

            init_field3d(pml_p->pml_f.Hxy_y_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hxz_y_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hzy_y_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hzx_y_h, 0, 0, 0);
        }
        else if (ymax > pml_max && ymin < pml_max)
        {
            init_field3d(pml_p->pml_f.Exz_y_h, g_p->x_max, ymax-pml_max, g_p->z_max);
            init_field3d(pml_p->pml_f.Exy_y_h, g_p->x_max, ymax-pml_max, g_p->z_max);
            init_field3d(pml_p->pml_f.Ezy_y_h, g_p->x_max, ymax-pml_max, g_p->z_max);
            init_field3d(pml_p->pml_f.Ezx_y_h, g_p->x_max, ymax-pml_max, g_p->z_max);

            init_field3d(pml_p->pml_f.Hxy_y_h, g_p->x_max, ymax-pml_max, g_p->z_max);
            init_field3d(pml_p->pml_f.Hxz_y_h, g_p->x_max, ymax-pml_max, g_p->z_max);
            init_field3d(pml_p->pml_f.Hzy_y_h, g_p->x_max, ymax-pml_max, g_p->z_max);
            init_field3d(pml_p->pml_f.Hzx_y_h, g_p->x_max, ymax-pml_max, g_p->z_max);

            init_field3d(pml_p->pml_f.Exz_y_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Exy_y_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Ezy_y_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Ezx_y_l, 0, 0, 0);

            init_field3d(pml_p->pml_f.Hxy_y_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hxz_y_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hzy_y_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hzx_y_l, 0, 0, 0);

        }
        else if (ymax < pml_min)
        {
            init_field3d(pml_p->pml_f.Exz_y_l, g_p->x_max, g_p->y_max-1, g_p->z_max);
            init_field3d(pml_p->pml_f.Exy_y_l, g_p->x_max, g_p->y_max-1, g_p->z_max);
            init_field3d(pml_p->pml_f.Ezy_y_l, g_p->x_max, g_p->y_max-1, g_p->z_max);
            init_field3d(pml_p->pml_f.Ezx_y_l, g_p->x_max, g_p->y_max-1, g_p->z_max);

            init_field3d(pml_p->pml_f.Hxy_y_l, g_p->x_max, g_p->y_max-1, g_p->z_max);
            init_field3d(pml_p->pml_f.Hxz_y_l, g_p->x_max, g_p->y_max-1, g_p->z_max);
            init_field3d(pml_p->pml_f.Hzy_y_l, g_p->x_max, g_p->y_max-1, g_p->z_max);
            init_field3d(pml_p->pml_f.Hzx_y_l, g_p->x_max, g_p->y_max-1, g_p->z_max);

            init_field3d(pml_p->pml_f.Exz_y_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Exy_y_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Ezy_y_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Ezx_y_h, 0, 0, 0);

            init_field3d(pml_p->pml_f.Hxy_y_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hxz_y_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hzy_y_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hzx_y_h, 0, 0, 0);
        }
        else if (ymin > pml_max)
        {
            init_field3d(pml_p->pml_f.Exz_y_h, g_p->x_max, g_p->y_max-1, g_p->z_max);
            init_field3d(pml_p->pml_f.Exy_y_h, g_p->x_max, g_p->y_max-1, g_p->z_max);
            init_field3d(pml_p->pml_f.Ezy_y_h, g_p->x_max, g_p->y_max-1, g_p->z_max);
            init_field3d(pml_p->pml_f.Ezx_y_h, g_p->x_max, g_p->y_max-1, g_p->z_max);

            init_field3d(pml_p->pml_f.Hxy_y_h, g_p->x_max, g_p->y_max-1, g_p->z_max);
            init_field3d(pml_p->pml_f.Hxz_y_h, g_p->x_max, g_p->y_max-1, g_p->z_max);
            init_field3d(pml_p->pml_f.Hzy_y_h, g_p->x_max, g_p->y_max-1, g_p->z_max);
            init_field3d(pml_p->pml_f.Hzx_y_h, g_p->x_max, g_p->y_max-1, g_p->z_max);

            init_field3d(pml_p->pml_f.Exz_y_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Exy_y_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Ezy_y_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Ezx_y_l, 0, 0, 0);

            init_field3d(pml_p->pml_f.Hxy_y_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hxz_y_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hzy_y_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hzx_y_l, 0, 0, 0);
        }
        else
        {
            /* simply to avoid warnings during compilation */
            init_field3d(pml_p->pml_f.Exz_y_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Exy_y_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Ezy_y_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Ezx_y_l, 0, 0, 0);

            init_field3d(pml_p->pml_f.Hxy_y_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hxz_y_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hzy_y_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hzx_y_l, 0, 0, 0);

            init_field3d(pml_p->pml_f.Exz_y_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Exy_y_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Ezy_y_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Ezx_y_h, 0, 0, 0);

            init_field3d(pml_p->pml_f.Hxy_y_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hxz_y_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hzy_y_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hzx_y_h, 0, 0, 0);
        }
    }

    if (mpi.num_proc_z == 1)
    {
        init_field3d(pml_p->pml_f.Hxz_z_l, g_p->x_max, g_p->y_max, pml_p->pmlz);
        init_field3d(pml_p->pml_f.Hxy_z_l, g_p->x_max, g_p->y_max, pml_p->pmlz);
        init_field3d(pml_p->pml_f.Hyz_z_l, g_p->x_max, g_p->y_max, pml_p->pmlz);
        init_field3d(pml_p->pml_f.Hyx_z_l, g_p->x_max, g_p->y_max, pml_p->pmlz);

        init_field3d(pml_p->pml_f.Exy_z_l, g_p->x_max, g_p->y_max, pml_p->pmlz);
        init_field3d(pml_p->pml_f.Exz_z_l, g_p->x_max, g_p->y_max, pml_p->pmlz);
        init_field3d(pml_p->pml_f.Eyz_z_l, g_p->x_max, g_p->y_max, pml_p->pmlz);
        init_field3d(pml_p->pml_f.Eyx_z_l, g_p->x_max, g_p->y_max, pml_p->pmlz);

        init_field3d(pml_p->pml_f.Hxz_z_h, g_p->x_max, g_p->y_max, pml_p->pmlz);
        init_field3d(pml_p->pml_f.Hxy_z_h, g_p->x_max, g_p->y_max, pml_p->pmlz);
        init_field3d(pml_p->pml_f.Hyz_z_h, g_p->x_max, g_p->y_max, pml_p->pmlz);
        init_field3d(pml_p->pml_f.Hyx_z_h, g_p->x_max, g_p->y_max, pml_p->pmlz);

        init_field3d(pml_p->pml_f.Exy_z_h, g_p->x_max, g_p->y_max, pml_p->pmlz);
        init_field3d(pml_p->pml_f.Exz_z_h, g_p->x_max, g_p->y_max, pml_p->pmlz);
        init_field3d(pml_p->pml_f.Eyz_z_h, g_p->x_max, g_p->y_max, pml_p->pmlz);
        init_field3d(pml_p->pml_f.Eyx_z_h, g_p->x_max, g_p->y_max, pml_p->pmlz);
    }
    else
    {
        int zmin = g_p->k_offset;
        int zmax = g_p->k_offset+g_p->kmax;
        int pml_min = pml_p->pmlz;
        int pml_max = g_p->z_full-1-pml_p->pmlz;

        if (zmin < pml_min && zmax > pml_min)
        {
            init_field3d(pml_p->pml_f.Hxz_z_l, g_p->x_max, g_p->y_max, pml_min-zmin);
            init_field3d(pml_p->pml_f.Hxy_z_l, g_p->x_max, g_p->y_max, pml_min-zmin);
            init_field3d(pml_p->pml_f.Hyz_z_l, g_p->x_max, g_p->y_max, pml_min-zmin);
            init_field3d(pml_p->pml_f.Hyx_z_l, g_p->x_max, g_p->y_max, pml_min-zmin);

            init_field3d(pml_p->pml_f.Exy_z_l, g_p->x_max, g_p->y_max, pml_min-zmin);
            init_field3d(pml_p->pml_f.Exz_z_l, g_p->x_max, g_p->y_max, pml_min-zmin);
            init_field3d(pml_p->pml_f.Eyz_z_l, g_p->x_max, g_p->y_max, pml_min-zmin);
            init_field3d(pml_p->pml_f.Eyx_z_l, g_p->x_max, g_p->y_max, pml_min-zmin);

            init_field3d(pml_p->pml_f.Hxz_z_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hxy_z_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hyz_z_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hyx_z_h, 0, 0, 0);

            init_field3d(pml_p->pml_f.Exy_z_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Exz_z_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Eyz_z_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Eyx_z_h, 0, 0, 0);
        }
        else if (zmax > pml_max && zmin < pml_max)
        {
            init_field3d(pml_p->pml_f.Hxz_z_h, g_p->x_max, g_p->y_max, zmax-pml_max);
            init_field3d(pml_p->pml_f.Hxy_z_h, g_p->x_max, g_p->y_max, zmax-pml_max);
            init_field3d(pml_p->pml_f.Hyz_z_h, g_p->x_max, g_p->y_max, zmax-pml_max);
            init_field3d(pml_p->pml_f.Hyx_z_h, g_p->x_max, g_p->y_max, zmax-pml_max);

            init_field3d(pml_p->pml_f.Exy_z_h, g_p->x_max, g_p->y_max, zmax-pml_max);
            init_field3d(pml_p->pml_f.Exz_z_h, g_p->x_max, g_p->y_max, zmax-pml_max);
            init_field3d(pml_p->pml_f.Eyz_z_h, g_p->x_max, g_p->y_max, zmax-pml_max);
            init_field3d(pml_p->pml_f.Eyx_z_h, g_p->x_max, g_p->y_max, zmax-pml_max);

            init_field3d(pml_p->pml_f.Hxz_z_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hxy_z_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hyz_z_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hyx_z_l, 0, 0, 0);

            init_field3d(pml_p->pml_f.Exy_z_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Exz_z_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Eyz_z_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Eyx_z_l, 0, 0, 0);
        }
        else if (zmax < pml_min)
        {
            init_field3d(pml_p->pml_f.Hxz_z_l, g_p->x_max, g_p->y_max, g_p->z_max);
            init_field3d(pml_p->pml_f.Hxy_z_l, g_p->x_max, g_p->y_max, g_p->z_max);
            init_field3d(pml_p->pml_f.Hyz_z_l, g_p->x_max, g_p->y_max, g_p->z_max);
            init_field3d(pml_p->pml_f.Hyx_z_l, g_p->x_max, g_p->y_max, g_p->z_max);

            init_field3d(pml_p->pml_f.Exy_z_l, g_p->x_max, g_p->y_max, g_p->z_max);
            init_field3d(pml_p->pml_f.Exz_z_l, g_p->x_max, g_p->y_max, g_p->z_max);
            init_field3d(pml_p->pml_f.Eyz_z_l, g_p->x_max, g_p->y_max, g_p->z_max);
            init_field3d(pml_p->pml_f.Eyx_z_l, g_p->x_max, g_p->y_max, g_p->z_max);

            init_field3d(pml_p->pml_f.Hxz_z_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hxy_z_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hyz_z_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hyx_z_h, 0, 0, 0);

            init_field3d(pml_p->pml_f.Exy_z_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Exz_z_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Eyz_z_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Eyx_z_h, 0, 0, 0);
        }
        else if (zmin > pml_max)
        {
            init_field3d(pml_p->pml_f.Hxz_z_h, g_p->x_max, g_p->y_max, g_p->z_max);
            init_field3d(pml_p->pml_f.Hxy_z_h, g_p->x_max, g_p->y_max, g_p->z_max);
            init_field3d(pml_p->pml_f.Hyz_z_h, g_p->x_max, g_p->y_max, g_p->z_max);
            init_field3d(pml_p->pml_f.Hyx_z_h, g_p->x_max, g_p->y_max, g_p->z_max);

            init_field3d(pml_p->pml_f.Exy_z_h, g_p->x_max, g_p->y_max, g_p->z_max);
            init_field3d(pml_p->pml_f.Exz_z_h, g_p->x_max, g_p->y_max, g_p->z_max);
            init_field3d(pml_p->pml_f.Eyz_z_h, g_p->x_max, g_p->y_max, g_p->z_max);
            init_field3d(pml_p->pml_f.Eyx_z_h, g_p->x_max, g_p->y_max, g_p->z_max);

            init_field3d(pml_p->pml_f.Hxz_z_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hxy_z_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hyz_z_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hyx_z_l, 0, 0, 0);

            init_field3d(pml_p->pml_f.Exy_z_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Exz_z_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Eyz_z_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Eyx_z_l, 0, 0, 0);
        }
        else
        {
            /* simply to avoid warnings during compilation */
            init_field3d(pml_p->pml_f.Hxz_z_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hxy_z_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hyz_z_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hyx_z_l, 0, 0, 0);

            init_field3d(pml_p->pml_f.Exy_z_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Exz_z_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Eyz_z_l, 0, 0, 0);
            init_field3d(pml_p->pml_f.Eyx_z_l, 0, 0, 0);

            init_field3d(pml_p->pml_f.Hxz_z_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hxy_z_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hyz_z_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Hyx_z_h, 0, 0, 0);

            init_field3d(pml_p->pml_f.Exy_z_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Exz_z_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Eyz_z_h, 0, 0, 0);
            init_field3d(pml_p->pml_f.Eyx_z_h, 0, 0, 0);
        }
    }
#else
    init_field3d(pml_p->pml_f.Eyx_x_l, pml_p->pmlx, g_p->y_max, g_p->z_max);
    init_field3d(pml_p->pml_f.Eyz_x_l, pml_p->pmlx, g_p->y_max, g_p->z_max);
    init_field3d(pml_p->pml_f.Ezx_x_l, pml_p->pmlx, g_p->y_max, g_p->z_max);
    init_field3d(pml_p->pml_f.Ezy_x_l, pml_p->pmlx, g_p->y_max, g_p->z_max);

    init_field3d(pml_p->pml_f.Hyx_x_l, pml_p->pmlx, g_p->y_max, g_p->z_max);
    init_field3d(pml_p->pml_f.Hyz_x_l, pml_p->pmlx, g_p->y_max, g_p->z_max);
    init_field3d(pml_p->pml_f.Hzx_x_l, pml_p->pmlx, g_p->y_max, g_p->z_max);
    init_field3d(pml_p->pml_f.Hzy_x_l, pml_p->pmlx, g_p->y_max, g_p->z_max);

    init_field3d(pml_p->pml_f.Eyx_x_h, pml_p->pmlx, g_p->y_max, g_p->z_max);
    init_field3d(pml_p->pml_f.Eyz_x_h, pml_p->pmlx, g_p->y_max, g_p->z_max);
    init_field3d(pml_p->pml_f.Ezx_x_h, pml_p->pmlx, g_p->y_max, g_p->z_max);
    init_field3d(pml_p->pml_f.Ezy_x_h, pml_p->pmlx, g_p->y_max, g_p->z_max);

    init_field3d(pml_p->pml_f.Hyx_x_h, pml_p->pmlx, g_p->y_max, g_p->z_max);
    init_field3d(pml_p->pml_f.Hyz_x_h, pml_p->pmlx, g_p->y_max, g_p->z_max);
    init_field3d(pml_p->pml_f.Hzx_x_h, pml_p->pmlx, g_p->y_max, g_p->z_max);
    init_field3d(pml_p->pml_f.Hzy_x_h, pml_p->pmlx, g_p->y_max, g_p->z_max);

    init_field3d(pml_p->pml_f.Exz_y_l, g_p->x_max, pml_p->pmly, g_p->z_max);
    init_field3d(pml_p->pml_f.Exy_y_l, g_p->x_max, pml_p->pmly, g_p->z_max);
    init_field3d(pml_p->pml_f.Ezy_y_l, g_p->x_max, pml_p->pmly, g_p->z_max);
    init_field3d(pml_p->pml_f.Ezx_y_l, g_p->x_max, pml_p->pmly, g_p->z_max);

    init_field3d(pml_p->pml_f.Hxy_y_l, g_p->x_max, pml_p->pmly, g_p->z_max);
    init_field3d(pml_p->pml_f.Hxz_y_l, g_p->x_max, pml_p->pmly, g_p->z_max);
    init_field3d(pml_p->pml_f.Hzy_y_l, g_p->x_max, pml_p->pmly, g_p->z_max);
    init_field3d(pml_p->pml_f.Hzx_y_l, g_p->x_max, pml_p->pmly, g_p->z_max);

    init_field3d(pml_p->pml_f.Exz_y_h, g_p->x_max, pml_p->pmly, g_p->z_max);
    init_field3d(pml_p->pml_f.Exy_y_h, g_p->x_max, pml_p->pmly, g_p->z_max);
    init_field3d(pml_p->pml_f.Ezy_y_h, g_p->x_max, pml_p->pmly, g_p->z_max);
    init_field3d(pml_p->pml_f.Ezx_y_h, g_p->x_max, pml_p->pmly, g_p->z_max);

    init_field3d(pml_p->pml_f.Hxy_y_h, g_p->x_max, pml_p->pmly, g_p->z_max);
    init_field3d(pml_p->pml_f.Hxz_y_h, g_p->x_max, pml_p->pmly, g_p->z_max);
    init_field3d(pml_p->pml_f.Hzy_y_h, g_p->x_max, pml_p->pmly, g_p->z_max);
    init_field3d(pml_p->pml_f.Hzx_y_h, g_p->x_max, pml_p->pmly, g_p->z_max);

    init_field3d(pml_p->pml_f.Hxz_z_l, g_p->x_max, g_p->y_max, pml_p->pmlz);
    init_field3d(pml_p->pml_f.Hxy_z_l, g_p->x_max, g_p->y_max, pml_p->pmlz);
    init_field3d(pml_p->pml_f.Hyz_z_l, g_p->x_max, g_p->y_max, pml_p->pmlz);
    init_field3d(pml_p->pml_f.Hyx_z_l, g_p->x_max, g_p->y_max, pml_p->pmlz);

    init_field3d(pml_p->pml_f.Exy_z_l, g_p->x_max, g_p->y_max, pml_p->pmlz);
    init_field3d(pml_p->pml_f.Exz_z_l, g_p->x_max, g_p->y_max, pml_p->pmlz);
    init_field3d(pml_p->pml_f.Eyz_z_l, g_p->x_max, g_p->y_max, pml_p->pmlz);
    init_field3d(pml_p->pml_f.Eyx_z_l, g_p->x_max, g_p->y_max, pml_p->pmlz);

    init_field3d(pml_p->pml_f.Hxz_z_h, g_p->x_max, g_p->y_max, pml_p->pmlz);
    init_field3d(pml_p->pml_f.Hxy_z_h, g_p->x_max, g_p->y_max, pml_p->pmlz);
    init_field3d(pml_p->pml_f.Hyz_z_h, g_p->x_max, g_p->y_max, pml_p->pmlz);
    init_field3d(pml_p->pml_f.Hyx_z_h, g_p->x_max, g_p->y_max, pml_p->pmlz);

    init_field3d(pml_p->pml_f.Exy_z_h, g_p->x_max, g_p->y_max, pml_p->pmlz);
    init_field3d(pml_p->pml_f.Exz_z_h, g_p->x_max, g_p->y_max, pml_p->pmlz);
    init_field3d(pml_p->pml_f.Eyz_z_h, g_p->x_max, g_p->y_max, pml_p->pmlz);
    init_field3d(pml_p->pml_f.Eyx_z_h, g_p->x_max, g_p->y_max, pml_p->pmlz);
#endif
#endif /* FULL_UPDATE */
}

void destroy_pml_fields_2d(pml_data* pml_p)
{

    destroy_field(pml_p->pml_f.Eyx_x_l);
    destroy_field(pml_p->pml_f.Eyz_x_l);
    destroy_field(pml_p->pml_f.Eyx_x_h);
    destroy_field(pml_p->pml_f.Eyz_x_h);
    destroy_field(pml_p->pml_f.Eyz_z_l);
    destroy_field(pml_p->pml_f.Eyx_z_l);
    destroy_field(pml_p->pml_f.Eyz_z_h);
    destroy_field(pml_p->pml_f.Eyx_z_h);

    free_int(pml_p->idxe);
    free_fp(pml_p->aex);
    free_fp(pml_p->bex);
    free_int(pml_p->idxh);
    free_fp(pml_p->ahx);
    free_fp(pml_p->bhx);
    free_fp(pml_p->Eyx);
    free_fp(pml_p->Eyz);

    free(pml_p->pml_f.Eyx_x_l);
    free(pml_p->pml_f.Eyz_x_l);
    free(pml_p->pml_f.Eyx_x_h);
    free(pml_p->pml_f.Eyz_x_h);
    free(pml_p->pml_f.Eyz_z_l);
    free(pml_p->pml_f.Eyx_z_l);
    free(pml_p->pml_f.Eyz_z_h);
    free(pml_p->pml_f.Eyx_z_h);
}

void destroy_pml_fields_3d(pml_data* pml_p)
{
#ifdef FULL_UPDATE
    free_int(pml_p->idex);
    free_int(pml_p->idhx);
    free_int(pml_p->idexf);
    free_int(pml_p->idhxf);
    free_int(pml_p->idey);
    free_int(pml_p->idhy);
    free_int(pml_p->ideyf);
    free_int(pml_p->idhyf);
    free_int(pml_p->idez);
    free_int(pml_p->idhz);
    free_int(pml_p->idezf);
    free_int(pml_p->idhzf);

    free_fp(pml_p->aexy);
    free_fp(pml_p->bexy);
    free_fp(pml_p->aexz);
    free_fp(pml_p->bexz);

    free_fp(pml_p->aeyx);
    free_fp(pml_p->beyx);
    free_fp(pml_p->aeyz);
    free_fp(pml_p->beyz);

    free_fp(pml_p->aezx);
    free_fp(pml_p->bezx);
    free_fp(pml_p->aezy);
    free_fp(pml_p->bezy);

    free_fp(pml_p->ahxy);
    free_fp(pml_p->bhxy);
    free_fp(pml_p->ahxz);
    free_fp(pml_p->bhxz);

    free_fp(pml_p->ahyx);
    free_fp(pml_p->bhyx);
    free_fp(pml_p->ahyz);
    free_fp(pml_p->bhyz);

    free_fp(pml_p->ahzx);
    free_fp(pml_p->bhzx);
    free_fp(pml_p->ahzy);
    free_fp(pml_p->bhzy);
    
    free_fp(pml_p->Exy);
    free_fp(pml_p->Exz);
    free_fp(pml_p->Eyx);
    free_fp(pml_p->Eyz);
    free_fp(pml_p->Ezx);
    free_fp(pml_p->Ezy);

    free_fp(pml_p->Hxy);
    free_fp(pml_p->Hxz);
    free_fp(pml_p->Hyx);
    free_fp(pml_p->Hyz);
    free_fp(pml_p->Hzx);
    free_fp(pml_p->Hzy);
#else /* FULL_UPDATE */
    destroy_field(pml_p->pml_f.Exy_y_l);
    destroy_field(pml_p->pml_f.Exy_y_h);
    destroy_field(pml_p->pml_f.Exy_z_l);
    destroy_field(pml_p->pml_f.Exy_z_h);
    destroy_field(pml_p->pml_f.Exz_y_l);
    destroy_field(pml_p->pml_f.Exz_y_h);
    destroy_field(pml_p->pml_f.Exz_z_l);
    destroy_field(pml_p->pml_f.Exz_z_h);
    destroy_field(pml_p->pml_f.Eyx_x_l);
    destroy_field(pml_p->pml_f.Eyx_x_h);
    destroy_field(pml_p->pml_f.Eyx_z_l);
    destroy_field(pml_p->pml_f.Eyx_z_h);
    destroy_field(pml_p->pml_f.Eyz_x_l);
    destroy_field(pml_p->pml_f.Eyz_x_h);
    destroy_field(pml_p->pml_f.Eyz_z_l);
    destroy_field(pml_p->pml_f.Eyz_z_h);
    destroy_field(pml_p->pml_f.Ezy_y_l);
    destroy_field(pml_p->pml_f.Ezy_y_h);
    destroy_field(pml_p->pml_f.Ezy_x_l);
    destroy_field(pml_p->pml_f.Ezy_x_h);
    destroy_field(pml_p->pml_f.Ezx_y_l);
    destroy_field(pml_p->pml_f.Ezx_y_h);
    destroy_field(pml_p->pml_f.Ezx_x_l);
    destroy_field(pml_p->pml_f.Ezx_x_h);

    free(pml_p->pml_f.Exy_y_l);
    free(pml_p->pml_f.Exy_y_h);
    free(pml_p->pml_f.Exy_z_l);
    free(pml_p->pml_f.Exy_z_h);
    free(pml_p->pml_f.Exz_y_l);
    free(pml_p->pml_f.Exz_y_h);
    free(pml_p->pml_f.Exz_z_l);
    free(pml_p->pml_f.Exz_z_h);
    free(pml_p->pml_f.Eyx_x_l);
    free(pml_p->pml_f.Eyx_x_h);
    free(pml_p->pml_f.Eyx_z_l);
    free(pml_p->pml_f.Eyx_z_h);
    free(pml_p->pml_f.Eyz_x_l);
    free(pml_p->pml_f.Eyz_x_h);
    free(pml_p->pml_f.Eyz_z_l);
    free(pml_p->pml_f.Eyz_z_h);
    free(pml_p->pml_f.Ezy_y_l);
    free(pml_p->pml_f.Ezy_y_h);
    free(pml_p->pml_f.Ezy_x_l);
    free(pml_p->pml_f.Ezy_x_h);
    free(pml_p->pml_f.Ezx_y_l);
    free(pml_p->pml_f.Ezx_y_h);
    free(pml_p->pml_f.Ezx_x_l);
    free(pml_p->pml_f.Ezx_x_h);

    destroy_field(pml_p->pml_f.Hxy_y_l);
    destroy_field(pml_p->pml_f.Hxy_y_h);
    destroy_field(pml_p->pml_f.Hxy_z_l);
    destroy_field(pml_p->pml_f.Hxy_z_h);
    destroy_field(pml_p->pml_f.Hxz_y_l);
    destroy_field(pml_p->pml_f.Hxz_y_h);
    destroy_field(pml_p->pml_f.Hxz_z_l);
    destroy_field(pml_p->pml_f.Hxz_z_h);
    destroy_field(pml_p->pml_f.Hyx_x_l);
    destroy_field(pml_p->pml_f.Hyx_x_h);
    destroy_field(pml_p->pml_f.Hyx_z_l);
    destroy_field(pml_p->pml_f.Hyx_z_h);
    destroy_field(pml_p->pml_f.Hyz_x_l);
    destroy_field(pml_p->pml_f.Hyz_x_h);
    destroy_field(pml_p->pml_f.Hyz_z_l);
    destroy_field(pml_p->pml_f.Hyz_z_h);
    destroy_field(pml_p->pml_f.Hzy_y_l);
    destroy_field(pml_p->pml_f.Hzy_y_h);
    destroy_field(pml_p->pml_f.Hzy_x_l);
    destroy_field(pml_p->pml_f.Hzy_x_h);
    destroy_field(pml_p->pml_f.Hzx_y_l);
    destroy_field(pml_p->pml_f.Hzx_y_h);
    destroy_field(pml_p->pml_f.Hzx_x_l);
    destroy_field(pml_p->pml_f.Hzx_x_h);

    free(pml_p->pml_f.Hxy_y_l);
    free(pml_p->pml_f.Hxy_y_h);
    free(pml_p->pml_f.Hxy_z_l);
    free(pml_p->pml_f.Hxy_z_h);
    free(pml_p->pml_f.Hxz_y_l);
    free(pml_p->pml_f.Hxz_y_h);
    free(pml_p->pml_f.Hxz_z_l);
    free(pml_p->pml_f.Hxz_z_h);
    free(pml_p->pml_f.Hyx_x_l);
    free(pml_p->pml_f.Hyx_x_h);
    free(pml_p->pml_f.Hyx_z_l);
    free(pml_p->pml_f.Hyx_z_h);
    free(pml_p->pml_f.Hyz_x_l);
    free(pml_p->pml_f.Hyz_x_h);
    free(pml_p->pml_f.Hyz_z_l);
    free(pml_p->pml_f.Hyz_z_h);
    free(pml_p->pml_f.Hzy_y_l);
    free(pml_p->pml_f.Hzy_y_h);
    free(pml_p->pml_f.Hzy_x_l);
    free(pml_p->pml_f.Hzy_x_h);
    free(pml_p->pml_f.Hzx_y_l);
    free(pml_p->pml_f.Hzx_y_h);
    free(pml_p->pml_f.Hzx_x_l);
    free(pml_p->pml_f.Hzx_x_h);
#endif /* FULL_UPDATE */
}

void destroy_pml2d(pml_data* pml_p)
{
    free(pml_p->c_ex);
    free(pml_p->c_ez);
    free(pml_p->c_hx);
    free(pml_p->c_hz);
    free(pml_p->d_ex);
    free(pml_p->d_ez);
    free(pml_p->d_hx);
    free(pml_p->d_hz);
    destroy_pml_fields_2d(pml_p);
}

void destroy_pml3d(pml_data* pml_p)
{
    free(pml_p->c_ex);
    free(pml_p->c_ey);
    free(pml_p->c_ez);
    free(pml_p->c_hx);
    free(pml_p->c_hy);
    free(pml_p->c_hz);
    free(pml_p->d_ex);
    free(pml_p->d_ey);
    free(pml_p->d_ez);
    free(pml_p->d_hx);
    free(pml_p->d_hy);
    free(pml_p->d_hz);
    destroy_pml_fields_3d(pml_p);
#ifdef CUDA
    destroy_cuda_pml_3d(pml_p);
#endif
}
