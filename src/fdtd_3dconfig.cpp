#include "fdtd.h"

static void update_moving_window_3d(vfield_n* D_p, vfield_n* E_p,
				    vfield_n* H_p, vfield_n* J_p,
				    geometry_data* g_p,
				    time_scheme* t_p,
				    media_data* m_p,
				    pml_data* p_p,
				    points_data_3d* pd_p,
				    sources_rt_data* s_p,
				    scheme_constants* sch_p,
				    int t,
				    const char* pathPrefix);

static void update_moving_front_3d(geometry_data* g_p,
			    time_scheme* t_p,
			    int t);

static void update_3d_dx(vfield_n* vd_p,
			 vfield_n* vh_p,
			 vfield_n* vj_p,
			 geometry_data* geometry_p,
			 pml_data* pml_p,
			 scheme_constants* sch_p);

static void update_3d_dy(vfield_n* vd_p,
			 vfield_n* vh_p,
			 vfield_n* vj_p,
			 geometry_data* geometry_p,
			 pml_data* pml_p,
			 scheme_constants* sch_p);

static void update_3d_dz(vfield_n* vd_p,
			 vfield_n* vh_p,
			 vfield_n* vj_p,
			 geometry_data* geometry_p,
			 pml_data* pml_p,
			 scheme_constants* sch_p);

static void update_3d_hx(vfield_n* vd_p,
			 vfield_n* vh_p,
			 geometry_data* geometry_p,
			 pml_data* pml_p,
			 scheme_constants* sch_p);

static void update_3d_hy(vfield_n* vd_p,
			 vfield_n* vh_p,
			 geometry_data* geometry_p,
			 pml_data* pml_p,
			 scheme_constants* sch_p);

static void update_3d_hz(vfield_n* vd_p,
			 vfield_n* vh_p,
			 geometry_data* geometry_p,
			 pml_data* pml_p,
			 scheme_constants* sch_p);

static void add_sources_3d_e(vfield_n* vf_p,
			     sources_rt_data* s_p,
			     scheme_constants* sch_p);

static void add_sources_3d_h(vfield_n* vf_p,
			     sources_rt_data* s_p,
			     scheme_constants* sch_p);

static void add_pulses_3d_e(vfield_n* vf_p,
			    sources_rt_data* s_p,
			    scheme_constants* sch_p);

static void add_pulses_3d_h(vfield_n* vf_p,
			    sources_rt_data* s_p,
			    scheme_constants* sch_p);

static void add_tfsf_3d_e(vfield_n* vf_p,
			  sources_rt_data* s_p,
			  scheme_constants* sch_p);

static void add_tfsf_3d_h(vfield_n* vf_p,
			  sources_rt_data* s_p,
			  scheme_constants* sch_p);

void add_sources_3d_e(vfield_n* vf_p,
		      sources_rt_data* s_p,
		      scheme_constants* sch_p)
{
    add_pulses_3d_e(vf_p, s_p, sch_p);
    add_tfsf_3d_e(vf_p, s_p, sch_p);
}

void add_sources_3d_h(vfield_n* vf_p,
                      sources_rt_data* s_p,
                      scheme_constants* sch_p)
{
    add_pulses_3d_h(vf_p, s_p, sch_p);
    add_tfsf_3d_h(vf_p, s_p, sch_p);
}

void add_pulses_3d_e(vfield_n* vf_p,
                     sources_rt_data* s_p,
                     scheme_constants* sch_p)
{
    field_n* Ex_p = vf_p->comp_p[x_e];
    field_n* Ey_p = vf_p->comp_p[y_e];

    if (s_p->pulse_enable == true)
    {
        if (s_p->pulse_zmin == true)
        {
/*            for (int i=0; i<s_p->Jey_zmin_p->nx; i++)
            {
                for (int j=0; j<s_p->Jey_zmin_p->ny; j++)
                {
                    int index = INDEX_P(Ey_p,i,j, s_p->pulse_z_min);
                    int index2d = INDEX_XY_P(s_p->Jey_zmin_p,i,j);
                    Ey_p->a[index] += sch_p->dt_4_pi*s_p->Jey_zmin_p->a[index2d];
                    Ex_p->a[index] += sch_p->dt_4_pi*s_p->Jex_zmin_p->a[index2d];
                }
		}*/
	    for (int k = 0; k < s_p->z_num; k++)
	    {
		int idx = s_p->je_zmin_idx[k];
		Ey_p->a[idx] += sch_p->dt_4_pi*s_p->Jey_zmin_p->a[k];
		Ex_p->a[idx] += sch_p->dt_4_pi*s_p->Jex_zmin_p->a[k];
	    }
        }

        if (s_p->pulse_zmax == true)
        {
/*            for (int i=0; i<s_p->Jey_zmax_p->nx; i++)
            {
                for (int j=0; j<s_p->Jey_zmax_p->ny; j++)
                {
                    int index = INDEX_P(Ey_p,i,j, s_p->pulse_z_max);
                    int index2d = INDEX_XY_P(s_p->Jey_zmax_p,i,j);
                    Ey_p->a[index] += sch_p->dt_4_pi*s_p->Jey_zmax_p->a[index2d];
                    Ex_p->a[index] += sch_p->dt_4_pi*s_p->Jex_zmax_p->a[index2d];
                }
		}*/
	    for (int k = 0; k < s_p->z_num; k++)
	    {
		int idx = s_p->je_zmax_idx[k];
		Ey_p->a[idx] += sch_p->dt_4_pi*s_p->Jey_zmax_p->a[k];
		Ex_p->a[idx] += sch_p->dt_4_pi*s_p->Jex_zmax_p->a[k];
	    }
        }
    }
}

void add_tfsf_3d_e(vfield_n* vf_p,
                   sources_rt_data* s_p,
                   scheme_constants* sch_p)
{
    field_n* Ex_p = vf_p->comp_p[x_e];
    field_n* Ey_p = vf_p->comp_p[y_e];
    field_n* Ez_p = vf_p->comp_p[z_e];

    if (s_p->tfsf_enable == true)
    {
        if (s_p->tfsf_zmin == true)
        {
            /*for (int i=0; i<s_p->tfsf_hy_zmin_p->nx; i++)
            {
                for (int j=0; j<s_p->tfsf_hy_zmin_p->ny; j++)
                {
                    int index = INDEX_P(Ex_p, i+s_p->x_min, j+s_p->y_min,s_p->z_min);
                    int index2d = INDEX_XY_P(s_p->tfsf_hy_zmin_p, i,j);
                    Ex_p->a[index] += sch_p->dt_dz*s_p->tfsf_hy_zmin_p->a[index2d];
                    Ey_p->a[index] -= sch_p->dt_dz*s_p->tfsf_hx_zmin_p->a[index2d];
                }
		}*/
	    for (int k=0; k<s_p->wz_num; k++)
	    {
		int idx = s_p->we_zmin_idx[k];
		Ex_p->a[idx] += sch_p->dt_dz*s_p->tfsf_hy_zmin_p->a[k];
		Ey_p->a[idx] -= sch_p->dt_dz*s_p->tfsf_hx_zmin_p->a[k];
	    }
        }

        if (s_p->tfsf_zmax == true)
        {
/*            for (int i=0; i<s_p->tfsf_hy_zmax_p->nx; i++)
            {
                for (int j=0; j<s_p->tfsf_hy_zmax_p->ny; j++)
                {
                    int index = INDEX_P(Ex_p, i+s_p->x_min, j+s_p->y_min,s_p->z_max);
                    int index2d = INDEX_XY_P(s_p->tfsf_hy_zmax_p, i,j);
                    Ex_p->a[index] -= sch_p->dt_dz*s_p->tfsf_hy_zmax_p->a[index2d];
                    Ey_p->a[index] += sch_p->dt_dz*s_p->tfsf_hx_zmax_p->a[index2d];
                }
		}*/
	    for (int k=0; k<s_p->wz_num; k++)
	    {
		int idx = s_p->we_zmax_idx[k];
		Ex_p->a[idx] -= sch_p->dt_dz*s_p->tfsf_hy_zmax_p->a[k];
		Ey_p->a[idx] += sch_p->dt_dz*s_p->tfsf_hx_zmax_p->a[k];
	    }
        }

        if (s_p->tfsf_ymin == true)
        {
/*            for (int i=0; i<s_p->tfsf_hz_ymin_p->nx; i++)
            {
                for (int k=0; k<s_p->tfsf_hz_ymin_p->ny; k++)
                {
                    int index = INDEX_P(Ex_p, i+s_p->x_min, s_p->y_min, k+s_p->z_min);
                    int index2d = INDEX_XY_P(s_p->tfsf_hz_ymin_p, i,k);
                    Ex_p->a[index] -= sch_p->dt_dy*s_p->tfsf_hz_ymin_p->a[index2d];
                    Ez_p->a[index] += sch_p->dt_dy*s_p->tfsf_hx_ymin_p->a[index2d];
                }
		}*/
	    for (int k=0; k<s_p->wy_num; k++)
	    {
		int idx = s_p->we_ymin_idx[k];
		Ex_p->a[idx] -= sch_p->dt_dy*s_p->tfsf_hz_ymin_p->a[k];
		Ez_p->a[idx] += sch_p->dt_dy*s_p->tfsf_hx_ymin_p->a[k];
	    }
        }

        if (s_p->tfsf_ymax == true)
        {
/*            for (int i=0; i<s_p->tfsf_hz_ymax_p->nx; i++)
            {
                for (int k=0; k<s_p->tfsf_hz_ymax_p->ny; k++)
                {
                    int index = INDEX_P(Ex_p, i+s_p->x_min, s_p->y_max, k+s_p->z_min);
                    int index2d = INDEX_XY_P(s_p->tfsf_hz_ymax_p, i,k);
                    Ex_p->a[index] += sch_p->dt_dy*s_p->tfsf_hz_ymax_p->a[index2d];
                    Ez_p->a[index] -= sch_p->dt_dy*s_p->tfsf_hx_ymax_p->a[index2d];
                }
		}*/
	    for (int k=0; k<s_p->wy_num; k++)
	    {
		int idx = s_p->we_ymax_idx[k];
		Ex_p->a[idx] += sch_p->dt_dy*s_p->tfsf_hz_ymax_p->a[k];
		Ez_p->a[idx] -= sch_p->dt_dy*s_p->tfsf_hx_ymax_p->a[k];
	    }
        }

        if (s_p->tfsf_xmin == true)
        {
/*            for (int j=0; j<s_p->tfsf_hz_xmin_p->nx; j++)
            {
                for (int k=0; k<s_p->tfsf_hz_xmin_p->ny; k++)
                {
                    int index = INDEX_P(Ey_p, s_p->x_min, j+s_p->y_min, k+s_p->z_min);
                    int index2d = INDEX_XY_P(s_p->tfsf_hz_xmin_p, j,k);
                    Ey_p->a[index] += sch_p->dt_dx*s_p->tfsf_hz_xmin_p->a[index2d];
                    Ez_p->a[index] -= sch_p->dt_dx*s_p->tfsf_hy_xmin_p->a[index2d];
                }
		}*/
	    for (int k=0; k<s_p->wx_num; k++)
	    {
		int idx = s_p->we_xmin_idx[k];
		Ey_p->a[idx] += sch_p->dt_dx*s_p->tfsf_hz_xmin_p->a[k];
		Ez_p->a[idx] -= sch_p->dt_dx*s_p->tfsf_hy_xmin_p->a[k];
	    }
        }

        if (s_p->tfsf_xmax == true)
        {
/*            for (int j=0; j<s_p->tfsf_hz_xmax_p->nx; j++)
            {
                for (int k=0; k<s_p->tfsf_hz_xmax_p->ny; k++)
                {
                    int index = INDEX_P(Ez_p, s_p->x_max, j+s_p->y_min, k+s_p->z_min);
                    int index2d = INDEX_XY_P(s_p->tfsf_hy_xmax_p, j,k);
                    Ey_p->a[index] -= sch_p->dt_dx*s_p->tfsf_hz_xmax_p->a[index2d];
                    Ez_p->a[index] += sch_p->dt_dx*s_p->tfsf_hy_xmax_p->a[index2d];
                }
		}*/
	    for (int k=0; k<s_p->wx_num; k++)
	    {
		int idx = s_p->we_xmax_idx[k];
		Ey_p->a[idx] -= sch_p->dt_dx*s_p->tfsf_hz_xmax_p->a[k];
		Ez_p->a[idx] += sch_p->dt_dx*s_p->tfsf_hy_xmax_p->a[k];
	    }
        }
    }
}

void add_pulses_3d_h(vfield_n* vf_p,
                     sources_rt_data* s_p,
                     scheme_constants* sch_p)
{
    field_n* Hx_p = vf_p->comp_p[x_e];
    field_n* Hy_p = vf_p->comp_p[y_e];

    if (s_p->pulse_enable == true)
    {
        if (s_p->pulse_zmin == true)
        {
/*            for (int i=0; i<s_p->Jhx_zmin_p->nx; i++)
            {
                for (int j=0; j<s_p->Jhx_zmin_p->ny; j++)
                {
                    int index = INDEX_P(Hx_p,i,j,s_p->pulse_z_min-1);
                    int index2d = INDEX_XY_P(s_p->Jhx_zmin_p,i,j);
                    Hx_p->a[index] += sch_p->dt_4_pi*s_p->Jhx_zmin_p->a[index2d];
                    Hy_p->a[index] += sch_p->dt_4_pi*s_p->Jhy_zmin_p->a[index2d];
                }
		}*/
	    for (int k = 0; k < s_p->z_num; k++)
	    {
		int idx = s_p->jh_zmin_idx[k];
		Hy_p->a[idx] += sch_p->dt_4_pi*s_p->Jhy_zmin_p->a[k];
		Hx_p->a[idx] += sch_p->dt_4_pi*s_p->Jhx_zmin_p->a[k];
	    }
        }
        if (s_p->pulse_zmax == true)
        {
            /*for (int i=0; i<s_p->Jhx_zmax_p->nx; i++)
            {
                for (int j=0; j<s_p->Jhx_zmax_p->ny; j++)
                {
                    int index = INDEX_P(Hx_p,i,j,s_p->pulse_z_max);
                    int index2d = INDEX_XY_P(s_p->Jhx_zmax_p,i,j);
                    Hx_p->a[index] += sch_p->dt_4_pi*s_p->Jhx_zmax_p->a[index2d];
                    Hy_p->a[index] += sch_p->dt_4_pi*s_p->Jhy_zmax_p->a[index2d];
                }
		}*/
	    for (int k = 0; k < s_p->z_num; k++)
	    {
		int idx = s_p->jh_zmax_idx[k];
		Hy_p->a[idx] += sch_p->dt_4_pi*s_p->Jhy_zmax_p->a[k];
		Hx_p->a[idx] += sch_p->dt_4_pi*s_p->Jhx_zmax_p->a[k];
	    }
        }
    }
}

void add_tfsf_3d_h(vfield_n* vf_p,
                   sources_rt_data* s_p,
                   scheme_constants* sch_p)
{
    field_n* Hx_p = vf_p->comp_p[x_e];
    field_n* Hy_p = vf_p->comp_p[y_e];
    field_n* Hz_p = vf_p->comp_p[z_e];

    if (s_p->tfsf_enable == true)
    {
        if (s_p->tfsf_zmin == true)
        {
/*            for (int i=0; i<s_p->tfsf_ey_zmin_p->nx; i++)
            {
                for (int j=0; j<s_p->tfsf_ey_zmin_p->ny; j++)
                {
                    int index = INDEX_P(Hx_p, i+s_p->x_min, j+s_p->y_min, s_p->z_min-1);
                    int index2d = INDEX_XY_P(s_p->tfsf_ey_zmin_p, i,j);
                    Hx_p->a[index] -= sch_p->dt_dz*s_p->tfsf_ey_zmin_p->a[index2d];
                    Hy_p->a[index] += sch_p->dt_dz*s_p->tfsf_ex_zmin_p->a[index2d];
                }
		}*/
	    for(int k=0; k<s_p->wz_num; k++)
	    {
		int idx = s_p->wh_zmin_idx[k];
		Hx_p->a[idx] -= sch_p->dt_dz*s_p->tfsf_ey_zmin_p->a[k];
		Hy_p->a[idx] += sch_p->dt_dz*s_p->tfsf_ex_zmin_p->a[k];
	    }
        }

        if (s_p->tfsf_zmax == true)
        {
            /*for (int i=0; i<s_p->tfsf_ey_zmax_p->nx; i++)
            {
                for (int j=0; j<s_p->tfsf_ey_zmax_p->ny; j++)
                {
                    int index = INDEX_P(Hx_p, i+s_p->x_min, j+s_p->y_min,s_p->z_max-1);
                    int index2d = INDEX_XY_P(s_p->tfsf_ey_zmax_p, i,j);
                    Hx_p->a[index] += sch_p->dt_dz*s_p->tfsf_ey_zmax_p->a[index2d];
                    Hy_p->a[index] -= sch_p->dt_dz*s_p->tfsf_ex_zmax_p->a[index2d];
                }
		}*/
	    for(int k=0; k<s_p->wz_num; k++)
	    {
		int idx = s_p->wh_zmax_idx[k];
		Hx_p->a[idx] += sch_p->dt_dz*s_p->tfsf_ey_zmax_p->a[k];
		Hy_p->a[idx] -= sch_p->dt_dz*s_p->tfsf_ex_zmax_p->a[k];
	    }
        }

        if (s_p->tfsf_ymin == true)
        {
/*            for (int i=0; i<s_p->tfsf_ez_ymin_p->nx; i++)
            {
                for (int k=0; k<s_p->tfsf_ez_ymin_p->ny; k++)
                {
                    int index = INDEX_P(Hx_p, i+s_p->x_min, s_p->y_min-1, k+s_p->z_min);
                    int index2d = INDEX_XY_P(s_p->tfsf_ez_ymin_p, i,k);
                    Hx_p->a[index] += sch_p->dt_dy*s_p->tfsf_ez_ymin_p->a[index2d];
                    Hz_p->a[index] -= sch_p->dt_dy*s_p->tfsf_ex_ymin_p->a[index2d];
                }
		}*/
	    for(int k=0; k<s_p->wy_num; k++)
	    {
		int idx = s_p->wh_ymin_idx[k];
		Hx_p->a[idx] += sch_p->dt_dy*s_p->tfsf_ez_ymin_p->a[k];
		Hz_p->a[idx] -= sch_p->dt_dy*s_p->tfsf_ex_ymin_p->a[k];
	    }
        }

        if (s_p->tfsf_ymax == true)
        {
/*            for (int i=0; i<s_p->tfsf_ez_ymax_p->nx; i++)
            {
                for (int k=0; k<s_p->tfsf_ez_ymax_p->ny; k++)
                {
                    int index = INDEX_P(Hx_p, i+s_p->x_min, s_p->y_max-1, k+s_p->z_min);
                    int index2d = INDEX_XY_P(s_p->tfsf_ez_ymax_p, i,k);
                    Hx_p->a[index] -= sch_p->dt_dy*s_p->tfsf_ez_ymax_p->a[index2d];
                    Hz_p->a[index] += sch_p->dt_dy*s_p->tfsf_ex_ymax_p->a[index2d];
                }
		}*/
	    for(int k=0; k<s_p->wy_num; k++)
	    {
		int idx = s_p->wh_ymax_idx[k];
		Hx_p->a[idx] -= sch_p->dt_dy*s_p->tfsf_ez_ymax_p->a[k];
		Hz_p->a[idx] += sch_p->dt_dy*s_p->tfsf_ex_ymax_p->a[k];
	    }
        }

        if (s_p->tfsf_xmin == true)
        {
/*            for (int j=0; j<s_p->tfsf_ez_xmin_p->nx; j++)
            {
                for (int k=0; k<s_p->tfsf_ez_xmin_p->ny; k++)
                {
                    int index = INDEX_P(Hy_p, s_p->x_min-1, j+s_p->y_min, k+s_p->z_min);
                    int index2d = INDEX_XY_P(s_p->tfsf_ez_xmin_p, j,k);
                    Hy_p->a[index] -= sch_p->dt_dx*s_p->tfsf_ez_xmin_p->a[index2d];
                    Hz_p->a[index] += sch_p->dt_dx*s_p->tfsf_ey_xmin_p->a[index2d];
                }
		}*/
	    for(int k=0; k<s_p->wx_num; k++)
	    {
		int idx = s_p->wh_xmin_idx[k];
		Hy_p->a[idx] -= sch_p->dt_dx*s_p->tfsf_ez_xmin_p->a[k];
		Hz_p->a[idx] += sch_p->dt_dx*s_p->tfsf_ey_xmin_p->a[k];
	    }
        }

        if (s_p->tfsf_xmax == true)
        {
/*            for (int j=0; j<s_p->tfsf_ez_xmax_p->nx; j++)
            {
                for (int k=0; k<s_p->tfsf_ez_xmax_p->ny; k++)
                {
                    int index = INDEX_P(Hy_p, s_p->x_max-1, j+s_p->y_min, k+s_p->z_min);
                    int index2d = INDEX_XY_P(s_p->tfsf_ez_xmax_p, j,k);
                    Hy_p->a[index] += sch_p->dt_dx*s_p->tfsf_ez_xmax_p->a[index2d];
                    Hz_p->a[index] -= sch_p->dt_dx*s_p->tfsf_ey_xmax_p->a[index2d];
                }
		}*/
	    for(int k=0; k<s_p->wx_num; k++)
	    {
		int idx = s_p->wh_xmax_idx[k];
		Hy_p->a[idx] += sch_p->dt_dx*s_p->tfsf_ez_xmax_p->a[k];
		Hz_p->a[idx] -= sch_p->dt_dx*s_p->tfsf_ey_xmax_p->a[k];
	    }
        }
    }
}


#ifdef FULL_UPDATE

void update_3d_dx(vfield_n* vd_p,
		  vfield_n* vh_p,
		  vfield_n* vj_p,
		  geometry_data* g_p,
		  pml_data* p,
		  scheme_constants* s)
{
    int i,ex;
    field_n* Ex_p = vd_p->comp_p[x_e];
    field_n* Jx_p = vj_p->comp_p[x_e];
    field_n* Hy_p = vh_p->comp_p[y_e];
    field_n* Hz_p = vh_p->comp_p[z_e];

    int dy = DY_P(Ex_p);
    int dz = DZ_P(Ex_p);

#ifdef ONE_CONFIG_OPENMP
#pragma omp parallel for private(ex)
#endif
    for (i=0; i<p->nexf; i++)
    {
        ex = p->idexf[i];

#ifdef USENDF
	FP dhz = s->b_z * (Hz_p->a[ex] - Hz_p->a[ex - dy]) +
	    s->a_z * (Hz_p->a[ex - dz] - Hz_p->a[ex - dz -dy] +
		      Hz_p->a[ex + dz] - Hz_p->a[ex + dz -dy]);
	FP dhy = s->b_y * (Hy_p->a[ex] - Hy_p->a[ex - dz]) +
	    s->a_y * (Hy_p->a[ex - dy] - Hy_p->a[ex - dy -dz] +
		      Hy_p->a[ex + dy] - Hy_p->a[ex + dy -dz]);
#else
	FP dhz = s->dc_x * (Hz_p->a[ex] - Hz_p->a[ex - dy]);
	FP dhy = s->dc_x * (Hy_p->a[ex] - Hy_p->a[ex - dz]);
#endif

        Ex_p->a[ex] += s->dt_dy * dhz - s->dt_dz * dhy - Jx_p->a[ex]*s->dt_4_pi;
    }

#ifdef ONE_CONFIG_OPENMP
#pragma omp parallel for private(ex)
#endif
    for (i=0; i<p->nex; i++)
    {
        ex = p->idex[i];

#ifdef USENDF
	FP dhz = s->b_z * (Hz_p->a[ex] - Hz_p->a[ex - dy]) +
	    s->a_z * (Hz_p->a[ex - dz] - Hz_p->a[ex - dz -dy] +
		    Hz_p->a[ex + dz] - Hz_p->a[ex + dz -dy]);
	FP dhy = s->b_y * (Hy_p->a[ex] - Hy_p->a[ex - dz]) +
	    s->a_y * (Hy_p->a[ex - dy] - Hy_p->a[ex - dy -dz] +
		    Hy_p->a[ex + dy] - Hy_p->a[ex + dy -dz]);
#else
	FP dhz = s->dc_x * (Hz_p->a[ex] - Hz_p->a[ex - dy]);
	FP dhy = s->dc_x * (Hy_p->a[ex] - Hy_p->a[ex - dz]);
#endif

        p->Exy[i] = p->aexy[i]*p->Exy[i] + p->bexy[i] * dhz;
        p->Exz[i] = p->aexz[i]*p->Exz[i] + p->bexz[i] * dhy;

        Ex_p->a[ex] = p->Exy[i] + p->Exz[i];
    }

#ifdef USENDF
        MPI_EXCHANGE_DOWN_3D_Y_1COMP(&mpi, g_p, Ex_p);
        MPI_EXCHANGE_DOWN_3D_Z_1COMP(&mpi, g_p, Ex_p);
#endif
        MPI_EXCHANGE_UP_3D_Y_1COMP(&mpi, g_p, Ex_p);
        MPI_EXCHANGE_UP_3D_Z_1COMP(&mpi, g_p, Ex_p);
}

void update_3d_dy(vfield_n* vd_p,
		  vfield_n* vh_p,
		  vfield_n* vj_p,
		  geometry_data* g_p,
		  pml_data* p,
		  scheme_constants* s)
{
    int i, ey;
    field_n* Ey_p = vd_p->comp_p[y_e];
    field_n* Jy_p = vj_p->comp_p[y_e];
    field_n* Hx_p = vh_p->comp_p[x_e];
    field_n* Hz_p = vh_p->comp_p[z_e];

    int dx = DX_P(Ey_p);
    int dz = DZ_P(Ey_p);

#ifdef ONE_CONFIG_OPENMP
#pragma omp parallel for private(ey)
#endif
    for (i=0; i< p->neyf; i++)
    {
        ey = p->ideyf[i];

#ifdef USENDF
	FP dhx = s->b_x * (Hx_p->a[ey] - Hx_p->a[ey - dz]) +
	    s->a_x * (Hx_p->a[ey - dx] - Hx_p->a[ey - dx - dz] +
		      Hx_p->a[ey + dx] - Hx_p->a[ey + dx - dz]);
	FP dhz = s->b_z * (Hz_p->a[ey] - Hz_p->a[ey - dx]) +
	    s->a_z * (Hz_p->a[ey - dz] - Hz_p->a[ey - dz - dx] +
		      Hz_p->a[ey + dz] - Hz_p->a[ey + dz - dx]);
#else
	FP dhx = s->dc_y * (Hx_p->a[ey] - Hx_p->a[ey - dz]);
	FP dhz = s->dc_y * (Hz_p->a[ey] - Hz_p->a[ey - dx]);
#endif

        Ey_p->a[ey] += s->dt_dz * dhx - s->dt_dx * dhz - Jy_p->a[ey] * s->dt_4_pi;
    }

#ifdef ONE_CONFIG_OPENMP
#pragma omp parallel for private(ey)
#endif
    for (i=0; i< p->ney; i++)
    {
        ey = p->idey[i];

#ifdef USENDF
	FP dhx = s->b_x * (Hx_p->a[ey] - Hx_p->a[ey - dz]) +
	    s->a_x * (Hx_p->a[ey - dx] - Hx_p->a[ey - dx - dz] +
		      Hx_p->a[ey + dx] - Hx_p->a[ey + dx - dz]);
	FP dhz = s->b_z * (Hz_p->a[ey] - Hz_p->a[ey - dx]) +
	    s->a_z * (Hz_p->a[ey - dz] - Hz_p->a[ey - dz - dx] +
		      Hz_p->a[ey + dz] - Hz_p->a[ey + dz - dx]);
#else
	FP dhx = s->dc_y * (Hx_p->a[ey] - Hx_p->a[ey - dz]);
	FP dhz = s->dc_y * (Hz_p->a[ey] - Hz_p->a[ey - dx]);
#endif

        p->Eyx[i] = p->aeyx[i]*p->Eyx[i] + p->beyx[i] * dhz;
        p->Eyz[i] = p->aeyz[i]*p->Eyz[i] + p->beyz[i] * dhx;

        Ey_p->a[ey] = p->Eyx[i] + p->Eyz[i];
    }

#ifdef USENDF
	MPI_EXCHANGE_DOWN_3D_X_1COMP(&mpi, g_p, Ey_p);
	MPI_EXCHANGE_DOWN_3D_Z_1COMP(&mpi, g_p, Ey_p);
#endif
        MPI_EXCHANGE_UP_3D_X_1COMP(&mpi, g_p, Ey_p);
	MPI_EXCHANGE_UP_3D_Z_1COMP(&mpi, g_p, Ey_p);
}

void update_3d_dz(vfield_n* vd_p,
		  vfield_n* vh_p,
		  vfield_n* vj_p,
		  geometry_data* g_p,
		  pml_data* p,
		  scheme_constants* s)
{
    int i,ez;
    field_n* Ez_p = vd_p->comp_p[z_e];
    field_n* Jz_p = vj_p->comp_p[z_e];
    field_n* Hx_p = vh_p->comp_p[x_e];
    field_n* Hy_p = vh_p->comp_p[y_e];

    int dx = DX_P(Ez_p);
    int dy = DY_P(Ez_p);

#ifdef ONE_CONFIG_OPENMP
#pragma omp parallel for private(ez)
#endif
    for (i=0; i< p->nezf; i++)
    {
        ez = p->idezf[i];

#ifdef USENDF
	FP dhy = s->b_y * (Hy_p->a[ez] - Hy_p->a[ez - dx]) +
	    s->a_y * (Hy_p->a[ez - dy] - Hy_p->a[ez - dy - dx] +
		      Hy_p->a[ez + dy] - Hy_p->a[ez + dy - dx]);
	FP dhx = s->b_x * (Hx_p->a[ez] - Hx_p->a[ez - dy]) +
	    s->a_x * (Hx_p->a[ez - dx] - Hx_p->a[ez - dx - dy] +
		      Hx_p->a[ez + dx] - Hx_p->a[ez + dx - dy]);
#else
	FP dhy = s->dc_z * (Hy_p->a[ez] - Hy_p->a[ez - dx]);
	FP dhx = s->dc_z * (Hx_p->a[ez] - Hx_p->a[ez - dy]);
#endif

        Ez_p->a[ez] += s->dt_dx * dhy - s->dt_dy * dhx - Jz_p->a[ez]*s->dt_4_pi;
    }

#ifdef ONE_CONFIG_OPENMP
#pragma omp parallel for private(ez)
#endif
    for (i=0; i< p->nez; i++)
    {
        ez = p->idez[i];

#ifdef USENDF
	FP dhy = s->b_y * (Hy_p->a[ez] - Hy_p->a[ez - dx]) +
	    s->a_y * (Hy_p->a[ez - dy] - Hy_p->a[ez - dy - dx] +
		    Hy_p->a[ez + dy] - Hy_p->a[ez + dy - dx]);
	FP dhx = s->b_x * (Hx_p->a[ez] - Hx_p->a[ez - dy]) +
	    s->a_x * (Hx_p->a[ez - dx] - Hx_p->a[ez - dx - dy] +
		      Hx_p->a[ez + dx] - Hx_p->a[ez + dx - dy]);
#else
	FP dhy = s->dc_z * (Hy_p->a[ez] - Hy_p->a[ez - dx]);
	FP dhx = s->dc_z * (Hx_p->a[ez] - Hx_p->a[ez - dy]);
#endif

        p->Ezx[i] = p->aezx[i]*p->Ezx[i] + p->bezx[i] * dhy;
        p->Ezy[i] = p->aezy[i]*p->Ezy[i] + p->bezy[i] * dhx;

        Ez_p->a[ez] = p->Ezx[i] + p->Ezy[i];
    }
#ifdef USENDF
	MPI_EXCHANGE_DOWN_3D_X_1COMP(&mpi, g_p, Ez_p);
        MPI_EXCHANGE_DOWN_3D_Y_1COMP(&mpi, g_p, Ez_p);
#endif
        MPI_EXCHANGE_UP_3D_X_1COMP(&mpi, g_p, Ez_p);
        MPI_EXCHANGE_UP_3D_Y_1COMP(&mpi, g_p, Ez_p);
}

void update_3d_hx(vfield_n* ve_p,
		  vfield_n* vh_p,
		  geometry_data* g_p,
		  pml_data* p,
		  scheme_constants* s)
{
    int i,hx;
    field_n* Hx_p = vh_p->comp_p[x_e];
    field_n* Ey_p = ve_p->comp_p[y_e];
    field_n* Ez_p = ve_p->comp_p[z_e];

    int dy = DY_P(Hx_p);
    int dz = DZ_P(Hx_p);

#ifdef ONE_CONFIG_OPENMP
#pragma omp parallel for private(hx)
#endif
    for (i=0; i<p->nhxf; i++)
    {
        hx = p->idhxf[i];

#ifdef USENDF
	FP dey = s->b_y * (Ey_p->a[hx + dz] - Ey_p->a[hx]) +
	    s->a_y * (Ey_p->a[hx - dy + dz] - Ey_p->a[hx - dy] +
		      Ey_p->a[hx + dy + dz] - Ey_p->a[hx + dy]);
	FP dez = s->b_z * (Ez_p->a[hx + dy] - Ez_p->a[hx]) +
	    s->a_z * (Ez_p->a[hx - dz + dy] - Ez_p->a[hx - dz] +
		      Ez_p->a[hx + dz + dy] - Ez_p->a[hx + dz]);
#else
	FP dey = s->dc_x * (Ey_p->a[hx + dz] - Ey_p->a[hx]);
	FP dez = s->dc_x * (Ez_p->a[hx + dy] - Ez_p->a[hx]);
#endif

        Hx_p->a[hx] += s->dt_dz * dey - s->dt_dy * dez;
    }

#ifdef ONE_CONFIG_OPENMP
#pragma omp parallel for private(hx)
#endif
    for (i=0; i<p->nhx; i++)
    {
        hx = p->idhx[i];

#ifdef USENDF
	FP dey = s->b_y * (Ey_p->a[hx + dz] - Ey_p->a[hx]) +
	    s->a_y * (Ey_p->a[hx - dy + dz] - Ey_p->a[hx - dy] +
		      Ey_p->a[hx + dy + dz] - Ey_p->a[hx + dy]);
	FP dez = s->b_z * (Ez_p->a[hx + dy] - Ez_p->a[hx]) +
	    s->a_z * (Ez_p->a[hx - dz + dy] - Ez_p->a[hx - dz] +
		      Ez_p->a[hx + dz + dy] - Ez_p->a[hx + dz]);
#else
	FP dey = s->dc_x * (Ey_p->a[hx + dz] - Ey_p->a[hx]);
	FP dez = s->dc_x * (Ez_p->a[hx + dy] - Ez_p->a[hx]);
#endif

        p->Hxy[i] = p->ahxy[i]*p->Hxy[i] + p->bhxy[i] * dez;
        p->Hxz[i] = p->ahxz[i]*p->Hxz[i] + p->bhxz[i] * dey;

        Hx_p->a[hx] = p->Hxy[i] + p->Hxz[i];
    }
#ifdef USENDF
        MPI_EXCHANGE_UP_3D_Y_1COMP(&mpi, g_p, Hx_p);
        MPI_EXCHANGE_UP_3D_Z_1COMP(&mpi, g_p, Hx_p);
#endif
        MPI_EXCHANGE_DOWN_3D_Y_1COMP(&mpi, g_p, Hx_p);
        MPI_EXCHANGE_DOWN_3D_Z_1COMP(&mpi, g_p, Hx_p);
}

void update_3d_hy(vfield_n* ve_p,
		  vfield_n* vh_p,
		  geometry_data* g_p,
		  pml_data* p,
		  scheme_constants* s)
{
    int i,hy;
    field_n* Hy_p = vh_p->comp_p[y_e];
    field_n* Ex_p = ve_p->comp_p[x_e];
    field_n* Ez_p = ve_p->comp_p[z_e];

    int dx = DX_P(Hy_p);
    int dz = DZ_P(Hy_p);

#ifdef ONE_CONFIG_OPENMP
#pragma omp parallel for private(hy)
#endif
    for (i=0; i<p->nhyf; i++)
    {
        hy = p->idhyf[i];

#ifdef USENDF
	FP dez = s->b_z * (Ez_p->a[hy + dx] - Ez_p->a[hy]) +
	    s->a_z * (Ez_p->a[hy - dz + dx] - Ez_p->a[hy - dz] +
		      Ez_p->a[hy + dz + dx] - Ez_p->a[hy + dz]);
	FP dex = s->b_x * (Ex_p->a[hy + dz] - Ex_p->a[hy]) +
	    s->a_x * (Ex_p->a[hy - dx + dz] - Ex_p->a[hy - dx] +
		      Ex_p->a[hy + dx + dz] - Ex_p->a[hy + dx]);

#else
	FP dez = s->dc_y * (Ez_p->a[hy + dx] - Ez_p->a[hy]);
	FP dex = s->dc_y * (Ex_p->a[hy + dz] - Ex_p->a[hy]);
#endif
        Hy_p->a[hy] += s->dt_dx * dez - s->dt_dz * dex;
    }

#ifdef ONE_CONFIG_OPENMP
#pragma omp parallel for private(hy)
#endif
    for (i=0; i<p->nhy; i++)
    {
        hy = p->idhy[i];

#ifdef USENDF
	FP dez = s->b_z * (Ez_p->a[hy + dx] - Ez_p->a[hy]) +
	    s->a_z * (Ez_p->a[hy - dz + dx] - Ez_p->a[hy - dz] +
		      Ez_p->a[hy + dz + dx] - Ez_p->a[hy + dz]);
	FP dex = s->b_x * (Ex_p->a[hy + dz] - Ex_p->a[hy]) +
	    s->a_x * (Ex_p->a[hy - dx + dz] - Ex_p->a[hy - dx] +
		      Ex_p->a[hy + dx + dz] - Ex_p->a[hy + dx]);

#else
	FP dez = s->dc_y * (Ez_p->a[hy + dx] - Ez_p->a[hy]);
	FP dex = s->dc_y * (Ex_p->a[hy + dz] - Ex_p->a[hy]);
#endif

        p->Hyx[i] = p->ahyx[i]*p->Hyx[i] + p->bhyx[i] * dez;
        p->Hyz[i] = p->ahyz[i]*p->Hyz[i] + p->bhyz[i] * dex;

        Hy_p->a[hy] = p->Hyx[i] + p->Hyz[i];
    }
#ifdef USENDF
	MPI_EXCHANGE_UP_3D_X_1COMP(&mpi, g_p, Hy_p);
        MPI_EXCHANGE_UP_3D_Z_1COMP(&mpi, g_p, Hy_p);
#endif
        MPI_EXCHANGE_DOWN_3D_X_1COMP(&mpi, g_p, Hy_p);
        MPI_EXCHANGE_DOWN_3D_Z_1COMP(&mpi, g_p, Hy_p);
}

void update_3d_hz(vfield_n* ve_p,
		  vfield_n* vh_p,
		  geometry_data* g_p,
		  pml_data* p,
		  scheme_constants* s)
{
    int i,hz;
    field_n* Hz_p = vh_p->comp_p[z_e];
    field_n* Ey_p = ve_p->comp_p[y_e];
    field_n* Ex_p = ve_p->comp_p[x_e];

    int dx = DX_P(Hz_p);
    int dy = DY_P(Hz_p);

#ifdef ONE_CONFIG_OPENMP
#pragma omp parallel for private(hz)
#endif
    for (i=0; i<p->nhzf; i++)
    {
        hz = p->idhzf[i];

#ifdef USENDF
	FP dex = s->b_x * (Ex_p->a[hz + dy] - Ex_p->a[hz]) +
	    s->a_x * (Ex_p->a[hz - dx + dy] - Ex_p->a[hz - dx] +
		      Ex_p->a[hz + dx + dy] - Ex_p->a[hz + dx]);
	FP dey = s->b_y * (Ey_p->a[hz + dx] - Ey_p->a[hz]) +
	    s->a_y * (Ey_p->a[hz - dy + dx] - Ey_p->a[hz - dy] +
		      Ey_p->a[hz + dy + dx] - Ey_p->a[hz + dy]);

#else
	FP dex = s->dc_z * (Ex_p->a[hz + dy] - Ex_p->a[hz]);
	FP dey = s->dc_z * (Ey_p->a[hz + dx] - Ey_p->a[hz]);
#endif

        Hz_p->a[hz] += s->dt_dy * dex - s->dt_dx * dey;
    }

#ifdef ONE_CONFIG_OPENMP
#pragma omp parallel for private(hz)
#endif
    for (i=0; i<p->nhz; i++)
    {
        hz = p->idhz[i];

#ifdef USENDF
	FP dex = s->b_x * (Ex_p->a[hz + dy] - Ex_p->a[hz]) +
	    s->a_x * (Ex_p->a[hz - dx + dy] - Ex_p->a[hz - dx] +
		      Ex_p->a[hz + dx + dy] - Ex_p->a[hz + dx]);
	FP dey = s->b_y * (Ey_p->a[hz + dx] - Ey_p->a[hz]) +
	    s->a_y * (Ey_p->a[hz - dy + dx] - Ey_p->a[hz - dy] +
		      Ey_p->a[hz + dy + dx] - Ey_p->a[hz + dy]);

#else
	FP dex = s->dc_z * (Ex_p->a[hz + dy] - Ex_p->a[hz]);
	FP dey = s->dc_z * (Ey_p->a[hz + dx] - Ey_p->a[hz]);
#endif

        p->Hzx[i] = p->ahzx[i]*p->Hzx[i] + p->bhzx[i] * dey;
        p->Hzy[i] = p->ahzy[i]*p->Hzy[i] + p->bhzy[i] * dex;

        Hz_p->a[hz] = p->Hzx[i] + p->Hzy[i];
    }
#ifdef USENDF
	MPI_EXCHANGE_UP_3D_X_1COMP(&mpi, g_p, Hz_p);
        MPI_EXCHANGE_UP_3D_Y_1COMP(&mpi, g_p, Hz_p);
#endif
        MPI_EXCHANGE_DOWN_3D_X_1COMP(&mpi, g_p, Hz_p);
        MPI_EXCHANGE_DOWN_3D_Y_1COMP(&mpi, g_p, Hz_p);
}

#else  /* FULL UPDATE */

void update_3d_dx(vfield_n* vd_p,
                  vfield_n* vh_p,
                  vfield_n* vj_p,
                  geometry_data* g_p,
                  pml_data* p,
                  scheme_constants* s)
{
    int i,j,k,ex,pj,j0,pk,k0;
    field_n* Ex_p = vd_p->comp_p[x_e];
    field_n* Jx_p = vj_p->comp_p[x_e];
    field_n* Hy_p = vh_p->comp_p[y_e];
    field_n* Hz_p = vh_p->comp_p[z_e];
    pml_fields* f = &p->pml_f;

    int dy = DY_P(Ex_p);
    int dz = DZ_P(Ex_p);

#ifdef ONE_CONFIG_OPENMP
#pragma omp parallel for private(i,j,ex,pj,j0,pk,k0)
#endif

    for (k=g_p->kmin; k<g_p->kmax + ADD; k++)
    {
        int k1 = k + g_p->k_offset;
        for (i=g_p->imin-1; i<g_p->imax+1; i++)
        {
            for (j=g_p->jmin; j<g_p->jmax + ADD; j++)
            {
                int j1 = j + g_p->j_offset;
		ex = INDEX_P(Ex_p,i,j,k);

#ifdef USENDF
		FP dhz = s->b_z * (Hz_p->a[ex] - Hz_p->a[ex - dy]) +
		    s->a_z * (Hz_p->a[ex - dz] - Hz_p->a[ex - dz -dy] +
			      Hz_p->a[ex + dz] - Hz_p->a[ex + dz -dy]);
		FP dhy = s->b_y * (Hy_p->a[ex] - Hy_p->a[ex - dz]) +
		    s->a_y * (Hy_p->a[ex - dy] - Hy_p->a[ex - dy -dz] +
			      Hy_p->a[ex + dy] - Hy_p->a[ex + dy -dz]);
#else
		FP dhz = s->dc_x * (Hz_p->a[ex] - Hz_p->a[ex - dy]);
		FP dhy = s->dc_x * (Hy_p->a[ex] - Hy_p->a[ex - dz]);
#endif

		if (k1 >= p->ez_l && k1 <= p->ez_h && j1 >= p->ey_l && j1 <= p->ey_h)
                {
		    Ex_p->a[ex] += s->dt_dy * dhz - s->dt_dz * dhy - Jx_p->a[ex]*s->dt_4_pi;
                }
                else
                {
                    if (k1 >= p->ez_l && k1 <= p->ez_h)
                    {
			if (j1 > p->ey_h)
                        {
                            j0 = INDEX_P(f->Exy_y_h,i,j1 - p->eoffy_h,k);
                            pj = p->epmly_h - j1;

                            f->Exy_y_h->a[j0] = p->c_hy[pj]*f->Exy_y_h->a[j0] - p->d_hy[pj] * dhz;
                            f->Exz_y_h->a[j0] += - s->dt_dz * dhy;
                            Ex_p->a[ex] = f->Exy_y_h->a[j0] + f->Exz_y_h->a[j0];
                        }
                        else
                        {
                            j0 = INDEX_P(f->Exy_y_l,i,j - p->eoffy_l,k);
                            pj = j1 - p->epmly_l;

                            f->Exy_y_l->a[j0] = p->c_ey[pj] * f->Exy_y_l->a[j0] - p->d_ey[pj] * dhz;
                            f->Exz_y_l->a[j0] += -s->dt_dz * dhy;
                            Ex_p->a[ex] = f->Exy_y_l->a[j0] + f->Exz_y_l->a[j0];
                        }
                    }
                    else if(k1 < p->ez_l)
                    {
                        k0 = INDEX_P(f->Exy_z_l,i,j,k - p->eoffz_l);
                        pk = k1 - p->epmlz_l;

                        if (j1 >= p->ey_l && j1 <= p->ey_h)
                        {
			    f->Exy_z_l->a[k0] += s->dt_dy * dhz;
                            f->Exz_z_l->a[k0] = p->c_ez[pk] * f->Exz_z_l->a[k0] + p->d_ez[pk] * dhy;
                            Ex_p->a[ex] = f->Exy_z_l->a[k0] + f->Exz_z_l->a[k0];
                        }
                        else if (j1 > p->ey_h)
                        {
			    j0 = INDEX_P(f->Exy_y_h,i,j1 - p->eoffy_h,k);
                            pj = p->epmly_h - j1;

			    f->Exy_y_h->a[j0] = p->c_hy[pj] * f->Exy_y_h->a[j0] - p->d_hy[pj] * dhz;
                            f->Exz_z_l->a[k0] = p->c_ez[pk] * f->Exz_z_l->a[k0] + p->d_ez[pk] * dhy;
                            Ex_p->a[ex] = f->Exy_y_h->a[j0] + f->Exz_z_l->a[k0];
                        }
                        else
                        {
			    j0 = INDEX_P(f->Exy_y_l,i,j - p->eoffy_l,k);
                            pj = j1 - p->epmly_l;

			    f->Exy_y_l->a[j0] = p->c_ey[pj] * f->Exy_y_l->a[j0] - p->d_ey[pj] * dhz;
                            f->Exz_z_l->a[k0] = p->c_ez[pk] * f->Exz_z_l->a[k0] + p->d_ez[pk] * dhy;
                            Ex_p->a[ex] = f->Exy_y_l->a[j0] + f->Exz_z_l->a[k0];
                        }
                    }
                    else
                    {
                        k0 = INDEX_P(f->Exy_z_h,i,j,k1 - p->eoffz_h);
                        pk = p->epmlz_h - k1;

                        if (j1 >= p->ey_l && j1 <= p->ey_h)
                        {
			    f->Exy_z_h->a[k0] += s->dt_dy * dhz;
                            f->Exz_z_h->a[k0] = p->c_hz[pk] * f->Exz_z_h->a[k0] + p->d_hz[pk] * dhy;
                            Ex_p->a[ex] = f->Exy_z_h->a[k0] + f->Exz_z_h->a[k0];
                        }
                        else if (j1 > p->ey_h)
                        {
			    j0 = INDEX_P(f->Exy_y_h,i,j1 - p->eoffy_h,k);
                            pj = p->epmly_h - j1;

                            f->Exy_y_h->a[j0] = p->c_hy[pj] * f->Exy_y_h->a[j0] - p->d_hy[pj] * dhz;
                            f->Exz_z_h->a[k0] = p->c_hz[pk] * f->Exz_z_h->a[k0] + p->d_hz[pk] * dhy;
                            Ex_p->a[ex] = f->Exy_y_h->a[j0] + f->Exz_z_h->a[k0];

                        }
                        else
                        {
			    j0 = INDEX_P(f->Exy_y_l,i,j - p->eoffy_l,k);
                            pj = j1 - p->epmly_l;

                            f->Exy_y_l->a[j0] = p->c_ey[pj] * f->Exy_y_l->a[j0] - p->d_ey[pj] * dhz;
                            f->Exz_z_h->a[k0] = p->c_hz[pk] * f->Exz_z_h->a[k0] + p->d_hz[pk] * dhy;
                            Ex_p->a[ex] = f->Exy_y_l->a[j0] + f->Exz_z_h->a[k0];
                        }
                    }
                } /* main part */
            } /* i */
        } /* j */
    } /* k */

#ifdef USENDF
        MPI_EXCHANGE_DOWN_3D_Y_1COMP(&mpi, g_p, Ex_p);
        MPI_EXCHANGE_DOWN_3D_Z_1COMP(&mpi, g_p, Ex_p);
#endif
        MPI_EXCHANGE_UP_3D_Y_1COMP(&mpi, g_p, Ex_p);
        MPI_EXCHANGE_UP_3D_Z_1COMP(&mpi, g_p, Ex_p);
}

void update_3d_dy(vfield_n* vd_p,
                  vfield_n* vh_p,
                  vfield_n* vj_p,
                  geometry_data* g_p,
                  pml_data* p,
                  scheme_constants* s)
{
    int i,j,k,ey,pi,i0,pk,k0;
    field_n* Ey_p = vd_p->comp_p[y_e];
    field_n* Jy_p = vj_p->comp_p[y_e];
    field_n* Hx_p = vh_p->comp_p[x_e];
    field_n* Hz_p = vh_p->comp_p[z_e];
    pml_fields* f = &p->pml_f;

    int dx = DX_P(Ey_p);
    int dz = DZ_P(Ey_p);

#ifdef ONE_CONFIG_OPENMP
#pragma omp parallel for private(i,j,ey,pi,i0,pk,k0)
#endif

    for (k=g_p->kmin; k<g_p->kmax + ADD; k++)
    {
        int k1 = k+g_p->k_offset;
        for (i=g_p->imin; i<g_p->imax + ADD; i++)
        {
            int i1 = i+g_p->i_offset;
            for (j=g_p->jmin-1; j<g_p->jmax+1; j++)
            {
		ey = INDEX_P(Ey_p,i,j,k);
#ifdef USENDF
		FP dhx = s->b_x * (Hx_p->a[ey] - Hx_p->a[ey - dz]) +
		    s->a_x * (Hx_p->a[ey - dx] - Hx_p->a[ey - dx - dz] +
			      Hx_p->a[ey + dx] - Hx_p->a[ey + dx - dz]);
		FP dhz = s->b_z * (Hz_p->a[ey] - Hz_p->a[ey - dx]) +
		    s->a_z * (Hz_p->a[ey - dz] - Hz_p->a[ey - dz - dx] +
			      Hz_p->a[ey + dz] - Hz_p->a[ey + dz - dx]);
#else
		FP dhx = s->dc_y * (Hx_p->a[ey] - Hx_p->a[ey - dz]);
		FP dhz = s->dc_y * (Hz_p->a[ey] - Hz_p->a[ey - dx]);
#endif

                if (k1 >= p->ez_l && k1 <= p->ez_h && i1 >= p->ex_l && i1 <= p->ex_h)
                {
                    Ey_p->a[ey] += s->dt_dz * dhx - s->dt_dx * dhz - Jy_p->a[ey] * s->dt_4_pi;
                }
                else
                {
                    if (k1 >= p->ez_l && k1 <= p->ez_h)
                    {
                        if (i1 > p->ex_h)
                        {
		            i0 = INDEX_P(f->Eyx_x_h,i1 - p->eoffx_h,j,k);
                            pi = p->epmlx_h - i1;

                            f->Eyx_x_h->a[i0] = p->c_hx[pi] * f->Eyx_x_h->a[i0] + p->d_hx[pi] * dhz;
                            f->Eyz_x_h->a[i0] += s->dt_dz * dhx;
                            Ey_p->a[ey] = f->Eyx_x_h->a[i0] + f->Eyz_x_h->a[i0];

                        }
                        else
                        {
		            i0 = INDEX_P(f->Eyx_x_l,i - p->eoffx_l,j,k);
                            pi = i1 - p->epmlx_l;

                            f->Eyx_x_l->a[i0] = p->c_ex[pi] * f->Eyx_x_l->a[i0] + p->d_ex[pi] * dhz;
                            f->Eyz_x_l->a[i0] += s->dt_dz * dhx;
                            Ey_p->a[INDEX_P(Ey_p,i,j,k)] = f->Eyx_x_l->a[i0] + f->Eyz_x_l->a[i0];
                        }
                    }
                    else if(k1 < p->ez_l)
                    {
                        k0 = INDEX_P(f->Eyx_z_l,i,j,k - p->eoffz_l);
                        pk = k1 - p->epmlz_l;

                        if (i1 >= p->ex_l && i1 <= p->ex_h)
                        {
			    f->Eyx_z_l->a[k0] += -s->dt_dx * dhz;
                            f->Eyz_z_l->a[k0] = p->c_ez[pk] * f->Eyz_z_l->a[k0] - p->d_ez[pk] * dhx;
                            Ey_p->a[ey] = f->Eyx_z_l->a[k0] + f->Eyz_z_l->a[k0];
                        }
                        else if (i1 > p->ex_h)
                        {
			    i0 = INDEX_P(f->Eyx_x_h,i1 - p->eoffx_h,j,k);
                            pi = p->epmlx_h - i1;

                            f->Eyx_x_h->a[i0] = p->c_hx[pi] * f->Eyx_x_h->a[i0] + p->d_hx[pi] * dhz;
                            f->Eyz_z_l->a[k0] = p->c_ez[pk] * f->Eyz_z_l->a[k0] - p->d_ez[pk] * dhx;
                            Ey_p->a[ey] = f->Eyx_x_h->a[i0] + f->Eyz_z_l->a[k0];
                        }
                        else
                        {
                            i0 = INDEX_P(f->Eyx_x_l,i - p->eoffx_l,j,k) ;
                            pi = i1 - p->epmlx_l;

                            f->Eyx_x_l->a[i0] = p->c_ex[pi] * f->Eyx_x_l->a[i0] + p->d_ex[pi] * dhz;
                            f->Eyz_z_l->a[k0] = p->c_ez[pk] * f->Eyz_z_l->a[k0] - p->d_ez[pk] * dhx;
                            Ey_p->a[ey] = f->Eyx_x_l->a[i0] + f->Eyz_z_l->a[k0];
                        }
                    }
                    else
                    {
                        k0 = INDEX_P(f->Eyx_z_h,i,j,k1 - p->eoffz_h);
                        pk = p->epmlz_h - k1;

                        if (i1 >= p->ex_l && i1 <= p->ex_h)
                        {
			    f->Eyx_z_h->a[k0] += -s->dt_dx * dhz;
                            f->Eyz_z_h->a[k0] = p->c_hz[pk]*f->Eyz_z_h->a[k0] - p->d_hz[pk] * dhx;
                            Ey_p->a[ey] = f->Eyx_z_h->a[k0] + f->Eyz_z_h->a[k0];
                        }
                        else if (i1 > p->ex_h)
                        {
			    i0 = INDEX_P(f->Eyx_x_h,i1 - p->eoffx_h,j,k);
                            pi = p->epmlx_h - i1;

                            f->Eyx_x_h->a[i0] = p->c_hx[pi] * f->Eyx_x_h->a[i0] + p->d_hx[pi] * dhz;
                            f->Eyz_z_h->a[k0] = p->c_hz[pk] * f->Eyz_z_h->a[k0] - p->d_hz[pk] * dhx;
                            Ey_p->a[ey] = f->Eyx_x_h->a[i0] + f->Eyz_z_h->a[k0];
                        }
                        else
                        {
			    i0 = INDEX_P(f->Eyx_x_l,i - p->eoffx_l,j,k);
                            pi = i1 - p->epmlx_l;

                            f->Eyx_x_l->a[i0] = p->c_ex[pi] * f->Eyx_x_l->a[i0] + p->d_ex[pi] * dhz;
                            f->Eyz_z_h->a[k0] = p->c_hz[pk] * f->Eyz_z_h->a[k0] - p->d_hz[pk] * dhx;
                            Ey_p->a[ey] = f->Eyx_x_l->a[i0] + f->Eyz_z_h->a[k0];
                        }
                    }
                }/* main part */
            }/* j */
        }/* i */
    }/* k */

#ifdef USENDF
	MPI_EXCHANGE_DOWN_3D_X_1COMP(&mpi, g_p, Ey_p);
	MPI_EXCHANGE_DOWN_3D_Z_1COMP(&mpi, g_p, Ey_p);
#endif
        MPI_EXCHANGE_UP_3D_X_1COMP(&mpi, g_p, Ey_p);
	MPI_EXCHANGE_UP_3D_Z_1COMP(&mpi, g_p, Ey_p);
}

void update_3d_dz(vfield_n* vd_p,
                  vfield_n* vh_p,
                  vfield_n* vj_p,
                  geometry_data* g_p,
                  pml_data* p,
                  scheme_constants* s)
{
    int i,j,k,ez,pj,j0,pi,i0;
    field_n* Ez_p = vd_p->comp_p[z_e];
    field_n* Jz_p = vj_p->comp_p[z_e];
    field_n* Hx_p = vh_p->comp_p[x_e];
    field_n* Hy_p = vh_p->comp_p[y_e];
    pml_fields* f = &p->pml_f;

    int dx = DX_P(Ez_p);
    int dy = DY_P(Ez_p);

#ifdef ONE_CONFIG_OPENMP
#pragma omp parallel for private(i,j,ez,pj,j0,pi,i0)
#endif

    for (k=g_p->kmin-1; k<g_p->kmax+1; k++)
    {
        for (i=g_p->imin; i<g_p->imax + ADD; i++)
        {
            int i1 = i + g_p->i_offset;
            for (j=g_p->jmin; j<g_p->jmax + ADD; j++)
            {
                int j1 = j + g_p->j_offset;
		ez = INDEX_P(Ez_p,i,j,k);

#ifdef USENDF
		FP dhy = s->b_y * (Hy_p->a[ez] - Hy_p->a[ez - dx]) +
		    s->a_y * (Hy_p->a[ez - dy] - Hy_p->a[ez - dy - dx] +
			      Hy_p->a[ez + dy] - Hy_p->a[ez + dy - dx]);
		FP dhx = s->b_x * (Hx_p->a[ez] - Hx_p->a[ez - dy]) +
		    s->a_x * (Hx_p->a[ez - dx] - Hx_p->a[ez - dx - dy] +
			      Hx_p->a[ez + dx] - Hx_p->a[ez + dx - dy]);
#else
		FP dhy = s->dc_z * (Hy_p->a[ez] - Hy_p->a[ez - dx]);
		FP dhx = s->dc_z * (Hx_p->a[ez] - Hx_p->a[ez - dy]);
#endif

                if (i1 >= p->ex_l && i1 <= p->ex_h && j1 >= p->ey_l && j1 <= p->ey_h)
                {
		     Ez_p->a[ez] += s->dt_dx * dhy - s->dt_dy * dhx - Jz_p->a[ez] *s->dt_4_pi;
                }
                else
                {
                    if (i1 >= p->ex_l && i1 <= p->ex_h)
                    {
                        if (j1 > p->ey_h)
                        {
                            j0 = INDEX_P(f->Ezy_y_h,i,j1 - p->eoffy_h,k) ;
                            pj = p->epmly_h - j1;

                            f->Ezy_y_h->a[j0] = p->c_hy[pj]*f->Ezy_y_h->a[j0] + p->d_hy[pj] * dhx;
                            f->Ezx_y_h->a[j0] += s->dt_dx * dhy;
                            Ez_p->a[ez] = f->Ezy_y_h->a[j0] + f->Ezx_y_h->a[j0];
                        }
                        else
                        {
                            j0 = INDEX_P(f->Ezy_y_l,i,j - p->eoffy_l,k);
                            pj = j1 - p->epmly_l;

                            f->Ezy_y_l->a[j0] = p->c_ey[pj]*f->Ezy_y_l->a[j0] + p->d_ey[pj] * dhx;
                            f->Ezx_y_l->a[j0] += s->dt_dx * dhy;
                            Ez_p->a[ez] = f->Ezy_y_l->a[j0] + f->Ezx_y_l->a[j0];
                        }
                    }
                    else if (i1 < p->ex_l)
                    {
                        i0 = INDEX_P(f->Ezy_x_l,i - p->eoffx_l,j,k);
                        pi = i1 - p->epmlx_l;

                        if (j1 >= p->ey_l && j1 <= p->ey_h)
                        {
			    f->Ezy_x_l->a[i0] += -s->dt_dy * dhx;
                            f->Ezx_x_l->a[i0] = p->c_ex[pi] * f->Ezx_x_l->a[i0] - p->d_ex[pi] * dhy;
                            Ez_p->a[ez] = f->Ezy_x_l->a[i0] + f->Ezx_x_l->a[i0];
                        }
                        else if (j1 > p->ey_h)
                        {
                            j0 = INDEX_P(f->Ezy_y_h,i,j1 - p->eoffy_h,k);
                            pj = p->epmly_h - j1;

                            f->Ezy_y_h->a[j0] = p->c_hy[pj] * f->Ezy_y_h->a[j0] + p->d_hy[pj] * dhx;
                            f->Ezx_x_l->a[i0] = p->c_ex[pi] * f->Ezx_x_l->a[i0] - p->d_ex[pi] * dhy;
                            Ez_p->a[ez] = f->Ezy_y_h->a[j0] + f->Ezx_x_l->a[i0];
                        }
                        else
                        {
                            j0 = INDEX_P(f->Ezy_y_l,i,j - p->eoffy_l,k);
                            pj = j1 - p->epmly_l;

                            f->Ezy_y_l->a[j0] = p->c_ey[pj] * f->Ezy_y_l->a[j0] + p->d_ey[pj] * dhx;
                            f->Ezx_x_l->a[i0] = p->c_ex[pi] * f->Ezx_x_l->a[i0] - p->d_ex[pi] * dhy;
                            Ez_p->a[ez] = f->Ezy_y_l->a[j0] + f->Ezx_x_l->a[i0];
                        }
                    }
                    else
                    {
                        i0 = INDEX_P(f->Ezy_x_h,i1 - p->eoffx_h,j,k);
                        pi = p->epmlx_h - i1;

                        if (j1 >= p->ey_l && j1 <= p->ey_h)
                        {
                            f->Ezy_x_h->a[i0] += -s->dt_dy * dhx;
                            f->Ezx_x_h->a[i0] = p->c_hx[pi] * f->Ezx_x_h->a[i0] - p->d_hx[pi] * dhy;
                            Ez_p->a[ez] = f->Ezy_x_h->a[i0] + f->Ezx_x_h->a[i0];
                        }
                        else if (j1 > p->ey_h)
                        {
                            j0 = INDEX_P(f->Ezy_y_h,i,j1 - p->eoffy_h,k);
                            pj = p->epmly_h - j1;

                            f->Ezy_y_h->a[j0] = p->c_hy[pj] * f->Ezy_y_h->a[j0] + p->d_hy[pj] * dhx;
                            f->Ezx_x_h->a[i0] = p->c_hx[pi] * f->Ezx_x_h->a[i0] - p->d_hx[pi] * dhy;
                            Ez_p->a[ez] = f->Ezy_y_h->a[j0] + f->Ezx_x_h->a[i0];

                        }
                        else
                        {
                            j0 = INDEX_P(f->Ezy_y_l,i,j - p->eoffy_l,k);
                            pj = j1 - p->epmly_l;

                            f->Ezy_y_l->a[j0] = p->c_ey[pj] * f->Ezy_y_l->a[j0] + p->d_ey[pj] * dhx;
                            f->Ezx_x_h->a[i0] = p->c_hx[pi] * f->Ezx_x_h->a[i0] - p->d_hx[pi] * dhy;
                            Ez_p->a[ez] = f->Ezy_y_l->a[j0] + f->Ezx_x_h->a[i0];
                        }
                    }
                } /* main part */
            } /* j */
        } /* i */
    } /* k */

#ifdef USENDF
	MPI_EXCHANGE_DOWN_3D_X_1COMP(&mpi, g_p, Ez_p);
        MPI_EXCHANGE_DOWN_3D_Y_1COMP(&mpi, g_p, Ez_p);
#endif
        MPI_EXCHANGE_UP_3D_X_1COMP(&mpi, g_p, Ez_p);
        MPI_EXCHANGE_UP_3D_Y_1COMP(&mpi, g_p, Ez_p);
}

void update_3d_hx(vfield_n* ve_p,
                  vfield_n* vh_p,
                  geometry_data* g_p,
                  pml_data* p,
                  scheme_constants* s)
{
    int i,j,k,hx,pj,j0,pk,k0;
    field_n* Hx_p = vh_p->comp_p[x_e];
    field_n* Ey_p = ve_p->comp_p[y_e];
    field_n* Ez_p = ve_p->comp_p[z_e];
    pml_fields* f = &p->pml_f;

    int dy = DY_P(Hx_p);
    int dz = DZ_P(Hx_p);

#ifdef ONE_CONFIG_OPENMP
#pragma omp parallel for private(i,j,hx,pj,j0,pk,k0)
#endif

    for (k=g_p->kmin - ADD; k<g_p->kmax; k++)
    {
        int k1 = k + g_p->k_offset;
        for (i=g_p->imin-1; i<g_p->imax+1; i++)
        {
            for (j=g_p->jmin - ADD; j<g_p->jmax; j++)
            {
                int j1 = j + g_p->j_offset;
		hx = INDEX_P(Hx_p,i,j,k);

#ifdef USENDF
		FP dey = s->b_y * (Ey_p->a[hx + dz] - Ey_p->a[hx]) +
		    s->a_y * (Ey_p->a[hx - dy + dz] - Ey_p->a[hx - dy] +
			      Ey_p->a[hx + dy + dz] - Ey_p->a[hx + dy]);
		FP dez = s->b_z * (Ez_p->a[hx + dy] - Ez_p->a[hx]) +
		    s->a_z * (Ez_p->a[hx - dz + dy] - Ez_p->a[hx - dz] +
			      Ez_p->a[hx + dz + dy] - Ez_p->a[hx + dz]);
#else
		FP dey = s->dc_x * (Ey_p->a[hx + dz] - Ey_p->a[hx]);
		FP dez = s->dc_x * (Ez_p->a[hx + dy] - Ez_p->a[hx]);
#endif

		if (k1 >= p->hz_l && k1 <= p->hz_h && j1 >= p->hy_l && j1 <= p->hy_h)
                {
		    Hx_p->a[hx] += s->dt_dz * dey - s->dt_dy * dez;
                }
                else
                {
                    if (k1 >= p->hz_l && k1 <= p->hz_h)
                    {
		        if (j1 > p->hy_h)
                        {
                            j0 = INDEX_P(f->Hxy_y_h,i,j1 - p->hoffy_h,k);
                            pj = p->hpmly_h - j1;

                            f->Hxy_y_h->a[j0] = p->c_ey[pj] * f->Hxy_y_h->a[j0] + p->d_ey[pj] * dez;
                            f->Hxz_y_h->a[j0] += s->dt_dz * dey;
                            Hx_p->a[hx] = f->Hxy_y_h->a[j0] + f->Hxz_y_h->a[j0];
                        }
                        else
                        {
                            j0 = INDEX_P(f->Hxy_y_l,i,j - p->hoffy_l,k);
                            pj = j1 - p->hpmly_l;

                            f->Hxy_y_l->a[j0] = p->c_hy[pj] * f->Hxy_y_l->a[j0] + p->d_hy[pj] * dez;
                            f->Hxz_y_l->a[j0] += s->dt_dz * dey;
                            Hx_p->a[hx] = f->Hxy_y_l->a[j0] + f->Hxz_y_l->a[j0];
                        }
                    }
                    else if(k1 < p->hz_l)
                    {
                        k0 = INDEX_P(f->Hxy_z_l,i,j,k - p->hoffz_l);
                        pk = k1 - p->hpmlz_l;

                        if (j1 >= p->hy_l && j1 <= p->hy_h)
                        {
                            f->Hxy_z_l->a[k0] += -s->dt_dy * dez;
                            f->Hxz_z_l->a[k0] = p->c_hz[pk] * f->Hxz_z_l->a[k0] - p->d_hz[pk] * dey;
                            Hx_p->a[hx] = f->Hxy_z_l->a[k0] + f->Hxz_z_l->a[k0];
                        }
                        else if (j1 > p->hy_h)
                        {
                            j0 = INDEX_P(f->Hxy_y_h,i,j1 - p->hoffy_h,k);
                            pj = p->hpmly_h - j1;

                            f->Hxy_y_h->a[j0] = p->c_ey[pj] * f->Hxy_y_h->a[j0] + p->d_ey[pj] * dez;
                            f->Hxz_z_l->a[k0] = p->c_hz[pk] * f->Hxz_z_l->a[k0] - p->d_hz[pk] * dey;
                            Hx_p->a[hx] = f->Hxy_y_h->a[j0] + f->Hxz_z_l->a[k0];
                        }
                        else
                        {
                            j0 = INDEX_P(f->Hxy_y_l,i,j - p->hoffy_l,k);
                            pj = j1 - p->hpmly_l;

                            f->Hxy_y_l->a[j0] = p->c_hy[pj] * f->Hxy_y_l->a[j0] + p->d_hy[pj] * dez;
                            f->Hxz_z_l->a[k0] = p->c_hz[pk] * f->Hxz_z_l->a[k0] - p->d_hz[pk] * dey;
                            Hx_p->a[hx] = f->Hxy_y_l->a[j0] + f->Hxz_z_l->a[k0];
                        }
                    }
                    else
                    {
                        k0 = INDEX_P(f->Hxy_z_h,i,j,k1 - p->hoffz_h);
                        pk = p->hpmlz_h - k1;

                        if (j1 >= p->hy_l && j1 <= p->hy_h)
                        {
                            f->Hxy_z_h->a[k0] += -s->dt_dy * dez;
                            f->Hxz_z_h->a[k0] = p->c_ez[pk] * f->Hxz_z_h->a[k0] - p->d_ez[pk] * dey;
                            Hx_p->a[hx] = f->Hxy_z_h->a[k0] + f->Hxz_z_h->a[k0];
                        }
                        else if (j1 > p->hy_h)
                        {
                            j0 = INDEX_P(f->Hxy_y_h,i,j1 - p->hoffy_h,k);
                            pj = p->hpmly_h - j1;

                            f->Hxy_y_h->a[j0] = p->c_ey[pj] * f->Hxy_y_h->a[j0] + p->d_ey[pj] * dez;
                            f->Hxz_z_h->a[k0] = p->c_ez[pk] * f->Hxz_z_h->a[k0] - p->d_ez[pk] * dey;
                            Hx_p->a[hx] = f->Hxy_y_h->a[j0] + f->Hxz_z_h->a[k0];

                        }
                        else
                        {
                            j0 = INDEX_P(f->Hxy_y_l,i,j - p->hoffy_l,k);
                            pj = j1 - p->hpmly_l;

                            f->Hxy_y_l->a[j0] = p->c_hy[pj] * f->Hxy_y_l->a[j0] + p->d_hy[pj] * dez;
                            f->Hxz_z_h->a[k0] = p->c_ez[pk] * f->Hxz_z_h->a[k0] - p->d_ez[pk] * dey;
                            Hx_p->a[hx] = f->Hxy_y_l->a[j0] + f->Hxz_z_h->a[k0];
                        }
                    }
                } /* main part */
            } /* j */
        } /* i */
    } /* k */
#ifdef USENDF
        MPI_EXCHANGE_UP_3D_Y_1COMP(&mpi, g_p, Hx_p);
        MPI_EXCHANGE_UP_3D_Z_1COMP(&mpi, g_p, Hx_p);
#endif
        MPI_EXCHANGE_DOWN_3D_Y_1COMP(&mpi, g_p, Hx_p);
        MPI_EXCHANGE_DOWN_3D_Z_1COMP(&mpi, g_p, Hx_p);
}

void update_3d_hy(vfield_n* ve_p,
                  vfield_n* vh_p,
                  geometry_data* g_p,
                  pml_data* p,
                  scheme_constants* s)
{
    int i,j,k,hy,pi,i0,pk,k0;
    field_n* Hy_p = vh_p->comp_p[y_e];
    field_n* Ex_p = ve_p->comp_p[x_e];
    field_n* Ez_p = ve_p->comp_p[z_e];
    pml_fields* f = &p->pml_f;

    int dx = DX_P(Hy_p);
    int dz = DZ_P(Hy_p);

#ifdef ONE_CONFIG_OPENMP
#pragma omp parallel for private(i,j,hy,pi,i0,pk,k0)
#endif

    for (k=g_p->kmin - ADD; k<g_p->kmax; k++)
    {
        int k1 = k + g_p->k_offset;
        for (i=g_p->imin - ADD; i<g_p->imax; i++)
        {
            int i1 = i + g_p->i_offset;
	    for (j=g_p->jmin-1; j<g_p->jmax+1; j++)
            {
		hy = INDEX_P(Hy_p,i,j,k);

#ifdef USENDF
		FP dez = s->b_z * (Ez_p->a[hy + dx] - Ez_p->a[hy]) +
		    s->a_z * (Ez_p->a[hy - dz + dx] - Ez_p->a[hy - dz] +
			      Ez_p->a[hy + dz + dx] - Ez_p->a[hy + dz]);
		FP dex = s->b_x * (Ex_p->a[hy + dz] - Ex_p->a[hy]) +
		    s->a_x * (Ex_p->a[hy - dx + dz] - Ex_p->a[hy - dx] +
			      Ex_p->a[hy + dx + dz] - Ex_p->a[hy + dx]);

#else
		FP dez = s->dc_y * (Ez_p->a[hy + dx] - Ez_p->a[hy]);
		FP dex = s->dc_y * (Ex_p->a[hy + dz] - Ex_p->a[hy]);
#endif

	        if (k1 >= p->hz_l && k1 <= p->hz_h && i1 >= p->hx_l && i1 <= p->hx_h)
	        {
		    Hy_p->a[hy] += s->dt_dx * dez - s->dt_dz * dex;
                }
                else
                {
                    if (k1 >= p->hz_l && k1 <= p->hz_h)
                    {
	                if (i1 > p->hx_h)
                        {
                            i0 = INDEX_P(f->Hyx_x_h,i1 - p->hoffx_h,j,k);
                            pi = p->hpmlx_h - i1;

                            f->Hyx_x_h->a[i0] = p->c_ex[pi]*f->Hyx_x_h->a[i0] - p->d_ex[pi] * dez;
                            f->Hyz_x_h->a[i0] += -s->dt_dz * dex;
                            Hy_p->a[hy] = f->Hyx_x_h->a[i0] + f->Hyz_x_h->a[i0];
                        }
                        else
                        {
                            i0 = INDEX_P(f->Hyx_x_l,i - p->hoffx_l,j,k);
                            pi = i1 - p->hpmlx_l;

                            f->Hyx_x_l->a[i0] = p->c_hx[pi] * f->Hyx_x_l->a[i0] - p->d_hx[pi] * dez;
                            f->Hyz_x_l->a[i0] += -s->dt_dz * dex;
			    Hy_p->a[hy] = f->Hyx_x_l->a[i0] + f->Hyz_x_l->a[i0];
                        }
                    }
                    else if(k1 < p->hz_l)
                    {
                        k0 = INDEX_P(f->Hyx_z_l,i,j,k - p->hoffz_l);
                        pk = k1 - p->hpmlz_l;

                        if (i1 >= p->hx_l && i1 <= p->hx_h)
                        {
	                    f->Hyx_z_l->a[k0] += s->dt_dx * dez;
                            f->Hyz_z_l->a[k0] = p->c_hz[pk] * f->Hyz_z_l->a[k0] + p->d_hz[pk] * dex;
                            Hy_p->a[hy] = f->Hyx_z_l->a[k0] + f->Hyz_z_l->a[k0];
                        }
                        else if (i1 > p->hx_h)
                        {
                            i0 = INDEX_P(f->Hyx_x_h,i1 - p->hoffx_h,j,k);
                            pi = p->hpmlx_h - i1;

                            f->Hyx_x_h->a[i0] = p->c_ex[pi] * f->Hyx_x_h->a[i0] - p->d_ex[pi] * dez;
                            f->Hyz_z_l->a[k0] = p->c_hz[pk] * f->Hyz_z_l->a[k0] + p->d_hz[pk] * dex;
                            Hy_p->a[hy] = f->Hyx_x_h->a[i0] + f->Hyz_z_l->a[k0];
                        }
                        else
                        {
                            i0 = INDEX_P(f->Hyx_x_l,i - p->hoffx_l,j,k);
                            pi = i1 - p->hpmlx_l;

                            f->Hyx_x_l->a[i0] = p->c_hx[pi] * f->Hyx_x_l->a[i0] - p->d_hx[pi] * dez;
                            f->Hyz_z_l->a[k0] = p->c_hz[pk] * f->Hyz_z_l->a[k0] + p->d_hz[pk] * dex;
                            Hy_p->a[hy] = f->Hyx_x_l->a[i0] + f->Hyz_z_l->a[k0];
                        }
                    }
                    else
                    {
                        k0 = INDEX_P(f->Hyx_z_h,i,j,k1 - p->hoffz_h);
                        pk = p->hpmlz_h - k1;

                        if (i1 >= p->hx_l && i1 <= p->hx_h)
                        {
	                    f->Hyx_z_h->a[k0] += s->dt_dx * dez;
                            f->Hyz_z_h->a[k0] = p->c_ez[pk] * f->Hyz_z_h->a[k0] + p->d_ez[pk] * dex;
                            Hy_p->a[hy] = f->Hyx_z_h->a[k0] + f->Hyz_z_h->a[k0];
                        }
                        else if (i1 > p->hx_h)
                        {
                            i0 = INDEX_P(f->Hyx_x_h,i1 - p->hoffx_h,j,k);
                            pi = p->hpmlx_h - i1;

                            f->Hyx_x_h->a[i0] = p->c_ex[pi] * f->Hyx_x_h->a[i0] - p->d_ex[pi] * dez;
                            f->Hyz_z_h->a[k0] = p->c_ez[pk] * f->Hyz_z_h->a[k0] + p->d_ez[pk] * dex;
                            Hy_p->a[hy] = f->Hyx_x_h->a[i0] + f->Hyz_z_h->a[k0];

                        }
                        else
                        {
                            i0 = INDEX_P(f->Hyx_x_l,i - p->hoffx_l,j,k);
                            pi = i1 - p->hpmlx_l;

                            f->Hyx_x_l->a[i0] = p->c_hx[pi] * f->Hyx_x_l->a[i0] - p->d_hx[pi] * dez;
                            f->Hyz_z_h->a[k0] = p->c_ez[pk] * f->Hyz_z_h->a[k0] + p->d_ez[pk] * dex;
                            Hy_p->a[hy] = f->Hyx_x_l->a[i0] + f->Hyz_z_h->a[k0];
                        }
                    }
                } /* main part */
            } /* j */
        } /* i */
    } /* k */
#ifdef USENDF
	MPI_EXCHANGE_UP_3D_X_1COMP(&mpi, g_p, Hy_p);
        MPI_EXCHANGE_UP_3D_Z_1COMP(&mpi, g_p, Hy_p);
#endif
        MPI_EXCHANGE_DOWN_3D_X_1COMP(&mpi, g_p, Hy_p);
        MPI_EXCHANGE_DOWN_3D_Z_1COMP(&mpi, g_p, Hy_p);
}

void update_3d_hz(vfield_n* ve_p,
                  vfield_n* vh_p,
                  geometry_data* g_p,
                  pml_data* p,
                  scheme_constants* s)
{
    int i,j,k,hz,pj,j0,pi,i0;
    field_n* Hz_p = vh_p->comp_p[z_e];
    field_n* Ey_p = ve_p->comp_p[y_e];
    field_n* Ex_p = ve_p->comp_p[x_e];
    pml_fields* f = &p->pml_f;

    int dx = DX_P(Hz_p);
    int dy = DY_P(Hz_p);

#ifdef ONE_CONFIG_OPENMP
#pragma omp parallel for private(i,j,hz,pj,j0,pi,i0)
#endif

    for (k = g_p->kmin-1; k < g_p->kmax+1; k++)
    {
        for (i = g_p->imin - ADD; i < g_p->imax; i++)
        {
            int i1 = i + g_p->i_offset;
            for (j = g_p->jmin - ADD; j<g_p->jmax; j++)
            {
	        int j1 = j + g_p->j_offset;
		hz = INDEX_P(Hz_p,i,j,k);

#ifdef USENDF
		FP dex = s->b_x * (Ex_p->a[hz + dy] - Ex_p->a[hz]) +
		    s->a_x * (Ex_p->a[hz - dx + dy] - Ex_p->a[hz - dx] +
			      Ex_p->a[hz + dx + dy] - Ex_p->a[hz + dx]);
		FP dey = s->b_y * (Ey_p->a[hz + dx] - Ey_p->a[hz]) +
		    s->a_y * (Ey_p->a[hz - dy + dx] - Ey_p->a[hz - dy] +
			      Ey_p->a[hz + dy + dx] - Ey_p->a[hz + dy]);

#else
		FP dex = s->dc_z * (Ex_p->a[hz + dy] - Ex_p->a[hz]);
		FP dey = s->dc_z * (Ey_p->a[hz + dx] - Ey_p->a[hz]);
#endif

	        if (i1 >= p->hx_l && i1 <= p->hx_h && j1 >= p->hy_l && j1 <= p->hy_h)
	        {
		    Hz_p->a[hz] += s->dt_dy * dex - s->dt_dx * dey;
                }
                else
                {
                    if (i1 >= p->hx_l && i1 <= p->hx_h)
                    {
                        if (j1 > p->hy_h)
                        {
                            j0 = INDEX_P(f->Hzy_y_h,i,j1 - p->hoffy_h,k);
                            pj = p->hpmly_h - j1;

                            f->Hzy_y_h->a[j0] = p->c_ey[pj] * f->Hzy_y_h->a[j0] - p->d_ey[pj] * dex;
                            f->Hzx_y_h->a[j0] += -s->dt_dx * dey;
                            Hz_p->a[hz] = f->Hzy_y_h->a[j0] + f->Hzx_y_h->a[j0];
                        }
                        else
                        {
                            j0 = INDEX_P(f->Hzy_y_l,i,j - p->hoffy_l,k);
                            pj = j1 - p->hpmly_l;

                            f->Hzy_y_l->a[j0] = p->c_hy[pj] * f->Hzy_y_l->a[j0] - p->d_hy[pj] * dex;
                            f->Hzx_y_l->a[j0] += -s->dt_dx * dey;
                            Hz_p->a[hz] = f->Hzy_y_l->a[j0] + f->Hzx_y_l->a[j0];
                        }
                    }
                    else if(i1 < p->hx_l)
                    {
                        i0 = INDEX_P(f->Hzy_x_l,i - p->hoffx_l,j,k);
                        pi = i1 - p->hpmlx_l;

                        if (j1 >= p->hy_l && j1 <= p->hy_h)
                        {
	                    f->Hzy_x_l->a[i0] += s->dt_dy * dex;
                            f->Hzx_x_l->a[i0] = p->c_hx[pi] * f->Hzx_x_l->a[i0] + p->d_hx[pi] * dey;
                            Hz_p->a[hz] = f->Hzy_x_l->a[i0] + f->Hzx_x_l->a[i0];
                        }
                        else if (j1 > p->hy_h)
                        {
                            j0 = INDEX_P(f->Hzy_y_h,i,j1 - p->hoffy_h,k);
                            pj = p->hpmly_h - j1;

                            f->Hzy_y_h->a[j0] = p->c_ey[pj] * f->Hzy_y_h->a[j0] - p->d_ey[pj] * dex;
                            f->Hzx_x_l->a[i0] = p->c_hx[pi] * f->Hzx_x_l->a[i0] + p->d_hx[pi] * dey;
                            Hz_p->a[hz] = f->Hzy_y_h->a[j0] + f->Hzx_x_l->a[i0];
                        }
                        else
                        {
                            j0 = INDEX_P(f->Hzy_y_l,i,j - p->hoffy_l,k);
                            pj = j1 - p->hpmly_l;

                            f->Hzy_y_l->a[j0] = p->c_hy[pj] * f->Hzy_y_l->a[j0] - p->d_hy[pj] * dex;
                            f->Hzx_x_l->a[i0] = p->c_hx[pi] * f->Hzx_x_l->a[i0] + p->d_hx[pi] * dey;
                            Hz_p->a[hz] = f->Hzy_y_l->a[j0] + f->Hzx_x_l->a[i0];
                        }
                    }
                    else
                    {
                        i0 = INDEX_P(f->Hzy_x_h,i1 - p->hoffx_h,j,k);
                        pi = p->hpmlx_h - i1;

                        if (j1 >= p->hy_l && j1 <= p->hy_h)
                        {
	                    f->Hzy_x_h->a[i0] += s->dt_dy * dex;
                            f->Hzx_x_h->a[i0] = p->c_ex[pi] * f->Hzx_x_h->a[i0] + p->d_ex[pi] * dey;
                            Hz_p->a[hz] = f->Hzy_x_h->a[i0] + f->Hzx_x_h->a[i0];
                        }
                        else if (j1 > p->hy_h)
                        {
                            j0 = INDEX_P(f->Hzy_y_h,i,j1 - p->hoffy_h,k);
                            pj = p->hpmly_h - j1;

                            f->Hzy_y_h->a[j0] = p->c_ey[pj] * f->Hzy_y_h->a[j0] - p->d_ey[pj] * dex;
                            f->Hzx_x_h->a[i0] = p->c_ex[pi] * f->Hzx_x_h->a[i0] + p->d_ex[pi] * dey;
                            Hz_p->a[hz] = f->Hzy_y_h->a[j0] + f->Hzx_x_h->a[i0];
                        }
                        else
                        {
                            j0 = INDEX_P(f->Hzy_y_l,i,j - p->hoffy_l,k);
                            pj = j1 - p->hpmly_l;

                            f->Hzy_y_l->a[j0] = p->c_hy[pj] * f->Hzy_y_l->a[j0] - p->d_hy[pj] * dex;
                            f->Hzx_x_h->a[i0] = p->c_ex[pi] * f->Hzx_x_h->a[i0] + p->d_ex[pi] * dey;
                            Hz_p->a[hz] = f->Hzy_y_l->a[j0] + f->Hzx_x_h->a[i0];
                        }
                    }
                } /* main part */
            } /* j */
        } /* i */
    } /* k */
#ifdef USENDF
	MPI_EXCHANGE_UP_3D_X_1COMP(&mpi, g_p, Hz_p);
        MPI_EXCHANGE_UP_3D_Y_1COMP(&mpi, g_p, Hz_p);
#endif
        MPI_EXCHANGE_DOWN_3D_X_1COMP(&mpi, g_p, Hz_p);
        MPI_EXCHANGE_DOWN_3D_Y_1COMP(&mpi, g_p, Hz_p);
}

#endif /* FULL UPDATE */

void calculate_e_from_d_3d(vfield_n* vfd_p,
                           vfield_n* vfe_p,
                           media_data* media_p,
                           geometry_data* g_p)
{
    if (media_p->recalc == false)
    {
	return;
    }

    int index;

    field_n* Ex_p = vfe_p->comp_p[x_e];
    field_n* Ey_p = vfe_p->comp_p[y_e];
    field_n* Ez_p = vfe_p->comp_p[z_e];
    field_n* Dx_p = vfd_p->comp_p[x_e];
    field_n* Dy_p = vfd_p->comp_p[y_e];
    field_n* Dz_p = vfd_p->comp_p[z_e];

    int nm = media_p->epsilon.nx * media_p->epsilon.ny * media_p->epsilon.nz;

    if (media_p->kerr > 0)
    {
#ifdef ONE_CONFIG_OPENMP
#pragma omp parallel for
#endif
        for (index=0; index < nm; index++)
        {
	    if (media_p->smoothing == false || media_p->t_eps_idx.a[index] == 0)
	    {
		if (media_p->n_max.a[index] > 0.)
		{
		    int i_temp = 0;
		    FP  abs, eps, eps_o;
		    FP  Dx_t, Dy_t, Dz_t, D2;

		    Dx_t = Dx_p->a[index];
		    Dy_t = Dy_p->a[index];
		    Dz_t = Dz_p->a[index];

		    D2 = Dx_t*Dx_t + Dy_t*Dy_t + Dz_t*Dz_t;

		    eps = media_p->update_eps(media_p, D2, index);

		    do
		    {
			eps_o = eps;

			abs = D2/(eps*eps);
			eps = media_p->update_eps(media_p, abs, index);
			i_temp++;
		    }
		    while(fabs(eps_o - eps) > 1e-5 && i_temp < 20);

		    Ex_p->a[index] = Dx_t / eps;
		    Ey_p->a[index] = Dy_t / eps;
		    Ez_p->a[index] = Dz_t / eps;
		}
		else
		{
		    Ex_p->a[index] = Dx_p->a[index]/media_p->epsilon.a[index];
		    Ey_p->a[index] = Dy_p->a[index]/media_p->epsilon.a[index];
		    Ez_p->a[index] = Dz_p->a[index]/media_p->epsilon.a[index];
		}
	    }
	    else
	    {
		FP  Dx, Dy, Dz;
		int idx = media_p->t_eps_idx.a[index];
		Dx = Dx_p->a[index];
		Dy = Dy_p->a[index];
		Dz = Dz_p->a[index];
		Ex_p->a[index] = calc_ex_from_d(&media_p->t_eps[idx], Dx, Dy, Dz);
		Ey_p->a[index] = calc_ey_from_d(&media_p->t_eps[idx], Dx, Dy, Dz);
		Ez_p->a[index] = calc_ez_from_d(&media_p->t_eps[idx], Dx, Dy, Dz);
	    }
        }
    }
    else /* no kerr */
    {
#ifdef ONE_CONFIG_OPENMP
#pragma omp parallel for
#endif
        for (index=0; index < nm; index++)
        {
	    if (media_p->smoothing == false || media_p->t_eps_idx.a[index] == 0)
	    {
		FP eps;

		if (media_p->n_max.a[index] > 0.)
		{
		    eps = media_p->update_eps(media_p, 0, index);
		}
		else
		{
		    eps = media_p->epsilon.a[index];
		}

		Ex_p->a[index] = Dx_p->a[index] / eps;
		Ey_p->a[index] = Dy_p->a[index] / eps;
		Ez_p->a[index] = Dz_p->a[index] / eps;
	    }
	    else
	    {
		FP  Dx, Dy, Dz;
		int idx = media_p->t_eps_idx.a[index];
		Dx = Dx_p->a[index];
		Dy = Dy_p->a[index];
		Dz = Dz_p->a[index];
		Ex_p->a[index] = calc_ex_from_d(&media_p->t_eps[idx], Dx, Dy, Dz);
		Ey_p->a[index] = calc_ey_from_d(&media_p->t_eps[idx], Dx, Dy, Dz);
		Ez_p->a[index] = calc_ez_from_d(&media_p->t_eps[idx], Dx, Dy, Dz);
	    }
	}
    }
}

int calculate_pulse_center(vfield_n* ve_p,
                           vfield_n* vd_p,
                           vfield_n* vh_p,
                           geometry_data* g_p)
{
    field_n* Ex_p = ve_p->comp_p[x_e];
    field_n* Ey_p = ve_p->comp_p[y_e];
    field_n* Ez_p = ve_p->comp_p[z_e];
    field_n* Dx_p = vd_p->comp_p[x_e];
    field_n* Dy_p = vd_p->comp_p[y_e];
    field_n* Dz_p = vd_p->comp_p[z_e];
    field_n* Hx_p = vh_p->comp_p[x_e];
    field_n* Hy_p = vh_p->comp_p[y_e];
    field_n* Hz_p = vh_p->comp_p[z_e];

    FP energy = 0.;
    FP de = 0.;
    FP temp = 0.;
    int z_c = 0;

    for (int k=g_p->kmin; k<g_p->kmax+1; k++)
    {
        for (int i=g_p->imin; i<g_p->imax+1; i++)
        {
            for (int j=g_p->jmin; j<g_p->jmax+1; j++)
            {
                int index = INDEX_P(Ex_p,i,j,k);

                de = Hx_p->a[index]*Hx_p->a[index] +
                     Hy_p->a[index]*Hy_p->a[index] +
                     Hz_p->a[index]*Hz_p->a[index] +
                     Ex_p->a[index]*Dx_p->a[index] +
                     Ey_p->a[index]*Dy_p->a[index] +
                     Ez_p->a[index]*Dz_p->a[index];
                energy += de;
                temp += k*de;

            }
        }
    }

#ifdef USEMPI
    FP energy_sum = 0.;
    FP temp_sum = 0.;

    MPI_Allreduce((void*)&energy,
                  (void*)&energy_sum,
                  1,
                  MPI_FP,
                  MPI_SUM,
                  MPI_COMM_WORLD);

    MPI_Allreduce((void*)&temp,
                  (void*)&temp_sum,
                  1,
                  MPI_FP,
                  MPI_SUM,
                  MPI_COMM_WORLD);

    energy  = energy_sum;
    temp = temp_sum;
#endif

    z_c = (int)(temp/energy);
    return z_c;
}

void calculate_energy_3d(vfield_n* ve_p,
                         vfield_n* vd_p,
                         vfield_n* vh_p,
                         media_data* media_p,
			 time_scheme* t_p,
                         geometry_data* g_p,
			 int t,
			 const char* pathPrefix)
{
    field_n* Ex_p = ve_p->comp_p[x_e];
    field_n* Ey_p = ve_p->comp_p[y_e];
    field_n* Ez_p = ve_p->comp_p[z_e];
    field_n* Dx_p = vd_p->comp_p[x_e];
    field_n* Dy_p = vd_p->comp_p[y_e];
    field_n* Dz_p = vd_p->comp_p[z_e];
    field_n* Hx_p = vh_p->comp_p[x_e];
    field_n* Hy_p = vh_p->comp_p[y_e];
    field_n* Hz_p = vh_p->comp_p[z_e];

    if(t%100 == 0)
    {
        FP energy = 0.;

        for (int k=g_p->kmin; k<g_p->kmax + ADD; k++)
        {
            for (int i=g_p->imin; i<g_p->imax + ADD; i++)
            {
                for (int j=g_p->jmin; j<g_p->jmax + ADD; j++)
                {

                    int index = INDEX_P(Ex_p,i,j,k);

                    if (media_p->media == drop_e)
                    {
                        if (media_p->epsilon.a[index] > 1)
                        {
                            /* calculate only inner energy */
                            energy += Hx_p->a[index]*Hx_p->a[index] +
                                      Hy_p->a[index]*Hy_p->a[index] +
                                      Hz_p->a[index]*Hz_p->a[index] +
                                      Ex_p->a[index]*Dx_p->a[index] +
                                      Ey_p->a[index]*Dy_p->a[index] +
                                      Ez_p->a[index]*Dz_p->a[index];
                        }
                    }
                    else
                    {
                        energy += Hx_p->a[index]*Hx_p->a[index] +
                                  Hy_p->a[index]*Hy_p->a[index] +
                                  Hz_p->a[index]*Hz_p->a[index] +
                                  Ex_p->a[index]*Dx_p->a[index] +
                                  Ey_p->a[index]*Dy_p->a[index] +
                                  Ez_p->a[index]*Dz_p->a[index];
                    }

		}
            }
        }

#ifdef USEMPI
        FP energy_sum = 0.;

        MPI_Reduce((void*)&energy,
                   (void*)&energy_sum,
                   1,
                   MPI_FP,
                   MPI_SUM,
                   0,
                   MPI_COMM_WORLD);

        energy  = energy_sum;
#endif
        RANK_S(0);

	static FILE* out = NULL;

	if (out == NULL)
	{
	    char tmp[1024];
	    sprintf(tmp, "%s/energy.dat", pathPrefix);

	    if (NULL == (out = fopen(tmp, "w")))
	    {
		fprintf(stderr, "Unable to open %s for writing\n", tmp);
		exit(-1);
	    }
	}

	fprintf(out, "%lf %le\n", t * t_p->delta_t * 3.33333333 , energy*media_p->c_en);
	fflush(out);

        if (energy*media_p->c_en > media_p->max_energy)
        {
            media_p->max_energy = media_p->c_en*energy;
        }

        fprintf(stderr, "Time step: %6d Energy: %12.10e[mJ] zmin=%d zmax=%d\n",
                t, energy*media_p->c_en,
                g_p->move_index,
                g_p->z_max + g_p->move_index);
        fflush(stderr);

	if (t == t_p->t_max)
	{
	    fclose(out);
	}

        RANK_F(0);
    }
}
#ifndef CUDA
/**  This function is the main work function for 3D case. This function consists of the following steps:
 *   \li 1. Initialisation of constants, pml, media, sources, near-to-far field transform
 *   \li 2. Fields initialisation. If Kerr media is used, or epsilon > 1 both D and E fields are used.
 *   In this case field named "E[xyz]" actually means D[xyz], and E[xyz]_r are the "real" E fields.
 *   First D is updated through Masxwell equation, and later it is recalculated to E. If no Kerr media
 *   is used and epsilon=1, E[xyz]_r are not allocated as D=E everywhere. In order to make program look
 *   the same in both cases, additional pointers E[xyz]_p are used. In the first case these pointers point
 *   to E[xyz]_r, in the second - to E[xyz]. Fields H[xyz] and J[xyz] are also allocated. For the case of moving
 *   window Jex, Jey, Jhx, Jhy are also allocated, note that moving window technique for 3D case is implemented
 *   only for MPI version.
 *   \li 3. MPI: MPI structures are initialised and signal handler is installed (SIGUSR1).
 *   On msc it is claimed that it is possible to start mpirun with additional command line option, so few minutes before calculation time is off
 *   system will sent user chosen signal (USR1), so program can store all needed data for restart.
 *   [On msc cluster for some reasons it does not work.]
 *   \li 4. If program is restarted, that is t start not equal 0, program reads in previously
 *   saved data and recalculate E from D, if either Kerr nonlinearity >0, or eps != 1.
 *   \li 5. **** Main loop is started. ****
 *   \li 6. MPI: Signal USR1 is blocked, to avoid interrupt during main loop execution.
 *   \li 7. For the case of moving front decomposition is updated.
 *   \li 8. Sources data are calculated and stored in additional arrays.
 *   \li 9. D fields are updated through Maxwell equation.
 *   \li 10. Sources for D are added.
 *   \li 11. MPI: D field is sent to upper node in x,y directions.
 *   \li 12. MPI: If moving window is not used, D field is sent to upper node in z direction
 *   Otherwise, D is transerred from sender to receiver, and H currents are updated.
 *   \li 13. Field E is recalculated from D
 *   \li 14. H fields are updated through Maxwell equation.
 *   \li 15. Sources for H are added.
 *   \li 16. MPI: H field is sent to upper node in x,y directions.
 *   \li 17. MPI: If moving window is not used, H field is sent to upper node in z direction
 *   Otherwise, H is transerred from sender to receiver, and D currents are updated.
 *   \li 18. Ionization is calculated
 *   \li 19. If results needed to be stored, all fields are dumped to disk.
 *   \li 20. Near-to-far transform is updated.
 *   \li 21. Slices and 3D data are stored to disk.
 *   \li 22. Energy is calculated
 *   \li 23. Points data are stored to disk.
 *   \li 24. If restart flag was set to 1, results are dumped to disk and flag is reset to 0.
 *   \li 25. Signal USR1 is unblocked.
 *   \li 26. **** Main loop is finished ****
 *   \li 27. Fields are destroyed.
 *   \li 28. Media, pml, point data structures are destroyed.
 *   \li 29. Near-to-far transform data are stored to disk and destroyed.
 *   \li 30. MPI: MPI structures are destroyed.
 *   \author  Evgeny Efimenko
 *   \param[in]   fdtdConfig - pointer to configTable structure containing configuration
 *   \param[in]   geometry - geometry_data structure, containing all geometry related data
 *   \param[in]   time_scheme - time_scheme structure, containing all time related information
 *   \param[in]   pathPrefix - pointer to char array containing root folder path
 *   \return  void
 *   \callgraph
 *   \callergraph
 */
int run_3D_config(configTable* fdtdConfig,
                  geometry_data geometry,
                  time_scheme timescheme,
                  const char* pathPrefix)
{
    media_data media;
    pml_data pml;
    sources_rt_data sources;
    scheme_constants scheme;
    points_data_3d pdata;
    save_data sdata;

    vfield_n *E_p, *D_p, *H_p, *J_p;

    E_p = init_vfield_n("E", geometry.x_max, geometry.y_max, geometry.z_max);
    H_p = init_vfield_n("H", geometry.x_max, geometry.y_max, geometry.z_max);
    J_p = init_vfield_n("J", geometry.x_max, geometry.y_max, geometry.z_max);

    init_constants(&scheme, &geometry, &timescheme);
    init_pml(fdtdConfig, &pml, &geometry, &timescheme, &scheme);
    
    init_media(fdtdConfig, &media, &geometry, &timescheme, &scheme);
    init_savedata(fdtdConfig, &sdata);
    
    if (media.recalc == true)
    {
	D_p = init_vfield_n("D", geometry.x_max, geometry.y_max, geometry.z_max);
    }
    else
    {
	D_p = E_p;
    }
    
    init_sources_rt(fdtdConfig, &geometry, &pml, &sources, D_p, H_p);
    init_moving_window_3d(&geometry, &sources, &pml, &timescheme, &scheme, pathPrefix);
    
    init_point_data_3d(fdtdConfig,
                       &pdata,
                       pathPrefix,
                       &timescheme,
                       &geometry,
                       E_p,
                       H_p);

    MPI_INIT_SENDRECV_3D(&mpi, &geometry, D_p, H_p);

    RANK_S(0);
    print_sources_rt(&sources, &media, &geometry, &timescheme, pathPrefix);
    print_media(&media);
    RANK_F(0);

    if (timescheme.t_start > 0)
    {
	RANK_S(0);
	fprintf(stderr, "Reading in dumped fields for t=%d\n", timescheme.t_start);
	RANK_F(0);

        read_dump(&timescheme,
		  D_p,
		  H_p,
		  J_p,
		  &pml,
		  &media,
		  pathPrefix);
    }

    RANK_S(0);
    fprintf(stderr, "\nRunning main loop\n");
    RANK_F(0);

    for (int t = timescheme.t_start + 1; t < timescheme.t_max+1; t++)
    {

        update_moving_front_3d(&geometry, &timescheme, t);

        update_sources_rt3d(&sources, &geometry, t);

        update_3d_dx(D_p, H_p, J_p, &geometry, &pml, &scheme);
        update_3d_dy(D_p, H_p, J_p, &geometry, &pml, &scheme);
        update_3d_dz(D_p, H_p, J_p, &geometry, &pml, &scheme);

#ifdef USENDF
	    MPI_EXCHANGE_DOWN_3D_X_2COMP(&mpi, &geometry, D_p);
        MPI_EXCHANGE_DOWN_3D_Y_2COMP(&mpi, &geometry, D_p);
        MPI_EXCHANGE_DOWN_3D_Z_2COMP(&mpi, &geometry, D_p);
#endif
        MPI_EXCHANGE_UP_3D_X_2COMP(&mpi, &geometry, D_p);
        MPI_EXCHANGE_UP_3D_Y_2COMP(&mpi, &geometry, D_p);
        MPI_EXCHANGE_UP_3D_Z_2COMP(&mpi, &geometry, D_p);

        add_sources_3d_e(D_p, &sources, &scheme);

	calculate_e_from_d_3d(D_p, E_p, &media, &geometry);

	update_3d_hx(E_p, H_p, &geometry, &pml, &scheme);
        update_3d_hy(E_p, H_p, &geometry, &pml, &scheme);
        update_3d_hz(E_p, H_p, &geometry, &pml, &scheme);

#ifdef USENDF
	MPI_EXCHANGE_UP_3D_X_2COMP(&mpi, &geometry, H_p);
        MPI_EXCHANGE_UP_3D_Y_2COMP(&mpi, &geometry, H_p);
        MPI_EXCHANGE_UP_3D_Z_2COMP(&mpi, &geometry, H_p);
#endif
        MPI_EXCHANGE_DOWN_3D_X_2COMP(&mpi, &geometry, H_p);
        MPI_EXCHANGE_DOWN_3D_Y_2COMP(&mpi, &geometry, H_p);
        MPI_EXCHANGE_DOWN_3D_Z_2COMP(&mpi, &geometry, H_p);

        add_sources_3d_h(H_p, &sources, &scheme);

	save_dump(t,
		  &timescheme,
		  D_p,
		  H_p,
		  J_p,
		  &pml,
		  &media,
		  pathPrefix);

	savedata_save_3d(&sdata, E_p, &geometry, &media, t, pathPrefix);

        if (media.type == so_e)
        {
            store_point_data_3d(&pdata,
                                &geometry,
                                t,
                                E_p,
                                H_p,
				&media);
        }
        else
        {
            store_point_data_3d(&pdata,
                                &geometry,
                                t,
                                E_p,
                                H_p,
				&media);
	}

	update_moving_window_3d(D_p, E_p, H_p, J_p,
				&geometry,
				&timescheme,
				&media,
				&pml,
				&pdata,
				&sources,
				&scheme,
				t,
				pathPrefix);


        calculate_energy_3d(E_p, D_p, H_p, &media, &timescheme, &geometry, t, pathPrefix);
    } /*for t */

    finalize_moving_window_3d(&geometry, &timescheme, &media, pathPrefix);

    destroy_sources_rt(&sources, &geometry);
    destroy_vfield_n(E_p);
    destroy_vfield_n(H_p);
    destroy_vfield_n(J_p);

    if (media.recalc == true)
    {
	destroy_vfield_n(D_p);
    }

    destroy_media(&media);
    destroy_pml3d(&pml);
    destroy_point_data_3d(&pdata);

    MPI_DELETE_SENDRECV_3D(&mpi);
    return 0;
}
#endif /* CUDA */

void init_moving_window_3d(geometry_data* g_p,
			   sources_rt_data* s_p,
			   pml_data* p,
			   time_scheme* ts_p,
			   scheme_constants* sch_p,
			   const char* pathPrefix)
{
    if (g_p->moving == 2)
    {
	char dirname[1024];

        sprintf(dirname, "%s/data/moving", pathPrefix);
        RANK_S(0)
        create_directory(dirname);
        RANK_F(0)

	if (s_p->pulses.pulse_p[0].env.type == tanh_e)
	{
	    ts_p->t0 = (int)((s_p->pulses.pulse_p[0].tdelay + g_p->z_max*g_p->delta_z*0.8)/ts_p->delta_t);
	}
	else
	{
	    ts_p->t0 = (int)((s_p->pulses.pulse_p[0].tdelay + g_p->z_max * g_p->delta_z * 0.8)/ts_p->delta_t);
	}

	g_p->move = (int)(ts_p->t_move * ts_p->delta_t * ts_p->v_move/ g_p->delta_z) + 1;
	g_p->move_index = 0;

	if (ts_p->t_start > ts_p->t0)
	{
	    g_p->move_index = p->pmlz + 2 + (ts_p->t_start - ts_p->t0)/ts_p->t_move*g_p->move;
	    zero_pml_z(p, g_p, ts_p, sch_p);
	    zero_sources_rt(s_p);
	}

	int n_shift = ((ts_p->t_max - ts_p->t0)/ts_p->t_move + 1);

	g_p->move_max = g_p->move*n_shift+g_p->z_max;
	RANK_S(0)
	    fprintf(stderr, "Moving window: z_min=%lf t_move=%d t0=%d move=%d\n",
		    (int)(6.*s_p->pulses.pulse_p[0].env.t0 + s_p->pulses.pulse_p[0].env.tp)/g_p->delta_z,
		    ts_p->t_move,
		    ts_p->t0,
		    g_p->move);
	RANK_F(0)
   }
}

static void update_moving_front_3d(geometry_data* g_p,
				   time_scheme* t_p,
				   int t)
{
    if (g_p->moving >= 1)
    {
	g_p->kmax = (int)(t * t_p->delta_t/g_p->delta_z) + 50;
	if (g_p->kmax > g_p->z_max-1)
	{
	    g_p->kmax = g_p->z_max-1;
	}
    }
}

void finalize_moving_window_3d(geometry_data* g_p,
			       time_scheme* t_p,
			       media_data* m_p,
			       const char* pathPrefix)
{
    if (g_p->moving == 2)
    {
        g_p->move_index -= g_p->move;
    }

}

static void update_moving_window_3d(vfield_n* D_p, vfield_n* E_p,
				    vfield_n* H_p, vfield_n* J_p,
				    geometry_data* g_p,
				    time_scheme* t_p,
				    media_data* m_p,
				    pml_data* p_p,
				    points_data_3d* pd_p,
				    sources_rt_data* s_p,
				    scheme_constants* sch_p,
				    int t,
				    const char* pathPrefix)
{
    if (g_p->moving == 2)
    {
	int start = (int)(0.45 * g_p->z_max);
	int thres = (int)(0.55 * g_p->z_max);

	if (t > t_p->t0)
	{
	    if  ((t-t_p->t0) % t_p->t_move == 0)
	    {
		int z_c = 0;

		if (t_p->v_move == 0.)
		{
		    z_c = calculate_pulse_center(E_p, D_p, H_p, g_p);
		}

		if (z_c > thres || t_p->v_move != 0.)
		{
		    if (t_p->v_move == 0.)
		    {
			g_p->move = z_c - start;
		    }

		    swap_all_3d(D_p, H_p, J_p, g_p, m_p, p_p, g_p->move);
		    clear_pml_z(p_p);
		    update_point_data_3d(pd_p, g_p, t, E_p);
		}
	    }
	}
	else if (t == t_p->t0)
	{
	    swap_all_3d(D_p, H_p, J_p, g_p, m_p, p_p, p_p->pmlz+2);
	    zero_sources_rt(s_p);
	    zero_pml_z(p_p, g_p, t_p, sch_p);
	    clear_pml_z(p_p);
	    update_point_data_3d(pd_p, g_p, t, E_p);
	}
    }
}

int swap_all_3d(vfield_n* D_p, vfield_n* H_p, vfield_n* J_p,
		geometry_data* g_p,
		media_data* m_p,
		pml_data* p_p,
		int index)
{
    swap_vfield_n_3d(D_p, index);
    swap_vfield_n_3d(H_p, index);
    swap_vfield_n_3d(J_p, index);
    swap_pml_3d(p_p, index);
    g_p->move_index += index;
    swap_media_3d(m_p, g_p, p_p, index);
    return 0;
}

int savedata_save_3d(save_data* sd_p,
		     vfield_n* E_p,
		     geometry_data* g_p,
		     media_data* m_p,
		     int t,
		     const char* pathPrefix)
{
    if (t == 1)
    {
	if (sd_p->save_0_step == true)
	{
	    char dirname[1024];
	    sprintf(dirname, "%s/data/%06d", pathPrefix, 0);
	    RANK_S(0)
                create_directory(dirname);
	    RANK_F(0)
	    
	    MPI_BARRIER(MPI_COMM_WORLD);
	   
	    store_slice(&m_p->n_max, 'x', g_p->x_full/2, "density", 0, g_p, dirname);
	    store_slice(&m_p->n_max, 'y', g_p->y_full/2, "density", 0, g_p, dirname);
	    store_slice(&m_p->n_max, 'z', g_p->z_full/2, "density", 0, g_p, dirname);
	    store_slice(&m_p->epsilon, 'x', g_p->x_full/2, "epsilon", 0, g_p, dirname);
	    store_slice(&m_p->epsilon, 'y', g_p->y_full/2, "epsilon", 0, g_p, dirname);
	    store_slice(&m_p->epsilon, 'z', g_p->z_full/2, "epsilon", 0, g_p, dirname);
	}
    }
    else
    {
	if(t%sd_p->delta_time == 0 && t >= sd_p->start_time && t <= sd_p->end_time)
	{
	    char dirname[1024];
	    sprintf(dirname, "%s/data/%06d", pathPrefix, t);
	    RANK_S(0)
                create_directory(dirname);
	    RANK_F(0)
            MPI_BARRIER(MPI_COMM_WORLD);

	    store_slice_n_3comp(E_p->comp_p[x_e], E_p->comp_p[y_e], E_p->comp_p[z_e], "E", 'x', g_p->x_full/2, t, g_p, dirname);
	    store_slice_n_3comp(E_p->comp_p[x_e], E_p->comp_p[y_e], E_p->comp_p[z_e], "E", 'y', g_p->y_full/2, t, g_p, dirname);
	    store_slice_n_3comp(E_p->comp_p[x_e], E_p->comp_p[y_e], E_p->comp_p[z_e], "E", 'z', g_p->z_full/2, t, g_p, dirname);
	}

	if (sd_p->save_3d == true && t == sd_p->save_3d_time)
	{
	    char dirname[1024];
	    sprintf(dirname, "%s/data/3D_%06d", pathPrefix, t);
	    RANK_S(0)
                create_directory(dirname);
	    RANK_F(0)
            MPI_BARRIER(MPI_COMM_WORLD);

	}
    }
    return 0;
}
