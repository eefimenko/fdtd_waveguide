#include "fdtd.h"
#include "fdtd_cuda_kernels.h"

__device__ __constant__ int cdn[3];
__device__ __constant__ int cgrid[6];
__device__ __constant__ FP  cscheme[18];

__device__ FP* aexy;
__device__ FP* bexy;
__device__ FP* aexz;
__device__ FP* bexz;

__device__ FP* aeyx;
__device__ FP* beyx;
__device__ FP* aeyz;
__device__ FP* beyz;

__device__ FP* aezx;
__device__ FP* bezx;
__device__ FP* aezy;
__device__ FP* bezy;

__device__ FP* ahxy;
__device__ FP* bhxy;
__device__ FP* ahxz;
__device__ FP* bhxz;

__device__ FP* ahyx;
__device__ FP* bhyx;
__device__ FP* ahyz;
__device__ FP* bhyz;

__device__ FP* ahzx;
__device__ FP* bhzx;
__device__ FP* ahzy;
__device__ FP* bhzy;

__device__ int* idex;
__device__ int* idhx;
__device__ int* idexf;
__device__ int* idhxf;
__device__ int* idey;
__device__ int* idhy;
__device__ int* ideyf;
__device__ int* idhyf;
__device__ int* idez;
__device__ int* idhz;
__device__ int* idezf;
__device__ int* idhzf;

__device__ FP* j_zmin;
__device__ FP* j_zmax;

__device__ int* jh_zmin_idx;
__device__ int* je_zmin_idx;
__device__ int* jh_zmax_idx;
__device__ int* je_zmax_idx;

__device__ FP* w_zmin;
__device__ FP* w_zmax;
__device__ FP* w_ymin;
__device__ FP* w_ymax;
__device__ FP* w_xmin;
__device__ FP* w_xmax;

__device__ int* wh_zmin_idx;
__device__ int* we_zmin_idx;
__device__ int* wh_zmax_idx;
__device__ int* we_zmax_idx;
__device__ int* wh_ymin_idx;
__device__ int* we_ymin_idx;
__device__ int* wh_ymax_idx;
__device__ int* we_ymax_idx;
__device__ int* wh_xmin_idx;
__device__ int* we_xmin_idx;
__device__ int* wh_xmax_idx;
__device__ int* we_xmax_idx;


__device__ FP* n_max;
__device__ FP* epsilon;
//__device__ FP* n;
//__device__ FP* absorption;
//__device__ FP* envelope;
//__device__ FP* envelope1;
//__device__ FP* envelope2;

int swap_cuda_field_3d(cuda_field* f_p, int num)
{
    memmove(f_p->d_p,
            f_p->d_p+num*f_p->nx*f_p->ny,
            (f_p->nz-num)*f_p->nx*f_p->ny*sizeof(FP));

    memset(f_p->d_p+(f_p->nz-num)*f_p->nx*f_p->ny,
           0,
           num*f_p->nx*f_p->ny*sizeof(FP));
    return 0;
}

int init_cuda_field_3d(cuda_field* field_p, 
		       int nx,
		       int ny, 
		       int nz)
{
    field_p->nx = nx;
    field_p->ny = ny;
    field_p->nz = nz;
    field_p->nxy = nx*ny;
    field_p->memsize = nx*ny*nz*sizeof(FP);

    if (0 == field_p->nx || 
	0 == field_p->ny || 
	0 == field_p->nz)
    {
	field_p->d_p = NULL;
	return 0;
    }

    FP* h_p = (FP*)malloc(field_p->memsize);

    if (NULL == h_p)
    {
	fprintf(stderr, "Error! Not enough memory: %d %d %d\n", nx, ny, nz);
	fflush(stderr);
	exit(-1);
    }
    memset(h_p, 0, field_p->memsize);
    
    cudaMalloc((void**) &field_p->d_p, field_p->memsize);
    int res = cudaMemcpy(field_p->d_p, h_p, field_p->memsize, cudaMemcpyHostToDevice);
    if (res != cudaSuccess)
    {
	fprintf(stderr, "Error memcpy res=%d\n", res);
	fflush(stderr);
	exit(-1);
    }
    free(h_p);
    
    return 0;
}

int init_cuda_field_2d(cuda_field* field_p, 
		       int nx,
		       int nz)
{
    return init_cuda_field_3d(field_p, 
			      nx,
			      1,
			      nz);
}

int malloc_and_init_cuda_field_3d(cuda_field** field_p, 
				  int nx,
				  int ny, 
				  int nz)
{
    if (NULL == (*field_p = (cuda_field*)malloc(sizeof(cuda_field))))
    {
	fprintf(stderr, "Error during malloc for cuda_field\n");
	fflush(stderr);
	exit(-1);
    }
    init_cuda_field_3d(*field_p, 
		       nx,
		       ny,
		       nz);
    return 0;
}

int malloc_and_init_cuda_field_2d(cuda_field** field_p, 
				  int nx,
				  int nz)
{
    return malloc_and_init_cuda_field_3d(field_p, 
					 nx,
					 1,
					 nz);
}

int zero_cuda_field(cuda_field* field_p)
{
    if (NULL == field_p->d_p)
    {
	fprintf(stderr, "Something is wrong with pointers in zero_cuda_field\n");
	fflush(stderr);
	exit(-1);
    }
    
    FP* h_p = (FP*)malloc(field_p->memsize);

    if (NULL == h_p)
    {
	fprintf(stderr, "Error! Not enough memory: %d\n", field_p->memsize);
	fflush(stderr);
	exit(-1);
    }
    memset(h_p, 0, field_p->memsize);
        
    int res = cudaMemcpy(field_p->d_p, h_p, field_p->memsize,
			 cudaMemcpyHostToDevice);
    if (res != 0)
    {
	fprintf(stderr, "res=%d\n", res);
	fflush(stderr);
    }
    free(h_p);
    return res;
}

int destroy_cuda_field(cuda_field* field_p)
{
    if (NULL != field_p->d_p)
    {
	int res = cudaFree(field_p->d_p);
	if (res != 0)
	{
	    fprintf(stderr, "cudaFree res = %d\n", res);
	    fflush(stderr);
	}
	field_p->d_p = NULL;
    }
    
    return 0;
}

int copy_cuda_field_h2d(cuda_field* field_p, FP* a_p)
{
    int res = cudaMemcpy(field_p->d_p, a_p, field_p->memsize,
			 cudaMemcpyHostToDevice);
    if (res != 0)
    {
	fprintf(stderr, "res=%d\n", res);
	fflush(stderr);
    }
    return res;
}

int copy_cuda_h2d(FP* src_p, FP* dst_p, int size)
{
    int res = cudaMemcpy(dst_p, src_p, size*sizeof(FP),
			 cudaMemcpyHostToDevice);
    if (res != 0)
    {
	fprintf(stderr, "res=%d\n", res);
	fflush(stderr);
    }
    return res;
}

int copy_cuda_field_d2h(cuda_field* field_p, FP* a_p)
{
    int res = cudaMemcpy(a_p, field_p->d_p, field_p->memsize,
			 cudaMemcpyDeviceToHost);
    if (res != 0)
    {
	fprintf(stderr, "res=%d\n", res);
	fflush(stderr);
    }
    return res;
}

void cuda_malloc(FP** dest_p, FP* src_p, int size)
{
    if (0 == size)
    {
	*dest_p = NULL;
	return;
    }
    int ssize = size*sizeof(FP);
    FP* temp_p;
    int res = cudaMalloc((void**)&temp_p, ssize);
    if (res != 0)
    {
	fprintf(stderr, "Cuda init fp size %d: %d\n", ssize, res);
	fflush(stderr);
    }
    
    if (src_p != NULL)
    {
	res = cudaMemcpy(temp_p, src_p, ssize, cudaMemcpyHostToDevice);
	if (res != 0)
	{
	    fprintf(stderr, "Cuda memcpy fp size %d: %d\n", ssize, res);
	    fflush(stderr);
	}
    }
    else
    {
	res = cudaMemset(temp_p, 0, ssize);
	if (res != 0)
	{
	    fprintf(stderr, "Cuda memset fp size %d: %d\n", ssize, res);
	    fflush(stderr);
	}
    }
    *dest_p = temp_p;
}

void cuda_free(FP* pointer)
{
    if (NULL != pointer)
    {
	cudaFree(pointer);
    }
}

void cuda_free_int(int* pointer)
{
    if (NULL != pointer)
    {
	cudaFree(pointer);
    }
}

void cuda_realloc(FP** dest_p, FP* src_p, int size)
{
    cuda_free(*dest_p);
    
    if (0 == size)
    {
	*dest_p = NULL;
	return;
    }
    int ssize = size*sizeof(FP);
    FP* temp_p;
    int res = cudaMalloc((void**)&temp_p, ssize);
    if (res != 0)
    {
	fprintf(stderr, "Cuda init fp size %d: %d\n", ssize, res);
	fflush(stderr);
    }
    
    if (src_p != NULL)
    {
	res = cudaMemcpy(temp_p, src_p, ssize, cudaMemcpyHostToDevice);
	if (res != 0)
	{
	    fprintf(stderr, "Cuda memcpy fp size %d: %d\n", ssize, res);
	    fflush(stderr);
	}
    }
    else
    {
	res = cudaMemset(temp_p, 0, ssize);
	if (res != 0)
	{
	    fprintf(stderr, "Cuda memset fp size %d: %d\n", ssize, res);
	    fflush(stderr);
	}
    }
    *dest_p = temp_p;
}

void cuda_malloc_int(int** dest_p, int* src_p, int size)
{
    if (0 == size)
    {
	*dest_p = NULL;
	return;
    }
    int ssize = size*sizeof(int);
    int* temp_p;
    int res = cudaMalloc((void**)&temp_p, ssize);
    if (res != 0)
    {
	fprintf(stderr, "Cuda init int size %d: %d\n", ssize, res);
	fflush(stderr);
    }
    
    res = cudaMemcpy(temp_p, src_p, ssize, cudaMemcpyHostToDevice);
    if (res != 0)
    {
	fprintf(stderr, "Cuda memcpy int size %d: %d\n", ssize, res);
	fflush(stderr);
    }
    *dest_p = temp_p;
}

void cuda_realloc_int(int** dest_p, int* src_p, int size)
{
    cuda_free_int(*dest_p);
    if (0 == size)
    {
	*dest_p = NULL;
	return;
    }
    int ssize = size*sizeof(int);
    int* temp_p;
    int res = cudaMalloc((void**)&temp_p, ssize);
    if (res != 0)
    {
	fprintf(stderr, "Cuda init int size %d: %d\n", ssize, res);
	fflush(stderr);
    }
    
    res = cudaMemcpy(temp_p, src_p, ssize, cudaMemcpyHostToDevice);
    if (res != 0)
    {
	fprintf(stderr, "Cuda memcpy int size %d: %d\n", ssize, res);
	fflush(stderr);
    }
    *dest_p = temp_p;
}

int init_cuda_pml_2d(pml_data* pml_p)
{
    return 0;
}

int init_cuda_geometry_2d(geometry_data* geometry_data_p)
{
    int geom[6];
    geom[I_OFFSET] = geometry_data_p->i_offset;
    geom[J_OFFSET] = 0;
    geom[K_OFFSET] = geometry_data_p->k_offset;
    geom[NX] = geometry_data_p->x_max;
    geom[NY] = 0;
    geom[NZ] = geometry_data_p->z_max;
    cudaMemcpyToSymbol(cgrid, geom, sizeof(int)*6);
    return 0;
}

int run_3D_config(configTable* fdtdConfig, 
		  geometry_data geometry,
		  time_scheme timescheme,
		  const char* pathPrefix)
{
    sources_rt_data sources_rt;
    pml_data        pml;
    media_data      media;
    save_data       sdata;
    scheme_constants scheme;
    points_data_3d pdata;
#ifdef USEMPI
    int gpuPerNode = fdtdConfig->getIntValue("gpu_per_node", 1);
    cudaSetDevice(mpi.rank%gpuPerNode);
    cudaDeviceProp devProp;
    cudaGetDeviceProperties(&devProp, mpi.rank%gpuPerNode);
    printDevProp(devProp, mpi.rank);
#else
    cudaDeviceProp devProp;
    cudaGetDeviceProperties(&devProp, 0);
    printDevProp(devProp, 0);
#endif
    
    init_constants(&scheme, &geometry, &timescheme);
    init_pml(fdtdConfig, &pml, &geometry, &timescheme, &scheme);
    init_savedata(fdtdConfig, &sdata);

    vfield_n *E_p, *D_p, *H_p, *J_p;
    
    E_p = init_vfield_n("E", geometry.x_max, geometry.y_max, geometry.z_max);
    H_p = init_vfield_n("H", geometry.x_max, geometry.y_max, geometry.z_max);
    J_p = init_vfield_n("J", geometry.x_max, geometry.y_max, geometry.z_max);
    
    init_sources_rt(fdtdConfig, &geometry, &pml, &sources_rt, E_p, H_p);
    cuda_field cdx, cdy, cdz, cex, cey, cez, cjx, cjy, cjz, chx, chy, chz;
    cuda_field *cdx_p, *cdy_p, *cdz_p;

    init_cuda_field_3d(&cex,  geometry.x_max, geometry.y_max, geometry.z_max);
    init_cuda_field_3d(&cey,  geometry.x_max, geometry.y_max, geometry.z_max);
    init_cuda_field_3d(&cez,  geometry.x_max, geometry.y_max, geometry.z_max);
    init_cuda_field_3d(&cjx,  geometry.x_max, geometry.y_max, geometry.z_max);
    init_cuda_field_3d(&cjy,  geometry.x_max, geometry.y_max, geometry.z_max);
    init_cuda_field_3d(&cjz,  geometry.x_max, geometry.y_max, geometry.z_max);
    init_cuda_field_3d(&chx,  geometry.x_max, geometry.y_max, geometry.z_max);
    init_cuda_field_3d(&chy,  geometry.x_max, geometry.y_max, geometry.z_max);
    init_cuda_field_3d(&chz,  geometry.x_max, geometry.y_max, geometry.z_max);
    
    int nmax = geometry.x_max*geometry.y_max*geometry.z_max;
    dim3  dimBlock(BLOCK_SIZE);
    dim3  gridfull((nmax + BLOCK_SIZE - 1)/BLOCK_SIZE);
    dim3  grids((sources_rt.z_num + BLOCK_SIZE - 1)/BLOCK_SIZE);
    dim3  gridwx((sources_rt.wx_num + BLOCK_SIZE - 1)/BLOCK_SIZE);
    dim3  gridwy((sources_rt.wy_num + BLOCK_SIZE - 1)/BLOCK_SIZE);
    dim3  gridwz((sources_rt.wz_num + BLOCK_SIZE - 1)/BLOCK_SIZE);
    
    init_media(fdtdConfig, &media, &geometry, &timescheme, &scheme);

    if (media.recalc == true)
    {
	D_p = init_vfield_n("D", geometry.x_max, geometry.y_max, geometry.z_max);
	init_cuda_field_3d(&cdx, geometry.x_max, geometry.y_max, geometry.z_max);
	init_cuda_field_3d(&cdy, geometry.x_max, geometry.y_max, geometry.z_max);
	init_cuda_field_3d(&cdz, geometry.x_max, geometry.y_max, geometry.z_max);
	cdx_p = &cdx;
	cdy_p = &cdy;
	cdz_p = &cdz;
    }
    else
    {
	D_p = E_p;
	init_cuda_field_3d(&cdx,  0, 0, 0);
	init_cuda_field_3d(&cdy,  0, 0, 0);
	init_cuda_field_3d(&cdz,  0, 0, 0);
	cdx_p = &cex;
	cdy_p = &cey;
	cdz_p = &cez;
    }
    
    init_moving_window_3d(&geometry, &sources_rt, &pml, &timescheme, &scheme, pathPrefix);
    
    init_point_data_3d(fdtdConfig,
                       &pdata,
                       pathPrefix,
                       &timescheme,
                       &geometry,
                       E_p,
                       H_p);
    
    MPI_INIT_SENDRECV_3D(&mpi, &geometry, D_p, H_p);
    
    RANK_S(0);
    print_sources_rt(&sources_rt, &media, &geometry, &timescheme, pathPrefix);
    print_media(&media);

    fprintf(stderr, "\nRunning main loop\n");
    fflush(stderr);
    RANK_F(0);
    
    for (int t = timescheme.t_start + 1; t < timescheme.t_max+1; t++)
    {
	int nemax = pml.nexf;
	int nhmax = pml.nhxf;
	if (pml.neyf > nemax)
	{
	    nemax = pml.neyf;
	}
	if (pml.nezf > nemax)
	{
	    nemax = pml.nezf;
	}
	if (pml.nhyf > nhmax)
	{
	    nhmax = pml.nhyf;
	}
	if (pml.nhzf > nhmax)
	{
	    nhmax = pml.nhzf;
	}
	
	dim3  grid_dx_pml((pml.nex + BLOCK_SIZE - 1)/BLOCK_SIZE);
	dim3  grid_dy_pml((pml.ney + BLOCK_SIZE - 1)/BLOCK_SIZE);
	dim3  grid_dz_pml((pml.nez + BLOCK_SIZE - 1)/BLOCK_SIZE);
	dim3  grid_hx_pml((pml.nhx + BLOCK_SIZE - 1)/BLOCK_SIZE);
	dim3  grid_hy_pml((pml.nhy + BLOCK_SIZE - 1)/BLOCK_SIZE);
	dim3  grid_hz_pml((pml.nhz + BLOCK_SIZE - 1)/BLOCK_SIZE);
	dim3  grid_d((nemax + BLOCK_SIZE - 1)/BLOCK_SIZE);
	dim3  grid_h((nhmax + BLOCK_SIZE - 1)/BLOCK_SIZE);
	
	update_sources_rt3d(&sources_rt, &geometry, t);
	
	fdtd_3d_D<<< grid_d, dimBlock>>>(cdx_p->d_p,
					 cdy_p->d_p,
					 cdz_p->d_p,
					 chx.d_p,
					 chy.d_p,
					 chz.d_p,
					 cjx.d_p,
					 cjy.d_p,
					 cjz.d_p,
					 idexf,
					 ideyf,
					 idezf,
					 pml.nexf,
					 pml.neyf,
					 pml.nezf);
	if ( cudaSuccess != cudaGetLastError() )
	{
	    fprintf(stderr, "Error fdtd_3d_D!\n" );
	    fflush(stderr);
	}
	
	fdtd_3d_Dx_pml<<< grid_dx_pml, dimBlock>>>(cdx_p->d_p,
						   cdy_p->d_p,
						   cdz_p->d_p,
						   chx.d_p,
						   chy.d_p,
						   chz.d_p,
						   pml.cExy,
						   pml.cExz,
						   aexy,
						   aexz,
						   bexy,
						   bexz,
						   idex,
						   pml.nex);
	if ( cudaSuccess != cudaGetLastError() )
	{
	    fprintf(stderr, "Error fdtd_3d_Dx_pml!\n" );
	    fflush(stderr);
	}
	
	fdtd_3d_Dy_pml<<< grid_dy_pml, dimBlock>>>(cdx_p->d_p,
						   cdy_p->d_p,
						   cdz_p->d_p,
						   chx.d_p,
						   chy.d_p,
						   chz.d_p,
						   pml.cEyx,
						   pml.cEyz,
						   aeyx,
						   aeyz,
						   beyx,
						   beyz,
						   idey,
						   pml.ney);
	if ( cudaSuccess != cudaGetLastError() )
	{
	    fprintf(stderr, "Error fdtd_3d_Dy_pml!\n" );
	    fflush(stderr);
	}
	
	fdtd_3d_Dz_pml<<< grid_dz_pml, dimBlock>>>(cdx_p->d_p,
						   cdy_p->d_p,
						   cdz_p->d_p,
						   chx.d_p,
						   chy.d_p,
						   chz.d_p,
						   pml.cEzx,
						   pml.cEzy,
						   aezx,
						   aezy,
						   bezx,
						   bezy,
						   idez,
						   pml.nez);
	if ( cudaSuccess != cudaGetLastError() )
	{
	    fprintf(stderr, "Error fdtd_3d_Dz_pml!\n" );
	    fflush(stderr);
	}
	
#ifdef USENDF
	MPI_EXCH_DOWN_X(&mpi, cdy_p, cdz_p);
        MPI_EXCH_DOWN_Y(&mpi, cdx_p, cdz_p);
        MPI_EXCH_DOWN_Z(&mpi, cdx_p, cdy_p);
#endif
        MPI_EXCH_UP_X(&mpi, cdy_p, cdz_p);
        MPI_EXCH_UP_Y(&mpi, cdx_p, cdz_p);
        MPI_EXCH_UP_Z(&mpi, cdx_p, cdy_p);
	
	if (sources_rt.pulse_enable == true)
	{
	    if (sources_rt.pulse_zmin == true)
	    {
		add_psources_3d_D_zmin <<< grids, dimBlock>>>(cdx_p->d_p,
							      cdy_p->d_p,
							      j_zmin,
							      je_zmin_idx,
							      sources_rt.z_num);
		if ( cudaSuccess != cudaGetLastError() )
		{
		    fprintf(stderr, "Error add_psources_3d_D_zmin!\n" );
		    fflush(stderr);
		}
	    }
	}
	
	if (sources_rt.tfsf_enable == true)
	{
	    if (sources_rt.tfsf_xmin == true)
	    {
		add_tfsf_3d_D_xmin <<< gridwx, dimBlock>>>(cdy_p->d_p,
							   cdz_p->d_p,
							   w_xmin,
							   we_xmin_idx,
							   sources_rt.wx_num);
	    }
	    
	    if (sources_rt.tfsf_xmax == true)
	    {
		add_tfsf_3d_D_xmax <<< gridwx, dimBlock>>>(cdy_p->d_p,
							   cdz_p->d_p,
							   w_xmax,
							   we_xmax_idx,
							   sources_rt.wx_num);
	    }
	    
	    if (sources_rt.tfsf_ymin == true)
	    {
		add_tfsf_3d_D_ymin <<< gridwy, dimBlock>>>(cdx_p->d_p,
							   cdz_p->d_p,
							   w_ymin,
							   we_ymin_idx,
							   sources_rt.wy_num);
	    }
	    
	    if (sources_rt.tfsf_ymax == true)
	    {
		add_tfsf_3d_D_ymax <<< gridwy, dimBlock>>>(cdx_p->d_p,
							   cdz_p->d_p,
							   w_ymax,
							   we_ymax_idx,
							   sources_rt.wy_num);
	    }
	    
	    if (sources_rt.tfsf_zmin == true)
	    {
		add_tfsf_3d_D_zmin <<< gridwz, dimBlock>>>(cdx_p->d_p,
							   cdy_p->d_p,
							   w_zmin,
							   we_zmin_idx,
							   sources_rt.wz_num);
	    }
	    if (sources_rt.tfsf_zmax == true)
	    {
		add_tfsf_3d_D_zmax <<< gridwz, dimBlock>>>(cdx_p->d_p,
							   cdy_p->d_p,
							   w_zmax,
							   we_zmax_idx,
							   sources_rt.wz_num);
	    }
	}
	
	if (media.recalc == true)
	{
	    if (media.kerr == 0)
	    {
		calculate_E_from_D_3d <<<gridfull, dimBlock>>>(cex.d_p,
							       cey.d_p,
							       cez.d_p,
							       cdx.d_p,
							       cdy.d_p,
							       cdz.d_p,
							       epsilon,
							       nmax);
		if ( cudaSuccess != cudaGetLastError() )
		{
		    fprintf(stderr, "Error calculate_E_from_D_3d!\n" );
		    fflush(stderr);
		}
	    }
	    else
	    {
		calculate_E_from_D_3d_kerr <<<gridfull, dimBlock>>>(cex.d_p,
								    cey.d_p,
								    cez.d_p,
								    cdx.d_p,
								    cdy.d_p,
								    cdz.d_p,
								    epsilon,
								    n_max,
								    media.kerr_khi[0],
								    nmax);
		if ( cudaSuccess != cudaGetLastError() )
		{
		    fprintf(stderr, "Error calculate_E_from_D_3d_kerr!\n" );
		    fflush(stderr);
		}
	    }
	}
	
	fdtd_3d_H<<< grid_h, dimBlock>>>(cex.d_p,
					 cey.d_p,
					 cez.d_p,
					 chx.d_p,
					 chy.d_p,
					 chz.d_p,
					 idhxf,
					 idhyf,
					 idhzf,
					 pml.nhxf,
					 pml.nhyf,
					 pml.nhzf);
	if ( cudaSuccess != cudaGetLastError() )
	{
	    fprintf(stderr, "Error fdtd_3d_H!\n" );
	    fflush(stderr);
	}
	
	fdtd_3d_Hx_pml<<< grid_hx_pml, dimBlock>>>(cex.d_p,
						   cey.d_p,
						   cez.d_p,
						   chx.d_p,
						   chy.d_p,
						   chz.d_p,
						   pml.cHxy,
						   pml.cHxz,
						   ahxy,
						   ahxz,
						   bhxy,
						   bhxz,
						   idhx,
						   pml.nhx);
	if ( cudaSuccess != cudaGetLastError() )
	{
	    fprintf(stderr, "Error fdtd_3d_Hx_pml!\n" );
	    fflush(stderr);
	}
	
	fdtd_3d_Hy_pml<<< grid_hy_pml, dimBlock>>>(cex.d_p,
						   cey.d_p,
						   cez.d_p,
						   chx.d_p,
						   chy.d_p,
						   chz.d_p,
						   pml.cHyx,
						   pml.cHyz,
						   ahyx,
						   ahyz,
						   bhyx,
						   bhyz,
						   idhy,
						   pml.nhy);
	if ( cudaSuccess != cudaGetLastError() )
	{
	    fprintf(stderr, "Error fdtd_3d_Hy_pml!\n" );
	    fflush(stderr);
	}
	
	fdtd_3d_Hz_pml<<< grid_hz_pml, dimBlock>>>(cex.d_p,
						   cey.d_p,
						   cez.d_p,
						   chx.d_p,
						   chy.d_p,
						   chz.d_p,
						   pml.cHzx,
						   pml.cHzy,
						   ahzx,
						   ahzy,
						   bhzx,
						   bhzy,
						   idhz,
						   pml.nhz);
	if ( cudaSuccess != cudaGetLastError() )
	{
	    fprintf(stderr, "Error fdtd_3d_Hy_pml!\n" );
	    fflush(stderr);
	}
	
#ifdef USENDF
	MPI_EXCH_UP_X(&mpi, &chy, &chz);
        MPI_EXCH_UP_Y(&mpi, &chx, &chz);
        MPI_EXCH_UP_Z(&mpi, &chx, &chy);
#endif
        MPI_EXCH_DOWN_X(&mpi, &chy, &chz);
        MPI_EXCH_DOWN_Y(&mpi, &chx, &chz);
        MPI_EXCH_DOWN_Z(&mpi, &chx, &chy);

	if (sources_rt.pulse_enable == true)
	{
	    if (sources_rt.pulse_zmin == true)
	    {
		add_psources_3d_H_zmin <<< grids, dimBlock>>>(chx.d_p,
							      chy.d_p,
							      j_zmin,
							      jh_zmin_idx,
							      sources_rt.z_num);
		if ( cudaSuccess != cudaGetLastError() )
		{
		    fprintf(stderr, "Error add_psources_3d_H_zmin !\n" );
		    fflush(stderr);
		}
	    }
	}
	
	if (sources_rt.tfsf_enable == true)
	{
	    if (sources_rt.tfsf_xmin == true)
	    {
		add_tfsf_3d_H_xmin <<< gridwx, dimBlock>>>(chy.d_p,
							   chz.d_p,
							   w_xmin,
							   wh_xmin_idx,
							   sources_rt.wx_num);
	    }
	    if (sources_rt.tfsf_xmax == true)
	    {
		add_tfsf_3d_H_xmax <<< gridwx, dimBlock>>>(chy.d_p,
							   chz.d_p,
							   w_xmax,
							   wh_xmax_idx,
							   sources_rt.wx_num);
	    }
	    if (sources_rt.tfsf_ymin == true)
	    {
		add_tfsf_3d_H_ymin <<< gridwy, dimBlock>>>(chx.d_p,
							   chz.d_p,
							   w_ymin,
							   wh_ymin_idx,
							   sources_rt.wy_num);
	    }
	    if (sources_rt.tfsf_ymax == true)
	    {
		add_tfsf_3d_H_ymax <<< gridwy, dimBlock>>>(chx.d_p,
							   chz.d_p,
							   w_ymax,
							   wh_ymax_idx,
							   sources_rt.wy_num);
	    }
	    if (sources_rt.tfsf_zmin == true)
	    {
		add_tfsf_3d_H_zmin <<< gridwz, dimBlock>>>(chx.d_p,
							   chy.d_p,
							   w_zmin,
							   wh_zmin_idx,
							   sources_rt.wz_num);
	    }
	    if (sources_rt.tfsf_zmax == true)
	    {
		add_tfsf_3d_H_zmax <<< gridwz, dimBlock>>>(chx.d_p,
							   chy.d_p,
							   w_zmax,
							   wh_zmax_idx,
							   sources_rt.wz_num);
	    }
	}

	if (t%sdata.delta_time == 0)
	{
	    copy_cuda_field_d2h(&cex, E_p->comp_p[x_e]->a);
	    copy_cuda_field_d2h(&cey, E_p->comp_p[y_e]->a);
	    copy_cuda_field_d2h(&cez, E_p->comp_p[z_e]->a);
	   
	    if (media.recalc == true)
	    {
		copy_cuda_field_d2h(&cdx, D_p->comp_p[x_e]->a);
		copy_cuda_field_d2h(&cdy, D_p->comp_p[y_e]->a);
		copy_cuda_field_d2h(&cdz, D_p->comp_p[z_e]->a);
	    }
	    
	    copy_cuda_field_d2h(&chx, H_p->comp_p[x_e]->a);
	    copy_cuda_field_d2h(&chy, H_p->comp_p[y_e]->a);
	    copy_cuda_field_d2h(&chz, H_p->comp_p[z_e]->a);
	    calculate_energy_3d(E_p, D_p, H_p, &media, &timescheme, &geometry, t, pathPrefix);
	}

	savedata_save_3d(&sdata, E_p, &geometry, &media, t, pathPrefix);
	
	if (media.type == so_e)
        {
            store_point_data_3d_cuda(&pdata,
				     &geometry,
				     t,
				     &cex,
				     &cey,
				     &cez,
				     &chx,
				     &chy,
				     &chz);
        }
        else
        {
            store_point_data_3d_cuda(&pdata,
				     &geometry,
				     t,
				     &cex,
				     &cey,
				     &cez,
				     &chx,
				     &chy,
				     &chz);
	}
	
	update_moving_window_3d_cuda(D_p, E_p, H_p, J_p,
				     cdx_p, cdy_p, cdz_p,
				     &cex, &cey, &cez,
				     &chx, &chy, &chz,
				     &cjx, &cjy, &cjz,
				     &geometry,
				     &timescheme,
				     &media,
				     &pml,
				     &pdata,
				     &sources_rt,
				     &scheme,
				     t,
				     pathPrefix);

    }/*for t */
    finalize_moving_window_3d(&geometry, &timescheme, &media, pathPrefix);
    destroy_cuda_field(&cex);
    destroy_cuda_field(&cey);
    destroy_cuda_field(&cez);
    destroy_cuda_field(&cdx);
    destroy_cuda_field(&cdy);
    destroy_cuda_field(&cdz);
    destroy_cuda_field(&chx);
    destroy_cuda_field(&chy);
    destroy_cuda_field(&chz);
    
    destroy_cuda_field(&cjy);
    destroy_cuda_field(&cjx);
    destroy_cuda_field(&cjz);
    destroy_sources_rt(&sources_rt, &geometry);
    destroy_media(&media);
    destroy_pml3d(&pml);
    destroy_vfield_n(E_p);
    destroy_point_data_3d(&pdata);
    if (media.recalc == true)
    {
	destroy_vfield_n(D_p);
    }
    destroy_vfield_n(H_p);
    destroy_vfield_n(J_p);
    MPI_DELETE_SENDRECV_3D(&mpi);
    
    return 0;
}

__global__ void calculate_E_from_D_3d(FP* ex_p,
				      FP* ey_p,
				      FP* ez_p,
				      FP* dx_p,
				      FP* dy_p,
				      FP* dz_p,
				      FP* epsilon,
				      int nmax)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;

    if (i < nmax)
    {
	FP eps_lin = epsilon[i];
	ex_p[i] = dx_p[i]/eps_lin;
	ey_p[i] = dy_p[i]/eps_lin;
	ez_p[i] = dz_p[i]/eps_lin;
    }
}

__global__ void calculate_E_from_D_3d_kerr(FP* ex_p,
					   FP* ey_p,
					   FP* ez_p,
					   FP* dx_p,
					   FP* dy_p,
					   FP* dz_p,
					   FP* epsilon,
					   FP* n_max,
					   FP khi, 
					   int nmax)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    
    if (i < nmax)
    {
	FP eps_lin = epsilon[i];
	if (n_max[i] > 0.)
	{
	    int i_temp = 0;
	    FP eps_o;
	    FP dx = dx_p[i];
	    FP dy = dy_p[i];
	    FP dz = dz_p[i];
	    FP d2 = dx * dx + dy * dy + dz * dz;
	    FP eps = eps_lin + d2 * khi;
	    do
	    {
		eps_o = eps;
		FP abs = d2/(eps*eps);
		eps = eps_lin + abs * khi;
		i_temp++;
	    }
	    while(fabs(eps_o - eps) > 1e-5 && i_temp < 20);
	    ex_p[i] = dx / eps;
	    ey_p[i] = dy / eps;
	    ez_p[i] = dz / eps;
	}
	else
	{
	    ex_p[i] = dx_p[i]/eps_lin;
	    ey_p[i] = dy_p[i]/eps_lin;
	    ez_p[i] = dz_p[i]/eps_lin;
	}
    }
}

__global__ void fdtd_3d_D(FP* ex_p,
			  FP* ey_p,
			  FP* ez_p,
			  FP* hx_p,
			  FP* hy_p,
			  FP* hz_p,
			  FP* jx_p,
			  FP* jy_p,
			  FP* jz_p,
			  int* idexf,
			  int* ideyf,
			  int* idezf,
			  int nexf,
			  int neyf,
			  int nezf)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    
    int dx = cdn[DX];
    int dy = cdn[DY];
    int dz = cdn[DZ];
    
    if (i < nexf)
    {
	int ex = idexf[i];
#ifdef USENDF
	FP dhz = cscheme[BZ] * (hz_p[ex] - hz_p[ex - dy]) +
	   cscheme[AZ] * (hz_p[ex - dz] - hz_p[ex - dz -dy] +
			  hz_p[ex + dz] - hz_p[ex + dz -dy]);
	FP dhy = cscheme[BY] * (hy_p[ex] - hy_p[ex - dz]) +
	    cscheme[AY] * (hy_p[ex - dy] - hy_p[ex - dy -dz] +
		      hy_p[ex + dy] - hy_p[ex + dy -dz]);
#else
	FP dhz = cscheme[DCX] * (hz_p[ex] - hz_p[ex - dy]);
	FP dhy = cscheme[DCX] * (hy_p[ex] - hy_p[ex - dz]);
#endif
	
	ex_p[ex] += 
	    cscheme[DTDY] * dhz - cscheme[DTDZ] * dhy -
	    cscheme[DT_4_PI] * jx_p[ex];
    }

    if (i < neyf)
    {
	int ey = ideyf[i];
#ifdef USENDF		
	FP dhx = cscheme[BX] * (hx_p[ey] - hx_p[ey - dz]) +
	    cscheme[AX] * (hx_p[ey - dx] - hx_p[ey - dx - dz] +
		      hx_p[ey + dx] - hx_p[ey + dx - dz]);
	FP dhz = cscheme[BZ] * (hz_p[ey] - hz_p[ey - dx]) +
	    cscheme[AZ] * (hz_p[ey - dz] - hz_p[ey - dz - dx] +
		      hz_p[ey + dz] - hz_p[ey + dz - dx]);
#else
	FP dhx = cscheme[DCY] * (hx_p[ey] - hx_p[ey - dz]);
	FP dhz = cscheme[DCY] * (hz_p[ey] - hz_p[ey - dx]);
#endif
	
	ey_p[ey] +=  cscheme[DTDZ] * dhx - cscheme[DTDX] * dhz - 
	    cscheme[DT_4_PI] * jy_p[ey];
    }
    
    if (i < nezf)
    {
	int ez = idezf[i];
#ifdef USENDF
	FP dhy = cscheme[BY] * (hy_p[ez] - hy_p[ez - dx]) +
	    cscheme[AY] * (hy_p[ez - dy] - hy_p[ez - dy - dx] +
		      hy_p[ez + dy] - hy_p[ez + dy - dx]);
	FP dhx = cscheme[BX] * (hx_p[ez] - hx_p[ez - dy]) +
	    cscheme[AX] * (hx_p[ez - dx] - hx_p[ez - dx - dy] +
		      hx_p[ez + dx] - hx_p[ez + dx - dy]);
#else
	FP dhy = cscheme[DCZ] * (hy_p[ez] - hy_p[ez - dx]);
	FP dhx = cscheme[DCZ] * (hx_p[ez] - hx_p[ez - dy]);
#endif
		
	ez_p[ez] += cscheme[DTDX] * dhy - cscheme[DTDY] * dhx -
	    cscheme[DT_4_PI] * jz_p[ez];
    }
    __syncthreads(); 
}

__global__ void fdtd_3d_H(FP* ex_p,
			  FP* ey_p,
			  FP* ez_p,
			  FP* hx_p,
			  FP* hy_p,
			  FP* hz_p,
			  int* idhxf,
			  int* idhyf,
			  int* idhzf,
			  int nhxf,
			  int nhyf,
			  int nhzf)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    
    int dx = cdn[DX];
    int dy = cdn[DY];
    int dz = cdn[DZ];
    
    if (i < nhxf)
    {
	int hx = idhxf[i];
	
#ifdef USENDF
	FP dey = cscheme[BY] * (ey_p[hx + dz] - ey_p[hx]) +
	    cscheme[AY] * (ey_p[hx - dy + dz] - ey_p[hx - dy] +
		      ey_p[hx + dy + dz] - ey_p[hx + dy]);
	FP dez = cscheme[BZ] * (ez_p[hx + dy] - ez_p[hx]) +
	    cscheme[AZ] * (ez_p[hx - dz + dy] - ez_p[hx - dz] +
		      ez_p[hx + dz + dy] - ez_p[hx + dz]);
#else
	FP dey = cscheme[DCX] * (ey_p[hx + dz] - ey_p[hx]);
	FP dez = cscheme[DCX] * (ez_p[hx + dy] - ez_p[hx]);
#endif
	hx_p[hx] += cscheme[DTDZ] * dey - cscheme[DTDY] * dez;
    }
    
    if (i < nhyf)
    {
	int hy = idhyf[i];
	
#ifdef USENDF
	FP dez = cscheme[BZ] * (ez_p[hy + dx] - ez_p[hy]) +
	    cscheme[AZ] * (ez_p[hy - dz + dx] - ez_p[hy - dz] +
		      ez_p[hy + dz + dx] - ez_p[hy + dz]);
	FP dex = cscheme[BX] * (ex_p[hy + dz] - ex_p[hy]) +
	    cscheme[AX] * (ex_p[hy - dx + dz] - ex_p[hy - dx] +
		      ex_p[hy + dx + dz] - ex_p[hy + dx]); 
		
#else
	FP dez = cscheme[DCY] * (ez_p[hy + dx] - ez_p[hy]);
	FP dex = cscheme[DCY] * (ex_p[hy + dz] - ex_p[hy]);
#endif
	
	hy_p[hy] += cscheme[DTDX] * dez - cscheme[DTDZ] * dex;
    }
    
    if (i < nhzf)
    {
	int hz = idhzf[i];
	
#ifdef USENDF
	FP dex = cscheme[BX] * (ex_p[hz + dy] - ex_p[hz]) +
	    cscheme[AX] * (ex_p[hz - dx + dy] - ex_p[hz - dx] +
		      ex_p[hz + dx + dy] - ex_p[hz + dx]);
	FP dey = cscheme[BY] * (ey_p[hz + dx] - ey_p[hz]) +
	    cscheme[AY] * (ey_p[hz - dy + dx] - ey_p[hz - dy] +
		      ey_p[hz + dy + dx] - ey_p[hz + dy]);
                    
#else
	FP dex = cscheme[DCZ] * (ex_p[hz + dy] - ex_p[hz]);
	FP dey = cscheme[DCZ] * (ey_p[hz + dx] - ey_p[hz]);
#endif
		
	hz_p[hz] += cscheme[DTDY] * dex - cscheme[DTDX] * dey;
    }
    __syncthreads();
}

__global__ void fdtd_3d_Hx_pml(FP* ex_p,
			       FP* ey_p,
			       FP* ez_p,
			       FP* hx_p,
			       FP* hy_p,
			       FP* hz_p,
			       FP* Hxy,
			       FP* Hxz,
			       FP* ahxy,
			       FP* ahxz,
			       FP* bhxy,
			       FP* bhxz,
			       int* idhx,
			       int nhx)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    
    int dy = cdn[DY];
    int dz = cdn[DZ];
    
    if (i < nhx)
    {
	int hx = idhx[i];
	
#ifdef USENDF
	FP dey = cscheme[BY] * (ey_p[hx + dz] - ey_p[hx]) +
	    cscheme[AY] * (ey_p[hx - dy + dz] - ey_p[hx - dy] +
		      ey_p[hx + dy + dz] - ey_p[hx + dy]);
	FP dez = cscheme[BZ] * (ez_p[hx + dy] - ez_p[hx]) +
	    cscheme[AZ] * (ez_p[hx - dz + dy] - ez_p[hx - dz] +
		      ez_p[hx + dz + dy] - ez_p[hx + dz]);
#else
	FP dey = cscheme[DCX] * (ey_p[hx + dz] - ey_p[hx]);
	FP dez = cscheme[DCX] * (ez_p[hx + dy] - ez_p[hx]);
#endif
       
	Hxy[i] = ahxy[i]*Hxy[i] + bhxy[i] * dez;
	Hxz[i] = ahxz[i]*Hxz[i] + bhxz[i] * dey;

	hx_p[hx] = Hxy[i] + Hxz[i];
    }
    __syncthreads();
}

__global__ void fdtd_3d_Hy_pml(FP* ex_p,
			       FP* ey_p,
			       FP* ez_p,
			       FP* hx_p,
			       FP* hy_p,
			       FP* hz_p,
			       FP* Hyx,
			       FP* Hyz,
			       FP* ahyx,
			       FP* ahyz,
			       FP* bhyx,
			       FP* bhyz,
			       int* idhy,
			       int nhy)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    
    int dx = cdn[DX];
    int dz = cdn[DZ];
    
    if (i < nhy)
    {
	int hy = idhy[i];
	
#ifdef USENDF
	FP dez = cscheme[BZ] * (ez_p[hy + dx] - ez_p[hy]) +
	    cscheme[AZ] * (ez_p[hy - dz + dx] - ez_p[hy - dz] +
		      ez_p[hy + dz + dx] - ez_p[hy + dz]);
	FP dex = cscheme[BX] * (ex_p[hy + dz] - ex_p[hy]) +
	    cscheme[AX] * (ex_p[hy - dx + dz] - ex_p[hy - dx] +
		      ex_p[hy + dx + dz] - ex_p[hy + dx]); 
		
#else
	FP dez = cscheme[DCY] * (ez_p[hy + dx] - ez_p[hy]);
	FP dex = cscheme[DCY] * (ex_p[hy + dz] - ex_p[hy]);
#endif
       
	Hyx[i] = ahyx[i]*Hyx[i] + bhyx[i] * dez;
	Hyz[i] = ahyz[i]*Hyz[i] + bhyz[i] * dex;
	
	hy_p[hy] = Hyx[i] + Hyz[i];
    }
    __syncthreads();
}

__global__ void fdtd_3d_Hz_pml(FP* ex_p,
			       FP* ey_p,
			       FP* ez_p,
			       FP* hx_p,
			       FP* hy_p,
			       FP* hz_p,
			       FP* Hzx,
			       FP* Hzy,
			       FP* ahzx,
			       FP* ahzy,
			       FP* bhzx,
			       FP* bhzy,
			       int* idhz,
			       int nhz)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;

    int dx = cdn[DX];
    int dy = cdn[DY];
    
    if (i < nhz)
    {
	int hz = idhz[i];
	
#ifdef USENDF
	FP dex = cscheme[BX] * (ex_p[hz + dy] - ex_p[hz]) +
	    cscheme[AX] * (ex_p[hz - dx + dy] - ex_p[hz - dx] +
		      ex_p[hz + dx + dy] - ex_p[hz + dx]);
	FP dey = cscheme[BY] * (ey_p[hz + dx] - ey_p[hz]) +
	    cscheme[AY] * (ey_p[hz - dy + dx] - ey_p[hz - dy] +
		      ey_p[hz + dy + dx] - ey_p[hz + dy]);
                    
#else
	FP dex = cscheme[DCZ] * (ex_p[hz + dy] - ex_p[hz]);
	FP dey = cscheme[DCZ] * (ey_p[hz + dx] - ey_p[hz]);
#endif
	
	Hzx[i] = ahzx[i]*Hzx[i] + bhzx[i] * dey;
	Hzy[i] = ahzy[i]*Hzy[i] + bhzy[i] * dex;
	
	hz_p[hz] = Hzx[i] + Hzy[i];
    }
    __syncthreads();
}

__global__ void fdtd_3d_Dx_pml(FP* ex_p,
			       FP* ey_p,
			       FP* ez_p,
			       FP* hx_p,
			       FP* hy_p,
			       FP* hz_p,
			       FP* Exy,
			       FP* Exz,
			       FP* aexy,
			       FP* aexz,
			       FP* bexy,
			       FP* bexz,
			       int* idex,
			       int nex)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;

    int dy = cdn[DY];
    int dz = cdn[DZ];

    if (i < nex)
    {
	int ex = idex[i];
	
#ifdef USENDF
	FP dhz = cscheme[BZ] * (hz_p[ex] - hz_p[ex - dy]) +
	   cscheme[AZ] * (hz_p[ex - dz] - hz_p[ex - dz -dy] +
			  hz_p[ex + dz] - hz_p[ex + dz -dy]);
	FP dhy = cscheme[BY] * (hy_p[ex] - hy_p[ex - dz]) +
	    cscheme[AY] * (hy_p[ex - dy] - hy_p[ex - dy -dz] +
		      hy_p[ex + dy] - hy_p[ex + dy -dz]);
#else
	FP dhz = cscheme[DCX] * (hz_p[ex] - hz_p[ex - dy]);
	FP dhy = cscheme[DCX] * (hy_p[ex] - hy_p[ex - dz]);
#endif
	
	Exy[i] = aexy[i]*Exy[i] + bexy[i] * dhz;
	Exz[i] = aexz[i]*Exz[i] + bexz[i] * dhy;

	ex_p[ex] = Exy[i] + Exz[i];
    }
    __syncthreads();
}

__global__ void fdtd_3d_Dy_pml(FP* ex_p,
			       FP* ey_p,
			       FP* ez_p,
			       FP* hx_p,
			       FP* hy_p,
			       FP* hz_p,
			       FP* Eyx,
			       FP* Eyz,
			       FP* aeyx,
			       FP* aeyz,
			       FP* beyx,
			       FP* beyz,
			       int* idey,
			       int ney)
{
    int i = blockIdx.x * BLOCK_SIZE + threadIdx.x;

    int dx = cdn[DX];
    int dz = cdn[DZ];

    if (i < ney)
    {
	int ey = idey[i];
	
#ifdef USENDF		
	FP dhx = cscheme[BX] * (hx_p[ey] - hx_p[ey - dz]) +
	    cscheme[AX] * (hx_p[ey - dx] - hx_p[ey - dx - dz] +
		      hx_p[ey + dx] - hx_p[ey + dx - dz]);
	FP dhz = cscheme[BZ] * (hz_p[ey] - hz_p[ey - dx]) +
	    cscheme[AZ] * (hz_p[ey - dz] - hz_p[ey - dz - dx] +
		      hz_p[ey + dz] - hz_p[ey + dz - dx]);
#else
	FP dhx = cscheme[DCY] * (hx_p[ey] - hx_p[ey - dz]);
	FP dhz = cscheme[DCY] * (hz_p[ey] - hz_p[ey - dx]);
#endif
	
	Eyx[i] = aeyx[i]*Eyx[i] + beyx[i] * dhz;
	Eyz[i] = aeyz[i]*Eyz[i] + beyz[i] * dhx;
	
	ey_p[ey] = Eyx[i] + Eyz[i];
    }
    __syncthreads();
}

__global__ void fdtd_3d_Dz_pml(FP* ex_p,
			       FP* ey_p,
			       FP* ez_p,
			       FP* hx_p,
			       FP* hy_p,
			       FP* hz_p,
			       FP* Ezx,
			       FP* Ezy,
			       FP* aezx,
			       FP* aezy,
			       FP* bezx,
			       FP* bezy,
			       int* idez,
			       int nez)
{
    int i = blockIdx.x * BLOCK_SIZE + threadIdx.x;

    int dx = cdn[DX];
    int dy = cdn[DY];
 
    if (i < nez)
    {
	int ez = idez[i];
#ifdef USENDF
	FP dhy = cscheme[BY] * (hy_p[ez] - hy_p[ez - dx]) +
	    cscheme[AY] * (hy_p[ez - dy] - hy_p[ez - dy - dx] +
		      hy_p[ez + dy] - hy_p[ez + dy - dx]);
	FP dhx = cscheme[BX] * (hx_p[ez] - hx_p[ez - dy]) +
	    cscheme[AX] * (hx_p[ez - dx] - hx_p[ez - dx - dy] +
		      hx_p[ez + dx] - hx_p[ez + dx - dy]);
#else
	FP dhy = cscheme[DCZ] * (hy_p[ez] - hy_p[ez - dx]);
	FP dhx = cscheme[DCZ] * (hx_p[ez] - hx_p[ez - dy]);
#endif
		
	Ezx[i] = aezx[i]*Ezx[i] + bezx[i] * dhy;
	Ezy[i] = aezy[i]*Ezy[i] + bezy[i] * dhx;
	
	ez_p[ez] = Ezx[i] + Ezy[i];
    }
    __syncthreads();   
}

void calculate_energy_3d_cuda(vfield_n* ve_p,
			      vfield_n* vd_p,
			      vfield_n* vh_p,
			      media_data* media_p,
			      time_scheme* t_p,
			      geometry_data* g_p,
			      int t,
			      const char* pathPrefix)
{
    FP energy = 0.;
    FP absorption = 0.;
    field_n* Ex_p = ve_p->comp_p[x_e];
    field_n* Ey_p = ve_p->comp_p[y_e];
    field_n* Ez_p = ve_p->comp_p[z_e];
    //field_n* Dx_p = vd_p->comp_p[x_e];
    //field_n* Dy_p = vd_p->comp_p[y_e];
    //field_n* Dz_p = vd_p->comp_p[z_e];
    field_n* Hx_p = vh_p->comp_p[x_e];
    field_n* Hy_p = vh_p->comp_p[y_e];
    field_n* Hz_p = vh_p->comp_p[z_e];
    
    for (int k=g_p->kmin; k<g_p->kmax + ADD; k++)
    {
	for (int i=g_p->imin; i<g_p->imax + ADD; i++)
	{
	    for (int j=g_p->jmin; j<g_p->jmax + ADD; j++)
	    {

		int index = INDEX_P(Ex_p,i,j,k);

		energy += Hx_p->a[index]*Hx_p->a[index] +
		    Hy_p->a[index]*Hy_p->a[index] +
		    Hz_p->a[index]*Hz_p->a[index] +
		    Ex_p->a[index]*Ex_p->a[index] +
		    Ey_p->a[index]*Ey_p->a[index] +
		    Ez_p->a[index]*Ez_p->a[index];
	    }
	}
    }

    fprintf(stderr, "Time step: %6d Energy: %12.10e[mJ] Absorption: %12.10e[mJ] Full: %12.10e[mJ] zmin=%d zmax=%d\n",
	    t, energy*media_p->c_en, absorption, energy*media_p->c_en+absorption,
	    g_p->move_index,
	    g_p->z_max + g_p->move_index);
    fflush(stderr);
}

int destroy_cuda_pml_3d(pml_data* pml_p)
{
    cuda_free(aexy);
    cuda_free(bexy);
    cuda_free(aexz);
    cuda_free(bexz);

    cuda_free(aeyx);
    cuda_free(beyx);
    cuda_free(aeyz);
    cuda_free(beyz);

    cuda_free(aezx);
    cuda_free(bezx);
    cuda_free(aezy);
    cuda_free(bezy);

    cuda_free(ahxy);
    cuda_free(bhxy);
    cuda_free(ahxz);
    cuda_free(bhxz);

    cuda_free(ahyx);
    cuda_free(bhyx);
    cuda_free(ahyz);
    cuda_free(bhyz);

    cuda_free(ahzx);
    cuda_free(bhzx);
    cuda_free(ahzy);
    cuda_free(bhzy);

    cuda_free(pml_p->cExy);
    cuda_free(pml_p->cExz);
    cuda_free(pml_p->cEyx);
    cuda_free(pml_p->cEyz);
    cuda_free(pml_p->cEzx);
    cuda_free(pml_p->cEzy);

    cuda_free(pml_p->cHxy);
    cuda_free(pml_p->cHxz);
    cuda_free(pml_p->cHyx);
    cuda_free(pml_p->cHyz);
    cuda_free(pml_p->cHzx);
    cuda_free(pml_p->cHzy);

    cuda_free_int(idex);
    cuda_free_int(idexf);
    cuda_free_int(idhx);
    cuda_free_int(idhxf);
    cuda_free_int(idey);
    cuda_free_int(ideyf);
    cuda_free_int(idhy);
    cuda_free_int(idhyf);
    cuda_free_int(idez);
    cuda_free_int(idezf);
    cuda_free_int(idhz);
    cuda_free_int(idhzf);
    return 0;
}

int init_cuda_pml_3d(pml_data* pml_p)
{
    cuda_malloc(&aexy, pml_p->aexy, pml_p->nex);
    cuda_malloc(&bexy, pml_p->bexy, pml_p->nex);
    cuda_malloc(&aexz, pml_p->aexz, pml_p->nex);
    cuda_malloc(&bexz, pml_p->bexz, pml_p->nex);

    cuda_malloc(&aeyx, pml_p->aeyx, pml_p->ney);
    cuda_malloc(&beyx, pml_p->beyx, pml_p->ney);
    cuda_malloc(&aeyz, pml_p->aeyz, pml_p->ney);
    cuda_malloc(&beyz, pml_p->beyz, pml_p->ney);

    cuda_malloc(&aezx, pml_p->aezx, pml_p->nez);
    cuda_malloc(&bezx, pml_p->bezx, pml_p->nez);
    cuda_malloc(&aezy, pml_p->aezy, pml_p->nez);
    cuda_malloc(&bezy, pml_p->bezy, pml_p->nez);

    cuda_malloc(&ahxy, pml_p->ahxy, pml_p->nhx);
    cuda_malloc(&bhxy, pml_p->bhxy, pml_p->nhx);
    cuda_malloc(&ahxz, pml_p->ahxz, pml_p->nhx);
    cuda_malloc(&bhxz, pml_p->bhxz, pml_p->nhx);

    cuda_malloc(&ahyx, pml_p->ahyx, pml_p->nhy);
    cuda_malloc(&bhyx, pml_p->bhyx, pml_p->nhy);
    cuda_malloc(&ahyz, pml_p->ahyz, pml_p->nhy);
    cuda_malloc(&bhyz, pml_p->bhyz, pml_p->nhy);

    cuda_malloc(&ahzx, pml_p->ahzx, pml_p->nhz);
    cuda_malloc(&bhzx, pml_p->bhzx, pml_p->nhz);
    cuda_malloc(&ahzy, pml_p->ahzy, pml_p->nhz);
    cuda_malloc(&bhzy, pml_p->bhzy, pml_p->nhz);

    cuda_malloc(&pml_p->cExy, pml_p->Exy, pml_p->nex);
    cuda_malloc(&pml_p->cExz, pml_p->Exz, pml_p->nex);
    cuda_malloc(&pml_p->cEyx, pml_p->Eyx, pml_p->ney);
    cuda_malloc(&pml_p->cEyz, pml_p->Eyz, pml_p->ney);
    cuda_malloc(&pml_p->cEzx, pml_p->Ezx, pml_p->nez);
    cuda_malloc(&pml_p->cEzy, pml_p->Ezy, pml_p->nez);

    cuda_malloc(&pml_p->cHxy, pml_p->Hxy, pml_p->nhx);
    cuda_malloc(&pml_p->cHxz, pml_p->Hxz, pml_p->nhx);
    cuda_malloc(&pml_p->cHyx, pml_p->Hyx, pml_p->nhy);
    cuda_malloc(&pml_p->cHyz, pml_p->Hyz, pml_p->nhy);
    cuda_malloc(&pml_p->cHzx, pml_p->Hzx, pml_p->nhz);
    cuda_malloc(&pml_p->cHzy, pml_p->Hzy, pml_p->nhz);

    cuda_malloc_int(&idex, pml_p->idex, pml_p->nex);
    cuda_malloc_int(&idexf, pml_p->idexf, pml_p->nexf);
    cuda_malloc_int(&idhx, pml_p->idhx, pml_p->nhx);
    cuda_malloc_int(&idhxf, pml_p->idhxf, pml_p->nhxf);
    cuda_malloc_int(&idey, pml_p->idey, pml_p->ney);
    cuda_malloc_int(&ideyf, pml_p->ideyf, pml_p->neyf);
    cuda_malloc_int(&idhy, pml_p->idhy, pml_p->nhy);
    cuda_malloc_int(&idhyf, pml_p->idhyf, pml_p->nhyf);
    cuda_malloc_int(&idez, pml_p->idez, pml_p->nez);
    cuda_malloc_int(&idezf, pml_p->idezf, pml_p->nezf);
    cuda_malloc_int(&idhz, pml_p->idhz, pml_p->nhz);
    cuda_malloc_int(&idhzf, pml_p->idhzf, pml_p->nhzf);
    
    return 0;
}


int reinit_cuda_pml_3d(pml_data* pml_p)
{
    cuda_realloc(&aexy, pml_p->aexy, pml_p->nex);
    cuda_realloc(&bexy, pml_p->bexy, pml_p->nex);
    cuda_realloc(&aexz, pml_p->aexz, pml_p->nex);
    cuda_realloc(&bexz, pml_p->bexz, pml_p->nex);

    cuda_realloc(&aeyx, pml_p->aeyx, pml_p->ney);
    cuda_realloc(&beyx, pml_p->beyx, pml_p->ney);
    cuda_realloc(&aeyz, pml_p->aeyz, pml_p->ney);
    cuda_realloc(&beyz, pml_p->beyz, pml_p->ney);

    cuda_realloc(&aezx, pml_p->aezx, pml_p->nez);
    cuda_realloc(&bezx, pml_p->bezx, pml_p->nez);
    cuda_realloc(&aezy, pml_p->aezy, pml_p->nez);
    cuda_realloc(&bezy, pml_p->bezy, pml_p->nez);

    cuda_realloc(&ahxy, pml_p->ahxy, pml_p->nhx);
    cuda_realloc(&bhxy, pml_p->bhxy, pml_p->nhx);
    cuda_realloc(&ahxz, pml_p->ahxz, pml_p->nhx);
    cuda_realloc(&bhxz, pml_p->bhxz, pml_p->nhx);

    cuda_realloc(&ahyx, pml_p->ahyx, pml_p->nhy);
    cuda_realloc(&bhyx, pml_p->bhyx, pml_p->nhy);
    cuda_realloc(&ahyz, pml_p->ahyz, pml_p->nhy);
    cuda_realloc(&bhyz, pml_p->bhyz, pml_p->nhy);

    cuda_realloc(&ahzx, pml_p->ahzx, pml_p->nhz);
    cuda_realloc(&bhzx, pml_p->bhzx, pml_p->nhz);
    cuda_realloc(&ahzy, pml_p->ahzy, pml_p->nhz);
    cuda_realloc(&bhzy, pml_p->bhzy, pml_p->nhz);

    cuda_realloc(&pml_p->cExy, pml_p->Exy, pml_p->nex);
    cuda_realloc(&pml_p->cExz, pml_p->Exz, pml_p->nex);
    cuda_realloc(&pml_p->cEyx, pml_p->Eyx, pml_p->ney);
    cuda_realloc(&pml_p->cEyz, pml_p->Eyz, pml_p->ney);

    cuda_realloc(&pml_p->cHxy, pml_p->Hxy, pml_p->nhx);
    cuda_realloc(&pml_p->cHxz, pml_p->Hxz, pml_p->nhx);
    cuda_realloc(&pml_p->cHyx, pml_p->Hyx, pml_p->nhy);
    cuda_realloc(&pml_p->cHyz, pml_p->Hyz, pml_p->nhy);

    cuda_realloc_int(&idex, pml_p->idex, pml_p->nex);
    cuda_realloc_int(&idexf, pml_p->idexf, pml_p->nexf);
    cuda_realloc_int(&idhx, pml_p->idhx, pml_p->nhx);
    cuda_realloc_int(&idhxf, pml_p->idhxf, pml_p->nhxf);
    cuda_realloc_int(&idey, pml_p->idey, pml_p->ney);
    cuda_realloc_int(&ideyf, pml_p->ideyf, pml_p->neyf);
    cuda_realloc_int(&idhy, pml_p->idhy, pml_p->nhy);
    cuda_realloc_int(&idhyf, pml_p->idhyf, pml_p->nhyf);
    cuda_realloc_int(&idez, pml_p->idez, pml_p->nez);
    cuda_realloc_int(&idezf, pml_p->idezf, pml_p->nezf);
    cuda_realloc_int(&idhz, pml_p->idhz, pml_p->nhz);
    cuda_realloc_int(&idhzf, pml_p->idhzf, pml_p->nhzf);
    
    return 0;
}

int init_cuda_geometry_3d(geometry_data* geometry_data_p)
{
    int geom[6];
    geom[NX] = geometry_data_p->x_max;
    geom[NY] = geometry_data_p->y_max;
    geom[NZ] = geometry_data_p->z_max;
    geom[I_OFFSET] = geometry_data_p->i_offset;
    geom[J_OFFSET] = geometry_data_p->j_offset;
    geom[K_OFFSET] = geometry_data_p->k_offset;
    int res = cudaMemcpyToSymbol(cgrid, geom, sizeof(int)*6);
    fprintf(stderr, "Init geometry Cuda: res=%d\n", res);
    fflush(stderr);
    int dn[3];
    dn[DX] = DX3(geom[NX], geom[NY], geom[NZ]);
    dn[DY] = DY3(geom[NX], geom[NY], geom[NZ]);
    dn[DZ] = DZ3(geom[NX], geom[NY], geom[NZ]);
    cudaMemcpyToSymbol(cdn, dn, sizeof(int)*3);
    return 0;
}

int cuda_init_constants(scheme_constants* sch_p)
{
    FP data[18];
    data[DTDX] = (FP)sch_p->dt_dx;
    data[DTDY] = (FP)sch_p->dt_dy;
    data[DTDZ] = (FP)sch_p->dt_dz;
    data[DT_4_PI] = (FP)sch_p->dt_4_pi;
    data[DT_FF_4_PI] = (FP)sch_p->dt_ff_4_pi;
    data[DX_4_PI] = (FP)sch_p->dx_4_pi;
    data[DY_4_PI] = (FP)sch_p->dy_4_pi;
    data[DZ_4_PI] = (FP)sch_p->dz_4_pi;
    data[DXY_4_PI] = (FP)sch_p->dxy_4_pi;
    data[AX] = (FP)sch_p->a_x;
    data[BX] = (FP)sch_p->b_x;
    data[AY] = (FP)sch_p->a_y;
    data[BY] = (FP)sch_p->b_y;
    data[AZ] = (FP)sch_p->a_z;
    data[BZ] = (FP)sch_p->b_z;
    data[DCX] = (FP)sch_p->dc_x;
    data[DCY] = (FP)sch_p->dc_y;
    data[DCZ] = (FP)sch_p->dc_z;
    cudaMemcpyToSymbol(cscheme, data, sizeof(FP)*18);
    return 0;
}


void init_psources_3d_cuda(sources_rt_data* s_p)
{
    if (s_p->pulse_enable == true)
    {
	
	if (s_p->pulse_zmin == true)
        {
	    cuda_malloc(&j_zmin, NULL, 4*s_p->z_num);
	    cuda_malloc_int(&je_zmin_idx, s_p->je_zmin_idx, s_p->z_num);
	    cuda_malloc_int(&jh_zmin_idx, s_p->jh_zmin_idx, s_p->z_num);
	}
	
	if (s_p->pulse_zmax == true)
        {
	    cuda_malloc(&j_zmax, NULL, 4*s_p->z_num);
	    cuda_malloc_int(&je_zmax_idx, s_p->je_zmax_idx, s_p->z_num);
	    cuda_malloc_int(&jh_zmax_idx, s_p->jh_zmax_idx, s_p->z_num);
	}
    }
}

void init_tfsf_3d_cuda(sources_rt_data* s_p)
{
    if (s_p->tfsf_enable == true)
    {
	if (s_p->tfsf_xmin == true)
        {
	    cuda_malloc(&w_xmin, NULL, 4*s_p->wx_num);
	    cuda_malloc_int(&we_xmin_idx, s_p->we_xmin_idx, s_p->wx_num);
	    cuda_malloc_int(&wh_xmin_idx, s_p->wh_xmin_idx, s_p->wx_num);
	}
	if (s_p->tfsf_xmax == true)
        {
	    cuda_malloc(&w_xmax, NULL, 4*s_p->wx_num);
	    cuda_malloc_int(&we_xmax_idx, s_p->we_xmax_idx, s_p->wx_num);
	    cuda_malloc_int(&wh_xmax_idx, s_p->wh_xmax_idx, s_p->wx_num);
	}

	if (s_p->tfsf_ymin == true)
        {
	    cuda_malloc(&w_ymin, NULL, 4*s_p->wy_num);
	    cuda_malloc_int(&we_ymin_idx, s_p->we_ymin_idx, s_p->wy_num);
	    cuda_malloc_int(&wh_ymin_idx, s_p->wh_ymin_idx, s_p->wy_num);
	}
	if (s_p->tfsf_ymax == true)
        {
	    cuda_malloc(&w_ymax, NULL, 4*s_p->wy_num);
	    cuda_malloc_int(&we_ymax_idx, s_p->we_ymax_idx, s_p->wy_num);
	    cuda_malloc_int(&wh_ymax_idx, s_p->wh_ymax_idx, s_p->wy_num);
	}

	if (s_p->tfsf_zmin == true)
        {
	    cuda_malloc(&w_zmin, NULL, 4*s_p->wz_num);
	    cuda_malloc_int(&we_zmin_idx, s_p->we_zmin_idx, s_p->wz_num);
	    cuda_malloc_int(&wh_zmin_idx, s_p->wh_zmin_idx, s_p->wz_num);
	}
	if (s_p->tfsf_zmax == true)
        {
	    cuda_malloc(&w_zmax, NULL, 4*s_p->wz_num);
	    cuda_malloc_int(&we_zmax_idx, s_p->we_zmax_idx, s_p->wz_num);
	    cuda_malloc_int(&wh_zmax_idx, s_p->wh_zmax_idx, s_p->wz_num);
	}
	
    }
}

void destroy_tfsf_3d_cuda(sources_rt_data* s_p)
{
    if (s_p->tfsf_enable == true)
    {
	if (s_p->tfsf_xmin == true)
        {
	    cudaFree(w_xmin);
	    cudaFree(we_xmin_idx);
	    cudaFree(wh_xmin_idx);
	}
	if (s_p->tfsf_xmax == true)
        {
	    cudaFree(w_xmax);
	    cudaFree(we_xmax_idx);
	    cudaFree(wh_xmax_idx);
	}

	if (s_p->tfsf_ymin == true)
        {
	    cudaFree(w_ymin);
	    cudaFree(we_ymin_idx);
	    cudaFree(wh_ymin_idx);
	}
	if (s_p->tfsf_ymax == true)
        {
	    cudaFree(w_ymax);
	    cudaFree(we_ymax_idx);
	    cudaFree(wh_ymax_idx);
	}

	if (s_p->tfsf_zmin == true)
        {
	    cudaFree(w_zmin);
	    cudaFree(we_zmin_idx);
	    cudaFree(wh_zmin_idx);
	}
	if (s_p->tfsf_zmax == true)
        {
	    cudaFree(w_zmax);
	    cudaFree(we_zmax_idx);
	    cudaFree(wh_zmax_idx);
	}
	
    }
}

void destroy_psources_3d_cuda(sources_rt_data* s_p)
{
    if (s_p->pulse_enable == true)
    {
	if (s_p->pulse_zmin == true)
        {
	    cudaFree(j_zmin);
	    cudaFree(je_zmin_idx);
	    cudaFree(jh_zmin_idx);
	}
	
	if (s_p->pulse_zmax == true)
        {
	    cudaFree(j_zmax);
	    cudaFree(je_zmax_idx);
	    cudaFree(jh_zmax_idx);
	}
    }
}

void update_sources_rt3d_cuda(sources_rt_data* s_p)
{
    if (s_p->pulse_enable == true)
    {
	if (s_p->pulse_zmin == true)
	{
	    int ssize = s_p->z_num*sizeof(FP);
	    memcpy((void*)s_p->j_zmin, (void*)s_p->Jex_zmin_p->a, ssize);
	    memcpy((void*)(s_p->j_zmin + s_p->z_num), (void*)s_p->Jey_zmin_p->a, ssize);
	    memcpy((void*)(s_p->j_zmin + 2*s_p->z_num), (void*)s_p->Jhx_zmin_p->a, ssize);
	    memcpy((void*)(s_p->j_zmin + 3*s_p->z_num), (void*)s_p->Jhy_zmin_p->a, ssize);
	    copy_cuda_h2d(s_p->j_zmin, j_zmin, 4*s_p->z_num);
	}

	if (s_p->pulse_zmax == true)
	{
	    int ssize = s_p->z_num*sizeof(FP);
	    memcpy((void*)s_p->j_zmax, s_p->Jex_zmax_p->a, ssize);
	    memcpy((void*)(s_p->j_zmax + s_p->z_num), (void*)s_p->Jey_zmax_p->a, ssize);
	    memcpy((void*)(s_p->j_zmax + 2*s_p->z_num), (void*)s_p->Jhx_zmax_p->a, ssize);
	    memcpy((void*)(s_p->j_zmax + 3*s_p->z_num), (void*)s_p->Jhy_zmax_p->a, ssize);
	    copy_cuda_h2d(s_p->j_zmax, j_zmax, 4*s_p->z_num);
	}
    }

    if (s_p->tfsf_enable == true)
    {
	if (s_p->tfsf_xmin == true)
        {
	    int size = s_p->wx_num;
	    int ssize = size*sizeof(FP);
	    memcpy((void*)(s_p->w_xmin),          (void*)s_p->tfsf_ey_xmin_p->a, ssize);
	    memcpy((void*)(s_p->w_xmin + size),   (void*)s_p->tfsf_ez_xmin_p->a, ssize);
	    memcpy((void*)(s_p->w_xmin + 2*size), (void*)s_p->tfsf_hy_xmin_p->a, ssize);
	    memcpy((void*)(s_p->w_xmin + 3*size), (void*)s_p->tfsf_hz_xmin_p->a, ssize);
	    copy_cuda_h2d(s_p->w_xmin, w_xmin, 4*size);
	}

	if (s_p->tfsf_xmax == true)
        {
	    int size = s_p->wx_num;
	    int ssize = size*sizeof(FP);
	    memcpy((void*)(s_p->w_xmax),          (void*)s_p->tfsf_ey_xmax_p->a, ssize);
	    memcpy((void*)(s_p->w_xmax + size),   (void*)s_p->tfsf_ez_xmax_p->a, ssize);
	    memcpy((void*)(s_p->w_xmax + 2*size), (void*)s_p->tfsf_hy_xmax_p->a, ssize);
	    memcpy((void*)(s_p->w_xmax + 3*size), (void*)s_p->tfsf_hz_xmax_p->a, ssize);
	    copy_cuda_h2d(s_p->w_xmax, w_xmax, 4*size);
	}

	if (s_p->tfsf_ymin == true)
        {
	    int size = s_p->wy_num;
	    int ssize = size*sizeof(FP);
	    memcpy((void*)(s_p->w_ymin),          (void*)s_p->tfsf_ex_ymin_p->a, ssize);
	    memcpy((void*)(s_p->w_ymin + size),   (void*)s_p->tfsf_ez_ymin_p->a, ssize);
	    memcpy((void*)(s_p->w_ymin + 2*size), (void*)s_p->tfsf_hx_ymin_p->a, ssize);
	    memcpy((void*)(s_p->w_ymin + 3*size), (void*)s_p->tfsf_hz_ymin_p->a, ssize);
	    copy_cuda_h2d(s_p->w_ymin, w_ymin, 4*size);
	}

	if (s_p->tfsf_ymax == true)
        {
	    int size = s_p->wy_num;
	    int ssize = size*sizeof(FP);
	    memcpy((void*)(s_p->w_ymax),          (void*)s_p->tfsf_ex_ymax_p->a, ssize);
	    memcpy((void*)(s_p->w_ymax + size),   (void*)s_p->tfsf_ez_ymax_p->a, ssize);
	    memcpy((void*)(s_p->w_ymax + 2*size), (void*)s_p->tfsf_hx_ymax_p->a, ssize);
	    memcpy((void*)(s_p->w_ymax + 3*size), (void*)s_p->tfsf_hz_ymax_p->a, ssize);
	    copy_cuda_h2d(s_p->w_ymax, w_ymax, 4*size);
	}

	if (s_p->tfsf_zmin == true)
        {
	    int size = s_p->wz_num;
	    int ssize = size*sizeof(FP);
	    memcpy((void*)(s_p->w_zmin),          (void*)s_p->tfsf_ex_zmin_p->a, ssize);
	    memcpy((void*)(s_p->w_zmin + size),   (void*)s_p->tfsf_ey_zmin_p->a, ssize);
	    memcpy((void*)(s_p->w_zmin + 2*size), (void*)s_p->tfsf_hx_zmin_p->a, ssize);
	    memcpy((void*)(s_p->w_zmin + 3*size), (void*)s_p->tfsf_hy_zmin_p->a, ssize);
	    copy_cuda_h2d(s_p->w_zmin, w_zmin, 4*size);
	}

	if (s_p->tfsf_zmax == true)
        {
	    int size = s_p->wz_num;
	    int ssize = size*sizeof(FP);
	    memcpy((void*)(s_p->w_zmax),          (void*)s_p->tfsf_ex_zmax_p->a, ssize);
	    memcpy((void*)(s_p->w_zmax + size),   (void*)s_p->tfsf_ey_zmax_p->a, ssize);
	    memcpy((void*)(s_p->w_zmax + 2*size), (void*)s_p->tfsf_hx_zmax_p->a, ssize);
	    memcpy((void*)(s_p->w_zmax + 3*size), (void*)s_p->tfsf_hy_zmax_p->a, ssize);
	    copy_cuda_h2d(s_p->w_zmax, w_zmax, 4*size);
	}
    }
}

__global__ void add_tfsf_3d_D_xmin(FP* ey_p,
				   FP* ez_p,
				   FP* w_xmin,
				   int* we_xmin_idx,
				   int num)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    
    if (i < num)
    {
	int idx = we_xmin_idx[i];
	ey_p[idx] += cscheme[DTDX] * w_xmin[i+3*num];
	ez_p[idx] -= cscheme[DTDX] * w_xmin[i+2*num];
    }
    __syncthreads(); 
}

__global__ void add_tfsf_3d_D_xmax(FP* ey_p,
				   FP* ez_p,
				   FP* w_xmax,
				   int* we_xmax_idx,
				   int num)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    
    if (i < num)
    {
	int idx = we_xmax_idx[i];
	ey_p[idx] -= cscheme[DTDX] * w_xmax[i+3*num];
	ez_p[idx] += cscheme[DTDX] * w_xmax[i+2*num];
    }
    __syncthreads();
}

__global__ void add_tfsf_3d_D_ymin(FP* ex_p,
				   FP* ez_p,
				   FP* w_ymin,
				   int* we_ymin_idx,
				   int num)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    
    if (i < num)
    {
	int idx = we_ymin_idx[i];
	ex_p[idx] -= cscheme[DTDY] * w_ymin[i+3*num];
	ez_p[idx] += cscheme[DTDY] * w_ymin[i+2*num];
    }
    __syncthreads(); 
}

__global__ void add_tfsf_3d_D_ymax(FP* ex_p,
				   FP* ez_p,
				   FP* w_ymax,
				   int* we_ymax_idx,
				   int num)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    
    if (i < num)
    {
	int idx = we_ymax_idx[i];
	ex_p[idx] += cscheme[DTDY] * w_ymax[i+3*num];
	ez_p[idx] -= cscheme[DTDY] * w_ymax[i+2*num];
    }
    __syncthreads();
}

__global__ void add_tfsf_3d_D_zmin(FP* ex_p,
				   FP* ey_p,
				   FP* w_zmin,
				   int* we_zmin_idx,
				   int num)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    
    if (i < num)
    {
	int idx = we_zmin_idx[i];
	ex_p[idx] += cscheme[DTDZ] * w_zmin[i+3*num];
	ey_p[idx] -= cscheme[DTDZ] * w_zmin[i+2*num];
    }
    __syncthreads();
}

__global__ void add_tfsf_3d_D_zmax(FP* ex_p,
				   FP* ey_p,
				   FP* w_zmax,
				   int* we_zmax_idx,
				   int num)
{
     int i = blockIdx.x * blockDim.x + threadIdx.x;
    
    if (i < num)
    {
	int idx = we_zmax_idx[i];
	ex_p[idx] -= cscheme[DTDZ] * w_zmax[i+3*num];
	ey_p[idx] += cscheme[DTDZ] * w_zmax[i+2*num];
    }
    __syncthreads();   
}

__global__ void add_tfsf_3d_H_xmin(FP* hy_p,
				   FP* hz_p,
				   FP* w_xmin,
				   int* wh_xmin_idx,
				   int num)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    
    if (i < num)
    {
	int idx = wh_xmin_idx[i];
	hy_p[idx] -= cscheme[DTDX] * w_xmin[i+num];
	hz_p[idx] += cscheme[DTDX] * w_xmin[i];
    }
    __syncthreads();
}

__global__ void add_tfsf_3d_H_xmax(FP* hy_p,
				   FP* hz_p,
				   FP* w_xmax,
				   int* wh_xmax_idx,
				   int num)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    
    if (i < num)
    {
	int idx = wh_xmax_idx[i];
	hy_p[idx] += cscheme[DTDX] * w_xmax[i+num];
	hz_p[idx] -= cscheme[DTDX] * w_xmax[i];
    }
    __syncthreads();
}

__global__ void add_tfsf_3d_H_ymin(FP* hx_p,
				   FP* hz_p,
				   FP* w_ymin,
				   int* wh_ymin_idx,
				   int num)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    
    if (i < num)
    {
	int idx = wh_ymin_idx[i];
	hx_p[idx] += cscheme[DTDY] * w_ymin[i+num];
	hz_p[idx] -= cscheme[DTDY] * w_ymin[i];
    }
    __syncthreads();
}

__global__ void add_tfsf_3d_H_ymax(FP* hx_p,
				   FP* hz_p,
				   FP* w_ymax,
				   int* wh_ymax_idx,
				   int num)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    
    if (i < num)
    {
	int idx = wh_ymax_idx[i];
	hx_p[idx] -= cscheme[DTDY] * w_ymax[i+num];
	hz_p[idx] += cscheme[DTDY] * w_ymax[i];
    }
    __syncthreads(); 
}

__global__ void add_tfsf_3d_H_zmin(FP* hx_p,
				   FP* hy_p,
				   FP* w_zmin,
				   int* wh_zmin_idx,
				   int num)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    
    if (i < num)
    {
	int idx = wh_zmin_idx[i];
	hx_p[idx] -= cscheme[DTDZ] * w_zmin[i+num];
	hy_p[idx] += cscheme[DTDZ] * w_zmin[i];
    }
    __syncthreads();
}

__global__ void add_tfsf_3d_H_zmax(FP* hx_p,
				   FP* hy_p,
				   FP* w_zmax,
				   int* wh_zmax_idx,
				   int num)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    
    if (i < num)
    {
	int idx = wh_zmax_idx[i];
	hx_p[idx] += cscheme[DTDZ] * w_zmax[i+num];
	hy_p[idx] -= cscheme[DTDZ] * w_zmax[i];
    }
    __syncthreads();
}

__global__ void add_psources_3d_D_zmin(FP* ex_p,
				       FP* ey_p,
				       FP* j_zmin,
				       int* je_zmin_idx,
				       int num)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    
    if (i < num)
    {
	int zmin_idx = je_zmin_idx[i];
	ex_p[zmin_idx] += cscheme[DT_4_PI] * j_zmin[i];
	ey_p[zmin_idx] += cscheme[DT_4_PI] * j_zmin[i+num];
    }
    __syncthreads(); 
}

__global__ void add_psources_3d_D_zmax(FP* ex_p,
				       FP* ey_p,
				       FP* j_zmax,
				       int* je_zmax_idx,
				       int num)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    
    if (i < num)
    {
	int zmax_idx = je_zmax_idx[i];
	ex_p[zmax_idx] += cscheme[DT_4_PI] * j_zmax[i];
	ey_p[zmax_idx] += cscheme[DT_4_PI] * j_zmax[i+num];	   
    }
    __syncthreads(); 
}

__global__ void add_psources_3d_H_zmin(FP* hx_p,
				       FP* hy_p,
				       FP* j_zmin,
				       int* jh_zmin_idx,
				       int num)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    
    if (i < num)
    {
	int zmin_idx = jh_zmin_idx[i];
	hx_p[zmin_idx] += cscheme[DT_4_PI] * j_zmin[i+2*num];
	hy_p[zmin_idx] += cscheme[DT_4_PI] * j_zmin[i+3*num];
    }
    __syncthreads(); 
}

__global__ void add_psources_3d_H_zmax(FP* hx_p,
				       FP* hy_p,
				       FP* j_zmax,
				       int* jh_zmax_idx,
				       int num)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    
    if (i < num)
    {
	int zmax_idx = jh_zmax_idx[i];
	hx_p[zmax_idx] += cscheme[DT_4_PI] * j_zmax[i+2*num];
	hy_p[zmax_idx] += cscheme[DT_4_PI] * j_zmax[i+3*num];	   
    }
    __syncthreads(); 
}


int run_2D_TM_config(configTable* fdtdConfig, 
		     geometry_data geometry,
		     time_scheme timescheme,
		     const char* pathPrefix)
{
    return 0;
}

int run_2D_TE_config(configTable* fdtdConfig, 
		     geometry_data geometry,
		     time_scheme timescheme,
		     const char* pathPrefix)
{
    sources_rt_data sources_rt;
    pml_data        pml;
    media_data      media;
    save_data       sdata;
    scheme_constants scheme;
    decompose_data* decomp_p = NULL;

    decomp_p = init_decomposition(fdtdConfig, 
				  &geometry);

    init_constants(&scheme, 
		   &geometry,
		   &timescheme);

    init_pml(fdtdConfig, &pml, &geometry, &timescheme, &scheme);

    init_sources_rt(fdtdConfig, &geometry, &pml, &sources_rt, NULL, NULL);

    init_savedata(fdtdConfig, &sdata);

    cuda_field Ey, Ey_r, Jy, Hx, Hz;
    //cuda_field* Ey_p;
    
    init_cuda_field_2d(&Ey,  geometry.x_max, geometry.z_max);
    init_cuda_field_2d(&Jy,  geometry.x_max, geometry.z_max);
    init_cuda_field_2d(&Hx,  geometry.x_max, geometry.z_max);
    init_cuda_field_2d(&Hz,  geometry.x_max, geometry.z_max);

    // setup execution parameters
    dim3  dimBlock(BLOCK_SIZE, BLOCK_SIZE);
    dim3  grid(geometry.x_max/dimBlock.x, geometry.z_max/dimBlock.y);
    
    init_media(fdtdConfig, 
	       &media, 
	       &geometry,
	       &timescheme,
	       &scheme);

    if (media.recalc == true)
    {
	init_cuda_field_2d(&Ey_r,  geometry.x_max, geometry.z_max);
	//Ey_p = &Ey_r;
    }
    else
    {
	init_cuda_field_2d(&Ey_r,  0, 0);
	//Ey_p = &Ey;
    }
    
    RANK_S(0);
    print_sources_rt(&sources_rt,
		     &media,
		     &geometry,
		     &timescheme,
		     pathPrefix);
    print_media(&media);

    fprintf(stderr, "\nRunning main loop\n");
    RANK_F(0);

    for (int t = 0; t < timescheme.t_max+1; t++)
    {
	fdtd_2dte_Ey<<< grid, dimBlock>>>(Ey.d_p,
					  Hx.d_p,
					  Hz.d_p,
					  Jy.d_p,
					  t*timescheme.delta_t,
					  NULL,
					  NULL,
					  NULL,
					  NULL,
					  NULL,
					  NULL,
					  NULL,
					  NULL);

	fdtd_2dte_Hx<<< grid, dimBlock>>>(Hx.d_p,
					  Ey.d_p);

	fdtd_2dte_Hz<<< grid, dimBlock>>>(Hz.d_p,
					  Ey.d_p);
	
	
    }/*for t */
//    sleep(1);
    
    destroy_cuda_field(&Ey);
    destroy_cuda_field(&Hx);
    destroy_cuda_field(&Hz);
    
    destroy_cuda_field(&Jy);
    destroy_sources_rt(&sources_rt, &geometry);
    destroy_media(&media);
    destroy_pml2d(&pml);
    destroy_decomposition(decomp_p);
    
    return 0;
}

__global__ void fdtd_2dte_Hz(FP* hz_p,
			     FP* ey_p)
{
    __syncthreads(); 
}

__global__ void fdtd_2dte_Hx(FP* hx_p,
			     FP* ey_p)
{
    __syncthreads(); 
}


__global__ void fdtd_2dte_Ey(FP* ey_p,
			     FP* hx_p,
			     FP* hz_p,
			     FP* jey_p,
			     FP  t_f,
			     FP* Eyx_x_l,
			     FP* Eyz_x_l,
			     FP* Eyx_x_h,
			     FP* Eyz_x_h,
			     FP* Eyz_z_l,
			     FP* Eyx_z_l,
			     FP* Eyz_z_h,
			     FP* Eyx_z_h)
{
    __syncthreads(); 
}

void init_media_3d_cuda(media_data* m_p)
{
    cuda_malloc(&epsilon, m_p->epsilon.a, m_p->ncells);
    cuda_malloc(&n_max, m_p->n_max.a, m_p->ncells);
}

void destroy_media_3d_cuda(media_data* m_p)
{
    cudaFree(epsilon);
    cudaFree(n_max);
}

void update_moving_window_3d_cuda(vfield_n* D_p, vfield_n* E_p, vfield_n* H_p, vfield_n* J_p,
				  cuda_field* cdx_p,
				  cuda_field* cdy_p,
				  cuda_field* cdz_p,
				  cuda_field* cex_p,
				  cuda_field* cey_p,
				  cuda_field* cez_p,
				  cuda_field* chx_p,
				  cuda_field* chy_p,
				  cuda_field* chz_p,
				  cuda_field* cjx_p,
				  cuda_field* cjy_p,
				  cuda_field* cjz_p,
				  geometry_data* g_p,
				  time_scheme* t_p,
				  media_data* m_p,
				  pml_data* p_p,
				  points_data_3d* pd_p,
				  sources_rt_data* s_p,
				  scheme_constants* sch_p,
				  int t,
				  const char* pathPrefix)
{
    if (g_p->moving == 2)
    {
	int start = (int)(0.45 * g_p->z_max);
	int thres = (int)(0.55 * g_p->z_max);

	if (t > t_p->t0)
	{
	    if  ((t-t_p->t0) % t_p->t_move == 0)
	    {
		int z_c = 0.;

		if (m_p->recalc == true)
		{
		    copy_cuda_field_d2h(cex_p, E_p->comp_p[x_e]->a);
		    copy_cuda_field_d2h(cey_p, E_p->comp_p[y_e]->a);
		    copy_cuda_field_d2h(cez_p, E_p->comp_p[z_e]->a);
		}
		copy_cuda_field_d2h(chx_p, H_p->comp_p[x_e]->a);
		copy_cuda_field_d2h(chy_p, H_p->comp_p[y_e]->a);
		copy_cuda_field_d2h(chz_p, H_p->comp_p[z_e]->a);
		copy_cuda_field_d2h(cdx_p, D_p->comp_p[x_e]->a);
		copy_cuda_field_d2h(cdy_p, D_p->comp_p[y_e]->a);
		copy_cuda_field_d2h(cdz_p, D_p->comp_p[z_e]->a);
		copy_cuda_field_d2h(cjx_p, J_p->comp_p[x_e]->a);
		copy_cuda_field_d2h(cjy_p, J_p->comp_p[y_e]->a);
		copy_cuda_field_d2h(cjz_p, J_p->comp_p[z_e]->a);
			
		if (t_p->v_move == 0.)
		{
		    z_c = calculate_pulse_center(E_p, D_p, H_p, g_p);
		}

		if (z_c > thres || t_p->v_move != 0.)
		{
		    if (t_p->v_move == 0.)
		    {
			g_p->move = z_c - start;
		    }

		    transfer_pml_d2h(p_p);
		    
		    swap_all_3d(D_p, H_p, J_p, g_p, m_p, p_p, g_p->move);
		    clear_pml_z(p_p);
		    update_point_data_3d(pd_p, g_p, t, E_p);
		    
		    transfer_pml_h2d(p_p);
		    copy_cuda_field_h2d(cdx_p, D_p->comp_p[x_e]->a);
		    copy_cuda_field_h2d(cdy_p, D_p->comp_p[y_e]->a);
		    copy_cuda_field_h2d(cdz_p, D_p->comp_p[z_e]->a);
		    copy_cuda_field_h2d(chx_p, H_p->comp_p[x_e]->a);
		    copy_cuda_field_h2d(chy_p, H_p->comp_p[y_e]->a);
		    copy_cuda_field_h2d(chz_p, H_p->comp_p[z_e]->a);
		    copy_cuda_field_h2d(cjx_p, J_p->comp_p[x_e]->a);
		    copy_cuda_field_h2d(cjy_p, J_p->comp_p[y_e]->a);
		    copy_cuda_field_h2d(cjz_p, J_p->comp_p[z_e]->a);
		}
	    }
	}
	else if (t == t_p->t0)
	{
	    copy_cuda_field_d2h(cdx_p, D_p->comp_p[x_e]->a);
	    copy_cuda_field_d2h(cdy_p, D_p->comp_p[y_e]->a);
	    copy_cuda_field_d2h(cdz_p, D_p->comp_p[z_e]->a);
	    copy_cuda_field_d2h(chx_p, H_p->comp_p[x_e]->a);
	    copy_cuda_field_d2h(chy_p, H_p->comp_p[y_e]->a);
	    copy_cuda_field_d2h(chz_p, H_p->comp_p[z_e]->a);
	    copy_cuda_field_d2h(cjx_p, J_p->comp_p[x_e]->a);
	    copy_cuda_field_d2h(cjy_p, J_p->comp_p[y_e]->a);
	    copy_cuda_field_d2h(cjz_p, J_p->comp_p[z_e]->a);
	    transfer_pml_d2h(p_p);
	    
	    swap_all_3d(D_p, H_p, J_p, g_p, m_p, p_p, p_p->pmlz+2);
	    zero_sources_rt(s_p);
	    zero_pml_z(p_p, g_p, t_p, sch_p);
	    clear_pml_z(p_p);
	    update_point_data_3d(pd_p, g_p, t, D_p);
	    
	    reinit_cuda_pml_3d(p_p);
	    transfer_pml_h2d(p_p);
	    copy_cuda_field_h2d(cdx_p, D_p->comp_p[x_e]->a);
	    copy_cuda_field_h2d(cdy_p, D_p->comp_p[y_e]->a);
	    copy_cuda_field_h2d(cdz_p, D_p->comp_p[z_e]->a);
	    copy_cuda_field_h2d(chx_p, H_p->comp_p[x_e]->a);
	    copy_cuda_field_h2d(chy_p, H_p->comp_p[y_e]->a);
	    copy_cuda_field_h2d(chz_p, H_p->comp_p[z_e]->a);
	    copy_cuda_field_h2d(cjx_p, J_p->comp_p[x_e]->a);
	    copy_cuda_field_h2d(cjy_p, J_p->comp_p[y_e]->a);
	    copy_cuda_field_h2d(cjz_p, J_p->comp_p[z_e]->a);
	}
    }
}

void swap_media_3d_cuda(media_data* m_p)
{
    cuda_memcpy_h2d(epsilon, m_p->epsilon.a, m_p->ncells);
    cuda_memcpy_h2d(n_max, m_p->n_max.a, m_p->ncells);
}

void transfer_pml_d2h(pml_data* p_p)
{
    cuda_memcpy_d2h(p_p->Exy, p_p->cExy, p_p->nex);
    cuda_memcpy_d2h(p_p->Exz, p_p->cExz, p_p->nex);
    cuda_memcpy_d2h(p_p->Hxy, p_p->cHxy, p_p->nhx);
    cuda_memcpy_d2h(p_p->Hxz, p_p->cHxz, p_p->nhx);

    cuda_memcpy_d2h(p_p->Eyx, p_p->cEyx, p_p->ney);
    cuda_memcpy_d2h(p_p->Eyz, p_p->cEyz, p_p->ney);
    cuda_memcpy_d2h(p_p->Hyx, p_p->cHyx, p_p->nhy);
    cuda_memcpy_d2h(p_p->Hyz, p_p->cHyz, p_p->nhy);

    cuda_memcpy_d2h(p_p->Ezx, p_p->cEzx, p_p->nez);
    cuda_memcpy_d2h(p_p->Ezy, p_p->cEzy, p_p->nez);
    cuda_memcpy_d2h(p_p->Hzx, p_p->cHzx, p_p->nhz);
    cuda_memcpy_d2h(p_p->Hzy, p_p->cHzy, p_p->nhz);
}

void transfer_pml_h2d(pml_data* p_p)
{
    cuda_memcpy_h2d(p_p->cExy, p_p->Exy, p_p->nex);
    cuda_memcpy_h2d(p_p->cExz, p_p->Exz, p_p->nex);
    cuda_memcpy_h2d(p_p->cHxy, p_p->Hxy, p_p->nhx);
    cuda_memcpy_h2d(p_p->cHxz, p_p->Hxz, p_p->nhx);

    cuda_memcpy_h2d(p_p->cEyx, p_p->Eyx, p_p->ney);
    cuda_memcpy_h2d(p_p->cEyz, p_p->Eyz, p_p->ney);
    cuda_memcpy_h2d(p_p->cHyx, p_p->Hyx, p_p->nhy);
    cuda_memcpy_h2d(p_p->cHyz, p_p->Hyz, p_p->nhy);

    cuda_memcpy_h2d(p_p->cEzx, p_p->Ezx, p_p->nez);
    cuda_memcpy_h2d(p_p->cEzy, p_p->Ezy, p_p->nez);
    cuda_memcpy_h2d(p_p->cHzx, p_p->Hzx, p_p->nhz);
    cuda_memcpy_h2d(p_p->cHzy, p_p->Hzy, p_p->nhz);
}

void cuda_memcpy_h2d(FP* dst_p, FP* src_p, int size)
{
    int res = cudaMemcpy(dst_p, src_p, size*sizeof(FP), cudaMemcpyHostToDevice);
    if (res != cudaSuccess)
    {
	fprintf(stderr, "cudaMemcpy error, res=%d\n", res);
	fflush(stderr);
    }
}

void cuda_memcpy_d2h(FP* dst_p, FP* src_p, int size)
{
    int res = cudaMemcpy(dst_p, src_p, size*sizeof(FP), cudaMemcpyDeviceToHost);
    if (res != cudaSuccess)
    {
	fprintf(stderr, "cudaMemcpy error, res=%d\n", res);
	fflush(stderr);
    }
}

void init_point_data_3d_cuda(points_data_3d* pnt_p)
{
    pnt_p->t_last = 0;
    if (pnt_p->num_points != 0)
    {
	int count = pnt_p->num_points * 6 * NUM_ITERATIONS;
	pnt_p->h_p = FP_MALLOC(count*sizeof(FP));
	cuda_malloc(&pnt_p->d_p, pnt_p->h_p, count);
    }
}

void destroy_point_data_3d_cuda(points_data_3d* pnt_p)
{
    if (pnt_p->num_points != 0)
    {
	free(pnt_p->h_p);
	cudaFree(pnt_p->d_p);
    }
}

void store_point_data_3d_cuda(points_data_3d* pnt_p,
			      geometry_data* geometry_p,
			      int t,
			      cuda_field* cex_p,
			      cuda_field* cey_p,
			      cuda_field* cez_p,
			      cuda_field* chx_p,
			      cuda_field* chy_p,
			      cuda_field* chz_p)
{
    if (0 != pnt_p->num_points)
    {
	int count = (t+1)%NUM_ITERATIONS;
        for(int i=0; i<pnt_p->num_points; i++)
        {
	    one_point_data_3d* p_p = &pnt_p->points_p[i];
	    int offset = pnt_p->num_points * 6 * count;
	    int idx = p_p->index; 

	    if (pnt_p->points_p[i].enable == true)
	    {
		store_point_data_gpu_3d<<<1,1>>>(pnt_p->d_p,
						 cex_p->d_p,
						 cey_p->d_p,
						 cez_p->d_p,
						 chx_p->d_p,
						 chy_p->d_p,
						 chz_p->d_p,
						 offset,
						 idx); 
	    }
	    
	}
		    
	if (count == 0)
	{
	    store_buffered_point_data_3d(pnt_p);
	    pnt_p->t_last += NUM_ITERATIONS;
	}
    }
}

__global__ void store_point_data_gpu_3d(FP* buffer,
					FP* cdx_p,
					FP* cdy_p,
					FP* cdz_p,
					FP* chx_p,
					FP* chy_p,
					FP* chz_p,
					int offset,
					int idx)
{
    buffer[offset++] = cdx_p[idx];
    buffer[offset++] = cdy_p[idx];
    buffer[offset++] = cdz_p[idx];
    buffer[offset++] = chx_p[idx];
    buffer[offset++] = chy_p[idx];
    buffer[offset++] = chz_p[idx];
}

void store_buffered_point_data_3d(points_data_3d* pnt_p)
{
    cuda_memcpy_d2h(pnt_p->h_p, pnt_p->d_p, pnt_p->num_points * 6 * NUM_ITERATIONS);
    
    for(int i=0; i<pnt_p->num_points; i++)
    {
	for(int t = 0; t < NUM_ITERATIONS; t++) 
	if (pnt_p->points_p[i].enable == true)
	{
	    one_point_data_3d* p_p = &pnt_p->points_p[i];
	    int count = pnt_p->num_points * 6 * t;
	    FP ex = pnt_p->h_p[count++];
	    FP ey = pnt_p->h_p[count++];
	    FP ez = pnt_p->h_p[count++];
	    FP fabs = (ex*ex+ey*ey+ez*ez);
	    FP hx = pnt_p->h_p[count++];
	    FP hy = pnt_p->h_p[count++];
	    FP hz = pnt_p->h_p[count++];
	    FP n = 0.;
	    FP abs = 0.;
            
	    fprintf(p_p->out,"%d %le %le %le %le %le %le %le %le %le %le\n",
		    t + 1 + pnt_p->t_last - p_p->t0,
		    ex,
		    ey,
		    ez,
		    sqrt(fabs),
		    hx,
		    hy,
		    hz,
		    sqrt(hx*hx+hy*hy+hz*hz),
		    n,
		    abs);
	    fflush(p_p->out);
	}
    }
}

void printDevProp(cudaDeviceProp devProp, int n)
{
    fprintf(stderr, "Card %d info\n", n);
    fprintf(stderr, "[%d]Major revision number:         %d\n", n,  devProp.major);
    fprintf(stderr, "[%d]Minor revision number:         %d\n", n,  devProp.minor);
    fprintf(stderr, "[%d]Name:                          %s\n", n,  devProp.name);
    fprintf(stderr, "[%d]Total global memory:           %u\n", n,  devProp.totalGlobalMem);
    fprintf(stderr, "[%d]Total shared memory per block: %u\n", n,  devProp.sharedMemPerBlock);
    fprintf(stderr, "[%d]Total registers per block:     %d\n", n,  devProp.regsPerBlock);
    fprintf(stderr, "[%d]Warp size:                     %d\n", n,  devProp.warpSize);
    fprintf(stderr, "[%d]Maximum memory pitch:          %u\n", n,  devProp.memPitch);
    fprintf(stderr, "[%d]Maximum threads per block:     %d\n", n,  devProp.maxThreadsPerBlock);
    for (int i = 0; i < 3; ++i)
    {
	fprintf(stderr, "[%d]Maximum dimension %d of block:  %d\n", n, i, devProp.maxThreadsDim[i]);
    }
    for (int i = 0; i < 3; ++i)
    {
	fprintf(stderr, "[%d]Maximum dimension %d of grid:   %d\n", n, i, devProp.maxGridSize[i]);
    }
    fprintf(stderr, "[%d]Clock rate:                    %d\n",  n, devProp.clockRate);
    fprintf(stderr, "[%d]Total constant memory:         %u\n",  n, devProp.totalConstMem);
    fprintf(stderr, "[%d]Texture alignment:             %u\n",  n, devProp.textureAlignment);
    fprintf(stderr, "[%d]Concurrent copy and execution: %s\n",  n, (devProp.deviceOverlap ? "Yes" : "No"));
    fprintf(stderr, "[%d]Number of multiprocessors:     %d\n",  n, devProp.multiProcessorCount);
    fprintf(stderr, "[%d]Kernel execution timeout:      %s\n",  n, (devProp.kernelExecTimeoutEnabled ? "Yes" : "No"));
    fflush(stderr);
    return;
}

#ifdef USEMPI
int delete_mpi_sendrecv_buffer3d_cuda(mpi_data* mpi_p)
{
    cudaFree(mpi_p->crecv_down_x_idx);
    cudaFree(mpi_p->crecv_down_y_idx);
    cudaFree(mpi_p->crecv_down_z_idx);
    cudaFree(mpi_p->crecv_up_x_idx);
    cudaFree(mpi_p->crecv_up_y_idx);
    cudaFree(mpi_p->crecv_up_z_idx);
    cudaFree(mpi_p->csend_down_x_idx);
    cudaFree(mpi_p->csend_down_y_idx);
    cudaFree(mpi_p->csend_down_z_idx);
    cudaFree(mpi_p->csend_up_x_idx);
    cudaFree(mpi_p->csend_up_y_idx);
    cudaFree(mpi_p->csend_up_z_idx);
    cudaFree(mpi_p->csb3d);
    cudaFree(mpi_p->crb3d);
    return 0;
}

int init_mpi_sendrecv_buffer3d_cuda(mpi_data* mpi_p)
{
    cuda_malloc_int(&mpi_p->crecv_down_x_idx, mpi_p->recv_down_x_idx, mpi_p->x_num);
    cuda_malloc_int(&mpi_p->crecv_down_y_idx, mpi_p->recv_down_y_idx, mpi_p->y_num);
    cuda_malloc_int(&mpi_p->crecv_down_z_idx, mpi_p->recv_down_z_idx, mpi_p->z_num);
    cuda_malloc_int(&mpi_p->crecv_up_x_idx, mpi_p->recv_up_x_idx, mpi_p->x_num);
    cuda_malloc_int(&mpi_p->crecv_up_y_idx, mpi_p->recv_up_y_idx, mpi_p->y_num);
    cuda_malloc_int(&mpi_p->crecv_up_z_idx, mpi_p->recv_up_z_idx, mpi_p->z_num);
    cuda_malloc_int(&mpi_p->csend_down_x_idx, mpi_p->send_down_x_idx, mpi_p->x_num);
    cuda_malloc_int(&mpi_p->csend_down_y_idx, mpi_p->send_down_y_idx, mpi_p->y_num);
    cuda_malloc_int(&mpi_p->csend_down_z_idx, mpi_p->send_down_z_idx, mpi_p->z_num);
    cuda_malloc_int(&mpi_p->csend_up_x_idx, mpi_p->send_up_x_idx, mpi_p->x_num);
    cuda_malloc_int(&mpi_p->csend_up_y_idx, mpi_p->send_up_y_idx, mpi_p->y_num);
    cuda_malloc_int(&mpi_p->csend_up_z_idx, mpi_p->send_up_z_idx, mpi_p->z_num);
    cuda_malloc(&mpi_p->csb3d, mpi_p->sb3d, mpi_p->buff_size);
    cuda_malloc(&mpi_p->crb3d, mpi_p->rb3d, mpi_p->buff_size);
    return 0;
}

int mpi_send_down_x_cuda(mpi_data* mpi_p, cuda_field* fy_p, cuda_field* fz_p)
{
    int count = 2 * mpi_p->x_num;
    int offset = mpi_p->x_num;
    
    dim3 block(BLOCK_SIZE);
    dim3 grid((offset + BLOCK_SIZE - 1)/BLOCK_SIZE);
    copy_send<<<grid, block>>>(fy_p->d_p,
			       fz_p->d_p,
			       mpi_p->csend_down_x_idx,
			       mpi_p->csb3d,
			       offset);
    
    cuda_memcpy_d2h(mpi_p->sb3d, mpi_p->csb3d, count);

    MPI_Send((void*)(mpi_p->sb3d),
	     count,
	     MPI_FP,
	     mpi_p->down_x,
	     0,
	     MPI_COMM_WORLD);
    return 0; 
}

int mpi_recv_down_x_cuda(mpi_data* mpi_p, cuda_field* fy_p, cuda_field* fz_p)
{
    int count = 2 * mpi_p->x_num;
    int offset = mpi_p->x_num;
    
    MPI_Recv((void*)(mpi_p->rb3d),
	     count,
	     MPI_FP,
	     mpi_p->up_x,
	     0,
	     MPI_COMM_WORLD,
	     MPI_STATUS_IGNORE);

    cuda_memcpy_h2d(mpi_p->crb3d, mpi_p->rb3d, count);
    
    dim3 block(BLOCK_SIZE);
    dim3 grid((offset + BLOCK_SIZE - 1)/BLOCK_SIZE);
    copy_recv<<<grid, block>>>(fy_p->d_p,
			       fz_p->d_p,
			       mpi_p->crecv_down_x_idx,
			       mpi_p->crb3d,
			       offset);

    return 0; 
}

int mpi_send_down_y_cuda(mpi_data* mpi_p, cuda_field* fx_p, cuda_field* fz_p)
{
    int count = 2 * mpi_p->y_num;
    int offset = mpi_p->y_num;
    
    dim3 block(BLOCK_SIZE);
    dim3 grid((offset + BLOCK_SIZE - 1)/BLOCK_SIZE);
    copy_send<<<grid, block>>>(fx_p->d_p,
			       fz_p->d_p,
			       mpi_p->csend_down_y_idx,
			       mpi_p->csb3d,
			       offset);
    
    cuda_memcpy_d2h(mpi_p->sb3d, mpi_p->csb3d, count);
    
    MPI_Send((void*)(mpi_p->sb3d),
	     count,
	     MPI_FP,
	     mpi_p->down_y,
	     0,
	     MPI_COMM_WORLD);
    return 0; 
}

int mpi_recv_down_y_cuda(mpi_data* mpi_p, cuda_field* fx_p, cuda_field* fz_p)
{
    int count = 2 * mpi_p->y_num;
    int offset = mpi_p->y_num;

    MPI_Recv((void*)(mpi_p->rb3d),
	     count,
	     MPI_FP,
	     mpi_p->up_y,
	     0,
	     MPI_COMM_WORLD,
	     MPI_STATUS_IGNORE);

    cuda_memcpy_h2d(mpi_p->crb3d, mpi_p->rb3d, count);
    dim3 block(BLOCK_SIZE);
    dim3 grid((offset + BLOCK_SIZE - 1)/BLOCK_SIZE);
    copy_recv<<<grid, block>>>(fx_p->d_p,
			       fz_p->d_p,
			       mpi_p->crecv_down_y_idx,
			       mpi_p->crb3d,
			       offset);

    return 0; 
}

int mpi_send_down_z_cuda(mpi_data* mpi_p, cuda_field* fx_p, cuda_field* fy_p)
{
    int count = 2 * mpi_p->z_num;
    int offset = mpi_p->z_num;
    
    dim3 block(BLOCK_SIZE);
    dim3 grid((offset + BLOCK_SIZE - 1)/BLOCK_SIZE);
    copy_send<<<grid, block>>>(fx_p->d_p,
			       fy_p->d_p,
			       mpi_p->csend_down_z_idx,
			       mpi_p->csb3d,
			       offset);
    
    cuda_memcpy_d2h(mpi_p->sb3d, mpi_p->csb3d, count);

    MPI_Send((void*)(mpi_p->sb3d),
	     count,
	     MPI_FP,
	     mpi_p->down_z,
	     0,
	     MPI_COMM_WORLD);
    return 0; 
}

int mpi_recv_down_z_cuda(mpi_data* mpi_p, cuda_field* fx_p, cuda_field* fy_p)
{
    int count = 2 * mpi_p->z_num;
    int offset = mpi_p->z_num;
    
    MPI_Recv((void*)(mpi_p->rb3d),
	     count,
	     MPI_FP,
	     mpi_p->up_z,
	     0,
	     MPI_COMM_WORLD,
	     MPI_STATUS_IGNORE);

    cuda_memcpy_h2d(mpi_p->crb3d, mpi_p->rb3d, count);

    dim3 block(BLOCK_SIZE);
    dim3 grid((offset + BLOCK_SIZE - 1)/BLOCK_SIZE);
    copy_recv<<<grid, block>>>(fx_p->d_p,
			       fy_p->d_p,
			       mpi_p->crecv_down_z_idx,
			       mpi_p->crb3d,
			       offset);
    return 0; 
}

int mpi_send_up_x_cuda(mpi_data* mpi_p, cuda_field* fy_p, cuda_field* fz_p)
{
    int count = 2 * mpi_p->x_num;
    int offset = mpi_p->x_num;

    dim3 block(BLOCK_SIZE);
    dim3 grid((offset + BLOCK_SIZE - 1)/BLOCK_SIZE);
    copy_send<<<grid, block>>>(fy_p->d_p,
			       fz_p->d_p,
			       mpi_p->csend_up_x_idx,
			       mpi_p->csb3d,
			       offset);
    
    cuda_memcpy_d2h(mpi_p->sb3d, mpi_p->csb3d, count);

    MPI_Send((void*)(mpi_p->sb3d),
	     count,
	     MPI_FP,
	     mpi_p->up_x,
	     0,
	     MPI_COMM_WORLD);
    return 0;
}

int mpi_recv_up_x_cuda(mpi_data* mpi_p, cuda_field* fy_p, cuda_field* fz_p)
{
    int count = 2 * mpi_p->x_num;
    int offset = mpi_p->x_num;
    
    MPI_Recv((void*)(mpi_p->rb3d),
	     count,
	     MPI_FP,
	     mpi_p->down_x,
	     0,
	     MPI_COMM_WORLD,
	     MPI_STATUS_IGNORE);

    cuda_memcpy_h2d(mpi_p->crb3d, mpi_p->rb3d, count);

    dim3 block(BLOCK_SIZE);
    dim3 grid((offset + BLOCK_SIZE - 1)/BLOCK_SIZE);
    copy_recv<<<grid, block>>>(fy_p->d_p,
			       fz_p->d_p,
			       mpi_p->crecv_up_x_idx,
			       mpi_p->crb3d,
			       offset);

    return 0;
}

int mpi_send_up_y_cuda(mpi_data* mpi_p, cuda_field* fx_p, cuda_field* fz_p)
{
    int count = 2 * mpi_p->y_num;
    int offset = mpi_p->y_num;
    
    dim3 block(BLOCK_SIZE);
    dim3 grid((offset + BLOCK_SIZE - 1)/BLOCK_SIZE);
    copy_send<<<grid, block>>>(fx_p->d_p,
			       fz_p->d_p,
			       mpi_p->csend_up_y_idx,
			       mpi_p->csb3d,
			       offset);
    
    cuda_memcpy_d2h(mpi_p->sb3d, mpi_p->csb3d, count);
 
    MPI_Send((void*)(mpi_p->sb3d),
	     count,
	     MPI_FP,
	     mpi_p->up_y,
	     0,
	     MPI_COMM_WORLD);
    return 0;
}

int mpi_recv_up_y_cuda(mpi_data* mpi_p, cuda_field* fx_p, cuda_field* fz_p)
{
    int count = 2 * mpi_p->y_num;
    int offset = mpi_p->y_num;
    
    MPI_Recv((void*)(mpi_p->rb3d),
	     count,
	     MPI_FP,
	     mpi_p->down_y,
	     0,
	     MPI_COMM_WORLD,
	     MPI_STATUS_IGNORE);

    cuda_memcpy_h2d(mpi_p->crb3d, mpi_p->rb3d, count);

    dim3 block(BLOCK_SIZE);
    dim3 grid((offset + BLOCK_SIZE - 1)/BLOCK_SIZE);
    copy_recv<<<grid, block>>>(fx_p->d_p,
			       fz_p->d_p,
			       mpi_p->crecv_up_y_idx,
			       mpi_p->crb3d,
			       offset);
    return 0;
}

int mpi_send_up_z_cuda(mpi_data* mpi_p, cuda_field* fx_p, cuda_field* fy_p)
{
    int count = 2 * mpi_p->z_num;
    int offset = mpi_p->z_num;
    
    dim3 block(BLOCK_SIZE);
    dim3 grid((offset + BLOCK_SIZE - 1)/BLOCK_SIZE);
    copy_send<<<grid, block>>>(fx_p->d_p,
			       fy_p->d_p,
			       mpi_p->csend_up_z_idx,
			       mpi_p->csb3d,
			       offset);
    
    cuda_memcpy_d2h(mpi_p->sb3d, mpi_p->csb3d, count);

    MPI_Send((void*)(mpi_p->sb3d),
	     count,
	     MPI_FP,
	     mpi_p->up_z,
	     0,
	     MPI_COMM_WORLD);
    return 0;
}

int mpi_recv_up_z_cuda(mpi_data* mpi_p, cuda_field* fx_p, cuda_field* fy_p)
{
    int count = 2 * mpi_p->z_num;
    int offset = mpi_p->z_num;

    MPI_Recv((void*)(mpi_p->rb3d),
	     count,
	     MPI_FP,
	     mpi_p->down_z,
	     0,
	     MPI_COMM_WORLD,
	     MPI_STATUS_IGNORE);
    
    cuda_memcpy_h2d(mpi_p->crb3d, mpi_p->rb3d, count);

    dim3 block(BLOCK_SIZE);
    dim3 grid((offset + BLOCK_SIZE - 1)/BLOCK_SIZE);
    copy_recv<<<grid, block>>>(fx_p->d_p,
			       fy_p->d_p,
			       mpi_p->crecv_up_z_idx,
			       mpi_p->crb3d,
			       offset);
    return 0;
}

__global__ void copy_send(FP* f1_p, FP* f2_p, int* idx_p, FP* buffer, int num)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;

    if (i < num)
    {
	int idx = idx_p[i];
	buffer[i] = f1_p[idx];
	buffer[i+num] = f2_p[idx];
    }
}

__global__ void copy_recv(FP* f1_p, FP* f2_p, int* idx_p, FP* buffer, int num)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;

    if (i < num)
    {
	int idx = idx_p[i];
	f1_p[idx] = buffer[i];
	f2_p[idx] = buffer[i+num];
    }
}

int  mpi_exch_up3d_x_2comp_cuda(mpi_data* mpi_p, cuda_field* fy_p, cuda_field* fz_p)
{
    if (mpi_p->num_proc_x == 1)
    {
        return 0;
    }

    if (mpi_p->i%2 == 0)
    {
	mpi_send_up_x_cuda(mpi_p, fy_p, fz_p);
        if (mpi_p->i != 0)
        {
	    mpi_recv_up_x_cuda(mpi_p, fy_p, fz_p);
        }
    }
    else
    {
	mpi_recv_up_x_cuda(mpi_p, fy_p, fz_p);
        if (mpi_p->i != mpi_p->num_proc_x-1)
        {
	    mpi_send_up_x_cuda(mpi_p, fy_p, fz_p);
	}
    }
    return 0;
}

int mpi_exch_up3d_y_2comp_cuda(mpi_data* mpi_p, cuda_field* fx_p, cuda_field* fz_p)
{
    if (mpi_p->num_proc_y == 1)
    {
        return 0;
    }
    
    if (mpi_p->j%2 == 0)
    {
	mpi_send_up_y_cuda(mpi_p, fx_p, fz_p);
        if (mpi_p->j != 0)
        {
	    mpi_recv_up_y_cuda(mpi_p, fx_p, fz_p);
        }
    }
    else
    {
	mpi_recv_up_y_cuda(mpi_p, fx_p, fz_p);
        if (mpi_p->j != mpi_p->num_proc_y-1)
        {
	    mpi_send_up_y_cuda(mpi_p, fx_p, fz_p);
        }
    }
    return 0;
}

int mpi_exch_up3d_z_2comp_cuda(mpi_data* mpi_p, cuda_field* fx_p, cuda_field* fy_p)
{
    if (mpi_p->num_proc_z == 1)
    {
        return 0;
    }

    if (mpi_p->k %2 == 0)
    {
	mpi_send_up_z_cuda(mpi_p, fx_p, fy_p);
        if (mpi_p->k != 0)
        {
	    mpi_recv_up_z_cuda(mpi_p, fx_p, fy_p);
	}
    }
    else
    {
	mpi_recv_up_z_cuda(mpi_p, fx_p, fy_p);
        if (mpi_p->k != mpi_p->num_proc_z-1)
        {
	    mpi_send_up_z_cuda(mpi_p, fx_p, fy_p);
        }
    }
    return 0;
}

int mpi_exch_down3d_x_2comp_cuda(mpi_data* mpi_p, cuda_field* fy_p, cuda_field* fz_p)
{
    if (mpi_p->num_proc_x == 1)
    {
        return 0;
    }
    if (mpi_p->i%2 == 0)
    {
	mpi_recv_down_x_cuda(mpi_p, fy_p, fz_p);

        if (mpi_p->i != 0)
        {
	    mpi_send_down_x_cuda(mpi_p, fy_p, fz_p);
        }
    }
    else
    {
	mpi_send_down_x_cuda(mpi_p, fy_p, fz_p);

        if (mpi_p->i != mpi_p->num_proc_x-1)
        {
	    mpi_recv_down_x_cuda(mpi_p, fy_p, fz_p);
	}
    }
    return 0;
}

int mpi_exch_down3d_y_2comp_cuda(mpi_data* mpi_p, cuda_field* fx_p, cuda_field* fz_p)
{
    if (mpi_p->num_proc_y == 1)
    {
        return 0;
    }

    if (mpi_p->j%2 == 0)
    {
	mpi_recv_down_y_cuda(mpi_p, fx_p, fz_p);

        if (mpi_p->j != 0)
        {
	    mpi_send_down_y_cuda(mpi_p, fx_p, fz_p);
        }
    }
    else
    {
	mpi_send_down_y_cuda(mpi_p, fx_p, fz_p);

        if (mpi_p->j != mpi_p->num_proc_y-1)
        {
	    mpi_recv_down_y_cuda(mpi_p, fx_p, fz_p);
        }
    }
    return 0;
}

int mpi_exch_down3d_z_2comp_cuda(mpi_data* mpi_p, cuda_field* fx_p, cuda_field* fy_p)
{
    if (mpi_p->num_proc_z == 1)
    {
        return 0;
    }

    if (mpi_p->k%2 == 0)
    {
	mpi_recv_down_z_cuda(mpi_p, fx_p, fy_p);
        if (mpi_p->k != 0)
        {
	    mpi_send_down_z_cuda(mpi_p, fx_p, fy_p);
        }

    }
    else
    {
	mpi_send_down_z_cuda(mpi_p, fx_p, fy_p);
        if (mpi_p->k != mpi_p->num_proc_z-1)
        {
	    mpi_recv_down_z_cuda(mpi_p, fx_p, fy_p);
	}
    }
    return 0;
}
#endif
