/** @file fdtd_support.cpp
    Contains some supporting routines */
#include "fdtd.h"

decompose_data* init_decomposition(configTable* ct_p,
                                   geometry_data* g_p)
{
    decompose_data* d_p;
    (void)ct_p;

    if (NULL == (d_p = (decompose_data*)malloc(sizeof(decompose_data))))
    {
        fprintf(stderr, "%s:%d Could not allocate memory\n", __FILE__, __LINE__);
        exit(-1);
    }

    d_p->fx_imin = g_p->imin-1;
    d_p->fx_imax = g_p->imax;
    d_p->fx_jmin = g_p->jmin;
    d_p->fx_jmax = g_p->jmax;
    d_p->fx_kmin = g_p->kmin-1;
    d_p->fx_kmax = g_p->kmax;

    d_p->fy_imin = g_p->imin;
    d_p->fy_imax = g_p->imax+1;
    d_p->fy_jmin = g_p->jmin;
    d_p->fy_jmax = g_p->jmax;
    d_p->fy_kmin = g_p->kmin;
    d_p->fy_kmax = g_p->kmax+1;

    d_p->fz_imin = g_p->imin-1;
    d_p->fz_imax = g_p->imax;
    d_p->fz_jmin = g_p->jmin;
    d_p->fz_jmax = g_p->jmax;
    d_p->fz_kmin = g_p->kmin-1;
    d_p->fz_kmax = g_p->kmax;

    return d_p;
}

void update_decomposition(decompose_data* d_p,
                          geometry_data* g_p)
{
    d_p->fy_kmax = g_p->kmax+1;
    d_p->fx_kmax = g_p->kmax;
    d_p->fz_kmax = g_p->kmax;
    return;
}

void destroy_decomposition(decompose_data* d_p)
{
    free(d_p);
}

double* malloc_double(const char* file, int line, int elements)
{
    int size = sizeof(double) * elements;
    double* ans_p = (double*)malloc(size);

    if (NULL == ans_p)
    {
        fprintf(stderr, "%s:%d  Unable to allocate memory: %d bytes\n Exit!\n",
                file,
                line,
                size);
	fflush(stderr);
        exit(-1);
    }
    memset((void*)ans_p, 0, size);
    return ans_p;
}

void free_fp(FP* fp_p)
{
    if (NULL == fp_p)
    {
	return;
    }
    free(fp_p);
}

void free_int(int* fp_p)
{
    if (NULL == fp_p)
    {
	return;
    }
    free(fp_p);
}


float* malloc_float(const char* file, int line, int elements)
{
    if (elements == 0)
    {
	return NULL;
    }

    int size = sizeof(float) * elements;
    float* ans_p = (float*)malloc(size);

    if (NULL == ans_p)
    {
        fprintf(stderr, "%s:%d  Unable to allocate memory: %d bytes\n Exit!\n",
                file,
                line,
                size);
	fflush(stderr);
        exit(-1);
    }
    memset((void*)ans_p, 0, size);
    return ans_p;
}

int* malloc_int(const char* file, int line, int elements)
{
    if (elements == 0)
    {
	return NULL;
    }

    int size = sizeof(int) * elements;
    int* ans_p = (int*)malloc(size);

    if (NULL == ans_p)
    {
        fprintf(stderr, "%s:%d  Unable to allocate memory: %d bytes\n Exit!\n",
                file,
                line,
                size);
	fflush(stderr);
        exit(-1);
    }
    memset((void*)ans_p, 0, size);
    return ans_p;
}

/**  Function is used to initialise 3d point(x,y,z)
 *   \author  Evgeny Efimenko
 *   \param   point_p - pointer to point structure
 *   \param   x - x coordinate of the point
 *   \param   y - y coordinate of the point
 *   \param   z - z coordinate of the point
 *   \return  void
 *   \callgraph
 *   \callergraph
 */
void init_point(point* point_p, FP x, FP y, FP z)
{
    point_p->x = x;
    point_p->y = y;
    point_p->z = z;
    return;
}

/**  Function is used to initialise all related to time parameters:
 *   time of calculation, time step, frequency for normalization,
 *   t_start - initial moment of time(for restart)
 *   t_save - moment of time to save data(for restart)
 *   \author  Evgeny Efimenko
 *   \param   ct_p - pointer to configuration structure
 *   \param   time_scheme_p - pointer to timescheme structure
 *   \return  void
 *   \callgraph
 *   \callergraph
 */

void init_time_scheme(configTable* ct_p, time_scheme* time_scheme_p)
{
    time_scheme_p->t_move = ct_p->getIntValue("t_move", 25);
    time_scheme_p->v_move = ct_p->getDoubleValue("v_move", 1.);
    time_scheme_p->t_max = ct_p->getIntValue("t_max", 1000);
    time_scheme_p->delta_t = ct_p->getDoubleValue("delta_t", 0.005);
    time_scheme_p->lambda = ct_p->getDoubleValue("m_wl", 800)*1e-3;/* m_wl in nm */
    time_scheme_p->freq = 2*M_PI/time_scheme_p->lambda;
    time_scheme_p->t_start   = ct_p->getIntValue("t_start", 0);
    time_scheme_p->t_save    = ct_p->getIntValue("t_save", 0);

    if (time_scheme_p->t_save <= 0)
    {
        time_scheme_p->save = false;
    }
    else
    {
        time_scheme_p->save = true;
    }
}

/**  Function is used to save results(for restart):
 *   1. Checks if restart is used and if t is a correct time
 *   2. Dumps D, H, J. E is not dumped, because it can be calculated from D.
 *   3. If ionization is on, all ionization fields (n, nm, absorption) are dumped
 *   4. All pml related data are dumped to disk.
 *   \author  Evgeny Efimenko
 *   \param t - current moment of time
 *   \param time_scheme_p - pointer to timescheme structure
 *   \param fd_p - pointer to 3 component D field
 *   \param fe_p - pointer to 3 component E field
 *   \param fj_p - pointer to 3 component J field
 *   \param fh_p - pointer to 3 component H field
 *   \param pml_p - pointer to pml structure
 *   \param media_p - pointer to media structure
 *   \param pathPrefix - path prefix, where to save data
 *   \return void
 *   \callgraph
 *   \callergraph
 *   \todo check if all nessessary data are stored!
 */

void save_dump(int t,
	       time_scheme* time_scheme_p,
	       vfield_n* fd_p,
	       vfield_n* fj_p,
	       vfield_n* fh_p,
	       pml_data* pml_p,
	       media_data* media_p,
	       const char* pathPrefix)
{
#ifndef CUDA
    if (time_scheme_p->save == true && t == time_scheme_p->t_save)
    {
        dump_vfield(fd_p, "D", pathPrefix, t);
        dump_vfield(fj_p, "J", pathPrefix, t);
        dump_vfield(fh_p, "H", pathPrefix, t);

	/* PML */
        dump_field(pml_p->pml_f.Exy_y_l, "E_XY_Y_L", pathPrefix, t);
        dump_field(pml_p->pml_f.Exy_y_h, "E_XY_Y_H", pathPrefix, t);
        dump_field(pml_p->pml_f.Exy_z_l, "E_XY_Z_L", pathPrefix, t);
        dump_field(pml_p->pml_f.Exy_z_h, "E_XY_Z_H", pathPrefix, t);
        dump_field(pml_p->pml_f.Exz_y_l, "E_XZ_Y_L", pathPrefix, t);
        dump_field(pml_p->pml_f.Exz_y_h, "E_XZ_Y_H", pathPrefix, t);
        dump_field(pml_p->pml_f.Exz_z_l, "E_XZ_Z_L", pathPrefix, t);
        dump_field(pml_p->pml_f.Exz_z_h, "E_XZ_Z_H", pathPrefix, t);
        dump_field(pml_p->pml_f.Eyx_x_l, "E_YX_X_L", pathPrefix, t);
        dump_field(pml_p->pml_f.Eyx_x_h, "E_YX_X_H", pathPrefix, t);
        dump_field(pml_p->pml_f.Eyx_z_l, "E_YX_Z_L", pathPrefix, t);
        dump_field(pml_p->pml_f.Eyx_z_h, "E_YX_Z_H", pathPrefix, t);
        dump_field(pml_p->pml_f.Eyz_x_l, "E_YZ_X_L", pathPrefix, t);
        dump_field(pml_p->pml_f.Eyz_x_h, "E_YZ_X_H", pathPrefix, t);
        dump_field(pml_p->pml_f.Eyz_z_l, "E_YZ_Z_L", pathPrefix, t);
        dump_field(pml_p->pml_f.Eyz_z_h, "E_YZ_Z_H", pathPrefix, t);
        dump_field(pml_p->pml_f.Ezy_y_l, "E_ZY_Y_L", pathPrefix, t);
        dump_field(pml_p->pml_f.Ezy_y_h, "E_ZY_Y_H", pathPrefix, t);
        dump_field(pml_p->pml_f.Ezy_x_l, "E_ZY_X_L", pathPrefix, t);
        dump_field(pml_p->pml_f.Ezy_x_h, "E_ZY_X_H", pathPrefix, t);
        dump_field(pml_p->pml_f.Ezx_y_l, "E_ZX_Y_L", pathPrefix, t);
        dump_field(pml_p->pml_f.Ezx_y_h, "E_ZX_Y_H", pathPrefix, t);
        dump_field(pml_p->pml_f.Ezx_x_l, "E_ZX_X_L", pathPrefix, t);
        dump_field(pml_p->pml_f.Ezx_x_h, "E_ZX_X_H", pathPrefix, t);

        dump_field(pml_p->pml_f.Hxy_y_l, "H_XY_Y_L", pathPrefix, t);
        dump_field(pml_p->pml_f.Hxy_y_h, "H_XY_Y_H", pathPrefix, t);
        dump_field(pml_p->pml_f.Hxy_z_l, "H_XY_Z_L", pathPrefix, t);
        dump_field(pml_p->pml_f.Hxy_z_h, "H_XY_Z_H", pathPrefix, t);
        dump_field(pml_p->pml_f.Hxz_y_l, "H_XZ_Y_L", pathPrefix, t);
        dump_field(pml_p->pml_f.Hxz_y_h, "H_XZ_Y_H", pathPrefix, t);
        dump_field(pml_p->pml_f.Hxz_z_l, "H_XZ_Z_L", pathPrefix, t);
        dump_field(pml_p->pml_f.Hxz_z_h, "H_XZ_Z_H", pathPrefix, t);
        dump_field(pml_p->pml_f.Hyx_x_l, "H_YX_X_L", pathPrefix, t);
        dump_field(pml_p->pml_f.Hyx_x_h, "H_YX_X_H", pathPrefix, t);
        dump_field(pml_p->pml_f.Hyx_z_l, "H_YX_Z_L", pathPrefix, t);
        dump_field(pml_p->pml_f.Hyx_z_h, "H_YX_Z_H", pathPrefix, t);
        dump_field(pml_p->pml_f.Hyz_x_l, "H_YZ_X_L", pathPrefix, t);
        dump_field(pml_p->pml_f.Hyz_x_h, "H_YZ_X_H", pathPrefix, t);
        dump_field(pml_p->pml_f.Hyz_z_l, "H_YZ_Z_L", pathPrefix, t);
        dump_field(pml_p->pml_f.Hyz_z_h, "H_YZ_Z_H", pathPrefix, t);
        dump_field(pml_p->pml_f.Hzy_y_l, "H_ZY_Y_L", pathPrefix, t);
        dump_field(pml_p->pml_f.Hzy_y_h, "H_ZY_Y_H", pathPrefix, t);
        dump_field(pml_p->pml_f.Hzy_x_l, "H_ZY_X_L", pathPrefix, t);
        dump_field(pml_p->pml_f.Hzy_x_h, "H_ZY_X_H", pathPrefix, t);
        dump_field(pml_p->pml_f.Hzx_y_l, "H_ZX_Y_L", pathPrefix, t);
        dump_field(pml_p->pml_f.Hzx_y_h, "H_ZX_Y_H", pathPrefix, t);
        dump_field(pml_p->pml_f.Hzx_x_l, "H_ZX_X_L", pathPrefix, t);
        dump_field(pml_p->pml_f.Hzx_x_h, "H_ZX_X_H", pathPrefix, t);
    }
#endif
    return;
}

/**  Function is used to restore results during restart:
 *   1. Checks if restart is used
 *   2. Restores D, H, J. E is not restored now, because it can be calculated from D.
 *   3. If ionization is on, all ionization fields (n, nm, absorption) are restored
 *   4. All pml related data are restored.
 *   \author  Evgeny Efimenko
 *   \param time_scheme_p - pointer to timescheme structure
 *   \param fd_p - pointer to 3 component D field
 *   \param fe_p - pointer to 3 component E field
 *   \param fj_p - pointer to 3 component J field
 *   \param fh_p - pointer to 3 component H field
 *   \param pml_p - pointer to pml structure
 *   \param media_p - pointer to media structure
 *   \param pathPrefix - path prefix, where to seek for data
 *   \return void
 *   \callgraph
 *   \callergraph
 *   \todo check if all nessessary data are restored!
 */

void read_dump(time_scheme* time_scheme_p,
	       vfield_n* fd_p,
	       vfield_n* fj_p,
	       vfield_n* fh_p,
	       pml_data* pml_p,
	       media_data* media_p,
	       const char* pathPrefix)
{
#ifndef CUDA
    if (time_scheme_p->t_start > 0)
    {
        int t = time_scheme_p->t_start;

        read_vfield(fd_p, "D", pathPrefix, t);
        read_vfield(fj_p, "J", pathPrefix, t);
        read_vfield(fh_p, "H", pathPrefix, t);

        /* PML */
        read_field(pml_p->pml_f.Exy_y_l, "E_XY_Y_L", pathPrefix, t);
        read_field(pml_p->pml_f.Exy_y_h, "E_XY_Y_H", pathPrefix, t);
        read_field(pml_p->pml_f.Exy_z_l, "E_XY_Z_L", pathPrefix, t);
        read_field(pml_p->pml_f.Exy_z_h, "E_XY_Z_H", pathPrefix, t);
        read_field(pml_p->pml_f.Exz_y_l, "E_XZ_Y_L", pathPrefix, t);
        read_field(pml_p->pml_f.Exz_y_h, "E_XZ_Y_H", pathPrefix, t);
        read_field(pml_p->pml_f.Exz_z_l, "E_XZ_Z_L", pathPrefix, t);
        read_field(pml_p->pml_f.Exz_z_h, "E_XZ_Z_H", pathPrefix, t);
        read_field(pml_p->pml_f.Eyx_x_l, "E_YX_X_L", pathPrefix, t);
        read_field(pml_p->pml_f.Eyx_x_h, "E_YX_X_H", pathPrefix, t);
        read_field(pml_p->pml_f.Eyx_z_l, "E_YX_Z_L", pathPrefix, t);
        read_field(pml_p->pml_f.Eyx_z_h, "E_YX_Z_H", pathPrefix, t);
        read_field(pml_p->pml_f.Eyz_x_l, "E_YZ_X_L", pathPrefix, t);
        read_field(pml_p->pml_f.Eyz_x_h, "E_YZ_X_H", pathPrefix, t);
        read_field(pml_p->pml_f.Eyz_z_l, "E_YZ_Z_L", pathPrefix, t);
        read_field(pml_p->pml_f.Eyz_z_h, "E_YZ_Z_H", pathPrefix, t);
        read_field(pml_p->pml_f.Ezy_y_l, "E_ZY_Y_L", pathPrefix, t);
        read_field(pml_p->pml_f.Ezy_y_h, "E_ZY_Y_H", pathPrefix, t);
        read_field(pml_p->pml_f.Ezy_x_l, "E_ZY_X_L", pathPrefix, t);
        read_field(pml_p->pml_f.Ezy_x_h, "E_ZY_X_H", pathPrefix, t);
        read_field(pml_p->pml_f.Ezx_y_l, "E_ZX_Y_L", pathPrefix, t);
        read_field(pml_p->pml_f.Ezx_y_h, "E_ZX_Y_H", pathPrefix, t);
        read_field(pml_p->pml_f.Ezx_x_l, "E_ZX_X_L", pathPrefix, t);
        read_field(pml_p->pml_f.Ezx_x_h, "E_ZX_X_H", pathPrefix, t);

        read_field(pml_p->pml_f.Hxy_y_l, "H_XY_Y_L", pathPrefix, t);
        read_field(pml_p->pml_f.Hxy_y_h, "H_XY_Y_H", pathPrefix, t);
        read_field(pml_p->pml_f.Hxy_z_l, "H_XY_Z_L", pathPrefix, t);
        read_field(pml_p->pml_f.Hxy_z_h, "H_XY_Z_H", pathPrefix, t);
        read_field(pml_p->pml_f.Hxz_y_l, "H_XZ_Y_L", pathPrefix, t);
        read_field(pml_p->pml_f.Hxz_y_h, "H_XZ_Y_H", pathPrefix, t);
        read_field(pml_p->pml_f.Hxz_z_l, "H_XZ_Z_L", pathPrefix, t);
        read_field(pml_p->pml_f.Hxz_z_h, "H_XZ_Z_H", pathPrefix, t);
        read_field(pml_p->pml_f.Hyx_x_l, "H_YX_X_L", pathPrefix, t);
        read_field(pml_p->pml_f.Hyx_x_h, "H_YX_X_H", pathPrefix, t);
        read_field(pml_p->pml_f.Hyx_z_l, "H_YX_Z_L", pathPrefix, t);
        read_field(pml_p->pml_f.Hyx_z_h, "H_YX_Z_H", pathPrefix, t);
        read_field(pml_p->pml_f.Hyz_x_l, "H_YZ_X_L", pathPrefix, t);
        read_field(pml_p->pml_f.Hyz_x_h, "H_YZ_X_H", pathPrefix, t);
        read_field(pml_p->pml_f.Hyz_z_l, "H_YZ_Z_L", pathPrefix, t);
        read_field(pml_p->pml_f.Hyz_z_h, "H_YZ_Z_H", pathPrefix, t);
        read_field(pml_p->pml_f.Hzy_y_l, "H_ZY_Y_L", pathPrefix, t);
        read_field(pml_p->pml_f.Hzy_y_h, "H_ZY_Y_H", pathPrefix, t);
        read_field(pml_p->pml_f.Hzy_x_l, "H_ZY_X_L", pathPrefix, t);
        read_field(pml_p->pml_f.Hzy_x_h, "H_ZY_X_H", pathPrefix, t);
        read_field(pml_p->pml_f.Hzx_y_l, "H_ZX_Y_L", pathPrefix, t);
        read_field(pml_p->pml_f.Hzx_y_h, "H_ZX_Y_H", pathPrefix, t);
        read_field(pml_p->pml_f.Hzx_x_l, "H_ZX_X_L", pathPrefix, t);
        read_field(pml_p->pml_f.Hzx_x_h, "H_ZX_X_H", pathPrefix, t);
    }
#endif
    return;
}

/**  Function is used to initialise data for point data saving in 3d case
 *   \author  Evgeny Efimenko
 *   \param ct_p - pointer to configuration structure,
 *   \param point_data_p - pointer to point_data structure
 *   \param path_prefix - path prefix where to save data
 *   \param timescheme_p - pointer to timescheme structure
 *   \param geometry_p - pointer to geometry structure
 *   \param ve_p - pointer to 3 component E field
 *   \param vh_p - pointer to 3 component H field
 *   \param n_p - pointer to electron density
 *   \return  void
 *   \callgraph
 *   \callergraph
 */
void init_point_data_3d(configTable* ct_p,
                        points_data_3d* pnt_p,
                        const char* pathPrefix,
                        time_scheme* t_p,
                        geometry_data* g_p,
                        vfield_n* ve_p,
                        vfield_n* vh_p)
{
    pnt_p->num_points = ct_p->getIntValue("num_points", 0);

    if (0 < pnt_p->num_points)
    {
        if (g_p->moving == 2)
        {
            pnt_p->t_max = (int) ((g_p->delta_z*g_p->z_max)/ t_p->delta_t) +
                                  (int)(1.5*t_p->t0);
        }
        else
        {
            pnt_p->t_max = t_p->t_max+1;
        }

        pnt_p->delta_t = t_p->delta_t;

        int *x, *y, *z;
        int *index;
        int count = 0;

        x = INT_MALLOC(pnt_p->num_points);
        y = INT_MALLOC(pnt_p->num_points);
	z = INT_MALLOC(pnt_p->num_points);
	index = INT_MALLOC(pnt_p->num_points);

        for (int i = 0; i<pnt_p->num_points; i++)
        {
            char temp[100];

            sprintf(temp,"xpos_%d",i);
            int temp_x_pos = ct_p->getIntValue(temp, g_p->x_full/2);
            if (temp_x_pos > g_p->x_full - 2)
            {
                RANK_S(0);
                fprintf(stderr,
                        "WARNING! [point: %3d] x_pos = %d exceeds allowed x_max = %ud, reduced to %ud\n",
                        i, temp_x_pos, g_p->x_full - 2, g_p->x_full - 10);
                RANK_F(0);
                temp_x_pos = g_p->x_full - 10;
            }
	    x[i] = temp_x_pos;

            sprintf(temp,"ypos_%d",i);
            int temp_y_pos = ct_p->getIntValue(temp, g_p->y_full/2);
            if (temp_y_pos > g_p->y_full - 2)
            {
                RANK_S(0);
                fprintf(stderr,
                        "WARNING! [point: %3d] y_pos = %d exceeds allowed y_max = %ud, reduced to %ud\n",
                        i, temp_y_pos, g_p->y_full - 2, g_p->y_full - 10);
                RANK_F(0);
                temp_y_pos = g_p->y_full - 10;
            }
	    y[i] = temp_y_pos;

            sprintf(temp,"zpos_%d",i);
            int temp_z_pos = ct_p->getIntValue(temp, g_p->z_full/2);

            if (temp_z_pos > g_p->z_full - 2 && g_p->moving != 2)
            {
                RANK_S(0);
                fprintf(stderr,
                        "WARNING! [point: %3d] z_pos = %d exceeds allowed z_max = %ud, reduced to %ud\n",
                        i, temp_z_pos, g_p->z_full - 2, g_p->z_full - 10);
                RANK_F(0);
                temp_z_pos = g_p->z_max - 10;
            }
	    z[i] = temp_z_pos;

            if (g_p->moving != 2)
            {
                if (g_p->i_offset < x[i] &&
		   (g_p->i_offset + g_p->x_max-1) > x[i] &&
		    g_p->j_offset < y[i] &&
		   (g_p->j_offset + g_p->y_max-1) > y[i] &&
		    g_p->k_offset < z[i] &&
                   (g_p->k_offset + g_p->z_max-1) > z[i])
                {
                    index[count] = i;
                    count++;
                }
            }
            else
            {
                if (g_p->i_offset < x[i] &&
		   (g_p->i_offset + g_p->x_max-1) > x[i] &&
                    g_p->j_offset < y[i] &&
                   (g_p->j_offset + g_p->y_max-1) > y[i])
                {
                    index[count] = i;
                    count++;
                }
            }
        }

        pnt_p->num_points = count;

        if (count > 0)
        {
            if (NULL == (pnt_p->points_p = (one_point_data_3d*)malloc(sizeof(one_point_data_3d)*count)))
            {
                fprintf(stderr, "%s:%d Unable to allocate memory\n", __FILE__, __LINE__);
                exit(-1);
            }
            for (int i=0; i<count; i++)
            {
                init_one_point_3d(t_p->t_start,
                                  pnt_p,
                                  g_p,
                                  i,
                                  x[index[i]],
                                  y[index[i]],
                                  z[index[i]],
                                  ve_p,
                                  vh_p,
				  pathPrefix);
            }
#ifdef CUDA
	    init_point_data_3d_cuda(pnt_p);
#endif
        }

        free(x);
        free(y);
        free(z);
        free(index);
    }
}

/**  Function is used to initialise data for point data saving in 2d case
 *   \author  Evgeny Efimenko
 *   \param ct_p - pointer to configuration structure,
 *   \param pnt_p - pointer to point_data structure
 *   \param path_prefix - path prefix where to save data
 *   \param timescheme_p - pointer to timescheme structure
 *   \param geometry_p - pointer to geometry structure
 *   \param ve_p - pointer to 3 component E field
 *   \param vh_p - pointer to 3 component H field
 *   \param n_p - pointer to electron density
 *   \return  void
 *   \callgraph
 *   \callergraph
 */
void init_point_data_2d(configTable* ct_p,
                        points_data_2d* pnt_p,
                        const char* pathPrefix,
                        time_scheme* timescheme_p,
                        geometry_data* geometry_p,
                        field* e_p)
{
    pnt_p->num_points = ct_p->getIntValue("num_points", 0);
    if (0 < pnt_p->num_points)
    {
        if (geometry_p->moving == 2)
        {
            pnt_p->t_max = (int) ((geometry_p->delta_x*geometry_p->x_max)/
                                         timescheme_p->delta_t) +
                                  (int)(1.5*timescheme_p->t0);
        }
        else
        {
            pnt_p->t_max = timescheme_p->t_max+1;
        }
        pnt_p->delta_t = timescheme_p->delta_t;

        int *x_p, *z_p;
        int *index_p;
        int count = 0;

        if (NULL == (x_p = (int*)malloc(sizeof(int)*pnt_p->num_points)))
        {
            exit(-1);
        }

        if (NULL == (z_p = (int*)malloc(sizeof(int)*pnt_p->num_points)))
        {
            exit(-1);
        }

        if (NULL == (index_p = (int*)malloc(sizeof(int)*pnt_p->num_points)))
        {
            exit(-1);
        }

        for (int i = 0; i<pnt_p->num_points; i++)
        {
            char temp[100];

            sprintf(temp,"xpos_%d",i);
            int temp_x_pos = ct_p->getIntValue(temp, geometry_p->x_full/2);
            if (temp_x_pos > (int)geometry_p->x_full - 2 && geometry_p->moving != 2)
            {
                RANK_S(0);
                fprintf(stderr,
                        "WARNING! [point: %3d] x_pos = %d exceeds allowed x_max = %ud, reduced to %ud\n",
                        i,
                        temp_x_pos,
                        geometry_p->x_full - 2,
                        geometry_p->x_full - 10);
                RANK_F(0);
                x_p[i] = geometry_p->x_full - 10;
            }
            else
            {
                x_p[i] = temp_x_pos;
            }

            sprintf(temp,"zpos_%d",i);
            int temp_z_pos = ct_p->getIntValue(temp, geometry_p->z_full/2);
            if (temp_z_pos > (int)geometry_p->z_full - 2)
            {
                RANK_S(0);
                fprintf(stderr,
                        "WARNING! [point: %3d] z_pos = %d exceeds allowed z_max = %ud, reduced to %u d\n",
                        i,
                        temp_z_pos,
                        geometry_p->z_full - 2,
                        geometry_p->z_full - 10);
                RANK_F(0);
                z_p[i] = geometry_p->z_max - 10;
            }
            else
            {
                z_p[i] = temp_z_pos;
            }

            if (geometry_p->moving != 2)
            {
                if (geometry_p->i_offset < x_p[i] &&
                        (geometry_p->i_offset + geometry_p->x_max-1) > x_p[i] &&
                        geometry_p->k_offset < z_p[i] &&
                        (geometry_p->k_offset + geometry_p->z_max-1) > z_p[i])
                {
                    index_p[count] = i;
                    count++;
                }
            }
            else
            {
                if (geometry_p->k_offset < z_p[i] &&
                        (geometry_p->k_offset + geometry_p->z_max-1) > z_p[i])
                {
                    index_p[count] = i;
                    count++;
                }
            }
        }

        pnt_p->num_points = count;

        if (count > 0)
        {
            if (NULL == (pnt_p->points_p = (one_point_data_2d*)malloc(sizeof(one_point_data_2d)*count)))
            {
                fprintf(stderr, "%s:%d Unable to allocate memory\n", __FILE__, __LINE__);
                exit(-1);
            }
            for (int i=0; i<count; i++)
            {
                init_one_point_2d(timescheme_p->t_start,
                                  pnt_p,
                                  geometry_p,
                                  i,
                                  x_p[index_p[i]],
                                  z_p[index_p[i]],
                                  e_p,
                                  pathPrefix);
            }
        }

        free(x_p);
        free(z_p);
        free(index_p);
    }
}

/**  Function is used to initialise data for individual point for point data saving in 3d case
 *   \author  Evgeny Efimenko
 *   \param t - current moment of time
 *   \param pnt_p - pointer to point_data structure
 *   \param geometry_p - pointer to geometry structure
 *   \param i - number of point to initialise
 *   \param x - x coordinate of the point in grid
 *   \param y - y coordinate of the point in grid
 *   \param z - z coordinate of the point in grid
 *   \param ve_p - pointer to 3 component E field
 *   \param vh_p - pointer to 3 component H field
 *   \param n_p - pointer to electron density
 *   \return  void
 *   \callgraph
 *   \callergraph
 */
void init_one_point_3d(int t,
                       points_data_3d* pnt_p,
                       geometry_data* geometry_p,
                       int i,
                       int x,
                       int y,
                       int z,
                       vfield_n* ve_p,
                       vfield_n* vh_p,
		       const char* pathPrefix)
{
    one_point_data_3d* point_p = &pnt_p->points_p[i];
    point_p->x_pos = x-geometry_p->i_offset;
    point_p->y_pos = y-geometry_p->j_offset;
    point_p->z_pos = z-geometry_p->k_offset;
    point_p->t0 = 0;

    point_p->index = INDEX_P(ve_p->comp_p[x_e],point_p->x_pos,point_p->y_pos,point_p->z_pos);

    sprintf(point_p->file, "%s/point_t%d_%d_%d_%d.dat", pathPrefix, t, x, y, z);

    point_p->enable = true;
    point_p->t0 = 0;

    if (geometry_p->moving == 2)
    {
        if (point_p->z_pos <= 0 || point_p->z_pos >= geometry_p->z_max-1)
        {
            point_p->enable = false;
        }
        point_p->t0 = t;
    }

    if (point_p->enable == true)
    {
        if (NULL == (point_p->out = fopen(point_p->file, "w")))
        {
            fprintf(stderr, "Unable to open file for writing %s\n", point_p->file);
	    point_p->open = false;
        }
	else
	{
	    point_p->open = true;
	    fprintf(point_p->out, "# %d %lf\n", pnt_p->t_max, pnt_p->delta_t);
	    fflush(point_p->out);
	    fprintf(stderr, "Open %s for writing\n", point_p->file);
	}
    }
    return;
}

/**  Function is used to initialise data for individual point for point data saving in 2d case
 *   \author  Evgeny Efimenko
 *   \param t - current moment of time
 *   \param pnt_p - pointer to point_data structure
 *   \param geometry_p - pointer to geometry structure
 *   \param i - number of point to initialise
 *   \param x - x coordinate of the point in grid
 *   \param y - y coordinate of the point in grid
 *   \param z - z coordinate of the point in grid
 *   \param ve_p - pointer to 3 component E field
 *   \param vh_p - pointer to 3 component H field
 *   \param n_p - pointer to electron density
 *   \return  void
 *   \callgraph
 *   \callergraph
 */
void init_one_point_2d(int t,
                       points_data_2d* pnt_p,
                       geometry_data* geometry_p,
                       int i,
                       int x,
                       int z,
                       field* e_p,
                       const char* pathPrefix)
{
    one_point_data_2d* point_p = &pnt_p->points_p[i];
    point_p->x_pos = x-geometry_p->i_offset;
    point_p->z_pos = z-geometry_p->k_offset;
    point_p->t0 = 0;
    char file[1024];

    point_p->index = INDEX2D_P(e_p,point_p->x_pos,point_p->z_pos);

    sprintf(file, "%s/point_t%d_%d_%d.dat",pathPrefix,t,x,z);

    point_p->enable = true;
    point_p->t0 = 0;

    if (geometry_p->moving == 2)
    {
        if (point_p->x_pos <= 0 || point_p->x_pos >= geometry_p->x_max-1)
        {
            point_p->enable = false;
        }
        point_p->t0 = t;
    }

    point_p->out = fopen(file, "w");
    fprintf(stderr, "Open %s\n", file);
    if (NULL == point_p->out)
    {
        fprintf(stderr, "Unable to open file for writing %s\n", file);
        exit(-1);
    }
    point_p->open = true;
    fprintf(point_p->out, "# %d %lf\n", pnt_p->t_max, pnt_p->delta_t);
    fflush(point_p->out);
}

/**  Function is used to destroy all data for point data saving for 3d
 *   \author  Evgeny Efimenko
 *   \param pnt_p - pointer to point_data structure
 *   \return  void
 *   \callgraph
 *   \callergraph
 */
void destroy_point_data_3d(points_data_3d* pnt_p)
{
    if (0 != pnt_p->num_points)
    {
        for(int i=0; i<pnt_p->num_points; i++)
        {
            destroy_one_point_data_3d(pnt_p, i);
        }
        free(pnt_p->points_p);
    }
#ifdef CUDA
    destroy_point_data_3d_cuda(pnt_p);
#endif
}

/**  Function is used to destroy point data for point data saving for 3d
 *   \author  Evgeny Efimenko
 *   \param pnt_p - pointer to point_data structure
 *   \return  void
 *   \callgraph
 *   \callergraph
 */
void destroy_one_point_data_3d(points_data_3d* pnt_p, int i)
{
    one_point_data_3d* point_p = &pnt_p->points_p[i];
    if (point_p->enable == true && point_p->open == true)
    {
        fclose(point_p->out);
    }
}

/**  Function is used to destroy all data for point data saving for 2d
 *   \author  Evgeny Efimenko
 *   \param pnt_p - pointer to point_data structure
 *   \return  void
 *   \callgraph
 *   \callergraph
 */
void destroy_point_data_2d(points_data_2d* pnt_p)
{
    if (0 != pnt_p->num_points)
    {
        for(int i=0; i<pnt_p->num_points; i++)
        {
            destroy_one_point_data_2d(pnt_p, i);
        }
        free(pnt_p->points_p);
    }
}

/**  Function is used to destroy point data for point data saving for 3d
 *   \author  Evgeny Efimenko
 *   \param pnt_p - pointer to point_data structure
 *   \return  void
 *   \callgraph
 *   \callergraph
 */
void destroy_one_point_data_2d(points_data_2d* pnt_p, int i)
{
    one_point_data_2d* point_p = &pnt_p->points_p[i];
    if (point_p->open == true)
    {
        fclose(point_p->out);
    }
}

/**  Function is used to store point data for moment of time t for point data saving in 3d geometry,
 *   data are written to file
 *   \author  Evgeny Efimenko
 *   \param pnt_p - pointer to point_data structure
 *   \param geometry_p - pointer to geometry structure
 *   \param t - moment of time
 *   \param ve_p - pointer to 3 component E field
 *   \param vh_p - pointer to 3 component H field
 *   \param n_p - pointer to electron density
 *   \param abs_p - pointer to absorption array
 *   \return  void
 *   \callgraph
 *   \callergraph
 */
void store_point_data_3d(points_data_3d* pnt_p,
                         geometry_data* geometry_p,
                         int t,
                         vfield_n* ve_p,
                         vfield_n* vh_p,
			 media_data* media_p)
{
    if (0 != pnt_p->num_points)
    {
        for(int i=0; i<pnt_p->num_points; i++)
        {
            if (pnt_p->points_p[i].enable == true)
            {
                one_point_data_3d* p_p = &pnt_p->points_p[i];
                FP ex = ve_p->comp_p[x_e]->a[p_p->index];
                FP ey = ve_p->comp_p[y_e]->a[p_p->index];
                FP ez = ve_p->comp_p[z_e]->a[p_p->index];
                FP fabs = (ex*ex+ey*ey+ez*ez);
                FP hx = vh_p->comp_p[x_e]->a[p_p->index];
                FP hy = vh_p->comp_p[y_e]->a[p_p->index];
                FP hz = vh_p->comp_p[z_e]->a[p_p->index];

                fprintf(p_p->out,"%d %le %le %le %le %le %le %le %le\n",
                        t+1-p_p->t0,
                        ex,
                        ey,
                        ez,
                        sqrt(fabs),
                        hx,
                        hy,
                        hz,
                        sqrt(hx*hx+hy*hy+hz*hz));
                fflush(p_p->out);
            }
        }
    }
}

/**  Function is used to store point data for moment of time t for point data saving in 3d geometry,
 *   data are written to file
 *   \author  Evgeny Efimenko
 *   \param pnt_p - pointer to point_data structure
 *   \param geometry_p - pointer to geometry structure
 *   \param t - moment of time
 *   \param ve_p - pointer to 3 component E field
 *   \param vh_p - pointer to 3 component H field
 *   \param n_p - pointer to electron density
 *   \param abs_p - pointer to absorption array
 *   \return  void
 *   \callgraph
 *   \callergraph
 */
void update_point_data_3d(points_data_3d* pnt_p,
                          geometry_data* geometry_p,
                          int t,
                          vfield_n* ve_p)
{
    if (0 != pnt_p->num_points)
    {
        for(int i=0; i<pnt_p->num_points; i++)
        {
            one_point_data_3d* p_p = &pnt_p->points_p[i];
            int z = p_p->z_pos - geometry_p->move_index;

            if (pnt_p->points_p[i].enable == true)
            {
                if (z <= 0 || z >= geometry_p->z_max-1)
                {
                    p_p->enable = false;
                    fclose(p_p->out);
                    p_p->open = false;
                    fprintf(stderr, "File[%d] closed\n", i);
                }
                else
                {
                    p_p->index = INDEX_P(ve_p->comp_p[x_e],p_p->x_pos,p_p->y_pos,z);
                }
            }
            else
            {
                if (z > 0 && z < geometry_p->z_max-1)
                {
                    p_p->index = INDEX_P(ve_p->comp_p[x_e],p_p->x_pos,p_p->y_pos,z);
                    p_p->t0 = t;
                    p_p->enable = true;
                    p_p->out = fopen(p_p->file, "w");
                    fprintf(stderr, "Open %s\n", p_p->file);
                    if (NULL == p_p->out)
                    {
                        fprintf(stderr, "Unable to open file for writing %s\n", p_p->file);
                        exit(-1);
                    }
                    p_p->open = true;
                    fprintf(p_p->out, "# %d %lf\n", pnt_p->t_max, pnt_p->delta_t);
                    fflush(p_p->out);
                }
            }
        }
    }
}

/**  Function is used to store point data for moment of time t for point data saving in 2d geometry,
 *   data are written to file
 *   \author  Evgeny Efimenko
 *   \param pnt_p - pointer to point_data structure
 *   \param geometry_p - pointer to geometry structure
 *   \param t - moment of time
 *   \param ve_p - pointer to 3 component E field
 *   \param vh_p - pointer to 3 component H field
 *   \param n_p - pointer to electron density
 *   \param abs_p - pointer to absorption array
 *   \return  void
 *   \callgraph
 *   \callergraph
 */
void store_point_data_2d(points_data_2d* pnt_p,
                         geometry_data* geometry_p,
                         int t,
                         field* fx_p,
                         field* fy_p,
                         field* fz_p)
{
    if (0 != pnt_p->num_points)
    {
        for(int i=0; i<pnt_p->num_points; i++)
        {
            if (pnt_p->points_p[i].enable == true)
            {
                one_point_data_2d* p_p = &pnt_p->points_p[i];
                FP fx  = fx_p->a[p_p->index];
                FP fy  = fy_p->a[p_p->index];
                FP fz  = fz_p->a[p_p->index];

                fprintf(p_p->out,"%d %lf %lf %lf\n",
                        t+1-p_p->t0,
                        fx,
                        fz,
                        fy);
                fflush(p_p->out);
            }
        }
    }
}

/**  Function is used to store point data for moment of time t for point data saving in 3d geometry,
 *   data are written to file
 *   \author  Evgeny Efimenko
 *   \param pnt_p - pointer to point_data structure
 *   \param geometry_p - pointer to geometry structure
 *   \param t - moment of time
 *   \param ve_p - pointer to 3 component E field
 *   \param vh_p - pointer to 3 component H field
 *   \param n_p - pointer to electron density
 *   \param abs_p - pointer to absorption array
 *   \return  void
 *   \callgraph
 *   \callergraph
 */
void update_point_data_2d(points_data_2d* pnt_p,
                          geometry_data* geometry_p,
                          int t,
                          field* e_p)
{
    if (0 != pnt_p->num_points)
    {
        for(int i=0; i<pnt_p->num_points; i++)
        {
            one_point_data_2d* p_p = &pnt_p->points_p[i];
            int x = p_p->x_pos - geometry_p->move_index;

            if (pnt_p->points_p[i].enable == true)
            {
                if (x <= 0 || x >= geometry_p->x_max-1)
                {
                    p_p->enable = false;
                    fclose(p_p->out);
                    p_p->open = false;
                    fprintf(stderr, "File[%d] closed\n", i);
                }
                else
                {
                    p_p->index = INDEX2D_P(e_p, x, p_p->z_pos);
                }
            }
            else
            {
                if (x > 0 &&  x < geometry_p->x_max-1)
                {
                    p_p->index = INDEX2D_P(e_p, x, p_p->z_pos);
                    p_p->t0 = t;
                    p_p->enable = true;
                }
            }
        }
    }
}

/**  Function is used to init structure containing all data related to 2d geometry
 *   Domain decomposition for MPI is also performed here
 *   \author  Evgeny Efimenko
 *   \param ct - referrence to configuration structure
 *   \param g_p - pointer to geometry data structure
 *   \return  void
 *   \callgraph
 *   \callergraph
 */
void init_geometry2d(configTable* ct_p, geometry_data* g_p)
{
    string gtype = ct_p->getStringValue("gtype", "3D");

    if (gtype.compare("2D-TM") == 0)
    {
        g_p->gtype = TWOD_TM_e;
        g_p->y_max = 1;
    }
    else if (gtype.compare("2D-TE") == 0)
    {
        g_p->gtype = TWOD_TE_e;
        g_p->y_max = 1;
    }
    else
    {
        fprintf(stderr, "Unknown geometry type\n");
        exit(-1);
    }

    MPI_BARRIER(MPI_COMM_WORLD);

    g_p->delta_x = ct_p->getDoubleValue("delta_x", 0.04);
    g_p->delta_y = ct_p->getDoubleValue("delta_y", 0.04);
    g_p->delta_z = ct_p->getDoubleValue("delta_z", 0.04);

    g_p->moving  = ct_p->getIntValue("moving", 0);

    g_p->x_full = ct_p->getIntValue("x_max", 64);
    g_p->y_full = 1;
    g_p->z_full = ct_p->getIntValue("z_max", 64);

#ifdef USEMPI
    mpi.num_proc_x = ct_p->getIntValue("num_proc_x", 1);
    mpi.num_proc_y = 1;
    mpi.num_proc_z = ct_p->getIntValue("num_proc_z", 1);

    if (mpi.num_proc_x * mpi.num_proc_z != (int)mpi.size)
    {
        fprintf(stderr, "Fallback to simple linear division\n");
        mpi.num_proc_x = mpi.size;
        mpi.num_proc_y = 1;
        mpi.num_proc_z = 1;
    }

    int temp_x_max = g_p->x_full/mpi.num_proc_x;
    int temp_z_max = g_p->z_full/mpi.num_proc_z;

    mpi.k = mpi.rank/mpi.num_proc_x;
    mpi.j = 0;
    mpi.i = mpi.rank%mpi.num_proc_x;

    if (mpi.num_proc_x > 1)
    {
        if (mpi.i == 0)
        {
            g_p->x_max = temp_x_max;
        }
        else
        {
            g_p->x_max = temp_x_max+1;
        }

        if (mpi.i > 0)
        {
            g_p->i_offset = (mpi.i-1)*temp_x_max + temp_x_max - 1;
        }
        else
        {
            g_p->i_offset = 0;
        }
    }
    else
    {
        g_p->x_max = g_p->x_full;
        g_p->i_offset = 0;
    }

    if (mpi.num_proc_z > 1)
    {
        if (mpi.k == 0)
        {
            g_p->z_max = temp_z_max;
        }
        else
        {
            g_p->z_max = temp_z_max+1;
        }

        if (mpi.k > 0)
        {
            g_p->k_offset = (mpi.k-1)*temp_z_max + temp_z_max -1;
        }
        else
        {
            g_p->k_offset = 0;
        }
    }
    else
    {
        g_p->z_max = g_p->z_full;
        g_p->k_offset = 0;
    }

#else
    g_p->i_offset = 0;
    g_p->x_max = g_p->x_full;
    g_p->j_offset = 0;
    g_p->y_max = g_p->y_full;
    g_p->k_offset = 0;
    g_p->z_max = g_p->z_full;
#endif
    g_p->imax = g_p->x_max-1;
    g_p->imin = 1;
    g_p->kmax = g_p->z_max-1;
    g_p->kmin = 1;
    g_p->move_index = 0;

#ifdef CUDA
    init_cuda_geometry_2d(g_p);
#endif
}

/**  Function is used to init structure containing all data related to 3d geometry
 *   Domain decomposition for MPI is also performed here
 *   \author  Evgeny Efimenko
 *   \param ct - referrence to configuration structure
 *   \param g_p - pointer to geometry data structure
 *   \return  void
 *   \callgraph
 *   \callergraph
 */
void init_test_geometry3d(configTable* ct_p, geometry_data* g_p)
{
    g_p->gtype = THREED_e;

    g_p->delta_x = ct_p->getDoubleValue("delta_x", 0.04);
    g_p->delta_y = ct_p->getDoubleValue("delta_y", 0.04);
    g_p->delta_z = ct_p->getDoubleValue("delta_z", 0.04);

    g_p->moving  = 0;

    g_p->z_full = 64;
    g_p->y_full = 64;
    g_p->x_full = 64;

    g_p->i_offset = 0;
    g_p->x_max = 64;
    g_p->k_offset = 0;
    g_p->z_max = 64;
    g_p->j_offset = 0;
    g_p->y_max = 64;
    g_p->move_index = 0;

    g_p->imax = g_p->x_max-1;
    g_p->imin = 1;
    g_p->jmax = g_p->y_max-1;
    g_p->jmin = 1;
    g_p->kmax = g_p->z_max-1;
    g_p->kmin = 1;
    return;
}

/**  Function is used to init structure containing all data related to 3d geometry
 *   Domain decomposition for MPI is also performed here
 *   \author  Evgeny Efimenko
 *   \param ct - referrence to configuration structure
 *   \param g_p - pointer to geometry data structure
 *   \return  void
 *   \callgraph
 *   \callergraph
 */
void init_geometry3d(configTable* ct_p, geometry_data* g_p)
{
    string gtype = ct_p->getStringValue("gtype", "3D");

    if (gtype.compare("3D") != 0)
    {
        fprintf(stderr, "Wrong geometry type\n");
        exit(-1);
    }

    g_p->gtype = THREED_e;

    MPI_BARRIER(MPI_COMM_WORLD);

    g_p->vf = ct_p->getDoubleValue("phase_velocity_z", 0);

    g_p->delta_x = ct_p->getDoubleValue("delta_x", 0.04);
    g_p->delta_y = ct_p->getDoubleValue("delta_y", 0.04);
    g_p->delta_z = ct_p->getDoubleValue("delta_z", 0.04);

    g_p->moving  = ct_p->getIntValue("moving", 0);

    g_p->x_full = ct_p->getIntValue("x_max", 64);
    g_p->y_full = ct_p->getIntValue("y_max", 64);
    g_p->z_full = ct_p->getIntValue("z_max", 64);

    g_p->x_max = g_p->x_full;
    g_p->y_max = g_p->y_full;
    g_p->z_max = g_p->z_full;

    g_p->i_offset = 0;
    g_p->j_offset = 0;
    g_p->k_offset = 0;

#ifdef USEMPI
#ifdef USENDF
    int add_cells = 2;
#else
    int add_cells = 1;
#endif
    mpi.num_proc_x = ct_p->getIntValue("num_proc_x", 1);
    mpi.num_proc_y = ct_p->getIntValue("num_proc_y", 1);
    mpi.num_proc_z = ct_p->getIntValue("num_proc_z", 1);

    if (mpi.num_proc_x *
	mpi.num_proc_y *
	mpi.num_proc_z != (int)mpi.size)
    {
        fprintf(stderr, "Fallback to simple linear division\n");
        mpi.num_proc_x = mpi.size;
        mpi.num_proc_y = 1;
        mpi.num_proc_z = 1;
    }

    if (g_p->moving == 2)
    {
        if (1 != mpi.num_proc_z)
        {
            fprintf(stderr, "Wrong config. For moving window should be num_proc_z = 1, instead %d\n", mpi.num_proc_z);
            exit(-1);
        }
    }

    int temp_x_max = g_p->x_full/mpi.num_proc_x;
    int temp_y_max = g_p->y_full/mpi.num_proc_y;
    int temp_z_max = g_p->z_full/mpi.num_proc_z;

    mpi.k = mpi.rank/(mpi.num_proc_x*mpi.num_proc_y);
    int resid = mpi.rank%(mpi.num_proc_x*mpi.num_proc_y);
    mpi.j = resid/mpi.num_proc_x;
    mpi.i = resid%mpi.num_proc_x;

    /* manipulation to compensate ghost points */
    /* initialize x direction */
    if (mpi.num_proc_x > 1)
    {
        if (mpi.i == 0)
        {
            g_p->x_max = temp_x_max;
        }
        else
        {
            g_p->x_max = temp_x_max + add_cells;
	    g_p->i_offset = mpi.i*temp_x_max - add_cells;
	}
    }

    if (mpi.num_proc_y > 1)
    {
        if (mpi.j == 0)
        {
            g_p->y_max = temp_y_max;
        }
        else
        {
	    g_p->y_max = temp_y_max + add_cells;
	    g_p->j_offset = mpi.j * temp_y_max - add_cells;
        }
    }

    if (mpi.num_proc_z > 1)
    {
        if (mpi.k == 0)
        {
            g_p->z_max = temp_z_max;
        }
        else
        {
	    g_p->z_max = temp_z_max + add_cells;
	    g_p->k_offset = mpi.k * temp_z_max - add_cells;
	}
    }
#endif

    g_p->imax = g_p->x_max-1;
    g_p->imin = 1;
    g_p->jmax = g_p->y_max-1;
    g_p->jmin = 1;
    g_p->kmax = g_p->z_max-1;
    g_p->kmin = 1;
    g_p->move_index = 0;

#ifdef CUDA
    init_cuda_geometry_3d(g_p);
#endif
}

int init_tensor_data(tensor_data* t_p,
		     FP x, FP y, FP z,
		     FP x_c, FP y_c, FP z_c,
		     FP eps, FP inv_eps)
{
    FP rx = fabs(x - x_c);
    FP ry = fabs(y - y_c);
    FP rz = fabs(z - z_c);

    FP r = sqrt(rx * rx + ry * ry + rz * rz);

    FP n1 = rx/r;
    FP n2 = ry/r;
    FP n3 = rz/r;

    t_p->t[t11] = n1 * n1 * inv_eps + (1 - n1 * n1) / eps;
    t_p->t[t12] = n1 * n2 * inv_eps - n1 * n2 / eps;
    t_p->t[t13] = n1 * n3 * inv_eps - n1 * n3 / eps;
    t_p->t[t21] = n2 * n1 * inv_eps - n2 * n1 / eps;
    t_p->t[t22] = n2 * n2 * inv_eps + (1 - n2 * n2) / eps;
    t_p->t[t23] = n2 * n3 * inv_eps - n2 * n3 / eps;
    t_p->t[t31] = n3 * n1 * inv_eps - n3 * n1 / eps;
    t_p->t[t32] = n3 * n2 * inv_eps - n3 * n2 / eps;
    t_p->t[t33] = n3 * n3 * inv_eps + (1 - n3 * n3) / eps;

    return 0;
}

int print_tensor_data(tensor_data* t_p)
{
    if (t_p->t[t11] > 1 || t_p->t[t22] > 1 || t_p->t[t33] > 1)
    {
	fprintf(stderr, "t11 = %lf\tt12=%lf\tt13=%lf\n", t_p->t[t11], t_p->t[t12], t_p->t[t13]);
	fprintf(stderr, "t21 = %lf\tt22=%lf\tt23=%lf\n", t_p->t[t21], t_p->t[t22], t_p->t[t23]);
	fprintf(stderr, "t31 = %lf\tt32=%lf\tt33=%lf\n", t_p->t[t31], t_p->t[t32], t_p->t[t33]);
    }
    return 0;
}

FP calc_ex_from_d(tensor_data* t_p, FP dx, FP dy, FP dz)
{
    return (dx * t_p->t[t11] + dy * t_p->t[t12] + dz * t_p->t[t13]);
}

FP calc_ey_from_d(tensor_data* t_p, FP dx, FP dy, FP dz)
{
    return (dx * t_p->t[t21] + dy * t_p->t[t22] + dz * t_p->t[t23]);
}

FP calc_ez_from_d(tensor_data* t_p, FP dx, FP dy, FP dz)
{
    return (dx * t_p->t[t31] + dy * t_p->t[t32] + dz * t_p->t[t33]);
}

/**  Function is used to create all needed directories
 *   if tag is specified in configuration, directory with its name
 *   is created. Otherwise, simple logic is used[not recommended]
 *   \author  Evgeny Efimenko
 *   \param fdtdConfig - referrence to configuration structure
 *   \param pathPrefix - root derictory
 *   \return  void
 *   \callgraph
 *   \callergraph
 */
void create_directories(configTable* fdtdConfig, char* pathPrefix)
{
    char tempStr[4096];
#ifdef CUDA
#ifdef USEMPI
    sprintf(pathPrefix,"%s/%s_cuda_%dx%dx%d",
            fdtdConfig->getStringValue("path_prefix", "results").c_str(),
            fdtdConfig->getStringValue("tag", "tag").c_str(),
            mpi.num_proc_x,
            mpi.num_proc_y,
            mpi.num_proc_z);
#else
    sprintf(pathPrefix,"%s/%s_cuda", fdtdConfig->getStringValue("path_prefix", "results").c_str(),
            fdtdConfig->getStringValue("tag", "tag").c_str());
#endif
#else /* CUDA */
#ifdef USEMPI
    sprintf(pathPrefix,"%s/%s_%dx%dx%d",
            fdtdConfig->getStringValue("path_prefix", "results").c_str(),
            fdtdConfig->getStringValue("tag", "new").c_str(),
            mpi.num_proc_x,
            mpi.num_proc_y,
            mpi.num_proc_z);
#else
    sprintf(pathPrefix,"%s/%s", fdtdConfig->getStringValue("path_prefix", "results").c_str(),
            fdtdConfig->getStringValue("tag", "new").c_str());
#endif
#endif /* CUDA */
    RANK_S(0);
    info_s0("Creating directories");
    create_directory(fdtdConfig->getStringValue("path_prefix", "results").c_str());

    create_directory(pathPrefix);

    sprintf(tempStr,"%s/data",pathPrefix);
    create_directory(tempStr);

    sprintf(tempStr,"%s/restart",pathPrefix);
    create_directory(tempStr);

    info_s0("Directories created.");

    sprintf(tempStr,"%s/config.log",pathPrefix);
    fdtdConfig->printTable(tempStr);
    RANK_F(0);
    return;
}

/**  Function is used to malloc field with memory allocation check
 *   \author  Evgeny Efimenko
 *   \param file - name of file (__FILE__)
 *   \param line - number of line (__LINE__)
 *   \return  field*
 *   \callgraph
 *   \callergraph
 */
field* malloc_field(const char*file, int line)
{
    field* ans_p = (field*)malloc(sizeof(field));
    if (NULL == ans_p)
    {
        fprintf(stderr, "%s:%d Unable to allocate field\nExit!\n", file, line);
	fflush(stderr);
        exit(-1);
    }
    return ans_p;
}

/**  Function is used to malloc FP field with memory allocation check and allocates internal array
 *   \author  Evgeny Efimenko
 *   \param file - name of file (__FILE__)
 *   \param line - number of line (__LINE__)
 *   \param nx - number of points in x direction
 *   \param ny - number of points in y direction
 *   \param nz - number of points in z direction
 *   \return  field*
 *   \callgraph
 *   \callergraph
 */
field* malloc_and_init_field(const char*file, int line, int nx, int ny, int nz)
{
    field* ans_p = (field*)malloc(sizeof(field));

    if (NULL == ans_p)
    {
        fprintf(stderr, "%s:%d Unable to allocate field\nExit!\n", file, line);
	fflush(stderr);
        exit(-1);
    }

    init_field3d(ans_p, nx, ny, nz);
    return ans_p;
}

/**  Function is used to malloc int field with memory allocation check and allocates internal array
 *   \author  Evgeny Efimenko
 *   \param file - name of file (__FILE__)
 *   \param line - number of line (__LINE__)
 *   \param nx - number of points in x direction
 *   \param ny - number of points in y direction
 *   \param nz - number of points in z direction
 *   \return  field*
 *   \callgraph
 *   \callergraph
 */
ifield* malloc_and_init_ifield(const char*file, int line, int nx, int ny, int nz)
{
    ifield* ans_p = (ifield*)malloc(sizeof(ifield));

    if (NULL == ans_p)
    {
        fprintf(stderr, "%s:%d Unable to allocate field\nExit!\n", file, line);
	fflush(stderr);
        exit(-1);
    }
    init_ifield3d(ans_p, nx, ny, nz);
    return ans_p;
}

/**  Function is used to init field for 2d fields, implemented as a wrapper to init_field3d
 *   \author  Evgeny Efimenko
 *   \param   field_p - pointer to field being initialised
 *   \param nx - number of points in x direction
 *   \param nz - number of points in z direction
 *   \return  field*
 *   \callgraph
 *   \callergraph
 */
int init_field(field* field_p,
               int nx,
               int nz)
{
    init_field3d(field_p, nx, 1, nz);
    return 0;
}

/**  Function is used to init int field for 3d fields
 *   \author  Evgeny Efimenko
 *   \param   field_p - pointer to field being initialised
 *   \param   nx - number of points in x direction
 *   \param   ny - number of points in x direction
 *   \param   nz - number of points in x direction
 *   \return  0
*/
int init_ifield3d(ifield* field_p,
                  int nx,
                  int ny,
                  int nz)
{
    int bytes = sizeof(int)*nx*nz*ny;
    field_p->nx = nx;
    field_p->ny = ny;
    field_p->nz = nz;
    field_p->nxy = nx*ny;

    if (0 == bytes)
    {
        field_p->a = NULL;
        return 0;
    }

    field_p->a = (int*)malloc(bytes);

    if (NULL == field_p->a)
    {
        fprintf(stderr, "Not enough memory %d bytes: %d %d %d\n", bytes, nx, ny, nz);
	fflush(stderr);
        exit(-1);
    }
    memset(field_p->a, 0, bytes);

    return 0;
}

/**  Function is used to zero int field for 3d fields
 *   \author  Evgeny Efimenko
 *   \param   ifield_p - pointer to field being initialised
 *   \return  0
*/
int zero_ifield3d(ifield* f_p)
{
    if (0 == f_p->nx || 0 == f_p->ny || 0 == f_p->nz ||	NULL == f_p->a)
    {
        return -1;
    }

    memset(f_p->a, 0, f_p->nx*f_p->ny*f_p->nz*sizeof(int));
    return 0;
}

/**  Function is used to destroy int field for 3d fields and to deallocate all memory
 *   \author  Evgeny Efimenko
 *   \param   ifield_p - pointer to field being initialised
 *   \return  0
*/
void destroy_ifield(ifield* f_p)
{
    if (0 == f_p->nx || 0 == f_p->ny ||	0 == f_p->nz ||	NULL == f_p->a)
    {
        return;
    }

    free(f_p->a);
    f_p->a = NULL;
}

/**  Function is used to init FP field for 3d fields
 *   \author  Evgeny Efimenko
 *   \param   field_p - pointer to field being initialised
 *   \param   nx - number of points in x direction
 *   \param   ny - number of points in x direction
 *   \param   nz - number of points in x direction
 *   \return  0
*/
int init_field3d(field* f_p,
                 int nx,
                 int ny,
                 int nz)
{
    int bytes = sizeof(FP)*nx*nz*ny;
    f_p->nx = nx;
    f_p->ny = ny;
    f_p->nz = nz;
    f_p->nxy = nx*ny;

    if (0 == bytes)
    {
        f_p->a = NULL;
        return 0;
    }

    f_p->a = (FP*)malloc(bytes);

    if (NULL == f_p->a)
    {
        fprintf(stderr, "Not enough memory %d bytes: %d %d %d\n", bytes, nx, ny, nz);
	fflush(stderr);
        exit(-1);
    }
    memset(f_p->a, 0, bytes);

    return 0;
}

/**  Function is used to zero FP field for 3d fields
 *   \author  Evgeny Efimenko
 *   \param   if_p - pointer to field being initialised
 *   \return  0
*/
int zero_field3d(field* f_p)
{
    if (0 == f_p->nx ||	0 == f_p->ny ||	0 == f_p->nz ||	NULL == f_p->a)
    {
	return -1;
    }

    memset(f_p->a, 0, f_p->nx*f_p->ny*f_p->nz*sizeof(FP));
    return 0;
}

/**  Function is used to zero FP field for 2d fields
 *   \author  Evgeny Efimenko
 *   \param   ifield_p - pointer to field being initialised
 *   \return  0
*/
int zero_field(field* f_p)
{
    memset(f_p->a, 0, f_p->nx*f_p->nz*sizeof(FP));
    return 0;
}

/**  Function is used to swap FP field for 2d fields
 *   \author  Evgeny Efimenko
 *   \param   f_p - pointer to field being initialised
 *   \param   num - number of points to swap
 *   \return  0
*/
int swap_field(field* f_p, int num)
{
    memmove(f_p->a, f_p->a + num * f_p->nz,
            (f_p->nx - num) * f_p->nz * sizeof(FP));
    memset(f_p->a + (f_p->nx - num) * f_p->nz, 0,
           num * f_p->nz * sizeof(FP));
    return 0;
}

/**  Function is used to swap FP field for 3d fields
 *   \author  Evgeny Efimenko
 *   \param   f_p - pointer to field being initialised
 *   \param   num - number of points to swap
 *   \return  0
*/
int swap_field3d(field* f_p, int num)
{
    int offset = num * f_p->nx * f_p->ny;
    int count = (f_p->nz - num) * f_p->nx * f_p->ny;
    memmove(f_p->a, f_p->a + offset, count * sizeof(FP));
    memset(f_p->a + count, 0, offset * sizeof(FP));
    return 0;
}

/**  Function is used to swap FP array
 *   \author  Evgeny Efimenko
 *   \param   f_p - pointer to array
 *   \param   num - number of points to swap
 *   \return  0
*/
int swap_array_3d(FP* a_p, int num, int nx, int ny, int nz)
{
    memmove(a_p, a_p+num*nx*ny, (nz-num)*nx*ny*sizeof(FP));
    memset(a_p+(nz-num)*nx*ny, 0, num*nx*ny*sizeof(FP));
    return 0;
}

/**  Function is used to swap FP field_n for 3d fields
 *   \author  Evgeny Efimenko
 *   \param   f_p - pointer to field being initialised
 *   \param   num - number of points to swap
 *   \return  0
*/
int swap_field_n_3d(field_n* f_p, int num)
{
    memmove(f_p->a,
            f_p->a+num*f_p->nx*f_p->ny,
            (f_p->nz-num)*f_p->nx*f_p->ny*sizeof(FP));

    memset(f_p->a+(f_p->nz-num)*f_p->nx*f_p->ny,
           0,
           num*f_p->nx*f_p->ny*sizeof(FP));
    return 0;
}

/**  Function is used to swap FP field for 1d fields
 *   \author  Evgeny Efimenko
 *   \param   f_p - pointer to field being initialised
 *   \param   num - number of points to swap
 *   \return  0
*/
int swap_field1d(field* f_p, int num)
{
    memmove(f_p->a, f_p->a+num, (f_p->nx-num)*sizeof(FP));
    memset(f_p->a+(f_p->nx-num), 0, num*sizeof(FP));
    return 0;
}

/**  Function is used to copy FP field for 2d fields
 *   \author  Evgeny Efimenko
 *   \param   src_p - pointer to source field
 *   \param   dest_p - pointer to destination field
 *   \param   num - number of points to swap
 *   \return  0
*/
int copy_field(field* src_p, field* dest_p)
{
    if (src_p->nx != dest_p->nx ||
	src_p->nz != dest_p->nz ||
	src_p->ny != dest_p->ny)
    {
        fprintf(stderr, "Array dimensions do not match\n"
                "src: %udx%udx%ud\n"
                "dest:%udx%udx%ud\n",
                src_p->nx,
                src_p->ny,
                src_p->nz,
                dest_p->nx,
                dest_p->ny,
                dest_p->nz);
        return -1;
    }
    memcpy((void*)dest_p->a, (void*)src_p->a, src_p->nx*src_p->nz*sizeof(FP));
    return 0;
}

void zero_array(FP* dst_p, int offset, int max)
{
    if (dst_p != NULL && offset >= 0 && max-offset >= 0)
    {
	memset(dst_p+offset, 0, (max-offset)*sizeof(FP));
    }
    else
    {
	fprintf(stderr, "pointer %p offset=%d max=%d\n", dst_p, offset, max);
	fflush(stderr);
    }
}

/**  Function is used to destroy FP field for 1d fields
 *   \author  Evgeny Efimenko
 *   \param   f_p - pointer to field being initialised
 *   \param   num - number of points to swap
 *   \return  0
*/
void destroy_field(field* f_p)
{
    if (0 == f_p->nx ||	0 == f_p->ny ||	0 == f_p->nz ||	NULL == f_p->a)
    {
        return;
    }

    free(f_p->a);
    f_p->a = NULL;
}

/**  Function is used to init FP field for 3d fields, new implementation
 *   During compilation can be switched between FP and float, default is FP
 *   \author  Evgeny Efimenko
 *   \param   f_p - pointer to field_n being initialised
 *   \param   name  - name of field
 *   \param   nx - number of points in x direction
 *   \param   ny - number of points in x direction
 *   \param   nz - number of points in x direction
 *   \return  0
*/
int init_field_n(field_n* f_p,
                 const char*name,
                 int nx,
                 int ny,
                 int nz)
{
    f_p->nx = nx;
    f_p->ny = ny;
    f_p->nz = nz;
    f_p->nxy = nx*ny;

    int bytes = nx*ny*nz*sizeof(FP);

    if (0 == bytes)
    {
        f_p->a = NULL;
    }
    else
    {
	f_p->a = (FP*)malloc(bytes);

	if (NULL == f_p->a)
	{
	    fprintf(stderr, "Not enough memory for %s: %d*%d*%d=%dMb %dKb %db\n",
		    f_p->name,
		    f_p->nx,
		    f_p->ny,
		    f_p->nz,
		    bytes/1048576,
		    (bytes%1048576)/1024,
		    (bytes%1048576)%1024);
	    fflush(stderr);
	    exit(-1);
	}

	memset(f_p->a, 0, bytes);
	sprintf(f_p->name,"%s", name);
    }
    return 0;
}

/**  Function is used to destroy FP field, new implementation
 *   \author  Evgeny Efimenko
 *   \param   f_p - pointer to field being initialised
 *   \return  0
*/
void destroy_field_n(field_n* f_p)
{
    if (0 == f_p->nx &&	0 == f_p->ny &&	0 == f_p->nz)
    {
        return;
    }

    if (NULL != f_p->a)
    {
        free(f_p->a);
        f_p->a = NULL;
    }
}

void dump_vfield(vfield_n* vf_p, const char*name, const char* pathPrefix, int t)
{
    gzFile out;
    int index;
    char name_full[512];
#ifdef USEMPI
    sprintf(name_full, "%s/restart/%s_t%d_%dx%dx%d.dat.gz", pathPrefix, name, t, mpi.i, mpi.j, mpi.k);
#else
    sprintf(name_full, "%s/restart/%s_t%d.dat.gz", pathPrefix, name, t);
#endif

    if (NULL == (out = gzopen(name_full, "wb9")))
    {
        fprintf(stderr, "Unable to open file %s for writing\n", name_full);
	fflush(stderr);
        exit(-1);
    }

    fprintf(stderr, "Saving %s\n", name_full);

    field_n* fx_p = vf_p->comp_p[x_e];
    field_n* fy_p = vf_p->comp_p[y_e];
    field_n* fz_p = vf_p->comp_p[z_e];
    FP array[3];

    for (int k = 0; k < fx_p->nz; k++)
    {
        for (int i = 0; i < fx_p->nx; i++)
        {
            for (int j = 0; j < fx_p->ny; j++)
            {
                index = INDEX_P(fx_p,i,j,k);
                array[0] = (FP)fx_p->a[index];
                array[1] = (FP)fy_p->a[index];
                array[2] = (FP)fz_p->a[index];
                gzwrite(out, (void*)array, sizeof(FP)*3);
            }
        }
    }
    gzclose(out);
    return;
}

void dump_field(field* f_p, const char*name, const char* pathPrefix, int t)
{
    gzFile out;
    int index;
    char name_full[512];
    FP temp;

    if (f_p->nx == 0 && f_p->ny == 0 && f_p->nz == 0)
    {
        return;
    }

#ifdef USEMPI
    sprintf(name_full, "%s/restart/%s_t%d_%dx%dx%d.dat.gz", pathPrefix, name, t, mpi.i, mpi.j, mpi.k);
#else
    sprintf(name_full, "%s/restart/%s_t%d.dat.gz", pathPrefix, name, t);
#endif

    if (NULL == (out = gzopen(name_full, "wb9")))
    {
        fprintf(stderr, "Unable to open file for writing\n");
        exit(-1);
    }

    fprintf(stderr, "Saving %s\n", name_full);

    for (int k = 0; k < f_p->nz; k++)
    {
        for (int i = 0; i < f_p->nx; i++)
        {
            for (int j = 0; j < f_p->ny; j++)
            {
                index = INDEX_P(f_p,i,j,k);
                temp = (FP)f_p->a[index];
                gzwrite(out, (void*)&temp, sizeof(FP));
            }
        }
    }
    gzclose(out);
    return;
}

void read_vfield(vfield_n* vf_p, const char*name, const char* pathPrefix, int t)
{
    gzFile out;
    int index;
    char name_full[512];
    FP array[3];

#ifdef USEMPI
    sprintf(name_full, "%s/restart/%s_t%d_%dx%dx%d.dat.gz", pathPrefix, name, t, mpi.i, mpi.j, mpi.k);
#else
    sprintf(name_full, "%s/restart/%s_t%d.dat.gz", pathPrefix, name, t);
#endif

    if (NULL == (out = gzopen(name_full, "rb")))
    {
        fprintf(stderr, "Unable to open file %s for reading\n", name_full);
        exit(-1);
    }

    field_n* fx_p = vf_p->comp_p[x_e];
    field_n* fy_p = vf_p->comp_p[y_e];
    field_n* fz_p = vf_p->comp_p[z_e];

    for (int k = 0; k < fx_p->nz; k++)
    {
        for (int i = 0; i < fx_p->nx; i++)
        {
            for (int j = 0; j < fx_p->ny; j++)
            {
                index = INDEX_P(fx_p,i,j,k);
                gzread(out, (void*)array, sizeof(FP)*3);
                fx_p->a[index] = array[0];
                fy_p->a[index] = array[1];
                fz_p->a[index] = array[2];
            }
        }
    }
    gzclose(out);
    return;
}

void read_field(field* f_p, const char*name, const char* pathPrefix, int t)
{
    gzFile out;
    int index;
    char name_full[512];
    FP temp;
    if (f_p->nx == 0 && f_p->ny == 0 && f_p->nz == 0)
    {
        return;
    }

#ifdef USEMPI
    sprintf(name_full, "%s/restart/%s_t%d_%dx%dx%d.dat.gz", pathPrefix, name, t, mpi.i, mpi.j, mpi.k);
#else
    sprintf(name_full, "%s/restart/%s_t%d.dat.gz", pathPrefix, name, t);
#endif

    if (NULL == (out = gzopen(name_full, "rb")))
    {
        fprintf(stderr, "Unable to open file %s for reading\n", name_full);
        exit(-1);
    }

    for (int k = 0; k < f_p->nz; k++)
    {
        for (int i = 0; i < f_p->nx; i++)
        {
            for (int j = 0; j < f_p->ny; j++)
            {
                index = INDEX_P(f_p,i,j,k);
                gzread(out, (void*)&temp, sizeof(FP));
                f_p->a[index] = temp;
            }
        }
    }
    gzclose(out);
    return;
}

void init_savedata(configTable* ct_p, save_data* save_data_p)
{
    save_data_p->start_time = ct_p->getIntValue("start_save", 0);
    save_data_p->end_time = ct_p->getIntValue("end_save", 1000000);
    save_data_p->delta_time = ct_p->getIntValue("delta_save", 100);
    save_data_p->save_3d_time = ct_p->getIntValue("save_3d_time", 1000000);

    if (0 == strcmp(ct_p->getStringValue("save_0_step", "off").c_str(),"on"))
    {
        save_data_p->save_0_step = true;
    }
    else
    {
        save_data_p->save_0_step = false;
    }

    if (0 == strcmp(ct_p->getStringValue("save_3d", "off").c_str(),"on"))
    {
        save_data_p->save_3d = true;
    }
    else
    {
        save_data_p->save_3d = false;
    }

    if (0 == strcmp(ct_p->getStringValue("compress", "on").c_str(),"on"))
    {
        save_data_p->compress = true;
    }
    else
    {
        save_data_p->compress = false;
    }

    if (0 == strcmp(ct_p->getStringValue("thin", "on").c_str(),"on"))
    {
        save_data_p->thin = true;
    }
    else
    {
        save_data_p->thin = false;
    }

    if (0 == strcmp(ct_p->getStringValue("save", "on").c_str(),"on"))
    {
        save_data_p->save = true;
    }
    else
    {
        save_data_p->save = false;
    }
}

void savedata_save(configTable* ct_p,
                   save_data* save_data_p,
                   geometry_data* geometry_p,
                   pml_data*   pml_p,
                   int t,
                   field* a1,
                   field* a2,
                   field* a3,
		   const char* pathPrefix,
                   bool force)
{
    (void)ct_p;
    (void)pml_p;

    int x_max = geometry_p->x_max;
    int z_max = geometry_p->z_max;
    int x_full = geometry_p->x_full;
    int z_full = geometry_p->z_full;
    int xoffset = geometry_p->i_offset;
    int zoffset = geometry_p->k_offset;
    FP delta_x = geometry_p->delta_x;
    FP delta_z = geometry_p->delta_z;

    if (false == save_data_p->save)
    {
        return;
    }

    if (t < save_data_p->start_time || t > save_data_p->end_time)
    {
        return;
    }

    if (t % save_data_p->delta_time == 0 || force == true)
    {
        char tmpString[1024];

        if(true == save_data_p->compress)
        {
#ifdef USEMPI
            sprintf(tmpString, "%s/data/data%06d_%d.r.gz", pathPrefix, t, mpi.rank);
#else
            sprintf(tmpString, "%s/data/data%06d.r.gz", pathPrefix, t);
#endif
            gzFile out = gzopen(tmpString, "wb");
            if (NULL == out)
            {
                fprintf(stderr, "Unable to create file %s errno %d\n",
                        tmpString, errno);
                return;
            }
            sprintf(tmpString, "# %d %d %lf %lf\n", x_full, z_full,
                    delta_x, delta_z);
            gzputs(out, tmpString);

            for (int j = 1; j< z_max; j++)
            {
                for (int i = 1; i< x_max; i++)
                {
                    int index = INDEX2D_P(a1, i, j);
                    if (save_data_p->thin    == false      ||
			a1->a[index] != 0       ||
			a2->a[index] != 0       ||
			a3->a[index] != 0)
                    {
                        sprintf(tmpString, "%d %d %e %e %e\n", i+xoffset+1, j+zoffset+1,
                                a1->a[index],
                                a2->a[index],
                                a3->a[index]);
                        gzputs(out, tmpString);
                    }
                }
            }
            gzclose(out);
        }
        else
        {
#ifdef USEMPI
            sprintf(tmpString, "%s/data/data%06d_%d.r", pathPrefix, t, mpi.rank);
#else
            sprintf(tmpString, "%s/data/data%06d.r", pathPrefix, t);
#endif
            FILE* out = fopen(tmpString, "w+");
            if (NULL == out)
            {
                fprintf(stderr, "Unable to create file %s errno %d\n",
                        tmpString, errno);
                return;
            }
            fprintf(out, "# %d %d %lf %lf\n", x_full, z_full, delta_x, delta_z);
            for (int i = 1; i< x_max; i++)
            {
                for (int j = 1; j< z_max; j++)
                {
                    int index = INDEX2D_P(a1, i, j);
                    if (save_data_p->thin    == false  ||
                            a1->a[index] != 0       ||
                            a2->a[index] != 0       ||
                            a3->a[index] != 0       )
                    {
                        fprintf(out, "%d %d %e %e %e\n", i+xoffset+1, j+zoffset+1,
                                a1->a[index],
                                a2->a[index],
                                a3->a[index]);
                    }
                }
            }
            fclose(out);
        }
    }
}

int run_one_config(const char*config, bool estimate)
{

    char pathPrefix[MAX_PATH_PREFIX_LENGTH];
    configTable* fdtdConfig = new configTable("default");
    int moving = 0;
    int t_max  = 0;
    int result;
    time_t time_ep1, time_ep2;

    time_ep1 = time(NULL);

    RANK_S(0);
    info_s0("Start reading configuration");
    RANK_F(0);

    if (-1 == (result = fdtdConfig->fillFromFile(config)))
    {
	fprintf(stderr, "Error opening %s\n", config);
	exit(-1);
    }

    RANK_S(0);
    info_s0("Reading configuration finished");
    RANK_F(0);

    moving = fdtdConfig->getIntValue("moving", 0);

    if (true == estimate)
    {
        t_max  = fdtdConfig->getIntValue("t_max", 1500);
        fdtdConfig->setIntValue("t_max", ESTIMATION_MAX_TIME);
        fdtdConfig->setIntValue("moving", 0);
	fdtdConfig->setIntValue("num_points", 0);
    }

    MPI_BARRIER(MPI_COMM_WORLD);

    geometry_data geometry;
    time_scheme timescheme;

    if (fdtdConfig->getStringValue("gtype", "3D").compare("3D") != 0)
    {
        init_geometry2d(fdtdConfig, &geometry);
    }
    else
    {
        init_geometry3d(fdtdConfig, &geometry);
    }

    MPI_BARRIER(MPI_COMM_WORLD);

    create_directories(fdtdConfig, pathPrefix);

    init_time_scheme(fdtdConfig, &timescheme);

    MPI_BARRIER(MPI_COMM_WORLD);

    if (TWOD_TE_e == geometry.gtype)
    {
        run_2D_TE_config(fdtdConfig,
                         geometry,
                         timescheme,
                         pathPrefix);
    }
    else if (TWOD_TM_e == geometry.gtype)
    {
        run_2D_TM_config(fdtdConfig,
                         geometry,
                         timescheme,
                         pathPrefix);
    }
    else
    {
        run_3D_config(fdtdConfig,
                      geometry,
                      timescheme,
                      pathPrefix);
    }
    time_ep2 = time(NULL);
    result = (int)(time_ep2 - time_ep1);
    if (true == estimate)
    {
        fdtdConfig->setIntValue("t_max", t_max);
        fdtdConfig->setIntValue("moving", moving);
        result *= t_max/ESTIMATION_MAX_TIME;
        if (0 < moving)
        {
            result /= 2;
        }
    }
    delete fdtdConfig;

    return result;
}

void assemble_vfield(vfield* vf_p, field* x_p, field* y_p, field* z_p)
{
    vf_p->comp_p[0] = x_p;
    vf_p->comp_p[1] = y_p;
    vf_p->comp_p[2] = z_p;
}

void assemble_vfield_n(vfield_n* vf_p, field_n* x_p, field_n* y_p, field_n* z_p)
{
    vf_p->comp_p[0] = x_p;
    vf_p->comp_p[1] = y_p;
    vf_p->comp_p[2] = z_p;
}

vfield_n* init_vfield_n(const char* name, int nx, int ny, int nz)
{
    char temp[1024];
    vfield_n* res_p = (vfield_n*)malloc(sizeof(vfield_n));

    if (NULL == res_p)
    {
	exit(-1);
    }

    field_n* x_p = (field_n*)malloc(sizeof(field_n));
    field_n* y_p = (field_n*)malloc(sizeof(field_n));
    field_n* z_p = (field_n*)malloc(sizeof(field_n));

    if (NULL == x_p || NULL == y_p || NULL == z_p)
    {
	exit(-1);
    }

    sprintf(temp, "%sx", name);
    init_field_n(x_p, temp, nx, ny, nz);

    sprintf(temp, "%sy", name);
    init_field_n(y_p, temp, nx, ny, nz);

    sprintf(temp, "%sz", name);
    init_field_n(z_p, temp, nx, ny, nz);

    res_p->comp_p[0] = x_p;
    res_p->comp_p[1] = y_p;
    res_p->comp_p[2] = z_p;

    return res_p;
}

int destroy_vfield_n(vfield_n* vf_p)
{
    for (int i = 0; i < 3; i++)
    {
	destroy_field_n(vf_p->comp_p[i]);
	free(vf_p->comp_p[i]);
    }
    free(vf_p);
    return 0;
}

/**  Function is used to swap FP field_n for 3d fields
 *   \author  Evgeny Efimenko
 *   \param   f_p - pointer to field being initialised
 *   \param   num - number of points to swap
 *   \return  0
*/
int swap_vfield_n_3d(vfield_n* f_p, int num)
{
    for (int i = 0; i < 3; i++)
    {
	swap_field_n_3d(f_p->comp_p[i], num);
    }
    return 0;
}

FP field_abs(field* field_x_p, field* field_z_p, int i, int j)
{
    FP field_z_temp = (field_z_p->a[j + field_z_p->nz*(i-1)] +
                           field_z_p ->a[j + field_z_p->nz*i])*0.5;
    FP field_x_temp = (field_x_p->a[j-1 + field_x_p->nz*i] +
                           field_x_p ->a[j + field_x_p->nz*i])*0.5;
    return sqrt(field_z_temp*field_z_temp + field_x_temp*field_x_temp);
}

int create_directory(const char*name)
{
#ifndef WIN
    int status = mkdir(name, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    if (0 == status)
    {
        info_text_s("Creating %s: %s\n", name, strerror(errno));
        return 0;
    }
    else if (EEXIST == errno)
    {
        warning_text_s("Creating %s: %s\n", name, strerror(errno));
        return 0;
    }
    else
    {
        error_text_s("Error while creating %s:%s\n", name, strerror(errno));
        exit(-1);
    }
#else
    int status = CreateDirectory(name, NULL);
    if (0 != status)
    {
        info_text_s("Creating %s\n", name);
        return 0;
    }
    else
    {
        int res = GetLastError();
        if (ERROR_ALREADY_EXISTS == res)
        {
            warning_text_s("Creating %s: Already exists\n", name);
            return 0;
        }
        else if (ERROR_PATH_NOT_FOUND == res)
        {
            error_text_s("Error while creating %s: Path not found\n", name);
            exit(-1);
        }
        else
        {
            error_text_s("Error while creating %s: Unknown error\n", name);
            exit(-1);
        }
    }
#endif
    return 0;
}

#ifdef USEMPI
void mpi_wait_up_x(field_n* fx_p,
                   field_n* fy_p,
                   field_n* fz_p)
{
    MPI_Status myStatus;
    if (mpi.num_proc_x != 1)
    {
        if (mpi.i != mpi.num_proc_x-1)
        {
            MPI_Wait(&mpi.s_x, &myStatus);
        }
        if (mpi.i != 0)
        {
            MPI_Wait(&mpi.r_x, &myStatus);

            int offset = fx_p->ny * fx_p->nz;

            for (int k = 0; k < fx_p->nz; k++)
            {
                for (int j = 0; j < fx_p->ny; j++)
                {
                    int index2d = INDEX_YZ_P(fx_p,j,k);
                    int index   = INDEX_P(fx_p,0,j,k);
                    fx_p->a[index] = mpi.rb3d_x[index2d];
                    fy_p->a[index] = mpi.rb3d_x[offset+index2d];
                    fz_p->a[index] = mpi.rb3d_x[2*offset+index2d];
                }
            }
        }
    }
}

void mpi_wait_up_y(field_n* fx_p,
                   field_n* fy_p,
                   field_n* fz_p)
{
    MPI_Status myStatus;
    if (mpi.num_proc_y != 1)
    {
        if (mpi.j != mpi.num_proc_y-1)
        {
            MPI_Wait(&mpi.s_y, &myStatus);
        }
        if (mpi.j != 0)
        {
            MPI_Wait(&mpi.r_y, &myStatus);

            int offset = fx_p->nx * fx_p->nz;

            for (int k = 0; k < fx_p->nz; k++)
            {
                for (int i = 0; i < fx_p->nx; i++)
                {
                    int index2d = INDEX_XZ_P(fx_p,i,k);
                    int index   = INDEX_P(fx_p,i,0,k);
                    fx_p->a[index] = mpi.rb3d_y[index2d];
                    fy_p->a[index] = mpi.rb3d_y[offset+index2d];
                    fz_p->a[index] = mpi.rb3d_y[2*offset+index2d];
                }
            }
        }
    }
}

void mpi_wait_up_z(field_n* fx_p,
                   field_n* fy_p,
                   field_n* fz_p)
{
    MPI_Status myStatus;
    if (mpi.num_proc_z != 1)
    {
        if (mpi.k != mpi.num_proc_z-1)
        {
            MPI_Wait(&mpi.s_z, &myStatus);
        }

        if (mpi.k != 0)
        {
            MPI_Wait(&mpi.r_z, &myStatus);

            int offset = fx_p->ny * fx_p->nx;

            for (int i = 0; i < fx_p->nx; i++)
            {
                for (int j = 0; j < fx_p->ny; j++)
                {
                    int index2d = INDEX_XY_P(fx_p,i,j);
                    int index   = INDEX_P(fx_p,i,j,0);
                    fx_p->a[index] = mpi.rb3d_z[index2d];
                    fy_p->a[index] = mpi.rb3d_z[offset+index2d];
                    fz_p->a[index] = mpi.rb3d_z[2*offset+index2d];
                }
            }
        }
    }
}
void mpi_wait_down_x(field_n* fx_p,
                     field_n* fy_p,
                     field_n* fz_p)
{
    MPI_Status myStatus;
    if (mpi.num_proc_x != 1)
    {
        if (mpi.i != 0)
        {
            MPI_Wait(&mpi.s_x, &myStatus);
        }

        if (mpi.i != mpi.num_proc_x-1)
        {
            MPI_Wait(&mpi.r_x, &myStatus);

            int offset = fx_p->ny * fx_p->nz;

            for (int k = 0; k < fx_p->nz; k++)
            {
                for (int j = 0; j < fx_p->ny; j++)
                {
                    int index2d = INDEX_YZ_P(fx_p,j,k);
                    int index   = INDEX_P(fx_p,fx_p->nx-1,j,k);
                    fx_p->a[index] = mpi.rb3d_x[index2d];
                    fy_p->a[index] = mpi.rb3d_x[offset+index2d];
                    fz_p->a[index] = mpi.rb3d_x[2*offset+index2d];
                }
            }
        }
    }
}

void mpi_wait_down_y(field_n* fx_p,
                     field_n* fy_p,
                     field_n* fz_p)
{
    MPI_Status myStatus;
    if (mpi.num_proc_y != 1)
    {
        if (mpi.j != 0)
        {
            MPI_Wait(&mpi.s_y, &myStatus);
        }
        if (mpi.j != mpi.num_proc_y-1)
        {
            MPI_Wait(&mpi.r_y, &myStatus);

            int offset = fx_p->nx * fx_p->nz;

            for (int k = 0; k < fx_p->nz; k++)
            {
                for (int i = 0; i < fx_p->nx; i++)
                {
                    int index2d = INDEX_XZ_P(fx_p,i,k);
                    int index   = INDEX_P(fx_p,i,fx_p->ny-1,k);
                    fx_p->a[index] = mpi.rb3d_y[index2d];
                    fy_p->a[index] = mpi.rb3d_y[offset+index2d];
                    fz_p->a[index] = mpi.rb3d_y[2*offset+index2d];
                }
            }
        }
    }

}
void mpi_wait_down_z(field_n* fx_p,
                     field_n* fy_p,
                     field_n* fz_p)
{
    MPI_Status myStatus;

    if (mpi.num_proc_z != 1)
    {
        if (mpi.k != 0)
        {
            MPI_Wait(&mpi.s_z, &myStatus);
        }
        if (mpi.k != mpi.num_proc_z-1)
        {
            MPI_Wait(&mpi.r_z, &myStatus);

            int offset = fx_p->ny * fx_p->nx;

            for (int i = 0; i < fx_p->nx; i++)
            {
                for (int j = 0; j < fx_p->ny; j++)
                {
                    int index2d = INDEX_XY_P(fx_p,i,j);
                    int index   = INDEX_P(fx_p,i,j,fx_p->nz-1);

                    fx_p->a[index] = mpi.rb3d_z[index2d];
                    fy_p->a[index] = mpi.rb3d_z[offset+index2d];
                    fz_p->a[index] = mpi.rb3d_z[2*offset+index2d];
                }
            }
        }

    }
}

void init_mpi(int argc, char *argv[], mpi_data* mpi_p)
{
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_p->size);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_p->rank);
    MPI_Get_processor_name(mpi_p->processor_name, &mpi_p->namelen);
}

void finalize_mpi()
{
    MPI_Finalize();
}

int  mpi_calc_rank(mpi_data* mpi_p, int i, int j, int k)
{
    int rank = i + j*mpi_p->num_proc_x + k*mpi_p->num_proc_y*mpi_p->num_proc_x;
    return rank;
}

int  init_mpi_sendrecv_buffer(mpi_data* mpi_p, geometry_data* geometry_p)
{
    if (mpi_p->i + 1 >= mpi_p->num_proc_x)
    {
        mpi_p->up_x = MPI_PROC_NULL;
    }
    else
    {
        mpi_p->up_x = mpi_calc_rank(mpi_p, mpi_p->i + 1, mpi_p->j, mpi_p->k);
    }

    if (mpi_p->i - 1 < 0)
    {
        mpi_p->down_x = MPI_PROC_NULL;
    }
    else
    {
        mpi_p->down_x = mpi_calc_rank(mpi_p, mpi_p->i - 1, mpi_p->j, mpi_p->k);
    }

    if (mpi_p->k + 1 >= mpi_p->num_proc_z)
    {
        mpi_p->up_z = MPI_PROC_NULL;
    }
    else
    {
        mpi_p->up_z = mpi_calc_rank(mpi_p, mpi_p->i, mpi_p->j, mpi_p->k+1);
    }

    if (mpi_p->k - 1 < 0)
    {
        mpi_p->down_z = MPI_PROC_NULL;
    }
    else
    {
        mpi_p->down_z = mpi_calc_rank(mpi_p, mpi_p->i, mpi_p->j, mpi_p->k-1);
    }

    if (geometry_p->x_max > geometry_p->z_max)
    {
        mpi_p->buff_size = geometry_p->x_max;
    }
    else
    {
        mpi_p->buff_size = geometry_p->z_max;
    }

    mpi_p->sendbuffer = FP_MALLOC(mpi_p->buff_size);
    mpi_p->recvbuffer = FP_MALLOC(mpi_p->buff_size);

    return 0;
}

int init_mpi_sendrecv_buffer3d(mpi_data* mpi_p, geometry_data* geometry_p, vfield_n* D_p, vfield_n* H_p)
{
    /* calculate neighbours in x directions */
    if (mpi_p->i + 1 >= mpi_p->num_proc_x)
    {
        mpi_p->up_x = MPI_PROC_NULL;
    }
    else
    {
        mpi_p->up_x = mpi_calc_rank(mpi_p, mpi_p->i + 1, mpi_p->j, mpi_p->k);
    }

    if (mpi_p->i - 1 < 0)
    {
        mpi_p->down_x = MPI_PROC_NULL;
    }
    else
    {
        mpi_p->down_x = mpi_calc_rank(mpi_p, mpi_p->i - 1, mpi_p->j, mpi_p->k);
    }

    /* calculate neighbours in y directions */
    if (mpi_p->j + 1 >= mpi_p->num_proc_y)
    {
        mpi_p->up_y = MPI_PROC_NULL;
    }
    else
    {
        mpi_p->up_y = mpi_calc_rank(mpi_p, mpi_p->i, mpi_p->j+1, mpi_p->k);
    }

    if (mpi_p->j - 1 < 0)
    {
        mpi_p->down_y = MPI_PROC_NULL;
    }
    else
    {
        mpi_p->down_y = mpi_calc_rank(mpi_p, mpi_p->i, mpi_p->j-1, mpi_p->k);
    }

    /* calculate neighbours in z directions */
    if (mpi_p->k + 1 >= mpi_p->num_proc_z)
    {
        mpi_p->up_z = MPI_PROC_NULL;
    }
    else
    {
        mpi_p->up_z = mpi_calc_rank(mpi_p, mpi_p->i, mpi_p->j, mpi_p->k+1);
    }

    if (mpi_p->k - 1 < 0)
    {
        mpi_p->down_z = MPI_PROC_NULL;
    }
    else
    {
        mpi_p->down_z = mpi_calc_rank(mpi_p, mpi_p->i, mpi_p->j, mpi_p->k-1);
    }

    mpi_p->sizex = 3*geometry_p->y_max*geometry_p->z_max;
    mpi_p->sizey = 3*geometry_p->x_max*geometry_p->z_max;
    mpi_p->sizez = 3*geometry_p->y_max*geometry_p->x_max;

    mpi_p->buff_size = mpi_p->sizex;

    if (mpi_p->sizey > mpi_p->buff_size)
    {
        mpi_p->buff_size = mpi_p->sizey;
    }

    if (mpi_p->sizez > mpi_p->buff_size)
    {
        mpi_p->buff_size = mpi_p->sizez;
    }

    mpi_p->sb3d = FP_MALLOC(mpi_p->buff_size);
    mpi_p->rb3d = FP_MALLOC(mpi_p->buff_size);
    mpi_p->sb3d_x = FP_MALLOC(mpi_p->sizex);
    mpi_p->rb3d_x = FP_MALLOC(mpi_p->sizex);
    mpi_p->sb3d_y = FP_MALLOC(mpi_p->sizey);
    mpi_p->rb3d_y = FP_MALLOC(mpi_p->sizey);
    mpi_p->sb3d_z = FP_MALLOC(mpi_p->sizez);
    mpi_p->rb3d_z = FP_MALLOC(mpi_p->sizez);

    mpi_p->x_num = geometry_p->y_max*geometry_p->z_max;
    mpi_p->y_num = geometry_p->x_max*geometry_p->z_max;
    mpi_p->z_num = geometry_p->y_max*geometry_p->x_max;

    mpi_p->recv_down_x_idx = INT_MALLOC(mpi_p->x_num);
    mpi_p->recv_down_y_idx = INT_MALLOC(mpi_p->y_num);
    mpi_p->recv_down_z_idx = INT_MALLOC(mpi_p->z_num);
    mpi_p->send_down_x_idx = INT_MALLOC(mpi_p->x_num);
    mpi_p->send_down_y_idx = INT_MALLOC(mpi_p->y_num);
    mpi_p->send_down_z_idx = INT_MALLOC(mpi_p->z_num);
    mpi_p->recv_up_x_idx = INT_MALLOC(mpi_p->x_num);
    mpi_p->recv_up_y_idx = INT_MALLOC(mpi_p->y_num);
    mpi_p->recv_up_z_idx = INT_MALLOC(mpi_p->z_num);
    mpi_p->send_up_x_idx = INT_MALLOC(mpi_p->x_num);
    mpi_p->send_up_y_idx = INT_MALLOC(mpi_p->y_num);
    mpi_p->send_up_z_idx = INT_MALLOC(mpi_p->z_num);

    field_n* fx_p = D_p->comp_p[x_e];
    field_n* fy_p = D_p->comp_p[y_e];

    int idx = 0;
#ifdef USENDF
    int from_d = 1;
    int from_u = fy_p->nx-2;
#else
    int from_d = 0;
    int from_u = fy_p->nx-1;
#endif
    for (int k = 0; k < fy_p->nz; k++)
    {
	for (int j = 0; j < fy_p->ny; j++)
	{
	    mpi_p->recv_down_x_idx[idx] = INDEX_P(fy_p,fy_p->nx-1,j,k);
	    mpi_p->send_down_x_idx[idx] = INDEX_P(fy_p,from_d,j,k);
	    mpi_p->recv_up_x_idx[idx] = INDEX_P(fy_p,0,j,k);
	    mpi_p->send_up_x_idx[idx] = INDEX_P(fy_p,from_u,j,k);
	    idx++;
	}
    }

    idx = 0;
#ifdef USENDF
    from_u = fx_p->ny-2;
#else
    from_u = fx_p->ny-1;
#endif
    for (int k = 0; k < fx_p->nz; k++)
    {
	for (int i = 0; i < fx_p->nx; i++)
	{
	    mpi_p->recv_down_y_idx[idx] = INDEX_P(fx_p,i,fx_p->ny-1,k);
	    mpi_p->send_down_y_idx[idx] = INDEX_P(fx_p,i,from_d,k);
	    mpi_p->recv_up_y_idx[idx] = INDEX_P(fx_p,i,0,k);
	    mpi_p->send_up_y_idx[idx] = INDEX_P(fx_p,i,from_u,k);
	    idx++;
	}
    }

    idx = 0;
#ifdef USENDF
    from_u = fx_p->nz-2;
#else
    from_u = fx_p->nz-1;
#endif
    for (int i = 0; i < fx_p->nx; i++)
    {
	for (int j = 0; j < fx_p->ny; j++)
	{
	    mpi_p->recv_down_z_idx[idx] = INDEX_P(fx_p,i,j,fx_p->nz-1);
	    mpi_p->send_down_z_idx[idx] = INDEX_P(fx_p,i,j,from_d);
	    mpi_p->recv_up_z_idx[idx] = INDEX_P(fx_p,i,j,0);
	    mpi_p->send_up_z_idx[idx] = INDEX_P(fx_p,i,j,from_u);
	    idx++;
	}
    }
#ifdef CUDA
    init_mpi_sendrecv_buffer3d_cuda(mpi_p);
#endif
    return 0;
}

int  delete_mpi_sendrecvbuffer(mpi_data* mpi_p)
{
    free_fp(mpi_p->sendbuffer);
    free_fp(mpi_p->recvbuffer);
    return 0;
}

int  delete_mpi_sendrecvbuffer3d(mpi_data* mpi_p)
{
    free_fp(mpi_p->sb3d);
    free_fp(mpi_p->rb3d);
    free_fp(mpi_p->sb3d_x);
    free_fp(mpi_p->rb3d_x);
    free_fp(mpi_p->sb3d_y);
    free_fp(mpi_p->rb3d_y);
    free_fp(mpi_p->sb3d_z);
    free_fp(mpi_p->rb3d_z);
    free_int(mpi_p->recv_up_x_idx);
    free_int(mpi_p->recv_up_y_idx);
    free_int(mpi_p->recv_up_z_idx);
    free_int(mpi_p->send_up_x_idx);
    free_int(mpi_p->send_up_y_idx);
    free_int(mpi_p->send_up_z_idx);
    free_int(mpi_p->recv_down_x_idx);
    free_int(mpi_p->recv_down_y_idx);
    free_int(mpi_p->recv_down_z_idx);
    free_int(mpi_p->send_down_x_idx);
    free_int(mpi_p->send_down_y_idx);
    free_int(mpi_p->send_down_z_idx);
#ifdef CUDA
    delete_mpi_sendrecv_buffer3d_cuda(mpi_p);
#endif
    return 0;
}

int mpi_send_down_x(mpi_data* mpi_p, vfield_n* vf_p)
{
    field_n* fy_p = vf_p->comp_p[y_e];
    field_n* fz_p = vf_p->comp_p[z_e];
    int count = 2 * mpi_p->x_num;
    int offset = mpi_p->x_num;

    for(int k=0; k<mpi_p->x_num; k++)
    {
	int idx = mpi_p->send_down_x_idx[k];
	mpi_p->sb3d[k] = fy_p->a[idx];
	mpi_p->sb3d[offset+k] = fz_p->a[idx];
    }

    MPI_Send((void*)(mpi_p->sb3d),
	     count,
	     MPI_FP,
	     mpi_p->down_x,
	     0,
	     MPI_COMM_WORLD);
    return 0;
}

int mpi_recv_down_x(mpi_data* mpi_p, vfield_n* vf_p)
{
    field_n* fy_p = vf_p->comp_p[y_e];
    field_n* fz_p = vf_p->comp_p[z_e];
    int count = 2 * mpi_p->x_num;
    int offset = mpi_p->x_num;

    MPI_Recv((void*)(mpi_p->rb3d),
	     count,
	     MPI_FP,
	     mpi_p->up_x,
	     0,
	     MPI_COMM_WORLD,
	     MPI_STATUS_IGNORE);

    for(int k=0; k<mpi_p->x_num; k++)
    {
	int idx = mpi_p->recv_down_x_idx[k];
	fy_p->a[idx] = mpi_p->rb3d[k];
	fz_p->a[idx] = mpi_p->rb3d[offset + k];
    }

    return 0;
}

int mpi_send_down_y(mpi_data* mpi_p, vfield_n* vf_p)
{
    field_n* fx_p = vf_p->comp_p[x_e];
    field_n* fz_p = vf_p->comp_p[z_e];
    int count = 2 * mpi_p->y_num;
    int offset = mpi_p->y_num;

    for (int k=0; k<mpi_p->y_num; k++)
    {
	int idx = mpi_p->send_down_y_idx[k];
	mpi_p->sb3d[k]          = fx_p->a[idx];
	mpi_p->sb3d[offset + k] = fz_p->a[idx];
    }

    MPI_Send((void*)(mpi_p->sb3d),
	     count,
	     MPI_FP,
	     mpi_p->down_y,
	     0,
	     MPI_COMM_WORLD);
    return 0;
}

int mpi_recv_down_y(mpi_data* mpi_p, vfield_n* vf_p)
{
    field_n* fx_p = vf_p->comp_p[x_e];
    field_n* fz_p = vf_p->comp_p[z_e];
    int count = 2 * mpi_p->y_num;
    int offset = mpi_p->y_num;

    MPI_Recv((void*)(mpi_p->rb3d),
	     count,
	     MPI_FP,
	     mpi_p->up_y,
	     0,
	     MPI_COMM_WORLD,
	     MPI_STATUS_IGNORE);

    for (int k=0; k<mpi_p->y_num; k++)
    {
	int idx = mpi_p->recv_down_y_idx[k];
	fx_p->a[idx] = mpi_p->rb3d[k];
	fz_p->a[idx] = mpi_p->rb3d[offset + k];
    }

    return 0;
}

int mpi_send_down_z(mpi_data* mpi_p, vfield_n* vf_p)
{
    field_n* fx_p = vf_p->comp_p[x_e];
    field_n* fy_p = vf_p->comp_p[y_e];
    int count = 2 * mpi_p->z_num;
    int offset = mpi_p->z_num;

    for (int k=0; k<mpi_p->z_num; k++)
    {
	int idx = mpi_p->send_down_z_idx[k];
	mpi_p->sb3d[k]          = fx_p->a[idx];
	mpi_p->sb3d[offset + k] = fy_p->a[idx];
    }

    MPI_Send((void*)(mpi_p->sb3d),
	     count,
	     MPI_FP,
	     mpi_p->down_z,
	     0,
	     MPI_COMM_WORLD);
    return 0;
}

int mpi_recv_down_z(mpi_data* mpi_p, vfield_n* vf_p)
{
    field_n* fx_p = vf_p->comp_p[x_e];
    field_n* fy_p = vf_p->comp_p[y_e];
    int count = 2 * mpi_p->z_num;
    int offset = mpi_p->z_num;
    MPI_Recv((void*)(mpi_p->rb3d),
	     count,
	     MPI_FP,
	     mpi_p->up_z,
	     0,
	     MPI_COMM_WORLD,
	     MPI_STATUS_IGNORE);

    for (int k=0; k<mpi_p->z_num; k++)
    {
	int idx = mpi_p->recv_down_z_idx[k];
	fx_p->a[idx] = mpi_p->rb3d[k];
	fy_p->a[idx] = mpi_p->rb3d[offset + k];
    }

    return 0;
}

int mpi_send_up_x(mpi_data* mpi_p, vfield_n* vf_p)
{
    field_n* fy_p = vf_p->comp_p[y_e];
    field_n* fz_p = vf_p->comp_p[z_e];

    int count = 2 * mpi_p->x_num;
    int offset = mpi_p->x_num;

    for(int k=0; k<mpi_p->x_num; k++)
    {
	int idx = mpi_p->send_up_x_idx[k];
	mpi_p->sb3d[k] = fy_p->a[idx];
	mpi_p->sb3d[offset+k] = fz_p->a[idx];
    }

    MPI_Send((void*)(mpi_p->sb3d),
	     count,
	     MPI_FP,
	     mpi_p->up_x,
	     0,
	     MPI_COMM_WORLD);
    return 0;
}

int mpi_recv_up_x(mpi_data* mpi_p, vfield_n* vf_p)
{
    field_n* fy_p = vf_p->comp_p[y_e];
    field_n* fz_p = vf_p->comp_p[z_e];

    int count = 2 * mpi_p->x_num;
    int offset = mpi_p->x_num;

    MPI_Recv((void*)(mpi_p->rb3d),
	     count,
	     MPI_FP,
	     mpi_p->down_x,
	     0,
	     MPI_COMM_WORLD,
	     MPI_STATUS_IGNORE);

    for(int k=0; k<mpi_p->x_num; k++)
    {
	int idx = mpi_p->recv_up_x_idx[k];
	fy_p->a[idx] = mpi_p->rb3d[k];
	fz_p->a[idx] = mpi_p->rb3d[offset + k];
    }

    return 0;
}

int mpi_send_up_y(mpi_data* mpi_p, vfield_n* vf_p)
{
    field_n* fx_p = vf_p->comp_p[x_e];
    field_n* fz_p = vf_p->comp_p[z_e];
    int count = 2 * mpi_p->y_num;
    int offset = mpi_p->y_num;

    for (int k=0; k<mpi_p->y_num; k++)
    {
	int idx = mpi_p->send_up_y_idx[k];
	mpi_p->sb3d[k]          = fx_p->a[idx];
	mpi_p->sb3d[offset + k] = fz_p->a[idx];
    }

    MPI_Send((void*)(mpi_p->sb3d),
	     count,
	     MPI_FP,
	     mpi_p->up_y,
	     0,
	     MPI_COMM_WORLD);
    return 0;
}

int mpi_recv_up_y(mpi_data* mpi_p, vfield_n* vf_p)
{
    field_n* fx_p = vf_p->comp_p[x_e];
    field_n* fz_p = vf_p->comp_p[z_e];
    int count = 2 * mpi_p->y_num;
    int offset = mpi_p->y_num;

    MPI_Recv((void*)(mpi_p->rb3d),
	     count,
	     MPI_FP,
	     mpi_p->down_y,
	     0,
	     MPI_COMM_WORLD,
	     MPI_STATUS_IGNORE);

    for (int k=0; k<mpi_p->y_num; k++)
    {
	int idx = mpi_p->recv_up_y_idx[k];
	fx_p->a[idx] = mpi_p->rb3d[k];
	fz_p->a[idx] = mpi_p->rb3d[offset + k];
    }

    return 0;
}

int mpi_send_up_z(mpi_data* mpi_p, vfield_n* vf_p)
{
    field_n* fx_p = vf_p->comp_p[x_e];
    field_n* fy_p = vf_p->comp_p[y_e];
    int count = 2 * mpi_p->z_num;
    int offset = mpi_p->z_num;

    for (int k=0; k<mpi_p->z_num; k++)
    {
	int idx = mpi_p->send_up_z_idx[k];
	mpi_p->sb3d[k]          = fx_p->a[idx];
	mpi_p->sb3d[offset + k] = fy_p->a[idx];
    }

    MPI_Send((void*)(mpi_p->sb3d),
	     count,
	     MPI_FP,
	     mpi_p->up_z,
	     0,
	     MPI_COMM_WORLD);
    return 0;
}

int mpi_recv_up_z(mpi_data* mpi_p, vfield_n* vf_p)
{
    field_n* fx_p = vf_p->comp_p[x_e];
    field_n* fy_p = vf_p->comp_p[y_e];
    int count = 2 * mpi_p->z_num;
    int offset = mpi_p->z_num;

    MPI_Recv((void*)(mpi_p->rb3d),
	     count,
	     MPI_FP,
	     mpi_p->down_z,
	     0,
	     MPI_COMM_WORLD,
	     MPI_STATUS_IGNORE);

    for (int k=0; k<mpi_p->z_num; k++)
    {
	int idx = mpi_p->recv_up_z_idx[k];
	fx_p->a[idx] = mpi_p->rb3d[k];
	fy_p->a[idx] = mpi_p->rb3d[offset + k];
    }

    return 0;
}

int  mpi_exch_up2d_z(mpi_data* mpi_p, geometry_data* geometry_p, field* f_p)
{
    int i;
    if (mpi_p->k%2 == 0)
    {
        if (mpi_p->k != 0)
        {
            MPI_Recv((void*)mpi_p->recvbuffer,
                     geometry_p->x_max,
                     MPI_FP,
                     mpi_p->down_z,
                     0,
                     MPI_COMM_WORLD,
                     MPI_STATUS_IGNORE);
            for (i = 0; i < geometry_p->x_max; i++)
            {
                f_p->a[INDEX2D_P(f_p, i, 0)] = mpi_p->recvbuffer[i];
            }
        }

        for (i = 0; i < geometry_p->x_max; i++)
        {
            mpi_p->sendbuffer[i] =
                f_p->a[INDEX2D_P(f_p, i, geometry_p->z_max - 1)];
        }
        MPI_Send((void*)mpi_p->sendbuffer,
                 geometry_p->x_max,
                 MPI_FP,
                 mpi_p->up_z,
                 0,
                 MPI_COMM_WORLD);
    }
    else
    {
        if (mpi_p->k != mpi_p->num_proc_z-1)
        {
            for (i = 0; i < geometry_p->x_max; i++)
            {
                mpi_p->sendbuffer[i] =
                    f_p->a[INDEX2D_P(f_p, i, geometry_p->z_max-1)];
            }
            MPI_Send((void*)mpi_p->sendbuffer,
                     geometry_p->x_max,
                     MPI_FP,
                     mpi_p->up_z,
                     0,
                     MPI_COMM_WORLD);
        }

        MPI_Recv((void*)mpi_p->recvbuffer,
                 geometry_p->x_max,
                 MPI_FP,
                 mpi_p->down_z,
                 0,
                 MPI_COMM_WORLD,
                 MPI_STATUS_IGNORE);
        for (i = 0; i < geometry_p->x_max; i++)
        {
            f_p->a[INDEX2D_P(f_p, i, 0)] = mpi_p->recvbuffer[i];
        }
    }
    return 0;
}

int  mpi_exch_up2d_x(mpi_data* mpi_p, geometry_data* geometry_p, field* f_p)
{
    int k;
    if (mpi_p->i%2 == 0)
    {
        for (k = 0; k < geometry_p->z_max; k++)
        {
            mpi_p->sendbuffer[k] =
                f_p->a[INDEX2D_P(f_p, geometry_p->x_max-1, k)];
        }
        MPI_Send((void*)mpi_p->sendbuffer,
                 geometry_p->z_max,
                 MPI_FP,
                 mpi_p->up_x,
                 0,
                 MPI_COMM_WORLD);

        if (mpi_p->i != 0)
        {
            MPI_Recv((void*)mpi_p->recvbuffer,
                     geometry_p->z_max,
                     MPI_FP,
                     mpi_p->down_x,
                     0,
                     MPI_COMM_WORLD,
                     MPI_STATUS_IGNORE);

            for (k = 0; k < geometry_p->z_max; k++)
            {
                f_p->a[INDEX2D_P(f_p, 0, k)] = mpi_p->recvbuffer[k];
            }
        }
    }
    else
    {
        MPI_Recv((void*)mpi_p->recvbuffer,
                 geometry_p->z_max,
                 MPI_FP,
                 mpi_p->down_x,
                 0,
                 MPI_COMM_WORLD,
                 MPI_STATUS_IGNORE);

        for (k = 0; k < geometry_p->z_max; k++)
        {
            f_p->a[INDEX2D_P(f_p, 0, k)] = mpi_p->recvbuffer[k];
        }

        if (mpi_p->i != mpi_p->num_proc_x-1)
        {
            for (k = 0; k < geometry_p->z_max; k++)
            {
                mpi_p->sendbuffer[k] =
                    f_p->a[INDEX2D_P(f_p, geometry_p->x_max - 1, k)];
            }
            MPI_Send((void*)mpi_p->sendbuffer,
                     geometry_p->z_max,
                     MPI_FP,
                     mpi_p->up_x,
                     0,
                     MPI_COMM_WORLD);
        }
    }
    return 0;
}

/**

   fields fx_p, fy_p, fz_p are assumed to have the same dimensions in x, y, z directions,
   so everywhere information from fx_p is used.
 */



int mpi_exch_up3d_x_2comp(mpi_data* mpi_p,
			  geometry_data* geometry_p,
			  vfield_n* vf_p)
{
    if (mpi_p->num_proc_x == 1)
    {
        return 0;
    }

    if (mpi_p->i%2 == 0)
    {
	mpi_send_up_x(mpi_p, vf_p);
        if (mpi_p->i != 0)
        {
	    mpi_recv_up_x(mpi_p, vf_p);
        }
    }
    else
    {
	mpi_recv_up_x(mpi_p, vf_p);
        if (mpi_p->i != mpi_p->num_proc_x-1)
        {
	    mpi_send_up_x(mpi_p, vf_p);
	}
    }
    return 0;
}

int mpi_exch_up3d_x_1comp(mpi_data* mpi_p,
			  geometry_data* geometry_p,
			  field_n* f_p)
{
    int j, k;
    int kmax = geometry_p->kmax+1;
    int index, index2d;
#ifdef USENDF
    int from = f_p->nx-2;
#else
    int from = f_p->nx-1;
#endif

    if (mpi_p->num_proc_x == 1)
    {
        return 0;
    }

    mpi_p->buff_size = f_p->ny * kmax;

    if (mpi_p->i%2 == 0)
    {

        for (k = 0; k < kmax; k++)
        {
            for (j = 0; j < f_p->ny; j++)
            {
                index2d = INDEX_YZ_P(f_p,j,k);
                index   = INDEX_P(f_p,from,j,k);
                mpi_p->sb3d[index2d]   = f_p->a[index];
	    }
        }
        MPI_Send((void*)(mpi_p->sb3d),
                 mpi_p->buff_size,
                 MPI_FP,
                 mpi_p->up_x,
                 0,
                 MPI_COMM_WORLD);

        if (mpi_p->i != 0)
        {
            MPI_Recv((void*)(mpi_p->rb3d),
                     mpi_p->buff_size,
                     MPI_FP,
                     mpi_p->down_x,
                     0,
                     MPI_COMM_WORLD,
                     MPI_STATUS_IGNORE);

            for (k = 0; k < kmax; k++)
            {
                for (j = 0; j < f_p->ny; j++)
                {
                    index2d = INDEX_YZ_P(f_p,j,k);
                    index   = INDEX_P(f_p,0,j,k);
                    f_p->a[index] = mpi_p->rb3d[index2d];
		}
            }
        }
    }
    else
    {
        MPI_Recv((void*)(mpi_p->rb3d),
                 mpi_p->buff_size,
                 MPI_FP,
                 mpi_p->down_x,
                 0,
                 MPI_COMM_WORLD,
                 MPI_STATUS_IGNORE);


        for (k = 0; k < kmax; k++)
        {
            for (j = 0; j < f_p->ny; j++)
            {
                index2d = INDEX_YZ_P(f_p,j,k);
                index   = INDEX_P(f_p,0,j,k);
                f_p->a[index] = mpi_p->rb3d[index2d];
	    }
        }

        if (mpi_p->i != mpi_p->num_proc_x-1)
        {
            for (k = 0; k < kmax; k++)
            {
                for (j = 0; j < f_p->ny; j++)
                {
                    index2d = INDEX_YZ_P(f_p,j,k);
                    index   = INDEX_P(f_p,from,j,k);
                    mpi_p->sb3d[index2d]   = f_p->a[index];
                }
            }
            MPI_Send((void*)(mpi_p->sb3d),
                     mpi_p->buff_size,
                     MPI_FP,
                     mpi_p->up_x,
                     0,
                     MPI_COMM_WORLD);
        }
    }
    return 0;
}

/**

   fields fx_p, fy_p, fz_p are assumed to have the same dimensions in x, y, z directions,
   so everywhere information from fx_p is used.
 */
int  mpi_exch_up3d_x_3comp_a(mpi_data* mpi_p,
                             geometry_data* geometry_p,
                             field_n* fx_p,
                             field_n* fy_p,
                             field_n* fz_p)
{
    int j, k;
    int index, index2d;

    if (mpi_p->num_proc_x == 1)
    {
        return 0;
    }

    int offset = fx_p->ny * fx_p->nz;
    int offset2 = 2 * offset;

    if (mpi_p->i%2 == 0)
    {
        for (k = 0; k < fx_p->nz; k++)
        {
            for (j = 0; j < fx_p->ny; j++)
            {
                index2d = INDEX_YZ_P(fx_p,j,k);
                index   = INDEX_P(fx_p,fx_p->nx-1,j,k);

                mpi_p->sb3d_x[index2d]          = fx_p->a[index];
                mpi_p->sb3d_x[offset+index2d]   = fy_p->a[index];
                mpi_p->sb3d_x[offset2+index2d] = fz_p->a[index];
            }
        }

        MPI_Isend((void*)(mpi_p->sb3d_x),
                  mpi_p->sizex,
                  MPI_FP,
                  mpi_p->up_x,
                  MPI_X_TAG,
                  MPI_COMM_WORLD,
                  &mpi_p->s_x);

        if (mpi_p->i != 0)
        {
            MPI_Irecv((void*)(mpi_p->rb3d_x),
                      mpi_p->sizex,
                      MPI_FP,
                      mpi_p->down_x,
                      MPI_X_TAG,
                      MPI_COMM_WORLD,
                      &mpi_p->r_x);
        }
    }
    else
    {
        MPI_Irecv((void*)(mpi_p->rb3d_x),
                  mpi_p->sizex,
                  MPI_FP,
                  mpi_p->down_x,
                  MPI_X_TAG,
                  MPI_COMM_WORLD,
                  &mpi_p->r_x);

        if (mpi_p->i != mpi_p->num_proc_x-1)
        {
            for (k = 0; k < fx_p->nz; k++)
            {
                for (j = 0; j < fx_p->ny; j++)
                {
                    index2d = INDEX_YZ_P(fx_p,j,k);
                    index   = INDEX_P(fx_p,fx_p->nx-1,j,k);

                    mpi_p->sb3d_x[index2d]          = fx_p->a[index];
                    mpi_p->sb3d_x[offset+index2d]   = fy_p->a[index];
                    mpi_p->sb3d_x[offset2+index2d] = fz_p->a[index];
                }
            }

            MPI_Isend((void*)(mpi_p->sb3d_x),
                      mpi_p->sizex,
                      MPI_FP,
                      mpi_p->up_x,
                      MPI_X_TAG,
                      MPI_COMM_WORLD,
                      &mpi_p->s_x);
        }
    }
    return 0;
}

/**

   fields fx_p, fy_p, fz_p are assumed to have the same dimensions in x, y, z directions,
   so everywhere information from fx_p is used.
 */

int mpi_exch_up3d_y_2comp(mpi_data* mpi_p,
			  geometry_data* geometry_p,
			  vfield_n* vf_p)
{
    if (mpi_p->num_proc_y == 1)
    {
        return 0;
    }

    if (mpi_p->j%2 == 0)
    {
	mpi_send_up_y(mpi_p, vf_p);
        if (mpi_p->j != 0)
        {
	    mpi_recv_up_y(mpi_p, vf_p);
        }
    }
    else
    {
	mpi_recv_up_y(mpi_p, vf_p);
        if (mpi_p->j != mpi_p->num_proc_y-1)
        {
	    mpi_send_up_y(mpi_p, vf_p);
        }
    }
    return 0;
}

int mpi_exch_up3d_y_1comp(mpi_data* mpi_p,
			  geometry_data* geometry_p,
			  field_n* f_p)
{
    int i, k;
    int kmax = geometry_p->kmax+1;
    int index, index2d;
#ifdef USENDF
    int from = f_p->ny-2;
#else
    int from = f_p->ny-1;
#endif

    if (mpi_p->num_proc_y == 1)
    {
        return 0;
    }

    mpi_p->buff_size = 2 * f_p->nx * kmax;

    if (mpi_p->j%2 == 0)
    {
        for (k = 0; k < kmax; k++)
        {
            for (i = 0; i < f_p->nx; i++)
            {
                index2d = INDEX_XZ_P(f_p,i,k);
                index   = INDEX_P(f_p,i,from,k);
                mpi_p->sb3d[index2d] = f_p->a[index];
            }
        }

        MPI_Send((void*)(mpi_p->sb3d),
                 mpi_p->buff_size,
                 MPI_FP,
                 mpi_p->up_y,
                 0,
                 MPI_COMM_WORLD);

        if (mpi_p->j != 0)
        {
            MPI_Recv((void*)(mpi_p->rb3d),
                     mpi_p->buff_size,
                     MPI_FP,
                     mpi_p->down_y,
                     0,
                     MPI_COMM_WORLD,
                     MPI_STATUS_IGNORE);

            for (k = 0; k < kmax; k++)
            {
                for (i = 0; i < f_p->nx; i++)
                {
                    index2d = INDEX_XZ_P(f_p,i,k);
                    index   = INDEX_P(f_p,i,0,k);
                    f_p->a[index] = mpi_p->rb3d[index2d];
                }
            }
        }
    }
    else
    {
        MPI_Recv((void*)(mpi_p->rb3d),
                 mpi_p->buff_size,
                 MPI_FP,
                 mpi_p->down_y,
                 0,
                 MPI_COMM_WORLD,
                 MPI_STATUS_IGNORE);

        for (k = 0; k < kmax; k++)
        {
            for (i = 0; i < f_p->nx; i++)
            {
                index2d = INDEX_XZ_P(f_p,i,k);
                index   = INDEX_P(f_p,i,0,k);
                f_p->a[index] = mpi_p->rb3d[index2d];
            }
        }

        if (mpi_p->j != mpi_p->num_proc_y-1)
        {
            for (k = 0; k < kmax; k++)
            {
                for (i = 0; i < f_p->nx; i++)
                {
                    index2d = INDEX_XZ_P(f_p,i,k);
                    index   = INDEX_P(f_p,i,from,k);
                    mpi_p->sb3d[index2d]        = f_p->a[index];
                }
            }
            MPI_Send((void*)(mpi_p->sb3d),
                     mpi_p->buff_size,
                     MPI_FP,
                     mpi_p->up_y,
                     0,
                     MPI_COMM_WORLD);
        }
    }
    return 0;
}

/**

   fields fx_p, fy_p, fz_p are assumed to have the same dimensions in x, y, z directions,
   so everywhere information from fx_p is used.
 */
int  mpi_exch_up3d_y_2comp_a(mpi_data* mpi_p,
                             geometry_data* geometry_p,
                             field_n* fx_p,
                             field_n* fy_p,
                             field_n* fz_p)
{
    int i, k;
    int index, index2d;

    if (mpi_p->num_proc_y == 1)
    {
        return 0;
    }

    int offset = fx_p->nx * fx_p->nz;
    int offset2 = 2 * offset;

    if (mpi_p->j%2 == 0)
    {
        for (k = 0; k < fx_p->nz; k++)
        {
            for (i = 0; i < fx_p->nx; i++)
            {
                index2d = INDEX_XZ_P(fx_p,i,k);
                index   = INDEX_P(fx_p,i,fx_p->ny-1,k);

                mpi_p->sb3d_y[index2d]          = fx_p->a[index];
                mpi_p->sb3d_y[offset+index2d]   = fy_p->a[index];
                mpi_p->sb3d_y[offset2+index2d] = fz_p->a[index];
            }
        }

        MPI_Isend((void*)(mpi_p->sb3d_y),
                  mpi_p->sizey,
                  MPI_FP,
                  mpi_p->up_y,
                  MPI_Y_TAG,
                  MPI_COMM_WORLD,
                  &mpi_p->s_y);

        if (mpi_p->j != 0)
        {
            MPI_Irecv((void*)(mpi_p->rb3d_y),
                      mpi_p->sizey,
                      MPI_FP,
                      mpi_p->down_y,
                      MPI_Y_TAG,
                      MPI_COMM_WORLD,
                      &mpi_p->r_y);
        }
    }
    else
    {
        MPI_Irecv((void*)(mpi_p->rb3d_y),
                  mpi_p->sizey,
                  MPI_FP,
                  mpi_p->down_y,
                  MPI_Y_TAG,
                  MPI_COMM_WORLD,
                  &mpi_p->r_y);

        if (mpi_p->j != mpi_p->num_proc_y-1)
        {
            for (k = 0; k < fx_p->nz; k++)
            {
                for (i = 0; i < fx_p->nx; i++)
                {
                    index2d = INDEX_XZ_P(fx_p,i,k);
                    index   = INDEX_P(fx_p,i,fx_p->ny-1,k);

                    mpi_p->sb3d_y[index2d]          = fx_p->a[index];
                    mpi_p->sb3d_y[offset+index2d]   = fy_p->a[index];
                    mpi_p->sb3d_y[offset2+index2d] = fz_p->a[index];
                }
            }
            MPI_Isend((void*)(mpi_p->sb3d_y),
                      mpi_p->sizey,
                      MPI_FP,
                      mpi_p->up_y,
                      MPI_Y_TAG,
                      MPI_COMM_WORLD,
                      &mpi_p->s_y);
        }
    }
    return 0;
}

/**
   fields fx_p, fy_p, fz_p are assumed to have the same dimensions in x, y, z directions,
   so everywhere information from fx_p is used.
 */

int  mpi_exch_up3d_z_2comp(mpi_data* mpi_p,
                           geometry_data* geometry_p,
			   vfield_n* vf_p)
{
    if (mpi_p->num_proc_z == 1)
    {
        return 0;
    }

    if (mpi_p->k %2 == 0)
    {
	mpi_send_up_z(mpi_p, vf_p);
        if (mpi_p->k != 0)
        {
	    mpi_recv_up_z(mpi_p, vf_p);
	}
    }
    else
    {
	mpi_recv_up_z(mpi_p, vf_p);
        if (mpi_p->k != mpi_p->num_proc_z-1)
        {
	    mpi_send_up_z(mpi_p, vf_p);
        }
    }
    return 0;
}

int mpi_exch_up3d_z_1comp(mpi_data* mpi_p,
			  geometry_data* geometry_p,
			  field_n* f_p)
{
    int i,j;
    int index, index2d;
#ifdef USENDF
    int from = f_p->nz-2;
#else
    int from = f_p->nz-1;
#endif

    if (mpi_p->num_proc_z == 1)
    {
        return 0;
    }

    mpi_p->buff_size = 2 * f_p->ny * f_p->nx;

    if (mpi_p->k %2 == 0)
    {
        for (i = 0; i < f_p->nx; i++)
        {
            for (j = 0; j < f_p->ny; j++)
            {
                index2d = INDEX_XY_P(f_p,i,j);
                index   = INDEX_P(f_p,i,j,from);
                mpi_p->sb3d[index2d]          = f_p->a[index];
	    }
        }

        MPI_Send((void*)(mpi_p->sb3d),
                 mpi_p->buff_size,
                 MPI_FP,
                 mpi_p->up_z,
                 0,
                 MPI_COMM_WORLD);

        if (mpi_p->k != 0)
        {
            MPI_Recv((void*)(mpi_p->rb3d),
                     mpi_p->buff_size,
                     MPI_FP,
                     mpi_p->down_z,
                     0,
                     MPI_COMM_WORLD,
                     MPI_STATUS_IGNORE);

            for (i = 0; i < f_p->nx; i++)
            {
                for (j = 0; j < f_p->ny; j++)
                {
                    index2d = INDEX_XY_P(f_p,i,j);
                    index   = INDEX_P(f_p,i,j,0);
                    f_p->a[index] = mpi_p->rb3d[index2d];
		}
            }
        }
    }
    else
    {
        MPI_Recv((void*)(mpi_p->rb3d),
                 mpi_p->buff_size,
                 MPI_FP,
                 mpi_p->down_z,
                 0,
                 MPI_COMM_WORLD,
                 MPI_STATUS_IGNORE);

        for (i = 0; i < f_p->nx; i++)
        {
            for (j = 0; j < f_p->ny; j++)
            {
                index2d = INDEX_XY_P(f_p,i,j);
                index   = INDEX_P(f_p,i,j,0);
                f_p->a[index] = mpi_p->rb3d[index2d];
	    }
        }

        if (mpi_p->k != mpi_p->num_proc_z-1)
        {
            for (i = 0; i < f_p->nx; i++)
            {
                for (j = 0; j < f_p->ny; j++)
                {
                    index2d = INDEX_XY_P(f_p,i,j);
                    index   = INDEX_P(f_p,i,j,from);
                    mpi_p->sb3d[index2d]          = f_p->a[index];
		}
            }
            MPI_Send((void*)(mpi_p->sb3d),
                     mpi_p->buff_size,
                     MPI_FP,
                     mpi_p->up_z,
                     0,
                     MPI_COMM_WORLD);
        }
    }
    return 0;
}

/**
   fields fx_p, fy_p, fz_p are assumed to have the same dimensions in x, y, z directions,
   so everywhere information from fx_p is used.
 */
int  mpi_exch_up3d_z_2comp_a(mpi_data* mpi_p,
                             geometry_data* geometry_p,
                             field_n* fx_p,
                             field_n* fy_p,
                             field_n* fz_p)
{
    int i,j;
    int index, index2d;

    if (mpi_p->num_proc_z == 1)
    {
        return 0;
    }

    int offset = fx_p->ny * fx_p->nx;
    int offset2 = 2 * offset;

    if (mpi_p->k %2 == 0)
    {
        for (i = 0; i < fx_p->nx; i++)
        {
            for (j = 0; j < fx_p->ny; j++)
            {
                index2d = INDEX_XY_P(fx_p,i,j);
                index   = INDEX_P(fx_p,i,j,fx_p->nz-1);

                mpi_p->sb3d_z[index2d]          = fx_p->a[index];
                mpi_p->sb3d_z[offset+index2d]   = fy_p->a[index];
                mpi_p->sb3d_z[offset2+index2d] = fz_p->a[index];
            }
        }

        MPI_Isend((void*)(mpi_p->sb3d_z),
                  mpi_p->sizez,
                  MPI_FP,
                  mpi_p->up_z,
                  MPI_Z_TAG,
                  MPI_COMM_WORLD,
                  &mpi_p->s_z);

        if (mpi_p->k != 0)
        {
            MPI_Irecv((void*)(mpi_p->rb3d_z),
                      mpi_p->sizez,
                      MPI_FP,
                      mpi_p->down_z,
                      MPI_Z_TAG,
                      MPI_COMM_WORLD,
                      &mpi_p->r_z);
        }
    }
    else
    {
        MPI_Irecv((void*)(mpi_p->rb3d_z),
                  mpi_p->sizez,
                  MPI_FP,
                  mpi_p->down_z,
                  MPI_Z_TAG,
                  MPI_COMM_WORLD,
                  &mpi_p->r_z);

        if (mpi_p->k != mpi_p->num_proc_z-1)
        {
            for (i = 0; i < fx_p->nx; i++)
            {
                for (j = 0; j < fx_p->ny; j++)
                {
                    index2d = INDEX_XY_P(fx_p,i,j);
                    index   = INDEX_P(fx_p,i,j,fx_p->nz-1);

                    mpi_p->sb3d_z[index2d]          = fx_p->a[index];
                    mpi_p->sb3d_z[offset+index2d]   = fy_p->a[index];
                    mpi_p->sb3d_z[offset2+index2d] = fz_p->a[index];
                }
            }
            MPI_Isend((void*)(mpi_p->sb3d_z),
                      mpi_p->sizez,
                      MPI_FP,
                      mpi_p->up_z,
                      MPI_Z_TAG,
                      MPI_COMM_WORLD,
                      &mpi_p->s_z);
        }
    }
    return 0;
}

/**

   fields fx_p, fy_p, fz_p are assumed to have the same dimensions in x, y, z directions,
   so everywhere information from fx_p is used.
 */

int mpi_exch_down3d_x_2comp(mpi_data* mpi_p,
			    geometry_data* geometry_p,
			    vfield_n* vf_p)
{
    if (mpi_p->num_proc_x == 1)
    {
        return 0;
    }
    if (mpi_p->i%2 == 0)
    {
	mpi_recv_down_x(mpi_p, vf_p);

        if (mpi_p->i != 0)
        {
	    mpi_send_down_x(mpi_p, vf_p);
        }
    }
    else
    {
	mpi_send_down_x(mpi_p, vf_p);

        if (mpi_p->i != mpi_p->num_proc_x-1)
        {
	    mpi_recv_down_x(mpi_p, vf_p);
	}
    }
    return 0;
}


int mpi_exch_down3d_x_1comp(mpi_data* mpi_p,
			    geometry_data* geometry_p,
			    field_n* f_p)
{
    int j, k;
    int kmax = geometry_p->kmax+1;
    int index, index2d;
#ifdef USENDF
    int from = 1;
#else
    int from = 0;
#endif

    if (mpi_p->num_proc_x == 1)
    {
        return 0;
    }

    mpi_p->buff_size = f_p->ny * kmax;

    if (mpi_p->i%2 == 0)
    {
        MPI_Recv((void*)(mpi_p->rb3d),
                 mpi_p->buff_size,
                 MPI_FP,
                 mpi_p->up_x,
                 0,
                 MPI_COMM_WORLD,
                 MPI_STATUS_IGNORE);

        for (k = 0; k < kmax; k++)
        {
            for (j = 0; j < f_p->ny; j++)
            {
                index2d = INDEX_YZ_P(f_p,j,k);
                index   = INDEX_P(f_p,f_p->nx-1,j,k);
		f_p->a[index] = mpi_p->rb3d[index2d];
            }
        }

        if (mpi_p->i != 0)
        {
            for (k = 0; k < kmax; k++)
            {
                for (j = 0; j < f_p->ny; j++)
                {
                    index2d = INDEX_YZ_P(f_p,j,k);
                    index   = INDEX_P(f_p,from,j,k);
		    mpi_p->sb3d[index2d]   = f_p->a[index];
                }
            }
            MPI_Send((void*)(mpi_p->sb3d),
                     mpi_p->buff_size,
                     MPI_FP,
                     mpi_p->down_x,
                     0,
                     MPI_COMM_WORLD);
        }
    }
    else
    {
        for (k = 0; k < kmax; k++)
        {
            for (j = 0; j < f_p->ny; j++)
            {
                index2d = INDEX_YZ_P(f_p,j,k);
                index   = INDEX_P(f_p,from,j,k);
		mpi_p->sb3d[index2d]   = f_p->a[index];
            }
        }
        MPI_Send((void*)(mpi_p->sb3d),
                 mpi_p->buff_size,
                 MPI_FP,
                 mpi_p->down_x,
                 0,
                 MPI_COMM_WORLD);

        if (mpi_p->i != mpi_p->num_proc_x-1)
        {
            MPI_Recv((void*)(mpi_p->rb3d),
                     mpi_p->buff_size,
                     MPI_FP,
                     mpi_p->up_x,
                     0,
                     MPI_COMM_WORLD,
                     MPI_STATUS_IGNORE);

            for (k = 0; k < kmax; k++)
            {
                for (j = 0; j < f_p->ny; j++)
                {
                    index2d = INDEX_YZ_P(f_p,j,k);
                    index   = INDEX_P(f_p,f_p->nx-1,j,k);
		    f_p->a[index] = mpi_p->rb3d[index2d];
                }
            }
        }
    }
    return 0;
}
/**

   fields fx_p, fy_p, fz_p are assumed to have the same dimensions in x, y, z directions,
   so everywhere information from fx_p is used.
 */
int  mpi_exch_down3d_x_2comp_a(mpi_data* mpi_p,
                               geometry_data* geometry_p,
                               field_n* fx_p,
                               field_n* fy_p,
                               field_n* fz_p)
{
    int j, k;
    int index, index2d;

    if (mpi_p->num_proc_x == 1)
    {
        return 0;
    }

    int offset = fx_p->ny * fx_p->nz;
    int offset2 = 2 * offset;

    if (mpi_p->i%2 == 0)
    {
        MPI_Irecv((void*)(mpi_p->rb3d_x),
                  mpi_p->sizex,
                  MPI_FP,
                  mpi_p->up_x,
                  MPI_X_TAG,
                  MPI_COMM_WORLD,
                  &mpi_p->r_x);

        if (mpi_p->i != 0)
        {
            for (k = 0; k < fx_p->nz; k++)
            {
                for (j = 0; j < fx_p->ny; j++)
                {
                    index2d = INDEX_YZ_P(fx_p,j,k);
                    index   = INDEX_P(fx_p,0,j,k);

                    mpi_p->sb3d_x[index2d]          = fx_p->a[index];
                    mpi_p->sb3d_x[offset+index2d]   = fy_p->a[index];
                    mpi_p->sb3d_x[offset2+index2d] = fz_p->a[index];
                }
            }
            MPI_Isend((void*)(mpi_p->sb3d_x),
                      mpi_p->sizex,
                      MPI_FP,
                      mpi_p->down_x,
                      MPI_X_TAG,
                      MPI_COMM_WORLD,
                      &mpi_p->s_x);
        }
    }
    else
    {
        for (k = 0; k < fx_p->nz; k++)
        {
            for (j = 0; j < fx_p->ny; j++)
            {
                index2d = INDEX_YZ_P(fx_p,j,k);
                index   = INDEX_P(fx_p,0,j,k);

                mpi_p->sb3d_x[index2d]          = fx_p->a[index];
                mpi_p->sb3d_x[offset+index2d]   = fy_p->a[index];
                mpi_p->sb3d_x[offset2+index2d] = fz_p->a[index];
            }
        }
        MPI_Isend((void*)(mpi_p->sb3d_x),
                  mpi_p->sizex,
                  MPI_FP,
                  mpi_p->down_x,
                  MPI_X_TAG,
                  MPI_COMM_WORLD,
                  &mpi_p->s_x);

        if (mpi_p->i != mpi_p->num_proc_x-1)
        {
            MPI_Irecv((void*)(mpi_p->rb3d_x),
                      mpi_p->sizex,
                      MPI_FP,
                      mpi_p->up_x,
                      MPI_X_TAG,
                      MPI_COMM_WORLD,
                      &mpi_p->r_x);
        }
    }
    return 0;
}

/**

   fields fx_p, fy_p, fz_p are assumed to have the same dimensions in x, y, z directions,
   so everywhere information from fx_p is used.
 */

int  mpi_exch_down3d_y_2comp(mpi_data* mpi_p,
                             geometry_data* geometry_p,
			     vfield_n* vf_p)
{
    if (mpi_p->num_proc_y == 1)
    {
        return 0;
    }

    if (mpi_p->j%2 == 0)
    {
	mpi_recv_down_y(mpi_p, vf_p);

        if (mpi_p->j != 0)
        {
	    mpi_send_down_y(mpi_p, vf_p);
        }
    }
    else
    {
	mpi_send_down_y(mpi_p, vf_p);

        if (mpi_p->j != mpi_p->num_proc_y-1)
        {
	    mpi_recv_down_y(mpi_p, vf_p);
        }
    }
    return 0;
}

int mpi_exch_down3d_y_1comp(mpi_data* mpi_p,
			    geometry_data* geometry_p,
			    field_n* f_p)
{
    int i, k;
    int kmax = geometry_p->kmax+1;
    int index, index2d;
#ifdef USENDF
    int from = 1;
#else
    int from = 0;
#endif

    if (mpi_p->num_proc_y == 1)
    {
        return 0;
    }

    mpi_p->buff_size = f_p->nx * kmax;

    if (mpi_p->j%2 == 0)
    {
        MPI_Recv((void*)(mpi_p->rb3d),
                 mpi_p->buff_size,
                 MPI_FP,
                 mpi_p->up_y,
                 0,
                 MPI_COMM_WORLD,
                 MPI_STATUS_IGNORE);

        for (k = 0; k < kmax; k++)
        {
            for (i = 0; i < f_p->nx; i++)
            {
                index2d = INDEX_XZ_P(f_p,i,k);
                index   = INDEX_P(f_p,i,f_p->ny-1,k);
                f_p->a[index] = mpi_p->rb3d[index2d];
            }
        }

        if (mpi_p->j != 0)
        {
            for (k = 0; k < kmax; k++)
            {
                for (i = 0; i < f_p->nx; i++)
                {
                    index2d = INDEX_XZ_P(f_p,i,k);
                    index   = INDEX_P(f_p,i,from,k);
                    mpi_p->sb3d[index2d]          = f_p->a[index];
                }
            }
            MPI_Send((void*)(mpi_p->sb3d),
                     mpi_p->buff_size,
                     MPI_FP,
                     mpi_p->down_y,
                     0,
                     MPI_COMM_WORLD);
        }
    }
    else
    {
        for (k = 0; k < kmax; k++)
        {
            for (i = 0; i < f_p->nx; i++)
            {
                index2d = INDEX_XZ_P(f_p,i,k);
                index   = INDEX_P(f_p,i,from,k);
                mpi_p->sb3d[index2d]          = f_p->a[index];
            }
        }
        MPI_Send((void*)(mpi_p->sb3d),
                 mpi_p->buff_size,
                 MPI_FP,
                 mpi_p->down_y,
                 0,
                 MPI_COMM_WORLD);

        if (mpi_p->j != mpi_p->num_proc_y-1)
        {
            MPI_Recv((void*)(mpi_p->rb3d),
                     mpi_p->buff_size,
                     MPI_FP,
                     mpi_p->up_y,
                     0,
                     MPI_COMM_WORLD,
                     MPI_STATUS_IGNORE);

            for (k = 0; k < kmax; k++)
            {
                for (i = 0; i < f_p->nx; i++)
                {
                    index2d = INDEX_XZ_P(f_p,i,k);
                    index   = INDEX_P(f_p,i,f_p->ny-1,k);
                    f_p->a[index] = mpi_p->rb3d[index2d];
                }
            }
        }
    }
    return 0;
}

/**

   fields fx_p, fy_p, fz_p are assumed to have the same dimensions in x, y, z directions,
   so everywhere information from fx_p is used.
 */
int  mpi_exch_down3d_y_2comp_a(mpi_data* mpi_p,
                               geometry_data* geometry_p,
                               field_n* fx_p,
                               field_n* fy_p,
                               field_n* fz_p)
{
    int i, k;
    int index, index2d;

    if (mpi_p->num_proc_y == 1)
    {
        return 0;
    }

    int offset = fx_p->nx * fx_p->nz;
    int offset2 = 2 * offset;

    if (mpi_p->j%2 == 0)
    {
        MPI_Irecv((void*)(mpi_p->rb3d_y),
                  mpi_p->sizey,
                  MPI_FP,
                  mpi_p->up_y,
                  MPI_Y_TAG,
                  MPI_COMM_WORLD,
                  &mpi_p->r_y);

        if (mpi_p->j != 0)
        {
            for (k = 0; k < fx_p->nz; k++)
            {
                for (i = 0; i < fx_p->nx; i++)
                {
                    index2d = INDEX_XZ_P(fx_p,i,k);
                    index   = INDEX_P(fx_p,i,0,k);

                    mpi_p->sb3d_y[index2d]          = fx_p->a[index];
                    mpi_p->sb3d_y[offset+index2d]   = fy_p->a[index];
                    mpi_p->sb3d_y[offset2+index2d] = fz_p->a[index];
                }
            }
            MPI_Isend((void*)(mpi_p->sb3d_y),
                      mpi_p->sizey,
                      MPI_FP,
                      mpi_p->down_y,
                      MPI_Y_TAG,
                      MPI_COMM_WORLD,
                      &mpi_p->s_y);
        }
    }
    else
    {
        for (k = 0; k < fx_p->nz; k++)
        {
            for (i = 0; i < fx_p->nx; i++)
            {
                index2d = INDEX_XZ_P(fx_p,i,k);
                index   = INDEX_P(fx_p,i,0,k);

                mpi_p->sb3d_y[index2d]          = fx_p->a[index];
                mpi_p->sb3d_y[offset+index2d]   = fy_p->a[index];
                mpi_p->sb3d_y[offset2+index2d] = fz_p->a[index];
            }
        }
        MPI_Isend((void*)(mpi_p->sb3d_y),
                  mpi_p->sizey,
                  MPI_FP,
                  mpi_p->down_y,
                  MPI_Y_TAG,
                  MPI_COMM_WORLD,
                  &mpi_p->s_y);

        if (mpi_p->j != mpi_p->num_proc_y-1)
        {
            MPI_Irecv((void*)(mpi_p->rb3d_y),
                      mpi_p->sizey,
                      MPI_FP,
                      mpi_p->up_y,
                      MPI_Y_TAG,
                      MPI_COMM_WORLD,
                      &mpi_p->r_y);
        }
    }
    return 0;
}

/**

   fields fx_p, fy_p, fz_p are assumed to have the same dimensions in x, y, z directions,
   so everywhere information from fx_p is used.
 */

int  mpi_exch_down3d_z_2comp(mpi_data* mpi_p,
                             geometry_data* geometry_p,
			     vfield_n* vf_p)
{
    if (mpi_p->num_proc_z == 1)
    {
        return 0;
    }

    if (mpi_p->k%2 == 0)
    {
	mpi_recv_down_z(mpi_p, vf_p);
        if (mpi_p->k != 0)
        {
	    mpi_send_down_z(mpi_p, vf_p);
        }

    }
    else
    {
	mpi_send_down_z(mpi_p, vf_p);
        if (mpi_p->k != mpi_p->num_proc_z-1)
        {
	    mpi_recv_down_z(mpi_p, vf_p);
	}
    }
    return 0;
}

int mpi_exch_down3d_z_1comp(mpi_data* mpi_p,
			    geometry_data* geometry_p,
			    field_n* f_p)
{
    int i,j;
    int index, index2d;
#ifdef USENDF
    int from = 1;
#else
    int from = 0;
#endif

    if (mpi_p->num_proc_z == 1)
    {
        return 0;
    }

    mpi_p->buff_size = f_p->ny * f_p->nx;

    if (mpi_p->k%2 == 0)
    {
        MPI_Recv((void*)(mpi_p->rb3d),
                 mpi_p->buff_size,
                 MPI_FP,
                 mpi_p->up_z,
                 0,
                 MPI_COMM_WORLD,
                 MPI_STATUS_IGNORE);

        for (i = 0; i < f_p->nx; i++)
        {
            for (j = 0; j < f_p->ny; j++)
            {
                index2d = INDEX_XY_P(f_p,i,j);
                index   = INDEX_P(f_p,i,j,f_p->nz-1);
                f_p->a[index] = mpi_p->rb3d[index2d];
            }
        }

        if (mpi_p->k != 0)
        {
            for (i = 0; i < f_p->nx; i++)
            {
                for (j = 0; j < f_p->ny; j++)
                {
                    index2d = INDEX_XY_P(f_p,i,j);
                    index   = INDEX_P(f_p,i,j,from);
                    mpi_p->sb3d[index2d]          = f_p->a[index];
                }
            }
            MPI_Send((void*)(mpi_p->sb3d),
                     mpi_p->buff_size,
                     MPI_FP,
                     mpi_p->down_z,
                     0,
                     MPI_COMM_WORLD);
        }

    }
    else
    {
        for (i = 0; i < f_p->nx; i++)
        {
            for (j = 0; j < f_p->ny; j++)
            {
                index2d = INDEX_XY_P(f_p,i,j);
                index   = INDEX_P(f_p,i,j,from);
                mpi_p->sb3d[index2d]          = f_p->a[index];
	    }
        }

        MPI_Send((void*)(mpi_p->sb3d),
                 mpi_p->buff_size,
                 MPI_FP,
                 mpi_p->down_z,
                 0,
                 MPI_COMM_WORLD);

        if (mpi_p->k != mpi_p->num_proc_z-1)
        {
            MPI_Recv((void*)(mpi_p->rb3d),
                     mpi_p->buff_size,
                     MPI_FP,
                     mpi_p->up_z,
                     0,
                     MPI_COMM_WORLD,
                     MPI_STATUS_IGNORE);

            for (i = 0; i < f_p->nx; i++)
            {
                for (j = 0; j < f_p->ny; j++)
                {
                    index2d = INDEX_XY_P(f_p,i,j);
                    index   = INDEX_P(f_p,i,j,f_p->nz-1);
                    f_p->a[index] = mpi_p->rb3d[index2d];
		}
            }
        }
    }
    return 0;
}

/**

   fields fx_p, fy_p, fz_p are assumed to have the same dimensions in x, y, z directions,
   so everywhere information from fx_p is used.
 */
int  mpi_exch_down3d_z_2comp_a(mpi_data* mpi_p,
                               geometry_data* geometry_p,
                               field_n* fx_p,
                               field_n* fy_p,
                               field_n* fz_p)
{
    int i,j;
    int index, index2d;

    if (mpi_p->num_proc_z == 1)
    {
        return 0;
    }

    int offset = fx_p->ny * fx_p->nx;
    int offset2 = 2 * offset;

    if (mpi_p->k%2 == 0)
    {
        MPI_Irecv((void*)(mpi_p->rb3d_z),
                  mpi_p->sizez,
                  MPI_FP,
                  mpi_p->up_z,
                  MPI_Z_TAG,
                  MPI_COMM_WORLD,
                  &mpi_p->r_z);

        if (mpi_p->k != 0)
        {
            for (i = 0; i < fx_p->nx; i++)
            {
                for (j = 0; j < fx_p->ny; j++)
                {
                    index2d = INDEX_XY_P(fx_p,i,j);
                    index   = INDEX_P(fx_p,i,j,0);
                    mpi_p->sb3d_z[index2d]          = fx_p->a[index];
                    mpi_p->sb3d_z[offset+index2d]   = fy_p->a[index];
                    mpi_p->sb3d_z[offset2+index2d] = fz_p->a[index];
                }
            }
            MPI_Isend((void*)(mpi_p->sb3d_z),
                      mpi_p->sizez,
                      MPI_FP,
                      mpi_p->down_z,
                      MPI_Z_TAG,
                      MPI_COMM_WORLD,
                      &mpi_p->s_z);
        }

    }
    else
    {
        for (i = 0; i < fx_p->nx; i++)
        {
            for (j = 0; j < fx_p->ny; j++)
            {
                index2d = INDEX_XY_P(fx_p,i,j);
                index   = INDEX_P(fx_p,i,j,0);
                mpi_p->sb3d_z[index2d]          = fx_p->a[index];
                mpi_p->sb3d_z[offset+index2d]   = fy_p->a[index];
                mpi_p->sb3d_z[offset2+index2d] = fz_p->a[index];
            }
        }

        MPI_Isend((void*)(mpi_p->sb3d_z),
                  mpi_p->sizez,
                  MPI_FP,
                  mpi_p->down_z,
                  MPI_Z_TAG,
                  MPI_COMM_WORLD,
                  &mpi_p->s_z);

        if (mpi_p->k != mpi_p->num_proc_z-1)
        {
            MPI_Irecv((void*)(mpi_p->rb3d_z),
                      mpi_p->sizez,
                      MPI_FP,
                      mpi_p->up_z,
                      MPI_Z_TAG,
                      MPI_COMM_WORLD,
                      &mpi_p->r_z);
        }
    }
    return 0;
}

int  mpi_exch_down2d_z(mpi_data* mpi_p, geometry_data* geometry_p, field* f_p)
{
    int i;
    if (mpi_p->k%2 == 0)
    {
        MPI_Recv((void*)mpi_p->recvbuffer,
                 geometry_p->x_max,
                 MPI_FP,
                 mpi_p->up_z,
                 0,
                 MPI_COMM_WORLD,
                 MPI_STATUS_IGNORE);
        for (i = 0; i < geometry_p->x_max; i++)
        {
            f_p->a[INDEX2D_P(f_p, i, geometry_p->z_max-1)] =
                mpi_p->recvbuffer[i];
        }

        if (mpi_p->k != 0)
        {
            for (i = 0; i < geometry_p->x_max; i++)
            {
                mpi_p->sendbuffer[i] = f_p->a[INDEX2D_P(f_p, i, 0)];
            }
            MPI_Send((void*)mpi_p->sendbuffer,
                     geometry_p->x_max,
                     MPI_FP,
                     mpi_p->down_z,
                     0,
                     MPI_COMM_WORLD);
        }
    }
    else
    {
        for (i = 0; i < geometry_p->x_max; i++)
        {
            mpi_p->sendbuffer[i] = f_p->a[INDEX2D_P(f_p, i, 0)];
        }
        MPI_Send((void*)mpi_p->sendbuffer,
                 geometry_p->x_max,
                 MPI_FP,
                 mpi_p->down_z,
                 0,
                 MPI_COMM_WORLD);

        if (mpi_p->k != mpi_p->num_proc_z-1)
        {

            MPI_Recv((void*)mpi_p->recvbuffer,
                     geometry_p->x_max,
                     MPI_FP,
                     mpi_p->up_z,
                     0,
                     MPI_COMM_WORLD,
                     MPI_STATUS_IGNORE);

            for (i = 0; i < geometry_p->x_max; i++)
            {
                f_p->a[INDEX2D_P(f_p, i, geometry_p->z_max-1)] =
                    mpi_p->recvbuffer[i];
            }
        }
    }
    return 0;
}

int  mpi_exch_down2d_x(mpi_data* mpi_p, geometry_data* geometry_p, field* f_p)
{
    int k;
    if (mpi_p->i%2 == 0)
    {
        MPI_Recv((void*)mpi_p->recvbuffer,
                 geometry_p->z_max,
                 MPI_FP,
                 mpi_p->up_x,
                 0,
                 MPI_COMM_WORLD,
                 MPI_STATUS_IGNORE);
        for (k = 0; k < geometry_p->z_max; k++)
        {
            f_p->a[INDEX2D_P(f_p, geometry_p->x_max-1, k)] =
                mpi_p->recvbuffer[k];
        }

        if (mpi_p->i != 0)
        {
            for (k = 0; k < geometry_p->z_max; k++)
            {
                mpi_p->sendbuffer[k] = f_p->a[INDEX2D_P(f_p, 0, k)];
            }
            MPI_Send((void*)mpi_p->sendbuffer,
                     geometry_p->z_max,
                     MPI_FP,
                     mpi_p->down_x,
                     0,
                     MPI_COMM_WORLD);
        }
    }
    else
    {
        for (k = 0; k < geometry_p->z_max; k++)
        {
            mpi_p->sendbuffer[k] = f_p->a[INDEX2D_P(f_p, 0, k)];
        }
        MPI_Send((void*)mpi_p->sendbuffer,
                 geometry_p->z_max,
                 MPI_FP,
                 mpi_p->down_x,
                 0,
                 MPI_COMM_WORLD);

        if (mpi_p->i != mpi_p->num_proc_x-1)
        {
            MPI_Recv((void*)mpi_p->recvbuffer,
                     geometry_p->z_max,
                     MPI_FP,
                     mpi_p->up_x,
                     0,
                     MPI_COMM_WORLD,
                     MPI_STATUS_IGNORE);
            for (k = 0; k < geometry_p->z_max; k++)
            {
                f_p->a[INDEX2D_P(f_p, geometry_p->x_max-1, k)] =
                    mpi_p->recvbuffer[k];
            }
        }
    }
    return 0;
}
#endif

void init_constants(scheme_constants* sch_p,
                    geometry_data* g_p,
                    time_scheme* t_p)
{
    sch_p->dt_dx = t_p->delta_t/g_p->delta_x;
    sch_p->dt_dy = t_p->delta_t/g_p->delta_y;
    sch_p->dt_dz = t_p->delta_t/g_p->delta_z;
    sch_p->dt_4_pi = t_p->delta_t * 4. * M_PI;

    sch_p->dt_ff_4_pi = t_p->delta_t*t_p->freq*t_p->freq/(4.*M_PI);
    sch_p->dx_4_pi = 1./(4.*M_PI*g_p->delta_x);
    sch_p->dy_4_pi = 1./(4.*M_PI*g_p->delta_y);
    sch_p->dz_4_pi = 1./(4.*M_PI*g_p->delta_z);
    sch_p->dxy_4_pi = 1./(4.*M_PI*g_p->delta_x*g_p->delta_y);

    if (g_p->gtype == THREED_e)
    {
#ifdef USENDF
	RANK_S(0)
        fprintf(stderr, "NDF scheme is used. AA is off\n");
        RANK_F(0)

	FP k = 1.25*(sqrt(3.) - 1)/(2.*sqrt(3.));

	sch_p->b_z = 1 - k;
	sch_p->a_z = 0.5*(1 - sch_p->b_z);

	sch_p->b_y = 1 - k * (g_p->delta_z * g_p->delta_z)/(g_p->delta_y * g_p->delta_y);
	sch_p->a_y = 0.5*(1 - sch_p->b_y);

	sch_p->b_x = 1 - k * (g_p->delta_z * g_p->delta_z)/(g_p->delta_x * g_p->delta_x);
	sch_p->a_x = 0.5*(1 - sch_p->b_x);

	g_p->vf = 1.;
#else
        FP R = t_p->lambda/sqrt(g_p->delta_x * g_p->delta_x +
                                    g_p->delta_y * g_p->delta_y +
                                    g_p->delta_z * g_p->delta_z);
        FP q = t_p->delta_t*sqrt(1./(g_p->delta_x * g_p->delta_x) +
                                     1./(g_p->delta_y * g_p->delta_y) +
                                     1./(g_p->delta_z * g_p->delta_z));

        FP amax = M_PI/(3.*R*asin(sin(M_PI/(R*sqrt(3.)))/sqrt(3.)));
        FP zy = g_p->delta_x/g_p->delta_y;
        FP zz = g_p->delta_x/g_p->delta_z;
        FP q_2 = (amax - 1)*0.5;

        FP K1 = M_PI/(R*sqrt(1 + 1/(zy*zy) + 1/(zz*zz)));
        FP a = sin(K1/(1 - q_2))*sin(K1/(1 - q_2))/
                   (zy*zy*sin(K1/((1 - q_2)*zy))*sin(K1/((1 -q_2)*zy)));

        FP b = sin(K1/(1 - q_2))*sin(K1/(1 - q_2))/
                   (zz*zz*sin(K1/((1 - q_2)*zz))*sin(K1/((1 -q_2)*zz)));

        FP K2 = sqrt(1 + a*zy*zy + b*zz*zz);
        FP eps_x = K2/(K1*q*sqrt(a*b))*asin(q*sin(K1/(1 - q_2))/K2);
        FP eps_y = a*eps_x;
        FP eps_z = b*eps_x;

	sch_p->dc_x = 1./eps_x;
        sch_p->dc_y = 1./eps_y;
        sch_p->dc_z = 1./eps_z;

	g_p->eps_x = eps_x;
	g_p->eps_y = eps_y;
	g_p->eps_z = eps_z;

	if (g_p->vf == 0.)
	{
	    FP vf = 2. / g_p->delta_z *
		asin(sqrt(eps_y*eps_x)*g_p->delta_z/t_p->delta_t * sin(0.5 * t_p->freq * t_p->delta_t))/t_p->freq;

	    g_p->vf = 1./vf;
	}

        RANK_S(0)
	    fprintf(stderr, "Artificial anysotropy coefficients: dc_x=%lf dc_y=%lf dc_z=%lf vf=%lf\n",
		    sch_p->dc_x, sch_p->dc_y, sch_p->dc_z, g_p->vf);
        RANK_F(0)
#endif

        sch_p->cx_hz = sch_p->dc_x*sch_p->dt_dy;
        sch_p->cx_hy = sch_p->dc_x*sch_p->dt_dz;

        sch_p->cy_hx = sch_p->dc_y*sch_p->dt_dz;
        sch_p->cy_hz = sch_p->dc_y*sch_p->dt_dx;

        sch_p->cz_hx = sch_p->dc_z*sch_p->dt_dy;
        sch_p->cz_hy = sch_p->dc_z*sch_p->dt_dx;
    }

#ifdef CUDA
    cuda_init_constants(sch_p);
#endif
}

int store_field_n_3comp(field_n* fx_p,
                        field_n* fy_p,
                        field_n* fz_p,
                        const char*name,
                        int t,
                        geometry_data* geometry_p,
                        const char* pathPrefix)
{
    char temp_name[1024];
    gzFile out;

#ifdef USEMPI
    sprintf(temp_name, "%s/data/%s_%06d_%d_%d_%d.dat.gz",
            pathPrefix,
            name,
            t,
            mpi.i,
            mpi.j,
            mpi.k);
#else
    sprintf(temp_name, "%s/data/%s_%06d.dat.gz", pathPrefix, name, t);
    fprintf(stderr, "Filename: %s\n", temp_name);
#endif

    if (Z_NULL == (out=gzopen(temp_name, "w")))
    {
        fprintf(stderr, "Error opening file: %s\n", temp_name);
        return -1;
    }

    gzprintf(out, "# %d %d %d %lf %lf %lf\n",
             geometry_p->x_full,
             geometry_p->y_full,
             geometry_p->z_full,
             geometry_p->delta_x,
             geometry_p->delta_y,
             geometry_p->delta_z);

    for (register int i=0; i<geometry_p->x_max-1; i++)
    {
        for (register int j=0; j<geometry_p->y_max-1; j++)
        {
            for (register int k=0; k<geometry_p->z_max-1; k++)
            {
                if (0. != fx_p->a[INDEX_P(fx_p,i,j,k)] ||
                        0. != fy_p->a[INDEX_P(fy_p,i,j,k)] ||
                        0. != fz_p->a[INDEX_P(fz_p,i,j,k)])
                {
                    gzprintf(out, "%d %d %d %le %le %le\n",
                             i+1+geometry_p->i_offset,
                             j+1+geometry_p->j_offset,
                             k+1+geometry_p->k_offset,
                             fx_p->a[INDEX_P(fx_p,i,j,k)],
                             fy_p->a[INDEX_P(fy_p,i,j,k)],
                             fz_p->a[INDEX_P(fz_p,i,j,k)]);
                }
            }
        }
    }

    gzclose(out);
    return 0;
}

int store_field_3comp(field* fx_p,
                      field* fy_p,
                      field* fz_p,
                      const char*name,
                      int t,
                      geometry_data* geometry_p,
                      const char* pathPrefix)
{
    char temp_name[1024];
    gzFile out;

#ifdef USEMPI
    sprintf(temp_name, "%s/data/%s_%06d_%d_%d_%d.dat.gz",
            pathPrefix,
            name,
            t,
            mpi.i,
            mpi.j,
            mpi.k);
#else
    sprintf(temp_name, "%s/data/%s_%06d.dat.gz", pathPrefix, name, t);
    fprintf(stderr, "Filename: %s\n", temp_name);
#endif

    if (Z_NULL == (out=gzopen(temp_name, "w")))
    {
        fprintf(stderr, "Error opening file: %s\n", temp_name);
        return -1;
    }

    gzprintf(out, "# %d %d %d %lf %lf %lf\n",
             geometry_p->x_full,
             geometry_p->y_full,
             geometry_p->z_full,
             geometry_p->delta_x,
             geometry_p->delta_y,
             geometry_p->delta_z);

    for (register int i=0; i<geometry_p->x_max-1; i++)
    {
        for (register int j=0; j<geometry_p->y_max-1; j++)
        {
            for (register int k=0; k<geometry_p->z_max-1; k++)
            {
                if (0. != fx_p->a[INDEX_P(fx_p,i,j,k)] ||
                        0. != fy_p->a[INDEX_P(fy_p,i,j,k)] ||
                        0. != fz_p->a[INDEX_P(fz_p,i,j,k)])
                {
                    gzprintf(out, "%d %d %d %le %le %le\n",
                             i+1+geometry_p->i_offset,
                             j+1+geometry_p->j_offset,
                             k+1+geometry_p->k_offset,
                             fx_p->a[INDEX_P(fx_p,i,j,k)],
                             fy_p->a[INDEX_P(fy_p,i,j,k)],
                             fz_p->a[INDEX_P(fz_p,i,j,k)]);
                }
            }
        }
    }

    gzclose(out);
    return 0;
}

int store_field_n(field_n* fx_p,
                  const char*name,
                  int t,
                  geometry_data* geometry_p,
                  const char* pathPrefix)
{
    char temp_name[1024];
    gzFile out;

#ifdef USEMPI
    sprintf(temp_name, "%s/data/%s_%06d_%d_%d_%d.dat.gz",
            pathPrefix,
            name,
            t,
            mpi.i,
            mpi.j,
            mpi.k);
#else
    sprintf(temp_name, "%s/data/%s_%06d.dat.gz", pathPrefix, name, t);
    fprintf(stderr, "Filename: %s\n", temp_name);
#endif

    if (Z_NULL == (out=gzopen(temp_name, "w")))
    {
        fprintf(stderr, "Error opening file: %s\n", temp_name);
        return -1;
    }

    gzprintf(out, "# %d %d %d %lf %lf %lf\n",
             geometry_p->x_full,
             geometry_p->y_full,
             geometry_p->z_full,
             geometry_p->delta_x,
             geometry_p->delta_y,
             geometry_p->delta_z);

    for (register int i=0; i<geometry_p->x_max-1; i++)
    {
        for (register int j=0; j<geometry_p->y_max-1; j++)
        {
            for (register int k=0; k<geometry_p->z_max-1; k++)
            {
                if (0. != fx_p->a[INDEX_P(fx_p,i,j,k)])
                {
                    gzprintf(out, "%d %d %d %le\n",
                             i+1+geometry_p->i_offset,
                             j+1+geometry_p->j_offset,
                             k+1+geometry_p->k_offset,
                             fx_p->a[INDEX_P(fx_p,i,j,k)]);
                }
            }
        }
    }

    gzclose(out);
    return 0;
}

int store_field(field* fx_p,
                const char*name,
                int t,
                geometry_data* geometry_p,
                const char* pathPrefix)
{
    char temp_name[1024];
    gzFile out;

#ifdef USEMPI
    sprintf(temp_name, "%s/%s_%06d_%d_%d_%d.dat.gz",
            pathPrefix,
            name,
            t,
            mpi.i,
            mpi.j,
            mpi.k);
#else
    sprintf(temp_name, "%s/%s_%06d.dat.gz", pathPrefix, name, t);
    fprintf(stderr, "Filename: %s\n", temp_name);
#endif

    if (Z_NULL == (out=gzopen(temp_name, "w")))
    {
        fprintf(stderr, "Error opening file: %s\n", temp_name);
        return -1;
    }

    gzprintf(out, "# %d %d %d %lf %lf %lf\n",
             geometry_p->x_full,
             geometry_p->y_full,
             geometry_p->z_full,
             geometry_p->delta_x,
             geometry_p->delta_y,
             geometry_p->delta_z);

    for (register int i=0; i<geometry_p->x_max-1; i++)
    {
        for (register int j=0; j<geometry_p->y_max-1; j++)
        {
            for (register int k=0; k<geometry_p->z_max-1; k++)
            {
                if (0. != fx_p->a[INDEX_P(fx_p,i,j,k)])
                {
                    gzprintf(out, "%d %d %d %le\n",
                             i+1+geometry_p->i_offset,
                             j+1+geometry_p->j_offset,
                             k+1+geometry_p->k_offset,
                             fx_p->a[INDEX_P(fx_p,i,j,k)]);
                }
            }
        }
    }

    gzclose(out);
    return 0;
}

int store_slice_n_3comp(field_n* fx_p,
                        field_n* fy_p,
                        field_n* fz_p,
                        const char*name,
                        char dir,
                        int slice,
                        int t,
                        geometry_data* geometry_p,
                        const char* pathPrefix)
{
    char temp_name[1024];
    gzFile out;
    MPI_BARRIER(MPI_COMM_WORLD);

#ifdef USEMPI
    if('x' == dir)
    {
        if (geometry_p->i_offset > slice ||  fx_p->nx-1 + geometry_p->i_offset <= slice)
        {
            return 0;
        }
        else
        {
            if (mpi.num_proc_y==1 && mpi.num_proc_z==1)
            {
                sprintf(temp_name, "%s/%s_%c%d_%06d.dat.gz", pathPrefix, name, dir, slice, t);
                fprintf(stderr, "Filename: %s\n", temp_name);
            }
            else
            {
                sprintf(temp_name, "%s/%s_%c%d_%06d_%d_%d.dat.gz",
                        pathPrefix, name, dir, slice, t, mpi.j, mpi.k);
	    }
        }
    }

    if('y' == dir)
    {
        if (geometry_p->j_offset > slice ||  fy_p->ny-1 + geometry_p->j_offset <= slice)
        {
            return 0;
        }
        else
        {
            if (mpi.num_proc_x==1 && mpi.num_proc_z==1)
            {
                sprintf(temp_name, "%s/%s_%c%d_%06d.dat.gz", pathPrefix, name, dir, slice, t);
                fprintf(stderr, "Filename: %s\n", temp_name);
            }
            else
            {
                sprintf(temp_name, "%s/%s_%c%d_%06d_%d_%d.dat.gz",
                        pathPrefix, name, dir, slice, t, mpi.i, mpi.k);
	    }
        }
    }

    if('z' == dir)
    {
        if (geometry_p->k_offset > slice ||  fz_p->nz-1 + geometry_p->k_offset <= slice)
        {
            return 0;
        }
        else
        {
            if (mpi.num_proc_x==1 && mpi.num_proc_y==1)
            {
                sprintf(temp_name, "%s/%s_%c%d_%06d.dat.gz", pathPrefix, name, dir, slice, t);
                fprintf(stderr, "Filename: %s\n", temp_name);
            }
            else
            {
                sprintf(temp_name, "%s/%s_%c%d_%06d_%d_%d.dat.gz",
                        pathPrefix, name, dir, slice, t, mpi.i, mpi.j);
	    }
        }
    }
#else
    sprintf(temp_name, "%s/%s_%c%d_%06d.dat.gz", pathPrefix, name, dir, slice, t);
    fprintf(stderr, "Filename: %s\n", temp_name);
#endif

    if (Z_NULL == (out = gzopen(temp_name, "w")))
    {
        fprintf(stderr, "Error opening file: %s\n", temp_name);
        return -1;
    }

    if ('x' == dir)
    {
        int i1 = slice-geometry_p->i_offset+1;

        gzprintf(out, "# %d %d %lf %lf\n",
                 geometry_p->y_full,
                 geometry_p->z_full,
                 geometry_p->delta_y,
                 geometry_p->delta_z);

        for (register int j=0; j<geometry_p->y_max-1; j++)
        {
            for (register int k=0; k<geometry_p->z_max-1; k++)
            {

                if (0. != fx_p->a[INDEX_P(fx_p,i1,j,k)] ||
                        0. != fy_p->a[INDEX_P(fy_p,i1,j,k)] ||
                        0. != fz_p->a[INDEX_P(fz_p,i1,j,k)])
                {
                    gzprintf(out, "%d %d %le %le %le %le\n",
                             j+1+geometry_p->j_offset,
                             k+1+geometry_p->k_offset,
                             fx_p->a[INDEX_P(fx_p,i1,j,k)],
                             fy_p->a[INDEX_P(fy_p,i1,j,k)],
                             fz_p->a[INDEX_P(fz_p,i1,j,k)],
                             sqrt(fx_p->a[INDEX_P(fx_p,i1,j,k)]*fx_p->a[INDEX_P(fx_p,i1,j,k)] +
                                  fy_p->a[INDEX_P(fy_p,i1,j,k)]*fy_p->a[INDEX_P(fy_p,i1,j,k)] +
                                  fz_p->a[INDEX_P(fz_p,i1,j,k)]*fz_p->a[INDEX_P(fz_p,i1,j,k)]));
                }
            }
        }
    }

    if ('y' == dir)
    {
        int j1 = slice-geometry_p->j_offset+1;
        gzprintf(out, "# %d %d %lf %lf\n",
                 geometry_p->x_full,
                 geometry_p->z_full,
                 geometry_p->delta_x,
                 geometry_p->delta_z);

        for (register int i=0; i<geometry_p->x_max-1; i++)
        {
            for (register int k=0; k<geometry_p->z_max-1; k++)
            {
                if (0. != fx_p->a[INDEX_P(fx_p,i,j1,k)] ||
                        0. != fy_p->a[INDEX_P(fy_p,i,j1,k)] ||
                        0. != fz_p->a[INDEX_P(fz_p,i,j1,k)])
                {
                    gzprintf(out, "%d %d %le %le %le %le\n",
                             i+1+geometry_p->i_offset,
                             k+1+geometry_p->k_offset,
                             fx_p->a[INDEX_P(fx_p,i,j1,k)],
                             fy_p->a[INDEX_P(fy_p,i,j1,k)],
                             fz_p->a[INDEX_P(fz_p,i,j1,k)],
                             sqrt(fx_p->a[INDEX_P(fx_p,i,j1,k)]*fx_p->a[INDEX_P(fx_p,i,j1,k)] +
                                  fy_p->a[INDEX_P(fy_p,i,j1,k)]*fy_p->a[INDEX_P(fy_p,i,j1,k)] +
                                  fz_p->a[INDEX_P(fz_p,i,j1,k)]*fz_p->a[INDEX_P(fz_p,i,j1,k)]));
                }
            }
        }
    }

    if ('z' == dir)
    {
        int k1 = slice-geometry_p->k_offset+1;
        gzprintf(out, "# %d %d %lf %lf\n",
                 geometry_p->x_full,
                 geometry_p->y_full,
                 geometry_p->delta_x,
                 geometry_p->delta_y);
        for (register int i=0; i<geometry_p->x_max-1; i++)
        {
            for (register int j=0; j<geometry_p->y_max-1; j++)
            {
                if (0. != fx_p->a[INDEX_P(fx_p,i,j,k1)] ||
                        0. != fy_p->a[INDEX_P(fy_p,i,j,k1)] ||
                        0. != fz_p->a[INDEX_P(fz_p,i,j,k1)])
                {
                    gzprintf(out, "%d %d %le %le %le %le\n",
                             i+1+geometry_p->i_offset,
                             j+1+geometry_p->j_offset,
                             fx_p->a[INDEX_P(fx_p,i,j,k1)],
                             fy_p->a[INDEX_P(fy_p,i,j,k1)],
                             fz_p->a[INDEX_P(fz_p,i,j,k1)],
                             sqrt(fx_p->a[INDEX_P(fx_p,i,j,k1)]*fx_p->a[INDEX_P(fx_p,i,j,k1)] +
                                  fy_p->a[INDEX_P(fy_p,i,j,k1)]*fy_p->a[INDEX_P(fy_p,i,j,k1)] +
                                  fz_p->a[INDEX_P(fz_p,i,j,k1)]*fz_p->a[INDEX_P(fz_p,i,j,k1)]));
                }
            }
        }
    }
    gzclose(out);
    return 0;
}

int store_slice_3comp(field* fx_p,
                      field* fy_p,
                      field* fz_p,
                      const char*name,
                      char dir,
                      int slice,
                      int t,
                      geometry_data* geometry_p,
                      const char* pathPrefix)
{
    char temp_name[1024];
    gzFile out;
    MPI_BARRIER(MPI_COMM_WORLD);

#ifdef USEMPI
    if('x' == dir)
    {
        if (geometry_p->i_offset > slice ||  fx_p->nx-1 + geometry_p->i_offset <= slice)
        {
            return 0;
        }
        else
        {
            if (mpi.num_proc_y==1 && mpi.num_proc_z==1)
            {
                sprintf(temp_name, "%s/data/%s_%c%d_%06d.dat.gz", pathPrefix, name, dir, slice, t);
                fprintf(stderr, "Filename: %s\n", temp_name);
            }
            else
            {
                sprintf(temp_name, "%s/data/%s_%c%d_%06d_%d_%d.dat.gz",
                        pathPrefix, name, dir, slice, t, mpi.j, mpi.k);
                //fprintf(stderr, "Filename: %s\n", temp_name);
            }
        }
    }

    if('y' == dir)
    {
        if (geometry_p->j_offset > slice ||  fy_p->ny-1 + geometry_p->j_offset <= slice)
        {
            return 0;
        }
        else
        {
            if (mpi.num_proc_x==1 && mpi.num_proc_z==1)
            {
                sprintf(temp_name, "%s/data/%s_%c%d_%06d.dat.gz", pathPrefix, name, dir, slice, t);
                fprintf(stderr, "Filename: %s\n", temp_name);
            }
            else
            {
                sprintf(temp_name, "%s/data/%s_%c%d_%06d_%d_%d.dat.gz",
                        pathPrefix, name, dir, slice, t, mpi.i, mpi.k);
                //fprintf(stderr, "Filename: %s\n", temp_name);
            }
        }
    }

    if('z' == dir)
    {
        if (geometry_p->k_offset > slice ||  fz_p->nz-1 + geometry_p->k_offset <= slice)
        {
            return 0;
        }
        else
        {
            if (mpi.num_proc_x==1 && mpi.num_proc_y==1)
            {
                sprintf(temp_name, "%s/data/%s_%c%d_%06d.dat.gz", pathPrefix, name, dir, slice, t);
                fprintf(stderr, "Filename: %s\n", temp_name);
            }
            else
            {
                sprintf(temp_name, "%s/data/%s_%c%d_%06d_%d_%d.dat.gz",
                        pathPrefix, name, dir, slice, t, mpi.i, mpi.j);
                //fprintf(stderr, "Filename: %s\n", temp_name);
            }
        }
    }
#else
    sprintf(temp_name, "%s/data/%s_%c%d_%06d.dat.gz", pathPrefix, name, dir, slice, t);
    fprintf(stderr, "Filename: %s\n", temp_name);
#endif

    if (Z_NULL == (out = gzopen(temp_name, "w")))
    {
        fprintf(stderr, "Error opening file: %s\n", temp_name);
        return -1;
    }

    if ('x' == dir)
    {
        int i1 = slice-geometry_p->i_offset+1;

        gzprintf(out, "# %d %d %lf %lf\n",
                 geometry_p->y_full,
                 geometry_p->z_full,
                 geometry_p->delta_y,
                 geometry_p->delta_z);

        for (register int j=0; j<geometry_p->y_max-1; j++)
        {
            for (register int k=0; k<geometry_p->z_max-1; k++)
            {

                if (0. != fx_p->a[INDEX_P(fx_p,i1,j,k)] ||
                        0. != fy_p->a[INDEX_P(fy_p,i1,j,k)] ||
                        0. != fz_p->a[INDEX_P(fz_p,i1,j,k)])
                {
                    gzprintf(out, "%d %d %lf %lf %lf %lf\n",
                             j+1+geometry_p->j_offset,
                             k+1+geometry_p->k_offset,
                             fx_p->a[INDEX_P(fx_p,i1,j,k)],
                             fy_p->a[INDEX_P(fy_p,i1,j,k)],
                             fz_p->a[INDEX_P(fz_p,i1,j,k)],
                             sqrt(fx_p->a[INDEX_P(fx_p,i1,j,k)]*fx_p->a[INDEX_P(fx_p,i1,j,k)] +
                                  fy_p->a[INDEX_P(fy_p,i1,j,k)]*fy_p->a[INDEX_P(fy_p,i1,j,k)] +
                                  fz_p->a[INDEX_P(fz_p,i1,j,k)]*fz_p->a[INDEX_P(fz_p,i1,j,k)]));
                }
            }
        }
    }

    if ('y' == dir)
    {
        int j1 = slice-geometry_p->j_offset+1;
        gzprintf(out, "# %d %d %lf %lf\n",
                 geometry_p->x_full,
                 geometry_p->z_full,
                 geometry_p->delta_x,
                 geometry_p->delta_z);

        for (register int i=0; i<geometry_p->x_max-1; i++)
        {
            for (register int k=0; k<geometry_p->z_max-1; k++)
            {
                if (0. != fx_p->a[INDEX_P(fx_p,i,j1,k)] ||
                        0. != fy_p->a[INDEX_P(fy_p,i,j1,k)] ||
                        0. != fz_p->a[INDEX_P(fz_p,i,j1,k)])
                {
                    gzprintf(out, "%d %d %lf %lf %lf %lf\n",
                             i+1+geometry_p->i_offset,
                             k+1+geometry_p->k_offset,
                             fx_p->a[INDEX_P(fx_p,i,j1,k)],
                             fy_p->a[INDEX_P(fy_p,i,j1,k)],
                             fz_p->a[INDEX_P(fz_p,i,j1,k)],
                             sqrt(fx_p->a[INDEX_P(fx_p,i,j1,k)]*fx_p->a[INDEX_P(fx_p,i,j1,k)] +
                                  fy_p->a[INDEX_P(fy_p,i,j1,k)]*fy_p->a[INDEX_P(fy_p,i,j1,k)] +
                                  fz_p->a[INDEX_P(fz_p,i,j1,k)]*fz_p->a[INDEX_P(fz_p,i,j1,k)]));
                }
            }
        }
    }

    if ('z' == dir)
    {
        int k1 = slice-geometry_p->k_offset+1;
        gzprintf(out, "# %d %d %lf %lf\n",
                 geometry_p->x_full,
                 geometry_p->y_full,
                 geometry_p->delta_x,
                 geometry_p->delta_y);
        for (register int i=0; i<geometry_p->x_max-1; i++)
        {
            for (register int j=0; j<geometry_p->y_max-1; j++)
            {
                if (0. != fx_p->a[INDEX_P(fx_p,i,j,k1)] ||
                        0. != fy_p->a[INDEX_P(fy_p,i,j,k1)] ||
                        0. != fz_p->a[INDEX_P(fz_p,i,j,k1)])
                {
                    gzprintf(out, "%d %d %lf %lf %lf %lf\n",
                             i+1+geometry_p->i_offset,
                             j+1+geometry_p->j_offset,
                             fx_p->a[INDEX_P(fx_p,i,j,k1)],
                             fy_p->a[INDEX_P(fy_p,i,j,k1)],
                             fz_p->a[INDEX_P(fz_p,i,j,k1)],
                             sqrt(fx_p->a[INDEX_P(fx_p,i,j,k1)]*fx_p->a[INDEX_P(fx_p,i,j,k1)] +
                                  fy_p->a[INDEX_P(fy_p,i,j,k1)]*fy_p->a[INDEX_P(fy_p,i,j,k1)] +
                                  fz_p->a[INDEX_P(fz_p,i,j,k1)]*fz_p->a[INDEX_P(fz_p,i,j,k1)]));
                }
            }
        }
    }
    gzclose(out);
    return 0;
}

int store_slice_n(field_n* f_p,
                  char dir,
                  int slice,
                  int t,
                  geometry_data* geometry_p,
                  const char* pathPrefix)
{
    char temp_name[1024];
    FILE* out;
    MPI_BARRIER(MPI_COMM_WORLD);
#ifdef USEMPI
    if('x' == dir)
    {
        if (geometry_p->x_full < slice)
        {
            fprintf(stderr, "Error: slice outside computation area: %d of %d", slice, geometry_p->x_full);
            return -1;
        }

        if (geometry_p->i_offset > slice ||  f_p->nx-1 + geometry_p->i_offset <= slice)
        {
            return 0;
        }
        else
        {
            if (mpi.num_proc_y==1 && mpi.num_proc_z==1)
            {
                sprintf(temp_name, "%s/data/%s_%c%d_%06d.dat", pathPrefix, f_p->name, dir, slice, t);
                fprintf(stderr, "Filename: %s\n", temp_name);
            }
            else
            {
                sprintf(temp_name, "%s/data/%s_%c%d_%06d_%d_%d.dat",
                        pathPrefix, f_p->name, dir, slice, t, mpi.j, mpi.k);
                //fprintf(stderr, "Filename: %s\n", temp_name);
            }
        }
    }

    if('y' == dir)
    {
        if (geometry_p->y_full < slice)
        {
            fprintf(stderr, "Error: slice outside computation area: %d of %d", slice, geometry_p->y_full);
            return -1;
        }

        if (geometry_p->j_offset > slice ||  f_p->ny-1 + geometry_p->j_offset <= slice)
        {
            return 0;
        }
        else
        {
            if (mpi.num_proc_x==1 && mpi.num_proc_z==1)
            {
                sprintf(temp_name, "%s/data/%s_%c%d_%06d.dat", pathPrefix, f_p->name, dir, slice, t);
                fprintf(stderr, "Filename: %s\n", temp_name);
            }
            else
            {
                sprintf(temp_name, "%s/data/%s_%c%d_%06d_%d_%d.dat",
                        pathPrefix, f_p->name, dir, slice, t, mpi.i, mpi.k);
                //fprintf(stderr, "Filename: %s\n", temp_name);
            }
        }
    }

    if('z' == dir)
    {
        if (geometry_p->z_full < slice)
        {
            fprintf(stderr, "Error: slice outside computation area: %d of %d", slice, geometry_p->z_full);
            return -1;
        }

        if (geometry_p->k_offset > slice ||  f_p->nz-1 + geometry_p->k_offset <= slice)
        {
            return 0;
        }
        else
        {
            if (mpi.num_proc_x==1 && mpi.num_proc_y==1)
            {
                sprintf(temp_name, "%s/data/%s_%c%d_%06d.dat", pathPrefix, f_p->name, dir, slice, t);
                fprintf(stderr, "Filename: %s\n", temp_name);
            }
            else
            {
                sprintf(temp_name, "%s/data/%s_%c%d_%06d_%d_%d.dat",
                        pathPrefix, f_p->name, dir, slice, t, mpi.i, mpi.j);
                //fprintf(stderr, "Filename: %s\n", temp_name);
            }
        }
    }
#else
    sprintf(temp_name, "%s/data/%s_%c%d_%06d.dat", pathPrefix, f_p->name, dir, slice, t);
    fprintf(stderr, "Filename: %s\n", temp_name);
#endif


    if (NULL == (out=fopen(temp_name, "w")))
    {
        fprintf(stderr, "Error opening file: %s\n", temp_name);
        return -1;
    }

    if ('x' == dir)
    {
        int i1 = slice-geometry_p->i_offset;

        fprintf(out, "# %d %d %lf %lf\n",
                geometry_p->y_full,
                geometry_p->z_full,
                geometry_p->delta_y,
                geometry_p->delta_z);

        for (register int j=0; j<f_p->ny; j++)
        {
            for (register int k=0; k<f_p->nz; k++)
            {

                if (0. != f_p->a[INDEX_P(f_p,i1,j,k)])
                {
                    fprintf(out, "%d %d %lf %lf %lf\n",
                            j+1+geometry_p->j_offset,
                            k+1+geometry_p->k_offset,
                            f_p->a[INDEX_P(f_p,i1,j,k)],
                            f_p->a[INDEX_P(f_p,i1,j,k)],
                            f_p->a[INDEX_P(f_p,i1,j,k)]);
                }
            }
        }
    }

    if ('y' == dir)
    {
        int j1 = slice-geometry_p->j_offset;
        fprintf(out, "# %d %d %lf %lf\n",
                geometry_p->x_full,
                geometry_p->z_full,
                geometry_p->delta_x,
                geometry_p->delta_z);

        for (register int i=0; i<f_p->nx; i++)
        {
            for (register int k=0; k<f_p->nz; k++)
            {
                if (0. != f_p->a[INDEX_P(f_p,i,j1,k)])
                {
                    fprintf(out, "%d %d %lf %lf %lf\n",
                            i+1+geometry_p->i_offset,
                            k+1+geometry_p->k_offset,
                            f_p->a[INDEX_P(f_p,i,j1,k)],
                            f_p->a[INDEX_P(f_p,i,j1,k)],
                            f_p->a[INDEX_P(f_p,i,j1,k)]);
                }
            }
        }
    }

    if ('z' == dir)
    {
        int k1 = slice-geometry_p->k_offset;
        fprintf(out, "# %d %d %lf %lf\n",
                geometry_p->x_full,
                geometry_p->y_full,
                geometry_p->delta_x,
                geometry_p->delta_y);
        for (register int i=0; i<f_p->nx; i++)
        {
            for (register int j=0; j<f_p->ny; j++)
            {
                if (0. != f_p->a[INDEX_P(f_p,i,j,k1)])
                {
                    fprintf(out, "%d %d %lf %lf %lf\n",
                            i+1+geometry_p->i_offset,
                            j+1+geometry_p->j_offset,
                            f_p->a[INDEX_P(f_p,i,j,k1)],
                            f_p->a[INDEX_P(f_p,i,j,k1)],
                            f_p->a[INDEX_P(f_p,i,j,k1)]);
                }
            }
        }
    }
    fflush(out);
    fclose(out);
    return 0;
}

int store_slice_3d_moving(field* f_p,
                          const char*name,
                          char dir,
                          int slice,
                          int t,
                          int iz,
                          geometry_data* geometry_p,
                          const char* pathPrefix)
{
    char temp_name[1024];
    gzFile out;
    MPI_BARRIER(MPI_COMM_WORLD);
#ifdef USEMPI
    if('x' == dir)
    {
        if (geometry_p->x_full < slice)
        {
            fprintf(stderr, "Error: slice outside computation area: %d of %d", slice, geometry_p->x_full);
            return -1;
        }

        if (geometry_p->i_offset > slice ||  f_p->nx-1 + geometry_p->i_offset <= slice)
        {
            return 0;
        }
        else
        {
            if (mpi.num_proc_y==1 && mpi.num_proc_z==1)
            {
                sprintf(temp_name, "%s/data/moving/%s_%c%d_%06d.dat.gz", pathPrefix, name, dir, slice, t);
                //fprintf(stderr, "Filename: %s\n", temp_name);
            }
            else
            {
                sprintf(temp_name, "%s/data/moving/%s_%c%d_%06d_%d_%d.dat.gz",
                        pathPrefix, name, dir, slice, t, mpi.j, mpi.k);
                //fprintf(stderr, "Filename: %s\n", temp_name);
            }
        }
    }

    if('y' == dir)
    {
        if (geometry_p->y_full < slice)
        {
            fprintf(stderr, "Error: slice outside computation area: %d of %d", slice, geometry_p->y_full);
            return -1;
        }

        if (geometry_p->j_offset > slice ||  f_p->ny-1 + geometry_p->j_offset <= slice)
        {
            return 0;
        }
        else
        {
            if (mpi.num_proc_x==1 && mpi.num_proc_z==1)
            {
                sprintf(temp_name, "%s/data/moving/%s_%c%d_%06d.dat.gz", pathPrefix, name, dir, slice, t);
                //fprintf(stderr, "Filename: %s\n", temp_name);
            }
            else
            {
                sprintf(temp_name, "%s/data/moving/%s_%c%d_%06d_%d_%d.dat.gz",
                        pathPrefix, name, dir, slice, t, mpi.i, mpi.k);
                //fprintf(stderr, "Filename: %s\n", temp_name);
            }
        }
    }

#else
    sprintf(temp_name, "%s/data/moving/%s_%c%d_%06d.dat.gz", pathPrefix, name, dir, slice, t);
    //fprintf(stderr, "Filename: %s\n", temp_name);
#endif


    if (Z_NULL == (out=gzopen(temp_name, "w")))
    {
        fprintf(stderr, "Error opening file: %s\n", temp_name);
        return -1;
    }

    if ('x' == dir)
    {
        int i1 = slice-geometry_p->i_offset;

        gzprintf(out, "# %d %d %lf %lf\n",
                 geometry_p->y_full,
                 geometry_p->move_max,
                 geometry_p->delta_y,
                 geometry_p->delta_z);

        for (register int j=0; j<f_p->ny; j++)
        {
            for (register int k=0; k<iz; k++)
            {

                if (0. != f_p->a[INDEX_P(f_p,i1,j,k)])
                {
                    gzprintf(out, "%d %d %lf %lf %lf\n",
                             j+1+geometry_p->j_offset,
                             k+1+geometry_p->move_index+geometry_p->k_offset,
                             f_p->a[INDEX_P(f_p,i1,j,k)],
                             f_p->a[INDEX_P(f_p,i1,j,k)],
                             f_p->a[INDEX_P(f_p,i1,j,k)]);
                }
            }
        }
    }

    if ('y' == dir)
    {
        int j1 = slice-geometry_p->j_offset;
        gzprintf(out, "# %d %d %lf %lf\n",
                 geometry_p->x_full,
                 geometry_p->move_max,
                 geometry_p->delta_x,
                 geometry_p->delta_z);

        for (register int i=0; i<f_p->nx; i++)
        {
            for (register int k=0; k<iz; k++)
            {
                if (0. != f_p->a[INDEX_P(f_p,i,j1,k)])
                {
                    gzprintf(out, "%d %d %lf %lf %lf\n",
                             i+1+geometry_p->i_offset,
                             k+1+geometry_p->move_index+geometry_p->k_offset,
                             f_p->a[INDEX_P(f_p,i,j1,k)],
                             f_p->a[INDEX_P(f_p,i,j1,k)],
                             f_p->a[INDEX_P(f_p,i,j1,k)]);
                }
            }
        }
    }

    gzclose(out);
    return 0;
}

int store_slice(field* f_p,
                char dir,
                int slice,
                const char* name,
                int t,
                geometry_data* geometry_p,
                const char* pathPrefix)
{
    char temp_name[1024];
    gzFile out;
    MPI_BARRIER(MPI_COMM_WORLD);
#ifdef USEMPI
    if('x' == dir)
    {
        if (geometry_p->x_full < slice)
        {
            fprintf(stderr, "Error: slice outside computation area: %d of %d", slice, geometry_p->x_full);
			fflush(stderr);
            return -1;
        }

        if (geometry_p->i_offset > slice ||  f_p->nx-1 + geometry_p->i_offset <= slice)
        {
            return 0;
        }
        else
        {
            if (mpi.num_proc_y==1 && mpi.num_proc_z==1)
            {
                sprintf(temp_name, "%s/%s_%c%d_%06d.dat.gz", pathPrefix, name, dir, slice, t);
                fprintf(stderr, "Filename: %s\n", temp_name);
				fflush(stderr);
            }
            else
            {
                sprintf(temp_name, "%s/%s_%c%d_%06d_%d_%d.dat.gz",
                        pathPrefix, name, dir, slice, t, mpi.j, mpi.k);
                //fprintf(stderr, "Filename: %s\n", temp_name);
            }
        }
    }

    if('y' == dir)
    {
        if (geometry_p->y_full < slice)
        {
            fprintf(stderr, "Error: slice outside computation area: %d of %d", slice, geometry_p->y_full);
			fflush(stderr);
            return -1;
        }

        if (geometry_p->j_offset > slice ||  f_p->ny-1 + geometry_p->j_offset <= slice)
        {
            return 0;
        }
        else
        {
            if (mpi.num_proc_x==1 && mpi.num_proc_z==1)
            {
                sprintf(temp_name, "%s/%s_%c%d_%06d.dat.gz", pathPrefix, name, dir, slice, t);
                //fprintf(stderr, "Filename: %s\n", temp_name);
            }
            else
            {
                sprintf(temp_name, "%s/%s_%c%d_%06d_%d_%d.dat.gz",
                        pathPrefix, name, dir, slice, t, mpi.i, mpi.k);
                //fprintf(stderr, "Filename: %s\n", temp_name);
            }
        }
    }

    if('z' == dir)
    {
        if (geometry_p->z_full < slice)
        {
            fprintf(stderr, "Error: slice outside computation area: %d of %d", slice, geometry_p->z_full);
			fflush(stderr);
            return -1;
        }

        if (geometry_p->k_offset > slice ||  f_p->nz-1 + geometry_p->k_offset <= slice)
        {
            return 0;
        }
        else
        {
            if (mpi.num_proc_x==1 && mpi.num_proc_y==1)
            {
                sprintf(temp_name, "%s/%s_%c%d_%06d.dat.gz", pathPrefix, name, dir, slice, t);
                //fprintf(stderr, "Filename: %s\n", temp_name);
            }
            else
            {
                sprintf(temp_name, "%s/%s_%c%d_%06d_%d_%d.dat.gz",
                        pathPrefix, name, dir, slice, t, mpi.i, mpi.j);
                //fprintf(stderr, "Filename: %s\n", temp_name);
            }
        }
    }
#else
    sprintf(temp_name, "%s/%s_%c%d_%06d.dat.gz", pathPrefix, name, dir, slice, t);
    fprintf(stderr, "Filename: %s\n", temp_name);
#endif

    if (Z_NULL == (out=gzopen(temp_name, "w")))
    {
        fprintf(stderr, "Error opening file: %s\n", temp_name);
		fflush(stderr);
        return -1;
    }
   
    if ('x' == dir)
    {
        int i1 = slice-geometry_p->i_offset;

        gzprintf(out, "# %d %d %lf %lf\n",
                 geometry_p->y_full,
                 geometry_p->z_full,
                 geometry_p->delta_y,
                 geometry_p->delta_z);

        for (register int j=0; j<f_p->ny; j++)
        {
            for (register int k=0; k<f_p->nz; k++)
            {

                if (0. != f_p->a[INDEX_P(f_p,i1,j,k)])
                {
                    gzprintf(out, "%d %d %lf\n",
                             j+1+geometry_p->j_offset,
                             k+1+geometry_p->k_offset,
                             f_p->a[INDEX_P(f_p,i1,j,k)]);
                }
            }
        }
    }

    if ('y' == dir)
    {
        int j1 = slice-geometry_p->j_offset;
        gzprintf(out, "# %d %d %lf %lf\n",
                 geometry_p->x_full,
                 geometry_p->z_full,
                 geometry_p->delta_x,
                 geometry_p->delta_z);

        for (register int i=0; i<f_p->nx; i++)
        {
            for (register int k=0; k<f_p->nz; k++)
            {
                if (0. != f_p->a[INDEX_P(f_p,i,j1,k)])
                {
                    gzprintf(out, "%d %d %lf\n",
                             i+1+geometry_p->i_offset,
                             k+1+geometry_p->k_offset,
                             f_p->a[INDEX_P(f_p,i,j1,k)]);
                }
            }
        }
    }

    if ('z' == dir)
    {
        int k1 = slice-geometry_p->k_offset;
        gzprintf(out, "# %d %d %lf %lf\n",
                 geometry_p->x_full,
                 geometry_p->y_full,
                 geometry_p->delta_x,
                 geometry_p->delta_y);
        for (register int i=0; i<f_p->nx; i++)
        {
            for (register int j=0; j<f_p->ny; j++)
            {
                if (0. != f_p->a[INDEX_P(f_p,i,j,k1)])
                {
                    gzprintf(out, "%d %d %lf\n",
                             i+1+geometry_p->i_offset,
                             j+1+geometry_p->j_offset,
                             f_p->a[INDEX_P(f_p,i,j,k1)]);
                }
            }
        }
    }

    gzclose(out);
    return 0;
}

int store_slice_2d_moving(save_data* save_data_p,
                          field* f_p,
                          const char*name,
                          int t,
                          int ix,
                          geometry_data* geometry_p,
                          const char* pathPrefix)
{
    char temp_name[1024];
    gzFile out;
    int index;

    MPI_BARRIER(MPI_COMM_WORLD);
#ifdef USEMPI
    if (mpi.num_proc_x==1 && mpi.num_proc_z==1)
    {
        sprintf(temp_name, "%s/data/%s_%06d.dat.gz", pathPrefix, name, t);
    }
    else
    {
        sprintf(temp_name, "%s/data/%s_%06d_%d.dat.gz",
                pathPrefix, name, t, mpi.rank);
    }
#else
    sprintf(temp_name, "%s/data/%s_%06d.dat.gz", pathPrefix, name, t);
#endif

    if (Z_NULL == (out=gzopen(temp_name, "w")))
    {
        fprintf(stderr, "Error opening file: %s\n", temp_name);
        return -1;
    }
    gzprintf(out, "# %d %d %lf %lf\n",
             geometry_p->move_max,
             geometry_p->z_full,
             geometry_p->delta_x,
             geometry_p->delta_z);

    for (register int i=0; i<ix; i++)
    {
        for (register int k=0; k<f_p->nz; k++)
        {
            index = INDEX2D_P(f_p,i,k);
            if (0. != f_p->a[index])
            {
                gzprintf(out, "%d %d %lf\n",
                         i+1+geometry_p->i_offset+geometry_p->move_index,
                         k+1+geometry_p->k_offset,
                         f_p->a[index]);
            }
        }
    }
    gzclose(out);

    return 0;
}

int store_slice_2d(save_data* save_data_p,
                   field* f_p,
                   const char*name,
                   int t,
                   geometry_data* geometry_p,
                   const char* pathPrefix)
{
    if (false == save_data_p->save)
    {
        return 0;
    }

    if (t < save_data_p->start_time || t > save_data_p->end_time)
    {
        return 0;
    }

    if (t % save_data_p->delta_time == 0)
    {
        char temp_name[1024];
        gzFile out;

        MPI_BARRIER(MPI_COMM_WORLD);
#ifdef USEMPI
        if (mpi.num_proc_x==1 && mpi.num_proc_z==1)
        {
            sprintf(temp_name, "%s/data/%s_%06d.dat.gz", pathPrefix, name, t);
        }
        else
        {
            sprintf(temp_name, "%s/data/%s_%06d_%d.dat.gz",
                    pathPrefix, name, t, mpi.rank);
        }
#else
        sprintf(temp_name, "%s/data/%s_%06d.dat.gz", pathPrefix, name, t);
#endif

        if (Z_NULL == (out=gzopen(temp_name, "w")))
        {
            fprintf(stderr, "Error opening file: %s\n", temp_name);
            return -1;
        }
        gzprintf(out, "# %d %d %lf %lf\n",
                 geometry_p->x_full,
                 geometry_p->z_full,
                 geometry_p->delta_x,
                 geometry_p->delta_z);

        for (register int i=0; i<f_p->nx; i++)
        {
            for (register int k=0; k<f_p->nz; k++)
            {
                int index = INDEX2D_P(f_p,i,k);
                if (0. != f_p->a[index])
                {
                    gzprintf(out, "%d %d %lf\n",
                             i+1+geometry_p->i_offset,
                             k+1+geometry_p->k_offset,
                             f_p->a[index]);
                }
            }
        }
        gzclose(out);
    }
    return 0;
}

int check_index(const char* file, int line, void* fld_p,
                int i,
                int j,
                int k)
{
    field* f_p = (field*)fld_p;
    int ans = ((j)+ (f_p->ny)*(i) + (f_p->nx) * (f_p->ny)*(k));
    if (ans >= f_p->nx*f_p->nz*f_p->ny)
    {
        fprintf(stderr, "file: %s line: %d i=%d(%d) j=%d(%d) k=%d(%d)\n",
                file,
                line,
                i,
                f_p->nx,
                j,
                f_p->ny,
                k,
                f_p->nz);
    }
    return ans;
}



