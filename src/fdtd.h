#ifndef __FDTD_H__
#define __FDTD_H__

#ifndef WIN
#include <linux/unistd.h>
#else
#define _CRT_SECURE_NO_WARNINGS
#include <windows.h>
#define _USE_MATH_DEFINES
#endif

#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cstdio>
#include <cmath>
#include <float.h>
#include <string.h>

#include <configFile.h>

#include <time.h>
#include <cstdio>
#include <wctype.h>
#include <zlib.h>
#include <fcntl.h>
#include <sys/stat.h>

#ifdef _OPENMP
#include <omp.h>
#endif

#ifdef USEMPI
#include <mpi.h>
#endif

#ifdef USELOG
#include <trace_log.h>
#endif

#ifdef USELOG

#ifdef USEMPI
#define error0(log,text,code) log_error2(log,text,code,mpi.rank,mpi.size)
#define err_s0(text,code)     log_error_def2(text,code,mpi.rank,mpi.size)
#define warn_s0(text)         log_warning_def2(text,mpi.rank,mpi.size)
#define info_s0(text)         log_info_def2(text,mpi.rank,mpi.size)
#else /* USEMPPI */
#define error0(log,text,code) log_error0(log,text,code)
#define err_s0(text,code)     log_error_def0(text,code)
#define warn_s0(text)         log_warning_def0(text)
#define info_s0(text)         log_info_def0(text)
#endif /* USEMPI */

#define info_text_s(format,...)  char str[4096]; sprintf(str,format,##  __VA_ARGS__); \
    log_text_info_s(str)
#define warning_text_s(format,...)  char str[4096]; sprintf(str,format,## __VA_ARGS__); \
    log_text_warning_s(str)
#define error_text_s(format,...)  char str[4096]; sprintf(str,format,## __VA_ARGS__); \
    log_text_error_s(str)

#else /* USELOG */
#define error0(log,text,code)
#define err_s0(text,code)
#define warn_s0(text)
#define info_s0(text)
#define info_text_s(format,...)     fprintf(stderr,format,## __VA_ARGS__);
#define warning_text_s(format,...)  fprintf(stderr,format,## __VA_ARGS__);
#define error_text_s(format,...)    fprintf(stderr,format,## __VA_ARGS__);
#endif /* USELOG */

#ifdef SINGLE
#define F  f
#define FP float
#define FP_MALLOC(num) malloc_float(__FILE__, __LINE__, (num))
#else
#define F 
#define FP double
#define FP_MALLOC(num) malloc_double(__FILE__, __LINE__, (num))
#endif /* SINGLE */

#ifdef USENDF
#define ADD 0
#else
#define ADD 1
#endif

#define NOT_UPDATED 0.
#define UPDATED     1.

#define NW 10000000
#define DF (NW/10.)
#define MAXSTAGE 8
#define PML_POWER 4.
#define PML_DAMPING 1e-12
#define DROP_REFINE_VOLUME 32
#define MAX_PATH_PREFIX_LENGTH 400
#define MAX_NAME_LENGTH 50
#define MAX_SAVE_FORMAT_LENGTH  200
#define ESTIMATION_MAX_TIME 100
#define NUM_TFSF_3D_POINTS  8
#define NUM_TFSF_2D_POINTS  4

#define N_1ATM 2.69e19 /* sm^-3, number of particles at 1 atm */
#define N_CR   1.7e21 /* sm^-3, wavelength 0.8 mkm */
#define I0     3.61e16 /* W/cm2, atomic intensity  */
#define E_AT   5.14e11 /* V/m, atomic field Ea */
#define T0     3.3333333333e-15 /* s, normalisation */

#define DOUBLE_MALLOC(num) malloc_double(__FILE__, __LINE__, (num))
#define FLOAT_MALLOC(num) malloc_float(__FILE__, __LINE__, (num))
#define INT_MALLOC(num) malloc_int(__FILE__, __LINE__, (num))
#define FIELD_MALLOC(field_p) (field_p)=malloc_field(__FILE__, __LINE__)
#define FIELD_MALLOC_AND_INIT(field_p, nx,ny,nz) (field_p)=malloc_and_init_field(__FILE__, __LINE__,(nx), (ny), (nz))
#define IFIELD_MALLOC_AND_INIT(field_p, nx,ny,nz) (field_p)=malloc_and_init_ifield(__FILE__, __LINE__,(nx), (ny), (nz))

#ifdef USEMPI
#define MAX_TRY 1024
#define MPI_X_TAG 0
#define MPI_Y_TAG 1
#define MPI_Z_TAG 2
#define RANK_S(r)  if(r == mpi.rank) {
#define RANK_F(r)  }
#define INIT_MPI(argc,argv,mpi) init_mpi(argc, argv, mpi)
#define INIT_MPI_3D(argc,argv,mpi) init_mpi(argc, argv, mpi, ct_p)
#define MPI_WAIT_UP_X(fx,fy,fz) mpi_wait_up_x(fx,fy,fz)
#define MPI_WAIT_UP_Y(fx,fy,fz) mpi_wait_up_y(fx,fy,fz)
#define MPI_WAIT_UP_Z(fx,fy,fz) mpi_wait_up_z(fx,fy,fz)
#define MPI_WAIT_DOWN_X(fx,fy,fz) mpi_wait_down_x(fx,fy,fz)
#define MPI_WAIT_DOWN_Y(fx,fy,fz) mpi_wait_down_y(fx,fy,fz)
#define MPI_WAIT_DOWN_Z(fx,fy,fz) mpi_wait_down_z(fx,fy,fz)
#define FINALIZE_MPI() finalize_mpi()
#define MPI_BARRIER(com) MPI_Barrier(com)
#define MPI_INIT_SENDRECV(mpi,geom) init_mpi_sendrecv_buffer(mpi,geom)
#define MPI_INIT_SENDRECV_3D(mpi,geom, D_p, H_p) init_mpi_sendrecv_buffer3d(mpi,geom, D_p, H_p)
#define MPI_DELETE_SENDRECV(mpi) delete_mpi_sendrecvbuffer(mpi)
#define MPI_DELETE_SENDRECV_3D(mpi) delete_mpi_sendrecvbuffer3d(mpi)

#ifndef SEPARATE_SEND
#define MPI_EXCHANGE_UP_3D_X_2COMP(mpi,geom,f) mpi_exch_up3d_x_2comp(mpi,geom,f)
#define MPI_EXCHANGE_UP_3D_Y_2COMP(mpi,geom,f) mpi_exch_up3d_y_2comp(mpi,geom,f)
#define MPI_EXCHANGE_UP_3D_Z_2COMP(mpi,geom,f) mpi_exch_up3d_z_2comp(mpi,geom,f)
#define MPI_EXCHANGE_UP_3D_X_1COMP(mpi,geom,f)
#define MPI_EXCHANGE_UP_3D_Y_1COMP(mpi,geom,f)
#define MPI_EXCHANGE_UP_3D_Z_1COMP(mpi,geom,f)
#define MPI_EXCHANGE_DOWN_3D_X_2COMP(mpi,geom,f) mpi_exch_down3d_x_2comp(mpi,geom,f)
#define MPI_EXCHANGE_DOWN_3D_Y_2COMP(mpi,geom,f) mpi_exch_down3d_y_2comp(mpi,geom,f)
#define MPI_EXCHANGE_DOWN_3D_Z_2COMP(mpi,geom,f) mpi_exch_down3d_z_2comp(mpi,geom,f)
#define MPI_EXCHANGE_DOWN_3D_X_1COMP(mpi,geom,f)
#define MPI_EXCHANGE_DOWN_3D_Y_1COMP(mpi,geom,f)
#define MPI_EXCHANGE_DOWN_3D_Z_1COMP(mpi,geom,f)

#else
#define MPI_EXCHANGE_UP_3D_X_2COMP(mpi,geom,f)
#define MPI_EXCHANGE_UP_3D_Y_2COMP(mpi,geom,f)
#define MPI_EXCHANGE_UP_3D_Z_2COMP(mpi,geom,f)
#define MPI_EXCHANGE_UP_3D_X_1COMP(mpi,geom,f) mpi_exch_up3d_x_1comp(mpi,geom,f)
#define MPI_EXCHANGE_UP_3D_Y_1COMP(mpi,geom,f) mpi_exch_up3d_y_1comp(mpi,geom,f)
#define MPI_EXCHANGE_UP_3D_Z_1COMP(mpi,geom,f) mpi_exch_up3d_z_1comp(mpi,geom,f)
#define MPI_EXCHANGE_DOWN_3D_X_2COMP(mpi,geom,f)
#define MPI_EXCHANGE_DOWN_3D_Y_2COMP(mpi,geom,f)
#define MPI_EXCHANGE_DOWN_3D_Z_2COMP(mpi,geom,f)
#define MPI_EXCHANGE_DOWN_3D_X_1COMP(mpi,geom,f) mpi_exch_down3d_x_1comp(mpi,geom,f)
#define MPI_EXCHANGE_DOWN_3D_Y_1COMP(mpi,geom,f) mpi_exch_down3d_y_1comp(mpi,geom,f)
#define MPI_EXCHANGE_DOWN_3D_Z_1COMP(mpi,geom,f) mpi_exch_down3d_z_1comp(mpi,geom,f)
#endif

#define MPI_EXCHANGE_UP_3D_X_3COMP_A(mpi,geom,fx,fy,fz) mpi_exch_up3d_x_3comp_a(mpi,geom,fx,fy,fz)
#define MPI_EXCHANGE_UP_3D_Y_3COMP_A(mpi,geom,fx,fy,fz) mpi_exch_up3d_y_3comp_a(mpi,geom,fx,fy,fz)
#define MPI_EXCHANGE_UP_3D_Z_3COMP_A(mpi,geom,fx,fy,fz) mpi_exch_up3d_z_3comp_a(mpi,geom,fx,fy,fz)
#define MPI_EXCHANGE_DOWN_3D_X_3COMP_A(mpi,geom,fx,fy,fz) mpi_exch_down3d_x_3comp_a(mpi,geom,fx,fy,fz)
#define MPI_EXCHANGE_DOWN_3D_Y_3COMP_A(mpi,geom,fx,fy,fz) mpi_exch_down3d_y_3comp_a(mpi,geom,fx,fy,fz)
#define MPI_EXCHANGE_DOWN_3D_Z_3COMP_A(mpi,geom,fx,fy,fz) mpi_exch_down3d_z_3comp_a(mpi,geom,fx,fy,fz)
#define MPI_EXCHANGE_UP_2D_X(mpi,geom,field) mpi_exch_up2d_x(mpi,geom,field)
#define MPI_EXCHANGE_DOWN_2D_X(mpi,geom,field) mpi_exch_down2d_x(mpi,geom,field)
#define MPI_EXCHANGE_UP_2D_Z(mpi,geom,field) mpi_exch_up2d_z(mpi,geom,field)
#define MPI_EXCHANGE_DOWN_2D_Z(mpi,geom,field) mpi_exch_down2d_z(mpi,geom,field)

#ifdef SINGLE
#define MPI_FP MPI_FLOAT
#else
#define MPI_FP MPI_DOUBLE
#endif /* SINGLE */

#else
#define RANK_S(r)
#define RANK_F(r)
#define INIT_MPI(argc,argv,mpi)
#define INIT_MPI_3D(argc,argv,mpi)
#define MPI_WAIT_UP_X(fx,fy,fz)
#define MPI_WAIT_UP_Y(fx,fy,fz)
#define MPI_WAIT_UP_Z(fx,fy,fz)
#define MPI_WAIT_DOWN_X(fx,fy,fz)
#define MPI_WAIT_DOWN_Y(fx,fy,fz)
#define MPI_WAIT_DOWN_Z(fx,fy,fz)
#define FINALIZE_MPI()
#define MPI_BARRIER(com)
#define MPI_INIT_SENDRECV(mpi, geom)
#define MPI_DELETE_SENDRECV(mpi)
#define MPI_EXCHANGE(mpi,geom,field)
#define MPI_INIT_SENDRECV_3D(mpi,geom, D_p, H_p)
#define MPI_DELETE_SENDRECV_3D(mpi)
#define MPI_EXCHANGE_UP_3D_X_1COMP(mpi,geom,f)
#define MPI_EXCHANGE_UP_3D_Y_1COMP(mpi,geom,f)
#define MPI_EXCHANGE_UP_3D_Z_1COMP(mpi,geom,f)
#define MPI_EXCHANGE_DOWN_3D_X_1COMP(mpi,geom,f)
#define MPI_EXCHANGE_DOWN_3D_Y_1COMP(mpi,geom,f)
#define MPI_EXCHANGE_DOWN_3D_Z_1COMP(mpi,geom,f)
#define MPI_EXCHANGE_UP_3D_X_2COMP(mpi,geom,f)
#define MPI_EXCHANGE_UP_3D_Y_2COMP(mpi,geom,f)
#define MPI_EXCHANGE_UP_3D_Z_2COMP(mpi,geom,f)
#define MPI_EXCHANGE_UP_3D_X_3COMP_A(mpi,geom,fx,fy,fz)
#define MPI_EXCHANGE_UP_3D_Y_3COMP_A(mpi,geom,fx,fy,fz)
#define MPI_EXCHANGE_UP_3D_Z_3COMP_A(mpi,geom,fx,fy,fz)
#define MPI_EXCHANGE_DOWN_3D_X_2COMP(mpi,geom,f)
#define MPI_EXCHANGE_DOWN_3D_Y_2COMP(mpi,geom,f)
#define MPI_EXCHANGE_DOWN_3D_Z_2COMP(mpi,geom,f)
#define MPI_EXCHANGE_DOWN_3D_X_3COMP_A(mpi,geom,fx,fy,fz)
#define MPI_EXCHANGE_DOWN_3D_Y_3COMP_A(mpi,geom,fx,fy,fz)
#define MPI_EXCHANGE_DOWN_3D_Z_3COMP_A(mpi,geom,fx,fy,fz)
#define MPI_EXCHANGE_UP_2D_X(mpi,geom,field)
#define MPI_EXCHANGE_DOWN_2D_X(mpi,geom,field)
#define MPI_EXCHANGE_UP_2D_Z(mpi,geom,field)
#define MPI_EXCHANGE_DOWN_2D_Z(mpi,geom,field)
#endif

#define INDEX(f,i,j,k)     ((j)+ f.ny*(i) + f.nxy*(k))
#define INDEX_P(f_p,i,j,k) ((j)+ (f_p)->ny*(i) + (f_p)->nxy * (k))
#define DX_P(f_p)          (f_p)->ny
#define DY_P(f_p)          1
#define DZ_P(f_p)          (f_p)->nxy
#define DX3(nx,ny,nz)      (ny)
#define DY3(nx,ny,nz)      1
#define DZ3(nx,ny,nz)      ((nx)*(ny))
//#define INDEX(f,i,j,k)     check_index(__FILE__, __LINE__, (void*)&f, i, j, k)
//#define INDEX_P(f_p,i,j,k) check_index(__FILE__, __LINE__, (void*)f_p, i, j, k)

#define IDX2(i,k,nx,nz) ((k) + (nz)*(i))
#define IDX3(i,j, k, nx, ny, nz) ((j) + (ny)*(i) + (nx)*(ny)*(k))
#define DX2(nx,nz)  (nz)
#define DZ2(nx,nz)  1
#define INDEX2D(f,i,k)     ((k) + f.nz*(i))
#define INDEX2D_P(f_p,i,k) ((k) + (f_p)->nz*(i))
//#define INDEX2D(f,i,k)      check_index(__FILE__, __LINE__, (void*)(&f), i, 0, k)
//#define INDEX2D_P(f_p,i,k) check_index(__FILE__, __LINE__, (void*)f_p, i, 0, k)

#define INDEX_XY(f,i,j) ((j)+ f.ny*(i))
#define INDEX_XY_P(f,i,j) ((j)+ (f->ny)*(i))
#define INDEX_XZ_P(f,i,k) ((i)+ (f->nx)*(k))
#define INDEX_YZ_P(f,j,k) ((j)+ (f->ny)*(k))

typedef enum comp_e {ex_e=0, ey_e=1, ez_e=2, hx_e=3, hy_e=4, hz_e=5, n_e=6} comp;
typedef enum polarization_e {linear_e = 0, circular_e = 1} polarization;
/* Enumeration for visualisation component choice */
typedef enum componet_e {x_e = 0, y_e = 1, z_e = 2, mod_e = 3} component;
typedef enum media_e {h_e = 0, he_e = 1, so_e = 2, mu_e = 3} media_type;
typedef enum boundary_e {xmin_e=0, xmax_e=1,
			 ymin_e=2, ymax_e=3,
			 zmin_e=4, zmax_e=5}boundary;
/** Enum for geometry type */
typedef enum geom_e {TWOD_TE_e, TWOD_TM_e, THREED_e} geometry_type;
typedef enum shape_e {slab_e, drop_e, cap_e} media_shape;
typedef enum point_e {E_e, H_e} point_type;
typedef enum env_e {tanh_e, gauss_e} envelope_type;
typedef enum slab_e {tan_e, par_e, sin_e} slab_type;

/** Simple structure representing point in Cartesian grid */
typedef struct point_tag
{
    FP x; /**< x coordinate */
    FP y; /**< y coordinate */
    FP z; /**< z coordinate */
} point;

void init_point(point* point_p, FP x, FP y, FP z);

double* malloc_double(const char* file, int line, int elements);
float* malloc_float(const char* file, int line, int elements);
int* malloc_int(const char* file, int line, int elements);

typedef struct ifield_tag
{
    int* a;
    int nx;
    int ny;
    int nz;
    int nxy;
} ifield;

int init_ifield3d(ifield* ifield_p,
                  int nx,
                  int ny,
                  int nz);

int zero_ifield3d(ifield* src_p);
void destroy_ifield(ifield* field_p);

typedef struct field_tag
{
    int nx;
    int ny;
    int nz;
    int nxy;
    FP* a;
} field;

field* malloc_field(const char*file, int line);
field* malloc_and_init_field(const char*file, int line, int nx, int ny, int nz);
ifield* malloc_and_init_ifield(const char*file, int line, int nx, int ny, int nz);

int init_field(field* field_p,
               int nx,
               int nz);

int init_field3d(field* field_p,
                 int nx,
                 int ny,
                 int nz);

int copy_field(field* src_p,
               field* dest_p);
int swap_field(field* src_p,
               int num);
int swap_field3d(field* src_p,
                 int num);
int swap_field1d(field* src_p,
                 int num);
int swap_array_3d(FP* a_p, int num, int nx, int ny, int nz);
int zero_field(field* src_p);
int zero_field3d(field* src_p);
void destroy_field(field* field_p);
void free_int(int* fp_p);
void free_fp(FP* fp_p);

typedef struct field_new_tag
{
    int nx;
    int ny;
    int nz;
    int nxy;
    FP* a;
    char name[1024];
} field_n;

int init_field_n(field_n* field_p,
                 const char*name,
                 int nx,
                 int ny,
                 int nz);

void destroy_field_n(field_n* field_p);

int swap_field_n_3d(field_n* src_p,
                    int num);

typedef struct vfield_tag
{
    /* 0 - x, 1 - y, 2 - z */
    field* comp_p[3];
} vfield;

typedef struct vfield_n_tag
{
    /* 0 - x, 1 - y, 2 - z */
    field_n* comp_p[3];
} vfield_n;

vfield_n* init_vfield_n(const char* name, int nx, int ny, int nz);
int destroy_vfield_n(vfield_n* vf_p);
int swap_vfield_n_3d(vfield_n* f_p, int num);
void assemble_vfield(vfield* vfield_p, field* x_p, field* y_p, field* z_p);
void assemble_vfield_n(vfield_n* vfield_p, field_n* x_p, field_n* y_p, field_n* z_p);

int check_index(const char*file, int line, void* f_p,
                int i,
                int j,
                int k);

typedef enum wave_type_tag {plane_e = 0, dipole_e = 1, simple_e = 2} wave_type;

typedef enum tensor_comp_e {t11 = 0, t12 = 1, t13 = 2,
			    t21 = 3, t22 = 4, t23 = 5,
			    t31 = 6, t32 = 7, t33 = 8 } tensor_comp;

typedef struct tensor_data_tag
{
    FP t[9];
} tensor_data;

int init_tensor_data(tensor_data* t_p,
		     FP x, FP y, FP z,
		     FP x_c, FP y_c, FP z_c,
		     FP eps, FP inv_eps);

int print_tensor_data(tensor_data* t_p);

FP calc_ex_from_d(tensor_data* t_p, FP dx, FP dy, FP dz);
FP calc_ey_from_d(tensor_data* t_p, FP dx, FP dy, FP dz);
FP calc_ez_from_d(tensor_data* t_p, FP dx, FP dy, FP dz);

/* geometry */
typedef struct geometry_data_tag
{
    geometry_type gtype;

    /* local size of current computation region */
    int x_max;
    int y_max;
    int z_max;

    /* full size of computation, used in computations
       for parallel implementation (MPI).
       Where possible these values should be used instead of
       x_max, y_max, z_max */
    int x_full;
    int y_full;
    int z_full;

    /* Offset for mPI implementation */
    int k_offset;
    int j_offset;
    int i_offset;

    /* spatial step */
    FP delta_x;
    FP delta_z;
    FP delta_y;

    /* moving none/front/window selector*/
    int moving;

    /* Possibly this vars should be removed */
    int imin;
    int imax;
    int jmin;
    int jmax;
    int kmin;
    int kmax;

    /* Moving window */
    int move;
    int move_index;

    /* this value is used only during saving data */
    int move_max;
    /* phase velocity along z */
    FP vf;
    /* AA coefficients */
    FP eps_x;
    FP eps_y;
    FP eps_z;
} geometry_data;

void init_geometry2d(configTable* ct_p, geometry_data* geometry_data_p);
void init_geometry3d(configTable* ct_p, geometry_data* geometry_data_p);
void init_test_geometry3d(configTable* ct_p, geometry_data* geometry_data_p);

typedef struct decompose_data_tag
{
    int fx_imin;
    int fx_imax;
    int fx_jmin;
    int fx_jmax;
    int fx_kmin;
    int fx_kmax;

    int fy_imin;
    int fy_imax;
    int fy_jmin;
    int fy_jmax;
    int fy_kmin;
    int fy_kmax;

    int fz_imin;
    int fz_imax;
    int fz_jmin;
    int fz_jmax;
    int fz_kmin;
    int fz_kmax;
} decompose_data;

decompose_data* init_decomposition(configTable* fdtdConfig,
                                   geometry_data* g_p);
void update_decomposition(decompose_data* d_p,
                          geometry_data* g_p);
void destroy_decomposition(decompose_data* d_p);

/* time scheme */
typedef struct time_scheme_tag
{
    int t_move;
    FP v_move; /* velocity of moving window, c */
    int t0;
    int t_max;
    int t_start;
    int t_save;
    bool save;
    FP delta_t;
    FP lambda;
    FP freq;
} time_scheme;

void init_time_scheme(configTable* ct_p, time_scheme* time_scheme_p);

typedef struct scheme_constants_tag
{
    FP dt_dx;
    FP dt_dy;
    FP dt_dz;

    FP dt_4_pi;
    FP dt_ff_4_pi;
    FP dx_4_pi;
    FP dxy_4_pi;
    FP dy_4_pi;
    FP dz_4_pi;

    FP dc_x;
    FP dc_y;
    FP dc_z;

    FP cx_hz;
    FP cx_hy;
    FP cy_hx;
    FP cy_hz;
    FP cz_hx;
    FP cz_hy;

    /* NDF scheme coefficients */
    FP a_x;
    FP b_x;
    FP a_y;
    FP b_y;
    FP a_z;
    FP b_z;
} scheme_constants;

void init_constants(scheme_constants* sch_p,
                    geometry_data* geometry_p,
                    time_scheme* timescheme_p);

/* PML related structures and functions */
typedef struct pml_fields_tag {
    field* Exy_y_l;
    field* Exy_y_h;
    field* Exy_z_l;
    field* Exy_z_h;
    field* Exz_y_l;
    field* Exz_y_h;
    field* Exz_z_l;
    field* Exz_z_h;
    field* Eyx_x_l;
    field* Eyx_x_h;
    field* Eyx_z_l;
    field* Eyx_z_h;
    field* Eyz_x_l;
    field* Eyz_x_h;
    field* Eyz_z_l;
    field* Eyz_z_h;
    field* Ezx_x_l;
    field* Ezx_x_h;
    field* Ezx_y_l;
    field* Ezx_y_h;
    field* Ezy_x_l;
    field* Ezy_x_h;
    field* Ezy_y_l;
    field* Ezy_y_h;

    field* Hxy_y_l;
    field* Hxy_y_h;
    field* Hxy_z_l;
    field* Hxy_z_h;
    field* Hxz_y_l;
    field* Hxz_y_h;
    field* Hxz_z_l;
    field* Hxz_z_h;
    field* Hyx_x_l;
    field* Hyx_x_h;
    field* Hyx_z_l;
    field* Hyx_z_h;
    field* Hyz_x_l;
    field* Hyz_x_h;
    field* Hyz_z_l;
    field* Hyz_z_h;
    field* Hzx_x_l;
    field* Hzx_x_h;
    field* Hzx_y_l;
    field* Hzx_y_h;
    field* Hzy_x_l;
    field* Hzy_x_h;
    field* Hzy_y_l;
    field* Hzy_y_h;
} pml_fields;

typedef struct pml_data_tag
{

    int ne;
    int nh;
    int nef;
    int nhf;
    int* idxe;
    int* idxh;
    int* idxef;
    int* idxhf;

    int xmax;
    int ymax;
    int zmax;

    int nex;
    int ney;
    int nez;
    int nhx;
    int nhy;
    int nhz;

    int nexf;
    int neyf;
    int nezf;
    int nhxf;
    int nhyf;
    int nhzf;

    int* idex;
    int* idhx;
    int* idey;
    int* idhy;
    int* idez;
    int* idhz;

    int* idexf;
    int* idhxf;
    int* ideyf;
    int* idhyf;
    int* idezf;
    int* idhzf;

    int nh_m;
    int ne_m;
    int ne1_m;
    int ne2_m;
    int starte_x;
    int ende_x;
    int starte_y;
    int ende_y;
    int starth_x;
    int endh_x;
    int starth_y;
    int endh_y;

    int se_x;
    int se_y;
    int se_z;
    int sh_x;
    int sh_y;
    int sh_z;

    int sl_e;
    int sl_h;

    int pmlx;
    int pmly;
    int pmlz;

    int ex_l;
    int ey_l;
    int ez_l;

    int ex_h;
    int ey_h;
    int ez_h;

    int hx_l;
    int hy_l;
    int hz_l;

    int hx_h;
    int hy_h;
    int hz_h;

    int eoffx_l;
    int eoffy_l;
    int eoffz_l;

    int eoffx_h;
    int eoffy_h;
    int eoffz_h;

    int hoffx_l;
    int hoffy_l;
    int hoffz_l;

    int hoffx_h;
    int hoffy_h;
    int hoffz_h;

    int hpmlx_h;
    int hpmly_h;
    int hpmlz_h;

    int hpmlx_l;
    int hpmly_l;
    int hpmlz_l;

    int epmlx_h;
    int epmly_h;
    int epmlz_h;

    int epmlx_l;
    int epmly_l;
    int epmlz_l;

    FP* c_ex;
    FP* c_hx;
    FP* d_ex;
    FP* d_hx;

    FP* c_ey;
    FP* d_ey;
    FP* c_hy;
    FP* d_hy;

    FP* c_ez;
    FP* d_ez;
    FP* c_hz;
    FP* d_hz;

    FP* aex;
    FP* bex;

    FP* aez;
    FP* bez;

    FP* ahx;
    FP* bhx;

    FP* ahz;
    FP* bhz;

    FP* ahxy;
    FP* bhxy;
    FP* ahxz;
    FP* bhxz;
    FP* ahyx;
    FP* bhyz;
    FP* ahyz;
    FP* bhyx;
    FP* ahzx;
    FP* bhzy;
    FP* ahzy;
    FP* bhzx;

    FP* aexy;
    FP* bexy;
    FP* aeyx;
    FP* beyz;
    FP* aezx;
    FP* bezy;
    FP* aexz;
    FP* bexz;
    FP* aeyz;
    FP* beyx;
    FP* aezy;
    FP* bezx;

    FP* Eyx;
    FP* Eyz;
    FP* Exy;
    FP* Exz;
    FP* Ezx;
    FP* Ezy;

    FP* Hyx;
    FP* Hyz;
    FP* Hxy;
    FP* Hxz;
    FP* Hzx;
    FP* Hzy;
#ifdef CUDA
    FP* cEyx;
    FP* cEyz;
    FP* cExy;
    FP* cExz;
    FP* cEzx;
    FP* cEzy;

    FP* cHyx;
    FP* cHyz;
    FP* cHxy;
    FP* cHxz;
    FP* cHzx;
    FP* cHzy;
#endif
    pml_fields pml_f;

} pml_data;



void init_pml(configTable* ct_p,
              pml_data* pml_p,
              geometry_data* geometry_p,
              time_scheme* timescheme_p,
              scheme_constants* sch_p);

void zero_pml_x(pml_data* pml_p,
                geometry_data* geometry_p,
                scheme_constants* sch_p);

void calc_pml_area_2d(pml_data* pml_p,
                      geometry_data* geometry_p);
void calc_pml_area_3d(pml_data* pml_p,
                      geometry_data* geometry_p);

void zero_pml_z(pml_data* pml_p,
                geometry_data* geometry_p,
                time_scheme* timescheme_p,
		scheme_constants* sch_p);
void clear_pml_z(pml_data* pml_p);

void swap_pml_2d(pml_data* pml_p, geometry_data* geometry_p, int move);
void swap_pml_3d(pml_data* pml_p, int move);
void destroy_pml3d(pml_data* pml_p);
void destroy_pml2d(pml_data* pml_p);

void init_pml_fields_2d(pml_data* pml_p,
                        geometry_data* geometry_p,
                        scheme_constants* sch_p);

void init_pml_fields_3d(pml_data* pml_p,
                        geometry_data* geometry_p,
                        scheme_constants* sch_p);

void reinit_pml_fields_3d(pml_data* pml_p,
			  geometry_data* geometry_p,
			  scheme_constants* sch_p);

void destroy_pml_fields_2d(pml_data* pml_p);
void destroy_pml_fields_3d(pml_data* pml_p);

int swap_pml_array_3d(FP* a_p, int move, int s_x, int max);

/* Point data related functions */
FP space_shape(FP z,
                   FP z0,
                   FP waist);

FP space_shape3d(FP y,
                     FP y0,
                     FP z,
                     FP z0,
                     FP waist);
void zero_array(FP* dst_p, int offset, int max);

struct envelope_data_tag;

typedef FP (*env_func)(struct envelope_data_tag* env_p,
                           FP t);

typedef struct envelope_data_tag
{
    FP duration;
    FP t0;
    FP alpha;
    FP tp;
    env_func f_p;
    envelope_type type;
} envelope_data;

/* Rt sources */
typedef struct pulse_rt_tag
{
    bool threed;
    int y_max;
    int x_max;
    int z_max;
    int plane;
    FP delta_x;
    FP delta_y;
    FP delta_s;
    FP w0;
    FP t0;
    FP alpha;
    FP tp;
    FP tdelay;
    FP amp;
    FP a0; /**< real pulse amplitude at the entrance */
    FP f;
    FP fs;
    FP fc;
    FP lambda;
    FP angle;
    FP x0;
    FP y0;
    FP freq;
    FP power;
    FP energy;
    FP fspot;
    FP focus;
    FP fgain;
    FP fint;
    FP famp;
    FP zr;
    FP p_pcr;
    FP a_opt;
    FP l_sf;
    FP sin_psi;
    FP cos_psi;
    FP* space_shape_array;
    polarization pol;
    field*  space_shape_array_ey;
    field*  space_shape_array_ex;
    envelope_data env;
} pulse_rt_data;

typedef struct pulses_tag
{
    /* full number of pulses */
    int num_pulses;
    pulse_rt_data* pulse_p;
} pulses_rt;

typedef struct tfsf_point_data_tag
{
    FP d;
    FP field;
    FP phase;
} tfsf_point_data;

typedef struct planewave_rt_tag
{
    FP t0;
    FP alpha;
    FP tp;
    FP tdelay;
    FP phi;
    FP psi;
    FP theta;
    FP sin_phi;
    FP cos_phi;
    FP sin_theta;
    FP cos_theta;
    FP sin_psi;
    FP cos_psi;
    FP vf;
    FP kx;
    FP ky;
    FP kz;
    FP ex;
    FP ey;
    FP ez;
    FP hx;
    FP hy;
    FP hz;
    FP exc;
    FP exs;
    FP eyc;
    FP eys;
    FP ezs;
    FP hxc;
    FP hxs;
    FP hyc;
    FP hys;
    FP hzc;
    FP amplitude;
    FP lambda;
    FP freq;
    FP phase; /* phase in radian: 0<phase<2pi*/
    FP chirp;
    FP delta_temp;
    FP energy;
    int point;
    FP* f_pos;
    bool linear;
    envelope_data env;
    wave_type type;
    tfsf_point_data* E_a;
    tfsf_point_data* H_a;
    int num;
} planewave_rt_data;

typedef struct waves_rt_tag
{
    int num_waves;
    planewave_rt_data* planewave_p;
    FP* Etemp;
    FP* Htemp;
    FP* ephase;
    FP* hphase;
    int ntemp;
} waves_rt;

typedef struct sources_rt_data_tag
{

    /* Currents to set pulses */
    /* for 2D */
    FP* pulse_ey_xmin;
    FP* pulse_ey_xmax;
    FP* pulse_ey_zmin;
    FP* pulse_ey_zmax;
    FP* pulse_hz_xmin;
    FP* pulse_hz_xmax;
    FP* pulse_hx_zmin;
    FP* pulse_hx_zmax;

    /* for 3d */
    int pulse_x_min;
    int pulse_x_max;
    int pulse_y_min;
    int pulse_y_max;
    int pulse_z_min;
    int pulse_z_max;

    bool pulse_xmin;
    bool pulse_xmax;
    bool pulse_ymin;
    bool pulse_ymax;
    bool pulse_zmin;
    bool pulse_zmax;

    field* Jex_zmin_p;
    field* Jey_zmin_p;
    field* Jhx_zmin_p;
    field* Jhy_zmin_p;
    field* Jex_zmax_p;
    field* Jey_zmax_p;
    field* Jhx_zmax_p;
    field* Jhy_zmax_p;
    field* Jez_xmin_p;
    field* Jey_xmin_p;
    field* Jhz_xmin_p;
    field* Jhy_xmin_p;
    field* Jez_xmax_p;
    field* Jey_xmax_p;
    field* Jhz_xmax_p;
    field* Jhy_xmax_p;
    field* Jez_ymin_p;
    field* Jex_ymin_p;
    field* Jhz_ymin_p;
    field* Jhx_ymin_p;
    field* Jez_ymax_p;
    field* Jex_ymax_p;
    field* Jhz_ymax_p;
    field* Jhx_ymax_p;
    int* je_zmin_idx;
    int* je_zmax_idx;
    int* jh_zmin_idx;
    int* jh_zmax_idx;
    FP* j_zmin;
    FP* j_zmax;
    int z_num;

    int wz_num;
    int wy_num;
    int wx_num;

    FP* w_xmin;
    FP* w_xmax;
    FP* w_ymin;
    FP* w_ymax;
    FP* w_zmin;
    FP* w_zmax;

    int* we_xmin_idx;
    int* wh_xmin_idx;
    int* we_xmax_idx;
    int* wh_xmax_idx;
    int* we_ymin_idx;
    int* wh_ymin_idx;
    int* we_ymax_idx;
    int* wh_ymax_idx;
    int* we_zmin_idx;
    int* wh_zmin_idx;
    int* we_zmax_idx;
    int* wh_zmax_idx;

    /* Total field/scattered field for plane waves */
    /* 2d*/
    FP* tfsf_ey_xmin;
    FP* tfsf_ey_xmax;
    FP* tfsf_ey_zmin;
    FP* tfsf_ey_zmax;

    FP* tfsf_hz_xmin;
    FP* tfsf_hz_xmax;
    FP* tfsf_hx_zmin;
    FP* tfsf_hx_zmax;

    /* tf-sf 3d*/
    field* tfsf_ey_xmin_p;
    field* tfsf_ez_xmin_p;
    field* tfsf_ey_xmax_p;
    field* tfsf_ez_xmax_p;
    field* tfsf_ex_ymin_p;
    field* tfsf_ez_ymin_p;
    field* tfsf_ex_ymax_p;
    field* tfsf_ez_ymax_p;
    field* tfsf_ey_zmin_p;
    field* tfsf_ex_zmin_p;
    field* tfsf_ey_zmax_p;
    field* tfsf_ex_zmax_p;

    field* tfsf_hy_xmin_p;
    field* tfsf_hz_xmin_p;
    field* tfsf_hy_xmax_p;
    field* tfsf_hz_xmax_p;
    field* tfsf_hx_ymin_p;
    field* tfsf_hz_ymin_p;
    field* tfsf_hx_ymax_p;
    field* tfsf_hz_ymax_p;
    field* tfsf_hy_zmin_p;
    field* tfsf_hx_zmin_p;
    field* tfsf_hy_zmax_p;
    field* tfsf_hx_zmax_p;

    bool tfsf_xmin;
    bool tfsf_xmax;
    bool tfsf_ymin;
    bool tfsf_ymax;
    bool tfsf_zmin;
    bool tfsf_zmax;

    point* vertices;

    int x_min;
    int x_max;
    int y_min;
    int y_max;
    int z_min;
    int z_max;

    int x_off;
    int y_off;
    int z_off;

    int x_w;
    int y_w;
    int z_w;
    int x_w_full;
    int y_w_full;
    int z_w_full;
    int exmax;
    int eymax;
    int ezmax;

    pulses_rt pulses;
    waves_rt waves;
    FP delta_t;
    bool tfsf_enable;
    bool pulse_enable;
} sources_rt_data;

void dump_vfield(vfield_n* vf_p, const char*name, const char* pathPrefix, int t);
void dump_field(field* f_p, const char*name, const char* pathPrefix, int t);
void read_vfield(vfield_n* vf_p, const char*name, const char* pathPrefix, int t);
void read_field(field* f_p, const char*name, const char* pathPrefix, int t);

/* Media related structures and functions */
struct media_data_tag;

typedef FP (*calc_eps_func)(struct media_data_tag* media_p,
                                FP sq_abs,
                                int index);
typedef struct media_data_tag
{
    int ncells;
    /* for media type slab */
    int ilow;
    int ihi;
    int jlow;
    int jhi;
    int klow;
    int khi;

    /* for media type drop */
    int          x_c;
    int          y_c;
    int          z_c;
    FP       r;
    FP       delta_r;

    /* eps for capillar wall*/
    FP       wall_eps;

    int stage;

    field        n_max;  /* neutrals density */

    /* kerr coefficients for ions */
    FP*      kerr_khi;

    /* for PPT formula */
    FP*      ip; /* ionization potential */
    FP*      L;  /* orbital momentum */
    FP*      m;  /* orbital momentum proection */

    FP*      temp;

    field        epsilon;
    FP       density;
    FP       eps;
    FP       khi3;
    FP       n_2;
    FP       n_0;
    FP       pcr;
    FP       f_step;
    FP       N_crit;
    FP       num_atm_N_cr;
    int           kerr;
    calc_eps_func update_eps;
    media_type    type;
    media_shape   media;
    slab_type     sshape;
    bool          recalc;
    bool          smoothing;
    tensor_data*  t_eps;
    ifield        t_eps_idx;

    /* parameters for solid */
    FP       wl;
    FP       damp;
    FP       g0;
    FP       x1;
    FP       nq;
    FP       b;
    FP       b1;
    FP       b2;
    FP       c1;
    FP       c0;
    FP       c_en;
    FP       c_abs;
    FP       c_abs1;
    FP       c_ion;
    FP       en_abs;
    FP       en_field;
    FP       mpidn;
    FP       abs_en;
    FP       l0;
    FP       max_energy;
    int          average;
    FP       alpha;
    FILE*        out;
} media_data;

void init_sources_rt(configTable* ct_p,
                     geometry_data* geometry_p,
                     pml_data* pml_p,
                     sources_rt_data* sources_rt_data_p,
		     vfield_n* e_p,
		     vfield_n* h_p);

void destroy_sources_rt(sources_rt_data* sources_rt_data_p, geometry_data* geometry_p);

void update_sources_rt(sources_rt_data* sources_rt_data_p,
                       geometry_data* geometry_data_p,
                       pml_data* pml_p,
                       int t);

void zero_sources_rt(sources_rt_data* sources_rt_data_p);

FP time_envelope3d(pulse_rt_data* pu_p,
		       FP t,
		       FP x,
		       FP y);

FP pw_time_shape(planewave_rt_data* pw_p,
                     FP t);

void update_sources_rt3d(sources_rt_data* sources_rt_data_p,
                         geometry_data* geometry_data_p,
                         int t);

void print_sources_rt(sources_rt_data* sources_rt_data_p,
                      media_data* media_p,
                      geometry_data* geometry_p,
                      time_scheme* timescheme_p,
                      const char* pathPrefix);

typedef struct one_point_data_3d_tag
{
    int x_pos;
    int y_pos;
    int z_pos;
    int index;
    bool enable;
    bool open;
    int t0;
    FILE* out;
    char  file[1024];
} one_point_data_3d;

typedef struct points_data_3d_tag
{
    int num_points;
    int t_max;
    FP delta_t;
    one_point_data_3d* points_p;
#ifdef CUDA
    FP* h_p;
    FP* d_p;
    int t_last;
#endif
} points_data_3d;

typedef struct one_point_data_2d_tag
{
    int x_pos;
    int z_pos;
    int index;
    bool enable;
    bool open;
    int t0;
    FILE* out;
} one_point_data_2d;

typedef struct points_data_2d_tag
{
    int num_points;
    int t_max;
    FP delta_t;
    one_point_data_2d* points_p;
} points_data_2d;

void init_point_data_3d(configTable* ct_p,
                        points_data_3d* point_data_p,
                        const char* pathPrefix,
                        time_scheme* timescheme_p,
                        geometry_data* geometry_p,
                        vfield_n* ve_p,
                        vfield_n* vh_p);
void init_one_point_3d(int t,
                       points_data_3d* point_data_p,
                       geometry_data* geometry_p,
                       int i,
                       int x,
                       int y,
                       int z,
                       vfield_n* ve_p,
                       vfield_n* vh_p,
		       const char* pathPrefix);
void update_point_data_3d(points_data_3d* point_data_p,
                          geometry_data* geometry_p,
                          int t,
                          vfield_n* ve_p);
void destroy_point_data_3d(points_data_3d* point_data_p);
void destroy_one_point_data_3d(points_data_3d* point_data_p, int i);
void store_point_data_3d(points_data_3d* point_data_p,
                         geometry_data* geometry_p,
                         int t,
                         vfield_n* ve_p,
                         vfield_n* vh_p,
			 media_data* media_p);

void init_point_data_2d(configTable* ct_p,
                        points_data_2d* point_data_p,
                        const char* path_prefix,
                        time_scheme* timescheme_p,
                        geometry_data* geometry_p,
                        field* ve);
void init_one_point_2d(int t,
                       points_data_2d* point_data_p,
                       geometry_data* geometry_p,
                       int i,
                       int x,
                       int z,
                       field* e_p,
                       const char* pathPrefix);
void update_point_data_2d(points_data_2d* point_data_p,
                          geometry_data* geometry_p,
                          int t,
                          field* e_p);
void destroy_point_data_2d(points_data_2d* point_data_p);
void destroy_one_point_data_2d(points_data_2d* point_data_p, int i);
void store_point_data_2d(points_data_2d* point_data_p,
                         geometry_data* geometry_p,
                         int t,
                         field* fx_p,
                         field* fy_p,
                         field* fz_p);

FP w(FP e);
FP w1(FP e);
FP w2(FP e);
FP w_solid(media_data* media_p,
               FP e);
FP w_multy(media_data* media_p,
               float E,
               int i);
int w_solid_test(media_data* media_p, const char* prefix);


FP calculate_norm(FP* w, int maxstage);


FP mask(FP g);

void init_media(configTable* ct_p,
                media_data* media_data_p,
                geometry_data* geometry_p,
                time_scheme* timescheme_p,
                scheme_constants* sch_p);

void destroy_media(media_data* media_data_p);

FP get_envelope(media_data* media_p, FP abs, int index);

void print_media(media_data* media_data_p);

void swap_media_2d(media_data* media_data_p,
                   geometry_data* geometry_p,
                   pml_data* pml_p,
                   int num);

void swap_media_3d(media_data* media_data_p,
                   geometry_data* geometry_p,
                   pml_data* pml_p,
                   int num);


/* Data save related parameters*/
typedef struct save_data_tag
{
    bool save;
    bool save_0_step;
    bool save_3d;
    int start_time;
    int end_time;
    int delta_time;
    bool compress;
    bool thin;
    bool save_ion;
    bool save_abs;
    int save_3d_time;
} save_data;

void init_savedata(configTable* ct_p, save_data* save_data_p);
void savedata_save(configTable* ct_p,
                   save_data* save_data_p,
                   geometry_data* geometry_p,
                   pml_data*   pml_p,
                   int t,
                   field* a1, field* a2,
                   field* a3,
                   const char* pathPrefix,
                   bool force);

void save_media_2d(save_data* save_data_p,
                   geometry_data* geometry_p,
                   time_scheme* timescheme_p,
                   media_data* media_p,
                   int t,
                   int index,
                   const char* pathPrefix);


int savedata_save_3d(save_data* sd_p,
		     vfield_n* E_p,
		     geometry_data* g_p,
		     media_data* m_p,
		     int t,
		     const char* pathPrefix);

int swap_all_3d(vfield_n* D_p, vfield_n* H_p, vfield_n* J_p,
		geometry_data* g_p,
		media_data* m_p,
		pml_data* p_p,
		int index);

void create_directories(configTable* fdtdConfig, char* pathPrefix);
int run_one_config(const char*config, bool estimate);
int test_ionization(const char*config);

FP time_shape(pulse_rt_data* pu_p,
                  FP t,
                  FP x);

FP time_shape3d(pulse_rt_data* pu_p,
                    FP t,
                    FP x,
                    FP y);

FP phase3d(pulse_rt_data* pu_p,
               FP t,
               FP x,
               FP y);

int run_2D_TE_config(configTable* fdtdConfig,
                     geometry_data geometry,
                     time_scheme timescheme,
                     const char* pathPrefix);

int run_2D_TM_config(configTable* fdtdConfig,
                     geometry_data geometry,
                     time_scheme timescheme,
                     const char* pathPrefix);

void add_sources_2d_fx(vfield* vf_p,
                       geometry_data* geometry_p,
                       sources_rt_data* s_p,
                       scheme_constants* sch_p);

void add_sources_2d_fy(vfield* vf_p,
                       geometry_data* geometry_p,
                       sources_rt_data* s_p,
                       scheme_constants* sch_p);

void add_sources_2d_fz(vfield* vf_p,
                       geometry_data* geometry_p,
                       sources_rt_data* s_p,
                       scheme_constants* sch_p);

void update_field_2d_te_fx(vfield* vfy_p,
                           vfield* vfx_p,
                           geometry_data* geometry_p,
                           decompose_data* decomp_p,
                           pml_data* pml_p,
                           scheme_constants* sch_p);

void update_field_2d_te_fx_new(vfield* vfy_p,
                               vfield* vfx_p,
                               geometry_data* geometry_p,
                               decompose_data* decomp_p,
                               pml_data* pml_p,
                               scheme_constants* sch_p);

void update_field_2d_te_fz_new(vfield* vfy_p,
                               vfield* vfz_p,
                               geometry_data* geometry_p,
                               decompose_data* decomp_p,
                               pml_data* pml_p,
                               scheme_constants* sch_p);

void update_field_2d_te_fz(vfield* vfy_p,
                           vfield* vfz_p,
                           geometry_data* geometry_p,
                           decompose_data* decomp_p,
                           pml_data* pml_p,
                           scheme_constants* sch_p);

void update_field_2d_te_fy(vfield* vfy_p,
                           vfield* vfxz_p,
                           vfield* vj_p,
                           geometry_data* geometry_p,
                           decompose_data* decomp_p,
                           pml_data* pml_p,
                           scheme_constants* sch_p);

void update_field_2d_te_fy_new(vfield* vfy_p,
                               vfield* vfxz_p,
                               vfield* vj_p,
                               geometry_data* geometry_p,
                               decompose_data* decomp_p,
                               pml_data* pml_p,
                               scheme_constants* sch_p);

void update_field_2d_tm_fx(vfield* vfy_p,
                           vfield* vfx_p,
                           vfield* vj_p,
                           geometry_data* geometry_p,
                           decompose_data* decomp_p,
                           pml_data* pml_p,
                           scheme_constants* sch_p);

void update_field_2d_tm_fz(vfield* vfy_p,
                           vfield* vfz_p,
                           vfield* vj_p,
                           geometry_data* geometry_p,
                           decompose_data* decomp_p,
                           pml_data* pml_p,
                           scheme_constants* sch_p);

void update_field_2d_tm_fy(vfield* vfy_p,
                           vfield* vfxz_p,
                           geometry_data* geometry_p,
                           decompose_data* decomp_p,
                           pml_data* pml_p,
                           scheme_constants* sch_p);

void calculate_energy_2d_te(vfield* vfd_p,
                            vfield* vfe_p,
                            vfield* vfh_p,
                            media_data* media_p,
                            geometry_data* geometry_p,
                            int t);

void calculate_energy_2d_tm(vfield* vfd_p,
                            vfield* vfe_p,
                            vfield* vfh_p,
                            media_data* media_p,
                            geometry_data* geometry_p,
                            int t);

void calculate_energy_3d(vfield_n* ve_p,
                         vfield_n* vd_p,
                         vfield_n* vh_p,
                         media_data* media_p,
			 time_scheme* t_p,
                         geometry_data* geometry_p,
                         int t,
			 const char* pathPrefix);

int calculate_pulse_center(vfield_n* ve_p,
                           vfield_n* vd_p,
                           vfield_n* vh_p,
                           geometry_data* geometry_p);

void calculate_plasma_volume_thr(media_data* media_p,
                                 geometry_data* geometry_p,
                                 FP thr,
                                 const char* pathPrefix);

void calculate_plasma_density(media_data* media_p,
                              geometry_data* geometry_p,
                              const char* pathPrefix);

void calculate_e_from_d_2d_te(vfield* vfd_p,
                              vfield* vfe_p,
                              media_data* media_p,
                              geometry_data* geometry_p,
                              decompose_data* decomp_p);

void calculate_e_from_d_2d_tm(vfield* vfd_p,
                              vfield* vfe_p,
                              vfield* vhy_p,
                              media_data* media_p,
                              geometry_data* geometry_p,
                              decompose_data* decomp_p);

void calculate_e_from_d_3d(vfield_n* vfd_p,
                           vfield_n* vfe_p,
                           media_data* media_p,
                           geometry_data* geometry_p);

void update_moving_boundary(geometry_data* geometry_p,
                            pml_data* pml_p,
                            scheme_constants* sch_p,
                            int t);

void init_moving_window_2d(geometry_data* g_p,
                           sources_rt_data* s_p,
                           pml_data* pml_p,
                           time_scheme* ts_p,
                           scheme_constants* sch_p);
void init_moving_window_3d(geometry_data* g_p,
			   sources_rt_data* s_p,
			   pml_data* pml_p,
			   time_scheme* ts_p,
			   scheme_constants* sch_p,
			   const char* pathPrefix);

void finalize_moving_window_3d(geometry_data* g_p,
			       time_scheme* t_p,
			       media_data* m_p,
			       const char* pathPrefix);

void check_fields(vfield_n* vd_p,
                  vfield_n* vh_p,
                  vfield_n* vj_p,
                  geometry_data* geometry_p,
                  int t);

FP field_abs(field* field_x_p,
                 field* field_z_p,
                 int x,
                 int z);

int create_directory(const char*name);

FP calculate_epsilon(media_data* media_p,
                         FP sq_abs,
                         int index);

FP calculate_epsilon_nk(media_data* media_p,
                            int index);

FP calc_eps_solid_k(media_data* media_p,
                        FP sq_abs,
                        int index);

FP calc_eps_gas_k(media_data* media_p,
                      FP sq_abs,
                      int index);

FP calc_eps_solid_nk(media_data* media_p,
                         FP sq_abs,
                         int index);

FP calc_eps_gas_nk(media_data* media_p,
                       FP sq_abs,
                       int index);

void save_dump(int t,
	       time_scheme* time_scheme_p,
	       vfield_n* fd_p,
	       vfield_n* fj_p,
	       vfield_n* fh_p,
	       pml_data* pml_p,
	       media_data* media_p,
	       const char* pathPrefix);

void read_dump(time_scheme* time_scheme_p,
	       vfield_n* fd_p,
	       vfield_n* fj_p,
	       vfield_n* fh_p,
	       pml_data* pml_p,
	       media_data* media_p,
	       const char* pathPrefix);

#ifdef USEMPI
typedef struct mpi_data_tag
{
    int rank;
    int size;
    char processor_name[MPI_MAX_PROCESSOR_NAME];
    int namelen;
    /* for 2d meshes */
    int up_nbr;
    int down_nbr;

    /* number of proc in x,y,z directions */
    int num_proc_x;
    int num_proc_y;
    int num_proc_z;

    /* for 3d meshes */
    int up_x;
    int down_x;
    int up_y;
    int down_y;
    int up_z;
    int down_z;

    /* position in matrix */
    int i,j,k;

    /* send/receive buffer for 2D */
    FP* sendbuffer;
    FP* recvbuffer;

    /* send/receive buffer for 3D */
    FP* sb3d;
    FP* rb3d;

    /* send/receive buffer for 3D: async
     currently not used */
    FP* sb3d_x;
    FP* sb3d_y;
    FP* sb3d_z;
    FP* rb3d_x;
    FP* rb3d_y;
    FP* rb3d_z;
    int buff_size;

    int* recv_down_x_idx;
    int* recv_down_y_idx;
    int* recv_down_z_idx;
    int* send_down_x_idx;
    int* send_down_y_idx;
    int* send_down_z_idx;
    int* recv_up_x_idx;
    int* recv_up_y_idx;
    int* recv_up_z_idx;
    int* send_up_x_idx;
    int* send_up_y_idx;
    int* send_up_z_idx;

    int  x_num;
    int  y_num;
    int  z_num;

    int sizex;
    int sizey;
    int sizez;

    /* for moving window */
    int sender;
    int receiver;

    MPI_Request r_x;
    MPI_Request r_y;
    MPI_Request r_z;
    MPI_Request s_x;
    MPI_Request s_y;
    MPI_Request s_z;

#ifdef CUDA
    int* crecv_down_x_idx;
    int* crecv_down_y_idx;
    int* crecv_down_z_idx;
    int* csend_down_x_idx;
    int* csend_down_y_idx;
    int* csend_down_z_idx;
    int* crecv_up_x_idx;
    int* crecv_up_y_idx;
    int* crecv_up_z_idx;
    int* csend_up_x_idx;
    int* csend_up_y_idx;
    int* csend_up_z_idx;
    FP* csb3d;
    FP* crb3d;
#endif
} mpi_data;

extern mpi_data mpi;

void init_mpi(int argc, char *argv[], mpi_data* mpi_p);
void finalize_mpi();
void mpi_wait_up_x(field_n* fx_p, field_n* fy_p, field_n* fz_p);
void mpi_wait_up_y(field_n* fx_p, field_n* fy_p, field_n* fz_p);
void mpi_wait_up_z(field_n* fx_p, field_n* fy_p, field_n* fz_p);
void mpi_wait_down_x(field_n* fx_p, field_n* fy_p, field_n* fz_p);
void mpi_wait_down_y(field_n* fx_p, field_n* fy_p, field_n* fz_p);
void mpi_wait_down_z(field_n* fx_p, field_n* fy_p, field_n* fz_p);

int  init_mpi_sendrecv_buffer(mpi_data* mpi_p, geometry_data* geometry_p);
int  init_mpi_sendrecv_buffer3d(mpi_data* mpi_p, geometry_data* geometry_p,
				vfield_n* D_p, vfield_n* H_p);
int  delete_mpi_sendrecvbuffer(mpi_data* mpi_p);
int  delete_mpi_sendrecvbuffer3d(mpi_data* mpi_p);

/* functions for 2D exchange */
int  mpi_exch_up2d_x(mpi_data* mpi_p, geometry_data* geometry_p, field* f_p);
int  mpi_exch_up2d_z(mpi_data* mpi_p, geometry_data* geometry_p, field* f_p);
int  mpi_exch_down2d_x(mpi_data* mpi_p, geometry_data* geometry_p, field* f_p);
int  mpi_exch_down2d_z(mpi_data* mpi_p, geometry_data* geometry_p, field* f_p);

/* correct implementation of data exchange */
int mpi_exch_up3d_x_1comp(mpi_data* mpi_p,
                          geometry_data* geometry_p,
			  field_n* f_p);

int mpi_exch_up3d_y_1comp(mpi_data* mpi_p,
			  geometry_data* geometry_p,
			  field_n* f_p);

int mpi_exch_up3d_z_1comp(mpi_data* mpi_p,
			  geometry_data* geometry_p,
			  field_n* f_p);

int mpi_exch_down3d_x_1comp(mpi_data* mpi_p,
			    geometry_data* geometry_p,
			    field_n* f_p);

int mpi_exch_down3d_y_1comp(mpi_data* mpi_p,
			    geometry_data* geometry_p,
			    field_n* f_p);

int mpi_exch_down3d_z_1comp(mpi_data* mpi_p,
			    geometry_data* geometry_p,
			    field_n* f_p);

int mpi_send_up_x(mpi_data* mpi_p, geometry_data* geometry_p);
int mpi_recv_up_x(mpi_data* mpi_p, geometry_data* geometry_p);
int mpi_send_up_y(mpi_data* mpi_p, geometry_data* geometry_p);
int mpi_recv_up_y(mpi_data* mpi_p, geometry_data* geometry_p);
int mpi_send_up_z(mpi_data* mpi_p, geometry_data* geometry_p);
int mpi_recv_up_z(mpi_data* mpi_p, geometry_data* geometry_p);
int mpi_send_down_x(mpi_data* mpi_p, geometry_data* geometry_p);
int mpi_recv_down_x(mpi_data* mpi_p, geometry_data* geometry_p);
int mpi_send_down_y(mpi_data* mpi_p, geometry_data* geometry_p);
int mpi_recv_down_y(mpi_data* mpi_p, geometry_data* geometry_p);
int mpi_send_down_z(mpi_data* mpi_p, geometry_data* geometry_p);
int mpi_recv_down_z(mpi_data* mpi_p, geometry_data* geometry_p);

int mpi_exch_up3d_x_2comp(mpi_data* mpi_p,
                          geometry_data* geometry_p,
                          vfield_n* f_p);

int mpi_exch_up3d_y_2comp(mpi_data* mpi_p,
                          geometry_data* geometry_p,
                          vfield_n* f_p);

int mpi_exch_up3d_z_2comp(mpi_data* mpi_p,
                          geometry_data* geometry_p,
                          vfield_n* f_p);

int mpi_exch_down3d_x_2comp(mpi_data* mpi_p,
                            geometry_data* geometry_p,
                            vfield_n* f_p);

int mpi_exch_down3d_y_2comp(mpi_data* mpi_p,
                            geometry_data* geometry_p,
                            vfield_n* fx_p);

int mpi_exch_down3d_z_2comp(mpi_data* mpi_p,
                            geometry_data* geometry_p,
                            vfield_n* f_p);

int mpi_exch_up3d_x_3comp_a(mpi_data* mpi_p,
                            geometry_data* geometry_p,
                            field_n* fx_p,
                            field_n* fy_p,
                            field_n* fz_p);

int mpi_exch_up3d_y_3comp_a(mpi_data* mpi_p,
                            geometry_data* geometry_p,
                            field_n* fx_p,
                            field_n* fy_p,
                            field_n* fz_p);

int mpi_exch_up3d_z_3comp_a(mpi_data* mpi_p,
                            geometry_data* geometry_p,
                            field_n* fx_p,
                            field_n* fy_p,
                            field_n* fz_p);

int mpi_exch_down3d_x_3comp_a(mpi_data* mpi_p,
                              geometry_data* geometry_p,
                              field_n* fx_p,
                              field_n* fy_p,
                              field_n* fz_p);

int mpi_exch_down3d_y_3comp_a(mpi_data* mpi_p,
                              geometry_data* geometry_p,
                              field_n* fx_p,
                              field_n* fy_p,
                              field_n* fz_p);

int mpi_exch_down3d_z_3comp_a(mpi_data* mpi_p,
                              geometry_data* geometry_p,
                              field_n* fx_p,
                              field_n* fy_p,
                              field_n* fz_p);

int  mpi_calc_rank(mpi_data* mpi_p, int i, int j, int k);
#endif /* MPI */

int run_3D_config(configTable* fdtdConfig,
                  geometry_data geometry,
                  time_scheme timescheme,
                  const char* pathPrefix);

int run_3D_test_ionization(configTable* fdtdConfig,
                           geometry_data geometry,
                           time_scheme timescheme,
                           const char* pathPrefix);

int store_slice(field* field_p,
                char dir,
                int slice,
                const char*name,
                int t,
                geometry_data* geometry_p,
                const char* pathPrefix);

int store_slice_2d(save_data* save_data_p,
                   field* field_p,
                   const char*name,
                   int t,
                   geometry_data* geometry_p,
                   const char* pathPrefix);

int store_slice_2d_moving(save_data* save_data_p,
                          field* field_p,
                          const char*name,
                          int t,
                          int index,
                          geometry_data* geometry_p,
                          const char* pathPrefix);

int store_slice_3d_moving(field* field_p,
                          const char*name,
                          char dir,
                          int slice,
                          int t,
                          int iz,
                          geometry_data* geometry_p,
                          const char* pathPrefix);

int store_slice_n(field_n* field_p,
                  char dir,
                  int slice,
                  int t,
                  geometry_data* geometry_p,
                  const char* pathPrefix);

int store_slice_n_3comp(field_n* fx_p,
                        field_n* fy_p,
                        field_n* fz_p,
                        const char*name,
                        char dir,
                        int slice,
                        int t,
                        geometry_data* geometry_p,
                        const char* pathPrefix);

int store_slice_3comp(field* fx_p,
                      field* fy_p,
                      field* fz_p,
                      const char* name,
                      char dir,
                      int slice,
                      int t,
                      geometry_data* geometry_p,
                      const char* pathPrefix);

int store_field_n_3comp(field_n* fx_p,
                        field_n* fy_p,
                        field_n* fz_p,
                        const char* name,
                        int t,
                        geometry_data* geometry_p,
                        const char* pathPrefix);

int store_field_3comp(field* fx_p,
                      field* fy_p,
                      field* fz_p,
                      const char* name,
                      int t,
                      geometry_data* geometry_p,
                      const char* pathPrefix);

int store_field_n(field_n* fx_p,
                  const char*name,
                  int t,
                  geometry_data* geometry_p,
                  const char* pathPrefix);

int store_field(field* fx_p,
                const char* name,
                int t,
                geometry_data* geometry_p,
                const char* pathPrefix);
#endif

#ifdef CUDA
#include "fdtd_cuda.h"
#endif
