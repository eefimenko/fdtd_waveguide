#ifndef _TEMPLATE_KERNEL_H_
#define _TEMPLATE_KERNEL_H_
void printDevProp(cudaDeviceProp devProp, int i);

__global__ void store_point_data_gpu_3d(FP* buffer,
					FP* cdx_p,
					FP* cdy_p,
					FP* cdz_p,
					FP* chx_p,
					FP* chy_p,
					FP* chz_p,
					int offset,
					int idx);

__global__ void calculate_E_from_D_3d_kerr(FP* ex_p,
					   FP* ey_p,
					   FP* ez_p,
					   FP* dx_p,
					   FP* dy_p,
					   FP* dz_p,
					   FP* epsilon,
					   FP* n_max,
					   FP khi, 
					   int nmax);

__global__ void calculate_E_from_D_3d(FP* ex_p,
				      FP* ey_p,
				      FP* ez_p,
				      FP* dx_p,
				      FP* dy_p,
				      FP* dz_p,
				      FP* eps,
				      int nmax);
__global__ void add_tfsf_3d_D_xmin(FP* ey_p,
				   FP* ez_p,
				   FP* w_xmin,
				   int* we_xmin_idx,
				   int num);
__global__ void add_tfsf_3d_D_xmax(FP* ey_p,
				   FP* ez_p,
				   FP* w_xmax,
				   int* we_xmax_idx,
				   int num);
__global__ void add_tfsf_3d_D_ymin(FP* ex_p,
				   FP* ez_p,
				   FP* w_ymin,
				   int* we_ymin_idx,
				   int num);
__global__ void add_tfsf_3d_D_ymax(FP* ex_p,
				   FP* ez_p,
				   FP* w_ymax,
				   int* we_ymax_idx,
				   int num);
__global__ void add_tfsf_3d_D_zmin(FP* ex_p,
				   FP* ey_p,
				   FP* w_zmin,
				   int* we_zmin_idx,
				   int num);
__global__ void add_tfsf_3d_D_zmax(FP* ex_p,
				   FP* ey_p,
				   FP* w_zmax,
				   int* we_zmax_idx,
				   int num);
__global__ void add_tfsf_3d_H_xmin(FP* hy_p,
				   FP* hz_p,
				   FP* w_xmin,
				   int* wh_xmin_idx,
				   int num);
__global__ void add_tfsf_3d_H_xmax(FP* hy_p,
				   FP* hz_p,
				   FP* w_xmax,
				   int* wh_xmax_idx,
				   int num);
__global__ void add_tfsf_3d_H_ymin(FP* hx_p,
				   FP* hz_p,
				   FP* w_ymin,
				   int* wh_ymin_idx,
				   int num);
__global__ void add_tfsf_3d_H_ymax(FP* hx_p,
				   FP* hz_p,
				   FP* w_ymax,
				   int* we_ymax_idx,
				   int num);
__global__ void add_tfsf_3d_H_zmin(FP* hx_p,
				   FP* hy_p,
				   FP* w_zmin,
				   int* wh_zmin_idx,
				   int num);
__global__ void add_tfsf_3d_H_zmax(FP* hx_p,
				   FP* hy_p,
				   FP* w_zmax,
				   int* wh_zmax_idx,
				   int num);

__global__ void add_psources_3d_H_zmin(FP* hx_p,
				       FP* hy_p,
				       FP* j_zmin,
				       int* jh_zmin_idx,
				       int num);

__global__ void add_psources_3d_H_zmax(FP* hx_p,
				       FP* hy_p,
				       FP* j_zmax,
				       int* jh_zmax_idx,
				       int num);

__global__ void add_psources_3d_D_zmin(FP* ex_p,
				       FP* ey_p,
				       FP* j_zmin,
				       int* je_zmin_idx,
				       int num);

__global__ void add_psources_3d_D_zmax(FP* ex_p,
				       FP* ey_p,
				       FP* j_zmax,
				       int* je_zmax_idx,
				       int num);

__global__ void fdtd_3d_D(FP* ex_p,
			  FP* ey_p,
			  FP* ez_p,
			  FP* hx_p,
			  FP* hy_p,
			  FP* hz_p,
			  FP* jx_p,
			  FP* jy_p,
			  FP* jz_p,
			  int* idexf,
			  int* ideyf,
			  int* idezf,
			  int nexf,
			  int neyf,
			  int nezf);

__global__ void fdtd_3d_H(FP* ex_p,
			  FP* ey_p,
			  FP* ez_p,
			  FP* hx_p,
			  FP* hy_p,
			  FP* hz_p,
			  int* idhxf,
			  int* idhyf,
			  int* idhzf,
			  int nhxf,
			  int nhyf,
			  int nhzf);

__global__ void fdtd_3d_D_pml(FP* ex_p,
			      FP* ey_p,
			      FP* ez_p,
			      FP* hx_p,
			      FP* hy_p,
			      FP* hz_p,
			      FP* Exy,
			      FP* Exz,
			      FP* Eyx,
			      FP* Eyz,
			      FP* Ezx,
			      FP* Ezy,
			      FP* aexy,
			      FP* aexz,
			      FP* bexy,
			      FP* bexz,
			      FP* aeyx,
			      FP* aeyz,
			      FP* beyx,
			      FP* beyz,
			      FP* aezx,
			      FP* aezy,
			      FP* bezx,
			      FP* bezy,
			      int* idex,
			      int* idey,
			      int* idez,
			      int nex,
			      int ney,
			      int nez);

__global__ void fdtd_2dte_Ey(FP* ey_p,
			     FP* hx_p,
			     FP* hz_p,
			     FP* jey_p,
			     FP  t_f,
			     FP* Eyx_x_l,
			     FP* Eyz_x_l,
			     FP* Eyx_x_h,
			     FP* Eyz_x_h,
			     FP* Eyz_z_l,
			     FP* Eyx_z_l,
			     FP* Eyz_z_h,
			     FP* Eyx_z_h);

__global__ void fdtd_2dte_Hx(FP* hx_p,
			     FP* ey_p);

__global__ void fdtd_2dte_Hz(FP* hz_p,
			     FP* ey_p);

__global__ void fdtd_3d_Dx(FP* ex_p,
			   FP* ey_p,
			   FP* ez_p,
			   FP* hx_p,
			   FP* hy_p,
			   FP* hz_p,
			   FP* jx_p,
			   FP* jy_p,
			   FP* jz_p,
			   FP  t_f,
			   int* idexf,
			   int nexf);

__global__ void fdtd_3d_Hx(FP* ex_p,
			   FP* ey_p,
			   FP* ez_p,
			   FP* hx_p,
			   FP* hy_p,
			   FP* hz_p,
			   int* idhxf,
			   int nhxf);

__global__ void fdtd_3d_Dx_pml(FP* ex_p,
			       FP* ey_p,
			       FP* ez_p,
			       FP* hx_p,
			       FP* hy_p,
			       FP* hz_p,
			       FP* Exy,
			       FP* Exz,
			       FP* aexy,
			       FP* aexz,
			       FP* bexy,
			       FP* bexz,
			       int* idex,
			       int nex);

__global__ void fdtd_3d_Hx_pml(FP* ex_p,
			       FP* ey_p,
			       FP* ez_p,
			       FP* hx_p,
			       FP* hy_p,
			       FP* hz_p,
			       FP* Hxy,
			       FP* Hxz,
			       FP* ahxy,
			       FP* ahxz,
			       FP* bhxy,
			       FP* bhxz,
			       int* idhx,
			       int nhx);

__global__ void fdtd_3d_Dy(FP* ex_p,
			   FP* ey_p,
			   FP* ez_p,
			   FP* hx_p,
			   FP* hy_p,
			   FP* hz_p,
			   FP* jx_p,
			   FP* jy_p,
			   FP* jz_p,
			   FP  t_f,
			   int* ideyf,
			   int neyf);

__global__ void fdtd_3d_Hy(FP* ex_p,
			   FP* ey_p,
			   FP* ez_p,
			   FP* hx_p,
			   FP* hy_p,
			   FP* hz_p,
			   int* idhyf,
			   int nhyf);

__global__ void fdtd_3d_Dy_pml(FP* ex_p,
			       FP* ey_p,
			       FP* ez_p,
			       FP* hx_p,
			       FP* hy_p,
			       FP* hz_p,
			       FP* Eyx,
			       FP* Eyz,
			       FP* aeyx,
			       FP* aeyz,
			       FP* beyx,
			       FP* beyz,
			       int* idey,
			       int ney);

__global__ void fdtd_3d_Hy_pml(FP* ex_p,
			       FP* ey_p,
			       FP* ez_p,
			       FP* hx_p,
			       FP* hy_p,
			       FP* hz_p,
			       FP* Hyx,
			       FP* Hyz,
			       FP* ahyx,
			       FP* ahyz,
			       FP* bhyx,
			       FP* bhyz,
			       int* idhy,
			       int nhy);

__global__ void fdtd_3d_Dz(FP* ex_p,
			   FP* ey_p,
			   FP* ez_p,
			   FP* hx_p,
			   FP* hy_p,
			   FP* hz_p,
			   FP* jx_p,
			   FP* jy_p,
			   FP* jz_p,
			   FP  t_f,
			   int* idezf,
			   int nezf);

__global__ void fdtd_3d_Hz(FP* ex_p,
			   FP* ey_p,
			   FP* ez_p,
			   FP* hx_p,
			   FP* hy_p,
			   FP* hz_p,
			   int* idhzf,
			   int nhzf);

__global__ void fdtd_3d_Dz_pml(FP* ex_p,
			       FP* ey_p,
			       FP* ez_p,
			       FP* hx_p,
			       FP* hy_p,
			       FP* hz_p,
			       FP* Ezx,
			       FP* Ezy,
			       FP* aezx,
			       FP* aezy,
			       FP* bezx,
			       FP* bezy,
			       int* idez,
			       int nez);

__global__ void fdtd_3d_Hz_pml(FP* ex_p,
			       FP* ey_p,
			       FP* ez_p,
			       FP* hx_p,
			       FP* hy_p,
			       FP* hz_p,
			       FP* Hzx,
			       FP* Hzy,
			       FP* ahzx,
			       FP* ahzy,
			       FP* bhzx,
			       FP* bhzy,
			       int* idhz,
			       int nhz);

__global__ void calculate_energy(FP* ex_p,
				 FP* ey_p,
				 FP* ez_p,
				 FP* hx_p,
				 FP* hy_p,
				 FP* hz_p,
				 int nmax,
    				 FP* energy);


#ifdef USEMPI
__global__ void copy_send(FP* f1_p, FP* f2_p, int* idx_p, FP* buffer, int num);
__global__ void copy_recv(FP* f1_p, FP* f2_p, int* idx_p, FP* buffer, int num);
#endif
#endif






