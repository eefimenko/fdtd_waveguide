#ifndef _FDTD_CUDA_H_
#define _FDTD_CUDA_H_

#define BLOCK_SIZE 256

#define I_OFFSET 0
#define J_OFFSET 1
#define K_OFFSET 2
#define NX       3
#define NY       4
#define NZ       5

#define DX       0
#define DY       1
#define DZ       2

#define DTDX       0
#define DTDY       1
#define DTDZ       2
#define DT_4_PI    3
#define DT_FF_4_PI 4
#define DX_4_PI    5
#define DY_4_PI    6
#define DZ_4_PI    7
#define DXY_4_PI   8
#define AX         9
#define BX         10
#define AY         11
#define BY         12
#define AZ         13
#define BZ         14
#define DCX        15
#define DCY        16
#define DCZ        17

#define NUM_ITERATIONS 1

typedef struct cuda_field_tag
{
    FP* d_p; //device field
    int nx;
    int ny;
    int nz;
    int nxy;
    int memsize;
} cuda_field;

int init_cuda_field_3d(cuda_field* field_p,
                       int nx,
                       int ny,
                       int nz);

int init_cuda_field_2d(cuda_field* field_p,
                       int nx,
                       int nz);

int init_cuda_field_1d(cuda_field* field_p,
                       int nx);

int zero_cuda_field(cuda_field* src_p);

int destroy_cuda_field(cuda_field* field_p);

int copy_cuda_field_h2d(cuda_field* field_p);

int copy_cuda_field_d2h(cuda_field* field_p, FP* a_p);

int malloc_and_init_cuda_field_3d(cuda_field** field_p,
                                  int nx,
                                  int ny,
                                  int nz);

int malloc_and_init_cuda_field_2d(cuda_field** field_p,
                                  int nx,
                                  int nz);

int malloc_and_init_cuda_field_1d(cuda_field** field_p,
                                  int nx);

int malloc_and_init_cuda_field_3d(cuda_field** field_p, 
				      int nx,
				      int ny, 
				      int nz);
int malloc_and_init_cuda_field_2d(cuda_field** field_p, 
				      int nx,
				      int nz);
int zero_cuda_field(cuda_field* field_p);
int destroy_cuda_field(cuda_field* field_p);
int copy_cuda_field_h2d(cuda_field* field_p, FP* a_p);
int copy_cuda_h2d(FP* src_p, FP* dst_p, int size);
int copy_cuda_field_d2h(cuda_field* field_p, FP* a_p);
void cuda_malloc_int(int** dest_p, int* src_p, int size);
void cuda_malloc(FP** dest_p, FP* src_p, int size);
void cuda_free(FP* pointer);
void cuda_free_int(FP* pointer);
void cuda_realloc_int(int** dest_p, int* src_p, int size);
void cuda_realloc(FP** dest_p, FP* src_p, int size);
int init_cuda_pml_2d(pml_data* pml_p);
int init_cuda_pml_3d(pml_data* pml_p);
int destroy_cuda_pml_3d(pml_data* pml_p);
int cuda_init_constants(scheme_constants* sch_p);
void init_psources_3d_cuda(sources_rt_data* s_p);
void destroy_psources_3d_cuda(sources_rt_data* s_p);
void update_sources_rt3d_cuda(sources_rt_data* s_p);
void init_tfsf_3d_cuda(sources_rt_data* s_p);
void destroy_tfsf_3d_cuda(sources_rt_data* s_p);
int init_cuda_geometry_2d(geometry_data* geometry_data_p);
int init_cuda_geometry_3d(geometry_data* geometry_data_p);
void calculate_energy_3d_cuda(vfield_n* ve_p,
			      vfield_n* vd_p,
			      vfield_n* vh_p,
			      media_data* media_p,
			      time_scheme* t_p,
			      geometry_data* g_p,
			      int t,
			      const char* pathPrefix);
void destroy_media_3d_cuda(media_data* m_p);
void init_media_3d_cuda(media_data* m_p);
void update_moving_window_3d_cuda(vfield_n* D_p, vfield_n* E_p, vfield_n* H_p, vfield_n* J_p,
				  cuda_field* cdx_p,
				  cuda_field* cdy_p,
				  cuda_field* cdz_p,
				  cuda_field* cex_p,
				  cuda_field* cey_p,
				  cuda_field* cez_p,
				  cuda_field* chx_p,
				  cuda_field* chy_p,
				  cuda_field* chz_p,
				  cuda_field* cjx_p,
				  cuda_field* cjy_p,
				  cuda_field* cjz_p,
				  geometry_data* g_p,
				  time_scheme* t_p,
				  media_data* m_p,
				  pml_data* p_p,
				  points_data_3d* pd_p,
				  sources_rt_data* s_p,
				  scheme_constants* sch_p,
				  int t,
				  const char* pathPrefix);
void swap_media_3d_cuda(media_data* m_p);
int reinit_cuda_pml_3d(pml_data* pml_p);
void transfer_pml_d2h(pml_data* p_p);
void transfer_pml_h2d(pml_data* p_p);
void cuda_memcpy_h2d(FP* dst_p, FP* src_p, int size);
void cuda_memcpy_d2h(FP* dst_p, FP* src_p, int size);
void init_point_data_3d_cuda(points_data_3d* pnt_p);
void destroy_point_data_3d_cuda(points_data_3d* pnt_p);
void store_point_data_3d_cuda(points_data_3d* pnt_p,
			      geometry_data* geometry_p,
			      int t,
			      cuda_field* cex_p,
			      cuda_field* cey_p,
			      cuda_field* cez_p,
			      cuda_field* chx_p,
			      cuda_field* chy_p
			      ,
			      cuda_field* chz_p);
void store_buffered_point_data_3d(points_data_3d* pnt_p);

#ifdef USEMPI
int delete_mpi_sendrecv_buffer3d_cuda(mpi_data* mpi_p);
int init_mpi_sendrecv_buffer3d_cuda(mpi_data* mpi_p);
int mpi_send_down_x_cuda(mpi_data* mpi_p, cuda_field* fy_p, cuda_field* fz_p);
int mpi_recv_down_x_cuda(mpi_data* mpi_p, cuda_field* fy_p, cuda_field* fz_p);
int mpi_send_down_y_cuda(mpi_data* mpi_p, cuda_field* fx_p, cuda_field* fz_p);
int mpi_recv_down_y_cuda(mpi_data* mpi_p, cuda_field* fx_p, cuda_field* fz_p);
int mpi_send_down_z_cuda(mpi_data* mpi_p, cuda_field* fx_p, cuda_field* fy_p);
int mpi_recv_down_z_cuda(mpi_data* mpi_p, cuda_field* fx_p, cuda_field* fy_p);
int mpi_send_up_x_cuda(mpi_data* mpi_p, cuda_field* fy_p, cuda_field* fz_p);
int mpi_recv_up_x_cuda(mpi_data* mpi_p, cuda_field* fy_p, cuda_field* fz_p);
int mpi_send_up_y_cuda(mpi_data* mpi_p, cuda_field* fx_p, cuda_field* fz_p);
int mpi_recv_up_y_cuda(mpi_data* mpi_p, cuda_field* fx_p, cuda_field* fz_p);
int mpi_send_up_z_cuda(mpi_data* mpi_p, cuda_field* fx_p, cuda_field* fy_p);
int mpi_recv_up_z_cuda(mpi_data* mpi_p, cuda_field* fx_p, cuda_field* fy_p);
int mpi_exch_up3d_x_2comp_cuda(mpi_data* mpi_p, cuda_field* fy_p, cuda_field* fz_p);
int mpi_exch_up3d_y_2comp_cuda(mpi_data* mpi_p, cuda_field* fx_p, cuda_field* fz_p);
int mpi_exch_up3d_z_2comp_cuda(mpi_data* mpi_p, cuda_field* fx_p, cuda_field* fy_p);
int mpi_exch_down3d_x_2comp_cuda(mpi_data* mpi_p, cuda_field* fy_p, cuda_field* fz_p);
int mpi_exch_down3d_y_2comp_cuda(mpi_data* mpi_p, cuda_field* fx_p, cuda_field* fz_p);
int mpi_exch_down3d_z_2comp_cuda(mpi_data* mpi_p, cuda_field* fx_p, cuda_field* fy_p);
#define MPI_EXCH_UP_X(mpi_p, fy_p, fz_p) mpi_exch_up3d_x_2comp_cuda(mpi_p, fy_p, fz_p)
#define MPI_EXCH_UP_Y(mpi_p, fx_p, fz_p) mpi_exch_up3d_y_2comp_cuda(mpi_p, fx_p, fz_p)
#define MPI_EXCH_UP_Z(mpi_p, fx_p, fy_p) mpi_exch_up3d_z_2comp_cuda(mpi_p, fx_p, fy_p)
#define MPI_EXCH_DOWN_X(mpi_p, fy_p, fz_p) mpi_exch_down3d_x_2comp_cuda(mpi_p, fy_p, fz_p)
#define MPI_EXCH_DOWN_Y(mpi_p, fx_p, fz_p) mpi_exch_down3d_y_2comp_cuda(mpi_p, fx_p, fz_p)
#define MPI_EXCH_DOWN_Z(mpi_p, fx_p, fy_p) mpi_exch_down3d_z_2comp_cuda(mpi_p, fx_p, fy_p)
#else
#define MPI_EXCH_UP_X(mpi_p, fy_p, fz_p)
#define MPI_EXCH_UP_Y(mpi_p, fx_p, fz_p)
#define MPI_EXCH_UP_Z(mpi_p, fx_p, fy_p)
#define MPI_EXCH_DOWN_X(mpi_p, fy_p, fz_p)
#define MPI_EXCH_DOWN_Y(mpi_p, fx_p, fz_p)
#define MPI_EXCH_DOWN_Z(mpi_p, fx_p, fy_p)
#endif

#endif 
