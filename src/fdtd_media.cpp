/** @file fdtd_media.cpp
    Contains all function related to media */
#include "fdtd.h"

static FP tanh_jet(FP z, FP z0, FP zrise, FP zlength);
static FP para_jet(FP z, FP zlength);
static FP sin_jet(FP z, FP zlength);

static void init_media_2d(configTable* ct_p,
                          media_data* m_p,
                          geometry_data* g_p,
                          time_scheme* t_p);

static void init_slab_3d(geometry_data* g_p,
                         media_data* m_p);

static void init_drop_3d(geometry_data* g_p,
                         media_data* m_p);

static void init_capillar_3d(geometry_data* g_p,
                             media_data* m_p);

static void init_slab_2d(geometry_data* g_p,
                         media_data* m_p);

static void init_drop_2d(geometry_data* g_p,
                         media_data* m_p);

static void init_capillar_2d(geometry_data* g_p,
                             media_data* m_p);

static void init_media_3d(configTable* ct_p,
                          media_data* m_p,
                          geometry_data* g_p,
                          time_scheme* t_p);

static FP calc_drop_dist(geometry_data* g_p,
                             media_data* media_p,
                             FP x,
                             FP y,
                             FP z);

/**  This function initialises media_data structure according to configuration.
 *   \li 1. Function sets the correct media type, currently,
 *   Hydrogen, Helium, Water, Multystage ionization
 *   \li 2. Function sets the correct media shape, currently, slab or drop.
 *   \li 3. Function initialises all media constants: eps, n, n2, collision rate, density
 *   and constants related to ionization.
 *   \li 4. Function calls appropriate function to allocate media specific fields.
 *   \author  Evgeny Efimenko
 *   \param[in]   ct_p - pointer to configTable structure containing configuration
 *   \param[out]   m_p - pointer to media_data structure
 *   \param[in]   g_p - pointer to geometry_data structure
 *   \param[in]   t_p - pointer to time_scheme structure
 *   \return  void
 *   \callgraph
 *   \callergraph
 */
void init_media(configTable* ct_p,
                media_data* m_p,
                geometry_data* g_p,
                time_scheme* t_p,
                scheme_constants* sch_p)
{
    string media_temp = ct_p->getStringValue("media", "hydrogen");
    string media_shape = ct_p->getStringValue("media_shape", "slab");
    string slab_shape = ct_p->getStringValue("slab_shape", "tanh");
    string smooth = ct_p->getStringValue("drop_smooth", "off");
    FP density = 0.;

    m_p->max_energy = 0;
    m_p->recalc = false;
    m_p->eps = ct_p->getDoubleValue("eps", 1);
    m_p->n_0 = sqrt(m_p->eps);
    
    m_p->kerr = ct_p->getIntValue("kerr", 0);
        
    density = ct_p->getDoubleValue("density", 0);
        
    m_p->khi3 = 0;
    m_p->n_2 = 0;
    
    m_p->c_en = (1.71e7)*(1.71e7)* // Ea SGS
                         (1e-4)*(1e-4)*(1e-4)*
                         g_p->delta_x*g_p->delta_y*g_p->delta_z* //dV SGS
                         1e-4/ // erg-> mJ
                         (8*M_PI);

    if (0 == smooth.compare("on"))
    {
	m_p->smoothing = true;
    }
    else
    {
	m_p->smoothing = false;
    }
    
    if (0 == slab_shape.compare("sin"))
    {
        m_p->sshape = sin_e;
    }
    else if (0 == slab_shape.compare("para"))
    {
        m_p->sshape = par_e;
    }
    else
    {
        m_p->sshape = tan_e;
    }
    
    m_p->update_eps = &calculate_epsilon;
    
    if (0 == media_temp.compare("water")) 
    {
        m_p->type = so_e;
 
	FP freq = 2.35e15*800./m_p->wl;
        FP N_solid = density*1e6; /* m-3 */
        FP N_crit = N_CR*1e6*(freq/2.35e15)*(freq/2.35e15); /* m-3 */

       	m_p->density = N_solid/N_crit;
        m_p->N_crit = N_crit;

        if (m_p->kerr > 0)
        {
            m_p->n_2 = ct_p->getDoubleValue("n2",0);
            m_p->khi3 = m_p->n_2*I0*2.*m_p->n_0/m_p->density;
            m_p->pcr = 1.8962*1e-8*m_p->wl*m_p->wl*1e-6/ (4*M_PI*m_p->n_0*m_p->n_2)*1e-9;
	}
    }
    else 
    {
        m_p->wl = ct_p->getDoubleValue("m_wl", 800);
        FP freq = 2.35e15*800./m_p->wl; /* 1/s */
        FP N_crit = N_CR*1e6*(freq/2.35e15)*(freq/2.35e15); /* m-3 */

        m_p->density = density;
        m_p->N_crit = N_crit;
        m_p->num_atm_N_cr = N_crit*1e-6/N_1ATM;

        if (m_p->kerr > 0)
        {
            m_p->khi3 = m_p->n_2*I0*2.*m_p->n_0*m_p->num_atm_N_cr;

            m_p->pcr = 1.8962*1e-8*m_p->wl*m_p->wl*1e-6/
                                (4*M_PI*m_p->n_0*m_p->n_2)*
                                1e-9/(m_p->density*m_p->num_atm_N_cr);

            m_p->n_2 *= m_p->density*m_p->num_atm_N_cr; /* "real" n2 */
	}
        else
        {
            m_p->khi3 = 0;
            m_p->n_2 = 0;
            m_p->pcr = 0;
	}
    }

    if (m_p->eps > 1 || m_p->kerr == 1)
    {
        m_p->recalc = true;
    }

    if (0 == media_shape.compare("drop"))
    {
        m_p->media = drop_e;
        m_p->r     = ct_p->getDoubleValue("radius", 10.);

        m_p->x_c = ct_p->getIntValue("x_c", g_p->x_full/2);
        m_p->y_c = ct_p->getIntValue("y_c", g_p->y_full/2);
        m_p->z_c = ct_p->getIntValue("z_c", g_p->z_full/2);
    }
    else if (0 == media_shape.compare("capillar"))
    {
        m_p->media   = cap_e;
        m_p->x_c     = g_p->x_full/2;
        m_p->y_c     = g_p->y_full/2;
        m_p->z_c     = 0;
        m_p->r     = ct_p->getDoubleValue("cap_radius", 10.);
        m_p->wall_eps = ct_p->getDoubleValue("wall_eps", 2.25);

        m_p->ilow = ct_p->getIntValue("cap_x_min", 12);
        m_p->ihi = ct_p->getIntValue("cap_x_max", g_p->x_full - 12);

        m_p->klow = ct_p->getIntValue("cap_z_min", 12);
        m_p->khi = ct_p->getIntValue("cap_z_max", g_p->z_full - 12);

        if (m_p->wall_eps > 1)
        {
            m_p->recalc = true;
        }
    }
    else
    {
        m_p->media = slab_e;
        m_p->ilow = ct_p->getIntValue("slab_x_min", 12);
        m_p->ihi  = ct_p->getIntValue("slab_x_max", g_p->x_full - 12);
        m_p->jlow = ct_p->getIntValue("slab_y_min", 12);
        m_p->jhi  = ct_p->getIntValue("slab_y_max", g_p->y_full - 12);
        m_p->klow = ct_p->getIntValue("slab_z_min", 12);
        m_p->khi  = ct_p->getIntValue("slab_z_max", g_p->z_full - 12);
        m_p->l0   = ct_p->getDoubleValue("slab_l0", 0);
    }

    if (g_p->gtype == THREED_e)
    {
        init_media_3d(ct_p, m_p, g_p, t_p);
    }
    else
    {
        init_media_2d(ct_p, m_p, g_p, t_p);
    }
}

/**  This function initialises media_data structure fields for 3d geometry according to configuration.
 *   \li 1. Function initialises:\n
 *      n_max - neutrals density \n
 *      n - electron density \n
 *      eps - dielectric permittivity \n
  *   \li 2. Depending on media shape function fills in n_max and eps fields.\n
 *   \author  Evgeny Efimenko
 *   \param   ct_p - pointer to configTable structure containing configuration
 *   \param   m_p - pointer to media_data structure
 *   \param   g_p - pointer to geometry_data structure
 *   \param   t_p - pointer to time_scheme structure
 *   \return  void
 *   \callgraph
 *   \callergraph
 */
static void init_media_3d(configTable* ct_p,
                          media_data* m_p,
                          geometry_data* g_p,
                          time_scheme* t_p)
{
    m_p->ncells = g_p->x_max * g_p->y_max * g_p->z_max;
    init_field3d(&m_p->n_max, g_p->x_max, g_p->y_max, g_p->z_max);
    init_field3d(&m_p->epsilon, g_p->x_max, g_p->y_max, g_p->z_max);
    
    if (m_p->type == so_e)
    {
	m_p->kerr_khi = &m_p->khi3;
    }
    
    if (m_p->media == slab_e)
    {
        init_slab_3d(g_p, m_p);
    }
    else if (m_p->media == drop_e)
    {
        init_drop_3d(g_p, m_p);
    }
    else /* capillar */
    {
        init_capillar_3d(g_p, m_p);
    }
#ifdef CUDA
    init_media_3d_cuda(m_p);
#endif
}

/**  This function initialises media_data structure fields for 2d geometry according to configuration.
 *   \li 1. Function initialises:\n
 *      n_max - neutrals density \n
 *      n - electron density \n
 *      eps - dielectric permittivity \n
 *      absorption - absorption of e/m energy due to ionization \n
 *   \li 2. Depending on media shape function fills in n_max and eps fields.\n
 *   \callgraph
 *   \callergraph
 *   @author  Evgeny Efimenko
 *   @param   ct_p - pointer to configTable structure containing configuration
 *   @param   m_p - pointer to media_data structure
 *   @param   g_p - pointer to geometry_data structure
 *   @param   t_p - pointer to time_scheme structure
 *   @return  void
 */
static void init_media_2d(configTable* ct_p,
                          media_data* m_p,
                          geometry_data* g_p,
                          time_scheme* t_p)
{
    init_field(&m_p->n_max, g_p->x_max, g_p->z_max);
    init_field(&m_p->epsilon, g_p->x_max, g_p->z_max);
    
    if (m_p->type == so_e)
    {
	m_p->kerr_khi = &m_p->khi3;
    }
    
    if (m_p->media == slab_e)
    {
        init_slab_2d(g_p, m_p);
    }
    else if (m_p->media == drop_e)
    {
        init_drop_2d(g_p, m_p);
    }
    else /* capillar */
    {
        init_capillar_2d(g_p, m_p);
    }
}

/**  Function deallocates media_data structure fields.
 *   \li 1. Function deallocates:\n
 *      n_max - neutrals density \n
 *      eps - dielectric permittivity \n
 *      absorption - absorption of e/m energy due to ionization \n
 *   \li 2. For tunneling ionization additionally deallocates stage+1 arrays:
 *      nm[0] - neutrals
  *   \author  Evgeny Efimenko
 *   \param   m_p - pointer to media_data structure
 *   \return  void
 *   \callgraph
 *   \callergraph
 */
void destroy_media(media_data* m_p)
{
    destroy_field(&m_p->n_max);
    destroy_field(&m_p->epsilon);

    if (m_p->smoothing ==true)
    {
	destroy_ifield(&m_p->t_eps_idx);
	free(m_p->t_eps);
    }

    if (m_p->type != so_e)
    {
	if (m_p->kerr > 0)
        {
            free(m_p->kerr_khi);
        }
    }
#ifdef CUDA
    destroy_media_3d_cuda(m_p);
#endif
}

/**  Function prints media_data structure fields to stderr.
 *   \author  Evgeny Efimenko
 *   \param   m_p - pointer to media_data structure
 *   \return  void
 *   \callgraph
 *   \callergraph
 */
void print_media(media_data* m_p)
{
    fprintf(stderr,
            "Media properties\n"
            "--------------\n");
    fprintf(stderr,     "Epsilon:              %3.2lf\n",
            m_p->eps);
    fprintf(stderr,     "Refraction index:     %3.2lf\n",
            m_p->n_0);

    if (m_p->kerr >0)
    {
        fprintf(stderr, "Kerr nonlinearity:    on\n");
        fprintf(stderr, "n2:                   %1.2le [cm2/W]\n",
                m_p->n_2);
        if (m_p->n_2 > 0)
        {
            fprintf(stderr, "P critical :          %lf [GW]\n",
                    m_p->pcr);
        }
    }
    else
    {
        fprintf(stderr, "Kerr nonlinearity:    off\n");
    }

    if (m_p->media == drop_e)
    {
        fprintf(stderr, "Shape:                sphere\n");
        fprintf(stderr, "Radius:               %4.3lf\n", m_p->r);
        fprintf(stderr, "x[center]             %d\n", m_p->x_c);
        fprintf(stderr, "y[center]             %d\n", m_p->y_c);
        fprintf(stderr, "z[center]             %d\n", m_p->z_c);
    }
    else if (m_p->media == cap_e)
    {
        fprintf(stderr, "Shape:                waveguide\n");
        fprintf(stderr, "Radius:               %4.3lf\n", m_p->r);
        fprintf(stderr, "Wall epsilon:         %4.3lf\n", m_p->wall_eps);
    }
    else/* slab */
    {
        fprintf(stderr, "Shape:                slab\n");
        fprintf(stderr, "X[low]:               %d\n", m_p->ilow);
        fprintf(stderr, "X[high]:              %d\n", m_p->ihi);
        fprintf(stderr, "Y[low]:               %d\n", m_p->jlow);
        fprintf(stderr, "Y[high]:              %d\n", m_p->jhi);
        fprintf(stderr, "Z[low]:               %d\n", m_p->klow);
        fprintf(stderr, "Z[high]:              %d\n", m_p->khi);
    }
}

/**  Function calculates distance from arbitrary point (x,y,z) to a centre of drop
 *   \author  Evgeny Efimenko
 *   \param   g_p - pointer to geometry_data structure
 *   \param   m_p - pointer to media_data structure
 *   \param   x,y,z - cartesian coordinates of arbitrary point
 *   \return  FP
 *   \callgraph
 *   \callergraph
 */
static FP calc_drop_dist(geometry_data* g_p,
                             media_data* media_p,
                             FP x,
                             FP y,
                             FP z)
{
    return sqrt((x - media_p->x_c*g_p->delta_x)*(x - media_p->x_c*g_p->delta_x) +
                (y - media_p->y_c*g_p->delta_y)*(y - media_p->y_c*g_p->delta_y) +
                (z - media_p->z_c*g_p->delta_z)*(z - media_p->z_c*g_p->delta_z));
}

/**  Function swaps media fields when moving window is used
 *   \author  Evgeny Efimenko
 *   \param   m_p - pointer to media_data structure
 *   \param   g_p - pointer to geometry_data structure
 *   \param   pml_p  - pointer to pml structure
 *   \param   num - number of points to move
 *   \return  FP
 *   \todo  Add envelope, update drop
 *   \callgraph
 *   \callergraph
 */
void swap_media_2d(media_data* m_p,
                   geometry_data* g_p,
                   pml_data* pml_p,
                   int num)
{
    if (m_p->media == slab_e)
    {
        init_slab_2d(g_p, m_p);
    }
    else if (m_p->media == drop_e)
    {
        init_drop_2d(g_p, m_p);
    }
    else /* capillar */
    {
        init_capillar_2d(g_p, m_p);
    }
}

/**  Function swaps media fields when moving window is used for 3D
 *   \author  Evgeny Efimenko
 *   \param   m_p - pointer to media_data structure
 *   \param   g_p - pointer to geometry_data structure
 *   \param   pml_p  - pointer to pml structure
 *   \param   num - number of points to move
 *   \return  FP
 *   \todo  Add envelope, update drop
 *   \callgraph
 *   \callergraph
 */
void swap_media_3d(media_data* m_p,
                   geometry_data* g_p,
                   pml_data* pml_p,
                   int num)
{
    if (m_p->media == slab_e)
    {
        init_slab_3d(g_p, m_p);
    }
    else if (m_p->media == drop_e)
    {
        init_drop_3d(g_p, m_p);
    }
    else /* capillar */
    {
        init_capillar_3d(g_p, m_p);
    }
#ifdef CUDA
    swap_media_3d_cuda(m_p);
#endif
}

/**  Function inits media fields for slab media type for 2D
 *   \author  Evgeny Efimenko
 *   \param   m_p - pointer to media_data structure
 *   \param   g_p - pointer to geometry_data structure
 *   \return  void
 *   \callgraph
 *   \callergraph
 */
static void init_slab_2d(geometry_data* g_p,
                         media_data* m_p)
{
    for (int i = 0; i < g_p->x_max; i++)
    {
        for (int k = 0; k < g_p->z_max; k++)
        {
            int i1 = i + g_p->move_index + g_p->i_offset;
            int k1 = k + g_p->k_offset;

            int index = INDEX2D(m_p->n_max,i,k);
            FP l0 = m_p->l0;

            if (i1 > m_p->ilow  && i1 < m_p->ihi &&
                    k1 > m_p->klow  && k1 < m_p->khi)
            {
                FP profile;
                if (m_p->sshape == sin_e)
                {
                    profile = sin_jet(i1*g_p->delta_x, l0);
                }
                else if (m_p->sshape == par_e)
                {
                    profile = para_jet(i1*g_p->delta_x, l0);
                }
                else
                {
                    profile = tanh_jet(i1*g_p->delta_x,
                                       m_p->ilow*g_p->delta_x,
                                       l0,
                                       (m_p->ihi-m_p->ilow)*g_p->delta_x - 4*l0);
                }
               
                m_p->n_max.a[index] = m_p->density * profile;
                m_p->epsilon.a[index] = 1+(m_p->eps-1) * profile;
            }
            else
            {
                m_p->epsilon.a[index] = 1.;
            }
        }
    }
}

/**  Function inits media fields for slab media type for 3D
 *   \author  Evgeny Efimenko
 *   \param   m_p - pointer to media_data structure
 *   \param   g_p - pointer to geometry_data structure
 *   \return  void
 *   \callgraph
 *   \callergraph
 */
static void init_slab_3d(geometry_data* g_p,
                         media_data* m_p)
{
    for (int k = 0; k < g_p->z_max; k++)
    {
        for (int i = 0; i < g_p->x_max; i++)
        {
            for (int j = 0; j < g_p->y_max; j++)
            {
                int i1 = i + g_p->i_offset;
                int j1 = j + g_p->j_offset;
                int k1 = k + g_p->k_offset + g_p->move_index;
                int index = INDEX((m_p->n_max),i,j,k);
                FP l0 = m_p->l0;

                if (k1 > m_p->klow && k1 < m_p->khi &&
		    j1 > m_p->jlow && j1 < m_p->jhi &&
		    i1 > m_p->ilow && i1 < m_p->ihi)
                {
                    FP profile;

                    if (m_p->sshape == sin_e)
                    {
                        profile = sin_jet(k1*g_p->delta_z, l0);
                    }
                    else if (m_p->sshape == par_e)
                    {
                        profile = para_jet(k1*g_p->delta_z, l0);
                    }
                    else
                    {
                        profile = tanh_jet(k1*g_p->delta_z,
                                           m_p->klow*g_p->delta_z,
                                           l0,
                                           (m_p->khi-m_p->klow)*g_p->delta_z - 4*l0);
                    }
                    m_p->n_max.a[index] = m_p->density*profile;
                    m_p->epsilon.a[index] = 1+(m_p->eps-1)*profile;
                }
                else
                {
                    m_p->n_max.a[index] = 0;
                    m_p->epsilon.a[index] = 1.;
                }
	    }
        }
    }
}

/**  Function inits media fields for capillar media type for 2D
 *   \author  Evgeny Efimenko
 *   \param   m_p - pointer to media_data structure
 *   \param   g_p - pointer to geometry_data structure
 *   \return  void
 *   \callgraph
 *   \callergraph
 */
static void init_capillar_2d(geometry_data* g_p,
                             media_data* m_p)
{
    int temp = (int)(m_p->r/g_p->delta_z);
    if (temp > g_p->z_full/2)
    {
        exit(-1);
    }

    for (int i = 0; i < g_p->x_max; i++)
    {
        for (int k = 0; k < g_p->z_max; k++)
        {
            int i1 = i + g_p->i_offset + g_p->move_index;
            int k1 = k + g_p->k_offset;
            int index = INDEX2D(m_p->n_max,i,k);

            if (i1 > m_p->ilow && i1 < m_p->ihi)
            {
                if (k1 > g_p->z_full/2 + temp ||
                        k1 < g_p->z_full/2 - temp)
                {
                    m_p->epsilon.a[index] = m_p->wall_eps;
                    m_p->n_max.a[index] = 0.;
                }
                else
                {
                    m_p->n_max.a[index] = m_p->density;
                    m_p->epsilon.a[index] = m_p->eps;
                }
            }
            else
            {
                m_p->n_max.a[index] = 0.;
                m_p->epsilon.a[index] = 1.;
            }
	}
    }
}

/**  Function inits media fields for capillar media type for 3D
 *   \author  Evgeny Efimenko
 *   \param   m_p - pointer to media_data structure
 *   \param   g_p - pointer to geometry_data structure
 *   \return  void
 *   \callgraph
 *   \callergraph
 */
static void init_capillar_3d(geometry_data* g_p,
                             media_data* m_p)
{
    point v[4];
    FP vol_full = (DROP_REFINE_VOLUME-1) * (DROP_REFINE_VOLUME-1) ;
    FP rdx = g_p->delta_x/DROP_REFINE_VOLUME;
    FP rdy = g_p->delta_y/DROP_REFINE_VOLUME;

    for (int k = 0; k < g_p->z_max; k++)
    {
        for (int i = 0; i < g_p->x_max; i++)
        {
            for (int j = 0; j < g_p->y_max; j++)
            {
                int i1 = i + g_p->i_offset;
                int j1 = j + g_p->j_offset;
                int k1 = k + g_p->k_offset + g_p->move_index;
                int index = INDEX((m_p->n_max),i,j,k);

                if (k1 > m_p->klow && k1 < m_p->khi)
                {
                    bool in = true;
                    bool out = true;
                    FP dx = i1*g_p->delta_x;
                    FP dy = j1*g_p->delta_y;

                    init_point(&v[0],
                               dx - g_p->delta_x*0.5,
                               dy - g_p->delta_y*0.5,
                               0);
                    init_point(&v[1],
                               dx - g_p->delta_x*0.5,
                               dy + g_p->delta_y*0.5,
                               0);
                    init_point(&v[2],
                               dx + g_p->delta_x*0.5,
                               dy - g_p->delta_y*0.5,
                               0);
                    init_point(&v[3],
                               dx + g_p->delta_x*0.5,
                               dy + g_p->delta_y*0.5,
                               0);
                    
                    for (int m = 0; m<4; m++)
                    {
                        FP r_temp = calc_drop_dist(g_p,
                                                       m_p,
                                                       v[m].x,
                                                       v[m].y,
                                                       0);
                        bool pos_b = (r_temp <= m_p->r);
                        in = in && pos_b;
                        out = out && !pos_b;
		    }
                    
                    if (in == false && out == true)
                    {
                        m_p->n_max.a[index] = 0;
                        m_p->epsilon.a[index] = m_p->wall_eps;
                    }
                    else if (in == true && out == false)
                    {
                        m_p->n_max.a[index] = m_p->density;
                        m_p->epsilon.a[index] = m_p->eps;
                    }
                    else if (in == false && out == false)
                    {
                        FP vol_in = 0;
                        for (int m = 0; m < DROP_REFINE_VOLUME-1; m++)
                        {
                            for (int p = 0; p < DROP_REFINE_VOLUME-1; p++)
                            {
                                FP r_temp = calc_drop_dist(g_p,
                                                               m_p,
                                                               v[0].x + (m+0.5)*rdx,
                                                               v[0].y + (p+0.5)*rdy,
                                                               0);
                                if (r_temp <= m_p->r)
                                {
                                    vol_in++;
                                }
                            }
                        }
                        m_p->n_max.a[index] = 0;
			FP ratio = vol_in/vol_full;
                        m_p->epsilon.a[index] = m_p->eps * ratio + m_p->wall_eps * (1. - ratio);
                        /* boundary */
                    }
                    else
                    {
                        fprintf(stderr, "Something is wrong\n");
                        exit(-1);
                    }
                } /* z */
                else
                {
                    m_p->n_max.a[index] = 0;
                    m_p->epsilon.a[index] = 1.;
                }
	    } /* k */
        } /* j */
    } /* i */
}

/**  Function inits media fields for drop media type for 2D
 *   \author  Evgeny Efimenko
 *   \param   m_p - pointer to media_data structure
 *   \param   g_p - pointer to geometry_data structure
 *   \return  void
 *   \callgraph
 *   \callergraph
 */
static void init_drop_2d(geometry_data* g_p,
                         media_data* m_p)
{
    point v[4];
    FP vol_full = DROP_REFINE_VOLUME *
                      DROP_REFINE_VOLUME;
    FP rdx = g_p->delta_x/DROP_REFINE_VOLUME;
    FP rdz = g_p->delta_z/DROP_REFINE_VOLUME;

    for (int i = 0; i < g_p->x_max; i++)
    {
        for (int k = 0; k < g_p->z_max; k++)
        {
            int index = INDEX2D(m_p->n_max,i,k);
            bool in = true;
            bool out = true;
            FP dx = (i + g_p->i_offset + g_p->move_index)*
                        g_p->delta_x;
            FP dz = (k + g_p->k_offset)*g_p->delta_z;

            init_point(&v[0],
                       dx - g_p->delta_x*0.5,
                       0,
                       dz - g_p->delta_z*0.5);
            init_point(&v[1],
                       dx - g_p->delta_x*0.5,
                       0,
                       dz + g_p->delta_z*0.5);
            init_point(&v[2],
                       dx + g_p->delta_x*0.5,
                       0,
                       dz - g_p->delta_z*0.5);
            init_point(&v[3],
                       dx + g_p->delta_x*0.5,
                       0,
                       dz + g_p->delta_z*0.5);
            FP r_aver = 0;
            for (int m = 0; m<4; m++)
            {
                FP r_temp = calc_drop_dist(g_p,
                                               m_p,
                                               v[m].x,
                                               v[m].y,
                                               v[m].z);
                bool pos_b = (r_temp <= m_p->r);
                r_aver += r_temp;
                in = in && pos_b;
                out = out && !pos_b;
            }
            r_aver *= 0.25;
            if (in == false && out == true)
            {
                m_p->epsilon.a[index] = 1.;
                m_p->n_max.a[index] = 0.;
            }
            else if (in == true && out == false)
            {
                m_p->n_max.a[index] = m_p->density;
                m_p->epsilon.a[index] = m_p->eps;
            }
            else if (in == false && out == false)
            {
                FP vol_in = 0;
                for (int m = 0; m < DROP_REFINE_VOLUME-1; m++)
                {
                    for (int p = 0; p < DROP_REFINE_VOLUME-1; p++)
                    {
                        FP r_temp = calc_drop_dist(g_p,
                                                       m_p,
                                                       v[0].x + (m+0.5)*rdx,
                                                       0,
                                                       v[0].z + (p+0.5)*rdz);
                        if (r_temp <= m_p->r)
                        {
                            vol_in++;
                        }
                    }
                }
                m_p->n_max.a[index] = m_p->density*vol_in/vol_full;
                m_p->epsilon.a[index] = 1. + (m_p->eps - 1.)*vol_in/vol_full;
                /* boundary */
            }
            else
            {
                fprintf(stderr, "Something is wrong\n");
                exit(-1);
            }
	}
    }
}

/**  Function inits media fields for drop media type for 3D
 *   \author  Evgeny Efimenko
 *   \param   m_p - pointer to media_data structure
 *   \param   g_p - pointer to geometry_data structure
 *   \return  void
 *   \callgraph
 *   \callergraph
 */
static void init_drop_3d(geometry_data* g_p,
                         media_data* m_p)
{
    point v[8];
    FP vol_full = (DROP_REFINE_VOLUME-1) *
                      (DROP_REFINE_VOLUME-1) *
                      (DROP_REFINE_VOLUME-1);
    
    FP rdx = g_p->delta_x/DROP_REFINE_VOLUME;
    FP rdy = g_p->delta_y/DROP_REFINE_VOLUME;
    FP rdz = g_p->delta_z/DROP_REFINE_VOLUME;
    
    int num = 0;

    for (int k = 0; k < g_p->z_max; k++)
    {
        for (int i = 0; i < g_p->x_max; i++)
        {
            for (int j = 0; j < g_p->y_max; j++)
            {
                int index = INDEX((m_p->n_max),i,j,k);
                bool in = true;
                bool out = true;
                FP dx = (i + g_p->i_offset)*g_p->delta_x;
                FP dy = (j + g_p->j_offset)*g_p->delta_y;
                FP dz = (k + g_p->move_index + g_p->k_offset)*g_p->delta_z;

                init_point(&v[0],
                           dx - g_p->delta_x*0.5,
                           dy - g_p->delta_y*0.5,
                           dz - g_p->delta_z*0.5);
                init_point(&v[1],
                           dx - g_p->delta_x*0.5,
                           dy - g_p->delta_y*0.5,
                           dz + g_p->delta_z*0.5);
                init_point(&v[2],
                           dx - g_p->delta_x*0.5,
                           dy + g_p->delta_y*0.5,
                           dz - g_p->delta_z*0.5);
                init_point(&v[3],
                           dx - g_p->delta_x*0.5,
                           dy + g_p->delta_y*0.5,
                           dz + g_p->delta_z*0.5);
                init_point(&v[4],
                           dx + g_p->delta_x*0.5,
                           dy - g_p->delta_y*0.5,
                           dz - g_p->delta_z*0.5);
                init_point(&v[5],
                           dx + g_p->delta_x*0.5,
                           dy - g_p->delta_y*0.5,
                           dz + g_p->delta_z*0.5);
                init_point(&v[6],
                           dx + g_p->delta_x*0.5,
                           dy + g_p->delta_y*0.5,
                           dz - g_p->delta_z*0.5);
                init_point(&v[7],
                           dx + g_p->delta_x*0.5,
                           dy + g_p->delta_y*0.5,
                           dz + g_p->delta_z*0.5);

                FP r_aver = 0;
                for (int m = 0; m<8; m++)
                {
                    FP r_temp = calc_drop_dist(g_p,
                                                   m_p,
                                                   v[m].x,
                                                   v[m].y,
                                                   v[m].z);
                    bool pos_b = (r_temp <= m_p->r);
                    in = in && pos_b;
                    out = out && !pos_b;
                    r_aver += r_temp;
                }
                r_aver *= 0.125;

                if (in == false && out == true)
                {
                    m_p->epsilon.a[index] = 1.;
                }
                else if (in == true && out == false)
                {
                    m_p->n_max.a[index] = m_p->density;
                    m_p->epsilon.a[index] = m_p->eps;
                }
                else if (in == false && out == false)
                {
                    FP vol_in = 0;
		    
		    if (m_p->smoothing == true)
		    {
			for (int m = 0; m < DROP_REFINE_VOLUME-1; m++)
			{
			    for (int p = 0; p < DROP_REFINE_VOLUME-1; p++)
			    {
				for (int q = 0; q < DROP_REFINE_VOLUME-1; q++)
				{
				    FP r_temp = calc_drop_dist(g_p,
								   m_p,
								   v[0].x + (m+0.5)*rdx,
								   v[0].y + (p+0.5)*rdy,
								   v[0].z + (q+0.5)*rdz);
				    if (r_temp <= m_p->r)
				    {
					vol_in++;
				    }
				}
			    }
			}
			  
			if (vol_in < vol_full)
			{
			    num++;
			}
		    }
		    
		    FP eps = 1. + (m_p->eps - 1.)*vol_in/vol_full;
		    FP nm = m_p->density*vol_in/vol_full;
		    
                    m_p->n_max.a[index] = nm;
                    m_p->epsilon.a[index] = eps;
		  
                    /* boundary */
                }
                else
                {
                    fprintf(stderr, "Something is wrong\n");
                    exit(-1);
                }

	    }
        }
    }

    if (m_p->smoothing == true)
    {
	if (num > 0)
	{
	    num++;
	    
	    int idx = 1;
	    
	    m_p->t_eps = (tensor_data*)malloc(num*sizeof(tensor_data));
	
	    if (m_p->t_eps == NULL)
	    {
		fprintf(stderr, "Unable to allocate memory\n");
		exit(-1);
	    }
	
	    memset(m_p->t_eps, 0, num*sizeof(tensor_data));
	
	    init_ifield3d(&m_p->t_eps_idx, g_p->x_max, g_p->y_max, g_p->z_max);
    
	    for (int k = 0; k < g_p->z_max; k++)
	    {
		for (int i = 0; i < g_p->x_max; i++)
		{
		    for (int j = 0; j < g_p->y_max; j++)
		    {
			int index = INDEX((m_p->n_max),i,j,k);
			
			if (m_p->epsilon.a[index] > 1 && m_p->epsilon.a[index] < m_p->eps)
			{
			    FP vol_in = 0;
			    FP dx = (i + g_p->i_offset)*g_p->delta_x;
			    FP dy = (j + g_p->j_offset)*g_p->delta_y;
			    FP dz = (k + g_p->move_index + g_p->k_offset)*g_p->delta_z;
			    
			    init_point(&v[0],
				       dx - g_p->delta_x*0.5,
				       dy - g_p->delta_y*0.5,
				       dz - g_p->delta_z*0.5);
			    			    
			    for (int m = 0; m < DROP_REFINE_VOLUME-1; m++)
			    {
				for (int p = 0; p < DROP_REFINE_VOLUME-1; p++)
				{
				    for (int q = 0; q < DROP_REFINE_VOLUME-1; q++)
				    {
					FP r_temp = calc_drop_dist(g_p,
								       m_p,
								       v[0].x + (m+0.5)*rdx,
								       v[0].y + (p+0.5)*rdy,
								       v[0].z + (q+0.5)*rdz);
					if (r_temp <= m_p->r)
					{
					    vol_in++;
					}
				    }
				}
			    }
			    if (vol_in < vol_full)
			    {
				FP ratio = vol_in/vol_full;
				FP eps = (1.- ratio) + m_p->eps * ratio;
				FP inv_eps = (1. - ratio) + 1./m_p->eps * ratio;
				
				FP c_x = m_p->x_c * g_p->delta_x;
				FP c_y = m_p->y_c * g_p->delta_y;
				FP c_z = m_p->z_c * g_p->delta_z;
				
				m_p->t_eps_idx.a[index] = idx;
				init_tensor_data(&m_p->t_eps[idx],
						 dx, dy, dz,
						 c_x, c_y, c_z,
						 eps, inv_eps);
				
				print_tensor_data(&m_p->t_eps[idx]);
				idx++;
			    }
			}
		    }
		}
	    }
	}
	else
	{
	    m_p->smoothing = false;
	}
    }
}

/**  Function is used to calculate epsilon for Kerr media
 *   accounting for multystage ionization
 *   this function is still experimental.
 *   \author  Evgeny Efimenko
 *   \param   m_p - pointer to media_data structure
 *   \param   sq_abs - square of electric field absolute value
 *   \param   index  - point index
 *   \return  FP
 *   \callgraph
 *   \callergraph
 */
FP calculate_epsilon(media_data* media_p,
                         FP sq_abs,
                         int index)
{
    return media_p->epsilon.a[index] + sq_abs*media_p->khi3;
}

static FP tanh_jet(FP z, FP z0, FP zrise, FP zlength)
{
    FP alpha = 2.276/zrise;
    return 0.25*(1. - tanh(-alpha*((z-z0) - zrise))) *
	(1. - tanh(alpha*((z-z0) - 3*zrise - zlength)));
}

static FP para_jet(FP z, FP zlength)
{
    FP ans = 0.;

    if (z <= zlength)
    {
        ans = 4*z*(zlength-z)/(zlength*zlength);;
    }

    return ans;
}

static FP sin_jet(FP z, FP zlength)
{
    FP ans = 0.;

    if (z <= zlength)
    {
        ans = sin(M_PI*z/zlength);
    }

    return ans;
}
