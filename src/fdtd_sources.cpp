#include "fdtd.h"

static void update_pw_field_simple_wave(sources_rt_data* s_p, int t, int n);

static FP envelope3d(pulse_rt_data* pu_p,
			 FP t,
			 FP x,
			 FP y);

static FP gauss_env(envelope_data* env_p, FP t);
static FP tanh_env(envelope_data* env_p, FP t);

static FP calculate_envelope(envelope_data* env_p, FP t);

static void print_pulses(pulses_rt* pulses_p,
			 media_data* media_p,
			 geometry_data* geometry_p,
			 time_scheme* timescheme_p,
			 const char* pathPrefix);
static void print_one_pulse(pulse_rt_data* pulse, media_data* media_p, int i);
static void print_waves(waves_rt* waves,
			time_scheme* timescheme_p,
			const char* pathPrefix);
static FP time_shape_dipole_wave(planewave_rt_data* w_p,
				     FP t,
				     FP d,
				     FP R);

static void print_one_wave(planewave_rt_data* wave, int i);

static FP calc_dist_3d(sources_rt_data* sources_rt_data_p,
			   int k,
			   FP x,
			   FP y,
			   FP z);

static void init_pulses(configTable* ct_p, sources_rt_data* s_p, geometry_data* geometry_p);
static void init_one_pulse(configTable* ct_p,
			   sources_rt_data* s_p,
			   pulse_rt_data* pulse,
			   int i,
			   geometry_data* geometry_p);

static void init_one_pulse_3d(configTable* ct_p,
			      sources_rt_data* s_d_p,
			      pulse_rt_data* pulse,
			      int i,
			      geometry_data* geometry_p);

static void init_one_pulse_2d(configTable* ct_p,
			      sources_rt_data* s_d_p,
			      pulse_rt_data* pulse,
			      int i,
			      geometry_data* geometry_p);

static void init_waves(configTable* ct_p,
		       geometry_data* geometry_p,
		       waves_rt* waves_p,
		       sources_rt_data* s_p);
static void destroy_waves(waves_rt* waves_p);
static void init_one_wave(planewave_rt_data* planewave,
			  sources_rt_data* s_p,
			  geometry_data* geometry_p,
			  FP amp,
			  FP lambda,
			  FP phase,
			  FP phi,
			  FP theta,
			  FP psi,
			  FP t0,
			  FP tp,
			  FP tdelay,
			  FP vf,
			  FP chirp,
			  bool linear,
			  wave_type type,
			  int ntemp);
static void init_one_wave_2d(planewave_rt_data* planewave,
			     geometry_data* geometry_p,
			     int ntemp);
static void init_one_wave_3d(planewave_rt_data* planewave,
			     sources_rt_data* s_p,
			     geometry_data* geometry_p,
			     int ntemp);
static void init_sources_rt_2d(configTable* ct_p,
			       geometry_data* geometry_p,
			       pml_data* pml_p,
			       sources_rt_data* sources_rt_data_p);
static void init_psources_2d(configTable* ct_p,
			     geometry_data* geometry_p,
			     pml_data* pml_p,
			     sources_rt_data* sources_rt_data_p);
static void init_tfsf_2d(configTable* ct_p,
			 geometry_data* geometry_p,
			 pml_data* pml_p,
			 sources_rt_data* sources_rt_data_p);
static void init_sources_rt_3d(configTable* ct_p,
			       geometry_data* geometry_p,
			       pml_data* pml_p,
			       sources_rt_data* sources_rt_data_p,
			       vfield_n* e_p,
			       vfield_n* h_p);
static void init_psources_3d(configTable* ct_p,
			     geometry_data* geometry_p,
			     pml_data* pml_p,
			     sources_rt_data* sources_rt_data_p,
			     vfield_n* e_p,
			     vfield_n* h_p);
static void init_tfsf_3d(configTable* ct_p,
			 geometry_data* geometry_p,
			 pml_data* pml_p,
			 sources_rt_data* sources_rt_data_p,
			 vfield_n* e_p,
			 vfield_n* h_p);
static void destroy_one_wave(planewave_rt_data* w_p);
static void print_one_wave(planewave_rt_data* wave, int i);



static void store_one_pulse(pulse_rt_data* pulse,
			    int i,
			    geometry_data* geometry_p,
			    time_scheme* timescheme_p,
			    const char* pathPrefix);
static void store_one_wave(waves_rt* waves,
			   int i,
			   time_scheme* timescheme_p,
			   const char* pathPrefix);

static void destroy_sources_rt_2d(sources_rt_data* sources_rt_data_p);
static void destroy_sources_rt_3d(sources_rt_data* sources_rt_data_p);
static void destroy_one_pulse(pulse_rt_data* pulse);
static void destroy_pulses(pulses_rt* pulses_p);

static void update_pw_field(sources_rt_data* sources_rt_data_p, int n);

static void update_pw_field_point(sources_rt_data* s_p,
				  int n,
				  int t,
				  int idx);

static FP interpolate_pw_field_e(sources_rt_data* sources_rt_data_p,
				     int n,
				     int t,
				     FP d);
static FP interpolate_pw_field_h(sources_rt_data* sources_rt_data_p,
				     int n,
				     int t,
				     FP d);
static FP interpolate_pw_phase_e(sources_rt_data* s_p,
				     int n,
				     FP d);
static FP interpolate_pw_phase_h(sources_rt_data* s_p,
				     int n,
				     FP d);

static void zero_pulse_currents_3d(sources_rt_data* sources_rt_data_p);
static void zero_tfsf_fields_3d(sources_rt_data* sources_rt_data_p);

static void add_dipole_wave_zmin(sources_rt_data* s_p,
				 geometry_data* g_p,
				 int t,
				 int n);
static void add_dipole_wave_zmax(sources_rt_data* s_p,
				 geometry_data* g_p,
				 int t,
				 int n);
static void add_dipole_wave_ymin(sources_rt_data* s_p,
				 geometry_data* g_p,
				 int t,
				 int n);
static void add_dipole_wave_ymax(sources_rt_data* s_p,
				 geometry_data* g_p,
				 int t,
				 int n);
static void add_dipole_wave_xmin(sources_rt_data* s_p,
				 geometry_data* g_p,
				 int t,
				 int n);
static void add_dipole_wave_xmax(sources_rt_data* s_p,
				 geometry_data* g_p,
				 int t,
				 int n);

static void add_simple_wave_zmin(sources_rt_data* s_p,
				 geometry_data* g_p,
				 int t,
				 int n);
static void add_simple_wave_zmax(sources_rt_data* s_p,
				 geometry_data* g_p,
				 int t,
				 int n);
static void add_simple_wave_ymin(sources_rt_data* s_p,
				 geometry_data* g_p,
				 int t,
				 int n);
static void add_simple_wave_ymax(sources_rt_data* s_p,
				 geometry_data* g_p,
				 int t,
				 int n);
static void add_simple_wave_xmin(sources_rt_data* s_p,
				 geometry_data* g_p,
				 int t,
				 int n);
static void add_simple_wave_xmax(sources_rt_data* s_p,
				 geometry_data* g_p,
				 int t,
				 int n);

static void add_linear_wave_zmin(sources_rt_data* s_p,
				 geometry_data* g_p,
				 int t,
				 int n);
static void add_linear_wave_zmax(sources_rt_data* s_p,
				 geometry_data* g_p,
				 int t,
				 int n);
static void add_linear_wave_ymin(sources_rt_data* s_p,
				 geometry_data* g_p,
				 int t,
				 int n);
static void add_linear_wave_ymax(sources_rt_data* s_p,
				 geometry_data* g_p,
				 int t,
				 int n);
static void add_linear_wave_xmin(sources_rt_data* s_p,
				 geometry_data* g_p,
				 int t,
				 int n);
static void add_linear_wave_xmax(sources_rt_data* s_p,
				 geometry_data* g_p,
				 int t,
				 int n);

static void add_circular_wave_zmin(sources_rt_data* s_p,
				   geometry_data* g_p,
				   int t,
				   int n);
static void add_circular_wave_zmax(sources_rt_data* s_p,
				   geometry_data* g_p,
				   int t,
				   int n);
static void add_circular_wave_ymin(sources_rt_data* s_p,
				   geometry_data* g_p,
				   int t,
				   int n);
static void add_circular_wave_ymax(sources_rt_data* s_p,
				   geometry_data* g_p,
				   int t,
				   int n);
static void add_circular_wave_xmin(sources_rt_data* s_p,
				   geometry_data* g_p,
				   int t,
				   int n);
static void add_circular_wave_xmax(sources_rt_data* s_p,
				   geometry_data* g_p,
				   int t,
				   int n);

FP time_shape(pulse_rt_data* pu_p,
                  FP t,
                  FP x)
{
    FP r = (x - pu_p->x0)/pu_p->f;
    FP td = -pu_p->f*(sqrt(pu_p->fc*pu_p->fc + (r - pu_p->fs)*(r - pu_p->fs)) - 1);
    FP res = pu_p->amp * calculate_envelope(&pu_p->env, t- td - pu_p->tdelay) *
	sin(pu_p->freq*(t - td - pu_p->tdelay));
    return res;
}

FP pw_time_shape(planewave_rt_data* pw_p, FP t)
{
    return pw_p->amplitude * pw_p->env.f_p(&pw_p->env, t - pw_p->tdelay);
}

FP time_shape3d(pulse_rt_data* pu_p,
                    FP t,
                    FP x,
                    FP y)
{
    FP xc = x - pu_p->x0;
    FP yc = y - pu_p->y0;
    FP r = sqrt(xc * xc + yc * yc)/pu_p->f;
    FP td = -pu_p->f * (sqrt(pu_p->fc*pu_p->fc + (r - pu_p->fs)*(r - pu_p->fs)) - 1.);
    FP t1 = t - td - pu_p->tdelay;
    return pu_p->amp * calculate_envelope(&pu_p->env, t1) * sin(pu_p->freq*t1);
}

void init_sources_rt(configTable* ct_p,
                     geometry_data* g_p,
                     pml_data* pml_p,
                     sources_rt_data* s_p,
		     vfield_n* e_p,
		     vfield_n* h_p)
{
    s_p->pulses.num_pulses = ct_p->getIntValue("num_sources", 0);
    if (0 == s_p->pulses.num_pulses)
    {
	s_p->pulses.num_pulses = ct_p->getIntValue("num_pulses", 0);
    }
    s_p->waves.num_waves = ct_p->getIntValue("num_waves", 0);

    s_p->tfsf_xmin = false;
    s_p->tfsf_xmax = false;
    s_p->tfsf_ymin = false;
    s_p->tfsf_ymax = false;
    s_p->tfsf_zmin = false;
    s_p->tfsf_zmax = false;

    s_p->pulse_xmin = false;
    s_p->pulse_xmax = false;
    s_p->pulse_ymin = false;
    s_p->pulse_ymax = false;
    s_p->pulse_zmin = false;
    s_p->pulse_zmax = false;

    if (s_p->waves.num_waves <= 0)
    {
        s_p->tfsf_enable = false;
    }
    else
    {
        s_p->tfsf_enable = true;

    }

    if (s_p->pulses.num_pulses <= 0)
    {
        s_p->pulse_enable = false;
    }
    else
    {
        s_p->pulse_enable = true;

    }

    if (s_p->tfsf_enable == false && s_p->pulse_enable == false)
    {
        fprintf(stderr, "No sources set. Exiting\n");
        exit(-1);
    }

    s_p->delta_t = ct_p->getDoubleValue("delta_t", 0.005);

    if (g_p->gtype == THREED_e)
    {
        init_sources_rt_3d(ct_p, g_p, pml_p, s_p, e_p, h_p);
    }
    else /* 2D geometry */
    {
        init_sources_rt_2d(ct_p, g_p, pml_p, s_p);
    }
}

static void init_sources_rt_2d(configTable* ct_p,
			       geometry_data* g_p,
			       pml_data* pml_p,
			       sources_rt_data* s_p)
{
    init_psources_2d(ct_p, g_p, pml_p, s_p);
    init_tfsf_2d(ct_p, g_p, pml_p, s_p);
}

static void init_psources_2d(configTable* ct_p,
			     geometry_data* g_p,
			     pml_data* pml_p,
			     sources_rt_data* s_p)
{
    if (s_p->pulse_enable == true)
    {
	int xmin = pml_p->pmlx + 1;
	int zmin = pml_p->pmlz + 1;
	
        int pulse_x_min = ct_p->getIntValue("pulse_x_min", xmin);
	int pulse_x_max = ct_p->getIntValue("pulse_x_max", g_p->x_full - xmin);
        int pulse_z_min = ct_p->getIntValue("pulse_z_min", zmin);
	int pulse_z_max = ct_p->getIntValue("pulse_z_max", g_p->z_full - zmin);

        s_p->pulse_x_min = pulse_x_min;
        s_p->pulse_x_max = pulse_x_max;
        s_p->pulse_z_min = pulse_z_min;
        s_p->pulse_z_max = pulse_z_max;

        if (g_p->i_offset < pulse_x_min &&
	    g_p->i_offset + g_p->x_max >= pulse_x_min)
        {
            s_p->pulse_x_min = pulse_x_min - g_p->i_offset;
            s_p->pulse_xmin = true;
            s_p->pulse_ey_xmin = FP_MALLOC(g_p->z_max);
            s_p->pulse_hz_xmin = FP_MALLOC(g_p->z_max);
        }

        if (g_p->i_offset < pulse_x_max &&
	    g_p->i_offset + g_p->x_max >= pulse_x_max)
        {
            s_p->pulse_x_max = pulse_x_max - g_p->i_offset;
            s_p->pulse_xmax = true;
            s_p->pulse_ey_xmax = FP_MALLOC(g_p->z_max);
            s_p->pulse_hz_xmax = FP_MALLOC(g_p->z_max);
        }

        if (g_p->k_offset < pulse_z_min &&
	    g_p->k_offset + g_p->z_max >= pulse_z_min)
        {
            s_p->pulse_z_min = pulse_z_min - g_p->k_offset;
            s_p->pulse_zmin = true;
            s_p->pulse_ey_zmin = FP_MALLOC(g_p->x_max);
            s_p->pulse_hx_zmin = FP_MALLOC(g_p->x_max);
        }

        if (g_p->k_offset < pulse_z_max &&
	    g_p->k_offset + g_p->z_max >= pulse_z_max)
        {
            s_p->pulse_z_max = pulse_z_max - g_p->k_offset;
            s_p->pulse_zmax = true;
            s_p->pulse_ey_zmax = FP_MALLOC(g_p->x_max);
            s_p->pulse_hx_zmax = FP_MALLOC(g_p->x_max);
        }
        init_pulses(ct_p, s_p, g_p);
    }
}

static void init_tfsf_2d(configTable* ct_p,
			 geometry_data* g_p,
			 pml_data* pml_p,
			 sources_rt_data* s_p)
{
    if (s_p->tfsf_enable == true)
    {
	int xmin = pml_p->pmlx + 3;
	int zmin = pml_p->pmlz + 3;
	
        int tfsf_x_min = ct_p->getIntValue("tfsf_x_min", xmin);
	int tfsf_x_max = ct_p->getIntValue("tfsf_x_max", g_p->x_full - xmin);
	int tfsf_z_min = ct_p->getIntValue("tfsf_z_min", zmin);
        int tfsf_z_max = ct_p->getIntValue("tfsf_z_max", g_p->z_full - zmin);
    
        s_p->x_w_full = tfsf_x_max - tfsf_x_min;
        s_p->z_w_full = tfsf_z_max - tfsf_z_min;
        s_p->x_off = 0;
        s_p->z_off = 0;
        s_p->x_min = 0;
        s_p->x_max = g_p->x_max;
        s_p->z_min = 0;
        s_p->z_max = g_p->z_max;

        if (g_p->k_offset + g_p->z_max > tfsf_z_min &&
	    g_p->k_offset < tfsf_z_max)
        {
            if (g_p->i_offset < tfsf_x_min &&
		g_p->i_offset + g_p->x_max > tfsf_x_min)
            {
                s_p->x_min = tfsf_x_min - g_p->i_offset;
                s_p->tfsf_xmin = true;
            }

            if (g_p->i_offset < tfsf_x_max &&
		g_p->i_offset + g_p->x_max > tfsf_x_max)
            {
                s_p->x_max = tfsf_x_max - g_p->i_offset;
                s_p->tfsf_xmax = true;
            }
        }

        if (g_p->i_offset + g_p->x_max > tfsf_x_min &&
	    g_p->i_offset < tfsf_x_max)
        {
            if (g_p->k_offset < tfsf_z_min &&
		g_p->k_offset + g_p->z_max > tfsf_z_min)
            {
                s_p->z_min = tfsf_z_min - g_p->k_offset;
                s_p->tfsf_zmin = true;
            }

            if (g_p->k_offset < tfsf_z_max &&
		g_p->k_offset + g_p->z_max > tfsf_z_max)
            {
                s_p->z_max = tfsf_z_max - g_p->k_offset;
                s_p->tfsf_zmax = true;
            }
        }

        s_p->x_w = s_p->x_max - s_p->x_min;
        s_p->z_w = s_p->z_max - s_p->z_min;

        if (s_p->tfsf_zmin != true && s_p->tfsf_zmax != true)
        {
            s_p->z_min = 0;
            s_p->z_max = g_p->z_max;
        }

        if (s_p->tfsf_xmin != true && s_p->tfsf_xmax != true)
        {
            s_p->x_min = 0;
            s_p->x_max = g_p->x_max;
        }

        int exmax = s_p->x_w;
        int ezmax = s_p->z_w;

        if (s_p->tfsf_xmin != true && s_p->tfsf_xmax == true)
        {
            s_p->x_min = 0;
        }

        if (s_p->tfsf_xmin == true && s_p->tfsf_xmax != true)
        {
            s_p->x_max = g_p->x_max;
        }

        if (s_p->tfsf_zmin != true && s_p->tfsf_zmax == true)
        {
            s_p->z_min = 0;
        }

        if (s_p->tfsf_zmin == true && s_p->tfsf_zmax != true)
        {
            s_p->z_max = g_p->z_max;
        }

        if (g_p->i_offset >= tfsf_x_min)
        {
            if (s_p->tfsf_xmin != true)
            {
                s_p->x_off = g_p->i_offset - tfsf_x_min;
            }
        }

        if (g_p->k_offset >= tfsf_z_min)
        {
            if (s_p->tfsf_zmin != true)
            {
                s_p->z_off = g_p->k_offset - tfsf_z_min;
            }
        }

        s_p->exmax = exmax;
        s_p->ezmax = ezmax;

        if (s_p->tfsf_xmin == true)
        {
            s_p->tfsf_ey_xmin = FP_MALLOC(s_p->ezmax);
            s_p->tfsf_hz_xmin = FP_MALLOC(s_p->ezmax);
        }

        if (s_p->tfsf_xmax == true)
        {
            s_p->tfsf_ey_xmax = FP_MALLOC(s_p->ezmax);
            s_p->tfsf_hz_xmax = FP_MALLOC(s_p->ezmax);
        }

        if (s_p->tfsf_zmin == true)
        {
            s_p->tfsf_ey_zmin = FP_MALLOC(s_p->exmax);
            s_p->tfsf_hx_zmin = FP_MALLOC(s_p->exmax);
        }

        if (s_p->tfsf_zmax == true)
        {
            s_p->tfsf_ey_zmax = FP_MALLOC(s_p->exmax);
            s_p->tfsf_hx_zmax = FP_MALLOC(s_p->exmax);
        }

        FP x_max_tfsf = g_p->delta_x*s_p->x_w_full;
        FP z_max_tfsf = g_p->delta_z*s_p->z_w_full;

        if (NULL == (s_p->vertices = (point*)malloc(NUM_TFSF_2D_POINTS*sizeof(point))))
        {
            fprintf(stderr, "%s:%d Unable to allocate memory\n", __FILE__, __LINE__);
            exit(-1);
        }
	
        init_point(&s_p->vertices[0], 0., 0., 0.);
        init_point(&s_p->vertices[1],x_max_tfsf, 0., 0.);
        init_point(&s_p->vertices[2],x_max_tfsf, 0., z_max_tfsf);
        init_point(&s_p->vertices[3], 0., 0., z_max_tfsf);

        init_waves(ct_p, g_p, &s_p->waves, s_p);
    }
}

static void init_sources_rt_3d(configTable* ct_p,
			       geometry_data* g_p,
			       pml_data* pml_p,
			       sources_rt_data* s_p,
			       vfield_n* e_p,
			       vfield_n* h_p)
{
    init_psources_3d(ct_p, g_p, pml_p, s_p, e_p, h_p);
    init_tfsf_3d(ct_p, g_p, pml_p, s_p, e_p, h_p);
}

static void init_psources_3d(configTable* ct_p,
			     geometry_data* g_p,
			     pml_data* pml_p,
			     sources_rt_data* s_p,
			     vfield_n* e_p,
			     vfield_n* h_p)
{

    field_n* Ey_p = e_p->comp_p[y_e];
    field_n* Hx_p = h_p->comp_p[x_e];
    
    if (s_p->pulse_enable == true)
    {
	int xmin = pml_p->pmlx + 1;
	int ymin = pml_p->pmly + 1;
	int zmin = pml_p->pmlz + 1;
	
        int pulse_x_min = ct_p->getIntValue("pulse_x_min", xmin);
	int pulse_x_max = ct_p->getIntValue("pulse_x_max", g_p->x_full - xmin);
	int pulse_y_min = ct_p->getIntValue("pulse_y_min", ymin);
	int pulse_y_max = ct_p->getIntValue("pulse_y_max", g_p->y_full - ymin);
	int pulse_z_min = ct_p->getIntValue("pulse_z_min", zmin);
	int pulse_z_max = ct_p->getIntValue("pulse_z_max", g_p->z_full - zmin);
	    
        s_p->pulse_x_min = pulse_x_min;
        s_p->pulse_x_max = pulse_x_max;
        s_p->pulse_y_min = pulse_y_min;
        s_p->pulse_y_max = pulse_y_max;
        s_p->pulse_z_min = pulse_z_min;
        s_p->pulse_z_max = pulse_z_max;

        if (g_p->i_offset < pulse_x_min &&
	    g_p->i_offset + g_p->x_max >= pulse_x_min)
        {
            s_p->pulse_x_min = pulse_x_min - g_p->i_offset;
            s_p->pulse_xmin = true;
        }

        if (g_p->i_offset < pulse_x_max &&
	    g_p->i_offset + g_p->x_max >= pulse_x_max)
        {
            s_p->pulse_x_max = pulse_x_max - g_p->i_offset;
            s_p->pulse_xmax = true;
        }


        if (g_p->j_offset < pulse_y_min &&
	    g_p->j_offset + g_p->y_max >= pulse_y_min)
        {
            s_p->pulse_y_min = pulse_y_min - g_p->j_offset;
            s_p->pulse_ymin = true;
        }

        if (g_p->j_offset < pulse_y_max &&
	    g_p->j_offset + g_p->y_max >= pulse_y_max)
        {
            s_p->pulse_y_max = pulse_y_max - g_p->j_offset;
            s_p->pulse_ymax = true;
        }


        if (g_p->k_offset < pulse_z_min &&
	    g_p->k_offset + g_p->z_max >= pulse_z_min)
        {
            s_p->pulse_z_min = pulse_z_min - g_p->k_offset;
            s_p->pulse_zmin = true;
        }

        if (g_p->k_offset < pulse_z_max &&
	    g_p->k_offset + g_p->z_max >= pulse_z_max)
        {
            s_p->pulse_z_max = pulse_z_max - g_p->k_offset;
            s_p->pulse_zmax = true;
        }

        init_pulses(ct_p, s_p, g_p);

        if (s_p->pulse_xmin == true)
        {
            FIELD_MALLOC_AND_INIT(s_p->Jey_xmin_p, g_p->y_max, g_p->z_max, 1);
            FIELD_MALLOC_AND_INIT(s_p->Jez_xmin_p, g_p->y_max, g_p->z_max, 1);
            FIELD_MALLOC_AND_INIT(s_p->Jhz_xmin_p, g_p->y_max, g_p->z_max, 1);
            FIELD_MALLOC_AND_INIT(s_p->Jhy_xmin_p, g_p->y_max, g_p->z_max, 1);
        }

        if (s_p->pulse_xmax == true)
        {
            FIELD_MALLOC_AND_INIT(s_p->Jey_xmax_p, g_p->y_max, g_p->z_max, 1);
            FIELD_MALLOC_AND_INIT(s_p->Jez_xmax_p, g_p->y_max, g_p->z_max, 1);
            FIELD_MALLOC_AND_INIT(s_p->Jhz_xmax_p, g_p->y_max, g_p->z_max, 1);
            FIELD_MALLOC_AND_INIT(s_p->Jhy_xmax_p, g_p->y_max, g_p->z_max, 1);
        }

        if (s_p->pulse_ymin == true)
        {
            FIELD_MALLOC_AND_INIT(s_p->Jex_ymin_p, g_p->x_max, g_p->z_max, 1);
            FIELD_MALLOC_AND_INIT(s_p->Jez_ymin_p, g_p->x_max, g_p->z_max, 1);
            FIELD_MALLOC_AND_INIT(s_p->Jhx_ymin_p, g_p->x_max, g_p->z_max, 1);
            FIELD_MALLOC_AND_INIT(s_p->Jhz_ymin_p, g_p->x_max, g_p->z_max, 1);
        }

        if (s_p->pulse_ymax == true)
        {
            FIELD_MALLOC_AND_INIT(s_p->Jex_ymax_p, g_p->x_max, g_p->z_max, 1);
            FIELD_MALLOC_AND_INIT(s_p->Jez_ymax_p, g_p->x_max, g_p->z_max, 1);
            FIELD_MALLOC_AND_INIT(s_p->Jhx_ymax_p, g_p->x_max, g_p->z_max, 1);
            FIELD_MALLOC_AND_INIT(s_p->Jhz_ymax_p, g_p->x_max, g_p->z_max, 1);
        }

        if (s_p->pulse_zmin == true)
        {
            FIELD_MALLOC_AND_INIT(s_p->Jey_zmin_p, g_p->x_max, g_p->y_max, 1);
            FIELD_MALLOC_AND_INIT(s_p->Jex_zmin_p, g_p->x_max, g_p->y_max, 1);
            FIELD_MALLOC_AND_INIT(s_p->Jhx_zmin_p, g_p->x_max, g_p->y_max, 1);
            FIELD_MALLOC_AND_INIT(s_p->Jhy_zmin_p, g_p->x_max, g_p->y_max, 1);
	    s_p->je_zmin_idx = INT_MALLOC(g_p->x_max*g_p->y_max);
	    s_p->jh_zmin_idx = INT_MALLOC(g_p->x_max*g_p->y_max);
	    s_p->j_zmin = FP_MALLOC(4*g_p->x_max*g_p->y_max);
	    s_p->z_num = g_p->x_max*g_p->y_max;
	    
	    for (int i=0; i<s_p->Jey_zmin_p->nx; i++)
            {
                for (int j=0; j<s_p->Jey_zmin_p->ny; j++)
                {
                    int index_e = INDEX_P(Ey_p, i, j, s_p->pulse_z_min);
		    int index_h = INDEX_P(Hx_p, i, j, s_p->pulse_z_min-1);
		    int index2d = INDEX_XY_P(s_p->Jey_zmin_p,i,j);
                    s_p->je_zmin_idx[index2d] = index_e;
		    s_p->jh_zmin_idx[index2d] = index_h;
                }
            }
	}

        if (s_p->pulse_zmax == true)
        {
            FIELD_MALLOC_AND_INIT(s_p->Jey_zmax_p, g_p->x_max, g_p->y_max, 1);
            FIELD_MALLOC_AND_INIT(s_p->Jex_zmax_p, g_p->x_max, g_p->y_max, 1);
            FIELD_MALLOC_AND_INIT(s_p->Jhx_zmax_p, g_p->x_max, g_p->y_max, 1);
            FIELD_MALLOC_AND_INIT(s_p->Jhy_zmax_p, g_p->x_max, g_p->y_max, 1);
	    s_p->je_zmax_idx = INT_MALLOC(g_p->x_max*g_p->y_max);
	    s_p->jh_zmax_idx = INT_MALLOC(g_p->x_max*g_p->y_max);
	    s_p->j_zmax = FP_MALLOC(4*g_p->x_max*g_p->y_max);
	    s_p->z_num = g_p->x_max*g_p->y_max;

	    for (int i=0; i<s_p->Jey_zmax_p->nx; i++)
            {
                for (int j=0; j<s_p->Jey_zmax_p->ny; j++)
                {
                    int index_e = INDEX_P(Ey_p, i, j, s_p->pulse_z_max);
		    int index_h = INDEX_P(Hx_p, i, j, s_p->pulse_z_max);
		    int index2d = INDEX_XY_P(s_p->Jey_zmax_p,i,j);
                    s_p->je_zmax_idx[index2d] = index_e;
		    s_p->jh_zmax_idx[index2d] = index_h;
                }
            }
        }
    }

#ifdef CUDA
    init_psources_3d_cuda(s_p);
#endif
}

static void init_tfsf_3d(configTable* ct_p,
			 geometry_data* g_p,
			 pml_data* pml_p,
			 sources_rt_data* s_p,
			 vfield_n* e_p,
			 vfield_n* h_p)
{
    field_n* Ex_p = e_p->comp_p[x_e];
    field_n* Ey_p = e_p->comp_p[y_e];
    field_n* Ez_p = e_p->comp_p[z_e];
    field_n* Hx_p = h_p->comp_p[x_e];
    field_n* Hy_p = h_p->comp_p[y_e];
     
    if (s_p->tfsf_enable == true)
    {
	int xmin = pml_p->pmlx + 3;
	int ymin = pml_p->pmly + 3;
	int zmin = pml_p->pmlz + 3;
	
        int tfsf_x_min = ct_p->getIntValue("tfsf_x_min", xmin);
	int tfsf_x_max = ct_p->getIntValue("tfsf_x_max", g_p->x_full - xmin);
	int tfsf_y_min = ct_p->getIntValue("tfsf_y_min", ymin);
	int tfsf_y_max = ct_p->getIntValue("tfsf_y_max", g_p->y_full - ymin);
	int tfsf_z_min = ct_p->getIntValue("tfsf_z_min", zmin);
	int tfsf_z_max = ct_p->getIntValue("tfsf_z_max", g_p->z_full - zmin);

        s_p->x_w_full = tfsf_x_max - tfsf_x_min;
        s_p->y_w_full = tfsf_y_max - tfsf_y_min;
        s_p->z_w_full = tfsf_z_max - tfsf_z_min;
        s_p->x_off = 0;
        s_p->y_off = 0;
        s_p->z_off = 0;
        s_p->x_min = 0;
        s_p->x_max = g_p->x_max;
        s_p->y_min = 0;
        s_p->y_max = g_p->y_max;
        s_p->z_min = 0;
        s_p->z_max = g_p->z_max;

        if (g_p->j_offset + g_p->y_max > tfsf_y_min &&
	    g_p->j_offset < tfsf_y_max &&
	    g_p->k_offset + g_p->z_max > tfsf_z_min &&
	    g_p->k_offset < tfsf_z_max)
        {
            if (g_p->i_offset < tfsf_x_min &&
		g_p->i_offset + g_p->x_max > tfsf_x_min)
            {
                s_p->x_min = tfsf_x_min - g_p->i_offset;
                s_p->tfsf_xmin = true;
            }

            if (g_p->i_offset < tfsf_x_max &&
		g_p->i_offset + g_p->x_max > tfsf_x_max)
            {
                s_p->x_max = tfsf_x_max - g_p->i_offset;
                s_p->tfsf_xmax = true;
            }
        }

        if (g_p->i_offset + g_p->x_max > tfsf_x_min &&
	    g_p->i_offset < tfsf_x_max &&
	    g_p->k_offset + g_p->z_max > tfsf_z_min &&
	    g_p->k_offset < tfsf_z_max)
        {

            if (g_p->j_offset < tfsf_y_min &&
		g_p->j_offset + g_p->y_max > tfsf_y_min)
            {
                s_p->y_min = tfsf_y_min - g_p->j_offset;
                s_p->tfsf_ymin = true;
            }

            if (g_p->j_offset < tfsf_y_max &&
		g_p->j_offset + g_p->y_max > tfsf_y_max)
            {
                s_p->y_max = tfsf_y_max - g_p->j_offset;
                s_p->tfsf_ymax = true;
            }
        }

        if (g_p->j_offset + g_p->y_max > tfsf_y_min &&
	    g_p->j_offset < tfsf_y_max &&
	    g_p->i_offset + g_p->x_max > tfsf_x_min &&
	    g_p->i_offset < tfsf_x_max)
        {
            if (g_p->k_offset < tfsf_z_min &&
		g_p->k_offset + g_p->z_max > tfsf_z_min)
            {
                s_p->z_min = tfsf_z_min - g_p->k_offset;
                s_p->tfsf_zmin = true;
            }

            if (g_p->k_offset < tfsf_z_max &&
		g_p->k_offset + g_p->z_max > tfsf_z_max)
            {
                s_p->z_max = tfsf_z_max - g_p->k_offset;
                s_p->tfsf_zmax = true;
            }
        }

        s_p->x_w = s_p->x_max - s_p->x_min;
        s_p->y_w = s_p->y_max - s_p->y_min;
        s_p->z_w = s_p->z_max - s_p->z_min;

        if (s_p->tfsf_zmin != true && s_p->tfsf_zmax != true)
        {
            s_p->z_min = 0;
            s_p->z_max = g_p->z_max;
        }

        if (s_p->tfsf_ymin != true && s_p->tfsf_ymax != true)
        {
            s_p->y_min = 0;
            s_p->y_max = g_p->y_max;
        }

        if (s_p->tfsf_xmin != true && s_p->tfsf_xmax != true)
        {
            s_p->x_min = 0;
            s_p->x_max = g_p->x_max;
        }

        if (s_p->tfsf_xmin != true && s_p->tfsf_xmax == true)
        {
            s_p->x_min = 0;
        }

        if (s_p->tfsf_xmin == true && s_p->tfsf_xmax != true)
        {
            s_p->x_max = g_p->x_max;
        }

        if (s_p->tfsf_ymin != true && s_p->tfsf_ymax == true)
        {
            s_p->y_min = 0;
        }

        if (s_p->tfsf_ymin == true && s_p->tfsf_ymax != true)
        {
            s_p->y_max = g_p->y_max;
        }

        if (s_p->tfsf_zmin != true && s_p->tfsf_zmax == true)
        {
            s_p->z_min = 0;
        }

        if (s_p->tfsf_zmin == true && s_p->tfsf_zmax != true)
        {
            s_p->z_max = g_p->z_max;
        }

        if (g_p->i_offset >= tfsf_x_min)
        {
            if (s_p->tfsf_xmin != true)
            {
                s_p->x_off = g_p->i_offset - tfsf_x_min;
            }
        }

        if (g_p->j_offset >= tfsf_y_min)
        {
            if (s_p->tfsf_ymin != true)
            {
                s_p->y_off = g_p->j_offset - tfsf_y_min;
            }
        }

        if (g_p->k_offset >= tfsf_z_min)
        {
            if (s_p->tfsf_zmin != true)
            {
                s_p->z_off = g_p->k_offset - tfsf_z_min;
            }
        }
	
	if (0 == ct_p->getStringValue("tfsf_zmax", "on").compare("off"))
	{
	    RANK_S(0)
		fprintf(stderr, "TFSF zmax is off\n");
	    RANK_F(0)
		s_p->tfsf_zmax = false; 
	}
	if (0 == ct_p->getStringValue("tfsf_zmin", "on").compare("off"))
	{
	    RANK_S(0)
		fprintf(stderr, "TFSF zmin is off\n");
	    RANK_F(0)
		s_p->tfsf_zmin = false; 
	}
	if (0 == ct_p->getStringValue("tfsf_ymax", "on").compare("off"))
	{
	    RANK_S(0)
		fprintf(stderr, "TFSF ymax is off\n");
	    RANK_F(0)
		s_p->tfsf_ymax = false; 
	}
	if (0 == ct_p->getStringValue("tfsf_ymin", "on").compare("off"))
	{
	    RANK_S(0)
		fprintf(stderr, "TFSF ymin is off\n");
	    RANK_F(0)
		s_p->tfsf_ymin = false; 
	}
	if (0 == ct_p->getStringValue("tfsf_xmax", "on").compare("off"))
	{
	    RANK_S(0)
		fprintf(stderr, "TFSF xmax is off\n");
	    RANK_F(0)
		s_p->tfsf_xmax = false; 
	}
	if (0 == ct_p->getStringValue("tfsf_xmin", "on").compare("off"))
	{
	    RANK_S(0)
		fprintf(stderr, "TFSF xmin is off\n");
	    RANK_F(0)
		s_p->tfsf_xmin = false; 
	}
	
        init_waves(ct_p, g_p, &s_p->waves, s_p);

	int exmax = s_p->x_w;
        int eymax = s_p->y_w;
        int ezmax = s_p->z_w;
	
        if (s_p->tfsf_xmin == true)
        {
	    FIELD_MALLOC_AND_INIT(s_p->tfsf_ey_xmin_p, eymax, ezmax, 1);
            FIELD_MALLOC_AND_INIT(s_p->tfsf_ez_xmin_p, eymax, ezmax, 1);
            FIELD_MALLOC_AND_INIT(s_p->tfsf_hy_xmin_p, eymax, ezmax, 1);
            FIELD_MALLOC_AND_INIT(s_p->tfsf_hz_xmin_p, eymax, ezmax, 1);
	    s_p->wx_num = eymax*ezmax;
	    s_p->we_xmin_idx = INT_MALLOC(s_p->wx_num);
	    s_p->wh_xmin_idx = INT_MALLOC(s_p->wx_num);
	    s_p->w_xmin = FP_MALLOC(4*s_p->wx_num);
	    	    
	    for (int j=0; j<s_p->tfsf_hz_xmin_p->nx; j++)
            {
                for (int k=0; k<s_p->tfsf_hz_xmin_p->ny; k++)
                {
                    int index_e = INDEX_P(Ey_p, s_p->x_min, j+s_p->y_min, k+s_p->z_min);
		    int index_h = INDEX_P(Hy_p, s_p->x_min-1, j+s_p->y_min, k+s_p->z_min);
                    int index2d = INDEX_XY_P(s_p->tfsf_hz_xmin_p, j,k);
                    s_p->we_xmin_idx[index2d] = index_e;
                    s_p->wh_xmin_idx[index2d] = index_h;
                }
            }
        }

        if (s_p->tfsf_xmax == true)
        {
            FIELD_MALLOC_AND_INIT(s_p->tfsf_ey_xmax_p, eymax, ezmax, 1);
            FIELD_MALLOC_AND_INIT(s_p->tfsf_ez_xmax_p, eymax, ezmax, 1);
            FIELD_MALLOC_AND_INIT(s_p->tfsf_hy_xmax_p, eymax, ezmax, 1);
            FIELD_MALLOC_AND_INIT(s_p->tfsf_hz_xmax_p, eymax, ezmax, 1);
	    s_p->wx_num = eymax*ezmax;
	    s_p->we_xmax_idx = INT_MALLOC(s_p->wx_num);
	    s_p->wh_xmax_idx = INT_MALLOC(s_p->wx_num);
	    s_p->w_xmax = FP_MALLOC(4*s_p->wx_num);
	    
	    
	    for (int j=0; j<s_p->tfsf_hz_xmax_p->nx; j++)
            {
                for (int k=0; k<s_p->tfsf_hz_xmax_p->ny; k++)
                {
                    int index_e = INDEX_P(Ez_p, s_p->x_max, j+s_p->y_min, k+s_p->z_min);
		    int index_h = INDEX_P(Hy_p, s_p->x_max-1, j+s_p->y_min, k+s_p->z_min);
                    int index2d = INDEX_XY_P(s_p->tfsf_hz_xmax_p, j,k);
                    s_p->we_xmax_idx[index2d] = index_e;
                    s_p->wh_xmax_idx[index2d] = index_h;
                }
            }
        }

        if (s_p->tfsf_ymin == true)
        {
            FIELD_MALLOC_AND_INIT(s_p->tfsf_ex_ymin_p, exmax, ezmax, 1);
            FIELD_MALLOC_AND_INIT(s_p->tfsf_ez_ymin_p, exmax, ezmax, 1);
            FIELD_MALLOC_AND_INIT(s_p->tfsf_hx_ymin_p, exmax, ezmax, 1);
            FIELD_MALLOC_AND_INIT(s_p->tfsf_hz_ymin_p, exmax, ezmax, 1);
	    s_p->wy_num = exmax*ezmax;
	    s_p->we_ymin_idx = INT_MALLOC(s_p->wy_num);
	    s_p->wh_ymin_idx = INT_MALLOC(s_p->wy_num);
	    s_p->w_ymin = FP_MALLOC(4*s_p->wy_num);
	    
	    for (int i=0; i<s_p->tfsf_hz_ymin_p->nx; i++)
            {
                for (int k=0; k<s_p->tfsf_hz_ymin_p->ny; k++)
                {
                    int index_e = INDEX_P(Ex_p, i+s_p->x_min, s_p->y_min, k+s_p->z_min);
		    int index_h = INDEX_P(Hx_p, i+s_p->x_min, s_p->y_min-1, k+s_p->z_min);
                    int index2d = INDEX_XY_P(s_p->tfsf_hz_ymin_p, i,k);
                    s_p->we_ymin_idx[index2d] = index_e;
		    s_p->wh_ymin_idx[index2d] = index_h;
                }
            }
        }

        if (s_p->tfsf_ymax == true)
        {
            FIELD_MALLOC_AND_INIT(s_p->tfsf_ex_ymax_p, exmax, ezmax, 1);
            FIELD_MALLOC_AND_INIT(s_p->tfsf_ez_ymax_p, exmax, ezmax, 1);
            FIELD_MALLOC_AND_INIT(s_p->tfsf_hx_ymax_p, exmax, ezmax, 1);
            FIELD_MALLOC_AND_INIT(s_p->tfsf_hz_ymax_p, exmax, ezmax, 1);
	    s_p->wy_num = exmax*ezmax;
	    s_p->we_ymax_idx = INT_MALLOC(s_p->wy_num);
	    s_p->wh_ymax_idx = INT_MALLOC(s_p->wy_num);
	    s_p->w_ymax = FP_MALLOC(4*s_p->wy_num);
	    
	    for (int i=0; i<s_p->tfsf_hz_ymax_p->nx; i++)
            {
                for (int k=0; k<s_p->tfsf_hz_ymax_p->ny; k++)
                {
                    int index_e = INDEX_P(Ex_p, i+s_p->x_min, s_p->y_max, k+s_p->z_min);
		    int index_h = INDEX_P(Hx_p, i+s_p->x_min, s_p->y_max-1, k+s_p->z_min);
                    int index2d = INDEX_XY_P(s_p->tfsf_hz_ymax_p, i,k);
                    s_p->we_ymax_idx[index2d] = index_e;
		    s_p->wh_ymax_idx[index2d] = index_h;
                }
            }
        }

        if (s_p->tfsf_zmin == true)
        {
            FIELD_MALLOC_AND_INIT(s_p->tfsf_ex_zmin_p, exmax, eymax, 1);
            FIELD_MALLOC_AND_INIT(s_p->tfsf_ey_zmin_p, exmax, eymax, 1);
            FIELD_MALLOC_AND_INIT(s_p->tfsf_hx_zmin_p, exmax, eymax, 1);
            FIELD_MALLOC_AND_INIT(s_p->tfsf_hy_zmin_p, exmax, eymax, 1);
	    s_p->wz_num = exmax*eymax;
	    s_p->we_zmin_idx = INT_MALLOC(s_p->wz_num);
	    s_p->wh_zmin_idx = INT_MALLOC(s_p->wz_num);
	    s_p->w_zmin = FP_MALLOC(4*s_p->wz_num);
	   
	    for (int i=0; i<s_p->tfsf_hy_zmin_p->nx; i++)
            {
                for (int j=0; j<s_p->tfsf_hy_zmin_p->ny; j++)
                {
                    int index_e = INDEX_P(Ex_p, i+s_p->x_min, j+s_p->y_min,s_p->z_min);
		    int index_h = INDEX_P(Hx_p, i+s_p->x_min, j+s_p->y_min, s_p->z_min-1);
                    int index2d = INDEX_XY_P(s_p->tfsf_hy_zmin_p, i,j);
                    s_p->we_zmin_idx[index2d] = index_e;
		    s_p->wh_zmin_idx[index2d] = index_h;
                }
            }
        }

        if (s_p->tfsf_zmax == true)
        {
            FIELD_MALLOC_AND_INIT(s_p->tfsf_ex_zmax_p, exmax, eymax, 1);
            FIELD_MALLOC_AND_INIT(s_p->tfsf_ey_zmax_p, exmax, eymax, 1);
            FIELD_MALLOC_AND_INIT(s_p->tfsf_hx_zmax_p, exmax, eymax, 1);
            FIELD_MALLOC_AND_INIT(s_p->tfsf_hy_zmax_p, exmax, eymax, 1);
	    s_p->wz_num = exmax*eymax;
	    s_p->we_zmax_idx = INT_MALLOC(s_p->wz_num);
	    s_p->wh_zmax_idx = INT_MALLOC(s_p->wz_num);
	    s_p->w_zmax = FP_MALLOC(4*s_p->wz_num);
	    
	    for (int i=0; i<s_p->tfsf_hy_zmax_p->nx; i++)
            {
                for (int j=0; j<s_p->tfsf_hy_zmax_p->ny; j++)
                {
                    int index_e = INDEX_P(Ex_p, i+s_p->x_min, j+s_p->y_min,s_p->z_max);
		    int index_h = INDEX_P(Hx_p, i+s_p->x_min, j+s_p->y_min,s_p->z_max-1);
                    int index2d = INDEX_XY_P(s_p->tfsf_hy_zmax_p, i,j);
                    s_p->we_zmax_idx[index2d] = index_e;
		    s_p->wh_zmax_idx[index2d] = index_h;
                }
            }
	}

#ifdef CUDA
	init_tfsf_3d_cuda(s_p);
#endif
	
        if (NULL == (s_p->vertices = (point*)malloc(NUM_TFSF_3D_POINTS*sizeof(point))))
        {
            fprintf(stderr, "%s:%d Unable to allocate memory\n", __FILE__, __LINE__);
            exit(-1);
        }

        FP x_max_tfsf = g_p->delta_x*(s_p->x_w_full+1);
        FP y_max_tfsf = g_p->delta_y*(s_p->y_w_full+1);
        FP z_max_tfsf = g_p->delta_z*(s_p->z_w_full+1);

        init_point(&s_p->vertices[0],0,0,0);
        init_point(&s_p->vertices[1],x_max_tfsf,0,0);
        init_point(&s_p->vertices[2],x_max_tfsf,y_max_tfsf,0);
        init_point(&s_p->vertices[3],0,y_max_tfsf,0);
        init_point(&s_p->vertices[4],0,0,z_max_tfsf);
        init_point(&s_p->vertices[5],x_max_tfsf,0,z_max_tfsf);
        init_point(&s_p->vertices[6],x_max_tfsf,y_max_tfsf,z_max_tfsf);
        init_point(&s_p->vertices[7],0,y_max_tfsf,z_max_tfsf);
    }
}

/** \brief Initialises structures for 2D pulse
    @author Evgeny Efimenko
    @param configTable* ct_p - pointer to config
    @param sources_rt_data* s_d_p,
    @param pulse_rt_data* pulse,
    @param int i,
    @param geometry_data* g_p
    @todo check correctness for different space steps
    \bug Possibly incorrect work for different space steps in different direction. Corrected.
*/

static void init_one_pulse_2d(configTable* ct_p,
			      sources_rt_data* s_d_p,
			      pulse_rt_data* pulse,
			      int i,
			      geometry_data* g_p)
{
    char temp[1024];
    pulse->threed = false;

    if (pulse->plane == xmin_e || pulse->plane == xmax_e)
    {
        sprintf(temp,"z0_%d",i);
        pulse->x0 = ct_p->getDoubleValue(temp, 0.5*g_p->delta_z*g_p->z_full);
        
        pulse->x_max = g_p->z_max;
        pulse->delta_x = g_p->delta_z;
        pulse->delta_s = g_p->delta_x;
    }
    else
    {
        sprintf(temp,"x0_%d",i);
        pulse->x0 = ct_p->getDoubleValue(temp, 0.5*g_p->delta_x*g_p->x_full);
        
        pulse->x_max = g_p->x_max;
        pulse->delta_x = g_p->delta_x;
        pulse->delta_s = g_p->delta_z;
    }

    if ((s_d_p->pulse_xmin == true && pulse->plane == xmin_e) ||
	(s_d_p->pulse_xmax == true && pulse->plane == xmax_e))
    {
        pulse->space_shape_array = FP_MALLOC(pulse->x_max);
        for (int k = 0; k < pulse->x_max; k++)
        {
            pulse->space_shape_array[k] =
                space_shape((k+g_p->k_offset)*pulse->delta_x,
                            pulse->x0,
                            pulse->w0/cos(M_PI*pulse->angle/180.));
        }
    }
    else if ((s_d_p->pulse_zmin == true && pulse->plane == zmin_e) ||
             (s_d_p->pulse_zmax == true && pulse->plane == zmax_e))
    {
        pulse->space_shape_array = FP_MALLOC(pulse->x_max);
        for (int i = 0; i < pulse->x_max; i++)
        {
            pulse->space_shape_array[i] =
                space_shape((i+g_p->i_offset)*pulse->delta_x,
                            pulse->x0,
                            pulse->w0/cos(M_PI*pulse->angle/180.));

        }
    }
    else
    {
        pulse->space_shape_array = NULL;
    }

    sprintf(temp,"amptype_%d",i);
    string p  = ct_p->getStringValue(temp, "old");

    if (p.compare("new") == 0)
    {
        pulse->amp = pulse->amp*(0.25/(M_PI*pulse->delta_s));
    }

    /* Pulse power [GW] */
    pulse->a0 = pulse->amp/(0.25/(M_PI*pulse->delta_s));
    pulse->power = 0.5482F*pulse->w0*pulse->w0*pulse->a0*pulse->a0;
    pulse->energy = pulse->power*pulse->env.duration*sqrt(0.5*M_PI)*1e9;
    pulse->focus = 0.;
    pulse->fgain = 0.;
    pulse->fspot = 0.;
    pulse->famp = 0.;
    pulse->fint = 0.;
}

/** \brief Initialises structures for 3D pulse
    @author Evgeny Efimenko
    @param configTable* ct_p - pointer to config
    @param sources_rt_data* s_d_p,
    @param pulse_rt_data* pulse,
    @param int i,
    @param geometry_data* g_p
    @todo check correctness for different space steps
    \bug Possibly incorrect work for different space steps in different direction. Unchecked
*/
static void init_one_pulse_3d(configTable* ct_p,
			      sources_rt_data* s_d_p,
			      pulse_rt_data* pulse,
			      int i,
			      geometry_data* g_p)
{
    char temp[1024];
    pulse->threed = true;
    pulse->delta_x = g_p->delta_x;
    pulse->delta_y = g_p->delta_y;
    pulse->delta_s = g_p->delta_z;

    sprintf(temp,"polarization_%d",i);

    string p  = ct_p->getStringValue(temp, "linear");

    if (p.compare("linear") == 0)
    {
        pulse->pol = linear_e;
        sprintf(temp,"psi_%d",i);
        FP psi = ct_p->getDoubleValue(temp, 0.);
        pulse->sin_psi = sin(psi*M_PI/180.);
        pulse->cos_psi = sin((90.-psi)*M_PI/180.);
    }
    else if (p.compare("circular") == 0)
    {
        pulse->pol = circular_e;
    }
    else
    {
        fprintf(stderr, "Unknown polarization\n");
        exit(-1);
    }
    pulse->x0 = 0;
    pulse->y0 = 0;
    if(pulse->plane == zmin_e || pulse->plane == zmax_e)
    {
        sprintf(temp,"x0_%d",i);
        pulse->x0 = ct_p->getDoubleValue(temp, 0.5*g_p->delta_x*g_p->x_full);
        
        sprintf(temp,"y0_%d",i);
        pulse->y0 = ct_p->getDoubleValue(temp, 0.5*g_p->delta_y*g_p->y_full);
    }

    if ((s_d_p->pulse_zmin == true && pulse->plane == zmin_e) ||
	(s_d_p->pulse_zmax == true && pulse->plane == zmax_e))
    {
        FIELD_MALLOC_AND_INIT(pulse->space_shape_array_ey,
                              g_p->x_max,
                              g_p->y_max,
                              1);

        FIELD_MALLOC_AND_INIT(pulse->space_shape_array_ex,
                              g_p->x_max,
                              g_p->y_max,
                              1);

        for (int i=0; i<pulse->space_shape_array_ey->nx; i++)
        {
            for (int j=0; j<pulse->space_shape_array_ey->ny; j++)
            {
                FP x = (i+g_p->i_offset)*pulse->delta_x;
                FP y = (j+g_p->j_offset+0.5)*pulse->delta_y;
                pulse->space_shape_array_ey->a[INDEX_XY_P(pulse->space_shape_array_ey,i,j)] =
                    space_shape3d(x,
                                  pulse->x0,
                                  y,
                                  pulse->y0,
                                  pulse->w0);

            }
        }

        for (int i=0; i<pulse->space_shape_array_ex->nx; i++)
        {
            for (int j=0; j<pulse->space_shape_array_ex->ny; j++)
            {
                FP x = (i+g_p->i_offset+0.5)*pulse->delta_x;
                FP y = (j+g_p->j_offset)*pulse->delta_y;
                pulse->space_shape_array_ex->a[INDEX_XY_P(pulse->space_shape_array_ex,i,j)] =
                    space_shape3d(x,
                                  pulse->x0,
                                  y,
                                  pulse->y0,
                                  pulse->w0);

            }
        }
    }
    else
    {
        pulse->space_shape_array_ey = NULL;
        pulse->space_shape_array_ex = NULL;
    }
    
    sprintf(temp,"set_position_%d",i);
    p = ct_p->getStringValue(temp, "input");

    if (p.compare("focal") == 0)
    {
	/* values are set for focal spot
	   should be recalculated to input */
	FP wf = pulse->w0/sqrt(2*log(2.));
	pulse->zr = M_PI*wf*wf/pulse->lambda;
	pulse->fgain = sqrt(1 + pulse->f*pulse->f/(pulse->zr*pulse->zr));
	FP w0 = wf*pulse->fgain;
	FP zri = M_PI*w0*w0/pulse->lambda;
	FP a = zri*zri/(2.*pulse->f);
	FP det = a*a - zri*zri;
	if (det < 0)
	{
	    fprintf(stderr, "Wrong focusing\n");
	    exit(-1);
	}
	pulse->f = a - sqrt(det);
        pulse->amp = pulse->amp/pulse->fgain*(0.25/(M_PI*pulse->delta_s));
	pulse->w0 = w0/sqrt(2.);
    } 
    else if (p.compare("input") == 0)
    {
        sprintf(temp,"amptype_%d",i);
	p  = ct_p->getStringValue(temp, "old");
	
	if (p.compare("new") == 0)
	{
	    pulse->amp = pulse->amp*(0.25/(M_PI*pulse->delta_s));
	}
    }
    else
    {
	fprintf(stderr, "Unknown set position\n");
	exit(-1);
    }

    pulse->a0 = pulse->amp/(0.25/(M_PI*pulse->delta_s)); /* actual amplitude at the entrance */
    pulse->power = pulse->w0*pulse->w0*pulse->a0*pulse->a0*I0*M_PI*1e-8*1e-9; //GW
    pulse->energy = pulse->power*pulse->env.duration*0.5*sqrt(M_PI/log(2.))*1e9; //J
	
    FP w0 = pulse->w0*sqrt(2.); //a0*exp(-r^2/w0^2)
    pulse->zr = M_PI*w0*w0/pulse->lambda;
    pulse->focus = pulse->f/(1. + pulse->f*pulse->f/(pulse->zr*pulse->zr));
    pulse->fgain = 1./sqrt( (1.- pulse->focus/pulse->f)*(1- pulse->focus/pulse->f) +
			    pulse->focus*pulse->focus/(pulse->zr*pulse->zr) );
    pulse->fspot = w0*sqrt(2*log(2.))/pulse->fgain;
    pulse->famp = pulse->fgain*pulse->a0;
    pulse->fint = pulse->famp*pulse->famp*I0;
}

static void init_one_pulse(configTable* ct_p,
			   sources_rt_data* s_d_p,
			   pulse_rt_data* pulse,
			   int i,
			   geometry_data* g_p)
{
    char temp[1024];

    pulse->delta_x = g_p->delta_z;
    pulse->delta_y = g_p->delta_y;

    sprintf(temp,"waist_%d",i);
    pulse->w0 = ct_p->getDoubleValue(temp, 1.5);

    sprintf(temp,"t0_%d",i);
    pulse->env.t0 = ct_p->getDoubleValue(temp, 10.);

    pulse->env.alpha = 2.276/pulse->env.t0;

    sprintf(temp,"tp_%d",i);
    pulse->env.tp = ct_p->getDoubleValue(temp, 10.);

    sprintf(temp,"tdelay_%d",i);
    pulse->tdelay = ct_p->getDoubleValue(temp, 10.);

    sprintf(temp, "envelope_%d", i);

    if (ct_p->getStringValue(temp, "gauss").compare("tanh") == 0)
    {
        pulse->env.f_p = &tanh_env;
        pulse->env.type = tanh_e;
        pulse->env.duration = (2*pulse->env.t0+pulse->env.tp)*3.33e-15;
    }
    else
    {
        pulse->env.f_p = &gauss_env;
        pulse->env.type = gauss_e;
        pulse->env.duration = pulse->env.tp*1.e-15;
        pulse->tdelay += 2*pulse->env.tp/3.33;
    }

    sprintf(temp,"amplitude_%d",i);
    pulse->amp = ct_p->getDoubleValue(temp);

    if (pulse->amp == -1)
    {
	sprintf(temp,"intensity_%d",i);
	FP intensity = ct_p->getDoubleValue(temp, 1e14);
	if (intensity < 0)
	{
	    fprintf(stderr, "Wrong parameters, should be set either amplitude, or intensity\n");
	    pulse->amp = 0;
	    exit(-1);
	}
	else
	{
	    pulse->amp = sqrt(intensity/3.61e16);
	}
    }

    sprintf(temp,"focallength_%d",i);
    pulse->f = ct_p->getDoubleValue(temp, 1e9);

    sprintf(temp,"wavelength_%d",i);
    pulse->lambda = ct_p->getDoubleValue(temp, 0.8);
    pulse->freq = 2*M_PI/pulse->lambda;

    sprintf(temp,"angle_%d",i);
    pulse->angle = ct_p->getDoubleValue(temp, 0.);

    pulse->fs = sin(M_PI*pulse->angle/180.);
    pulse->fc = cos(M_PI*pulse->angle/180.);

    sprintf(temp,"plane_%d",i);
    pulse->plane = ct_p->getIntValue(temp);

    if (pulse->plane == -1)
    {
        if (g_p->gtype == THREED_e)
        {
            pulse->plane = 4; /* z_min by default */
        }
        else
        {
            pulse->plane = 0;
        }
    }

    if (g_p->gtype == THREED_e)
    {
        init_one_pulse_3d(ct_p, s_d_p, pulse, i, g_p);
    }
    else
    {
        init_one_pulse_2d(ct_p, s_d_p, pulse, i, g_p);
    }
}

static void init_pulses(configTable* ct_p, sources_rt_data* s_p, geometry_data* g_p)
{
    pulses_rt* pulses_p = &s_p->pulses;
    pulses_p->num_pulses = ct_p->getIntValue("num_sources", 0);
    if (0 == pulses_p->num_pulses)
    {
	pulses_p->num_pulses = ct_p->getIntValue("num_pulses", 0);
    }

    if (0 == pulses_p->num_pulses)
    {
        pulses_p->pulse_p = NULL;
        return;
    }

    pulses_p->pulse_p = (pulse_rt_data*)malloc(pulses_p->num_pulses*sizeof(pulse_rt_data));

    if (NULL == pulses_p->pulse_p)
    {
        return;
    }
    memset(pulses_p->pulse_p, sizeof(pulses_p->num_pulses*sizeof(pulse_rt_data)), 0);

    for (int i = 0; i < pulses_p->num_pulses; i++)
    {
        init_one_pulse(ct_p, s_p, &pulses_p->pulse_p[i], i, g_p);
    }
    return;
}


static void destroy_one_pulse(pulse_rt_data* pulse)
{
    if (pulse->threed == true)
    {
        if (pulse->space_shape_array_ey != NULL)
        {
            destroy_field(pulse->space_shape_array_ey);
            free(pulse->space_shape_array_ey);
            pulse->space_shape_array_ey = NULL;
        }

        if (pulse->space_shape_array_ex != NULL)
        {
            destroy_field(pulse->space_shape_array_ex);
            free(pulse->space_shape_array_ex);
            pulse->space_shape_array_ex = NULL;
        }
    }
    else
    {
        if (pulse->space_shape_array != NULL)
        {
            free(pulse->space_shape_array);
            pulse->space_shape_array = NULL;
        }

    }
    return;
}

static void destroy_pulses(pulses_rt* pulses_p)
{
    if (0 == pulses_p->num_pulses || NULL == pulses_p->pulse_p)
    {
        return;
    }

    for (int i = 0; i < pulses_p->num_pulses; i++)
    {
        destroy_one_pulse(&pulses_p->pulse_p[i]);
    }

    free(pulses_p->pulse_p);
    pulses_p->pulse_p = NULL;
    return;
}

void print_sources_rt(sources_rt_data* s_p,
                      media_data* m_p,
                      geometry_data* g_p,
                      time_scheme* t_p,
                      const char* pathPrefix)
{
    print_pulses(&s_p->pulses, m_p, g_p, t_p, pathPrefix);
    print_waves(&s_p->waves, t_p, pathPrefix);
}

static void print_pulses(pulses_rt* pulses_p,
			 media_data* m_p,
			 geometry_data* g_p,
			 time_scheme* t_p,
			 const char* pathPrefix)
{
    fprintf(stderr, "Number of pulses: %d\n", pulses_p->num_pulses);

    if (0 == pulses_p->num_pulses)
    {
        fprintf(stderr,"\n");
        return;
    }

    for (int i=0; i<pulses_p->num_pulses; i++)
    {
        print_one_pulse(pulses_p->pulse_p, m_p, i);
        store_one_pulse(pulses_p->pulse_p, i,  g_p, t_p, pathPrefix);
    }
    fprintf(stderr,"\n");
}

static void print_one_pulse(pulse_rt_data* pulse,
			    media_data* m_p,
			    int i)
{
    FP pcr_temp = m_p->pcr*pulse[i].lambda*pulse[i].lambda*1e6/
	(m_p->wl*m_p->wl);

    if (m_p->kerr == 1 && m_p->pcr > 0)
    {
        pulse[i].p_pcr = pulse[i].power/pcr_temp;

        if (pulse[i].p_pcr > 1)
        {
            pulse[i].l_sf = 0.367*pulse[i].zr/(sqrt(pulse[i].p_pcr)-0.852);
            pulse[i].a_opt = pulse[i].lambda/(2*M_PI)*
		sqrt(2./(m_p->n_2*m_p->n_0*I0*pulse[i].a0*pulse[i].a0));
        }
        else
        {
            pulse[i].l_sf = 0;
            pulse[i].a_opt = 0;
        }
    }
    else
    {
        pulse[i].l_sf = 0;
        pulse[i].a_opt = 0;
        pulse[i].p_pcr = 0;
    }

    fprintf(stderr, "pulse[%d]: w0 = %.2f [mkm] \n", i, pulse[i].w0);
    fprintf(stderr, "pulse[%d]: Duration(FWHM) = %.2f [fs]\n", i,
            pulse[i].env.duration*1e15);
    if (pulse[i].env.type == tanh_e)
    {
        fprintf(stderr, "pulse[%d]: envelope = tanh\n", i);
        fprintf(stderr, "pulse[%d]: t0 = %.2f \n", i, pulse[i].env.t0);
        fprintf(stderr, "pulse[%d]: tp = %.2f \n", i, pulse[i].env.tp);

    }
    else
    {
        fprintf(stderr, "pulse[%d]: envelope = gauss\n", i);
        fprintf(stderr, "pulse[%d]: tp = %.2f \n", i, pulse[i].env.tp);
    }
    fprintf(stderr, "pulse[%d]: tdelay = %.2f \n", i, pulse[i].tdelay);
    fprintf(stderr, "pulse[%d]: input amp = %.3f [Ea]\n", i, pulse[i].a0);
    fprintf(stderr, "pulse[%d]: intensity = %3.2e [TW/cm^2]\n", i,
            pulse[i].a0*pulse[i].a0*I0*1e-12);
    fprintf(stderr, "pulse[%d]: f = %.2f [mkm] \n", i, pulse[i].f);
    fprintf(stderr, "pulse[%d]: lambda = %.2f [mkm]\n", i, pulse[i].lambda);
    fprintf(stderr, "pulse[%d]: x0 = %.2f [mkm] \n", i, pulse[i].x0);
    fprintf(stderr, "pulse[%d]: dx = %.3f [mkm]\n", i, pulse[i].delta_x);
    if (pulse[i].threed == true)
    {
        fprintf(stderr, "pulse[%d]: y0 = %.2f [mkm] \n", i, pulse[i].y0);
        fprintf(stderr, "pulse[%d]: dy = %.3f [mkm]\n", i, pulse[i].delta_y);
    }
    fprintf(stderr, "pulse[%d]: angle = %f \n", i, pulse[i].angle);
    fprintf(stderr, "pulse[%d]: power = %f [GW]\n", i, pulse[i].power);
    fprintf(stderr, "pulse[%d]: Pcr   = %f [GW]\n", i, pcr_temp);
    fprintf(stderr, "pulse[%d]: energy = %e [J]\n", i, pulse[i].energy);
    fprintf(stderr, "pulse[%d]: plane = %d\n", i, pulse[i].plane);
    if (pulse[i].p_pcr > 0)
    {
        fprintf(stderr, "pulse[%d]: P/Pcr = %f\n", i, pulse[i].p_pcr);
        fprintf(stderr, "pulse[%d]: self-focusing length = %f [mkm]\n", i, pulse[i].l_sf);
        fprintf(stderr, "pulse[%d]: a optim = %f [mkm]\n", i, pulse[i].a_opt);
    }

    fprintf(stderr, "pulse[%d]: zr = %f [mum]\n", i, pulse[i].zr);
    fprintf(stderr, "pulse[%d]: real focus = %f [mum]\n", i, pulse[i].focus);
    fprintf(stderr, "pulse[%d]: focal gain = %f \n", i, pulse[i].fgain);
    fprintf(stderr, "pulse[%d]: focal spot = %f [mum]\n", i, pulse[i].fspot);
    fprintf(stderr, "pulse[%d]: focal amplitude = %f [Ea]\n", i, pulse[i].famp);
    fprintf(stderr, "pulse[%d]: focal intensity = %e [W/cm^2]\n", i, pulse[i].fint);
}

static void store_one_pulse(pulse_rt_data* pulse,
			    int i,
			    geometry_data* g_p,
			    time_scheme* t_p,
			    const char* pathPrefix)
{
    char tmp[1024];
    FILE* sfile;

    sprintf(tmp, "%s/pulse_%d.dat", pathPrefix, i);

    if (NULL == (sfile = fopen(tmp,"w")))
    {
        perror("Unable to create file for writing");
        exit(-1);
    }

    fprintf(sfile, "# %d %lf\n", t_p->t_max, t_p->delta_t*3.33);
    for (int t = 0; t < t_p->t_max+1; t++)
    {
        FP temp = time_shape3d(&pulse[i],
                                   (t+0.5)*t_p->delta_t,
                                   pulse[i].x0,
                                   pulse[i].y0)/(0.25/(M_PI*pulse[i].delta_s));

        FP temp1 = envelope3d(&pulse[i],
                                  (t+0.5)*t_p->delta_t,
                                  pulse[i].x0,
                                  pulse[i].y0)/(0.25/(M_PI*pulse[i].delta_s));

        fprintf(sfile, "%d %lf %lf %lf %lf\n",t, temp, temp1, temp*temp, temp1*temp1);
    }
    fclose(sfile);
}

static void store_one_wave(waves_rt* waves,
			   int i,
			   time_scheme* t_p,
			   const char* pathPrefix)
{
    char tmp[1024];
    FILE* sfile;

    sprintf(tmp, "%s/wave_%d.dat", pathPrefix, i);

    if (NULL == (sfile = fopen(tmp,"w")))
    {
        perror("Unable to create file for writing");
        exit(-1);
    }
    FP phase0 = waves->planewave_p[i].freq *
	t_p->delta_t *
	waves->planewave_p[i].vf;
    fprintf(sfile, "# %d %lf\n", t_p->t_max, t_p->delta_t);
    for (int t = 0; t < t_p->t_max+1; t++)
    {
        FP phase = phase0 * t + waves->planewave_p[i].phase;
        fprintf(sfile, "%d %lf\n",t,
                pw_time_shape(&waves->planewave_p[i], t*t_p->delta_t)*
                sin(phase));
    }
    fclose(sfile);
}

static void destroy_sources_rt_3d(sources_rt_data* s_p)
{
    if (s_p->pulse_enable == true)
    {
        if (s_p->pulse_zmin == true)
        {
            destroy_field(s_p->Jey_zmin_p);
            destroy_field(s_p->Jex_zmin_p);
            destroy_field(s_p->Jhy_zmin_p);
            destroy_field(s_p->Jhx_zmin_p);

            free(s_p->Jey_zmin_p);
            free(s_p->Jex_zmin_p);
            free(s_p->Jhx_zmin_p);
            free(s_p->Jhy_zmin_p);
	    free(s_p->jh_zmin_idx);
	    free(s_p->je_zmin_idx);
	    free(s_p->j_zmin);
        }

        if (s_p->pulse_zmax == true)
        {
            destroy_field(s_p->Jey_zmax_p);
            destroy_field(s_p->Jex_zmax_p);
            destroy_field(s_p->Jhy_zmax_p);
            destroy_field(s_p->Jhx_zmax_p);

            free(s_p->Jey_zmax_p);
            free(s_p->Jex_zmax_p);
            free(s_p->Jhx_zmax_p);
            free(s_p->Jhy_zmax_p);
	    free(s_p->jh_zmax_idx);
	    free(s_p->je_zmax_idx);
	    free(s_p->j_zmax);
        }

        if (s_p->pulse_xmin == true)
        {
            destroy_field(s_p->Jey_xmin_p);
            destroy_field(s_p->Jez_xmin_p);
            destroy_field(s_p->Jhy_xmin_p);
            destroy_field(s_p->Jhz_xmin_p);

            free(s_p->Jey_xmin_p);
            free(s_p->Jez_xmin_p);
            free(s_p->Jhz_xmin_p);
            free(s_p->Jhy_xmin_p);
        }
        if (s_p->pulse_xmax == true)
        {
            destroy_field(s_p->Jey_xmax_p);
            destroy_field(s_p->Jez_xmax_p);
            destroy_field(s_p->Jhy_xmax_p);
            destroy_field(s_p->Jhz_xmax_p);

            free(s_p->Jey_xmax_p);
            free(s_p->Jez_xmax_p);
            free(s_p->Jhz_xmax_p);
            free(s_p->Jhy_xmax_p);
        }

        if (s_p->pulse_ymin == true)
        {
            destroy_field(s_p->Jex_ymin_p);
            destroy_field(s_p->Jez_ymin_p);
            destroy_field(s_p->Jhx_ymin_p);
            destroy_field(s_p->Jhz_ymin_p);

            free(s_p->Jex_ymin_p);
            free(s_p->Jez_ymin_p);
            free(s_p->Jhz_ymin_p);
            free(s_p->Jhx_ymin_p);
        }

        if (s_p->pulse_ymax == true)
        {
            destroy_field(s_p->Jex_ymax_p);
            destroy_field(s_p->Jez_ymax_p);
            destroy_field(s_p->Jhx_ymax_p);
            destroy_field(s_p->Jhz_ymax_p);

            free(s_p->Jex_ymax_p);
            free(s_p->Jez_ymax_p);
            free(s_p->Jhz_ymax_p);
            free(s_p->Jhx_ymax_p);
        }
#ifdef CUDA
	destroy_psources_3d_cuda(s_p);
#endif
    }

    if (s_p->tfsf_enable == true)
    {
        if (s_p->tfsf_xmin == true)
        {
            destroy_field(s_p->tfsf_ey_xmin_p);
            destroy_field(s_p->tfsf_ez_xmin_p);
            destroy_field(s_p->tfsf_hy_xmin_p);
            destroy_field(s_p->tfsf_hz_xmin_p);

            free(s_p->tfsf_ey_xmin_p);
            free(s_p->tfsf_ez_xmin_p);
            free(s_p->tfsf_hy_xmin_p);
            free(s_p->tfsf_hz_xmin_p);

	    free(s_p->we_xmin_idx);
	    free(s_p->wh_xmin_idx);
	    free(s_p->w_xmin);
        }

        if (s_p->tfsf_xmax == true)
        {
            destroy_field(s_p->tfsf_ey_xmax_p);
            destroy_field(s_p->tfsf_ez_xmax_p);
            destroy_field(s_p->tfsf_hy_xmax_p);
            destroy_field(s_p->tfsf_hz_xmax_p);

            free(s_p->tfsf_ey_xmax_p);
            free(s_p->tfsf_ez_xmax_p);
            free(s_p->tfsf_hy_xmax_p);
            free(s_p->tfsf_hz_xmax_p);

	    free(s_p->we_xmax_idx);
	    free(s_p->wh_xmax_idx);
	    free(s_p->w_xmax);
        }
	
        if (s_p->tfsf_ymin == true)
        {
            destroy_field(s_p->tfsf_ex_ymin_p);
            destroy_field(s_p->tfsf_ez_ymin_p);
            destroy_field(s_p->tfsf_hx_ymin_p);
            destroy_field(s_p->tfsf_hz_ymin_p);

            free(s_p->tfsf_ex_ymin_p);
            free(s_p->tfsf_ez_ymin_p);
            free(s_p->tfsf_hx_ymin_p);
            free(s_p->tfsf_hz_ymin_p);

	    free(s_p->we_ymin_idx);
	    free(s_p->wh_ymin_idx);
	    free(s_p->w_ymin);
        }

        if (s_p->tfsf_ymax == true)
        {
            destroy_field(s_p->tfsf_ex_ymax_p);
            destroy_field(s_p->tfsf_ez_ymax_p);
            destroy_field(s_p->tfsf_hx_ymax_p);
            destroy_field(s_p->tfsf_hz_ymax_p);

            free(s_p->tfsf_ex_ymax_p);
            free(s_p->tfsf_ez_ymax_p);
            free(s_p->tfsf_hx_ymax_p);
            free(s_p->tfsf_hz_ymax_p);

	    free(s_p->we_ymax_idx);
	    free(s_p->wh_ymax_idx);
	    free(s_p->w_ymax);
        }

        if (s_p->tfsf_zmin == true)
        {
            destroy_field(s_p->tfsf_ex_zmin_p);
            destroy_field(s_p->tfsf_ey_zmin_p);
            destroy_field(s_p->tfsf_hx_zmin_p);
            destroy_field(s_p->tfsf_hy_zmin_p);

            free(s_p->tfsf_ex_zmin_p);
            free(s_p->tfsf_ey_zmin_p);
            free(s_p->tfsf_hx_zmin_p);
            free(s_p->tfsf_hy_zmin_p);

	    free(s_p->we_zmin_idx);
	    free(s_p->wh_zmin_idx);
	    free(s_p->w_zmin);
        }

        if (s_p->tfsf_zmax == true)
        {
            destroy_field(s_p->tfsf_ex_zmax_p);
            destroy_field(s_p->tfsf_ey_zmax_p);
            destroy_field(s_p->tfsf_hx_zmax_p);
            destroy_field(s_p->tfsf_hy_zmax_p);

            free(s_p->tfsf_ex_zmax_p);
            free(s_p->tfsf_ey_zmax_p);
            free(s_p->tfsf_hx_zmax_p);
            free(s_p->tfsf_hy_zmax_p);

	    free(s_p->we_zmax_idx);
	    free(s_p->wh_zmax_idx);
	    free(s_p->w_zmax);
        }
        free(s_p->vertices);
#ifdef CUDA
	destroy_tfsf_3d_cuda(s_p);
#endif
    }
}

static void destroy_sources_rt_2d(sources_rt_data* s_p)
{
    if (s_p->tfsf_enable == true)
    {
        if (s_p->tfsf_xmin == true)
        {
            free(s_p->tfsf_ey_xmin);
            free(s_p->tfsf_hz_xmin);
        }

        if (s_p->tfsf_xmax == true)
        {
            free(s_p->tfsf_ey_xmax);
            free(s_p->tfsf_hz_xmax);
        }

        if (s_p->tfsf_zmin == true)
        {
            free(s_p->tfsf_ey_zmin);
            free(s_p->tfsf_hx_zmin);
        }

        if (s_p->tfsf_zmax == true)
        {
            free(s_p->tfsf_ey_zmax);
            free(s_p->tfsf_hx_zmax);
        }
        free(s_p->vertices);
    }

    if (s_p->pulse_enable == true)
    {
        if (s_p->pulse_xmin == true)
        {
            free(s_p->pulse_ey_xmin);
            free(s_p->pulse_hz_xmin);
        }

        if (s_p->pulse_xmax == true)
        {
            free(s_p->pulse_ey_xmax);
            free(s_p->pulse_hz_xmax);
        }

        if (s_p->pulse_zmin == true)
        {
            free(s_p->pulse_ey_zmin);
            free(s_p->pulse_hx_zmin);
        }

        if (s_p->pulse_zmax == true)
        {
            free(s_p->pulse_ey_zmax);
            free(s_p->pulse_hx_zmax);
        }
    }
}

void destroy_sources_rt(sources_rt_data* s_p, geometry_data* g_p)
{
    destroy_pulses(&s_p->pulses);
    destroy_waves(&s_p->waves);

    if (g_p->gtype == THREED_e)
    {
        destroy_sources_rt_3d(s_p);
    }
    else
    {
        destroy_sources_rt_2d(s_p);
    }
}

static void print_waves(waves_rt* waves,
			time_scheme* t_p,
			const char* pathPrefix)
{
    if (waves->num_waves > 0)
    {
	fprintf(stderr, "Number of waves: %d\n", waves->num_waves);
	
	for (int i=0; i<waves->num_waves; i++ )
	{
	    print_one_wave(&waves->planewave_p[i], i);
	    store_one_wave(waves, i, t_p, pathPrefix);
	}
    }
    fprintf(stderr,"\n");
}

static void print_one_wave(planewave_rt_data* wave, int i)
{
    FP a0 = wave->amplitude*0.5*M_PI;

    fprintf(stderr, "wave[%d]: energy = %f J sm^-2 \n", i, wave->energy*0.25*M_PI*M_PI);
    fprintf(stderr, "wave[%d]: phi = %f \n", i, wave->phi);
    fprintf(stderr, "wave[%d]: psi = %f \n", i, wave->psi);
    fprintf(stderr, "wave[%d]: theta = %f \n", i, wave->theta);
    fprintf(stderr, "wave[%d]: amplitude = %f \n", i, a0);
    fprintf(stderr, "wave[%d]: intensity = %3.2e TW/cm^2\n", i, a0*a0*I0*1e-12);

    if (wave->type == plane_e)
    {
	fprintf(stderr, "wave[%d] type: plane wave\n", i);
    }
    else if(wave->type == dipole_e)
    {
	fprintf(stderr, "wave[%d] type: dipole wave\n", i);
    }
    else if(wave->type == simple_e)
    {
	fprintf(stderr, "wave[%d] type: simple wave\n", i);
    }
    else
    {
	fprintf(stderr, "wave[%d] type: unknown\n", i);
	exit(-1);
    }
    
    if (wave->env.type == tanh_e)
    {
        fprintf(stderr, "wave[%d] envelope: tanh\n", i);
        fprintf(stderr, "wave[%d]: t0 = %f \n", i, wave->t0);
        fprintf(stderr, "wave[%d]: tp = %f \n", i, wave->tp);
        fprintf(stderr, "wave[%d]: Duration(FWHM) = %.2f fs\n ", i,
                (wave->tp + 2*wave->t0)*3.333);
    }
    else
    {
        fprintf(stderr, "wave[%d] envelope: gauss\n", i);
        fprintf(stderr, "wave[%d]: Duration(FWHM) = %f fs\n", i, wave->tp);
    }

    fprintf(stderr, "wave[%d]: tdelay = %f \n", i, wave->tdelay);
    fprintf(stderr, "wave[%d]: lambda = %f \n", i, wave->lambda);
    fprintf(stderr, "wave[%d]: phase = %f \n", i, wave->phase);
    fprintf(stderr, "wave[%d]: freq = %f \n", i, wave->freq);
    fprintf(stderr, "wave[%d]: delta_temp = %f \n", i, wave->delta_temp);
    fprintf(stderr, "wave[%d]: chirp = %f \n", i, wave->chirp);
}

static void destroy_one_wave(planewave_rt_data* w_p)
{
    free(w_p->f_pos);
}

static void destroy_waves(waves_rt* waves)
{
    if (waves->num_waves == 0)
    {
        return;
    }

    for (int i=0; i<waves->num_waves; i++ )
    {
        destroy_one_wave(&waves->planewave_p[i]);
    }

    if (NULL != waves->Etemp)
    {
	free(waves->Etemp);
    }
    if (NULL != waves->Htemp)
    {
	free(waves->Htemp);
    }
    if (NULL != waves->ephase)
    {
	free(waves->ephase);
    }
    if (NULL != waves->hphase)
    {
	free(waves->hphase);
    }
    free(waves->planewave_p);

    return;
}

static void init_one_wave(planewave_rt_data* planewave,
			  sources_rt_data* s_p,
			  geometry_data* g_p,
			  FP amplitude,
			  FP lambda,
			  FP phase,
			  FP phi,
			  FP theta,
			  FP psi,
			  FP t0,
			  FP tp,
			  FP tdelay,
			  FP vf,
			  FP chirp,
			  bool   linear,
			  wave_type type,
			  int ntemp)
{
    planewave->amplitude = amplitude/(0.5*M_PI*1.051)*sqrt(0.1/g_p->delta_z); //dirty workaround, does not work
    planewave->lambda = lambda;
    planewave->freq = 2*M_PI/lambda;
    planewave->t0 = t0;
    planewave->alpha = 2.276/t0;
    planewave->tp = tp;
    planewave->tdelay = tdelay;
    planewave->vf = vf;
    planewave->chirp = chirp;
    planewave->linear = linear;
    planewave->env.t0 = t0;
    planewave->env.alpha = planewave->alpha;
    planewave->env.tp = tp;
    planewave->type = type;
    
    if (phase < 0.) /* Random phase */
    {
        FP r = (FP)rand()/((FP)RAND_MAX + 1.);
        r *= 2*M_PI;
        planewave->phase = r;
    }
    else
    {
        planewave->phase = phase;
    }

    planewave->phi = phi;
    planewave->theta = theta;
    planewave->psi = psi;

    planewave->sin_phi = sin(phi*M_PI/180.);
    planewave->cos_phi = sin((90.-phi)*M_PI/180.);

    if (g_p->gtype == THREED_e)
    {
        init_one_wave_3d(planewave, s_p, g_p, ntemp);
    }
    else
    {
        init_one_wave_2d(planewave, g_p, ntemp);
    }

    planewave->f_pos = FP_MALLOC(ntemp);

    for (int i=0; i < ntemp; i++)
    {
        planewave->f_pos[i] = planewave->delta_temp * (i-2);
    }
}

static void init_one_wave_2d(planewave_rt_data* planewave,
			     geometry_data* g_p,
			     int ntemp)
{
    planewave->kx = planewave->cos_phi;
    planewave->ky = 0;
    planewave->kz = planewave->sin_phi;
    planewave->linear = true;

    if (planewave->phi >=0 && planewave->phi <= 90)
    {
        planewave->point = 0;
    }
    else if (planewave->phi > 90 && planewave->phi <= 180)
    {
        planewave->point = 1;
    }
    else if (planewave->phi > 180 && planewave->phi <= 270)
    {
        planewave->point = 2;
    }
    else
    {
        planewave->point = 3;
    }
    FP dx = g_p->x_full*g_p->delta_x;
    FP dz = g_p->z_full*g_p->delta_z;
    FP diag = sqrt(dx*dx + dz*dz);

    FP cos_gamma = dx/diag*fabs(planewave->kx) +
	dz/diag*fabs(planewave->kz);
    planewave->delta_temp = 1.1*diag*cos_gamma/ntemp;
    planewave->ey = 1;

    planewave->hx = planewave->sin_phi;
    planewave->hz = -planewave->cos_phi;
    planewave->energy = 0;
}

static void init_one_wave_3d(planewave_rt_data* planewave,
			     sources_rt_data* s_p,
			     geometry_data* g_p,
			     int ntemp)
{
    if (planewave->theta < 0 || planewave->theta > 180)
    {
        fprintf(stderr, "Wrong angle theta range: %lf\n", planewave->theta);
        exit(-1);
    }
    planewave->sin_theta = sin(planewave->theta*M_PI/180.);
    planewave->cos_theta = sin((90.-planewave->theta)*M_PI/180.);


    if (planewave->psi < 0 || planewave->psi > 360)
    {
        fprintf(stderr, "Wrong angle psi range: %lf\n", planewave->psi);
        exit(-1);
    }
    planewave->sin_psi = sin(planewave->psi*M_PI/180.);
    planewave->cos_psi = sin((90.-planewave->psi)*M_PI/180.);

    planewave->kx = planewave->sin_theta*planewave->cos_phi;
    planewave->ky = planewave->sin_theta*planewave->sin_phi;
    planewave->kz = planewave->cos_theta;

    if (planewave->theta >= 0 && planewave->theta <= 90)
    {
        if (planewave->phi >=0 && planewave->phi <= 90)
        {
            planewave->point = 0;
        }
        else if (planewave->phi > 90 && planewave->phi <= 180)
        {
            planewave->point = 1;
        }
        else if (planewave->phi > 180 && planewave->phi <= 270)
        {
            planewave->point = 2;
        }
        else
        {
            planewave->point = 3;
        }
    }
    else
    {
        if (planewave->phi >=0 && planewave->phi <= 90)
        {
            planewave->point = 4;
        }
        else if (planewave->phi > 90 && planewave->phi <= 180)
        {
            planewave->point = 5;
        }
        else if (planewave->phi > 180 && planewave->phi <= 270)
        {
            planewave->point = 6;
        }
        else
        {
            planewave->point = 7;
        }
    }
    
    if (planewave->type == simple_e)
    {
	planewave->num = ntemp+1;
	if (NULL == (planewave->E_a = (tfsf_point_data*)malloc(sizeof(tfsf_point_data)*planewave->num)))
	{
	    fprintf(stderr, "Unable to allocate memory\n");
	    exit(-1);
	}
	
	if (NULL == (planewave->H_a = (tfsf_point_data*)malloc(sizeof(tfsf_point_data)*planewave->num)))
	{
	    fprintf(stderr, "Unable to allocate memory\n");
	    exit(-1);
	}
	
	int k_off = s_p->z_off;
	
	for (int k=0; k<planewave->num; k++)
	{
	    planewave->E_a[k].d = (k + 1. + k_off) * g_p->delta_z;
	    planewave->E_a[k].field = 0;
	    planewave->E_a[k].phase = 0;
	    planewave->H_a[k].d = (k + 0.5 + k_off) * g_p->delta_z;
	    planewave->H_a[k].field = 0;
	    planewave->H_a[k].phase = 0;
	}
    }
    else
    {
	FP dx = g_p->x_full*g_p->delta_x;
	FP dy = g_p->y_full*g_p->delta_y;
	FP dz = g_p->z_full*g_p->delta_z;
	FP diag = sqrt(dx*dx + dy*dy + dz*dz);
	
	FP cos_gamma = dx/diag*fabs(planewave->kx) +
	    dy/diag*fabs(planewave->ky) +
	    dz/diag*fabs(planewave->kz);
	
	planewave->delta_temp = 1.1*diag*cos_gamma/ntemp;
    }
    if (planewave->linear == true)
    {
	planewave->ex = planewave->cos_psi*planewave->sin_phi -
	    planewave->sin_psi*planewave->cos_theta*planewave->cos_phi;
	planewave->ey = -planewave->cos_psi*planewave->cos_phi -
	    planewave->sin_psi*planewave->cos_theta*planewave->sin_phi;
	planewave->ez =  planewave->sin_psi*planewave->sin_theta;

	planewave->hx = planewave->sin_psi*planewave->sin_phi +
	    planewave->cos_psi*planewave->cos_theta*planewave->cos_phi;
	planewave->hy = -planewave->sin_psi*planewave->cos_phi +
	    planewave->cos_psi*planewave->cos_theta*planewave->sin_phi;
	planewave->hz = -planewave->cos_psi*planewave->sin_theta;
    }
    else
    {
	planewave->exc = planewave->sin_phi;
	planewave->exs = - planewave->cos_theta*planewave->cos_phi;
	planewave->eyc = - planewave->cos_phi;
	planewave->eys = - planewave->cos_theta*planewave->sin_phi;
	planewave->ezs = planewave->sin_theta;

	planewave->hxc = planewave->cos_theta*planewave->cos_phi;
	planewave->hxs = planewave->sin_phi;
	planewave->hyc = planewave->cos_theta*planewave->sin_phi;
	planewave->hys = - planewave->cos_phi;
	planewave->hzc = - planewave->sin_theta;
    }
    
    planewave->energy = I0*planewave->amplitude*planewave->amplitude*
	planewave->env.tp*1e-15*0.5*sqrt(M_PI/log(2.)); //J sm^-2
}

FP time_envelope3d(pulse_rt_data* pu_p,
			 FP t,
			 FP x,
			 FP y)
{
    return envelope3d(pu_p, t, x, y);
}

static FP envelope3d(pulse_rt_data* pu_p,
			 FP t,
			 FP x,
			 FP y)
{
    FP xc = x - pu_p->x0;
    FP yc = y - pu_p->y0;
    FP r = sqrt(xc * xc + yc * yc)/pu_p->f;
    FP td = -pu_p->f * (sqrt(pu_p->fc*pu_p->fc + (r - pu_p->fs)*(r - pu_p->fs)) - 1.);
    return pu_p->amp * calculate_envelope(&pu_p->env, t- td - pu_p->tdelay);
}



static FP time_shape_dipole_wave(planewave_rt_data* w_p,
				     FP t,
				     FP d,
				     FP R)
{
    FP t1 = t - R + d - w_p->tdelay;
    return w_p->amplitude * calculate_envelope(&w_p->env, t1) * sin(w_p->freq*t1);
}

FP phase3d(pulse_rt_data* pu_p,
               FP t,
               FP x,
               FP y)
{
    FP xc = x - pu_p->x0;
    FP yc = y - pu_p->y0;
    FP r = sqrt(xc * xc + yc * yc)/pu_p->f;
    FP td = -pu_p->f * (sqrt(pu_p->fc*pu_p->fc + (r - pu_p->fs)*(r - pu_p->fs)) - 1.);
    return pu_p->freq*(t - td - pu_p->tdelay);
}

FP space_shape(FP z,
                   FP z0,
                   FP waist)
{
    return exp(-(z-z0)*(z-z0)/(waist*waist));
}

FP space_shape3d(FP y,
                     FP y0,
                     FP x,
                     FP x0,
                     FP waist)
{
    return exp(-((x-x0)*(x-x0) + (y-y0)*(y-y0))/(2*waist*waist));
}

static void init_waves(configTable* ct_p,
		       geometry_data* g_p,
		       waves_rt* waves_p,
		       sources_rt_data* s_p)
{

    int num_waves = ct_p->getIntValue("num_waves", 0);
    int x_max = g_p->x_full;
    int z_max = g_p->z_full;
    bool simple = false;
    
    waves_p->num_waves = num_waves;

    if (0 == num_waves)
    {
        waves_p->planewave_p = NULL;
        return;
    }

    srand(time(NULL));
 
    waves_p->ntemp = x_max > z_max ? 10000*x_max : 10000*z_max;

    waves_p->planewave_p =
        (planewave_rt_data*)malloc(num_waves*sizeof(planewave_rt_data));

    if (NULL == waves_p->planewave_p)
    {
        exit(-1);
    }

    num_waves = ct_p->getIntValue("num_waves", 0);

    for (int i=0; i<num_waves && i<waves_p->num_waves; i++)
    {
        char temp[100];

        sprintf(temp,"pw_amplitude_%d",i);
        FP amplitude = ct_p->getDoubleValue(temp);

	if (amplitude == -1)
	{
	    sprintf(temp,"pw_intensity_%d",i);
	    FP intensity = ct_p->getDoubleValue(temp, 1e14);
	    if (intensity < 0)
	    {
		fprintf(stderr, "Wrong parameters, should be set either amplitude, or intensity\n");
		amplitude = 0;
	    }
	    else
	    {
		amplitude = sqrt(intensity/3.61e16);
	    }
	}
	
        sprintf(temp,"pw_wavelength_%d",i);
        FP lambda = ct_p->getDoubleValue(temp, 0.8);

        sprintf(temp,"pw_phase_%d",i);
        FP phase = ct_p->getDoubleValue(temp, 0.);

        sprintf(temp,"pw_t0_%d",i);
        FP t0 = ct_p->getDoubleValue(temp, 10.);

        sprintf(temp,"pw_tp_%d",i);
        FP tp = ct_p->getDoubleValue(temp, 10.);

        sprintf(temp,"pw_tdelay_%d",i);
        FP tdelay = ct_p->getDoubleValue(temp, 10.);

        sprintf(temp,"pw_vf_%d",i);
        FP vf = ct_p->getDoubleValue(temp, 1.);

	if (vf == 0.)
	{
	    vf = g_p->vf;
	}

        sprintf(temp,"pw_phi_%d",i);
        FP phi = ct_p->getDoubleValue(temp, 0.);

        sprintf(temp,"pw_polarization_%d",i);
        string linear_str = ct_p->getStringValue(temp, "linear");
        bool linear;

        if (linear_str.compare("linear") == 0)
        {
            linear = true;
        }
        else
        {
            linear = false;
        }

        FP theta = 0., psi=0.;
        if (g_p->gtype == THREED_e)
        {
            sprintf(temp,"pw_theta_%d",i);
            theta = ct_p->getDoubleValue(temp, 0.);

            sprintf(temp,"pw_psi_%d",i);
            psi = ct_p->getDoubleValue(temp, 0.);
        }

        sprintf(temp, "pw_envelope_%d", i);
        if (ct_p->getStringValue(temp, "gauss").compare("tanh") == 0)
        {
            waves_p->planewave_p[i].env.f_p = &tanh_env;
            waves_p->planewave_p[i].env.type = tanh_e;
        }
        else
        {
            waves_p->planewave_p[i].env.f_p = &gauss_env;
            waves_p->planewave_p[i].env.type = gauss_e;
        }

        sprintf(temp,"pw_chirp_%d",i);
        FP chirp = ct_p->getDoubleValue(temp, 0);

	sprintf(temp,"pw_type_%d",i);
        string type = ct_p->getStringValue(temp, "plane");
	
	wave_type itype = plane_e;
	
	if (type.compare("dipole") == 0)
	{
	    itype = dipole_e;
	}

	if (type.compare("simple") == 0)
	{
	    if (1 < num_waves)
	    {
		fprintf(stderr, "Error configuration, simple wave can be only one\n");
		exit(-1);
	    }
	    itype = simple_e;
	    phi = 0.;
	    theta = 0.;
	    simple = true;
	    waves_p->ntemp = s_p->z_w;
	}

        init_one_wave(&waves_p->planewave_p[i],
		      s_p,
                      g_p,
                      amplitude,
                      lambda,
                      phase,
                      phi,
                      theta,
                      psi,
                      t0,
                      tp,
                      tdelay,
                      vf,
                      chirp,
                      linear,
		      itype,
                      waves_p->ntemp);
    }

    if (simple == false)
    {
	waves_p->Etemp = FP_MALLOC(waves_p->ntemp);
	waves_p->Htemp = FP_MALLOC(waves_p->ntemp);
	waves_p->ephase = FP_MALLOC(waves_p->ntemp);
	waves_p->hphase = FP_MALLOC(waves_p->ntemp);
    }
    else
    {
	waves_p->Etemp = NULL;
	waves_p->Htemp = NULL;
	waves_p->ephase = NULL;
	waves_p->hphase = NULL;
    }
}

static void update_pw_field(sources_rt_data* s_p, int n)
{
    planewave_rt_data* pw_p = &s_p->waves.planewave_p[n];
    memset((void*)pw_p->f_pos, 0, sizeof(FP)*s_p->waves.ntemp);
}

static void update_pw_field_simple_wave(sources_rt_data* s_p, int t, int n)
{
    planewave_rt_data* pw_p = &s_p->waves.planewave_p[n];
    FP omega0 = pw_p->freq;
    FP beta = pw_p->chirp;
    FP tdt = t * s_p->delta_t - pw_p->tdelay;
    
    for (int i=0; i<pw_p->num; i++)
    {
	FP t0 = tdt - pw_p->E_a[i].d/pw_p->vf;
	FP t1 = tdt - 0.5 * s_p->delta_t - pw_p->H_a[i].d/pw_p->vf;
	FP ephase = (omega0 - beta * t0) * t0;
	FP hphase = (omega0 - beta * t1) * t1;

	pw_p->E_a[i].field = -sin(ephase) * pw_time_shape(pw_p, ephase/omega0);
	pw_p->H_a[i].field = -sin(hphase) * pw_time_shape(pw_p, hphase/omega0);
    }
}

static void update_pw_field_point(sources_rt_data* s_p,
				  int n,
				  int t,
				  int idx)
{
    planewave_rt_data* pw_p = &s_p->waves.planewave_p[n];
    FP omega0 = pw_p->freq;
    FP beta = pw_p->chirp;
    FP kdx = pw_p->delta_temp/pw_p->vf;
    FP t0 = t * s_p->delta_t - pw_p->tdelay - idx*kdx;
    FP t1 = t0 - 0.5 * s_p->delta_t;
    FP ephase = (omega0 - beta * t0) * t0;
    FP hphase = (omega0 - beta * t1) * t1;
    
    if (pw_p->linear == true)
    {
	s_p->waves.Etemp[idx] = -sin(ephase) * pw_time_shape(pw_p, ephase/omega0);
	s_p->waves.Htemp[idx] = -sin(hphase) * pw_time_shape(pw_p, hphase/omega0);
    }
    else
    {
	s_p->waves.Etemp[idx] = - pw_time_shape(pw_p, ephase/omega0);
	s_p->waves.Htemp[idx] = - pw_time_shape(pw_p, hphase/omega0);
	s_p->waves.ephase[idx] = ephase;
	s_p->waves.hphase[idx] = hphase;
    }
}

static FP interpolate_pw_field_e(sources_rt_data* s_p,
				     int n,
				     int t,
				     FP d)
{
    FP dx = d/s_p->waves.planewave_p[n].delta_temp;
    
    int res = (int)(dx)+2;
    
    if (s_p->waves.planewave_p[n].f_pos[res] == NOT_UPDATED)
    {
	update_pw_field_point(s_p, n, t, res);
	s_p->waves.planewave_p[n].f_pos[res] = UPDATED;
    }

    if (s_p->waves.planewave_p[n].f_pos[res+1] == NOT_UPDATED)
    {
	update_pw_field_point(s_p, n, t, res+1);
	s_p->waves.planewave_p[n].f_pos[res+1] = UPDATED;
    }
	
    FP ratio = dx - res + 2;

    return s_p->waves.Etemp[res] * ratio + s_p->waves.Etemp[res+1] * (1. - ratio);
}

static FP interpolate_pw_field_h(sources_rt_data* s_p,
				     int n,
				     int t,
				     FP d)
{
    FP dx = d/s_p->waves.planewave_p[n].delta_temp;
    int res = (int)(dx)+2;

    if (s_p->waves.planewave_p[n].f_pos[res] == 0.)
    {
	update_pw_field_point(s_p, n, t, res);
	s_p->waves.planewave_p[n].f_pos[res] = 1.;
    }

    if (s_p->waves.planewave_p[n].f_pos[res+1] == 0.)
    {
	update_pw_field_point(s_p, n, t, res+1);
	s_p->waves.planewave_p[n].f_pos[res+1] = 1.;
    }
    
    FP ratio = dx - res + 2;

    return s_p->waves.Htemp[res] * ratio + s_p->waves.Htemp[res+1] * (1. - ratio);
}


/* must always be called after interpolate_pw_field_e */
static FP interpolate_pw_phase_e(sources_rt_data* s_p,
				     int n,
				     FP d)
{
    FP dx = d/s_p->waves.planewave_p[n].delta_temp;
    int res = (int)(dx)+2;
    FP ratio = dx - res + 2;

    return s_p->waves.ephase[res] * ratio + s_p->waves.ephase[res+1] * (1. - ratio);
}

/* must always be called after interpolate_pw_field_h */
static FP interpolate_pw_phase_h(sources_rt_data* s_p,
				     int n,
				     FP d)
{
    FP dx = d/s_p->waves.planewave_p[n].delta_temp;
    int res = (int)(dx)+2;
    FP ratio = dx - res + 2;

    return s_p->waves.hphase[res] * ratio + s_p->waves.hphase[res+1] * (1. - ratio);
}

static FP calc_dist_3d(sources_rt_data* s_p,
			   int k,
			   FP x,
			   FP y,
			   FP z)
{
    int num = s_p->waves.planewave_p[k].point;

    return s_p->waves.planewave_p[k].kx*(x-s_p->vertices[num].x) +
	s_p->waves.planewave_p[k].ky*(y-s_p->vertices[num].y) +
	s_p->waves.planewave_p[k].kz*(z-s_p->vertices[num].z);
}

static void zero_tfsf_fields_3d(sources_rt_data* s_p)
{
    if (s_p->tfsf_xmin == true)
    {
        zero_field3d(s_p->tfsf_ey_xmin_p);
        zero_field3d(s_p->tfsf_ez_xmin_p);
        zero_field3d(s_p->tfsf_hy_xmin_p);
        zero_field3d(s_p->tfsf_hz_xmin_p);
    }

    if (s_p->tfsf_xmax == true)
    {
        zero_field3d(s_p->tfsf_ey_xmax_p);
        zero_field3d(s_p->tfsf_ez_xmax_p);
        zero_field3d(s_p->tfsf_hy_xmax_p);
        zero_field3d(s_p->tfsf_hz_xmax_p);
    }

    if (s_p->tfsf_ymin == true)
    {
        zero_field3d(s_p->tfsf_ex_ymin_p);
        zero_field3d(s_p->tfsf_ez_ymin_p);
        zero_field3d(s_p->tfsf_hx_ymin_p);
        zero_field3d(s_p->tfsf_hz_ymin_p);
    }

    if (s_p->tfsf_ymax == true)
    {
        zero_field3d(s_p->tfsf_ex_ymax_p);
        zero_field3d(s_p->tfsf_ez_ymax_p);
        zero_field3d(s_p->tfsf_hx_ymax_p);
        zero_field3d(s_p->tfsf_hz_ymax_p);
    }

    if (s_p->tfsf_zmin == true)
    {
        zero_field3d(s_p->tfsf_ey_zmin_p);
        zero_field3d(s_p->tfsf_ex_zmin_p);
        zero_field3d(s_p->tfsf_hy_zmin_p);
        zero_field3d(s_p->tfsf_hx_zmin_p);
    }

    if (s_p->tfsf_zmax == true)
    {
        zero_field3d(s_p->tfsf_ey_zmax_p);
        zero_field3d(s_p->tfsf_ex_zmax_p);
        zero_field3d(s_p->tfsf_hy_zmax_p);
        zero_field3d(s_p->tfsf_hx_zmax_p);
    }
    return;
}

static void zero_pulse_currents_3d(sources_rt_data* s_p)
{
    if (s_p->pulse_zmin == true)
    {
        zero_field3d(s_p->Jey_zmin_p);
        zero_field3d(s_p->Jex_zmin_p);
        zero_field3d(s_p->Jhx_zmin_p);
        zero_field3d(s_p->Jhy_zmin_p);
    }

    if (s_p->pulse_zmax == true)
    {
        zero_field3d(s_p->Jey_zmax_p);
        zero_field3d(s_p->Jex_zmax_p);
        zero_field3d(s_p->Jhx_zmax_p);
        zero_field3d(s_p->Jhy_zmax_p);
    }
}

void update_sources_rt3d(sources_rt_data* s_p,
                         geometry_data* g_p,
                         int t)
{
    if (s_p->pulse_enable == true)
    {
        int i_off = g_p->i_offset;
        int j_off = g_p->j_offset;

        zero_pulse_currents_3d(s_p);

        FP field, phase;
        FP te = (t+0.5)*s_p->delta_t;
        FP th = t*s_p->delta_t;

        for (int k=0; k < s_p->pulses.num_pulses; k++)
        {
            FP sin_psi = s_p->pulses.pulse_p[k].sin_psi;
            FP cos_psi = s_p->pulses.pulse_p[k].cos_psi;

            if (s_p->pulse_zmin == true && s_p->pulses.pulse_p[k].plane == zmin_e)
            {
                if (s_p->pulses.pulse_p[k].pol == circular_e)
                {
                    for (int i=0; i<s_p->Jey_zmin_p->nx; i++)
                    {
                        for (int j=0; j<s_p->Jey_zmin_p->ny; j++)
                        {
                            int index = INDEX_XY_P(s_p->Jey_zmin_p,i,j);
                            FP x = (i+i_off)*g_p->delta_x;
                            FP y = (j+0.5+j_off)*g_p->delta_y;
                            field = s_p->pulses.pulse_p[k].space_shape_array_ey->a[index]*
				envelope3d(&s_p->pulses.pulse_p[k], te, x, y);
                            phase = phase3d(&s_p->pulses.pulse_p[k],
                                            te,
                                            x,
                                            y);

                            cos_psi = cos(phase);
                            s_p->Jey_zmin_p->a[index] = field * cos_psi;

                            field = s_p->pulses.pulse_p[k].space_shape_array_ey->a[index]*
				envelope3d(&s_p->pulses.pulse_p[k],
					   th,
					   x,
					   y);
                            phase = phase3d(&s_p->pulses.pulse_p[k],
                                            th,
                                            x,
                                            y);
                            cos_psi = cos(phase);
                            s_p->Jhx_zmin_p->a[index] = -field * cos_psi;
                        }
                    }

                    for (int i=0; i<s_p->Jex_zmin_p->nx; i++)
                    {
                        for (int j=0; j<s_p->Jex_zmin_p->ny; j++)
                        {
                            int index = INDEX_XY_P(s_p->Jex_zmin_p,i,j);
                            FP x = (i+0.5+i_off)*g_p->delta_x;
                            FP y = (j+j_off)*g_p->delta_y;
                            field = s_p->pulses.pulse_p[k].space_shape_array_ex->a[index]*
				envelope3d(&s_p->pulses.pulse_p[k],
					   te,
					   x,
					   y);
                            phase = phase3d(&s_p->pulses.pulse_p[k],
                                            te,
                                            x,
                                            y);
                            sin_psi = sin(phase);

                            s_p->Jex_zmin_p->a[index] = field * sin_psi;
                            field = s_p->pulses.pulse_p[k].space_shape_array_ex->a[index]*
				envelope3d(&s_p->pulses.pulse_p[k],
					   th,
					   x,
					   y);
                            phase = phase3d(&s_p->pulses.pulse_p[k],
                                            th,
                                            x,
                                            y);
                            sin_psi = sin(phase);
                            s_p->Jhy_zmin_p->a[index] = field * sin_psi;
                        }
                    }
                }
                else /* linear polarization */
                {
                    for (int i=0; i<s_p->Jey_zmin_p->nx; i++)
                    {
                        for (int j=0; j<s_p->Jey_zmin_p->ny; j++)
                        {
                            int index = INDEX_XY_P(s_p->Jey_zmin_p,i,j);
                            FP x = (i+i_off)*g_p->delta_x;
                            FP y = (j+0.5+j_off)*g_p->delta_y;
                            field = s_p->pulses.pulse_p[k].space_shape_array_ey->a[index]*
				time_shape3d(&s_p->pulses.pulse_p[k],
					     te,
					     x,
					     y);
                            s_p->Jey_zmin_p->a[index] = field * cos_psi;
                            field = s_p->pulses.pulse_p[k].space_shape_array_ey->a[index]*
				time_shape3d(&s_p->pulses.pulse_p[k],
					     th,
					     x,
					     y);
                            s_p->Jhx_zmin_p->a[index] = -field * cos_psi;
                        }
                    }

                    for (int i=0; i<s_p->Jex_zmin_p->nx; i++)
                    {
                        for (int j=0; j<s_p->Jex_zmin_p->ny; j++)
                        {
                            int index = INDEX_XY_P(s_p->Jex_zmin_p,i,j);
                            FP x = (i+0.5+i_off)*g_p->delta_x;
                            FP y = (j+j_off)*g_p->delta_y;
                            field = s_p->pulses.pulse_p[k].space_shape_array_ex->a[index]*
				time_shape3d(&s_p->pulses.pulse_p[k],
					     te,
					     x,
					     y);
                            s_p->Jex_zmin_p->a[index] = field * sin_psi;
                            field = s_p->pulses.pulse_p[k].space_shape_array_ex->a[index]*
				time_shape3d(&s_p->pulses.pulse_p[k],
					     th,
					     x,
					     y);
                            s_p->Jhy_zmin_p->a[index] = field * sin_psi;
                        }
                    }
                }
            }
            if (s_p->pulse_zmax == true && s_p->pulses.pulse_p[k].plane == zmax_e)
            {
                if (s_p->pulses.pulse_p[k].pol == circular_e)
                {
                    for (int i=0; i<s_p->Jey_zmax_p->nx; i++)
                    {
                        for (int j=0; j<s_p->Jey_zmax_p->ny; j++)
                        {
                            int index = INDEX_XY_P(s_p->Jey_zmax_p,i,j);
                            FP x = (i+i_off)*g_p->delta_x;
                            FP y = (j+0.5+j_off)*g_p->delta_y;
                            field = -s_p->pulses.pulse_p[k].space_shape_array_ey->a[index]*
				envelope3d(&s_p->pulses.pulse_p[k],
					   te,
					   x,
					   y);
                            phase = phase3d(&s_p->pulses.pulse_p[k],
                                            te,
                                            x,
                                            y);
                            cos_psi = cos(phase);
                            s_p->Jey_zmax_p->a[index] = field * cos_psi;

                            field = s_p->pulses.pulse_p[k].space_shape_array_ey->a[index]*
				envelope3d(&s_p->pulses.pulse_p[k],
					   th,
					   x,
					   y);
                            phase = phase3d(&s_p->pulses.pulse_p[k],
                                            th,
                                            x,
                                            y);
                            cos_psi = cos(phase);
                            s_p->Jhx_zmax_p->a[index] = -field * cos_psi;
                        }
                    }
                    for (int i=0; i<s_p->Jex_zmax_p->nx; i++)
                    {
                        for (int j=0; j<s_p->Jex_zmax_p->ny; j++)
                        {
                            int index = INDEX_XY_P(s_p->Jex_zmax_p,i,j);
                            FP x = (i+0.5+i_off)*g_p->delta_x;
                            FP y = (j+j_off)*g_p->delta_y;
                            field = -s_p->pulses.pulse_p[k].space_shape_array_ex->a[index]*
				envelope3d(&s_p->pulses.pulse_p[k],
					   te,
					   x,
					   y);
                            phase = phase3d(&s_p->pulses.pulse_p[k],
                                            te,
                                            x,
                                            y);
                            sin_psi = sin(phase);

                            s_p->Jex_zmax_p->a[index] = field * sin_psi;
                            field = s_p->pulses.pulse_p[k].space_shape_array_ex->a[index]*
				envelope3d(&s_p->pulses.pulse_p[k],
					   th,
					   x,
					   y);
                            phase = phase3d(&s_p->pulses.pulse_p[k],
                                            th,
                                            x,
                                            y);
                            sin_psi = sin(phase);
                            s_p->Jhy_zmax_p->a[index] = field * sin_psi;
                        }
                    }
                }
                else /* linear polarization */
                {
                    for (int i=0; i<s_p->Jey_zmax_p->nx; i++)
                    {
                        for (int j=0; j<s_p->Jey_zmax_p->ny; j++)
                        {
                            int index = INDEX_XY_P(s_p->Jey_zmax_p,i,j);
                            FP x = (i+i_off)*g_p->delta_x;
                            FP y = (j+0.5+j_off)*g_p->delta_y;
                            field = -s_p->pulses.pulse_p[k].space_shape_array_ey->a[index]*
				time_shape3d(&s_p->pulses.pulse_p[k],
					     te,
					     x,
					     y);
                            s_p->Jey_zmax_p->a[index] = field * cos_psi;
                            field = s_p->pulses.pulse_p[k].space_shape_array_ey->a[index]*
				time_shape3d(&s_p->pulses.pulse_p[k],
					     th,
					     x,
					     y);
                            s_p->Jhx_zmax_p->a[index] = -field * cos_psi;
                        }
                    }

                    for (int i=0; i<s_p->Jex_zmax_p->nx; i++)
                    {
                        for (int j=0; j<s_p->Jex_zmax_p->ny; j++)
                        {
                            int index = INDEX_XY_P(s_p->Jex_zmax_p,i,j);
                            FP x = (i+0.5+i_off)*g_p->delta_x;
                            FP y = (j+j_off)*g_p->delta_y;
                            field = -s_p->pulses.pulse_p[k].space_shape_array_ex->a[index]*
				time_shape3d(&s_p->pulses.pulse_p[k],
					     te,
					     x,
					     y);
                            s_p->Jex_zmax_p->a[index] = field * sin_psi;
                            field = s_p->pulses.pulse_p[k].space_shape_array_ex->a[index]*
				time_shape3d(&s_p->pulses.pulse_p[k],
					     th,
					     x,
					     y);
                            s_p->Jhy_zmax_p->a[index] = field * sin_psi;
                        }
                    }
                }
            }
        }
    }

    if (s_p->tfsf_enable == true)
    {
  	zero_tfsf_fields_3d(s_p);

	for (int n=0; n < s_p->waves.num_waves; n++)
	{
	    if (s_p->waves.planewave_p[n].type == simple_e)
	    {
		/* plane wave travelling along z, optimized for performance*/
		update_pw_field_simple_wave(s_p, t, n);

		add_simple_wave_zmin(s_p, g_p, t, n);
		add_simple_wave_zmax(s_p, g_p, t, n);
		add_simple_wave_ymin(s_p, g_p, t, n);
		add_simple_wave_ymax(s_p, g_p, t, n);
		add_simple_wave_xmin(s_p, g_p, t, n);
		add_simple_wave_xmax(s_p, g_p, t, n);
	    }
	    else if (s_p->waves.planewave_p[n].type == dipole_e)
	    {
		/* dipole wave */
		add_dipole_wave_zmin(s_p, g_p, t, n);
		add_dipole_wave_zmax(s_p, g_p, t, n);
		add_dipole_wave_ymin(s_p, g_p, t, n);
		add_dipole_wave_ymax(s_p, g_p, t, n);
		add_dipole_wave_xmin(s_p, g_p, t, n);
		add_dipole_wave_xmax(s_p, g_p, t, n);
	    }
	    else
	    {
		/* plane wave */
		update_pw_field(s_p, n);
	    
		if (s_p->waves.planewave_p[n].linear == true)
		{
		    add_linear_wave_zmin(s_p, g_p, t, n);
		    add_linear_wave_zmax(s_p, g_p, t, n);
		    add_linear_wave_ymin(s_p, g_p, t, n);
		    add_linear_wave_ymax(s_p, g_p, t, n);
		    add_linear_wave_xmin(s_p, g_p, t, n);
		    add_linear_wave_xmax(s_p, g_p, t, n);
		}
		else
		{
		    add_circular_wave_zmin(s_p, g_p, t, n);
		    add_circular_wave_zmax(s_p, g_p, t, n);
		    add_circular_wave_ymin(s_p, g_p, t, n);
		    add_circular_wave_ymax(s_p, g_p, t, n);
		    add_circular_wave_xmin(s_p, g_p, t, n);
		    add_circular_wave_xmax(s_p, g_p, t, n);
		}
	    }
	}
    }
        
#ifdef CUDA
    update_sources_rt3d_cuda(s_p);
#endif
}

static void add_linear_wave_zmin(sources_rt_data* s_p,
				 geometry_data* g_p,
				 int t,
				 int n)
{
    int i_off = s_p->x_off;
    int j_off = s_p->y_off;

    FP dx = g_p->delta_x;
    FP dy = g_p->delta_y;
    FP dz = g_p->delta_z;

    FP ex = s_p->waves.planewave_p[n].ex;
    FP ey = s_p->waves.planewave_p[n].ey;
    FP hx = s_p->waves.planewave_p[n].hx;
    FP hy = s_p->waves.planewave_p[n].hy;

    if (s_p->tfsf_zmin == true)
    {
	for (int i=0; i< s_p->tfsf_ex_zmin_p->nx; i++)
	{
	    for (int j=0; j< s_p->tfsf_ex_zmin_p->ny; j++)
	    {
		int index = INDEX_XY_P(s_p->tfsf_ey_zmin_p, i, j);

		FP d = calc_dist_3d(s_p,
					n,
					(i+1.5+i_off)*dx,
					(j+1+j_off)*dy,
					dz);
		
		FP field = interpolate_pw_field_e(s_p, n, t, d);

		s_p->tfsf_ex_zmin_p->a[index] += ex * field;

		d = calc_dist_3d(s_p,
				 n,
				 (i+1+i_off)*dx,
				 (j+1.5+j_off)*dy,
				 dz);

		field = interpolate_pw_field_e(s_p, n, t, d);

		s_p->tfsf_ey_zmin_p->a[index] += ey * field;
	    }

	}


	for (int i=0; i< s_p->tfsf_hx_zmin_p->nx; i++)
	{
	    for (int j=0; j< s_p->tfsf_hx_zmin_p->ny; j++)
	    {
		int index = INDEX_XY_P(s_p->tfsf_hy_zmin_p, i, j);
		FP d = calc_dist_3d(s_p,
					n,
					(i+1+i_off)*dx,
					(j+1.5+j_off)*dy,
					0.5*dz);

		FP field = interpolate_pw_field_h(s_p, n, t, d);

		s_p->tfsf_hx_zmin_p->a[index] += hx * field;

		d = calc_dist_3d(s_p,
				 n,
				 (i+1.5+i_off)*dx,
				 (j+1+j_off)*dy,
				 0.5*dz);
		    
		field = interpolate_pw_field_h(s_p, n, t, d);

		s_p->tfsf_hy_zmin_p->a[index] += hy * field;
	    }
	}
    }
}

static void add_linear_wave_zmax(sources_rt_data* s_p,
				 geometry_data* g_p,
				 int t,
				 int n)
{
    int i_off = s_p->x_off;
    int j_off = s_p->y_off;

    FP dx = g_p->delta_x;
    FP dy = g_p->delta_y;
    FP dz = g_p->delta_z;

    int zwf = s_p->z_w_full;

    FP ex = s_p->waves.planewave_p[n].ex;
    FP ey = s_p->waves.planewave_p[n].ey;
    FP hx = s_p->waves.planewave_p[n].hx;
    FP hy = s_p->waves.planewave_p[n].hy;

    if (s_p->tfsf_zmax == true)
    {
	for (int i=0; i< s_p->tfsf_ex_zmax_p->nx; i++)
	{
	    for (int j=0; j< s_p->tfsf_ex_zmax_p->ny; j++)
	    {
		int index = INDEX_XY_P(s_p->tfsf_ey_zmax_p, i, j);
		FP d = calc_dist_3d(s_p,
					n,
					(i+1.5+i_off)*dx,
					(j+1+j_off)*dy,
					(zwf+1)*dz);
		
		FP field = interpolate_pw_field_e(s_p, n, t, d);

		s_p->tfsf_ex_zmax_p->a[index] += ex * field;

		d = calc_dist_3d(s_p,
				 n,
				 (i+1+i_off)*dx,
				 (j+1.5+j_off)*dy,
				 (zwf+1)*dz);
		    
		field = interpolate_pw_field_e(s_p, n, t, d);

		s_p->tfsf_ey_zmax_p->a[index] += ey * field;
	    }
	}

	for (int i=0; i< s_p->tfsf_hx_zmax_p->nx; i++)
	{
	    for (int j=0; j< s_p->tfsf_hx_zmax_p->ny; j++)
	    {
		int index = INDEX_XY_P(s_p->tfsf_hy_zmax_p, i, j);
		FP d = calc_dist_3d(s_p,
					n,
					(i+1+i_off)*dx,
					(j+1.5+j_off)*dy,
					(zwf+0.5)*dz);
		    
		FP field = interpolate_pw_field_h(s_p, n, t, d);

		s_p->tfsf_hx_zmax_p->a[index] += hx * field;

		d = calc_dist_3d(s_p,
				 n,
				 (i+1.5+i_off)*dx,
				 (j+1+j_off)*dy,
				 (zwf+0.5)*dz);
		    
		field = interpolate_pw_field_h(s_p, n, t, d);

		s_p->tfsf_hy_zmax_p->a[index] += hy * field;
	    }
	}
    }
}

static void add_linear_wave_ymin(sources_rt_data* s_p,
				 geometry_data* g_p,
				 int t,
				 int n)
{
    int i_off = s_p->x_off;
    int k_off = s_p->z_off;

    FP dx = g_p->delta_x;
    FP dy = g_p->delta_y;
    FP dz = g_p->delta_z;

    FP ex = s_p->waves.planewave_p[n].ex;
    FP ez = s_p->waves.planewave_p[n].ez;
    FP hx = s_p->waves.planewave_p[n].hx;
    FP hz = s_p->waves.planewave_p[n].hz;
    
    if (s_p->tfsf_ymin == true)
    {
	for (int i=0; i< s_p->tfsf_ex_ymin_p->nx; i++)
	{
	    for (int k=0; k< s_p->tfsf_ex_ymin_p->ny; k++)
	    {
		int index = INDEX_XY_P(s_p->tfsf_ex_ymin_p, i, k);
		FP d = calc_dist_3d(s_p,
					n,
					(i+1.5+i_off)*dx,
					dy,
					(k+1+k_off)*dz);
		    
		FP field = interpolate_pw_field_e(s_p, n, t, d);

		s_p->tfsf_ex_ymin_p->a[index] += ex * field;

		d = calc_dist_3d(s_p,
				 n,
				 (i+1+i_off)*dx,
				 dy,
				 (k+1.5+k_off)*dz);
		    
		field = interpolate_pw_field_e(s_p, n, t, d);

		s_p->tfsf_ez_ymin_p->a[index] += ez * field;
	    }
	}

	for (int i=0; i< s_p->tfsf_hx_ymin_p->nx; i++)
	{
	    for (int k=0; k< s_p->tfsf_hx_ymin_p->ny; k++)
	    {
		int index = INDEX_XY_P(s_p->tfsf_hx_ymin_p, i, k);
		FP d = calc_dist_3d(s_p,
					n,
					(i+1+i_off)*dx,
					0.5*dy,
					(k+1.5+k_off)*dz);
		    
		FP field = interpolate_pw_field_h(s_p, n, t, d);
		
		s_p->tfsf_hx_ymin_p->a[index] += hx * field;

		d = calc_dist_3d(s_p,
				 n,
				 (i+1.5+i_off)*dx,
				 0.5*dy,
				 (k+1+k_off)*dz);
		    
		field = interpolate_pw_field_h(s_p, n, t, d);

		s_p->tfsf_hz_ymin_p->a[index] += hz * field;
	    }
	}
    }
}

static void add_linear_wave_ymax(sources_rt_data* s_p,
				 geometry_data* g_p,
				 int t,
				 int n)
{
    int i_off = s_p->x_off;
    int k_off = s_p->z_off;

    FP dx = g_p->delta_x;
    FP dy = g_p->delta_y;
    FP dz = g_p->delta_z;

    int ywf = s_p->y_w_full;
    
    FP ex = s_p->waves.planewave_p[n].ex;
    FP ez = s_p->waves.planewave_p[n].ez;
    FP hx = s_p->waves.planewave_p[n].hx;
    FP hz = s_p->waves.planewave_p[n].hz;
    
    if (s_p->tfsf_ymax == true)
    {
	for (int i=0; i< s_p->tfsf_ex_ymax_p->nx; i++)
	{
	    for (int k=0; k< s_p->tfsf_ez_ymax_p->ny; k++)
	    {
		int index = INDEX_XY_P(s_p->tfsf_ex_ymax_p, i, k);
		FP d = calc_dist_3d(s_p,
					n,
					(i+1.5+i_off)*dx,
					(ywf+1)*dy,
					(k+1+k_off)*dz);
		    
		FP field = interpolate_pw_field_e(s_p, n, t, d);

		s_p->tfsf_ex_ymax_p->a[index] += ex * field;

		d = calc_dist_3d(s_p,
				 n,
				 (i+1+i_off)*dx,
				 (ywf+1)*dy,
				 (k+1.5+k_off)*dz);
		    
		field = interpolate_pw_field_e(s_p, n, t, d);
		
		s_p->tfsf_ez_ymax_p->a[index] += ez * field;
	    }
	}

	for (int i=0; i< s_p->tfsf_hx_ymax_p->nx; i++)
	{
	    for (int k=0; k< s_p->tfsf_hx_ymax_p->ny; k++)
	    {
		int index = INDEX_XY_P(s_p->tfsf_hx_ymax_p, i, k);
		FP d = calc_dist_3d(s_p,
					n,
					(i+1+i_off)*dx,
					(ywf+0.5)*dy,
					(k+1.5+k_off)*dz);
		    
		FP field = interpolate_pw_field_h(s_p, n, t, d);
		    
		s_p->tfsf_hx_ymax_p->a[index] += hx * field;

		d = calc_dist_3d(s_p,
				 n,
				 (i+1.5+i_off)*dx,
				 (ywf+0.5)*dy,
				 (k+1+k_off)*dz);
		    
		field = interpolate_pw_field_h(s_p, n, t, d);
	
		s_p->tfsf_hz_ymax_p->a[index] += hz * field;
	    }
	}
    }
}

static void add_linear_wave_xmin(sources_rt_data* s_p,
				 geometry_data* g_p,
				 int t,
				 int n)
{
    int j_off = s_p->y_off;
    int k_off = s_p->z_off;

    FP dx = g_p->delta_x;
    FP dy = g_p->delta_y;
    FP dz = g_p->delta_z;

    FP ey = s_p->waves.planewave_p[n].ey;
    FP ez = s_p->waves.planewave_p[n].ez;
    FP hy = s_p->waves.planewave_p[n].hy;
    FP hz = s_p->waves.planewave_p[n].hz;
    
    if (s_p->tfsf_xmin == true)
    {
	for (int j=0; j< s_p->tfsf_ey_xmin_p->nx; j++)
	{
	    for (int k=0; k< s_p->tfsf_ey_xmin_p->ny; k++)
	    {
		int index = INDEX_XY_P(s_p->tfsf_ey_xmin_p, j, k);
		FP d = calc_dist_3d(s_p,
					n,
					dx,
					(j+1.5+j_off)*dy,
					(k+1+k_off)*dz);
		    
		FP field = interpolate_pw_field_e(s_p, n, t, d);
	
		s_p->tfsf_ey_xmin_p->a[index] += ey * field;

		d = calc_dist_3d(s_p,
				 n,
				 dx,
				 (j+1+j_off)*dy,
				 (k+1.5+k_off)*dz);
		    
		field = interpolate_pw_field_e(s_p, n, t, d);
	
		s_p->tfsf_ez_xmin_p->a[index] += ez * field;
	    }
	}

	for (int j=0; j< s_p->tfsf_hy_xmin_p->nx; j++)
	{
	    for (int k=0; k< s_p->tfsf_hy_xmin_p->ny; k++)
	    {
		int index = INDEX_XY_P(s_p->tfsf_hy_xmin_p, j, k);
		FP d = calc_dist_3d(s_p,
					n,
					0.5*dx,
					(j+1+j_off)*dy,
					(k+1.5+k_off)*dz);
		    
		FP field = interpolate_pw_field_h(s_p, n, t, d);
		    
		s_p->tfsf_hy_xmin_p->a[index] += hy * field;

		d = calc_dist_3d(s_p,
				 n,
				 0.5*dx,
				 (j+1.5+j_off)*dy,
				 (k+1+k_off)*dz);
		    
		field = interpolate_pw_field_h(s_p, n, t, d);

		s_p->tfsf_hz_xmin_p->a[index] += hz * field;
	    }
	}
    }
}

static void add_linear_wave_xmax(sources_rt_data* s_p,
				 geometry_data* g_p,
				 int t,
				 int n)
{
    int j_off = s_p->y_off;
    int k_off = s_p->z_off;

    FP dx = g_p->delta_x;
    FP dy = g_p->delta_y;
    FP dz = g_p->delta_z;

    int xwf = s_p->x_w_full;

    FP ey = s_p->waves.planewave_p[n].ey;
    FP ez = s_p->waves.planewave_p[n].ez;
    FP hy = s_p->waves.planewave_p[n].hy;
    FP hz = s_p->waves.planewave_p[n].hz;

    if (s_p->tfsf_xmax == true)
    {
	for (int j=0; j< s_p->tfsf_ey_xmax_p->nx; j++)
	{
	    for (int k=0; k< s_p->tfsf_ey_xmax_p->ny; k++)
	    {
		int index = INDEX_XY_P(s_p->tfsf_ey_xmax_p, j, k);
		FP d = calc_dist_3d(s_p,
					n,
					(xwf+1)*dx,
					(j+1.5+j_off)*dy,
					(k+1+k_off)*dz);
		    
		FP field = interpolate_pw_field_e(s_p, n, t, d);
	
		s_p->tfsf_ey_xmax_p->a[index] += ey * field;

		d = calc_dist_3d(s_p,
				 n,
				 (xwf+1)*dx,
				 (j+1+j_off)*dy,
				 (k+1.5+k_off)*dz);
		    
		field = interpolate_pw_field_e(s_p, n, t, d);
	
		s_p->tfsf_ez_xmax_p->a[index] += ez * field;
	    }
	}

	for (int j=0; j< s_p->tfsf_hy_xmax_p->nx; j++)
	{
	    for (int k=0; k< s_p->tfsf_hy_xmax_p->ny; k++)
	    {
		int index = INDEX_XY_P(s_p->tfsf_hy_xmax_p, j, k);
		FP d = calc_dist_3d(s_p,
					n,
					(xwf+0.5)*dx,
					(j+1+j_off)*dy,
					(k+1.5+k_off)*dz);
		    
		FP field = interpolate_pw_field_h(s_p, n, t, d);
	
		s_p->tfsf_hy_xmax_p->a[index] += hy * field;

		d = calc_dist_3d(s_p,
				 n,
				 (xwf+0.5)*dx,
				 (j+1.5+j_off)*dy,
				 (k+1+k_off)*dz);
		    
		field = interpolate_pw_field_h(s_p, n, t, d);
	
		s_p->tfsf_hz_xmax_p->a[index] += hz * field;
	    }
	}
    }
}

static void add_simple_wave_zmin(sources_rt_data* s_p,
				 geometry_data* g_p,
				 int t,
				 int n)
{
    if (s_p->tfsf_zmin == true)
    {
	for (int i=0; i< s_p->tfsf_ex_zmin_p->nx; i++)
	{
	    for (int j=0; j< s_p->tfsf_ex_zmin_p->ny; j++)
	    {
		int index = INDEX_XY_P(s_p->tfsf_ey_zmin_p, i, j);
		s_p->tfsf_ey_zmin_p->a[index] = -s_p->waves.planewave_p[n].E_a[0].field;
		s_p->tfsf_hx_zmin_p->a[index] = s_p->waves.planewave_p[n].H_a[0].field;
	    }
	}
    }
}

static void add_simple_wave_zmax(sources_rt_data* s_p,
				 geometry_data* g_p,
				 int t,
				 int n)
{
    if (s_p->tfsf_zmax == true)
    {
	for (int i=0; i< s_p->tfsf_ex_zmax_p->nx; i++)
	{
	    for (int j=0; j< s_p->tfsf_ex_zmax_p->ny; j++)
	    {
		int index = INDEX_XY_P(s_p->tfsf_ey_zmax_p, i, j);
		s_p->tfsf_ey_zmax_p->a[index] =
		    -s_p->waves.planewave_p[n].E_a[s_p->waves.planewave_p[n].num-1].field;
		s_p->tfsf_hx_zmax_p->a[index] =
		    s_p->waves.planewave_p[n].H_a[s_p->waves.planewave_p[n].num-1].field;
	    }
	}
    }
}

static void add_simple_wave_ymin(sources_rt_data* s_p,
				 geometry_data* g_p,
				 int t,
				 int n)
{
    if (s_p->tfsf_ymin == true)
    {
	for (int i=0; i< s_p->tfsf_hx_ymin_p->nx; i++)
	{
	    for (int k=0; k< s_p->tfsf_hx_ymin_p->ny; k++)
	    {
		int index = INDEX_XY_P(s_p->tfsf_hx_ymin_p, i, k);
		s_p->tfsf_hx_ymin_p->a[index] = s_p->waves.planewave_p[n].H_a[k+1].field;
	    }
	}
    }
}

static void add_simple_wave_ymax(sources_rt_data* s_p,
				 geometry_data* g_p,
				 int t,
				 int n)
{
    if (s_p->tfsf_ymax == true)
    {
	for (int i=0; i< s_p->tfsf_hx_ymax_p->nx; i++)
	{
	    for (int k=0; k< s_p->tfsf_hx_ymax_p->ny; k++)
	    {
		int index = INDEX_XY_P(s_p->tfsf_hx_ymax_p, i, k);
		s_p->tfsf_hx_ymax_p->a[index] = s_p->waves.planewave_p[n].H_a[k+1].field;
	    }
	}
    }
}

static void add_simple_wave_xmin(sources_rt_data* s_p,
				 geometry_data* g_p,
				 int t,
				 int n)
{
    if (s_p->tfsf_xmin == true)
    {
	for (int j=0; j< s_p->tfsf_ey_xmin_p->nx; j++)
	{
	    for (int k=0; k< s_p->tfsf_ey_xmin_p->ny; k++)
	    {
		int index = INDEX_XY_P(s_p->tfsf_ey_xmin_p, j, k);
		s_p->tfsf_ey_xmin_p->a[index] = -s_p->waves.planewave_p[n].E_a[k].field;
	    }
	}
    }
}

static void add_simple_wave_xmax(sources_rt_data* s_p,
				 geometry_data* g_p,
				 int t,
				 int n)
{
    if (s_p->tfsf_xmax == true)
    {
	for (int j=0; j< s_p->tfsf_ey_xmax_p->nx; j++)
	{
	    for (int k=0; k< s_p->tfsf_ey_xmax_p->ny; k++)
	    {
		int index = INDEX_XY_P(s_p->tfsf_ey_xmax_p, j, k);
		s_p->tfsf_ey_xmax_p->a[index] = -s_p->waves.planewave_p[n].E_a[k].field;
	    }
	}
    }
}

static void add_circular_wave_zmin(sources_rt_data* s_p,
				   geometry_data* g_p,
				   int t,
				   int n)
{
    int i_off = s_p->x_off;
    int j_off = s_p->y_off;

    FP dx = g_p->delta_x;
    FP dy = g_p->delta_y;
    FP dz = g_p->delta_z;
    
    FP ex = s_p->waves.planewave_p[n].ex;
    FP ey = s_p->waves.planewave_p[n].ey;
    FP hx = s_p->waves.planewave_p[n].hx;
    FP hy = s_p->waves.planewave_p[n].hy;

    if (s_p->tfsf_zmin == true)
    {
	for (int i=0; i< s_p->tfsf_ex_zmin_p->nx; i++)
	{
	    for (int j=0; j< s_p->tfsf_ex_zmin_p->ny; j++)
	    {
		int index = INDEX_XY_P(s_p->tfsf_ey_zmin_p, i, j);

		FP d = calc_dist_3d(s_p,
					n,
					(i+1.5+i_off)*dx,
					(j+1+j_off)*dy,
					dz);

		FP field = interpolate_pw_field_e(s_p, n, t, d);
		FP phase = interpolate_pw_phase_e(s_p, n, d);
			
		ex = s_p->waves.planewave_p[n].exc * cos(phase) +
		    s_p->waves.planewave_p[n].exs * sin(phase);
		
		s_p->tfsf_ex_zmin_p->a[index] += ex * field;

		d = calc_dist_3d(s_p,
				 n,
				 (i+1+i_off)*dx,
				 (j+1.5+j_off)*dy,
				 dz);

		field = interpolate_pw_field_e(s_p, n, t, d);
		phase = interpolate_pw_phase_e(s_p, n, d);

		ey = s_p->waves.planewave_p[n].eyc * cos(phase) +
		    s_p->waves.planewave_p[n].eys * sin(phase);

		s_p->tfsf_ey_zmin_p->a[index] += ey * field;
	    }

	}


	for (int i=0; i< s_p->tfsf_hx_zmin_p->nx; i++)
	{
	    for (int j=0; j< s_p->tfsf_hx_zmin_p->ny; j++)
	    {
		int index = INDEX_XY_P(s_p->tfsf_hy_zmin_p, i, j);
		FP d = calc_dist_3d(s_p,
					n,
					(i+1+i_off)*dx,
					(j+1.5+j_off)*dy,
					0.5*dz);

		FP field = interpolate_pw_field_h(s_p, n, t, d);
		FP phase = interpolate_pw_phase_h(s_p, n, d);
		
		hx = s_p->waves.planewave_p[n].hxc * cos(phase) +
		    s_p->waves.planewave_p[n].hxs * sin(phase);

		s_p->tfsf_hx_zmin_p->a[index] += hx * field;

		d = calc_dist_3d(s_p,
				 n,
				 (i+1.5+i_off)*dx,
				 (j+1+j_off)*dy,
				 0.5*dz);
		    
		field = interpolate_pw_field_h(s_p, n, t, d);
		phase = interpolate_pw_phase_h(s_p, n, d);
			
		hy = s_p->waves.planewave_p[n].hyc * cos(phase) +
		    s_p->waves.planewave_p[n].hys * sin(phase);
		
		s_p->tfsf_hy_zmin_p->a[index] += hy * field;
	    }
	}
    }
}

static void add_circular_wave_zmax(sources_rt_data* s_p,
				   geometry_data* g_p,
				   int t,
				   int n)
{
    int i_off = s_p->x_off;
    int j_off = s_p->y_off;

    FP dx = g_p->delta_x;
    FP dy = g_p->delta_y;
    FP dz = g_p->delta_z;

    int zwf = s_p->z_w_full;

    FP ex = s_p->waves.planewave_p[n].ex;
    FP ey = s_p->waves.planewave_p[n].ey;
    FP hx = s_p->waves.planewave_p[n].hx;
    FP hy = s_p->waves.planewave_p[n].hy;

    if (s_p->tfsf_zmax == true)
    {
	for (int i=0; i< s_p->tfsf_ex_zmax_p->nx; i++)
	{
	    for (int j=0; j< s_p->tfsf_ex_zmax_p->ny; j++)
	    {
		int index = INDEX_XY_P(s_p->tfsf_ey_zmax_p, i, j);
		FP d = calc_dist_3d(s_p,
					n,
					(i+1.5+i_off)*dx,
					(j+1+j_off)*dy,
					(zwf+1)*dz);
		    
		FP field = interpolate_pw_field_e(s_p, n, t, d);
		FP phase = interpolate_pw_phase_e(s_p, n, d);
		
		ex = s_p->waves.planewave_p[n].exc * cos(phase) +
		    s_p->waves.planewave_p[n].exs * sin(phase);
		
		s_p->tfsf_ex_zmax_p->a[index] += ex * field;

		d = calc_dist_3d(s_p,
				 n,
				 (i+1+i_off)*dx,
				 (j+1.5+j_off)*dy,
				 (zwf+1)*dz);
		    
		field = interpolate_pw_field_e(s_p, n, t, d);
		phase = interpolate_pw_phase_e(s_p, n, d);
			
		ey = s_p->waves.planewave_p[n].eyc * cos(phase) +
		    s_p->waves.planewave_p[n].eys * sin(phase);
		
		s_p->tfsf_ey_zmax_p->a[index] += ey * field;
	    }
	}

	for (int i=0; i< s_p->tfsf_hx_zmax_p->nx; i++)
	{
	    for (int j=0; j< s_p->tfsf_hx_zmax_p->ny; j++)
	    {
		int index = INDEX_XY_P(s_p->tfsf_hy_zmax_p, i, j);
		FP d = calc_dist_3d(s_p,
					n,
					(i+1+i_off)*dx,
					(j+1.5+j_off)*dy,
					(zwf+0.5)*dz);
		    
		FP field = interpolate_pw_field_h(s_p, n, t, d);
		FP phase = interpolate_pw_phase_h(s_p, n, d);
		
		hx = s_p->waves.planewave_p[n].hxc * cos(phase) +
		    s_p->waves.planewave_p[n].hxs * sin(phase);
		
		s_p->tfsf_hx_zmax_p->a[index] += hx * field;

		d = calc_dist_3d(s_p,
				 n,
				 (i+1.5+i_off)*dx,
				 (j+1+j_off)*dy,
				 (zwf+0.5)*dz);
		    
		field = interpolate_pw_field_h(s_p, n, t, d);
		phase = interpolate_pw_phase_h(s_p, n, d);
			
		hy = s_p->waves.planewave_p[n].hyc * cos(phase) +
		    s_p->waves.planewave_p[n].hys * sin(phase);
		
		s_p->tfsf_hy_zmax_p->a[index] += hy * field;
	    }
	}
    }
}

static void add_circular_wave_ymin(sources_rt_data* s_p,
				   geometry_data* g_p,
				   int t,
				   int n)
{
    int i_off = s_p->x_off;
    int k_off = s_p->z_off;

    FP dx = g_p->delta_x;
    FP dy = g_p->delta_y;
    FP dz = g_p->delta_z;
    
    FP ex = s_p->waves.planewave_p[n].ex;
    FP ez = s_p->waves.planewave_p[n].ez;
    FP hx = s_p->waves.planewave_p[n].hx;
    FP hz = s_p->waves.planewave_p[n].hz;
    
    if (s_p->tfsf_ymin == true)
    {
	for (int i=0; i< s_p->tfsf_ex_ymin_p->nx; i++)
	{
	    for (int k=0; k< s_p->tfsf_ex_ymin_p->ny; k++)
	    {
		int index = INDEX_XY_P(s_p->tfsf_ex_ymin_p, i, k);
		FP d = calc_dist_3d(s_p,
					n,
					(i+1.5+i_off)*dx,
					dy,
					(k+1+k_off)*dz);
		    
		FP field = interpolate_pw_field_e(s_p, n, t, d);
		FP phase = interpolate_pw_phase_e(s_p, n, d);
		
		ex = s_p->waves.planewave_p[n].exc * cos(phase) +
		    s_p->waves.planewave_p[n].exs * sin(phase);
		
		s_p->tfsf_ex_ymin_p->a[index] += ex * field;

		d = calc_dist_3d(s_p,
				 n,
				 (i+1+i_off)*dx,
				 dy,
				 (k+1.5+k_off)*dz);
		    
		field = interpolate_pw_field_e(s_p, n, t, d);
		phase = interpolate_pw_phase_e(s_p, n, d);
			
		ez = s_p->waves.planewave_p[n].ezs * sin(phase);
		
		s_p->tfsf_ez_ymin_p->a[index] += ez * field;
	    }
	}

	for (int i=0; i< s_p->tfsf_hx_ymin_p->nx; i++)
	{
	    for (int k=0; k< s_p->tfsf_hx_ymin_p->ny; k++)
	    {
		int index = INDEX_XY_P(s_p->tfsf_hx_ymin_p, i, k);
		FP d = calc_dist_3d(s_p,
					n,
					(i+1+i_off)*dx,
					0.5*dy,
					(k+1.5+k_off)*dz);
		    
		FP field = interpolate_pw_field_h(s_p, n, t, d);
		FP phase = interpolate_pw_phase_h(s_p, n, d);
		
		hx = s_p->waves.planewave_p[n].hxc * cos(phase) +
		    s_p->waves.planewave_p[n].hxs * sin(phase);
		
		s_p->tfsf_hx_ymin_p->a[index] += hx * field;

		d = calc_dist_3d(s_p,
				 n,
				 (i+1.5+i_off)*dx,
				 0.5*dy,
				 (k+1+k_off)*dz);
		    
		field = interpolate_pw_field_h(s_p, n, t, d);
		phase = interpolate_pw_phase_h(s_p, n, d);
			
		hz = s_p->waves.planewave_p[n].hzc * cos(phase);
	
		s_p->tfsf_hz_ymin_p->a[index] += hz * field;
	    }
	}
    }
}

static void add_circular_wave_ymax(sources_rt_data* s_p,
				   geometry_data* g_p,
				   int t,
				   int n)
{
    int i_off = s_p->x_off;
    int k_off = s_p->z_off;

    FP dx = g_p->delta_x;
    FP dy = g_p->delta_y;
    FP dz = g_p->delta_z;

    int ywf = s_p->y_w_full;
    
    FP ex = s_p->waves.planewave_p[n].ex;
    FP ez = s_p->waves.planewave_p[n].ez;
    FP hx = s_p->waves.planewave_p[n].hx;
    FP hz = s_p->waves.planewave_p[n].hz;
    
    if (s_p->tfsf_ymax == true)
    {
	for (int i=0; i< s_p->tfsf_ex_ymax_p->nx; i++)
	{
	    for (int k=0; k< s_p->tfsf_ez_ymax_p->ny; k++)
	    {
		int index = INDEX_XY_P(s_p->tfsf_ex_ymax_p, i, k);
		FP d = calc_dist_3d(s_p,
					n,
					(i+1.5+i_off)*dx,
					(ywf+1)*dy,
					(k+1+k_off)*dz);
		    
		FP field = interpolate_pw_field_e(s_p, n, t, d);
		FP phase = interpolate_pw_phase_e(s_p, n, d);
		
		ex = s_p->waves.planewave_p[n].exc * cos(phase) +
		    s_p->waves.planewave_p[n].exs * sin(phase);
		
		s_p->tfsf_ex_ymax_p->a[index] += ex * field;

		d = calc_dist_3d(s_p,
				 n,
				 (i+1+i_off)*dx,
				 (ywf+1)*dy,
				 (k+1.5+k_off)*dz);
		    
		field = interpolate_pw_field_e(s_p, n, t, d);
		phase = interpolate_pw_phase_e(s_p, n, d);

		ez = s_p->waves.planewave_p[n].ezs * sin(phase);
		
		s_p->tfsf_ez_ymax_p->a[index] += ez * field;
	    }
	}

	for (int i=0; i< s_p->tfsf_hx_ymax_p->nx; i++)
	{
	    for (int k=0; k< s_p->tfsf_hx_ymax_p->ny; k++)
	    {
		int index = INDEX_XY_P(s_p->tfsf_hx_ymax_p, i, k);
		FP d = calc_dist_3d(s_p,
					n,
					(i+1+i_off)*dx,
					(ywf+0.5)*dy,
					(k+1.5+k_off)*dz);
		    
		FP field = interpolate_pw_field_h(s_p, n, t, d);
		FP phase = interpolate_pw_phase_h(s_p, n, d);
		
		hx = s_p->waves.planewave_p[n].hxc * cos(phase) +
		    s_p->waves.planewave_p[n].hxs * sin(phase);
		
		s_p->tfsf_hx_ymax_p->a[index] += hx * field;

		d = calc_dist_3d(s_p,
				 n,
				 (i+1.5+i_off)*dx,
				 (ywf+0.5)*dy,
				 (k+1+k_off)*dz);
		    
		field = interpolate_pw_field_h(s_p, n, t, d);
		phase = interpolate_pw_phase_h(s_p, n, d);
			
		hz = s_p->waves.planewave_p[n].hzc * cos(phase);
		
		s_p->tfsf_hz_ymax_p->a[index] += hz * field;
	    }
	}
    }
}

static void add_circular_wave_xmin(sources_rt_data* s_p,
				   geometry_data* g_p,
				   int t,
				   int n)
{
    int j_off = s_p->y_off;
    int k_off = s_p->z_off;

    FP dx = g_p->delta_x;
    FP dy = g_p->delta_y;
    FP dz = g_p->delta_z;

    FP ey = s_p->waves.planewave_p[n].ey;
    FP ez = s_p->waves.planewave_p[n].ez;
    FP hy = s_p->waves.planewave_p[n].hy;
    FP hz = s_p->waves.planewave_p[n].hz;
    
    if (s_p->tfsf_xmin == true)
    {
	for (int j=0; j< s_p->tfsf_ey_xmin_p->nx; j++)
	{
	    for (int k=0; k< s_p->tfsf_ey_xmin_p->ny; k++)
	    {
		int index = INDEX_XY_P(s_p->tfsf_ey_xmin_p, j, k);
		FP d = calc_dist_3d(s_p,
					n,
					dx,
					(j+1.5+j_off)*dy,
					(k+1+k_off)*dz);
		    
		FP field = interpolate_pw_field_e(s_p, n, t, d);
		FP phase = interpolate_pw_phase_e(s_p, n, d);
		
		ey = s_p->waves.planewave_p[n].eyc * cos(phase) +
		    s_p->waves.planewave_p[n].eys * sin(phase);
		
		s_p->tfsf_ey_xmin_p->a[index] += ey * field;

		d = calc_dist_3d(s_p,
				 n,
				 dx,
				 (j+1+j_off)*dy,
				 (k+1.5+k_off)*dz);
		    
		field = interpolate_pw_field_e(s_p, n, t, d);
		phase = interpolate_pw_phase_h(s_p, n, d);
			
		ez = s_p->waves.planewave_p[n].ezs * sin(phase);
		
		s_p->tfsf_ez_xmin_p->a[index] += ez * field;
	    }
	}

	for (int j=0; j< s_p->tfsf_hy_xmin_p->nx; j++)
	{
	    for (int k=0; k< s_p->tfsf_hy_xmin_p->ny; k++)
	    {
		int index = INDEX_XY_P(s_p->tfsf_hy_xmin_p, j, k);
		FP d = calc_dist_3d(s_p,
					n,
					0.5*dx,
					(j+1+j_off)*dy,
					(k+1.5+k_off)*dz);
		    
		FP field = interpolate_pw_field_h(s_p, n, t, d);
		FP phase = interpolate_pw_phase_h(s_p, n, d);
		
		hy = s_p->waves.planewave_p[n].hyc * cos(phase) +
		    s_p->waves.planewave_p[n].hys * sin(phase);
		
		s_p->tfsf_hy_xmin_p->a[index] += hy * field;

		d = calc_dist_3d(s_p,
				 n,
				 0.5*dx,
				 (j+1.5+j_off)*dy,
				 (k+1+k_off)*dz);
		    
		field = interpolate_pw_field_h(s_p, n, t, d);
		phase = interpolate_pw_phase_h(s_p, n, d);
			
		hz = s_p->waves.planewave_p[n].hzc * cos(phase);
		
		s_p->tfsf_hz_xmin_p->a[index] += hz * field;
	    }
	}
    }
}

static void add_circular_wave_xmax(sources_rt_data* s_p,
				   geometry_data* g_p,
				   int t,
				   int n)
{
    int j_off = s_p->y_off;
    int k_off = s_p->z_off;

    FP dx = g_p->delta_x;
    FP dy = g_p->delta_y;
    FP dz = g_p->delta_z;

    int xwf = s_p->x_w_full;

    FP ey = s_p->waves.planewave_p[n].ey;
    FP ez = s_p->waves.planewave_p[n].ez;
    FP hy = s_p->waves.planewave_p[n].hy;
    FP hz = s_p->waves.planewave_p[n].hz;
    
    if (s_p->tfsf_xmax == true)
    {
	for (int j=0; j< s_p->tfsf_ey_xmax_p->nx; j++)
	{
	    for (int k=0; k< s_p->tfsf_ey_xmax_p->ny; k++)
	    {
		int index = INDEX_XY_P(s_p->tfsf_ey_xmax_p, j, k);
		FP d = calc_dist_3d(s_p,
					n,
					(xwf+1)*dx,
					(j+1.5+j_off)*dy,
					(k+1+k_off)*dz);
		    
		FP field = interpolate_pw_field_e(s_p, n, t, d);
		FP phase = interpolate_pw_phase_e(s_p, n, d);
		
		ey = s_p->waves.planewave_p[n].eyc * cos(phase) +
		    s_p->waves.planewave_p[n].eys * sin(phase);
		
		s_p->tfsf_ey_xmax_p->a[index] += ey * field;

		d = calc_dist_3d(s_p,
				 n,
				 (xwf+1)*dx,
				 (j+1+j_off)*dy,
				 (k+1.5+k_off)*dz);
		    
		field = interpolate_pw_field_e(s_p, n, t, d);
		phase = interpolate_pw_phase_e(s_p, n, d);
			
		ez = s_p->waves.planewave_p[n].ezs * sin(phase);
		
		s_p->tfsf_ez_xmax_p->a[index] += ez * field;
	    }
	}

	for (int j=0; j< s_p->tfsf_hy_xmax_p->nx; j++)
	{
	    for (int k=0; k< s_p->tfsf_hy_xmax_p->ny; k++)
	    {
		int index = INDEX_XY_P(s_p->tfsf_hy_xmax_p, j, k);
		FP d = calc_dist_3d(s_p,
					n,
					(xwf+0.5)*dx,
					(j+1+j_off)*dy,
					(k+1.5+k_off)*dz);
		    
		FP field = interpolate_pw_field_h(s_p, n, t, d);
		FP phase = interpolate_pw_phase_h(s_p, n, d);
		
		hy = s_p->waves.planewave_p[n].hyc * cos(phase) +
		    s_p->waves.planewave_p[n].hys * sin(phase);
		
		s_p->tfsf_hy_xmax_p->a[index] += hy * field;

		d = calc_dist_3d(s_p,
				 n,
				 (xwf+0.5)*dx,
				 (j+1.5+j_off)*dy,
				 (k+1+k_off)*dz);
		    
		field = interpolate_pw_field_h(s_p, n, t, d);
		phase = interpolate_pw_phase_h(s_p, n, d);
			
		hz = s_p->waves.planewave_p[n].hzc * cos(phase);
		
		s_p->tfsf_hz_xmax_p->a[index] += hz * field;
	    }
	}
    }
}

static void add_dipole_wave_zmin(sources_rt_data* s_p,
				 geometry_data* g_p,
				 int t,
				 int n)
{
    int i_off = s_p->x_off;
    int j_off = s_p->y_off;
  
    FP dx = g_p->delta_x;
    FP dy = g_p->delta_y;
    FP dz = g_p->delta_z;
    
    FP xwf_c = s_p->x_w_full/2*g_p->delta_x;
    FP ywf_c = s_p->y_w_full/2*g_p->delta_y;
    FP zwf_c = s_p->z_w_full/2*g_p->delta_z;
    
    FP R = sqrt(xwf_c*xwf_c + ywf_c*ywf_c + zwf_c*zwf_c);
    
    if (s_p->tfsf_zmin == true)
    {
	FP z_o = dz - zwf_c;
	
	for (int i=0; i< s_p->tfsf_ex_zmin_p->nx; i++)
	{
	    for (int j=0; j< s_p->tfsf_ex_zmin_p->ny; j++)
	    {
		int index = INDEX_XY_P(s_p->tfsf_ey_zmin_p, i, j);

		FP x_o = (i+1.5+i_off)*dx - xwf_c;
		FP y_o = (j+1+j_off)*dy - ywf_c;
			
		FP d = sqrt(x_o*x_o + y_o*y_o + z_o*z_o);
		
		FP field = time_shape_dipole_wave(&s_p->waves.planewave_p[n],
						      t*s_p->delta_t,
						      d,
						      R);    	
		
		s_p->tfsf_ex_zmin_p->a[index] += x_o * z_o * field/(d * d * d);
		
		x_o = (i+1+i_off)*dx - xwf_c;
		y_o = (j+1.5+j_off)*dy - ywf_c;
			
		d = sqrt(x_o*x_o + y_o*y_o + z_o*z_o);
		
		field = time_shape_dipole_wave(&s_p->waves.planewave_p[n],
					       t*s_p->delta_t,
					       d,
					       R);
			
		s_p->tfsf_ey_zmin_p->a[index] += y_o * z_o* field/(d*d*d);
	    }

	}

	z_o = 0.5*dz - zwf_c;
	
	for (int i=0; i< s_p->tfsf_hx_zmin_p->nx; i++)
	{
	    for (int j=0; j< s_p->tfsf_hx_zmin_p->ny; j++)
	    {
		int index = INDEX_XY_P(s_p->tfsf_hy_zmin_p, i, j);
		
		FP x_o = (i+1.+i_off)*dx - xwf_c;
		FP y_o = (j+1.5+j_off)*dy - ywf_c;
			
		FP d = sqrt(x_o*x_o + y_o*y_o + z_o*z_o);
		
		FP field = time_shape_dipole_wave(&s_p->waves.planewave_p[n],
						      (t-0.5)*s_p->delta_t,
						      d,
						      R);

		s_p->tfsf_hx_zmin_p->a[index] += y_o * field / (d*d);
	
		x_o = (i+1.5+i_off)*dx - xwf_c;
		y_o = (j+1.+j_off)*dy - ywf_c;
			
		d = sqrt(x_o*x_o + y_o*y_o + z_o*z_o);
		
		field = time_shape_dipole_wave(&s_p->waves.planewave_p[n],
					       (t-0.5)*s_p->delta_t,
					       d,
					       R);
	
		s_p->tfsf_hy_zmin_p->a[index] += -x_o * field / (d*d);
	    }
	}
    }
}

static void add_dipole_wave_zmax(sources_rt_data* s_p,
				 geometry_data* g_p,
				 int t,
				 int n)
{
    int i_off = s_p->x_off;
    int j_off = s_p->y_off;

    FP dx = g_p->delta_x;
    FP dy = g_p->delta_y;
    FP dz = g_p->delta_z;

    int zwf = s_p->z_w_full;
    
    FP xwf_c = s_p->x_w_full/2*g_p->delta_x;
    FP ywf_c = s_p->y_w_full/2*g_p->delta_y;
    FP zwf_c = s_p->z_w_full/2*g_p->delta_z;
    
    FP R = sqrt(xwf_c*xwf_c + ywf_c*ywf_c + zwf_c*zwf_c);

    if (s_p->tfsf_zmax == true)
    {
	FP z_o = (zwf+1)*dz - zwf_c;
	
	for (int i=0; i< s_p->tfsf_ex_zmax_p->nx; i++)
	{
	    for (int j=0; j< s_p->tfsf_ex_zmax_p->ny; j++)
	    {
		int index = INDEX_XY_P(s_p->tfsf_ey_zmax_p, i, j);
		
		FP x_o = (i+1.5+i_off)*dx - xwf_c;
		FP y_o = (j+1+j_off)*dy - ywf_c;
			
		FP d = sqrt(x_o*x_o + y_o*y_o + z_o*z_o);
		
		FP field = time_shape_dipole_wave(&s_p->waves.planewave_p[n],
						      t*s_p->delta_t,
						      d,
						      R);
		
		s_p->tfsf_ex_zmax_p->a[index] += x_o * z_o * field/(d * d * d);

		x_o = (i+1+i_off)*dx - xwf_c;
		y_o = (j+1.5+j_off)*dy - ywf_c;
	
		d = sqrt(x_o*x_o + y_o*y_o + z_o*z_o);
		
		field = time_shape_dipole_wave(&s_p->waves.planewave_p[n],
					       t*s_p->delta_t,
					       d,
					       R);
		
		s_p->tfsf_ey_zmax_p->a[index] += y_o * z_o* field/(d*d*d);
	    }
	}

	z_o = (zwf+0.5)*dz - zwf_c;
	
	for (int i=0; i< s_p->tfsf_hx_zmax_p->nx; i++)
	{
	    for (int j=0; j< s_p->tfsf_hx_zmax_p->ny; j++)
	    {
		int index = INDEX_XY_P(s_p->tfsf_hy_zmax_p, i, j);
				
		FP x_o = (i+1.+i_off)*dx - xwf_c;
		FP y_o = (j+1.5+j_off)*dy - ywf_c;
			
		FP d = sqrt(x_o*x_o + y_o*y_o + z_o*z_o);
		
		FP field = time_shape_dipole_wave(&s_p->waves.planewave_p[n],
						      (t-0.5)*s_p->delta_t,
						      d,
						      R);
		
		s_p->tfsf_hx_zmax_p->a[index] += y_o * field / (d*d);

		x_o = (i+1.5+i_off)*dx - xwf_c;
		y_o = (j+1.+j_off)*dy - ywf_c;
			
		d = sqrt(x_o*x_o + y_o*y_o + z_o*z_o);
		field = time_shape_dipole_wave(&s_p->waves.planewave_p[n],
					       (t-0.5)*s_p->delta_t,
					       d,
					       R);

		s_p->tfsf_hy_zmax_p->a[index] += -x_o * field / (d*d);
	    }
	}
    }
}

static void add_dipole_wave_ymin(sources_rt_data* s_p,
				 geometry_data* g_p,
				 int t,
				 int n)
{
    int i_off = s_p->x_off;
    int k_off = s_p->z_off;

    FP dx = g_p->delta_x;
    FP dy = g_p->delta_y;
    FP dz = g_p->delta_z;

    FP xwf_c = s_p->x_w_full/2*g_p->delta_x;
    FP ywf_c = s_p->y_w_full/2*g_p->delta_y;
    FP zwf_c = s_p->z_w_full/2*g_p->delta_z;
    
    FP R = sqrt(xwf_c*xwf_c + ywf_c*ywf_c + zwf_c*zwf_c);

    if (s_p->tfsf_ymin == true)
    {
	FP y_o = dy - ywf_c;
	for (int i=0; i< s_p->tfsf_ex_ymin_p->nx; i++)
	{
	    for (int k=0; k< s_p->tfsf_ex_ymin_p->ny; k++)
	    {
		int index = INDEX_XY_P(s_p->tfsf_ex_ymin_p, i, k);
				
		FP x_o = (i+1.5+i_off)*dx - xwf_c;
		FP z_o = (k+1+k_off)*dz - zwf_c;
		
		FP d = sqrt(x_o*x_o + y_o*y_o + z_o*z_o);
		
		FP field = time_shape_dipole_wave(&s_p->waves.planewave_p[n],
						      t*s_p->delta_t,
						      d,
						      R);

		s_p->tfsf_ex_ymin_p->a[index] += z_o * x_o * field/ (d*d*d);

		x_o = (i+1.+i_off)*dx - xwf_c;
		z_o = (k+1.5+k_off)*dz - zwf_c;
		
		d = sqrt(x_o*x_o + y_o*y_o + z_o*z_o);
		
		field = time_shape_dipole_wave(&s_p->waves.planewave_p[n],
					       t*s_p->delta_t,
					       d,
					       R);

		s_p->tfsf_ez_ymin_p->a[index] += -( y_o * y_o + x_o * x_o) * field / (d * d * d);
	    }
	}
	
	y_o = 0.5*dy - ywf_c;
	
	for (int i=0; i< s_p->tfsf_hx_ymin_p->nx; i++)
	{
	    for (int k=0; k< s_p->tfsf_hx_ymin_p->ny; k++)
	    {
		int index = INDEX_XY_P(s_p->tfsf_hx_ymin_p, i, k);
			
		FP x_o = (i+1.+i_off)*dx - xwf_c;
		FP z_o = (k+1.5+k_off)*dz - zwf_c;
		
		FP d = sqrt(x_o*x_o + y_o*y_o + z_o*z_o);
		
		FP field = time_shape_dipole_wave(&s_p->waves.planewave_p[n],
						      (t-0.5)*s_p->delta_t,
						      d,
						      R);
		
		s_p->tfsf_hx_ymin_p->a[index] += y_o * field/(d*d);
	    }
	}
    }
}

static void add_dipole_wave_ymax(sources_rt_data* s_p,
				 geometry_data* g_p,
				 int t,
				 int n)
{
    int i_off = s_p->x_off;
    int k_off = s_p->z_off;

    FP dx = g_p->delta_x;
    FP dy = g_p->delta_y;
    FP dz = g_p->delta_z;

    int ywf = s_p->y_w_full;
    
    FP xwf_c = s_p->x_w_full/2*g_p->delta_x;
    FP ywf_c = s_p->y_w_full/2*g_p->delta_y;
    FP zwf_c = s_p->z_w_full/2*g_p->delta_z;
    
    FP R = sqrt(xwf_c*xwf_c + ywf_c*ywf_c + zwf_c*zwf_c);
    
    if (s_p->tfsf_ymax == true)
    {
	FP y_o = (ywf+1)*dy - ywf_c;
	
	for (int i=0; i< s_p->tfsf_ex_ymax_p->nx; i++)
	{
	    for (int k=0; k< s_p->tfsf_ez_ymax_p->ny; k++)
	    {
		int index = INDEX_XY_P(s_p->tfsf_ex_ymax_p, i, k);

		FP x_o = (i+1.5+i_off)*dx - xwf_c;
		FP z_o = (k+1+k_off)*dz - zwf_c;

		FP d = sqrt(x_o*x_o + y_o*y_o + z_o*z_o);
		
		FP field = time_shape_dipole_wave(&s_p->waves.planewave_p[n],
						      t*s_p->delta_t,
						      d,
						      R);
		    
		s_p->tfsf_ex_ymax_p->a[index] += z_o * x_o * field / (d*d*d);
		
		x_o = (i+1.+i_off)*dx - xwf_c;
		z_o = (k+1.5+k_off)*dz - zwf_c;
		
		d = sqrt(x_o*x_o + y_o*y_o + z_o*z_o);
		
		field = time_shape_dipole_wave(&s_p->waves.planewave_p[n],
					       t*s_p->delta_t,
					       d,
					       R);
		
		s_p->tfsf_ez_ymax_p->a[index] += -( y_o * y_o + x_o * x_o) * field / (d * d * d);
	    }
	}

	y_o = (ywf+0.5)*dy - ywf_c;
	
	for (int i=0; i< s_p->tfsf_hx_ymax_p->nx; i++)
	{
	    for (int k=0; k< s_p->tfsf_hx_ymax_p->ny; k++)
	    {
		int index = INDEX_XY_P(s_p->tfsf_hx_ymax_p, i, k);

		FP x_o = (i+1.+i_off)*dx - xwf_c;
		FP z_o = (k+1.5+k_off)*dz - zwf_c;
		
		FP d = sqrt(x_o*x_o + y_o*y_o + z_o*z_o);
		
		FP field = time_shape_dipole_wave(&s_p->waves.planewave_p[n],
						      (t-0.5)*s_p->delta_t,
						      d,
						      R);
		
		s_p->tfsf_hx_ymax_p->a[index] += y_o * field/(d*d);
	    }
	}
    }
}

static void add_dipole_wave_xmin(sources_rt_data* s_p,
				 geometry_data* g_p,
				 int t,
				 int n)
{
    int j_off = s_p->y_off;
    int k_off = s_p->z_off;

    FP dx = g_p->delta_x;
    FP dy = g_p->delta_y;
    FP dz = g_p->delta_z;

    FP xwf_c = s_p->x_w_full/2*g_p->delta_x;
    FP ywf_c = s_p->y_w_full/2*g_p->delta_y;
    FP zwf_c = s_p->z_w_full/2*g_p->delta_z;
    
    FP R = sqrt(xwf_c*xwf_c + ywf_c*ywf_c + zwf_c*zwf_c);

    if (s_p->tfsf_xmin == true)
    {
	FP x_o = dx - xwf_c;
	
	for (int j=0; j< s_p->tfsf_ey_xmin_p->nx; j++)
	{
	    for (int k=0; k< s_p->tfsf_ey_xmin_p->ny; k++)
	    {
		int index = INDEX_XY_P(s_p->tfsf_ey_xmin_p, j, k);

		FP y_o = (j+1.5+j_off)*dy - ywf_c;
		FP z_o = (k+1+k_off)*dz - zwf_c;
		
		FP d = sqrt(x_o*x_o + y_o*y_o + z_o*z_o);
		
		FP field = time_shape_dipole_wave(&s_p->waves.planewave_p[n],
						      t*s_p->delta_t,
						      d,
						      R);
				
		s_p->tfsf_ey_xmin_p->a[index] += y_o * z_o* field/(d*d*d);
		
		y_o = (j+1.+j_off)*dy - ywf_c;
		z_o = (k+1.5+k_off)*dz - zwf_c;
		
		d = sqrt(x_o*x_o + y_o*y_o + z_o*z_o);
		
		field = time_shape_dipole_wave(&s_p->waves.planewave_p[n],
					       t*s_p->delta_t,
					       d,
					       R);
				
		s_p->tfsf_ez_xmin_p->a[index] += -( y_o * y_o + x_o * x_o) * field / (d * d * d);
	    }
	}
	
	x_o = 0.5*dx - xwf_c;
	
	for (int j=0; j< s_p->tfsf_hy_xmin_p->nx; j++)
	{
	    for (int k=0; k< s_p->tfsf_hy_xmin_p->ny; k++)
	    {
		int index = INDEX_XY_P(s_p->tfsf_hy_xmin_p, j, k);

		FP y_o = (j+1.+j_off)*dy - ywf_c;
		FP z_o = (k+1.5+k_off)*dz - zwf_c;
		
		FP d = sqrt(x_o*x_o + y_o*y_o + z_o*z_o);
		
		FP field = time_shape_dipole_wave(&s_p->waves.planewave_p[n],
						      (t-0.5)*s_p->delta_t,
						      d,
						      R);
		
		s_p->tfsf_hy_xmin_p->a[index] += -x_o * field / (d*d);
	    }
	}
    }
}

static void add_dipole_wave_xmax(sources_rt_data* s_p,
				 geometry_data* g_p,
				 int t,
				 int n)
{
    int j_off = s_p->y_off;
    int k_off = s_p->z_off;

    FP dx = g_p->delta_x;
    FP dy = g_p->delta_y;
    FP dz = g_p->delta_z;

    int xwf = s_p->x_w_full;

    FP xwf_c = s_p->x_w_full/2*g_p->delta_x;
    FP ywf_c = s_p->y_w_full/2*g_p->delta_y;
    FP zwf_c = s_p->z_w_full/2*g_p->delta_z;
    
    FP R = sqrt(xwf_c*xwf_c + ywf_c*ywf_c + zwf_c*zwf_c);

    if (s_p->tfsf_xmax == true)
    {
	FP x_o = (xwf+1)*dx - xwf_c;
	
	for (int j=0; j< s_p->tfsf_ey_xmax_p->nx; j++)
	{
	    for (int k=0; k< s_p->tfsf_ey_xmax_p->ny; k++)
	    {
		int index = INDEX_XY_P(s_p->tfsf_ey_xmax_p, j, k);

		FP y_o = (j+1.5+j_off)*dy - ywf_c;
		FP z_o = (k+1+k_off)*dz - zwf_c;
		
		FP d = sqrt(x_o*x_o + y_o*y_o + z_o*z_o);
		
		FP field = time_shape_dipole_wave(&s_p->waves.planewave_p[n],
						      t*s_p->delta_t,
						      d,
						      R);
		
		s_p->tfsf_ey_xmax_p->a[index] +=  y_o * z_o* field/(d*d*d);

		y_o = (j+1.+j_off)*dy - ywf_c;
		z_o = (k+1.5+k_off)*dz - zwf_c;
		
		d = sqrt(x_o*x_o + y_o*y_o + z_o*z_o);
		
		field = time_shape_dipole_wave(&s_p->waves.planewave_p[n],
					       t*s_p->delta_t,
					       d,
					       R);
		
		s_p->tfsf_ez_xmax_p->a[index] += -( y_o * y_o + x_o * x_o) * field / (d * d * d);
	    }
	}

	x_o = (xwf+0.5)*dx - xwf_c;
	
	for (int j=0; j< s_p->tfsf_hy_xmax_p->nx; j++)
	{
	    for (int k=0; k< s_p->tfsf_hy_xmax_p->ny; k++)
	    {
		int index = INDEX_XY_P(s_p->tfsf_hy_xmax_p, j, k);

		FP y_o = (j+1.+j_off)*dy - ywf_c;
		FP z_o = (k+1.5+k_off)*dz - zwf_c;
		
		FP d = sqrt(x_o*x_o + y_o*y_o + z_o*z_o);
		
		FP field = time_shape_dipole_wave(&s_p->waves.planewave_p[n],
						      (t-0.5)*s_p->delta_t,
						      d,
						      R);
		
		s_p->tfsf_hy_xmax_p->a[index] += -x_o * field / (d*d);
	    }
	}
    }
}

void zero_sources_rt(sources_rt_data* s_p)
{
    s_p->pulse_enable = false;
    s_p->tfsf_enable = false;
}

void update_sources_rt(sources_rt_data* s_p,
                       geometry_data* g_p,
                       pml_data* pml_p,
                       int t)
{
    if (s_p->pulse_enable == true)
    {
        if (s_p->pulse_xmin == true)
        {
            memset((void*)s_p->pulse_ey_xmin,0, (g_p->z_max)*sizeof(FP));
            memset((void*)s_p->pulse_hz_xmin,0, (g_p->z_max)*sizeof(FP));
        }

        if (s_p->pulse_xmax == true)
        {
            memset((void*)s_p->pulse_ey_xmax,0, (g_p->z_max)*sizeof(FP));
            memset((void*)s_p->pulse_hz_xmax,0, (g_p->z_max)*sizeof(FP));
        }

        if (s_p->pulse_zmin == true)
        {
            memset((void*)s_p->pulse_ey_zmin,0, (g_p->x_max)*sizeof(FP));
            memset((void*)s_p->pulse_hx_zmin,0, (g_p->x_max)*sizeof(FP));
        }

        if (s_p->pulse_zmax == true)
        {
            memset((void*)s_p->pulse_ey_zmax,0, (g_p->x_max)*sizeof(FP));
            memset((void*)s_p->pulse_hx_zmax,0, (g_p->x_max)*sizeof(FP));
        }

        int j;

        for (int k=0; k < s_p->pulses.num_pulses; k++)
        {
            if (s_p->pulses.pulse_p[k].plane == xmin_e)
            {
                if (s_p->pulse_xmin == true)
                {
#ifdef ONE_CONFIG_OPENMP
#pragma omp parallel for
#endif
                    for (j = 0; j < s_p->pulses.pulse_p[k].x_max; j++)
                    {
                        s_p->pulse_ey_xmin[j] +=
                            time_shape(&s_p->pulses.pulse_p[k],
                                       (t-0.5)*s_p->delta_t,
                                       (j+g_p->k_offset)*s_p->pulses.pulse_p[k].delta_x) *
                            s_p->pulses.pulse_p[k].space_shape_array[j];

                        s_p->pulse_hz_xmin[j] +=
                            time_shape(&s_p->pulses.pulse_p[k],
                                       t*s_p->delta_t,
                                       (j+g_p->k_offset)*s_p->pulses.pulse_p[k].delta_x) *
                            s_p->pulses.pulse_p[k].space_shape_array[j];
                    }
                }
            }

            if (s_p->pulses.pulse_p[k].plane == xmax_e)
            {
                if (s_p->pulse_xmax == true)
                {
#ifdef ONE_CONFIG_OPENMP
#pragma omp parallel for
#endif
                    for (j = 0; j < s_p->pulses.pulse_p[k].x_max; j++)
                    {
                        s_p->pulse_ey_xmax[j] +=
                            time_shape(&s_p->pulses.pulse_p[k],
                                       (t-0.5)*s_p->delta_t,
                                       (j+g_p->k_offset)*s_p->pulses.pulse_p[k].delta_x) *
                            s_p->pulses.pulse_p[k].space_shape_array[j];

                        s_p->pulse_hz_xmax[j] +=
                            -time_shape(&s_p->pulses.pulse_p[k],
                                        t*s_p->delta_t,
                                        (j+g_p->k_offset)*s_p->pulses.pulse_p[k].delta_x) *
                            s_p->pulses.pulse_p[k].space_shape_array[j];
                    }
                }
            }

            if (s_p->pulses.pulse_p[k].plane == zmin_e)
            {
                if (s_p->pulse_zmin == true)
                {
#ifdef ONE_CONFIG_OPENMP
#pragma omp parallel for
#endif
                    for (j = 0; j < s_p->pulses.pulse_p[k].x_max; j++)
                    {
                        s_p->pulse_ey_zmin[j] +=
                            time_shape(&s_p->pulses.pulse_p[k],
                                       (t-0.5)*s_p->delta_t,
                                       (j+g_p->i_offset)*s_p->pulses.pulse_p[k].delta_x) *
                            s_p->pulses.pulse_p[k].space_shape_array[j];

                        s_p->pulse_hx_zmin[j] +=
                            -time_shape(&s_p->pulses.pulse_p[k],
                                        t*s_p->delta_t,
                                        (j+g_p->i_offset)*s_p->pulses.pulse_p[k].delta_x) *
                            s_p->pulses.pulse_p[k].space_shape_array[j];
                    }
                }
            }

            if (s_p->pulses.pulse_p[k].plane == zmax_e)
            {
                if (s_p->pulse_zmax == true)
                {
#ifdef ONE_CONFIG_OPENMP
#pragma omp parallel for
#endif
                    for (j = 0; j < s_p->pulses.pulse_p[k].x_max; j++)
                    {
                        s_p->pulse_ey_zmax[j] +=
                            time_shape(&s_p->pulses.pulse_p[k],
                                       (t-0.5)*s_p->delta_t,
                                       (j+g_p->i_offset)*s_p->pulses.pulse_p[k].delta_x) *
                            s_p->pulses.pulse_p[k].space_shape_array[j];

                        s_p->pulse_hx_zmax[j] +=
                            time_shape(&s_p->pulses.pulse_p[k],
                                       t*s_p->delta_t,
                                       (j+g_p->i_offset)*s_p->pulses.pulse_p[k].delta_x) *
                            s_p->pulses.pulse_p[k].space_shape_array[j];
                    }
                }
            }
        }
    }

    if (s_p->tfsf_enable == true)
    {
        if (s_p->tfsf_xmin == true)
        {
            memset((void*)s_p->tfsf_ey_xmin,0, (s_p->ezmax)*sizeof(FP));
            memset((void*)s_p->tfsf_hz_xmin,0, (s_p->ezmax)*sizeof(FP));
        }

        if (s_p->tfsf_xmax == true)
        {
            memset((void*)s_p->tfsf_ey_xmax,0, (s_p->ezmax)*sizeof(FP));
            memset((void*)s_p->tfsf_hz_xmax,0, (s_p->ezmax)*sizeof(FP));
        }


        if(s_p->tfsf_zmin == true)
        {
            memset((void*)s_p->tfsf_ey_zmin,0, (s_p->exmax)*sizeof(FP));
            memset((void*)s_p->tfsf_hx_zmin,0, (s_p->exmax)*sizeof(FP));
        }

        if (s_p->tfsf_zmax == true)
        {
            memset((void*)s_p->tfsf_ey_zmax,0, (s_p->exmax)*sizeof(FP));
            memset((void*)s_p->tfsf_hx_zmax,0, (s_p->exmax)*sizeof(FP));
        }

        int i_off = s_p->x_off;
        int k_off = s_p->z_off;

        FP dx = g_p->delta_x;
        FP dz = g_p->delta_z;

        int xwf = s_p->x_w_full;
        int zwf = s_p->z_w_full;

        for (int n=0; n < s_p->waves.num_waves; n++)
        {

            FP ey = s_p->waves.planewave_p[n].ey;
            FP hx = s_p->waves.planewave_p[n].hx;
            FP hz = s_p->waves.planewave_p[n].hz;

            update_pw_field(s_p, n);

            if (s_p->tfsf_zmin == true)
            {
                for (int i=0; i< s_p->exmax; i++)
                {
                    FP d = calc_dist_3d(s_p,
                                            n,
                                            (i+1+i_off)*dx,
                                            0,
                                            dz);

                    FP field = interpolate_pw_field_e(s_p, n, t, d);
		    
                    s_p->tfsf_ey_zmin[i] += ey * field;
                }

                for (int i=0; i< s_p->exmax; i++)
                {
                    FP d = calc_dist_3d(s_p,
                                            n,
                                            (i+1+i_off)*dx,
                                            0,
                                            0.5*dz);

		    FP field = interpolate_pw_field_h(s_p, n, t, d);
		    
                    s_p->tfsf_hx_zmin[i] += hx * field;
                }
            }

            if (s_p->tfsf_zmax == true)
            {
                for (int i=0; i< s_p->exmax; i++)
                {
                    FP d = calc_dist_3d(s_p,
                                            n,
                                            (i+1+i_off)*dx,
                                            0,
                                            (zwf+1)*dz);
		    
                    FP field = interpolate_pw_field_e(s_p, n, t, d);
		    
                    s_p->tfsf_ey_zmax[i] += ey * field;
                }

                for (int i=0; i< s_p->exmax; i++)
                {
                    FP d = calc_dist_3d(s_p,
                                            n,
                                            (i+1+i_off)*dx,
                                            0,
                                            (zwf+0.5)*dz);
		    
                    FP field = interpolate_pw_field_h(s_p, n, t, d);
		    
                    s_p->tfsf_hx_zmax[i] += hx * field;
                }
            }

            if (s_p->tfsf_xmin == true)
            {
                for (int k=0; k< s_p->ezmax; k++)
                {
                    FP d = calc_dist_3d(s_p,
                                            n,
                                            dx,
                                            0,
                                            (k+1+k_off)*dz);
		    
                    FP field = interpolate_pw_field_e(s_p, n, t, d);
		    
                    s_p->tfsf_ey_xmin[k] += ey * field;
                }

                for (int k=0; k< s_p->ezmax; k++)
                {
                    FP d = calc_dist_3d(s_p,
                                            n,
                                            0.5*dx,
                                            0,
                                            (k+1+k_off)*dz);
		    
                    FP field = interpolate_pw_field_h(s_p, n, t, d);
		    
                    s_p->tfsf_hz_xmin[k] += hz * field;
                }
            }

            if (s_p->tfsf_xmax == true)
            {
                for (int k=0; k< s_p->ezmax; k++)
                {
                    FP d = calc_dist_3d(s_p,
                                            n,
                                            (xwf+1)*dx,
                                            0,
                                            (k+1+k_off)*dz);

                    FP field = interpolate_pw_field_e(s_p, n, t, d);
		    
                    s_p->tfsf_ey_xmax[k] += ey * field;
                }
                for (int k=0; k< s_p->ezmax; k++)
                {
                    FP d = calc_dist_3d(s_p,
                                            n,
                                            (xwf+0.5)*dx,
                                            0,
                                            (k+1+k_off)*dz);

		    FP field = interpolate_pw_field_h(s_p, n, t, d);
		    
                    s_p->tfsf_hz_xmax[k] += hz * field;
                }
            }
        }
    }
}

static FP calculate_envelope(envelope_data* env_p, FP t)
{
    return env_p->f_p(env_p, t);
}

static FP tanh_env(envelope_data* env_p, FP t)
{
    return 0.25*(1. - tanh(-env_p->alpha*(t - env_p->t0))) *
	(1. - tanh(env_p->alpha*(t - 3*env_p->t0 - env_p->tp)));
}

static FP gauss_env(envelope_data* env_p, FP t)
{
    FP temp = 3.3333/env_p->tp;
    return exp(-2.*M_LN2*t*t*temp*temp);
}

