#include "fdtd.h"

void add_sources_2d_fx(vfield* vf_p,
                       geometry_data* geometry_p,
                       sources_rt_data* s_p,
                       scheme_constants* sch_p)
{
    field* fx_p = vf_p->comp_p[x_e];

    if (s_p->pulse_enable == true)
    {
        if (s_p->pulse_zmin == true)
        {
            for (int i=geometry_p->imin-1; i< geometry_p->imax+1; i++)
            {
                fx_p->a[INDEX2D_P(fx_p,i,s_p->pulse_z_min)] +=
                    s_p->pulse_hx_zmin[i]*sch_p->dt_4_pi;
            }
        }

        if (s_p->pulse_zmax == true)
        {
            for (int i=geometry_p->imin-1; i< geometry_p->imax+1; i++)
            {
                fx_p->a[INDEX2D_P(fx_p,i,s_p->pulse_z_max)] +=
                    s_p->pulse_hx_zmax[i]*sch_p->dt_4_pi;
            }
        }
    }

    if (s_p->tfsf_enable == true)
    {
        if (s_p->tfsf_zmin == true)
        {
            for (int i=0; i < s_p->exmax; i++)
            {
                fx_p->a[INDEX2D_P(fx_p, i+s_p->x_min, s_p->z_min-1)] +=
                    sch_p->dt_dz*s_p->tfsf_ey_zmin[i];
            }
        }

        if (s_p->tfsf_zmax == true)
        {
            for (int i=0; i < s_p->exmax; i++)
            {
                fx_p->a[INDEX2D_P(fx_p, i+s_p->x_min, s_p->z_max-1)] +=
                    -sch_p->dt_dz*s_p->tfsf_ey_zmax[i];
            }
        }
    }
}

void add_sources_2d_fy(vfield* vf_p,
                       geometry_data* geometry_p,
                       sources_rt_data* s_p,
                       scheme_constants* sch_p)
{
    field* fy_p = vf_p->comp_p[y_e];

    if (s_p->pulse_enable == true)
    {
        if (s_p->pulse_xmin == true)
        {
            for (int k=geometry_p->kmin-1; k< geometry_p->kmax+1; k++)
            {
                fy_p->a[INDEX2D_P(fy_p,s_p->pulse_x_min+1,k)] +=
                    s_p->pulse_ey_xmin[k]*sch_p->dt_4_pi;
            }
        }

        if (s_p->pulse_xmax == true)
        {
            for (int k=geometry_p->kmin-1; k< geometry_p->kmax+1; k++)
            {
                fy_p->a[INDEX2D_P(fy_p,s_p->pulse_x_max,k)] +=
                    s_p->pulse_ey_xmax[k]*sch_p->dt_4_pi;
            }
        }

        if (s_p->pulse_zmin == true)
        {
            for (int i=geometry_p->imin-1; i< geometry_p->imax+1; i++)
            {
                fy_p->a[INDEX2D_P(fy_p,i,s_p->pulse_z_min+1)] +=
                    s_p->pulse_ey_zmin[i]*sch_p->dt_4_pi;
            }
        }

        if (s_p->pulse_zmax == true)
        {
            for (int i=geometry_p->imin-1; i< geometry_p->imax+1; i++)
            {
                fy_p->a[INDEX2D_P(fy_p,i,s_p->pulse_z_max)] +=
                    s_p->pulse_ey_zmax[i]*sch_p->dt_4_pi;
            }
        }
    }

    if (s_p->tfsf_enable == true)
    {
        if (s_p->tfsf_zmin == true)
        {
            for (int i=0; i < s_p->exmax; i++)
            {
                fy_p->a[INDEX2D_P(fy_p, i+s_p->x_min, s_p->z_min)] +=
                    -sch_p->dt_dz*s_p->tfsf_hx_zmin[i]; /* - */
            }
        }

        if (s_p->tfsf_zmax == true)
        {
            for (int i=0; i < s_p->exmax; i++)
            {
                fy_p->a[INDEX2D_P(fy_p, i+s_p->x_min, s_p->z_max)] +=
                    sch_p->dt_dz*s_p->tfsf_hx_zmax[i];
            }
        }

        if (s_p->tfsf_xmin == true)
        {
            for (int k=0; k < s_p->ezmax; k++)
            {
                fy_p->a[INDEX2D_P(fy_p, s_p->x_min, k+s_p->z_min)] +=
                    sch_p->dt_dx*s_p->tfsf_hz_xmin[k]; /* + */
            }
        }

        if (s_p->tfsf_xmax == true)
        {
            for (int k=0; k < s_p->ezmax; k++)
            {
                fy_p->a[INDEX2D_P(fy_p, s_p->x_max, k+s_p->z_min)] +=
                    -sch_p->dt_dx*s_p->tfsf_hz_xmax[k];
            }
        }
    }
}

void add_sources_2d_fz(vfield* vf_p,
                       geometry_data* geometry_p,
                       sources_rt_data* s_p,
                       scheme_constants* sch_p)
{
    field* fz_p = vf_p->comp_p[z_e];

    if (s_p->pulse_enable == true)
    {
        if (s_p->pulse_xmin == true)
        {
            for (int k=geometry_p->kmin-1; k< geometry_p->kmax+1; k++)
            {
                fz_p->a[INDEX2D_P(fz_p,s_p->pulse_x_min,k)] +=
                    s_p->pulse_hz_xmin[k]*sch_p->dt_4_pi;
            }
        }

        if (s_p->pulse_xmax == true)
        {
            for (int k=geometry_p->kmin-1; k< geometry_p->kmax+1; k++)
            {
                fz_p->a[INDEX2D_P(fz_p,s_p->pulse_x_max,k)] +=
                    s_p->pulse_hz_xmax[k]*sch_p->dt_4_pi;
            }
        }
    }

    if (s_p->tfsf_enable == true)
    {
        if (s_p->tfsf_xmin == true)
        {
            for (int k=0; k < s_p->ezmax; k++)
            {
                fz_p->a[INDEX2D_P(fz_p, s_p->x_min-1, k+s_p->z_min)] +=
                    -sch_p->dt_dx*s_p->tfsf_ey_xmin[k];
            }
        }

        if (s_p->tfsf_xmax == true)
        {
            for (int k=0; k < s_p->ezmax; k++)
            {
                fz_p->a[INDEX2D_P(fz_p, s_p->x_max-1, k+s_p->z_min)] +=
                    sch_p->dt_dx*s_p->tfsf_ey_xmax[k];
            }
        }
    }
}

#ifndef CUDA
void update_field_2d_te_fy_new(vfield* vfy_p,
                               vfield* vfxz_p,
                               vfield* vj_p,
                               geometry_data* geometry_p,
                               decompose_data* decomp_p,
                               pml_data* pml_p,
                               scheme_constants* sch_p)
{
    int i=0, ey=0, dx=0, dz=0;
    field* fx_p = vfxz_p->comp_p[x_e];
    field* fy_p = vfy_p->comp_p[y_e];
    field* fz_p = vfxz_p->comp_p[z_e];
    field* fjy_p = vj_p->comp_p[y_e];

#ifdef ONE_CONFIG_OPENMP
#pragma omp parallel for private(ey,dx,dz)
#endif
    for (i=0; i<pml_p->nef; i++)
    {
        ey = pml_p->idxef[i];
        dx = ey - DX2(fz_p->nx, fz_p->nz);
        dz = ey - DZ2(fx_p->nx, fx_p->nz);

        fy_p->a[ey] +=
            sch_p->dt_dz * (fx_p->a[ey] - fx_p->a[dz]) -
            sch_p->dt_dx * (fz_p->a[ey] - fz_p->a[dx]) -
            fjy_p->a[ey] * sch_p->dt_4_pi;
    } /* main part */

#ifdef ONE_CONFIG_OPENMP
#pragma omp parallel for private(ey,dx,dz)
#endif
    for (i=0; i<pml_p->ne; i++)
    {
        ey = pml_p->idxe[i];
        dx = ey - DX2(fz_p->nx, fz_p->nz);
        dz = ey - DZ2(fx_p->nx, fx_p->nz);

        pml_p->Eyx[i] = pml_p->aex[i]*pml_p->Eyx[i] + pml_p->bex[i]*(fz_p->a[ey] - fz_p->a[dx]);
        pml_p->Eyz[i] = pml_p->aez[i]*pml_p->Eyz[i] + pml_p->bez[i]*(fx_p->a[ey] - fx_p->a[dz]);

        fy_p->a[ey] = pml_p->Eyx[i] + pml_p->Eyz[i];
    }
}


void update_field_2d_te_fy(vfield* vfy_p,
                           vfield* vfxz_p,
                           vfield* vj_p,
                           geometry_data* geometry_p,
                           decompose_data* decomp_p,
                           pml_data* pml_p,
                           scheme_constants* sch_p)
{
    int i=0,k=0, i0=0, k0=0, i1=0, k1=0, pmli=0, pmlk=0, ey=0;
    field* fx_p = vfxz_p->comp_p[x_e];
    field* fy_p = vfy_p->comp_p[y_e];
    field* fz_p = vfxz_p->comp_p[z_e];
    field* fjy_p = vj_p->comp_p[y_e];

#ifdef ONE_CONFIG_OPENMP
#pragma omp parallel for private(k,i1,k1,i0,k0,pmli,pmlk,ey)
#endif
    for (i=decomp_p->fy_imin; i<decomp_p->fy_imax; i++)
    {
        i1 = i+geometry_p->i_offset;
        for (k=decomp_p->fy_kmin; k<decomp_p->fy_kmax; k++)
        {
            k1 = k+geometry_p->k_offset;
            if (k1 >= pml_p->ez_l && k1 <= pml_p->ez_h &&
                i1 >= pml_p->ex_l && i1 <= pml_p->ex_h)
            {
                ey = INDEX2D_P(fy_p,i,k);
                fy_p->a[ey] +=
                    sch_p->dt_dz*(fx_p->a[INDEX2D_P(fx_p,i,k)]-fx_p->a[INDEX2D_P(fx_p,i,k-1)]) -
                    sch_p->dt_dx*(fz_p->a[INDEX2D_P(fz_p,i,k)]-fz_p->a[INDEX2D_P(fz_p,i-1,k)]) -
                    fjy_p->a[ey]*sch_p->dt_4_pi;
            } /* main part */
            else
            {
                if (k1 >= pml_p->ez_l && k1 <= pml_p->ez_h)
                {
                    /* 7 & 8 */
                    if (i1 > pml_p->ex_h)
                    {
                        i0 = i1 - pml_p->eoffx_h;
                        pmli = pml_p->epmlx_h - i1;

                        pml_p->pml_f.Eyx_x_h->a[INDEX2D_P(pml_p->pml_f.Eyx_x_h,i0,k)] =
                            pml_p->c_hx[pmli]*pml_p->pml_f.Eyx_x_h->a[INDEX2D_P(pml_p->pml_f.Eyx_x_h,i0,k)]+
                            pml_p->d_hx[pmli]*(fz_p->a[INDEX2D_P(fz_p,i,k)]-fz_p->a[INDEX2D_P(fz_p,i-1,k)]);
                        pml_p->pml_f.Eyz_x_h->a[INDEX2D_P(pml_p->pml_f.Eyz_x_h,i0,k)] +=
                            sch_p->dt_dz*(fx_p->a[INDEX2D_P(fx_p,i,k)]-fx_p->a[INDEX2D_P(fx_p,i,k-1)]);
                        fy_p->a[INDEX2D_P(fy_p,i,k)] = pml_p->pml_f.Eyx_x_h->a[INDEX2D_P(pml_p->pml_f.Eyx_x_h,i0,k)] +
                                                       pml_p->pml_f.Eyz_x_h->a[INDEX2D_P(pml_p->pml_f.Eyz_x_h,i0,k)];
                    }
                    else
                    {
                        i0 = i - pml_p->eoffx_l;
                        pmli = i1 - pml_p->epmlx_l;
                        pml_p->pml_f.Eyx_x_l->a[INDEX2D_P(pml_p->pml_f.Eyx_x_l,i0,k)] =
                            pml_p->c_ex[pmli]*pml_p->pml_f.Eyx_x_l->a[INDEX2D_P(pml_p->pml_f.Eyx_x_l,i0,k)]+
                            pml_p->d_ex[pmli]*(fz_p->a[INDEX2D_P(fz_p,i,k)]-fz_p->a[INDEX2D_P(fz_p,i-1,k)]);
                        pml_p->pml_f.Eyz_x_l->a[INDEX2D_P(pml_p->pml_f.Eyz_x_l,i0,k)] +=
                            sch_p->dt_dz*(fx_p->a[INDEX2D_P(fx_p,i,k)]-fx_p->a[INDEX2D_P(fx_p,i,k-1)]);
                        fy_p->a[INDEX2D_P(fy_p,i,k)] = pml_p->pml_f.Eyx_x_l->a[INDEX2D_P(pml_p->pml_f.Eyx_x_l,i0,k)] +
                                                       pml_p->pml_f.Eyz_x_l->a[INDEX2D_P(pml_p->pml_f.Eyz_x_l,i0,k)];
                    }
                }
                else if(k1 < pml_p->ez_l)
                {
                    k0 = k - pml_p->eoffz_l;
                    pmlk = k1 - pml_p->epmlz_l;

                    if (i1 >= pml_p->ex_l && i1 <= pml_p->ex_h)
                    {
                        /*3*/
                        pml_p->pml_f.Eyx_z_l->a[INDEX2D_P(pml_p->pml_f.Eyx_z_l,i,k0)] +=
                            -sch_p->dt_dx*(fz_p->a[INDEX2D_P(fz_p,i,k)]-fz_p->a[INDEX2D_P(fz_p,i-1,k)]);
                        pml_p->pml_f.Eyz_z_l->a[INDEX2D_P(pml_p->pml_f.Eyz_z_l,i,k0)] =
                            pml_p->c_ez[pmlk]*pml_p->pml_f.Eyz_z_l->a[INDEX2D_P(pml_p->pml_f.Eyz_z_l,i,k0)] -
                            pml_p->d_ez[pmlk]*(fx_p->a[INDEX2D_P(fx_p,i,k)]-fx_p->a[INDEX2D_P(fx_p,i,k-1)]);
                        fy_p->a[INDEX2D_P(fy_p,i,k)] = pml_p->pml_f.Eyx_z_l->a[INDEX2D_P(pml_p->pml_f.Eyx_z_l,i,k0)] +
                                                       pml_p->pml_f.Eyz_z_l->a[INDEX2D_P(pml_p->pml_f.Eyz_z_l,i,k0)];
                    }
                    else if (i1 > pml_p->ex_h)
                    {
                        /*2*/
                        i0 = i1 - pml_p->eoffx_h;
                        pmli = pml_p->epmlx_h - i1;

                        pml_p->pml_f.Eyx_x_h->a[INDEX2D_P(pml_p->pml_f.Eyx_x_h,i0,k)] =
                            pml_p->c_hx[pmli]*pml_p->pml_f.Eyx_x_h->a[INDEX2D_P(pml_p->pml_f.Eyx_x_h,i0,k)]+
                            pml_p->d_hx[pmli]*(fz_p->a[INDEX2D_P(fz_p,i,k)]-fz_p->a[INDEX2D_P(fz_p,i-1,k)]);
                        pml_p->pml_f.Eyz_z_l->a[INDEX2D_P(pml_p->pml_f.Eyz_z_l,i,k0)] =
                            pml_p->c_ez[pmlk]*pml_p->pml_f.Eyz_z_l->a[INDEX2D_P(pml_p->pml_f.Eyz_z_l,i,k0)]-
                            pml_p->d_ez[pmlk]*(fx_p->a[INDEX2D_P(fx_p,i,k)]-fx_p->a[INDEX2D_P(fx_p,i,k-1)]);
                        fy_p->a[INDEX2D_P(fy_p,i,k)] = pml_p->pml_f.Eyx_x_h->a[INDEX2D_P(pml_p->pml_f.Eyx_x_h,i0,k)] +
                                                       pml_p->pml_f.Eyz_z_l->a[INDEX2D_P(pml_p->pml_f.Eyz_z_l,i,k0)];
                    }
                    else
                    {
                        i0 = i - pml_p->eoffx_l;
                        pmli = i1 - pml_p->epmlx_l;

                        pml_p->pml_f.Eyx_x_l->a[INDEX2D_P(pml_p->pml_f.Eyx_x_l,i0,k)] =
                            pml_p->c_ex[pmli]*pml_p->pml_f.Eyx_x_l->a[INDEX2D_P(pml_p->pml_f.Eyx_x_l,i0,k)] +
                            pml_p->d_ex[pmli]*(fz_p->a[INDEX2D_P(fz_p,i,k)]-fz_p->a[INDEX2D_P(fz_p,i-1,k)]);
                        pml_p->pml_f.Eyz_z_l->a[INDEX2D_P(pml_p->pml_f.Eyz_z_l,i,k0)] =
                            pml_p->c_ez[k1]*pml_p->pml_f.Eyz_z_l->a[INDEX2D_P(pml_p->pml_f.Eyz_z_l,i,k0)]-
                            pml_p->d_ez[k1]*(fx_p->a[INDEX2D_P(fx_p,i,k)]-fx_p->a[INDEX2D_P(fx_p,i,k-1)]);
                        fy_p->a[INDEX2D_P(fy_p,i,k)] = pml_p->pml_f.Eyx_x_l->a[INDEX2D_P(pml_p->pml_f.Eyx_x_l,i0,k)] +
                                                       pml_p->pml_f.Eyz_z_l->a[INDEX2D_P(pml_p->pml_f.Eyz_z_l,i,k0)];
                    }
                }
                else
                {
                    k0 = k1 - pml_p->eoffz_h;
                    pmlk = pml_p->epmlz_h - k1;

                    if (i1 >= pml_p->ex_l && i1 <= pml_p->ex_h)
                    {
                        /* 6*/
                        pml_p->pml_f.Eyx_z_h->a[INDEX2D_P(pml_p->pml_f.Eyx_z_h,i,k0)] +=
                            -sch_p->dt_dx*(fz_p->a[INDEX2D_P(fz_p,i,k)]-fz_p->a[INDEX2D_P(fz_p,i-1,k)]);
                        pml_p->pml_f.Eyz_z_h->a[INDEX2D_P(pml_p->pml_f.Eyz_z_h,i,k0)] =
                            pml_p->c_hz[pmlk]*pml_p->pml_f.Eyz_z_h->a[INDEX2D_P(pml_p->pml_f.Eyz_z_h,i,k0)] -
                            pml_p->d_hz[pmlk]*(fx_p->a[INDEX2D_P(fx_p,i,k)]-fx_p->a[INDEX2D_P(fx_p,i,k-1)]);
                        fy_p->a[INDEX2D_P(fy_p,i,k)] = pml_p->pml_f.Eyx_z_h->a[INDEX2D_P(pml_p->pml_f.Eyx_z_h,i,k0)] +
                                                       pml_p->pml_f.Eyz_z_h->a[INDEX2D_P(pml_p->pml_f.Eyz_z_h,i,k0)];
                    }
                    else if (i1 > pml_p->ex_h)
                    {
                        /*5*/
                        i0 = i1 - pml_p->eoffx_h;
                        pmli = pml_p->epmlx_h - i1;

                        pml_p->pml_f.Eyx_x_h->a[INDEX2D_P(pml_p->pml_f.Eyx_x_h,i0,k)] =
                            pml_p->c_hx[pmli]*pml_p->pml_f.Eyx_x_h->a[INDEX2D_P(pml_p->pml_f.Eyx_x_h,i0,k)] +
                            pml_p->d_hx[pmli]*(fz_p->a[INDEX2D_P(fz_p,i,k)]-fz_p->a[INDEX2D_P(fz_p,i-1,k)]);
                        pml_p->pml_f.Eyz_z_h->a[INDEX2D_P(pml_p->pml_f.Eyz_z_h,i,k0)] =
                            pml_p->c_hz[pmlk]*pml_p->pml_f.Eyz_z_h->a[INDEX2D_P(pml_p->pml_f.Eyz_z_h,i,k0)]-
                            pml_p->d_hz[pmlk]*(fx_p->a[INDEX2D_P(fx_p,i,k)]-fx_p->a[INDEX2D_P(fx_p,i,k-1)]);
                        fy_p->a[INDEX2D_P(fy_p,i,k)] = pml_p->pml_f.Eyx_x_h->a[INDEX2D_P(pml_p->pml_f.Eyx_x_h,i0,k)] +
                                                       pml_p->pml_f.Eyz_z_h->a[INDEX2D_P(pml_p->pml_f.Eyz_z_h,i,k0)];

                    }
                    else
                    {   /*6*/
                        i0 = i - pml_p->eoffx_l;
                        pmli = i1 - pml_p->epmlx_l;

                        pml_p->pml_f.Eyx_x_l->a[INDEX2D_P(pml_p->pml_f.Eyx_x_l,i0,k)] =
                            pml_p->c_ex[pmli]*pml_p->pml_f.Eyx_x_l->a[INDEX2D_P(pml_p->pml_f.Eyx_x_l,i0,k)]+
                            pml_p->d_ex[pmli]*(fz_p->a[INDEX2D_P(fz_p,i,k)]-fz_p->a[INDEX2D_P(fz_p,i-1,k)]);
                        pml_p->pml_f.Eyz_z_h->a[INDEX2D_P(pml_p->pml_f.Eyz_z_h,i,k0)] =
                            pml_p->c_hz[pmlk]*pml_p->pml_f.Eyz_z_h->a[INDEX2D_P(pml_p->pml_f.Eyz_z_h,i,k0)]-
                            pml_p->d_hz[pmlk]*(fx_p->a[INDEX2D_P(fx_p,i,k)]-fx_p->a[INDEX2D_P(fx_p,i,k-1)]);
                        fy_p->a[INDEX2D_P(fy_p,i,k)] = pml_p->pml_f.Eyx_x_l->a[INDEX2D_P(pml_p->pml_f.Eyx_x_l,i0,k)] +
                                                       pml_p->pml_f.Eyz_z_h->a[INDEX2D_P(pml_p->pml_f.Eyz_z_h,i,k0)];
                    }
                }
            }/* main part */
        }/* i */
    }/* k */
}

void update_field_2d_tm_fy(vfield* vfy_p,
                           vfield* vfxz_p,
                           geometry_data* geometry_p,
                           decompose_data* decomp_p,
                           pml_data* pml_p,
                           scheme_constants* sch_p)
{
    int i=0,k=0, i0=0, k0=0, i1=0, k1=0, pmli=0, pmlk=0, ey=0;
    field* fx_p = vfxz_p->comp_p[x_e];
    field* fy_p = vfy_p->comp_p[y_e];
    field* fz_p = vfxz_p->comp_p[z_e];

#ifdef ONE_CONFIG_OPENMP
#pragma omp parallel for private(k,i1,k1,i0,k0,pmli,pmlk,ey)
#endif
    for (i=decomp_p->fy_imin; i<decomp_p->fy_imax; i++)
    {
        i1 = i+geometry_p->i_offset;
        for (k=decomp_p->fy_kmin; k<decomp_p->fy_kmax; k++)
        {
            k1 = k+geometry_p->k_offset;
            if (k1 >= pml_p->ez_l && k1 <= pml_p->ez_h &&
                    i1 >= pml_p->ex_l && i1 <= pml_p->ex_h)
            {
                ey = INDEX2D_P(fy_p,i,k);
                fy_p->a[ey] +=
                    sch_p->dt_dz*(fx_p->a[INDEX2D_P(fx_p,i,k)]-fx_p->a[INDEX2D_P(fx_p,i,k-1)]) -
                    sch_p->dt_dx*(fz_p->a[INDEX2D_P(fz_p,i,k)]-fz_p->a[INDEX2D_P(fz_p,i-1,k)]);
            } /* main part */
            else
            {
                if (k1 >= pml_p->ez_l && k1 <= pml_p->ez_h)
                {
                    /* 7 & 8 */
                    if (i1 > pml_p->ex_h)
                    {
                        i0 = i1 - pml_p->eoffx_h;
                        pmli = pml_p->epmlx_h - i1;

                        pml_p->pml_f.Eyx_x_h->a[INDEX2D_P(pml_p->pml_f.Eyx_x_h,i0,k)] =
                            pml_p->c_hx[pmli]*pml_p->pml_f.Eyx_x_h->a[INDEX2D_P(pml_p->pml_f.Eyx_x_h,i0,k)]+
                            pml_p->d_hx[pmli]*(fz_p->a[INDEX2D_P(fz_p,i,k)]-fz_p->a[INDEX2D_P(fz_p,i-1,k)]);
                        pml_p->pml_f.Eyz_x_h->a[INDEX2D_P(pml_p->pml_f.Eyz_x_h,i0,k)] +=
                            sch_p->dt_dz*(fx_p->a[INDEX2D_P(fx_p,i,k)]-fx_p->a[INDEX2D_P(fx_p,i,k-1)]);
                        fy_p->a[INDEX2D_P(fy_p,i,k)] = pml_p->pml_f.Eyx_x_h->a[INDEX2D_P(pml_p->pml_f.Eyx_x_h,i0,k)] +
                                                       pml_p->pml_f.Eyz_x_h->a[INDEX2D_P(pml_p->pml_f.Eyz_x_h,i0,k)];
                    }
                    else
                    {
                        i0 = i - pml_p->eoffx_l;
                        pmli = i1 - pml_p->epmlx_l;
                        pml_p->pml_f.Eyx_x_l->a[INDEX2D_P(pml_p->pml_f.Eyx_x_l,i0,k)] =
                            pml_p->c_ex[pmli]*pml_p->pml_f.Eyx_x_l->a[INDEX2D_P(pml_p->pml_f.Eyx_x_l,i0,k)]+
                            pml_p->d_ex[pmli]*(fz_p->a[INDEX2D_P(fz_p,i,k)]-fz_p->a[INDEX2D_P(fz_p,i-1,k)]);
                        pml_p->pml_f.Eyz_x_l->a[INDEX2D_P(pml_p->pml_f.Eyz_x_l,i0,k)] +=
                            sch_p->dt_dz*(fx_p->a[INDEX2D_P(fx_p,i,k)]-fx_p->a[INDEX2D_P(fx_p,i,k-1)]);
                        fy_p->a[INDEX2D_P(fy_p,i,k)] = pml_p->pml_f.Eyx_x_l->a[INDEX2D_P(pml_p->pml_f.Eyx_x_l,i0,k)] +
                                                       pml_p->pml_f.Eyz_x_l->a[INDEX2D_P(pml_p->pml_f.Eyz_x_l,i0,k)];
                    }
                }
                else if(k1 < pml_p->ez_l)
                {
                    k0 = k - pml_p->eoffz_l;
                    pmlk = k1 - pml_p->epmlz_l;

                    if (i1 >= pml_p->ex_l && i1 <= pml_p->ex_h)
                    {
                        /*3*/
                        pml_p->pml_f.Eyx_z_l->a[INDEX2D_P(pml_p->pml_f.Eyx_z_l,i,k0)] +=
                            -sch_p->dt_dx*(fz_p->a[INDEX2D_P(fz_p,i,k)]-fz_p->a[INDEX2D_P(fz_p,i-1,k)]);
                        pml_p->pml_f.Eyz_z_l->a[INDEX2D_P(pml_p->pml_f.Eyz_z_l,i,k0)] =
                            pml_p->c_ez[pmlk]*pml_p->pml_f.Eyz_z_l->a[INDEX2D_P(pml_p->pml_f.Eyz_z_l,i,k0)] -
                            pml_p->d_ez[pmlk]*(fx_p->a[INDEX2D_P(fx_p,i,k)]-fx_p->a[INDEX2D_P(fx_p,i,k-1)]);
                        fy_p->a[INDEX2D_P(fy_p,i,k)] = pml_p->pml_f.Eyx_z_l->a[INDEX2D_P(pml_p->pml_f.Eyx_z_l,i,k0)] +
                                                       pml_p->pml_f.Eyz_z_l->a[INDEX2D_P(pml_p->pml_f.Eyz_z_l,i,k0)];
                    }
                    else if (i1 > pml_p->ex_h)
                    {
                        /*2*/
                        i0 = i1 - pml_p->eoffx_h;
                        pmli = pml_p->epmlx_h - i1;

                        pml_p->pml_f.Eyx_x_h->a[INDEX2D_P(pml_p->pml_f.Eyx_x_h,i0,k)] =
                            pml_p->c_hx[pmli]*pml_p->pml_f.Eyx_x_h->a[INDEX2D_P(pml_p->pml_f.Eyx_x_h,i0,k)]+
                            pml_p->d_hx[pmli]*(fz_p->a[INDEX2D_P(fz_p,i,k)]-fz_p->a[INDEX2D_P(fz_p,i-1,k)]);
                        pml_p->pml_f.Eyz_z_l->a[INDEX2D_P(pml_p->pml_f.Eyz_z_l,i,k0)] =
                            pml_p->c_ez[pmlk]*pml_p->pml_f.Eyz_z_l->a[INDEX2D_P(pml_p->pml_f.Eyz_z_l,i,k0)]-
                            pml_p->d_ez[pmlk]*(fx_p->a[INDEX2D_P(fx_p,i,k)]-fx_p->a[INDEX2D_P(fx_p,i,k-1)]);
                        fy_p->a[INDEX2D_P(fy_p,i,k)] = pml_p->pml_f.Eyx_x_h->a[INDEX2D_P(pml_p->pml_f.Eyx_x_h,i0,k)] +
                                                       pml_p->pml_f.Eyz_z_l->a[INDEX2D_P(pml_p->pml_f.Eyz_z_l,i,k0)];
                    }
                    else
                    {
                        i0 = i - pml_p->eoffx_l;
                        pmli = i1 - pml_p->epmlx_l;

                        pml_p->pml_f.Eyx_x_l->a[INDEX2D_P(pml_p->pml_f.Eyx_x_l,i0,k)] =
                            pml_p->c_ex[pmli]*pml_p->pml_f.Eyx_x_l->a[INDEX2D_P(pml_p->pml_f.Eyx_x_l,i0,k)] +
                            pml_p->d_ex[pmli]*(fz_p->a[INDEX2D_P(fz_p,i,k)]-fz_p->a[INDEX2D_P(fz_p,i-1,k)]);
                        pml_p->pml_f.Eyz_z_l->a[INDEX2D_P(pml_p->pml_f.Eyz_z_l,i,k0)] =
                            pml_p->c_ez[k1]*pml_p->pml_f.Eyz_z_l->a[INDEX2D_P(pml_p->pml_f.Eyz_z_l,i,k0)]-
                            pml_p->d_ez[k1]*(fx_p->a[INDEX2D_P(fx_p,i,k)]-fx_p->a[INDEX2D_P(fx_p,i,k-1)]);
                        fy_p->a[INDEX2D_P(fy_p,i,k)] = pml_p->pml_f.Eyx_x_l->a[INDEX2D_P(pml_p->pml_f.Eyx_x_l,i0,k)] +
                                                       pml_p->pml_f.Eyz_z_l->a[INDEX2D_P(pml_p->pml_f.Eyz_z_l,i,k0)];
                    }
                }
                else
                {
                    k0 = k1 - pml_p->eoffz_h;
                    pmlk = pml_p->epmlz_h - k1;

                    if (i1 >= pml_p->ex_l && i1 <= pml_p->ex_h)
                    {
                        /* 6*/
                        pml_p->pml_f.Eyx_z_h->a[INDEX2D_P(pml_p->pml_f.Eyx_z_h,i,k0)] +=
                            -sch_p->dt_dx*(fz_p->a[INDEX2D_P(fz_p,i,k)]-fz_p->a[INDEX2D_P(fz_p,i-1,k)]);
                        pml_p->pml_f.Eyz_z_h->a[INDEX2D_P(pml_p->pml_f.Eyz_z_h,i,k0)] =
                            pml_p->c_hz[pmlk]*pml_p->pml_f.Eyz_z_h->a[INDEX2D_P(pml_p->pml_f.Eyz_z_h,i,k0)] -
                            pml_p->d_hz[pmlk]*(fx_p->a[INDEX2D_P(fx_p,i,k)]-fx_p->a[INDEX2D_P(fx_p,i,k-1)]);
                        fy_p->a[INDEX2D_P(fy_p,i,k)] = pml_p->pml_f.Eyx_z_h->a[INDEX2D_P(pml_p->pml_f.Eyx_z_h,i,k0)] +
                                                       pml_p->pml_f.Eyz_z_h->a[INDEX2D_P(pml_p->pml_f.Eyz_z_h,i,k0)];
                    }
                    else if (i1 > pml_p->ex_h)
                    {
                        /*5*/
                        i0 = i1 - pml_p->eoffx_h;
                        pmli = pml_p->epmlx_h - i1;

                        pml_p->pml_f.Eyx_x_h->a[INDEX2D_P(pml_p->pml_f.Eyx_x_h,i0,k)] =
                            pml_p->c_hx[pmli]*pml_p->pml_f.Eyx_x_h->a[INDEX2D_P(pml_p->pml_f.Eyx_x_h,i0,k)] +
                            pml_p->d_hx[pmli]*(fz_p->a[INDEX2D_P(fz_p,i,k)]-fz_p->a[INDEX2D_P(fz_p,i-1,k)]);
                        pml_p->pml_f.Eyz_z_h->a[INDEX2D_P(pml_p->pml_f.Eyz_z_h,i,k0)] =
                            pml_p->c_hz[pmlk]*pml_p->pml_f.Eyz_z_h->a[INDEX2D_P(pml_p->pml_f.Eyz_z_h,i,k0)]-
                            pml_p->d_hz[pmlk]*(fx_p->a[INDEX2D_P(fx_p,i,k)]-fx_p->a[INDEX2D_P(fx_p,i,k-1)]);
                        fy_p->a[INDEX2D_P(fy_p,i,k)] = pml_p->pml_f.Eyx_x_h->a[INDEX2D_P(pml_p->pml_f.Eyx_x_h,i0,k)] +
                                                       pml_p->pml_f.Eyz_z_h->a[INDEX2D_P(pml_p->pml_f.Eyz_z_h,i,k0)];

                    }
                    else
                    {   /*6*/
                        i0 = i - pml_p->eoffx_l;
                        pmli = i1 - pml_p->epmlx_l;

                        pml_p->pml_f.Eyx_x_l->a[INDEX2D_P(pml_p->pml_f.Eyx_x_l,i0,k)] =
                            pml_p->c_ex[pmli]*pml_p->pml_f.Eyx_x_l->a[INDEX2D_P(pml_p->pml_f.Eyx_x_l,i0,k)]+
                            pml_p->d_ex[pmli]*(fz_p->a[INDEX2D_P(fz_p,i,k)]-fz_p->a[INDEX2D_P(fz_p,i-1,k)]);
                        pml_p->pml_f.Eyz_z_h->a[INDEX2D_P(pml_p->pml_f.Eyz_z_h,i,k0)] =
                            pml_p->c_hz[pmlk]*pml_p->pml_f.Eyz_z_h->a[INDEX2D_P(pml_p->pml_f.Eyz_z_h,i,k0)]-
                            pml_p->d_hz[pmlk]*(fx_p->a[INDEX2D_P(fx_p,i,k)]-fx_p->a[INDEX2D_P(fx_p,i,k-1)]);
                        fy_p->a[INDEX2D_P(fy_p,i,k)] = pml_p->pml_f.Eyx_x_l->a[INDEX2D_P(pml_p->pml_f.Eyx_x_l,i0,k)] +
                                                       pml_p->pml_f.Eyz_z_h->a[INDEX2D_P(pml_p->pml_f.Eyz_z_h,i,k0)];
                    }
                }
            }/* main part */
        }/* i */
    }/* k */
}


void update_field_2d_te_fx_new(vfield* vfy_p,
                               vfield* vfx_p,
                               geometry_data* geometry_p,
                               decompose_data* decomp_p,
                               pml_data* pml_p,
                               scheme_constants* sch_p)
{
    int i=0, hx=0, dz=0;
    field* fx_p = vfx_p->comp_p[x_e];
    field* fy_p = vfy_p->comp_p[y_e];

#ifdef ONE_CONFIG_OPENMP
#pragma omp parallel for private(hx,dz)
#endif
    for (i=0; i<pml_p->nhf; i++)
    {
        hx = pml_p->idxhf[i];
        dz = hx + DZ2(fx_p->nx, fx_p->nz);

        fx_p->a[hx] +=
            sch_p->dt_dz*(fy_p->a[dz] - fy_p->a[hx]);
    } /* main part */


#ifdef ONE_CONFIG_OPENMP
#pragma omp parallel for private(hx,dz)
#endif
    for (i=0; i<pml_p->nh; i++)
    {
        hx = pml_p->idxh[i];
        dz = hx + DZ2(fx_p->nx, fx_p->nz);

        fx_p->a[hx] = pml_p->ahz[i]*fx_p->a[hx] + pml_p->bhz[i]*(fy_p->a[dz] - fy_p->a[hx]);
    }
}

void update_field_2d_te_fx(vfield* vfy_p,
                           vfield* vfx_p,
                           geometry_data* geometry_p,
                           decompose_data* decomp_p,
                           pml_data* pml_p,
                           scheme_constants* sch_p)
{
    int i=0, k=0, k1=0, pmlk=0, hx=0;
    field* fx_p = vfx_p->comp_p[x_e];
    field* fy_p = vfy_p->comp_p[y_e];

#ifdef ONE_CONFIG_OPENMP
#pragma omp parallel for private(k,k1,hx,pmlk)
#endif
    for (i=decomp_p->fx_imin; i<decomp_p->fx_imax; i++)
    {
        for (k=decomp_p->fx_kmin; k<decomp_p->fx_kmax; k++)
        {
            k1 = k + geometry_p->k_offset;
            if (k1 >= pml_p->hz_l && k1 <= pml_p->hz_h)
            {
                hx = INDEX2D_P(fx_p,i,k);
                fx_p->a[hx] +=
                    sch_p->dt_dz*(fy_p->a[INDEX2D_P(fy_p,i,k+1)]-fy_p->a[INDEX2D_P(fy_p,i,k)]);
            }
            else if(k1 < pml_p->hz_l)
            {
                pmlk = k1 - pml_p->hpmlz_l;
                fx_p->a[INDEX2D_P(fx_p,i,k)] = pml_p->c_hz[pmlk]*fx_p->a[INDEX2D_P(fx_p,i,k)]-
                                               pml_p->d_hz[pmlk]*(fy_p->a[INDEX2D_P(fy_p,i,k+1)]-fy_p->a[INDEX2D_P(fy_p,i,k)]);
            }
            else
            {
                pmlk = pml_p->hpmlz_h - k1;
                fx_p->a[INDEX2D_P(fx_p,i,k)] = pml_p->c_ez[pmlk]*fx_p->a[INDEX2D_P(fx_p,i,k)]-
                                               pml_p->d_ez[pmlk]*(fy_p->a[INDEX2D_P(fy_p,i,k+1)]-fy_p->a[INDEX2D_P(fy_p,i,k)]);
            }
        } /* i */
    } /* k */
}

void update_field_2d_tm_fx(vfield* vfy_p,
                           vfield* vfx_p,
                           vfield* vjx_p,
                           geometry_data* geometry_p,
                           decompose_data* decomp_p,
                           pml_data* pml_p,
                           scheme_constants* sch_p)
{
    int i=0, k=0, k1=0, pmlk=0, hx=0;
    field* fx_p = vfx_p->comp_p[x_e];
    field* fy_p = vfy_p->comp_p[y_e];
    field* fjx_p = vjx_p->comp_p[x_e];

#ifdef ONE_CONFIG_OPENMP
#pragma omp parallel for private(k,k1,hx,pmlk)
#endif
    for (i=decomp_p->fx_imin; i<decomp_p->fx_imax; i++)
    {
        for (k=decomp_p->fx_kmin; k<decomp_p->fx_kmax; k++)
        {
            k1 = k + geometry_p->k_offset;
            if (k1 >= pml_p->hz_l && k1 <= pml_p->hz_h)
            {
                hx = INDEX2D_P(fx_p,i,k);
                fx_p->a[hx] +=
                    sch_p->dt_dz*(fy_p->a[INDEX2D_P(fy_p,i,k+1)]-fy_p->a[INDEX2D_P(fy_p,i,k)]) -
                    fjx_p->a[hx]*sch_p->dt_4_pi;
            }
            else if(k1 < pml_p->hz_l)
            {
                pmlk = k1 - pml_p->hpmlz_l;
                fx_p->a[INDEX2D_P(fx_p,i,k)] = pml_p->c_hz[pmlk]*fx_p->a[INDEX2D_P(fx_p,i,k)]-
                                               pml_p->d_hz[pmlk]*(fy_p->a[INDEX2D_P(fy_p,i,k+1)]-fy_p->a[INDEX2D_P(fy_p,i,k)]);
            }
            else
            {
                pmlk = pml_p->hpmlz_h - k1;
                fx_p->a[INDEX2D_P(fx_p,i,k)] = pml_p->c_ez[pmlk]*fx_p->a[INDEX2D_P(fx_p,i,k)]-
                                               pml_p->d_ez[pmlk]*(fy_p->a[INDEX2D_P(fy_p,i,k+1)]-fy_p->a[INDEX2D_P(fy_p,i,k)]);
            }
        } /* i */
    } /* k */
}

void update_field_2d_te_fz_new(vfield* vfy_p,
                               vfield* vfz_p,
                               geometry_data* geometry_p,
                               decompose_data* decomp_p,
                               pml_data* pml_p,
                               scheme_constants* sch_p)
{
    int i=0, hz=0, dx=0;
    field* fy_p = vfy_p->comp_p[y_e];
    field* fz_p = vfz_p->comp_p[z_e];

#ifdef ONE_CONFIG_OPENMP
#pragma omp parallel for private(hz,dx)
#endif
    for (i=0; i<pml_p->nhf; i++)
    {
        hz = pml_p->idxhf[i];
        dx = hz + DX2(fz_p->nx, fz_p->nz);

        fz_p->a[hz] +=
            -sch_p->dt_dx*(fy_p->a[dx] - fy_p->a[hz]);
    } /* main part */

#ifdef ONE_CONFIG_OPENMP
#pragma omp parallel for private(hz,dx)
#endif
    for (i=0; i<pml_p->nh; i++)
    {
        hz = pml_p->idxh[i];
        dx = hz + DX2(fz_p->nx, fz_p->nz);

        fz_p->a[hz] = pml_p->ahx[i]*fz_p->a[hz] + pml_p->bhx[i]*(fy_p->a[dx] - fy_p->a[hz]);
    } /* main part */
}


void update_field_2d_te_fz(vfield* vfy_p,
                           vfield* vfz_p,
                           geometry_data* geometry_p,
                           decompose_data* decomp_p,
                           pml_data* pml_p,
                           scheme_constants* sch_p)
{
    int i=0, k=0, i1=0, pmli=0, hz=0;
    field* fy_p = vfy_p->comp_p[y_e];
    field* fz_p = vfz_p->comp_p[z_e];

#ifdef ONE_CONFIG_OPENMP
#pragma omp parallel for private(k,i1,hz,pmli)
#endif
    for (i=decomp_p->fz_imin; i<decomp_p->fz_imax; i++)
    {
        i1 = i + geometry_p->i_offset;
        for (k=decomp_p->fz_kmin; k<decomp_p->fz_kmax; k++)
        {
            if (i1 >= pml_p->hx_l && i1 <= pml_p->hx_h)
            {
                hz = INDEX2D_P(fz_p,i,k);
                fz_p->a[hz] +=
                    -sch_p->dt_dx*(fy_p->a[INDEX2D_P(fy_p,i+1,k)]-fy_p->a[INDEX2D_P(fy_p,i,k)]);
            } /* main part */
            else if(i1 < pml_p->hx_l)
            {
                pmli = i1 - pml_p->hpmlx_l;
                fz_p->a[INDEX2D_P(fz_p,i,k)] = pml_p->c_hx[pmli]*fz_p->a[INDEX2D_P(fz_p,i,k)]+
                                               pml_p->d_hx[pmli]*(fy_p->a[INDEX2D_P(fy_p,i+1,k)]-fy_p->a[INDEX2D_P(fy_p,i,k)]);
            }
            else
            {
                pmli = pml_p->hpmlx_h - i1;
                fz_p->a[INDEX2D_P(fz_p,i,k)] = pml_p->c_ex[pmli]*fz_p->a[INDEX2D_P(fz_p,i,k)]+
                                               pml_p->d_ex[pmli]*(fy_p->a[INDEX2D_P(fy_p,i+1,k)]-fy_p->a[INDEX2D_P(fy_p,i,k)]);
            } /* main part */
        } /* i */
    } /* k */
}

void update_field_2d_tm_fz(vfield* vfy_p,
                           vfield* vfz_p,
                           vfield* vjz_p,
                           geometry_data* geometry_p,
                           decompose_data* decomp_p,
                           pml_data* pml_p,
                           scheme_constants* sch_p)
{
    int i=0, k=0, i1=0, pmli=0, hz=0;
    field* fy_p = vfy_p->comp_p[y_e];
    field* fz_p = vfz_p->comp_p[z_e];
    field* fjz_p = vjz_p->comp_p[z_e];

#ifdef ONE_CONFIG_OPENMP
#pragma omp parallel for private(k,i1,hz,pmli)
#endif
    for (i=decomp_p->fz_imin; i<decomp_p->fz_imax; i++)
    {
        i1 = i + geometry_p->i_offset;
        for (k=decomp_p->fz_kmin; k<decomp_p->fz_kmax; k++)
        {
            if (i1 >= pml_p->hx_l && i1 <= pml_p->hx_h)
            {
                hz = INDEX2D_P(fz_p,i,k);
                fz_p->a[hz] +=
                    -sch_p->dt_dx*(fy_p->a[INDEX2D_P(fy_p,i+1,k)]-fy_p->a[INDEX2D_P(fy_p,i,k)]) -
                    fjz_p->a[hz]*sch_p->dt_4_pi;
            } /* main part */
            else if(i1 < pml_p->hx_l)
            {
                pmli = i1 - pml_p->hpmlx_l;
                fz_p->a[INDEX2D_P(fz_p,i,k)] = pml_p->c_hx[pmli]*fz_p->a[INDEX2D_P(fz_p,i,k)]+
                                               pml_p->d_hx[pmli]*(fy_p->a[INDEX2D_P(fy_p,i+1,k)]-fy_p->a[INDEX2D_P(fy_p,i,k)]);
            }
            else
            {
                pmli = pml_p->hpmlx_h - i1;
                fz_p->a[INDEX2D_P(fz_p,i,k)] = pml_p->c_ex[pmli]*fz_p->a[INDEX2D_P(fz_p,i,k)]+
                                               pml_p->d_ex[pmli]*(fy_p->a[INDEX2D_P(fy_p,i+1,k)]-fy_p->a[INDEX2D_P(fy_p,i,k)]);
            } /* main part */
        } /* i */
    } /* k */
}
#endif

void calculate_energy_2d_te(vfield* vfd_p,
                            vfield* vfe_p,
                            vfield* vfh_p,
                            media_data* media_p,
                            geometry_data* geometry_p,
                            int t)
{
    int i, k, ey, hz, hx;
    field* Ey_p = vfe_p->comp_p[y_e];
    field* Dy_p = vfd_p->comp_p[y_e];
    field* Hx_p = vfh_p->comp_p[x_e];
    field* Hz_p = vfh_p->comp_p[z_e];

    if (t%100 == 0)
    {
        FP energy = 0.;

#ifdef ONE_CONFIG_OPENMP
#pragma omp parallel for private(k,ey,hz,hx) reduction(+:energy)
#endif
        for (i=geometry_p->imin-1; i<geometry_p->imax; i++)
        {
            for (k=geometry_p->kmin-1; k<geometry_p->kmax; k++)
            {
                ey = INDEX2D_P(Ey_p,i,k);
                hz = INDEX2D_P(Hz_p,i,k);
                hx = INDEX2D_P(Hx_p,i,k);
                energy += Ey_p->a[ey]*Dy_p->a[ey] +
                          Hz_p->a[hz]*Hz_p->a[hz] +
                          Hx_p->a[hx]*Hx_p->a[hx];
            }

        }

#ifdef USEMPI
        FP energ = 0;
	MPI_Reduce((void*)&energy,
                   (void*)&energ,
                   1,
                   MPI_FP,
                   MPI_SUM,
                   0,
                   MPI_COMM_WORLD);
	energy  = energ;
#endif
        RANK_S(0);
	fprintf(stderr, "Time step: %6u Energy: %12.10e Zmin: %d Zmax: %u %d\n",
                t, energy*media_p->c_en, 
                geometry_p->imin+geometry_p->move_index,
                geometry_p->imax+geometry_p->move_index, geometry_p->move_index);

        fflush(stderr);
        RANK_F(0);
    }
}

void calculate_energy_2d_tm(vfield* vfd_p,
                            vfield* vfe_p,
                            vfield* vfh_p,
                            media_data* media_p,
                            geometry_data* geometry_p,
                            int t)
{
    int i, k, hy, ez, ex;
    field* Ex_p = vfe_p->comp_p[x_e];
    field* Dx_p = vfd_p->comp_p[x_e];
    field* Ez_p = vfe_p->comp_p[z_e];
    field* Dz_p = vfd_p->comp_p[z_e];
    field* Hy_p = vfh_p->comp_p[y_e];

    if (t%100 == 0)
    {
        FP energy = 0.;
#ifdef ONE_CONFIG_OPENMP
#pragma omp parallel for private(k,hy,ez,ex) reduction(+:energy)
#endif
        for (i=geometry_p->imin-1; i<geometry_p->imax; i++)
        {
            for (k=geometry_p->kmin-1; k<geometry_p->kmax; k++)
            {
                hy = INDEX2D_P(Hy_p,i,k);
                ez = INDEX2D_P(Ez_p,i,k);
                ex = INDEX2D_P(Ex_p,i,k);
                energy += Hy_p->a[hy]*Hy_p->a[hy] +
                          Ez_p->a[ez]*Dz_p->a[ez] +
                          Dx_p->a[ex]*Ex_p->a[ex];
            }

        }

#ifdef USEMPI
        FP energ = 0;
	MPI_Reduce((void*)&energy,
                   (void*)&energ,
                   1,
                   MPI_FP,
                   MPI_SUM,
                   0,
                   MPI_COMM_WORLD);
	energy  = energ;
#endif
        RANK_S(0);
        fprintf(stderr, "Time step: %6u Energy: %12.10e Zmin: %d Zmax: Move=%d\n",
                t, energy*media_p->c_en,
                geometry_p->imin+geometry_p->move_index,
		geometry_p->imax+geometry_p->move_index);
        RANK_F(0);
    }
}

void calculate_e_from_d_2d_te(vfield* vfd_p,
                              vfield* vfe_p,
                              media_data* media_p,
                              geometry_data* geometry_p,
                              decompose_data* decomp_p)
{
    int i,k,i_temp,index;
    FP Dy_t,Ey_t,Dy_n,abs,eps;

    field* Ey_p = vfe_p->comp_p[y_e];
    field* Dy_p = vfd_p->comp_p[y_e];

#ifdef ONE_CONFIG_OPENMP
#pragma omp parallel for private(k,i_temp,index,Dy_t,Ey_t,abs,eps)
#endif
#ifndef USEMPI
    for (i = decomp_p->fx_imin; i < decomp_p->fx_imax; i++)
    {
        for (k = decomp_p->fz_kmin; k < decomp_p->fz_kmax; k++)
        {
#else
    for (i = decomp_p->fx_imin; i < decomp_p->fx_imax+1; i++)
    {
        for (k = decomp_p->fz_kmin; k < decomp_p->fz_kmax+1; k++)
        {
#endif
            i_temp = 0;
            index = INDEX2D_P(Dy_p,i,k);

            if (media_p->n_max.a[index] > 0)
            {
                Dy_t = Dy_p->a[index];
                Ey_t = Dy_p->a[index];
                abs = Ey_t*Ey_t;
                eps = calculate_epsilon(media_p,abs,index);

                Ey_p->a[index] = Dy_t / eps;

                if (media_p->kerr > 0)
                {
                    abs = Ey_p->a[index]*Ey_p->a[index];
                    eps = calculate_epsilon(media_p,abs,index);

                    Dy_n = Ey_p->a[index]*eps;

                    while(fabs(Dy_n - Dy_t)>1e-7 &&
                            i_temp < 20)
                    {
                        Ey_p->a[index] = Dy_t / eps;
                        abs = Ey_p->a[index]*Ey_p->a[index];
                        eps = calculate_epsilon(media_p,abs,index);
                        Dy_n = Ey_p->a[index]*eps;
                        i_temp++;
                    }
                }
            }
            else
            {
                Ey_p->a[index] = Dy_p->a[index]/media_p->epsilon.a[index];
            }
        }
    }
}


void calculate_e_from_d_2d_tm(vfield* vfd_p,
                              vfield* vfe_p,
                              vfield* vhy_p,
                              media_data* media_p,
                              geometry_data* geometry_p,
                              decompose_data* decomp_p)
{
    int i,k,i_temp,index;
    FP Dx_t,Ex_t,Dx_n,Dz_t,Ez_t,Dz_n,abs,eps;

    field* Ex_p = vfe_p->comp_p[x_e];
    field* Dx_p = vfd_p->comp_p[x_e];
    field* Ez_p = vfe_p->comp_p[z_e];
    field* Dz_p = vfd_p->comp_p[z_e];

#ifdef ONE_CONFIG_OPENMP
#pragma omp parallel for private(k,i_temp,index,Dx_t,Ex_t,Dx_n,Dz_t,Ez_t,Dz_n,abs,eps)
#endif
#ifndef USEMPI
    for (i = decomp_p->fx_imin; i < decomp_p->fx_imax; i++)
    {
        for (k = decomp_p->fz_kmin; k < decomp_p->fz_kmax; k++)
        {
#else
    for (i = decomp_p->fx_imin; i < decomp_p->fx_imax+1; i++)
    {
        for (k = decomp_p->fz_kmin; k < decomp_p->fz_kmax+1; k++)
        {
#endif
            i_temp = 0;
            index = INDEX2D_P(Dx_p,i,k);

            if (media_p->n_max.a[index] > 0)
            {
                Dz_t = Dz_p->a[index];
                Dx_t = Dx_p->a[index];
                Ez_t = Dz_p->a[index];
                Ex_t = Dx_p->a[index];

                abs = Ez_t*Ez_t + Ex_t*Ex_t;
                eps = calculate_epsilon(media_p, abs, index);

                Ex_p->a[index] = Dx_t / eps;
                Ez_p->a[index] = Dz_t / eps;

                if (media_p->kerr > 0)
                {
                    abs = Ex_p->a[index]*Ex_p->a[index] + Ez_p->a[index]*Ez_p->a[index];
                    eps = calculate_epsilon(media_p, abs, index);

                    Dx_n = Ex_p->a[index]*eps;
                    Dz_n = Ez_p->a[index]*eps;

                    while(fabs(Dx_n - Dx_t)>1e-7 && fabs(Dz_n - Dz_t)>1e-7 &&
                            i_temp < 20)
                    {
                        Ex_p->a[index] = Dx_t / eps;
                        Ez_p->a[index] = Dz_t / eps;

                        abs = Ex_p->a[index]*Ex_p->a[index] + Ez_p->a[index]*Ez_p->a[index];
                        eps = calculate_epsilon(media_p, abs, index);
                        Dx_n = Ex_p->a[index]*eps;
                        Dz_n = Ez_p->a[index]*eps;
                        i_temp++;
                    }
                }
            }
            else
            {
                Ex_p->a[index] = Dx_p->a[index]/media_p->epsilon.a[index];
                Ez_p->a[index] = Dz_p->a[index]/media_p->epsilon.a[index];
            }
        }
    }
}

void init_moving_window_2d(geometry_data* g_p,
                           sources_rt_data* s_p,
                           pml_data* pml_p,
                           time_scheme* ts_p,
                           scheme_constants* sch_p)
{
    if (s_p->pulses.pulse_p[0].env.type == tanh_e)
    {
        ts_p->t0 = (int)((s_p->pulses.pulse_p[0].tdelay + g_p->x_max*g_p->delta_x*0.8)/ts_p->delta_t);
    }
    else
    {
        ts_p->t0 = (int)((s_p->pulses.pulse_p[0].tdelay + g_p->x_max*g_p->delta_x -
                          2*s_p->pulses.pulse_p[0].env.tp/3.33333)/ts_p->delta_t);
    }
    g_p->move = (int)(ts_p->t_move * ts_p->delta_t / g_p->delta_x);
    g_p->move_index = 0;
    if (ts_p->t_start > ts_p->t0)
    {
        g_p->move_index =
            pml_p->pmlx + 2 + (ts_p->t_start - ts_p->t0)/ts_p->t_move*g_p->move;
        zero_pml_x(pml_p,
                   g_p,
                   sch_p);
        zero_sources_rt(s_p);
    }

    int n_shift = ((ts_p->t_max - ts_p->t0)/ts_p->t_move + 1);
    g_p->move_max = g_p->move*n_shift+g_p->x_max;
    RANK_S(0)
    fprintf(stderr, "Moving window: z_min=%lf t_move=%d t0=%d move=%d\n",
            (int)(6.*s_p->pulses.pulse_p[0].env.t0 + s_p->pulses.pulse_p[0].env.tp)/g_p->delta_x,
            ts_p->t_move,
            ts_p->t0,
            g_p->move);
    RANK_F(0)
}

void update_moving_boundary(geometry_data* geometry_p,
                            pml_data* pml_p,
                            scheme_constants* sch_p,
                            int t)
{
    /* Update moving frame */
    if (0 < geometry_p->moving)
    {
        /* moving front */
        geometry_p->imax =
            (int)((FP)t * sch_p->dt_dx) + pml_p->pmlx + 10 -
            geometry_p->i_offset;
        geometry_p->imin = 1;

        if (geometry_p->imax > geometry_p->x_max-1)
        {
            geometry_p->imax = geometry_p->x_max-1;
        }
        else if (geometry_p->imax < 1)
        {
            geometry_p->imax = 1;
        }
    }
}

#if !defined(USE_PTHREADS) && !defined(CUDA)
int run_2D_TE_config(configTable* fdtdConfig,
                     geometry_data geometry,
                     time_scheme timescheme,
                     const char* pathPrefix)
{
    MPI_INIT_SENDRECV(&mpi, &geometry);

    sources_rt_data sources_rt;
    pml_data        pml;
    media_data      media;
    points_data_2d  pdata;
    save_data       sdata;
    scheme_constants scheme;
    decompose_data* decomp_p = NULL;

    decomp_p = init_decomposition(fdtdConfig,
                                  &geometry);

    init_constants(&scheme,
                   &geometry,
                   &timescheme);

    init_pml(fdtdConfig, &pml, &geometry, &timescheme, &scheme);

    init_sources_rt(fdtdConfig, &geometry, &pml, &sources_rt, NULL, NULL);

    init_savedata(fdtdConfig, &sdata);

    field Ey, Ey_r, Jy, Hx, Hz;
    field* Ey_p;
    vfield E, D, H, J;

    init_field(&Ey,  geometry.x_max, geometry.z_max);
    init_field(&Jy,  geometry.x_max, geometry.z_max);
    init_field(&Hx,  geometry.x_max, geometry.z_max);
    init_field(&Hz,  geometry.x_max, geometry.z_max);

    if (geometry.moving == 2)
    {
        init_moving_window_2d(&geometry, &sources_rt, &pml, &timescheme, &scheme);
    }

    init_media(fdtdConfig,
               &media,
               &geometry,
               &timescheme,
               &scheme);

    if (media.recalc == true)
    {
        /* Initialize 'real' field Ey */
        init_field(&Ey_r, geometry.x_max, geometry.z_max);
        Ey_p = &Ey_r;
    }
    else
    {
        init_field(&Ey_r,  0, 0);
        Ey_p = &Ey;
    }

    assemble_vfield(&D, NULL, &Ey, NULL);
    assemble_vfield(&E, NULL, Ey_p, NULL);
    assemble_vfield(&J, NULL, &Jy, NULL);
    assemble_vfield(&H, &Hx, NULL, &Hz);

    RANK_S(0);
    print_sources_rt(&sources_rt,
                     &media,
                     &geometry,
                     &timescheme,
                     pathPrefix);
    print_media(&media);

    fprintf(stderr, "\nRunning main loop\n");
    RANK_F(0);

    init_point_data_2d(fdtdConfig,
                       &pdata,
                       pathPrefix,
                       &timescheme,
                       &geometry,
                       Ey_p);

    for (int t = 0; t < timescheme.t_max+1; t++)
    {
        update_moving_boundary(&geometry,
                               &pml,
                               &scheme,
                               t);

        update_sources_rt(&sources_rt,
                          &geometry,
                          &pml,
                          t);


        update_field_2d_te_fy_new(&D,
                                  &H,
                                  &J,
                                  &geometry,
                                  decomp_p,
                                  &pml,
                                  &scheme);

        add_sources_2d_fy(&D,
                          &geometry,
                          &sources_rt,
                          &scheme);

        MPI_EXCHANGE_UP_2D_X(&mpi, &geometry, &Ey);
        MPI_EXCHANGE_UP_2D_Z(&mpi, &geometry, &Ey);

        if (media.recalc == true)
        {
            calculate_e_from_d_2d_te(&D,
                                     &E,
                                     &media,
                                     &geometry,
                                     decomp_p);
        }

        update_field_2d_te_fx_new(&E,
                                  &H,
                                  &geometry,
                                  decomp_p,
                                  &pml,
                                  &scheme);

        MPI_EXCHANGE_DOWN_2D_Z(&mpi, &geometry, &Hx);
        MPI_EXCHANGE_DOWN_2D_X(&mpi, &geometry, &Hx);


        add_sources_2d_fx(&H,
                          &geometry,
                          &sources_rt,
                          &scheme);

        update_field_2d_te_fz_new(&E,
                                  &H,
                                  &geometry,
                                  decomp_p,
                                  &pml,
                                  &scheme);

        MPI_EXCHANGE_DOWN_2D_Z(&mpi, &geometry, &Hz);
        MPI_EXCHANGE_DOWN_2D_X(&mpi, &geometry, &Hz);

        add_sources_2d_fz(&H,
                          &geometry,
                          &sources_rt,
                          &scheme);

        calculate_energy_2d_te(&D,
                               &E,
                               &H,
                               &media,
                               &geometry,
                               t);

	store_point_data_2d(&pdata,
                            &geometry,
                            t,
                            &Hx,
                            Ey_p,
                            &Hz);
                            
        if (t == 0)
        {
            savedata_save(fdtdConfig,
                          &sdata,
                          &geometry,
                          &pml,
                          t,
                          &Hx,
                          &Hz,
                          Ey_p,
			  pathPrefix,
                          false);
        }
        else
        {
            savedata_save(fdtdConfig,
                          &sdata,
                          &geometry,
                          &pml,
                          t,
                          &Hx,
                          &Hz,
                          Ey_p,
			  pathPrefix,
                          false);
        }

	if (geometry.moving == 2)
        {
            if (t > timescheme.t0)
            {
                if  ((t-timescheme.t0)%timescheme.t_move == 0)
                {
		    swap_field(&Ey, geometry.move);
                    swap_field(&Jy, geometry.move);
                    swap_field(&Hx, geometry.move);
                    swap_field(&Hz, geometry.move);
                    swap_pml_2d(&pml, &geometry, geometry.move);
                    geometry.move_index += geometry.move;
                    swap_media_2d(&media,
                                  &geometry,
                                  &pml,
                                  geometry.move);
                    update_point_data_2d(&pdata,
                                         &geometry,
                                         t,
                                         Ey_p);
                }
            }
            else if (t == timescheme.t0)
            {
                zero_sources_rt(&sources_rt);

                swap_field(&Ey, pml.pmlx+2);
                swap_field(&Jy, pml.pmlx+2);
                swap_field(&Hx, pml.pmlx+2);
                swap_field(&Hz, pml.pmlx+2);

                swap_pml_2d(&pml, &geometry, pml.pmlx+2);

                geometry.move_index += pml.pmlx+2;

                swap_media_2d(&media,
                              &geometry,
                              &pml,
                              pml.pmlx+2);
                zero_pml_x(&pml,
                           &geometry,
                           &scheme);

                update_point_data_2d(&pdata,
                                     &geometry,
                                     t,
                                     Ey_p);
            }
        }
    }/*for t */



    if (geometry.moving == 2)
    {
        geometry.move_index -= geometry.move;
    }

    destroy_point_data_2d(&pdata);
    destroy_sources_rt(&sources_rt, &geometry);
    destroy_media(&media);
    destroy_pml2d(&pml);
    destroy_decomposition(decomp_p);
    destroy_field(&Ey);
    destroy_field(&Ey_r);
    destroy_field(&Jy);
    destroy_field(&Hx);
    destroy_field(&Hz);

    MPI_DELETE_SENDRECV(&mpi);
    return 0;
}

int run_2D_TM_config(configTable* fdtdConfig,
                     geometry_data geometry,
                     time_scheme timescheme,
                     const char* pathPrefix)
{
    MPI_INIT_SENDRECV(&mpi, &geometry);

    sources_rt_data sources_rt;
    pml_data        pml;
    media_data      media;
    points_data_2d  pdata;
    save_data       sdata;
    scheme_constants scheme;
    decompose_data* decomp_p = NULL;

    decomp_p = init_decomposition(fdtdConfig, &geometry);

    init_constants(&scheme, &geometry, &timescheme);

    init_pml(fdtdConfig, &pml, &geometry, &timescheme, &scheme);

    init_sources_rt(fdtdConfig, &geometry, &pml, &sources_rt, NULL, NULL);

    init_savedata(fdtdConfig, &sdata);

    field Hy, Jx, Jz, Ex, Ez, Ex_r, Ez_r;
    field *Ex_p, *Ez_p;
    vfield E, D, H, J;

    init_field(&Hy, geometry.x_max, geometry.z_max);
    init_field(&Jz, geometry.x_max, geometry.z_max);
    init_field(&Jx, geometry.x_max, geometry.z_max);
    init_field(&Ex, geometry.x_max, geometry.z_max);
    init_field(&Ez, geometry.x_max, geometry.z_max);

    if (geometry.moving == 2)
    {
        init_moving_window_2d(&geometry, &sources_rt, &pml, &timescheme, &scheme);
    }

    init_media(fdtdConfig,
               &media,
               &geometry,
               &timescheme,
               &scheme);

    if (media.recalc == true)
    {
        init_field(&Ex_r, geometry.x_max, geometry.z_max);
        init_field(&Ez_r, geometry.x_max, geometry.z_max);
        Ex_p = &Ex_r;
        Ez_p = &Ez_r;
    }
    else
    {
        init_field(&Ex_r, 0, 0);
        init_field(&Ez_r, 0, 0);
        Ex_p = &Ex;
        Ez_p = &Ez;
    }

    assemble_vfield(&D, &Ex, NULL, &Ez);
    assemble_vfield(&E, Ex_p, NULL, Ez_p);
    assemble_vfield(&J, &Jx, NULL, &Jz);
    assemble_vfield(&H, NULL, &Hy, NULL);

    RANK_S(0);
    print_sources_rt(&sources_rt,
                     &media,
                     &geometry,
                     &timescheme,
                     pathPrefix);
    print_media(&media);
    fprintf(stderr, "\nRunning main loop\n");
    RANK_F(0);

    init_point_data_2d(fdtdConfig,
                       &pdata,
                       pathPrefix,
                       &timescheme,
                       &geometry,
                       &Hy);



    for (int t = 0; t < timescheme.t_max+1; t++)
    {
        update_moving_boundary(&geometry,
                               &pml,
                               &scheme,
                               t);

        update_sources_rt(&sources_rt,
                          &geometry,
                          &pml,
                          t);

        update_field_2d_tm_fx(&H,
                              &D,
                              &J,
                              &geometry,
                              decomp_p,
                              &pml,
                              &scheme);

        MPI_EXCHANGE_DOWN_2D_Z(&mpi, &geometry, &Ex);
        MPI_EXCHANGE_DOWN_2D_X(&mpi, &geometry, &Ex);

        update_field_2d_tm_fz(&H,
                              &D,
                              &J,
                              &geometry,
                              decomp_p,
                              &pml,
                              &scheme);

        MPI_EXCHANGE_DOWN_2D_Z(&mpi, &geometry, &Ez);
        MPI_EXCHANGE_DOWN_2D_X(&mpi, &geometry, &Ez);

        if (media.recalc == true)
        {
            calculate_e_from_d_2d_tm(&D,
                                     &E,
                                     &H,
                                     &media,
                                     &geometry,
                                     decomp_p);
        }

        add_sources_2d_fx(&D,
                          &geometry,
                          &sources_rt,
                          &scheme);

        add_sources_2d_fz(&D,
                          &geometry,
                          &sources_rt,
                          &scheme);


        update_field_2d_tm_fy(&H,
                              &E,
                              &geometry,
                              decomp_p,
                              &pml,
                              &scheme);

        add_sources_2d_fy(&H,
                          &geometry,
                          &sources_rt,
                          &scheme);

        MPI_EXCHANGE_UP_2D_X(&mpi, &geometry, &Hy);
        MPI_EXCHANGE_UP_2D_Z(&mpi, &geometry, &Hy);

        calculate_energy_2d_tm(&D,
                               &E,
                               &H,
                               &media,
                               &geometry,
                               t);

	store_point_data_2d(&pdata,
                            &geometry,
                            t,
                            Ex_p,
                            &Hy,
                            Ez_p);
        if (t == 0)
        {
            savedata_save(fdtdConfig,
                          &sdata,
                          &geometry,
                          &pml,
                          t,
                          Ex_p,
                          Ez_p,
                          &Hy,
			  pathPrefix,
                          false);
        }
        else
        {
            savedata_save(fdtdConfig,
                          &sdata,
                          &geometry,
                          &pml,
                          t,
                          Ex_p,
                          Ez_p,
                          &Hy,
			  pathPrefix,
                          false);
        }

	if (geometry.moving == 2)
        {
            if (t > timescheme.t0)
            {
                if  ((t-timescheme.t0)%timescheme.t_move == 0)
                {
		    swap_field(&Hy, geometry.move);
                    swap_field(&Jx, geometry.move);
                    swap_field(&Jz, geometry.move);
                    swap_field(&Ex, geometry.move);
                    swap_field(&Ez, geometry.move);
                    swap_pml_2d(&pml, &geometry, geometry.move);
                    geometry.move_index += geometry.move;
                    swap_media_2d(&media,
                                  &geometry,
                                  &pml,
                                  geometry.move);
                    update_point_data_2d(&pdata,
                                         &geometry,
                                         t,
                                         &Hy);
                }
            }
            else if (t == timescheme.t0)
            {
                zero_sources_rt(&sources_rt);
                swap_field(&Hy, pml.pmlx+2);
                swap_field(&Jx, pml.pmlx+2);
                swap_field(&Jz, pml.pmlx+2);
                swap_field(&Ex, pml.pmlx+2);
                swap_field(&Ez, pml.pmlx+2);
                swap_pml_2d(&pml, &geometry, pml.pmlx+2);
                geometry.move_index += pml.pmlx+2;
                swap_media_2d(&media,
                              &geometry,
                              &pml,
                              pml.pmlx+2);
                zero_pml_x(&pml,
                           &geometry,
                           &scheme);
                update_point_data_2d(&pdata,
                                     &geometry,
                                     t,
                                     &Hy);
            }
        }
    }/*for t */

    if (geometry.moving == 2)
    {
        geometry.move_index -= geometry.move;
    }

    destroy_point_data_2d(&pdata);
    destroy_sources_rt(&sources_rt, &geometry);
    destroy_media(&media);
    destroy_pml2d(&pml);
    destroy_decomposition(decomp_p);
    destroy_field(&Hy);
    destroy_field(&Ex_r);
    destroy_field(&Ez_r);
    destroy_field(&Ex);
    destroy_field(&Ez);
    destroy_field(&Jx);
    destroy_field(&Jz);

    MPI_DELETE_SENDRECV(&mpi);
    return 0;
}
#endif
