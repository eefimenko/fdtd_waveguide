#include "fdtd.h"

#ifdef USEMPI
mpi_data mpi;
#endif

int main(int argc, char* argv[])
{
    int time;

    INIT_MPI(argc, argv, &mpi);

    RANK_S(0)
    fprintf(stderr, "Waveguide FDTD program\n");
    fprintf(stderr, "Author: Evgeny Efimenko\n");
    fprintf(stderr, "Author: evgeny.efimenko@gmail.com\n");
    fprintf(stderr, "Version: 1.0.0\n");
    fprintf(stderr, "Build time: %s %s\n", __DATE__, __TIME__);
    
#ifdef USENDF
    fprintf(stderr, "Built with NDF\n");
#endif
    
#ifdef USEMPI
    fprintf(stderr, "Built with MPI\n");
#endif
    
#ifdef ONE_CONFIG_OPENMP
    fprintf(stderr, "Built with OpenMP\n");
#endif
    
#ifdef USELOG
    fprintf(stderr, "Built with log library\n");
#endif
    
#ifndef CUDA
    
#ifdef FULL_UPDATE
    fprintf(stderr, "Built with full update scheme\n");
#else
    fprintf(stderr, "Built with old update scheme\n");
#endif
    
#else
    
#ifdef FULL_UPDATE
    fprintf(stderr, "Built with CUDA\n");
#else
    fprintf(stderr, "CUDA version must be built with FULL_UPDATE only\n");
    fprintf(stderr, "Please rebuild program\n");
    exit(-1);
#endif
    
#endif
    RANK_F(0)

    if (argc == 1)
    {
        RANK_S(0)
	    info_text_s("No cfg files. Usage: <binary> <cfg-files>\n");
        RANK_F(0)
        return -1;
    }

    if (0 == strcmp("-e", argv[1]) || 0 == strcmp("--estimate", argv[1]))
    {
        RANK_S(0);
        info_s0("Estimation started");
        RANK_F(0);
        for (int i = 2; i < argc; i++)
        {
            time = run_one_config(argv[i], true);
            RANK_S(0);
            info_text_s("%d config: %s. Estimation time: %d sec\n", i-1, argv[i], time);
            RANK_F(0);
        }
        RANK_S(0);
        info_s0("Estimation finished");
        RANK_F(0);
    }
    else
    {
        for (int i = 1; i < argc; i++)
        {
            time = run_one_config(argv[i], false);
            RANK_S(0);
            info_text_s("%d config: %s. Run time: %d sec\n", i, argv[i], time);
            RANK_F(0);
        }
    }
    FINALIZE_MPI();
    return 0;
}
